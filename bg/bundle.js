/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(Object.prototype.hasOwnProperty.call(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"bg": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push(["./source/bg/app.js","vendors"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/@babel/runtime/helpers/get.js":
/*!****************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/get.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var superPropBase = __webpack_require__(/*! ./superPropBase */ "./node_modules/@babel/runtime/helpers/superPropBase.js");

function _get(target, property, receiver) {
  if (typeof Reflect !== "undefined" && Reflect.get) {
    module.exports = _get = Reflect.get;
  } else {
    module.exports = _get = function _get(target, property, receiver) {
      var base = superPropBase(target, property);
      if (!base) return;
      var desc = Object.getOwnPropertyDescriptor(base, property);

      if (desc.get) {
        return desc.get.call(receiver);
      }

      return desc.value;
    };
  }

  return _get(target, property, receiver || target);
}

module.exports = _get;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/objectWithoutProperties.js":
/*!************************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/objectWithoutProperties.js ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var objectWithoutPropertiesLoose = __webpack_require__(/*! ./objectWithoutPropertiesLoose */ "./node_modules/@babel/runtime/helpers/objectWithoutPropertiesLoose.js");

function _objectWithoutProperties(source, excluded) {
  if (source == null) return {};
  var target = objectWithoutPropertiesLoose(source, excluded);
  var key, i;

  if (Object.getOwnPropertySymbols) {
    var sourceSymbolKeys = Object.getOwnPropertySymbols(source);

    for (i = 0; i < sourceSymbolKeys.length; i++) {
      key = sourceSymbolKeys[i];
      if (excluded.indexOf(key) >= 0) continue;
      if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue;
      target[key] = source[key];
    }
  }

  return target;
}

module.exports = _objectWithoutProperties;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/objectWithoutPropertiesLoose.js":
/*!*****************************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/objectWithoutPropertiesLoose.js ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _objectWithoutPropertiesLoose(source, excluded) {
  if (source == null) return {};
  var target = {};
  var sourceKeys = Object.keys(source);
  var key, i;

  for (i = 0; i < sourceKeys.length; i++) {
    key = sourceKeys[i];
    if (excluded.indexOf(key) >= 0) continue;
    target[key] = source[key];
  }

  return target;
}

module.exports = _objectWithoutPropertiesLoose;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/superPropBase.js":
/*!**************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/superPropBase.js ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var getPrototypeOf = __webpack_require__(/*! ./getPrototypeOf */ "./node_modules/@babel/runtime/helpers/getPrototypeOf.js");

function _superPropBase(object, property) {
  while (!Object.prototype.hasOwnProperty.call(object, property)) {
    object = getPrototypeOf(object);
    if (object === null) break;
  }

  return object;
}

module.exports = _superPropBase;

/***/ }),

/***/ "./node_modules/uuid/dist/esm-browser/bytesToUuid.js":
/*!***********************************************************!*\
  !*** ./node_modules/uuid/dist/esm-browser/bytesToUuid.js ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/**
 * Convert array of 16 byte values to UUID string format of the form:
 * XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX
 */
var byteToHex = [];

for (var i = 0; i < 256; ++i) {
  byteToHex[i] = (i + 0x100).toString(16).substr(1);
}

function bytesToUuid(buf, offset) {
  var i = offset || 0;
  var bth = byteToHex; // join used to fix memory issue caused by concatenation: https://bugs.chromium.org/p/v8/issues/detail?id=3175#c4

  return [bth[buf[i++]], bth[buf[i++]], bth[buf[i++]], bth[buf[i++]], '-', bth[buf[i++]], bth[buf[i++]], '-', bth[buf[i++]], bth[buf[i++]], '-', bth[buf[i++]], bth[buf[i++]], '-', bth[buf[i++]], bth[buf[i++]], bth[buf[i++]], bth[buf[i++]], bth[buf[i++]], bth[buf[i++]]].join('');
}

/* harmony default export */ __webpack_exports__["default"] = (bytesToUuid);

/***/ }),

/***/ "./node_modules/uuid/dist/esm-browser/index.js":
/*!*****************************************************!*\
  !*** ./node_modules/uuid/dist/esm-browser/index.js ***!
  \*****************************************************/
/*! exports provided: v1, v3, v4, v5 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _v1_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./v1.js */ "./node_modules/uuid/dist/esm-browser/v1.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "v1", function() { return _v1_js__WEBPACK_IMPORTED_MODULE_0__["default"]; });

/* harmony import */ var _v3_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./v3.js */ "./node_modules/uuid/dist/esm-browser/v3.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "v3", function() { return _v3_js__WEBPACK_IMPORTED_MODULE_1__["default"]; });

/* harmony import */ var _v4_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./v4.js */ "./node_modules/uuid/dist/esm-browser/v4.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "v4", function() { return _v4_js__WEBPACK_IMPORTED_MODULE_2__["default"]; });

/* harmony import */ var _v5_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./v5.js */ "./node_modules/uuid/dist/esm-browser/v5.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "v5", function() { return _v5_js__WEBPACK_IMPORTED_MODULE_3__["default"]; });






/***/ }),

/***/ "./node_modules/uuid/dist/esm-browser/md5.js":
/*!***************************************************!*\
  !*** ./node_modules/uuid/dist/esm-browser/md5.js ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/*
 * Browser-compatible JavaScript MD5
 *
 * Modification of JavaScript MD5
 * https://github.com/blueimp/JavaScript-MD5
 *
 * Copyright 2011, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * https://opensource.org/licenses/MIT
 *
 * Based on
 * A JavaScript implementation of the RSA Data Security, Inc. MD5 Message
 * Digest Algorithm, as defined in RFC 1321.
 * Version 2.2 Copyright (C) Paul Johnston 1999 - 2009
 * Other contributors: Greg Holt, Andrew Kepert, Ydnar, Lostinet
 * Distributed under the BSD License
 * See http://pajhome.org.uk/crypt/md5 for more info.
 */
function md5(bytes) {
  if (typeof bytes == 'string') {
    var msg = unescape(encodeURIComponent(bytes)); // UTF8 escape

    bytes = new Array(msg.length);

    for (var i = 0; i < msg.length; i++) {
      bytes[i] = msg.charCodeAt(i);
    }
  }

  return md5ToHexEncodedArray(wordsToMd5(bytesToWords(bytes), bytes.length * 8));
}
/*
 * Convert an array of little-endian words to an array of bytes
 */


function md5ToHexEncodedArray(input) {
  var i;
  var x;
  var output = [];
  var length32 = input.length * 32;
  var hexTab = '0123456789abcdef';
  var hex;

  for (i = 0; i < length32; i += 8) {
    x = input[i >> 5] >>> i % 32 & 0xff;
    hex = parseInt(hexTab.charAt(x >>> 4 & 0x0f) + hexTab.charAt(x & 0x0f), 16);
    output.push(hex);
  }

  return output;
}
/*
 * Calculate the MD5 of an array of little-endian words, and a bit length.
 */


function wordsToMd5(x, len) {
  /* append padding */
  x[len >> 5] |= 0x80 << len % 32;
  x[(len + 64 >>> 9 << 4) + 14] = len;
  var i;
  var olda;
  var oldb;
  var oldc;
  var oldd;
  var a = 1732584193;
  var b = -271733879;
  var c = -1732584194;
  var d = 271733878;

  for (i = 0; i < x.length; i += 16) {
    olda = a;
    oldb = b;
    oldc = c;
    oldd = d;
    a = md5ff(a, b, c, d, x[i], 7, -680876936);
    d = md5ff(d, a, b, c, x[i + 1], 12, -389564586);
    c = md5ff(c, d, a, b, x[i + 2], 17, 606105819);
    b = md5ff(b, c, d, a, x[i + 3], 22, -1044525330);
    a = md5ff(a, b, c, d, x[i + 4], 7, -176418897);
    d = md5ff(d, a, b, c, x[i + 5], 12, 1200080426);
    c = md5ff(c, d, a, b, x[i + 6], 17, -1473231341);
    b = md5ff(b, c, d, a, x[i + 7], 22, -45705983);
    a = md5ff(a, b, c, d, x[i + 8], 7, 1770035416);
    d = md5ff(d, a, b, c, x[i + 9], 12, -1958414417);
    c = md5ff(c, d, a, b, x[i + 10], 17, -42063);
    b = md5ff(b, c, d, a, x[i + 11], 22, -1990404162);
    a = md5ff(a, b, c, d, x[i + 12], 7, 1804603682);
    d = md5ff(d, a, b, c, x[i + 13], 12, -40341101);
    c = md5ff(c, d, a, b, x[i + 14], 17, -1502002290);
    b = md5ff(b, c, d, a, x[i + 15], 22, 1236535329);
    a = md5gg(a, b, c, d, x[i + 1], 5, -165796510);
    d = md5gg(d, a, b, c, x[i + 6], 9, -1069501632);
    c = md5gg(c, d, a, b, x[i + 11], 14, 643717713);
    b = md5gg(b, c, d, a, x[i], 20, -373897302);
    a = md5gg(a, b, c, d, x[i + 5], 5, -701558691);
    d = md5gg(d, a, b, c, x[i + 10], 9, 38016083);
    c = md5gg(c, d, a, b, x[i + 15], 14, -660478335);
    b = md5gg(b, c, d, a, x[i + 4], 20, -405537848);
    a = md5gg(a, b, c, d, x[i + 9], 5, 568446438);
    d = md5gg(d, a, b, c, x[i + 14], 9, -1019803690);
    c = md5gg(c, d, a, b, x[i + 3], 14, -187363961);
    b = md5gg(b, c, d, a, x[i + 8], 20, 1163531501);
    a = md5gg(a, b, c, d, x[i + 13], 5, -1444681467);
    d = md5gg(d, a, b, c, x[i + 2], 9, -51403784);
    c = md5gg(c, d, a, b, x[i + 7], 14, 1735328473);
    b = md5gg(b, c, d, a, x[i + 12], 20, -1926607734);
    a = md5hh(a, b, c, d, x[i + 5], 4, -378558);
    d = md5hh(d, a, b, c, x[i + 8], 11, -2022574463);
    c = md5hh(c, d, a, b, x[i + 11], 16, 1839030562);
    b = md5hh(b, c, d, a, x[i + 14], 23, -35309556);
    a = md5hh(a, b, c, d, x[i + 1], 4, -1530992060);
    d = md5hh(d, a, b, c, x[i + 4], 11, 1272893353);
    c = md5hh(c, d, a, b, x[i + 7], 16, -155497632);
    b = md5hh(b, c, d, a, x[i + 10], 23, -1094730640);
    a = md5hh(a, b, c, d, x[i + 13], 4, 681279174);
    d = md5hh(d, a, b, c, x[i], 11, -358537222);
    c = md5hh(c, d, a, b, x[i + 3], 16, -722521979);
    b = md5hh(b, c, d, a, x[i + 6], 23, 76029189);
    a = md5hh(a, b, c, d, x[i + 9], 4, -640364487);
    d = md5hh(d, a, b, c, x[i + 12], 11, -421815835);
    c = md5hh(c, d, a, b, x[i + 15], 16, 530742520);
    b = md5hh(b, c, d, a, x[i + 2], 23, -995338651);
    a = md5ii(a, b, c, d, x[i], 6, -198630844);
    d = md5ii(d, a, b, c, x[i + 7], 10, 1126891415);
    c = md5ii(c, d, a, b, x[i + 14], 15, -1416354905);
    b = md5ii(b, c, d, a, x[i + 5], 21, -57434055);
    a = md5ii(a, b, c, d, x[i + 12], 6, 1700485571);
    d = md5ii(d, a, b, c, x[i + 3], 10, -1894986606);
    c = md5ii(c, d, a, b, x[i + 10], 15, -1051523);
    b = md5ii(b, c, d, a, x[i + 1], 21, -2054922799);
    a = md5ii(a, b, c, d, x[i + 8], 6, 1873313359);
    d = md5ii(d, a, b, c, x[i + 15], 10, -30611744);
    c = md5ii(c, d, a, b, x[i + 6], 15, -1560198380);
    b = md5ii(b, c, d, a, x[i + 13], 21, 1309151649);
    a = md5ii(a, b, c, d, x[i + 4], 6, -145523070);
    d = md5ii(d, a, b, c, x[i + 11], 10, -1120210379);
    c = md5ii(c, d, a, b, x[i + 2], 15, 718787259);
    b = md5ii(b, c, d, a, x[i + 9], 21, -343485551);
    a = safeAdd(a, olda);
    b = safeAdd(b, oldb);
    c = safeAdd(c, oldc);
    d = safeAdd(d, oldd);
  }

  return [a, b, c, d];
}
/*
 * Convert an array bytes to an array of little-endian words
 * Characters >255 have their high-byte silently ignored.
 */


function bytesToWords(input) {
  var i;
  var output = [];
  output[(input.length >> 2) - 1] = undefined;

  for (i = 0; i < output.length; i += 1) {
    output[i] = 0;
  }

  var length8 = input.length * 8;

  for (i = 0; i < length8; i += 8) {
    output[i >> 5] |= (input[i / 8] & 0xff) << i % 32;
  }

  return output;
}
/*
 * Add integers, wrapping at 2^32. This uses 16-bit operations internally
 * to work around bugs in some JS interpreters.
 */


function safeAdd(x, y) {
  var lsw = (x & 0xffff) + (y & 0xffff);
  var msw = (x >> 16) + (y >> 16) + (lsw >> 16);
  return msw << 16 | lsw & 0xffff;
}
/*
 * Bitwise rotate a 32-bit number to the left.
 */


function bitRotateLeft(num, cnt) {
  return num << cnt | num >>> 32 - cnt;
}
/*
 * These functions implement the four basic operations the algorithm uses.
 */


function md5cmn(q, a, b, x, s, t) {
  return safeAdd(bitRotateLeft(safeAdd(safeAdd(a, q), safeAdd(x, t)), s), b);
}

function md5ff(a, b, c, d, x, s, t) {
  return md5cmn(b & c | ~b & d, a, b, x, s, t);
}

function md5gg(a, b, c, d, x, s, t) {
  return md5cmn(b & d | c & ~d, a, b, x, s, t);
}

function md5hh(a, b, c, d, x, s, t) {
  return md5cmn(b ^ c ^ d, a, b, x, s, t);
}

function md5ii(a, b, c, d, x, s, t) {
  return md5cmn(c ^ (b | ~d), a, b, x, s, t);
}

/* harmony default export */ __webpack_exports__["default"] = (md5);

/***/ }),

/***/ "./node_modules/uuid/dist/esm-browser/rng.js":
/*!***************************************************!*\
  !*** ./node_modules/uuid/dist/esm-browser/rng.js ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return rng; });
// Unique ID creation requires a high quality random # generator. In the browser we therefore
// require the crypto API and do not support built-in fallback to lower quality random number
// generators (like Math.random()).
// getRandomValues needs to be invoked in a context where "this" is a Crypto implementation. Also,
// find the complete implementation of crypto (msCrypto) on IE11.
var getRandomValues = typeof crypto != 'undefined' && crypto.getRandomValues && crypto.getRandomValues.bind(crypto) || typeof msCrypto != 'undefined' && typeof msCrypto.getRandomValues == 'function' && msCrypto.getRandomValues.bind(msCrypto);
var rnds8 = new Uint8Array(16); // eslint-disable-line no-undef

function rng() {
  if (!getRandomValues) {
    throw new Error('crypto.getRandomValues() not supported. See https://github.com/uuidjs/uuid#getrandomvalues-not-supported');
  }

  return getRandomValues(rnds8);
}

/***/ }),

/***/ "./node_modules/uuid/dist/esm-browser/sha1.js":
/*!****************************************************!*\
  !*** ./node_modules/uuid/dist/esm-browser/sha1.js ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
// Adapted from Chris Veness' SHA1 code at
// http://www.movable-type.co.uk/scripts/sha1.html
function f(s, x, y, z) {
  switch (s) {
    case 0:
      return x & y ^ ~x & z;

    case 1:
      return x ^ y ^ z;

    case 2:
      return x & y ^ x & z ^ y & z;

    case 3:
      return x ^ y ^ z;
  }
}

function ROTL(x, n) {
  return x << n | x >>> 32 - n;
}

function sha1(bytes) {
  var K = [0x5a827999, 0x6ed9eba1, 0x8f1bbcdc, 0xca62c1d6];
  var H = [0x67452301, 0xefcdab89, 0x98badcfe, 0x10325476, 0xc3d2e1f0];

  if (typeof bytes == 'string') {
    var msg = unescape(encodeURIComponent(bytes)); // UTF8 escape

    bytes = new Array(msg.length);

    for (var i = 0; i < msg.length; i++) {
      bytes[i] = msg.charCodeAt(i);
    }
  }

  bytes.push(0x80);
  var l = bytes.length / 4 + 2;
  var N = Math.ceil(l / 16);
  var M = new Array(N);

  for (var i = 0; i < N; i++) {
    M[i] = new Array(16);

    for (var j = 0; j < 16; j++) {
      M[i][j] = bytes[i * 64 + j * 4] << 24 | bytes[i * 64 + j * 4 + 1] << 16 | bytes[i * 64 + j * 4 + 2] << 8 | bytes[i * 64 + j * 4 + 3];
    }
  }

  M[N - 1][14] = (bytes.length - 1) * 8 / Math.pow(2, 32);
  M[N - 1][14] = Math.floor(M[N - 1][14]);
  M[N - 1][15] = (bytes.length - 1) * 8 & 0xffffffff;

  for (var i = 0; i < N; i++) {
    var W = new Array(80);

    for (var t = 0; t < 16; t++) {
      W[t] = M[i][t];
    }

    for (var t = 16; t < 80; t++) {
      W[t] = ROTL(W[t - 3] ^ W[t - 8] ^ W[t - 14] ^ W[t - 16], 1);
    }

    var a = H[0];
    var b = H[1];
    var c = H[2];
    var d = H[3];
    var e = H[4];

    for (var t = 0; t < 80; t++) {
      var s = Math.floor(t / 20);
      var T = ROTL(a, 5) + f(s, b, c, d) + e + K[s] + W[t] >>> 0;
      e = d;
      d = c;
      c = ROTL(b, 30) >>> 0;
      b = a;
      a = T;
    }

    H[0] = H[0] + a >>> 0;
    H[1] = H[1] + b >>> 0;
    H[2] = H[2] + c >>> 0;
    H[3] = H[3] + d >>> 0;
    H[4] = H[4] + e >>> 0;
  }

  return [H[0] >> 24 & 0xff, H[0] >> 16 & 0xff, H[0] >> 8 & 0xff, H[0] & 0xff, H[1] >> 24 & 0xff, H[1] >> 16 & 0xff, H[1] >> 8 & 0xff, H[1] & 0xff, H[2] >> 24 & 0xff, H[2] >> 16 & 0xff, H[2] >> 8 & 0xff, H[2] & 0xff, H[3] >> 24 & 0xff, H[3] >> 16 & 0xff, H[3] >> 8 & 0xff, H[3] & 0xff, H[4] >> 24 & 0xff, H[4] >> 16 & 0xff, H[4] >> 8 & 0xff, H[4] & 0xff];
}

/* harmony default export */ __webpack_exports__["default"] = (sha1);

/***/ }),

/***/ "./node_modules/uuid/dist/esm-browser/v1.js":
/*!**************************************************!*\
  !*** ./node_modules/uuid/dist/esm-browser/v1.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _rng_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./rng.js */ "./node_modules/uuid/dist/esm-browser/rng.js");
/* harmony import */ var _bytesToUuid_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./bytesToUuid.js */ "./node_modules/uuid/dist/esm-browser/bytesToUuid.js");

 // **`v1()` - Generate time-based UUID**
//
// Inspired by https://github.com/LiosK/UUID.js
// and http://docs.python.org/library/uuid.html

var _nodeId;

var _clockseq; // Previous uuid creation time


var _lastMSecs = 0;
var _lastNSecs = 0; // See https://github.com/uuidjs/uuid for API details

function v1(options, buf, offset) {
  var i = buf && offset || 0;
  var b = buf || [];
  options = options || {};
  var node = options.node || _nodeId;
  var clockseq = options.clockseq !== undefined ? options.clockseq : _clockseq; // node and clockseq need to be initialized to random values if they're not
  // specified.  We do this lazily to minimize issues related to insufficient
  // system entropy.  See #189

  if (node == null || clockseq == null) {
    var seedBytes = options.random || (options.rng || _rng_js__WEBPACK_IMPORTED_MODULE_0__["default"])();

    if (node == null) {
      // Per 4.5, create and 48-bit node id, (47 random bits + multicast bit = 1)
      node = _nodeId = [seedBytes[0] | 0x01, seedBytes[1], seedBytes[2], seedBytes[3], seedBytes[4], seedBytes[5]];
    }

    if (clockseq == null) {
      // Per 4.2.2, randomize (14 bit) clockseq
      clockseq = _clockseq = (seedBytes[6] << 8 | seedBytes[7]) & 0x3fff;
    }
  } // UUID timestamps are 100 nano-second units since the Gregorian epoch,
  // (1582-10-15 00:00).  JSNumbers aren't precise enough for this, so
  // time is handled internally as 'msecs' (integer milliseconds) and 'nsecs'
  // (100-nanoseconds offset from msecs) since unix epoch, 1970-01-01 00:00.


  var msecs = options.msecs !== undefined ? options.msecs : new Date().getTime(); // Per 4.2.1.2, use count of uuid's generated during the current clock
  // cycle to simulate higher resolution clock

  var nsecs = options.nsecs !== undefined ? options.nsecs : _lastNSecs + 1; // Time since last uuid creation (in msecs)

  var dt = msecs - _lastMSecs + (nsecs - _lastNSecs) / 10000; // Per 4.2.1.2, Bump clockseq on clock regression

  if (dt < 0 && options.clockseq === undefined) {
    clockseq = clockseq + 1 & 0x3fff;
  } // Reset nsecs if clock regresses (new clockseq) or we've moved onto a new
  // time interval


  if ((dt < 0 || msecs > _lastMSecs) && options.nsecs === undefined) {
    nsecs = 0;
  } // Per 4.2.1.2 Throw error if too many uuids are requested


  if (nsecs >= 10000) {
    throw new Error("uuid.v1(): Can't create more than 10M uuids/sec");
  }

  _lastMSecs = msecs;
  _lastNSecs = nsecs;
  _clockseq = clockseq; // Per 4.1.4 - Convert from unix epoch to Gregorian epoch

  msecs += 12219292800000; // `time_low`

  var tl = ((msecs & 0xfffffff) * 10000 + nsecs) % 0x100000000;
  b[i++] = tl >>> 24 & 0xff;
  b[i++] = tl >>> 16 & 0xff;
  b[i++] = tl >>> 8 & 0xff;
  b[i++] = tl & 0xff; // `time_mid`

  var tmh = msecs / 0x100000000 * 10000 & 0xfffffff;
  b[i++] = tmh >>> 8 & 0xff;
  b[i++] = tmh & 0xff; // `time_high_and_version`

  b[i++] = tmh >>> 24 & 0xf | 0x10; // include version

  b[i++] = tmh >>> 16 & 0xff; // `clock_seq_hi_and_reserved` (Per 4.2.2 - include variant)

  b[i++] = clockseq >>> 8 | 0x80; // `clock_seq_low`

  b[i++] = clockseq & 0xff; // `node`

  for (var n = 0; n < 6; ++n) {
    b[i + n] = node[n];
  }

  return buf ? buf : Object(_bytesToUuid_js__WEBPACK_IMPORTED_MODULE_1__["default"])(b);
}

/* harmony default export */ __webpack_exports__["default"] = (v1);

/***/ }),

/***/ "./node_modules/uuid/dist/esm-browser/v3.js":
/*!**************************************************!*\
  !*** ./node_modules/uuid/dist/esm-browser/v3.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _v35_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./v35.js */ "./node_modules/uuid/dist/esm-browser/v35.js");
/* harmony import */ var _md5_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./md5.js */ "./node_modules/uuid/dist/esm-browser/md5.js");


var v3 = Object(_v35_js__WEBPACK_IMPORTED_MODULE_0__["default"])('v3', 0x30, _md5_js__WEBPACK_IMPORTED_MODULE_1__["default"]);
/* harmony default export */ __webpack_exports__["default"] = (v3);

/***/ }),

/***/ "./node_modules/uuid/dist/esm-browser/v35.js":
/*!***************************************************!*\
  !*** ./node_modules/uuid/dist/esm-browser/v35.js ***!
  \***************************************************/
/*! exports provided: DNS, URL, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DNS", function() { return DNS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "URL", function() { return URL; });
/* harmony import */ var _bytesToUuid_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./bytesToUuid.js */ "./node_modules/uuid/dist/esm-browser/bytesToUuid.js");


function uuidToBytes(uuid) {
  // Note: We assume we're being passed a valid uuid string
  var bytes = [];
  uuid.replace(/[a-fA-F0-9]{2}/g, function (hex) {
    bytes.push(parseInt(hex, 16));
  });
  return bytes;
}

function stringToBytes(str) {
  str = unescape(encodeURIComponent(str)); // UTF8 escape

  var bytes = new Array(str.length);

  for (var i = 0; i < str.length; i++) {
    bytes[i] = str.charCodeAt(i);
  }

  return bytes;
}

var DNS = '6ba7b810-9dad-11d1-80b4-00c04fd430c8';
var URL = '6ba7b811-9dad-11d1-80b4-00c04fd430c8';
/* harmony default export */ __webpack_exports__["default"] = (function (name, version, hashfunc) {
  var generateUUID = function generateUUID(value, namespace, buf, offset) {
    var off = buf && offset || 0;
    if (typeof value == 'string') value = stringToBytes(value);
    if (typeof namespace == 'string') namespace = uuidToBytes(namespace);
    if (!Array.isArray(value)) throw TypeError('value must be an array of bytes');
    if (!Array.isArray(namespace) || namespace.length !== 16) throw TypeError('namespace must be uuid string or an Array of 16 byte values'); // Per 4.3

    var bytes = hashfunc(namespace.concat(value));
    bytes[6] = bytes[6] & 0x0f | version;
    bytes[8] = bytes[8] & 0x3f | 0x80;

    if (buf) {
      for (var idx = 0; idx < 16; ++idx) {
        buf[off + idx] = bytes[idx];
      }
    }

    return buf || Object(_bytesToUuid_js__WEBPACK_IMPORTED_MODULE_0__["default"])(bytes);
  }; // Function#name is not settable on some platforms (#270)


  try {
    generateUUID.name = name;
  } catch (err) {} // For CommonJS default export support


  generateUUID.DNS = DNS;
  generateUUID.URL = URL;
  return generateUUID;
});

/***/ }),

/***/ "./node_modules/uuid/dist/esm-browser/v4.js":
/*!**************************************************!*\
  !*** ./node_modules/uuid/dist/esm-browser/v4.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _rng_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./rng.js */ "./node_modules/uuid/dist/esm-browser/rng.js");
/* harmony import */ var _bytesToUuid_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./bytesToUuid.js */ "./node_modules/uuid/dist/esm-browser/bytesToUuid.js");



function v4(options, buf, offset) {
  var i = buf && offset || 0;

  if (typeof options == 'string') {
    buf = options === 'binary' ? new Array(16) : null;
    options = null;
  }

  options = options || {};
  var rnds = options.random || (options.rng || _rng_js__WEBPACK_IMPORTED_MODULE_0__["default"])(); // Per 4.4, set bits for version and `clock_seq_hi_and_reserved`

  rnds[6] = rnds[6] & 0x0f | 0x40;
  rnds[8] = rnds[8] & 0x3f | 0x80; // Copy bytes to buffer, if provided

  if (buf) {
    for (var ii = 0; ii < 16; ++ii) {
      buf[i + ii] = rnds[ii];
    }
  }

  return buf || Object(_bytesToUuid_js__WEBPACK_IMPORTED_MODULE_1__["default"])(rnds);
}

/* harmony default export */ __webpack_exports__["default"] = (v4);

/***/ }),

/***/ "./node_modules/uuid/dist/esm-browser/v5.js":
/*!**************************************************!*\
  !*** ./node_modules/uuid/dist/esm-browser/v5.js ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _v35_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./v35.js */ "./node_modules/uuid/dist/esm-browser/v35.js");
/* harmony import */ var _sha1_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./sha1.js */ "./node_modules/uuid/dist/esm-browser/sha1.js");


var v5 = Object(_v35_js__WEBPACK_IMPORTED_MODULE_0__["default"])('v5', 0x50, _sha1_js__WEBPACK_IMPORTED_MODULE_1__["default"]);
/* harmony default export */ __webpack_exports__["default"] = (v5);

/***/ }),

/***/ "./source/bg/app.js":
/*!**************************!*\
  !*** ./source/bg/app.js ***!
  \**************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/defineProperty */ "./node_modules/@babel/runtime/helpers/defineProperty.js");
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/slicedToArray */ "./node_modules/@babel/runtime/helpers/slicedToArray.js");
/* harmony import */ var _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime/helpers/asyncToGenerator */ "./node_modules/@babel/runtime/helpers/asyncToGenerator.js");
/* harmony import */ var _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ "./node_modules/@babel/runtime/helpers/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @babel/runtime/helpers/createClass */ "./node_modules/@babel/runtime/helpers/createClass.js");
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _fetcher__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./fetcher */ "./source/bg/fetcher.js");
/* harmony import */ var _user__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./user */ "./source/bg/user.js");
/* harmony import */ var _notificator__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./notificator */ "./source/bg/notificator.js");
/* harmony import */ var _merchants__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./merchants */ "./source/bg/merchants.js");
/* harmony import */ var _cartController__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./cartController */ "./source/bg/cartController.js");
/* harmony import */ var _cashback__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./cashback */ "./source/bg/cashback.js");
/* harmony import */ var _validation_store__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./validation-store */ "./source/bg/validation-store.js");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../utils */ "./source/utils.js");
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../constants */ "./source/constants.js");
/* harmony import */ var _modules_sentry__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../modules/sentry */ "./source/modules/sentry.js");







function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }











Object(_modules_sentry__WEBPACK_IMPORTED_MODULE_15__["default"])();

var App = /*#__PURE__*/function () {
  function App() {
    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_4___default()(this, App);

    this.user = new _user__WEBPACK_IMPORTED_MODULE_7__["default"]({
      url: _constants__WEBPACK_IMPORTED_MODULE_14__["USER_URL"]
    });
    this.notificator = new _notificator__WEBPACK_IMPORTED_MODULE_8__["default"]();
    this.merchants = new _merchants__WEBPACK_IMPORTED_MODULE_9__["default"]({
      url: _constants__WEBPACK_IMPORTED_MODULE_14__["MERCHANTS_URL"]
    });
    this.cashback = new _cashback__WEBPACK_IMPORTED_MODULE_11__["default"]();
    this.validationStore = new _validation_store__WEBPACK_IMPORTED_MODULE_12__["default"]({
      getMerchantConfig: this.merchants.getMerchantConfig
    });
    this.cartController = new _cartController__WEBPACK_IMPORTED_MODULE_10__["default"]({
      getMerchantConfig: this.merchants.getMerchantConfig
    });
    this.serpConfigs = new _fetcher__WEBPACK_IMPORTED_MODULE_6__["default"]({
      url: _constants__WEBPACK_IMPORTED_MODULE_14__["SERP_CONFIGS_URL"],
      name: 'serpConfigs',
      options: {
        updateInterval: 5 * 60 * 60 * 1000 // 5 hrs

      }
    });
    this.setMessagesListener();
    this.setEventsListener();
  }

  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_5___default()(App, [{
    key: "setEventsListener",
    value: function setEventsListener() {
      var _this = this;

      this.merchants.on('update:state', function (_ref) {
        var mid = _ref.mid,
            state = _ref.state;

        _this.validationStore.update({
          mid: mid,
          result: {
            activated: state.activated
          }
        });
      });
      this.validationStore.on('update', /*#__PURE__*/function () {
        var _ref3 = _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_3___default()( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function _callee(_ref2) {
          var mid, params, invalidParams, tabs;
          return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function _callee$(_context) {
            while (1) {
              switch (_context.prev = _context.next) {
                case 0:
                  mid = _ref2.mid, params = _ref2.params;
                  invalidParams = [];
                  Object.entries(params).forEach(function (_ref4) {
                    var _ref5 = _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_2___default()(_ref4, 2),
                        name = _ref5[0],
                        isValid = _ref5[1];

                    if (!isValid) {
                      invalidParams.push({
                        section: 'invalidParams',
                        parameter: name
                      });
                    }
                  });

                  if (!(invalidParams.length > 0)) {
                    _context.next = 8;
                    break;
                  }

                  _context.next = 6;
                  return _this.merchants.getMerchantTabs(mid);

                case 6:
                  tabs = _context.sent;
                  tabs.forEach(function (tab) {
                    var isSpecialPage = _this.merchants.isSpecialPage({
                      id: mid,
                      url: tab.url
                    });

                    if (isSpecialPage !== 'checkout') {
                      return;
                    }

                    _this.notificator.sendNotification({
                      forcedTabId: tab.id,
                      to: 'content',
                      params: invalidParams
                    });
                  });

                case 8:
                case "end":
                  return _context.stop();
              }
            }
          }, _callee);
        }));

        return function (_x) {
          return _ref3.apply(this, arguments);
        };
      }());
    }
  }, {
    key: "getSERPConfig",
    value: function getSERPConfig() {
      return this.serpConfigs.data;
    }
  }, {
    key: "getContentData",
    value: function getContentData(_ref6) {
      var sender = _ref6.sender,
          href = _ref6.href;
      var merchant = this.merchants.getMerchantWithState(href || sender.url);
      return _objectSpread(_objectSpread({}, merchant), {}, {
        user: this.user.data
      });
    }
  }, {
    key: "getUserInfo",
    value: function getUserInfo() {
      return this.user.data;
    }
  }, {
    key: "checkAttribution",
    value: function checkAttribution(_ref7) {
      var url = _ref7.url,
          tabId = _ref7.tabId;
      // Check if activation/suppression proccess goes in tab
      var processName = this.cashback.checkProcess({
        url: url,
        tabId: tabId
      });

      if (processName === 'attribution') {
        var _this$merchants$getMe = this.merchants.getMerchantWithState(url),
            merchantData = _this$merchants$getMe.merchantData,
            merchantState = _this$merchants$getMe.merchantState; // If user on the merchant page then
        // 3rd-party link was activated in this tab before so merchant should be disabled


        if (merchantData && !(merchantState === null || merchantState === void 0 ? void 0 : merchantState.activation)) {
          this.merchants.setState({
            id: merchantData.id,
            data: this.merchants.getDefaultState('suppressed')
          });
        }

        this.cashback.clearProcess(processName, tabId);
      }
    } // Hardcoded to save currys token

  }, {
    key: "saveCurrysBid",
    value: function saveCurrysBid(_ref8) {
      var bid = _ref8.bid;
      this.cartController.currysBid = bid;
    }
  }, {
    key: "setMessagesListener",
    value: function setMessagesListener() {
      var _this2 = this;

      chrome.runtime.onMessage.addListener(function (message, sender, sendResponse) {
        var module = message.module,
            action = message.action,
            data = message.data;
        var context = _this2[module] || _this2;

        if (typeof context[action] === 'function') {
          Object(_utils__WEBPACK_IMPORTED_MODULE_13__["asyncWrapper"])(context[action].bind(context), _objectSpread(_objectSpread({}, data), {}, {
            sender: sender
          })).then(sendResponse);
          return true;
        }

        return false;
      });
      chrome.cookies.onChanged.addListener(function (_ref9) {
        var cookie = _ref9.cookie;
        return _this2.merchants.checkActivationStatus(cookie);
      });
      chrome.webRequest.onBeforeRequest.addListener(function (details) {
        _this2.checkAttribution(details);
      }, {
        urls: ['<all_urls>'],
        types: ['main_frame']
      });
      chrome.tabs.onUpdated.addListener(function (tabId, changeInfo, _ref10) {
        var url = _ref10.url;

        _this2.checkAttribution({
          url: url,
          tabId: tabId
        });
      });
    }
  }, {
    key: "injectFrameApp",
    value: function () {
      var _injectFrameApp = _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_3___default()( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function _callee3(_ref11) {
        var frameUrl, sender, tab, frames, appFrames;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                frameUrl = _ref11.frameUrl, sender = _ref11.sender;
                tab = sender.tab;
                _context3.next = 4;
                return browser.webNavigation.getAllFrames({
                  tabId: tab.id
                });

              case 4:
                frames = _context3.sent;
                appFrames = frames.filter(function (_ref12) {
                  var url = _ref12.url;
                  return url.includes(frameUrl);
                });
                appFrames.map( /*#__PURE__*/function () {
                  var _ref13 = _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_3___default()( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function _callee2(frame) {
                    var frameId;
                    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function _callee2$(_context2) {
                      while (1) {
                        switch (_context2.prev = _context2.next) {
                          case 0:
                            frameId = frame.frameId;
                            _context2.prev = 1;
                            _context2.next = 4;
                            return browser.tabs.sendMessage(tab.id, {
                              action: 'ping'
                            }, {
                              frameId: frameId
                            });

                          case 4:
                            _context2.next = 14;
                            break;

                          case 6:
                            _context2.prev = 6;
                            _context2.t0 = _context2["catch"](1);
                            _context2.next = 10;
                            return browser.tabs.executeScript(tab.id, {
                              file: 'browser-polyfill.js',
                              frameId: frameId
                            });

                          case 10:
                            _context2.next = 12;
                            return browser.tabs.executeScript(tab.id, {
                              file: 'vendors/bundle.js',
                              frameId: frameId
                            });

                          case 12:
                            _context2.next = 14;
                            return browser.tabs.executeScript(tab.id, {
                              file: 'frame/bundle.js',
                              frameId: frameId
                            });

                          case 14:
                            _context2.next = 16;
                            return browser.tabs.sendMessage(tab.id, {
                              action: 'process',
                              data: {
                                url: tab.url
                              }
                            }, {
                              frameId: frameId
                            });

                          case 16:
                            return _context2.abrupt("return", true);

                          case 17:
                          case "end":
                            return _context2.stop();
                        }
                      }
                    }, _callee2, null, [[1, 6]]);
                  }));

                  return function (_x3) {
                    return _ref13.apply(this, arguments);
                  };
                }());

              case 7:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }));

      function injectFrameApp(_x2) {
        return _injectFrameApp.apply(this, arguments);
      }

      return injectFrameApp;
    }()
  }]);

  return App;
}();

window.app = new App();

/***/ }),

/***/ "./source/bg/cartController.js":
/*!*************************************!*\
  !*** ./source/bg/cartController.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return CartController; });
/* harmony import */ var _babel_runtime_helpers_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/objectWithoutProperties */ "./node_modules/@babel/runtime/helpers/objectWithoutProperties.js");
/* harmony import */ var _babel_runtime_helpers_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/defineProperty */ "./node_modules/@babel/runtime/helpers/defineProperty.js");
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime/helpers/asyncToGenerator */ "./node_modules/@babel/runtime/helpers/asyncToGenerator.js");
/* harmony import */ var _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ "./node_modules/@babel/runtime/helpers/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @babel/runtime/helpers/createClass */ "./node_modules/@babel/runtime/helpers/createClass.js");
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var uuid__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! uuid */ "./node_modules/uuid/dist/esm-browser/index.js");
/* harmony import */ var _modules_parser_index__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../modules/parser/index */ "./source/modules/parser/index.js");
/* harmony import */ var _modules_api__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../modules/api */ "./source/modules/api.js");







function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_1___default()(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }





var CartController = /*#__PURE__*/function () {
  function CartController(_ref) {
    var getMerchantConfig = _ref.getMerchantConfig;

    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_4___default()(this, CartController);

    this.getMerchantConfig = getMerchantConfig;
    this.parsedCarts = {};
  }

  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_5___default()(CartController, [{
    key: "process",
    value: function () {
      var _process = _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_3___default()( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_2___default.a.mark(function _callee(_ref2) {
        var mid, handledData, items, total, cartId, discountedData;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_2___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                mid = _ref2.mid;
                _context.next = 3;
                return this.parseCart({
                  mid: mid
                });

              case 3:
                if (!this.parsedCarts[mid]) {
                  _context.next = 10;
                  break;
                }

                handledData = this.handleParsedData(this.parsedCarts[mid]);
                items = handledData.items, total = handledData.total, cartId = handledData.cartId;
                _context.next = 8;
                return _modules_api__WEBPACK_IMPORTED_MODULE_8__["default"].getDiscountByCartId(cartId, {
                  items: items,
                  total: total,
                  merchant_id: mid
                });

              case 8:
                discountedData = _context.sent;
                return _context.abrupt("return", this.handleDiscountedData(discountedData, this.parsedCarts[mid]));

              case 10:
                return _context.abrupt("return", null);

              case 11:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function process(_x) {
        return _process.apply(this, arguments);
      }

      return process;
    }()
  }, {
    key: "processCarrysConfig",
    value: function processCarrysConfig(config) {
      return _objectSpread(_objectSpread({}, config), {}, {
        url: "".concat(config.url).concat(this.currysBid)
      });
    }
  }, {
    key: "parseCart",
    value: function () {
      var _parseCart = _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_3___default()( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_2___default.a.mark(function _callee2(_ref3) {
        var mid, config, parser, parsedData;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_2___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                mid = _ref3.mid;
                _context2.next = 3;
                return this.getMerchantConfig({
                  id: mid,
                  moduleName: 'cartParser'
                });

              case 3:
                config = _context2.sent;

                // Hardcoded to use currys token in request
                if (config && mid === 'f1a7e030-65a6-4263-a7fd-5600dbf2dcaf') {
                  config = this.processCarrysConfig(config);
                }

                parser = new _modules_parser_index__WEBPACK_IMPORTED_MODULE_7__["default"](config);
                _context2.next = 8;
                return parser.process();

              case 8:
                parsedData = _context2.sent;
                return _context2.abrupt("return", this.saveCartData({
                  mid: mid,
                  parsedData: parsedData
                }));

              case 10:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      function parseCart(_x2) {
        return _parseCart.apply(this, arguments);
      }

      return parseCart;
    }()
  }, {
    key: "saveCartData",
    value: function saveCartData(_ref4) {
      var mid = _ref4.mid,
          parsedData = _ref4.parsedData;
      var products = parsedData.products;

      if (products.length) {
        var _this$parsedCarts, _this$parsedCarts$mid;

        var cartId = ((_this$parsedCarts = this.parsedCarts) === null || _this$parsedCarts === void 0 ? void 0 : (_this$parsedCarts$mid = _this$parsedCarts[mid]) === null || _this$parsedCarts$mid === void 0 ? void 0 : _this$parsedCarts$mid.cartId) || Object(uuid__WEBPACK_IMPORTED_MODULE_6__["v4"])();
        this.parsedCarts[mid] = _objectSpread({
          cartId: cartId
        }, parsedData);
        return parsedData;
      }

      delete this.parsedCarts[mid];
      return null;
    }
  }, {
    key: "handleParsedData",
    value: function handleParsedData(data) {
      var products = data.products,
          total = data.total,
          totalCurrency = data.totalCurrency,
          cartId = data.cartId; // eslint-disable-line

      var items = products.map(function (product) {
        var price = product.price,
            currency = product.currency,
            brand = product.brand,
            image = product.image,
            colour = product.colour; // eslint-disable-line

        return _objectSpread(_objectSpread({}, product), {}, {
          price: {
            value: price,
            currency: currency.code
          },
          meta: {
            brand: brand,
            image: image,
            colour: colour
          }
        });
      });
      return {
        items: items,
        total: {
          value: total,
          currency: totalCurrency.code
        },
        cartId: cartId
      };
    }
  }, {
    key: "handleDiscountedData",
    value: function handleDiscountedData(data, _ref5) {
      var _ref5$products = _ref5.products,
          products = _ref5$products === void 0 ? [] : _ref5$products;
      var items = data.items,
          discoTotal = data.discounted_total;
      var discoProducts = items.map(function (item) {
        var price = item.price,
            discoPrice = item.discounted_price,
            rest = _babel_runtime_helpers_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_0___default()(item, ["price", "discounted_price"]); // eslint-disable-line


        var _products$find = products.find(function (_ref6) {
          var sku = _ref6.sku;
          return sku === rest.sku;
        }),
            name = _products$find.name,
            image = _products$find.image,
            url = _products$find.url;

        return _objectSpread(_objectSpread({
          discoPrice: discoPrice,
          price: price.value
        }, rest), {}, {
          name: name,
          image: image,
          url: url
        }); // eslint-disable-line
      });
      return {
        discoProducts: discoProducts,
        discoTotal: discoTotal
      };
    }
  }]);

  return CartController;
}();



/***/ }),

/***/ "./source/bg/cashback.js":
/*!*******************************!*\
  !*** ./source/bg/cashback.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Cashback; });
/* harmony import */ var _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/slicedToArray */ "./node_modules/@babel/runtime/helpers/slicedToArray.js");
/* harmony import */ var _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ "./node_modules/@babel/runtime/helpers/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/createClass */ "./node_modules/@babel/runtime/helpers/createClass.js");
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../constants */ "./source/constants.js");



 // This class manages processes like activation/suppression started in tab

var Cashback = /*#__PURE__*/function () {
  function Cashback() {
    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1___default()(this, Cashback);

    this.patterns = {
      attribution: _constants__WEBPACK_IMPORTED_MODULE_3__["SUPPRESSION_PATTERNS"]
    };
    this.processes = {
      attribution: {}
    };
  }

  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2___default()(Cashback, [{
    key: "checkProcess",
    value: function checkProcess(_ref) {
      var _this = this;

      var url = _ref.url,
          tabId = _ref.tabId;
      // Check if user goes through affiliate link/3rd party link
      var processName = Object.keys(this.patterns).find(function (name) {
        return _this.patterns[name].some(function (pattern) {
          return url.search(pattern) > -1;
        });
      });

      if (processName) {
        // Clear previous state in tab
        this.clearProcess(processName, tabId); // Mark that some of process started in tab with tabId
        // Timer will refresh state after 30 seconds

        var timer = setTimeout(function () {
          return _this.clearProcess(processName, tabId);
        }, 30000);
        this.processes[processName][tabId] = {
          timer: timer
        };
      }

      return this.getProcess(tabId);
    }
  }, {
    key: "getProcess",
    value: function getProcess(tabId) {
      var _ref2 = Object.entries(this.processes).find(function (_ref4) {
        var _ref5 = _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0___default()(_ref4, 2),
            processes = _ref5[1];

        return processes[tabId];
      }) || [],
          _ref3 = _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0___default()(_ref2, 1),
          processName = _ref3[0];

      return processName;
    }
  }, {
    key: "clearProcess",
    value: function clearProcess(processName, tabId) {
      var _this$processes$proce;

      clearTimeout((_this$processes$proce = this.processes[processName][tabId]) === null || _this$processes$proce === void 0 ? void 0 : _this$processes$proce.timer);
      delete this.processes[processName][tabId];
    }
  }]);

  return Cashback;
}();



/***/ }),

/***/ "./source/bg/cookieChecker.js":
/*!************************************!*\
  !*** ./source/bg/cookieChecker.js ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return CookieChecker; });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/asyncToGenerator */ "./node_modules/@babel/runtime/helpers/asyncToGenerator.js");
/* harmony import */ var _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ "./node_modules/@babel/runtime/helpers/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime/helpers/createClass */ "./node_modules/@babel/runtime/helpers/createClass.js");
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_3__);





var CookieChecker = /*#__PURE__*/function () {
  function CookieChecker() {
    var config = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];

    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_2___default()(this, CookieChecker);

    this.config = config;
  }

  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_3___default()(CookieChecker, [{
    key: "check",
    value: function () {
      var _check = _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1___default()( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var checks;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.next = 2;
                return Promise.all(this.config.map( /*#__PURE__*/function () {
                  var _ref2 = _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1___default()( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(_ref) {
                    var domain, _ref$cookies, cookies, browserCookies;

                    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
                      while (1) {
                        switch (_context.prev = _context.next) {
                          case 0:
                            domain = _ref.domain, _ref$cookies = _ref.cookies, cookies = _ref$cookies === void 0 ? [] : _ref$cookies;
                            _context.next = 3;
                            return browser.cookies.getAll({
                              domain: domain
                            });

                          case 3:
                            browserCookies = _context.sent;
                            return _context.abrupt("return", cookies.every(function (_ref3) {
                              var namePattern = _ref3.namePattern,
                                  valuePattern = _ref3.valuePattern;
                              var isCookiesValid = browserCookies.some(function (_ref4) {
                                var name = _ref4.name,
                                    value = _ref4.value;
                                var isCookieValid = name.search(namePattern) > -1 && value.search(valuePattern) > -1;
                                return isCookieValid;
                              });
                              return isCookiesValid;
                            }));

                          case 5:
                          case "end":
                            return _context.stop();
                        }
                      }
                    }, _callee);
                  }));

                  return function (_x) {
                    return _ref2.apply(this, arguments);
                  };
                }()));

              case 2:
                checks = _context2.sent;
                return _context2.abrupt("return", checks.every(function (value) {
                  return value === true;
                }));

              case 4:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      function check() {
        return _check.apply(this, arguments);
      }

      return check;
    }()
  }]);

  return CookieChecker;
}();



/***/ }),

/***/ "./source/bg/fetcher.js":
/*!******************************!*\
  !*** ./source/bg/fetcher.js ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Fetcher; });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/asyncToGenerator */ "./node_modules/@babel/runtime/helpers/asyncToGenerator.js");
/* harmony import */ var _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/defineProperty */ "./node_modules/@babel/runtime/helpers/defineProperty.js");
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ "./node_modules/@babel/runtime/helpers/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime/helpers/createClass */ "./node_modules/@babel/runtime/helpers/createClass.js");
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @babel/runtime/helpers/inherits */ "./node_modules/@babel/runtime/helpers/inherits.js");
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @babel/runtime/helpers/possibleConstructorReturn */ "./node_modules/@babel/runtime/helpers/possibleConstructorReturn.js");
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @babel/runtime/helpers/getPrototypeOf */ "./node_modules/@babel/runtime/helpers/getPrototypeOf.js");
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var events__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! events */ "./node_modules/events/events.js");
/* harmony import */ var events__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(events__WEBPACK_IMPORTED_MODULE_8__);









function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_2___default()(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_7___default()(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_7___default()(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_6___default()(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }



var Fetcher = /*#__PURE__*/function (_EventEmitter) {
  _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_5___default()(Fetcher, _EventEmitter);

  var _super = _createSuper(Fetcher);

  function Fetcher(_ref) {
    var _this;

    var name = _ref.name,
        url = _ref.url,
        _ref$options = _ref.options,
        options = _ref$options === void 0 ? {} : _ref$options;

    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_3___default()(this, Fetcher);

    _this = _super.call(this);
    _this.name = name;
    _this.url = url;
    _this.options = _objectSpread({
      updateInterval: 5 * 60 * 1000,
      // 5 min
      updateErrorInterval: 30 * 1000
    }, options);
    _this.updateTime = 0;
    _this.updateTimer = null;
    _this.updateController = null;

    _this.init();

    return _this;
  }

  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_4___default()(Fetcher, [{
    key: "init",
    value: function () {
      var _init = _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1___default()( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return this.load();

              case 2:
                _context.next = 4;
                return this.update();

              case 4:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function init() {
        return _init.apply(this, arguments);
      }

      return init;
    }()
  }, {
    key: "load",
    value: function () {
      var _load = _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1___default()( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var cachedData;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.next = 2;
                return browser.storage.local.get(["".concat(this.name, ".data"), "".concat(this.name, ".updateTime")]);

              case 2:
                cachedData = _context2.sent;
                this.data = cachedData["".concat(this.name, ".data")] || this.data;
                this.updateTime = cachedData["".concat(this.name, ".updateTime")] || 0;

              case 5:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      function load() {
        return _load.apply(this, arguments);
      }

      return load;
    }()
  }, {
    key: "save",
    value: function () {
      var _save = _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1___default()( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3() {
        var _browser$storage$loca;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.next = 2;
                return browser.storage.local.set((_browser$storage$loca = {}, _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_2___default()(_browser$storage$loca, "".concat(this.name, ".data"), this.data), _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_2___default()(_browser$storage$loca, "".concat(this.name, ".updateTime"), this.updateTime), _browser$storage$loca));

              case 2:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this);
      }));

      function save() {
        return _save.apply(this, arguments);
      }

      return save;
    }()
  }, {
    key: "parse",
    value: function parse(data) {
      return data;
    }
  }, {
    key: "request",
    value: function () {
      var _request = _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1___default()( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee4() {
        var url,
            options,
            signal,
            response,
            _args4 = arguments;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                url = _args4.length > 0 && _args4[0] !== undefined ? _args4[0] : this.url;
                options = _args4.length > 1 && _args4[1] !== undefined ? _args4[1] : {};

                if (this.updateController) {
                  this.updateController.abort();
                }

                this.updateController = new AbortController();
                signal = this.updateController.signal;
                _context4.next = 7;
                return fetch(url, _objectSpread({
                  signal: signal
                }, options));

              case 7:
                response = _context4.sent;
                return _context4.abrupt("return", response.json());

              case 9:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, this);
      }));

      function request() {
        return _request.apply(this, arguments);
      }

      return request;
    }()
  }, {
    key: "update",
    value: function () {
      var _update = _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1___default()( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee5(force) {
        var _this$options, updateInterval, updateErrorInterval, data;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _this$options = this.options, updateInterval = _this$options.updateInterval, updateErrorInterval = _this$options.updateErrorInterval;

                if (!(force || this.updateTime + updateInterval < Date.now())) {
                  _context5.next = 16;
                  break;
                }

                _context5.prev = 2;
                _context5.next = 5;
                return this.request();

              case 5:
                data = _context5.sent;
                this.data = this.parse(data);
                _context5.next = 9;
                return this.save();

              case 9:
                this.refreshUpdateTimer({});
                return _context5.abrupt("return", this.data);

              case 13:
                _context5.prev = 13;
                _context5.t0 = _context5["catch"](2);
                return _context5.abrupt("return", this.refreshUpdateTimer({
                  interval: updateErrorInterval,
                  force: true
                }));

              case 16:
                this.refreshUpdateTimer({
                  time: this.updateTime
                });
                return _context5.abrupt("return", null);

              case 18:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, this, [[2, 13]]);
      }));

      function update(_x) {
        return _update.apply(this, arguments);
      }

      return update;
    }()
  }, {
    key: "refreshUpdateTimer",
    value: function refreshUpdateTimer(_ref2) {
      var _this2 = this;

      var _ref2$time = _ref2.time,
          time = _ref2$time === void 0 ? Date.now() : _ref2$time,
          _ref2$interval = _ref2.interval,
          interval = _ref2$interval === void 0 ? this.options.updateInterval : _ref2$interval,
          force = _ref2.force;
      clearInterval(this.updateTimer);
      this.updateTime = time;
      this.updateTimer = setInterval(function () {
        clearInterval(_this2.updateTimer);

        _this2.update(force);
      }, time + interval - Date.now());
    }
  }]);

  return Fetcher;
}(events__WEBPACK_IMPORTED_MODULE_8___default.a);



/***/ }),

/***/ "./source/bg/merchants.js":
/*!********************************!*\
  !*** ./source/bg/merchants.js ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Merchants; });
/* harmony import */ var _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/slicedToArray */ "./node_modules/@babel/runtime/helpers/slicedToArray.js");
/* harmony import */ var _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/asyncToGenerator */ "./node_modules/@babel/runtime/helpers/asyncToGenerator.js");
/* harmony import */ var _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ "./node_modules/@babel/runtime/helpers/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime/helpers/createClass */ "./node_modules/@babel/runtime/helpers/createClass.js");
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @babel/runtime/helpers/assertThisInitialized */ "./node_modules/@babel/runtime/helpers/assertThisInitialized.js");
/* harmony import */ var _babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _babel_runtime_helpers_get__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @babel/runtime/helpers/get */ "./node_modules/@babel/runtime/helpers/get.js");
/* harmony import */ var _babel_runtime_helpers_get__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_get__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @babel/runtime/helpers/inherits */ "./node_modules/@babel/runtime/helpers/inherits.js");
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @babel/runtime/helpers/possibleConstructorReturn */ "./node_modules/@babel/runtime/helpers/possibleConstructorReturn.js");
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @babel/runtime/helpers/getPrototypeOf */ "./node_modules/@babel/runtime/helpers/getPrototypeOf.js");
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @babel/runtime/helpers/defineProperty */ "./node_modules/@babel/runtime/helpers/defineProperty.js");
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var _fetcher__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./fetcher */ "./source/bg/fetcher.js");
/* harmony import */ var _cookieChecker__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./cookieChecker */ "./source/bg/cookieChecker.js");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../utils */ "./source/utils.js");
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../constants */ "./source/constants.js");












function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_10___default()(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_9___default()(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_9___default()(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_8___default()(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }






var Merchants = /*#__PURE__*/function (_Fetcher) {
  _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_7___default()(Merchants, _Fetcher);

  var _super = _createSuper(Merchants);

  function Merchants(_ref) {
    var _this;

    var _ref$name = _ref.name,
        name = _ref$name === void 0 ? 'merchants' : _ref$name,
        _url = _ref.url,
        _ref$options = _ref.options,
        options = _ref$options === void 0 ? {} : _ref$options;

    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_3___default()(this, Merchants);

    _this = _super.call(this, {
      name: name,
      url: _url,
      options: options
    });

    _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_10___default()(_babel_runtime_helpers_assertThisInitialized__WEBPACK_IMPORTED_MODULE_5___default()(_this), "getMerchantConfig", function (_ref2) {
      var id = _ref2.id,
          url = _ref2.url,
          moduleName = _ref2.moduleName;
      var mid = id;

      if (!id && !url) {
        return null;
      }

      if (!mid && url) {
        var _URL = new URL(url),
            host = _URL.host;

        var merchant = _this.data.find(function (_ref3) {
          var domain = _ref3.domain;
          return host.includes(domain);
        });

        mid = merchant === null || merchant === void 0 ? void 0 : merchant.id;
      }

      if (mid) {
        var _merchantConfig$confi;

        var merchantConfig = _this.configs.data.find(function (config) {
          return mid === config.mid;
        });

        return moduleName ? merchantConfig === null || merchantConfig === void 0 ? void 0 : (_merchantConfig$confi = merchantConfig.configs) === null || _merchantConfig$confi === void 0 ? void 0 : _merchantConfig$confi[moduleName] : merchantConfig;
      }

      return null;
    });

    _this.data = [];
    _this.states = {};
    _this.configs = new _fetcher__WEBPACK_IMPORTED_MODULE_11__["default"]({
      url: _constants__WEBPACK_IMPORTED_MODULE_14__["MERCHANTS_CONFIGS_URL"],
      name: 'merchantsConfigs',
      options: {
        updateInterval: 5 * 60 * 60 * 1000 // 5 hrs

      }
    });

    _this.configs.update(true);

    return _this;
  }

  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_4___default()(Merchants, [{
    key: "parse",
    value: function parse() {
      var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      return data.merchants;
    }
  }, {
    key: "update",
    value: function () {
      var _update = _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2___default()( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function _callee(force) {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return _babel_runtime_helpers_get__WEBPACK_IMPORTED_MODULE_6___default()(_babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_9___default()(Merchants.prototype), "update", this).call(this, force);

              case 2:
                this.updateStates();
                return _context.abrupt("return", this.data);

              case 4:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function update(_x) {
        return _update.apply(this, arguments);
      }

      return update;
    }()
  }, {
    key: "updateStates",
    value: function updateStates(refresh) {
      var _this2 = this;

      this.data.forEach(function (_ref4) {
        var id = _ref4.id;

        var defaultState = _this2.getDefaultState();

        _this2.states[id] = refresh ? defaultState : _this2.states[id] || defaultState;
      });
    }
  }, {
    key: "getDefaultState",
    value: function getDefaultState() {
      var name = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'default';
      var states = {
        "default": {
          activated: false,
          showNotification: true
        },
        activated: {
          activated: true,
          activation: false,
          suppressed: false,
          showNotification: true
        },
        suppressed: {
          activated: false,
          suppressed: true,
          showNotification: false
        }
      };
      return states[name];
    }
  }, {
    key: "setState",
    value: function setState(_ref5) {
      var id = _ref5.id,
          url = _ref5.url,
          data = _ref5.data;

      try {
        var mid = id || this.getByUrl(url).id;
        this.states[mid] = _objectSpread(_objectSpread({}, this.states[mid] || {}), data);
        this.updateContentData({
          id: mid,
          data: {
            merchantState: this.states[mid]
          }
        });
        this.emit('update:state', {
          mid: mid,
          state: _objectSpread({}, this.states[mid])
        });
        return this.states[mid];
      } catch (e) {
        return null;
      }
    }
  }, {
    key: "updateContentData",
    value: function () {
      var _updateContentData = _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2___default()( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function _callee3(_ref6) {
        var id, data, tabs;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                id = _ref6.id, data = _ref6.data;
                _context3.next = 3;
                return this.getMerchantTabs(id);

              case 3:
                tabs = _context3.sent;
                return _context3.abrupt("return", Promise.all(tabs.map( /*#__PURE__*/function () {
                  var _ref7 = _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2___default()( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function _callee2(tab) {
                    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function _callee2$(_context2) {
                      while (1) {
                        switch (_context2.prev = _context2.next) {
                          case 0:
                            return _context2.abrupt("return", browser.tabs.sendMessage(tab.id, {
                              action: 'update',
                              data: data
                            }));

                          case 1:
                          case "end":
                            return _context2.stop();
                        }
                      }
                    }, _callee2);
                  }));

                  return function (_x3) {
                    return _ref7.apply(this, arguments);
                  };
                }())));

              case 5:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this);
      }));

      function updateContentData(_x2) {
        return _updateContentData.apply(this, arguments);
      }

      return updateContentData;
    }()
  }, {
    key: "getMerchantTabs",
    value: function () {
      var _getMerchantTabs = _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2___default()( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function _callee4(id) {
        var _this3 = this;

        var tabs;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.next = 2;
                return browser.tabs.query({});

              case 2:
                tabs = _context4.sent;
                return _context4.abrupt("return", tabs.filter(function (_ref8) {
                  var url = _ref8.url;
                  return (_this3.getByUrl(url) || {}).id === id;
                }));

              case 4:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4);
      }));

      function getMerchantTabs(_x4) {
        return _getMerchantTabs.apply(this, arguments);
      }

      return getMerchantTabs;
    }()
  }, {
    key: "isSpecialPage",
    value: function isSpecialPage(_ref9) {
      var id = _ref9.id,
          url = _ref9.url;
      var config = this.getMerchantConfig({
        id: id
      });

      if (config) {
        var _ref10 = Object.entries(config.pages || {}).find(function (_ref12) {
          var _ref13 = _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0___default()(_ref12, 2),
              pattern = _ref13[1];

          return url.search(pattern) > -1;
        }) || [],
            _ref11 = _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0___default()(_ref10, 1),
            pageName = _ref11[0];

        return pageName;
      }

      return null;
    }
  }, {
    key: "get",
    value: function get(url) {
      var merchant = this.getByUrl(url);
      return !merchant ? this.getBySpecialPage(url) : merchant;
    }
  }, {
    key: "getById",
    value: function getById(id) {
      return this.data.find(function (_ref14) {
        var merchantId = _ref14.id;
        return id === merchantId;
      });
    }
  }, {
    key: "getBySpecialPage",
    value: function getBySpecialPage(url) {
      try {
        var config = this.configs.data.find(function (_ref15) {
          var _ref15$pages = _ref15.pages,
              pages = _ref15$pages === void 0 ? {} : _ref15$pages;
          return Object.values(pages).some(function (pattern) {
            return url.search(pattern) > -1;
          });
        });
        return (config === null || config === void 0 ? void 0 : config.mid) && this.getById(config === null || config === void 0 ? void 0 : config.mid);
      } catch (e) {
        return null;
      }
    }
  }, {
    key: "getByUrl",
    value: function getByUrl(url) {
      try {
        var _URL2 = new URL(url),
            host = _URL2.host;

        return this.data.find(function (_ref16) {
          var domain = _ref16.domain,
              matchPattern = _ref16.matchPattern;
          return host.includes(domain) && url.search(matchPattern) > -1;
        });
      } catch (e) {
        return null;
      }
    }
  }, {
    key: "getMerchantInfo",
    value: function getMerchantInfo(_ref17) {
      var id = _ref17.id,
          url = _ref17.url;
      var merchantData = null;
      var merchantState = null;
      var merchantConfig = null;

      if (id) {
        merchantData = this.getById(id);
      }

      if (!merchantData && url) {
        merchantData = this.getByUrl(url);
        merchantData = merchantData || this.getBySpecialPage(url);
      }

      if (merchantData) {
        merchantConfig = this.configs.data.find(function (config) {
          return merchantData.id === config.mid;
        });
        merchantState = this.states[merchantData.id];
        return _objectSpread(_objectSpread(_objectSpread({}, merchantData), merchantState), {}, {
          config: merchantConfig
        });
      }

      return null;
    }
  }, {
    key: "getMerchantWithState",
    value: function getMerchantWithState(url) {
      var merchantData = this.get(url);
      var merchantState = this.states[merchantData === null || merchantData === void 0 ? void 0 : merchantData.id];
      return {
        merchantData: merchantData,
        merchantState: merchantState
      };
    }
  }, {
    key: "activationTimeout",
    value: function () {
      var _activationTimeout = _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2___default()( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function _callee5(_ref18) {
        var id, time, _ref18$activationProg, activationProgress;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                id = _ref18.id, time = _ref18.time, _ref18$activationProg = _ref18.activationProgress, activationProgress = _ref18$activationProg === void 0 ? 0 : _ref18$activationProg;

                if (!(activationProgress < 100 && !this.states[id].activated)) {
                  _context5.next = 6;
                  break;
                }

                this.setState({
                  id: id,
                  data: {
                    activationProgress: activationProgress
                  }
                });
                _context5.next = 5;
                return Object(_utils__WEBPACK_IMPORTED_MODULE_13__["asyncTimeout"])(time / 20);

              case 5:
                return _context5.abrupt("return", this.activationTimeout({
                  id: id,
                  time: time,
                  activationProgress: activationProgress + 5
                }));

              case 6:
                return _context5.abrupt("return", true);

              case 7:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, this);
      }));

      function activationTimeout(_x5) {
        return _activationTimeout.apply(this, arguments);
      }

      return activationTimeout;
    }()
  }, {
    key: "activate",
    value: function () {
      var _activate = _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2___default()( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function _callee6(_ref19) {
        var id, _this$data$find, url, _yield$browser$tabs$c, tabId, activationTab;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                id = _ref19.id;
                // TODO: Activate only if activation state false
                _this$data$find = this.data.find(function (_ref20) {
                  var mid = _ref20.id;
                  return mid === id;
                }), url = _this$data$find.activationUrl;
                _context6.next = 4;
                return browser.tabs.create({
                  url: url,
                  active: false,
                  pinned: true
                });

              case 4:
                _yield$browser$tabs$c = _context6.sent;
                tabId = _yield$browser$tabs$c.id;
                this.setState({
                  id: id,
                  data: {
                    activation: true
                  }
                });
                console.time('activate');
                _context6.next = 10;
                return this.activationTimeout({
                  id: id,
                  time: 10000
                });

              case 10:
                console.timeEnd('activate');
                _context6.prev = 11;
                _context6.next = 14;
                return browser.tabs.get(tabId);

              case 14:
                activationTab = _context6.sent;

                if (activationTab.active) {
                  _context6.next = 18;
                  break;
                }

                _context6.next = 18;
                return browser.tabs.remove(tabId);

              case 18:
                _context6.next = 23;
                break;

              case 20:
                _context6.prev = 20;
                _context6.t0 = _context6["catch"](11);
                console.log(_context6.t0);

              case 23:
                _context6.next = 25;
                return this.checkMerchantCookies(id);

              case 25:
                this.setState({
                  id: id,
                  data: {
                    activation: false,
                    activationProgress: 0
                  }
                });

              case 26:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6, this, [[11, 20]]);
      }));

      function activate(_x6) {
        return _activate.apply(this, arguments);
      }

      return activate;
    }()
  }, {
    key: "checkMerchantCookies",
    value: function () {
      var _checkMerchantCookies = _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2___default()( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function _callee7(id) {
        var merchantConfig, checker, isCookiesValid, state;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function _callee7$(_context7) {
          while (1) {
            switch (_context7.prev = _context7.next) {
              case 0:
                merchantConfig = this.configs.data.find(function (_ref21) {
                  var mid = _ref21.mid;
                  return mid === id;
                });

                if (!merchantConfig) {
                  _context7.next = 8;
                  break;
                }

                checker = new _cookieChecker__WEBPACK_IMPORTED_MODULE_12__["default"](merchantConfig.configs.cookieChecker);
                _context7.next = 5;
                return checker.check();

              case 5:
                isCookiesValid = _context7.sent;
                state = this.states[id];

                if (isCookiesValid && !state.activated) {
                  this.setState({
                    id: id,
                    data: this.getDefaultState('activated')
                  });
                }

              case 8:
              case "end":
                return _context7.stop();
            }
          }
        }, _callee7, this);
      }));

      function checkMerchantCookies(_x7) {
        return _checkMerchantCookies.apply(this, arguments);
      }

      return checkMerchantCookies;
    }()
  }, {
    key: "checkActivationStatus",
    value: function () {
      var _checkActivationStatus = _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2___default()( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function _callee8(cookie) {
        var domain, name, merchantConfig, id, checker, isCookiesValid, state;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function _callee8$(_context8) {
          while (1) {
            switch (_context8.prev = _context8.next) {
              case 0:
                domain = cookie.domain, name = cookie.name; // Find merchant config by changed cookie
                // If config found than affiliate cookie changed

                merchantConfig = this.configs.data.find(function (config) {
                  var _config$configs;

                  var cookieCheckerConfig = (config === null || config === void 0 ? void 0 : (_config$configs = config.configs) === null || _config$configs === void 0 ? void 0 : _config$configs.cookieChecker) || [];
                  return cookieCheckerConfig.some(function (_ref22) {
                    var domainPattern = _ref22.domain,
                        cookies = _ref22.cookies;

                    if (domain.search(domainPattern) > -1) {
                      return cookies.some(function (_ref23) {
                        var namePattern = _ref23.namePattern;
                        return name.search(namePattern) > -1;
                      });
                    }

                    return false;
                  });
                });

                if (!merchantConfig) {
                  _context8.next = 13;
                  break;
                }

                id = merchantConfig.mid;
                checker = new _cookieChecker__WEBPACK_IMPORTED_MODULE_12__["default"](merchantConfig.configs.cookieChecker);
                _context8.next = 7;
                return checker.check();

              case 7:
                isCookiesValid = _context8.sent;
                state = this.states[id];

                if (!(state.activated !== isCookiesValid)) {
                  _context8.next = 13;
                  break;
                }

                if (!isCookiesValid) {
                  _context8.next = 13;
                  break;
                }

                this.setState({
                  id: id,
                  data: this.getDefaultState('activated')
                });
                return _context8.abrupt("return", true);

              case 13:
                return _context8.abrupt("return", false);

              case 14:
              case "end":
                return _context8.stop();
            }
          }
        }, _callee8, this);
      }));

      function checkActivationStatus(_x8) {
        return _checkActivationStatus.apply(this, arguments);
      }

      return checkActivationStatus;
    }()
  }]);

  return Merchants;
}(_fetcher__WEBPACK_IMPORTED_MODULE_11__["default"]);



/***/ }),

/***/ "./source/bg/notificator.js":
/*!**********************************!*\
  !*** ./source/bg/notificator.js ***!
  \**********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/defineProperty */ "./node_modules/@babel/runtime/helpers/defineProperty.js");
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/asyncToGenerator */ "./node_modules/@babel/runtime/helpers/asyncToGenerator.js");
/* harmony import */ var _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ "./node_modules/@babel/runtime/helpers/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime/helpers/createClass */ "./node_modules/@babel/runtime/helpers/createClass.js");
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../constants */ "./source/constants.js");






function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_1___default()(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }



var Notificator = /*#__PURE__*/function () {
  function Notificator() {
    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_3___default()(this, Notificator);

    this.timerIdByTabId = {};
  }

  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_4___default()(Notificator, [{
    key: "sendNotification",
    value: function () {
      var _sendNotification = _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2___default()( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var _this = this;

        var data,
            to,
            sender,
            forcedTabId,
            _sender$tab,
            tabId,
            timerId,
            _args = arguments;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                data = _args.length > 0 && _args[0] !== undefined ? _args[0] : {};
                to = data.to, sender = data.sender, forcedTabId = data.forcedTabId;

                if (!(to === 'popup')) {
                  _context.next = 5;
                  break;
                }

                this.sendNotificationToPopup(data);
                return _context.abrupt("return");

              case 5:
                if (!(to === 'content')) {
                  _context.next = 12;
                  break;
                }

                tabId = forcedTabId || (sender === null || sender === void 0 ? void 0 : (_sender$tab = sender.tab) === null || _sender$tab === void 0 ? void 0 : _sender$tab.id);

                if (tabId in this.timerIdByTabId) {
                  clearTimeout(this.timerIdByTabId[tabId]);
                }

                this.sendNotificationToContent(_objectSpread(_objectSpread({}, data), {}, {
                  tabId: tabId
                }));
                timerId = setTimeout(function () {
                  _this.sendNotificationToContent({
                    tabId: tabId
                  });

                  delete _this.timerIdByTabId[tabId];
                }, 60000);
                this.timerIdByTabId[tabId] = timerId;
                return _context.abrupt("return");

              case 12:
                console.warn("sendNotification with invalid 'to': ".concat(to));

              case 13:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function sendNotification() {
        return _sendNotification.apply(this, arguments);
      }

      return sendNotification;
    }()
  }, {
    key: "sendNotificationToPopup",
    value: function sendNotificationToPopup(data) {
      chrome.runtime.sendMessage({
        action: 'sendNotificationToPopup',
        data: data
      });
    }
  }, {
    key: "sendNotificationToContent",
    value: function () {
      var _sendNotificationToContent = _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2___default()( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var data,
            notifications,
            _args2 = arguments;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                data = _args2.length > 0 && _args2[0] !== undefined ? _args2[0] : {};
                notifications = [];

                if (Array.isArray(data.params)) {
                  data.params.forEach(function (_ref) {
                    var section = _ref.section,
                        parameter = _ref.parameter;
                    notifications.push(_constants__WEBPACK_IMPORTED_MODULE_5__["NOTIFICATIONS"][section][parameter]);
                  });
                }

                chrome.tabs.sendMessage(data.tabId, {
                  action: 'sendNotificationToContent',
                  data: _objectSpread(_objectSpread({}, data), {}, {
                    notifications: notifications
                  })
                });

              case 4:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }));

      function sendNotificationToContent() {
        return _sendNotificationToContent.apply(this, arguments);
      }

      return sendNotificationToContent;
    }()
  }]);

  return Notificator;
}();

/* harmony default export */ __webpack_exports__["default"] = (Notificator);

/***/ }),

/***/ "./source/bg/user.js":
/*!***************************!*\
  !*** ./source/bg/user.js ***!
  \***************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return User; });
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/defineProperty */ "./node_modules/@babel/runtime/helpers/defineProperty.js");
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ "./node_modules/@babel/runtime/helpers/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/createClass */ "./node_modules/@babel/runtime/helpers/createClass.js");
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime/helpers/inherits */ "./node_modules/@babel/runtime/helpers/inherits.js");
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime/helpers/possibleConstructorReturn */ "./node_modules/@babel/runtime/helpers/possibleConstructorReturn.js");
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @babel/runtime/helpers/getPrototypeOf */ "./node_modules/@babel/runtime/helpers/getPrototypeOf.js");
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _fetcher__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./fetcher */ "./source/bg/fetcher.js");







function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5___default()(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_5___default()(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_4___default()(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }



var User = /*#__PURE__*/function (_Fetcher) {
  _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_3___default()(User, _Fetcher);

  var _super = _createSuper(User);

  function User(_ref) {
    var _this;

    var _ref$name = _ref.name,
        name = _ref$name === void 0 ? 'user' : _ref$name,
        url = _ref.url,
        _ref$options = _ref.options,
        options = _ref$options === void 0 ? {} : _ref$options;

    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1___default()(this, User);

    _this = _super.call(this, {
      name: name,
      url: url,
      options: options
    });
    _this.data = {};
    _this.options = _objectSpread({
      updateInterval: 45 * 60 * 1000,
      // 45 min
      updateErrorInterval: 60 * 1000
    }, options);
    return _this;
  }

  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2___default()(User, [{
    key: "parse",
    value: function parse(data) {
      // Also have card_type and created
      return {
        id: data.id,
        email: data.email,
        phone: data.phone,
        firstName: data.first_name,
        lastName: data.last_name,
        cardLastDigits: data.card_last_digits,
        emailConfirmed: data.email_confirmed,
        phoneConfirmed: data.phone_confirmed,
        cardLinked: data.card_linked
      };
    }
  }]);

  return User;
}(_fetcher__WEBPACK_IMPORTED_MODULE_6__["default"]);



/***/ }),

/***/ "./source/bg/validation-store.js":
/*!***************************************!*\
  !*** ./source/bg/validation-store.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ValidationStore; });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/asyncToGenerator */ "./node_modules/@babel/runtime/helpers/asyncToGenerator.js");
/* harmony import */ var _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ "./node_modules/@babel/runtime/helpers/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime/helpers/createClass */ "./node_modules/@babel/runtime/helpers/createClass.js");
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime/helpers/inherits */ "./node_modules/@babel/runtime/helpers/inherits.js");
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @babel/runtime/helpers/possibleConstructorReturn */ "./node_modules/@babel/runtime/helpers/possibleConstructorReturn.js");
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @babel/runtime/helpers/getPrototypeOf */ "./node_modules/@babel/runtime/helpers/getPrototypeOf.js");
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var events__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! events */ "./node_modules/events/events.js");
/* harmony import */ var events__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(events__WEBPACK_IMPORTED_MODULE_7__);








function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_6___default()(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_6___default()(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_5___default()(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

 // This class stores information about validated carts

var ValidationStore = /*#__PURE__*/function (_EventEmitter) {
  _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_4___default()(ValidationStore, _EventEmitter);

  var _super = _createSuper(ValidationStore);

  function ValidationStore(_ref) {
    var _this;

    var getMerchantConfig = _ref.getMerchantConfig;

    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_2___default()(this, ValidationStore);

    _this = _super.call(this);
    _this.validatedMerchants = {};
    _this.getMerchantConfig = getMerchantConfig;
    _this.basicParameters = ['activated', 'card'];
    return _this;
  }

  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_3___default()(ValidationStore, [{
    key: "update",
    value: function () {
      var _update = _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1___default()( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(_ref2) {
        var mid, result, params;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                mid = _ref2.mid, result = _ref2.result;
                this.validatedMerchants[mid] = this.validatedMerchants[mid] || {};
                Object.assign(this.validatedMerchants[mid], result);
                _context.next = 5;
                return this.validateAllParams({
                  mid: mid
                });

              case 5:
                params = _context.sent;
                this.emit('update', {
                  mid: mid,
                  params: params
                });

              case 7:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function update(_x) {
        return _update.apply(this, arguments);
      }

      return update;
    }()
  }, {
    key: "getValidationParams",
    value: function () {
      var _getValidationParams = _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1___default()( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2(mid) {
        var config, merchantParams;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.next = 2;
                return this.getMerchantConfig({
                  id: mid,
                  moduleName: 'validator'
                });

              case 2:
                _context2.t0 = _context2.sent;

                if (_context2.t0) {
                  _context2.next = 5;
                  break;
                }

                _context2.t0 = [];

              case 5:
                config = _context2.t0;
                merchantParams = Object.values(config).map(function (_ref3) {
                  var items = _ref3.items;
                  return Object.keys(items);
                }).flat();
                return _context2.abrupt("return", this.basicParameters.concat(merchantParams));

              case 8:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      function getValidationParams(_x2) {
        return _getValidationParams.apply(this, arguments);
      }

      return getValidationParams;
    }()
  }, {
    key: "validateAllParams",
    value: function () {
      var _validateAllParams = _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1___default()( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3(_ref4) {
        var _this2 = this;

        var mid, params;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                mid = _ref4.mid;
                _context3.next = 3;
                return this.getValidationParams(mid);

              case 3:
                params = _context3.sent;
                return _context3.abrupt("return", Object.fromEntries(params.map(function (name) {
                  var _this2$validatedMerch, _this2$validatedMerch2;

                  return [name, !!((_this2$validatedMerch = _this2.validatedMerchants) === null || _this2$validatedMerch === void 0 ? void 0 : (_this2$validatedMerch2 = _this2$validatedMerch[mid]) === null || _this2$validatedMerch2 === void 0 ? void 0 : _this2$validatedMerch2[name])];
                })));

              case 5:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this);
      }));

      function validateAllParams(_x3) {
        return _validateAllParams.apply(this, arguments);
      }

      return validateAllParams;
    }()
  }, {
    key: "isAllParamsValid",
    value: function () {
      var _isAllParamsValid = _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1___default()( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee4(_ref5) {
        var _this3 = this;

        var mid, params;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                mid = _ref5.mid;
                _context4.next = 3;
                return this.getValidationParams(mid);

              case 3:
                params = _context4.sent;
                return _context4.abrupt("return", params.every(function (name) {
                  return _this3.validatedMerchants[mid][name];
                }));

              case 5:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, this);
      }));

      function isAllParamsValid(_x4) {
        return _isAllParamsValid.apply(this, arguments);
      }

      return isAllParamsValid;
    }()
  }]);

  return ValidationStore;
}(events__WEBPACK_IMPORTED_MODULE_7___default.a);



/***/ }),

/***/ "./source/constants.js":
/*!*****************************!*\
  !*** ./source/constants.js ***!
  \*****************************/
/*! exports provided: API_ROOT, USER_URL, CHECK_CARD_ERRORS, DOMAIN, OFFERS_URL, MERCHANTS_URL, MERCHANTS_CONFIGS_URL, SUPPRESSION_PATTERNS, SERP_CONFIGS_URL, NOTIFICATIONS */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "API_ROOT", function() { return API_ROOT; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "USER_URL", function() { return USER_URL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CHECK_CARD_ERRORS", function() { return CHECK_CARD_ERRORS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DOMAIN", function() { return DOMAIN; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OFFERS_URL", function() { return OFFERS_URL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MERCHANTS_URL", function() { return MERCHANTS_URL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MERCHANTS_CONFIGS_URL", function() { return MERCHANTS_CONFIGS_URL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SUPPRESSION_PATTERNS", function() { return SUPPRESSION_PATTERNS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SERP_CONFIGS_URL", function() { return SERP_CONFIGS_URL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NOTIFICATIONS", function() { return NOTIFICATIONS; });
// TODO: Make dynamic api url for dev/prod
var API_ROOT = 'https://api-dev.getdisco.com/api/v1';
var DOMAIN = 'getdisco.com';
var USER_URL = "".concat(API_ROOT, "/me");
var MERCHANTS_URL = '/merchants.json';
var MERCHANTS_CONFIGS_URL = '/configs.json';
var OFFERS_URL = '/offers.json';
var SERP_CONFIGS_URL = '/serp.json';
var CHECK_CARD_ERRORS = {
  wrongNumber: 'You entered wrong card number',
  wrongParams: 'You enter wrong card number'
};
var SUPPRESSION_PATTERNS = ['awin1.com', 'kqzyfj.com', 'dotomi.com', 'emjcd.com', 'afsrc=1', 'ohost=www.google.com'];
var NOTIFICATIONS = {
  invalidParams: {
    activated: 'Please activate merchant to get discount.',
    card: 'Please use Disco card to get discount.',
    delivery: 'Please choose delivery to get discount',
    promo: 'You can not get discount if you use promo code'
  }
};


/***/ }),

/***/ "./source/modules/api.js":
/*!*******************************!*\
  !*** ./source/modules/api.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ "./node_modules/@babel/runtime/helpers/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/createClass */ "./node_modules/@babel/runtime/helpers/createClass.js");
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _constants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../constants */ "./source/constants.js");



/* eslint-disable class-methods-use-this */


var API = /*#__PURE__*/function () {
  function API() {
    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_0___default()(this, API);
  }

  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_1___default()(API, [{
    key: "getDiscountByCartId",
    // constructor() {}
    value: function getDiscountByCartId(cartId, data) {
      // data.items
      // data.total
      // data.merchant_id
      return this.call("/discount/cart/".concat(cartId), {
        method: 'POST',
        body: JSON.stringify(data)
      });
    }
  }, {
    key: "call",
    value: function call(endpoint) {
      var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      var url = "".concat(_constants__WEBPACK_IMPORTED_MODULE_2__["API_ROOT"]).concat(endpoint);
      return fetch(url, options).then(function (response) {
        // response.status – HTTP code of the response
        // response.ok – true if the status is 200-299
        if (!response.ok) {
          return Promise.reject(new Error("Bad status for api response: ".concat(response.status)));
        }

        return response.json();
      })["catch"](function (error) {
        // TODO: Add logger
        window.app.notificator.sendNotification({
          to: 'popup',
          // Error -> Object
          error: {
            name: error.name,
            message: error.message
          }
        });
      });
    }
  }]);

  return API;
}();

var api = new API();
/* harmony default export */ __webpack_exports__["default"] = (api);

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL0BiYWJlbC9ydW50aW1lL2hlbHBlcnMvZ2V0LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9AYmFiZWwvcnVudGltZS9oZWxwZXJzL29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9AYmFiZWwvcnVudGltZS9oZWxwZXJzL29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzTG9vc2UuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL0BiYWJlbC9ydW50aW1lL2hlbHBlcnMvc3VwZXJQcm9wQmFzZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvdXVpZC9kaXN0L2VzbS1icm93c2VyL2J5dGVzVG9VdWlkLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy91dWlkL2Rpc3QvZXNtLWJyb3dzZXIvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3V1aWQvZGlzdC9lc20tYnJvd3Nlci9tZDUuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3V1aWQvZGlzdC9lc20tYnJvd3Nlci9ybmcuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3V1aWQvZGlzdC9lc20tYnJvd3Nlci9zaGExLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy91dWlkL2Rpc3QvZXNtLWJyb3dzZXIvdjEuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3V1aWQvZGlzdC9lc20tYnJvd3Nlci92My5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvdXVpZC9kaXN0L2VzbS1icm93c2VyL3YzNS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvdXVpZC9kaXN0L2VzbS1icm93c2VyL3Y0LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy91dWlkL2Rpc3QvZXNtLWJyb3dzZXIvdjUuanMiLCJ3ZWJwYWNrOi8vLy4vc291cmNlL2JnL2FwcC5qcyIsIndlYnBhY2s6Ly8vLi9zb3VyY2UvYmcvY2FydENvbnRyb2xsZXIuanMiLCJ3ZWJwYWNrOi8vLy4vc291cmNlL2JnL2Nhc2hiYWNrLmpzIiwid2VicGFjazovLy8uL3NvdXJjZS9iZy9jb29raWVDaGVja2VyLmpzIiwid2VicGFjazovLy8uL3NvdXJjZS9iZy9mZXRjaGVyLmpzIiwid2VicGFjazovLy8uL3NvdXJjZS9iZy9tZXJjaGFudHMuanMiLCJ3ZWJwYWNrOi8vLy4vc291cmNlL2JnL25vdGlmaWNhdG9yLmpzIiwid2VicGFjazovLy8uL3NvdXJjZS9iZy91c2VyLmpzIiwid2VicGFjazovLy8uL3NvdXJjZS9iZy92YWxpZGF0aW9uLXN0b3JlLmpzIiwid2VicGFjazovLy8uL3NvdXJjZS9jb25zdGFudHMuanMiLCJ3ZWJwYWNrOi8vLy4vc291cmNlL21vZHVsZXMvYXBpLmpzIl0sIm5hbWVzIjpbImluaXRTZW50cnkiLCJBcHAiLCJ1c2VyIiwiVXNlciIsInVybCIsIlVTRVJfVVJMIiwibm90aWZpY2F0b3IiLCJOb3RpZmljYXRvciIsIm1lcmNoYW50cyIsIk1lcmNoYW50cyIsIk1FUkNIQU5UU19VUkwiLCJjYXNoYmFjayIsIkNhc2hiYWNrIiwidmFsaWRhdGlvblN0b3JlIiwiVmFsaWRhdGlvblN0b3JlIiwiZ2V0TWVyY2hhbnRDb25maWciLCJjYXJ0Q29udHJvbGxlciIsIkNhcnRDb250cm9sbGVyIiwic2VycENvbmZpZ3MiLCJGZXRjaGVyIiwiU0VSUF9DT05GSUdTX1VSTCIsIm5hbWUiLCJvcHRpb25zIiwidXBkYXRlSW50ZXJ2YWwiLCJzZXRNZXNzYWdlc0xpc3RlbmVyIiwic2V0RXZlbnRzTGlzdGVuZXIiLCJvbiIsIm1pZCIsInN0YXRlIiwidXBkYXRlIiwicmVzdWx0IiwiYWN0aXZhdGVkIiwicGFyYW1zIiwiaW52YWxpZFBhcmFtcyIsIk9iamVjdCIsImVudHJpZXMiLCJmb3JFYWNoIiwiaXNWYWxpZCIsInB1c2giLCJzZWN0aW9uIiwicGFyYW1ldGVyIiwibGVuZ3RoIiwiZ2V0TWVyY2hhbnRUYWJzIiwidGFicyIsInRhYiIsImlzU3BlY2lhbFBhZ2UiLCJpZCIsInNlbmROb3RpZmljYXRpb24iLCJmb3JjZWRUYWJJZCIsInRvIiwiZGF0YSIsInNlbmRlciIsImhyZWYiLCJtZXJjaGFudCIsImdldE1lcmNoYW50V2l0aFN0YXRlIiwidGFiSWQiLCJwcm9jZXNzTmFtZSIsImNoZWNrUHJvY2VzcyIsIm1lcmNoYW50RGF0YSIsIm1lcmNoYW50U3RhdGUiLCJhY3RpdmF0aW9uIiwic2V0U3RhdGUiLCJnZXREZWZhdWx0U3RhdGUiLCJjbGVhclByb2Nlc3MiLCJiaWQiLCJjdXJyeXNCaWQiLCJjaHJvbWUiLCJydW50aW1lIiwib25NZXNzYWdlIiwiYWRkTGlzdGVuZXIiLCJtZXNzYWdlIiwic2VuZFJlc3BvbnNlIiwibW9kdWxlIiwiYWN0aW9uIiwiY29udGV4dCIsImFzeW5jV3JhcHBlciIsImJpbmQiLCJ0aGVuIiwiY29va2llcyIsIm9uQ2hhbmdlZCIsImNvb2tpZSIsImNoZWNrQWN0aXZhdGlvblN0YXR1cyIsIndlYlJlcXVlc3QiLCJvbkJlZm9yZVJlcXVlc3QiLCJkZXRhaWxzIiwiY2hlY2tBdHRyaWJ1dGlvbiIsInVybHMiLCJ0eXBlcyIsIm9uVXBkYXRlZCIsImNoYW5nZUluZm8iLCJmcmFtZVVybCIsImJyb3dzZXIiLCJ3ZWJOYXZpZ2F0aW9uIiwiZ2V0QWxsRnJhbWVzIiwiZnJhbWVzIiwiYXBwRnJhbWVzIiwiZmlsdGVyIiwiaW5jbHVkZXMiLCJtYXAiLCJmcmFtZSIsImZyYW1lSWQiLCJzZW5kTWVzc2FnZSIsImV4ZWN1dGVTY3JpcHQiLCJmaWxlIiwid2luZG93IiwiYXBwIiwicGFyc2VkQ2FydHMiLCJwYXJzZUNhcnQiLCJoYW5kbGVkRGF0YSIsImhhbmRsZVBhcnNlZERhdGEiLCJpdGVtcyIsInRvdGFsIiwiY2FydElkIiwiYXBpIiwiZ2V0RGlzY291bnRCeUNhcnRJZCIsIm1lcmNoYW50X2lkIiwiZGlzY291bnRlZERhdGEiLCJoYW5kbGVEaXNjb3VudGVkRGF0YSIsImNvbmZpZyIsIm1vZHVsZU5hbWUiLCJwcm9jZXNzQ2FycnlzQ29uZmlnIiwicGFyc2VyIiwiUGFyc2VyIiwicHJvY2VzcyIsInBhcnNlZERhdGEiLCJzYXZlQ2FydERhdGEiLCJwcm9kdWN0cyIsInV1aWR2NCIsInRvdGFsQ3VycmVuY3kiLCJwcm9kdWN0IiwicHJpY2UiLCJjdXJyZW5jeSIsImJyYW5kIiwiaW1hZ2UiLCJjb2xvdXIiLCJ2YWx1ZSIsImNvZGUiLCJtZXRhIiwiZGlzY29Ub3RhbCIsImRpc2NvdW50ZWRfdG90YWwiLCJkaXNjb1Byb2R1Y3RzIiwiaXRlbSIsImRpc2NvUHJpY2UiLCJkaXNjb3VudGVkX3ByaWNlIiwicmVzdCIsImZpbmQiLCJza3UiLCJwYXR0ZXJucyIsImF0dHJpYnV0aW9uIiwiU1VQUFJFU1NJT05fUEFUVEVSTlMiLCJwcm9jZXNzZXMiLCJrZXlzIiwic29tZSIsInBhdHRlcm4iLCJzZWFyY2giLCJ0aW1lciIsInNldFRpbWVvdXQiLCJnZXRQcm9jZXNzIiwiY2xlYXJUaW1lb3V0IiwiQ29va2llQ2hlY2tlciIsIlByb21pc2UiLCJhbGwiLCJkb21haW4iLCJnZXRBbGwiLCJicm93c2VyQ29va2llcyIsImV2ZXJ5IiwibmFtZVBhdHRlcm4iLCJ2YWx1ZVBhdHRlcm4iLCJpc0Nvb2tpZXNWYWxpZCIsImlzQ29va2llVmFsaWQiLCJjaGVja3MiLCJ1cGRhdGVFcnJvckludGVydmFsIiwidXBkYXRlVGltZSIsInVwZGF0ZVRpbWVyIiwidXBkYXRlQ29udHJvbGxlciIsImluaXQiLCJsb2FkIiwic3RvcmFnZSIsImxvY2FsIiwiZ2V0IiwiY2FjaGVkRGF0YSIsInNldCIsImFib3J0IiwiQWJvcnRDb250cm9sbGVyIiwic2lnbmFsIiwiZmV0Y2giLCJyZXNwb25zZSIsImpzb24iLCJmb3JjZSIsIkRhdGUiLCJub3ciLCJyZXF1ZXN0IiwicGFyc2UiLCJzYXZlIiwicmVmcmVzaFVwZGF0ZVRpbWVyIiwiaW50ZXJ2YWwiLCJ0aW1lIiwiY2xlYXJJbnRlcnZhbCIsInNldEludGVydmFsIiwiRXZlbnRFbWl0dGVyIiwiVVJMIiwiaG9zdCIsIm1lcmNoYW50Q29uZmlnIiwiY29uZmlncyIsInN0YXRlcyIsIk1FUkNIQU5UU19DT05GSUdTX1VSTCIsInVwZGF0ZVN0YXRlcyIsInJlZnJlc2giLCJkZWZhdWx0U3RhdGUiLCJzaG93Tm90aWZpY2F0aW9uIiwic3VwcHJlc3NlZCIsImdldEJ5VXJsIiwidXBkYXRlQ29udGVudERhdGEiLCJlbWl0IiwiZSIsInF1ZXJ5IiwicGFnZXMiLCJwYWdlTmFtZSIsImdldEJ5U3BlY2lhbFBhZ2UiLCJtZXJjaGFudElkIiwidmFsdWVzIiwiZ2V0QnlJZCIsIm1hdGNoUGF0dGVybiIsImFjdGl2YXRpb25Qcm9ncmVzcyIsImFzeW5jVGltZW91dCIsImFjdGl2YXRpb25UaW1lb3V0IiwiYWN0aXZhdGlvblVybCIsImNyZWF0ZSIsImFjdGl2ZSIsInBpbm5lZCIsImNvbnNvbGUiLCJ0aW1lRW5kIiwiYWN0aXZhdGlvblRhYiIsInJlbW92ZSIsImxvZyIsImNoZWNrTWVyY2hhbnRDb29raWVzIiwiY2hlY2tlciIsImNvb2tpZUNoZWNrZXIiLCJjaGVjayIsImNvb2tpZUNoZWNrZXJDb25maWciLCJkb21haW5QYXR0ZXJuIiwidGltZXJJZEJ5VGFiSWQiLCJzZW5kTm90aWZpY2F0aW9uVG9Qb3B1cCIsInNlbmROb3RpZmljYXRpb25Ub0NvbnRlbnQiLCJ0aW1lcklkIiwid2FybiIsIm5vdGlmaWNhdGlvbnMiLCJBcnJheSIsImlzQXJyYXkiLCJOT1RJRklDQVRJT05TIiwiZW1haWwiLCJwaG9uZSIsImZpcnN0TmFtZSIsImZpcnN0X25hbWUiLCJsYXN0TmFtZSIsImxhc3RfbmFtZSIsImNhcmRMYXN0RGlnaXRzIiwiY2FyZF9sYXN0X2RpZ2l0cyIsImVtYWlsQ29uZmlybWVkIiwiZW1haWxfY29uZmlybWVkIiwicGhvbmVDb25maXJtZWQiLCJwaG9uZV9jb25maXJtZWQiLCJjYXJkTGlua2VkIiwiY2FyZF9saW5rZWQiLCJ2YWxpZGF0ZWRNZXJjaGFudHMiLCJiYXNpY1BhcmFtZXRlcnMiLCJhc3NpZ24iLCJ2YWxpZGF0ZUFsbFBhcmFtcyIsIm1lcmNoYW50UGFyYW1zIiwiZmxhdCIsImNvbmNhdCIsImdldFZhbGlkYXRpb25QYXJhbXMiLCJmcm9tRW50cmllcyIsIkFQSV9ST09UIiwiRE9NQUlOIiwiT0ZGRVJTX1VSTCIsIkNIRUNLX0NBUkRfRVJST1JTIiwid3JvbmdOdW1iZXIiLCJ3cm9uZ1BhcmFtcyIsImNhcmQiLCJkZWxpdmVyeSIsInByb21vIiwiQVBJIiwiY2FsbCIsIm1ldGhvZCIsImJvZHkiLCJKU09OIiwic3RyaW5naWZ5IiwiZW5kcG9pbnQiLCJvayIsInJlamVjdCIsIkVycm9yIiwic3RhdHVzIiwiZXJyb3IiXSwibWFwcGluZ3MiOiI7UUFBQTtRQUNBO1FBQ0E7UUFDQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLFFBQVEsb0JBQW9CO1FBQzVCO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0EsaUJBQWlCLDRCQUE0QjtRQUM3QztRQUNBO1FBQ0Esa0JBQWtCLDJCQUEyQjtRQUM3QztRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7O1FBRUE7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBOzs7UUFHQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMENBQTBDLGdDQUFnQztRQUMxRTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLHdEQUF3RCxrQkFBa0I7UUFDMUU7UUFDQSxpREFBaUQsY0FBYztRQUMvRDs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0EseUNBQXlDLGlDQUFpQztRQUMxRSxnSEFBZ0gsbUJBQW1CLEVBQUU7UUFDckk7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwyQkFBMkIsMEJBQTBCLEVBQUU7UUFDdkQsaUNBQWlDLGVBQWU7UUFDaEQ7UUFDQTtRQUNBOztRQUVBO1FBQ0Esc0RBQXNELCtEQUErRDs7UUFFckg7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBLGdCQUFnQix1QkFBdUI7UUFDdkM7OztRQUdBO1FBQ0E7UUFDQTtRQUNBOzs7Ozs7Ozs7Ozs7QUN2SkEsb0JBQW9CLG1CQUFPLENBQUMsK0VBQWlCOztBQUU3QztBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLHNCOzs7Ozs7Ozs7OztBQ3RCQSxtQ0FBbUMsbUJBQU8sQ0FBQyw2R0FBZ0M7O0FBRTNFO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsZUFBZSw2QkFBNkI7QUFDNUM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsMEM7Ozs7Ozs7Ozs7O0FDckJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsYUFBYSx1QkFBdUI7QUFDcEM7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSwrQzs7Ozs7Ozs7Ozs7QUNmQSxxQkFBcUIsbUJBQU8sQ0FBQyxpRkFBa0I7O0FBRS9DO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSxnQzs7Ozs7Ozs7Ozs7O0FDWEE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLGVBQWUsU0FBUztBQUN4QjtBQUNBOztBQUVBO0FBQ0E7QUFDQSxzQkFBc0I7O0FBRXRCO0FBQ0E7O0FBRWUsMEVBQVcsRTs7Ozs7Ozs7Ozs7O0FDakIxQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUF3QztBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7QUNGeEM7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGtEQUFrRDs7QUFFbEQ7O0FBRUEsbUJBQW1CLGdCQUFnQjtBQUNuQztBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsYUFBYSxjQUFjO0FBQzNCO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLGFBQWEsY0FBYztBQUMzQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLGFBQWEsbUJBQW1CO0FBQ2hDO0FBQ0E7O0FBRUE7O0FBRUEsYUFBYSxhQUFhO0FBQzFCO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRWUsa0VBQUcsRTs7Ozs7Ozs7Ozs7O0FDek5sQjtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0JBQStCOztBQUVoQjtBQUNmO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLEM7Ozs7Ozs7Ozs7OztBQ2RBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0Esa0RBQWtEOztBQUVsRDs7QUFFQSxtQkFBbUIsZ0JBQWdCO0FBQ25DO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxpQkFBaUIsT0FBTztBQUN4Qjs7QUFFQSxtQkFBbUIsUUFBUTtBQUMzQjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBLGlCQUFpQixPQUFPO0FBQ3hCOztBQUVBLG1CQUFtQixRQUFRO0FBQzNCO0FBQ0E7O0FBRUEsb0JBQW9CLFFBQVE7QUFDNUI7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLG1CQUFtQixRQUFRO0FBQzNCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRWUsbUVBQUksRTs7Ozs7Ozs7Ozs7O0FDMUZuQjtBQUFBO0FBQUE7QUFBMkI7QUFDZ0I7QUFDM0M7QUFDQTtBQUNBOztBQUVBOztBQUVBLGNBQWM7OztBQUdkO0FBQ0EsbUJBQW1COztBQUVuQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0VBQStFO0FBQy9FO0FBQ0E7O0FBRUE7QUFDQSxzREFBc0QsK0NBQUc7O0FBRXpEO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTs7O0FBR0EsaUZBQWlGO0FBQ2pGOztBQUVBLDJFQUEyRTs7QUFFM0UsNkRBQTZEOztBQUU3RDtBQUNBO0FBQ0EsR0FBRztBQUNIOzs7QUFHQTtBQUNBO0FBQ0EsR0FBRzs7O0FBR0g7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSx1QkFBdUI7O0FBRXZCLDBCQUEwQjs7QUFFMUI7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUI7O0FBRXJCO0FBQ0E7QUFDQSxzQkFBc0I7O0FBRXRCLG1DQUFtQzs7QUFFbkMsNkJBQTZCOztBQUU3QixpQ0FBaUM7O0FBRWpDLDJCQUEyQjs7QUFFM0IsaUJBQWlCLE9BQU87QUFDeEI7QUFDQTs7QUFFQSxxQkFBcUIsK0RBQVc7QUFDaEM7O0FBRWUsaUVBQUUsRTs7Ozs7Ozs7Ozs7O0FDOUZqQjtBQUFBO0FBQUE7QUFBMkI7QUFDQTtBQUMzQixTQUFTLHVEQUFHLGFBQWEsK0NBQUc7QUFDYixpRUFBRSxFOzs7Ozs7Ozs7Ozs7QUNIakI7QUFBQTtBQUFBO0FBQUE7QUFBMkM7O0FBRTNDO0FBQ0E7QUFDQTtBQUNBLDRCQUE0QixFQUFFO0FBQzlCO0FBQ0EsR0FBRztBQUNIO0FBQ0E7O0FBRUE7QUFDQSwwQ0FBMEM7O0FBRTFDOztBQUVBLGlCQUFpQixnQkFBZ0I7QUFDakM7QUFDQTs7QUFFQTtBQUNBOztBQUVPO0FBQ0E7QUFDUTtBQUNmO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw2SUFBNkk7O0FBRTdJO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLHVCQUF1QixVQUFVO0FBQ2pDO0FBQ0E7QUFDQTs7QUFFQSxrQkFBa0IsK0RBQVc7QUFDN0IsSUFBSTs7O0FBR0o7QUFDQTtBQUNBLEdBQUcsZUFBZTs7O0FBR2xCO0FBQ0E7QUFDQTtBQUNBLEM7Ozs7Ozs7Ozs7OztBQ3ZEQTtBQUFBO0FBQUE7QUFBMkI7QUFDZ0I7O0FBRTNDO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSwrQ0FBK0MsK0NBQUcsSUFBSTs7QUFFdEQ7QUFDQSxrQ0FBa0M7O0FBRWxDO0FBQ0Esb0JBQW9CLFNBQVM7QUFDN0I7QUFDQTtBQUNBOztBQUVBLGdCQUFnQiwrREFBVztBQUMzQjs7QUFFZSxpRUFBRSxFOzs7Ozs7Ozs7Ozs7QUMxQmpCO0FBQUE7QUFBQTtBQUEyQjtBQUNFO0FBQzdCLFNBQVMsdURBQUcsYUFBYSxnREFBSTtBQUNkLGlFQUFFLEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNIakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFQUEsZ0VBQVU7O0lBRUpDLEc7QUFDSixpQkFBYztBQUFBOztBQUNaLFNBQUtDLElBQUwsR0FBWSxJQUFJQyw2Q0FBSixDQUFTO0FBQUVDLFNBQUcsRUFBRUMsb0RBQVFBO0FBQWYsS0FBVCxDQUFaO0FBQ0EsU0FBS0MsV0FBTCxHQUFtQixJQUFJQyxvREFBSixFQUFuQjtBQUNBLFNBQUtDLFNBQUwsR0FBaUIsSUFBSUMsa0RBQUosQ0FBYztBQUFFTCxTQUFHLEVBQUVNLHlEQUFhQTtBQUFwQixLQUFkLENBQWpCO0FBQ0EsU0FBS0MsUUFBTCxHQUFnQixJQUFJQyxrREFBSixFQUFoQjtBQUNBLFNBQUtDLGVBQUwsR0FBdUIsSUFBSUMsMERBQUosQ0FBb0I7QUFDekNDLHVCQUFpQixFQUFFLEtBQUtQLFNBQUwsQ0FBZU87QUFETyxLQUFwQixDQUF2QjtBQUdBLFNBQUtDLGNBQUwsR0FBc0IsSUFBSUMsd0RBQUosQ0FBbUI7QUFDdkNGLHVCQUFpQixFQUFFLEtBQUtQLFNBQUwsQ0FBZU87QUFESyxLQUFuQixDQUF0QjtBQUdBLFNBQUtHLFdBQUwsR0FBbUIsSUFBSUMsZ0RBQUosQ0FBWTtBQUM3QmYsU0FBRyxFQUFFZ0IsNERBRHdCO0FBRTdCQyxVQUFJLEVBQUUsYUFGdUI7QUFHN0JDLGFBQU8sRUFBRTtBQUNQQyxzQkFBYyxFQUFFLElBQUksRUFBSixHQUFTLEVBQVQsR0FBYyxJQUR2QixDQUM2Qjs7QUFEN0I7QUFIb0IsS0FBWixDQUFuQjtBQU9BLFNBQUtDLG1CQUFMO0FBQ0EsU0FBS0MsaUJBQUw7QUFDRDs7Ozt3Q0FFbUI7QUFBQTs7QUFDbEIsV0FBS2pCLFNBQUwsQ0FBZWtCLEVBQWYsQ0FBa0IsY0FBbEIsRUFBa0MsZ0JBQW9CO0FBQUEsWUFBakJDLEdBQWlCLFFBQWpCQSxHQUFpQjtBQUFBLFlBQVpDLEtBQVksUUFBWkEsS0FBWTs7QUFDcEQsYUFBSSxDQUFDZixlQUFMLENBQXFCZ0IsTUFBckIsQ0FBNEI7QUFDMUJGLGFBQUcsRUFBSEEsR0FEMEI7QUFFMUJHLGdCQUFNLEVBQUU7QUFDTkMscUJBQVMsRUFBRUgsS0FBSyxDQUFDRztBQURYO0FBRmtCLFNBQTVCO0FBTUQsT0FQRDtBQVNBLFdBQUtsQixlQUFMLENBQXFCYSxFQUFyQixDQUF3QixRQUF4QjtBQUFBLHdMQUFrQztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBU0MscUJBQVQsU0FBU0EsR0FBVCxFQUFjSyxNQUFkLFNBQWNBLE1BQWQ7QUFDMUJDLCtCQUQwQixHQUNWLEVBRFU7QUFHaENDLHdCQUFNLENBQ0hDLE9BREgsQ0FDV0gsTUFEWCxFQUVHSSxPQUZILENBRVcsaUJBQXFCO0FBQUE7QUFBQSx3QkFBbkJmLElBQW1CO0FBQUEsd0JBQWJnQixPQUFhOztBQUM1Qix3QkFBSSxDQUFDQSxPQUFMLEVBQWM7QUFDWkosbUNBQWEsQ0FBQ0ssSUFBZCxDQUFtQjtBQUNqQkMsK0JBQU8sRUFBRSxlQURRO0FBRWpCQyxpQ0FBUyxFQUFFbkI7QUFGTSx1QkFBbkI7QUFJRDtBQUNGLG1CQVRIOztBQUhnQyx3QkFjNUJZLGFBQWEsQ0FBQ1EsTUFBZCxHQUF1QixDQWRLO0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUEseUJBZVgsS0FBSSxDQUFDakMsU0FBTCxDQUFla0MsZUFBZixDQUErQmYsR0FBL0IsQ0FmVzs7QUFBQTtBQWV4QmdCLHNCQWZ3QjtBQWlCOUJBLHNCQUFJLENBQUNQLE9BQUwsQ0FBYSxVQUFDUSxHQUFELEVBQVM7QUFDcEIsd0JBQU1DLGFBQWEsR0FBRyxLQUFJLENBQUNyQyxTQUFMLENBQWVxQyxhQUFmLENBQTZCO0FBQ2pEQyx3QkFBRSxFQUFFbkIsR0FENkM7QUFFakR2Qix5QkFBRyxFQUFFd0MsR0FBRyxDQUFDeEM7QUFGd0MscUJBQTdCLENBQXRCOztBQUtBLHdCQUFJeUMsYUFBYSxLQUFLLFVBQXRCLEVBQWtDO0FBQ2hDO0FBQ0Q7O0FBRUQseUJBQUksQ0FBQ3ZDLFdBQUwsQ0FBaUJ5QyxnQkFBakIsQ0FBa0M7QUFDaENDLGlDQUFXLEVBQUVKLEdBQUcsQ0FBQ0UsRUFEZTtBQUVoQ0csd0JBQUUsRUFBRSxTQUY0QjtBQUdoQ2pCLDRCQUFNLEVBQUVDO0FBSHdCLHFCQUFsQztBQUtELG1CQWZEOztBQWpCOEI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsU0FBbEM7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFtQ0Q7OztvQ0FFZTtBQUNkLGFBQU8sS0FBS2YsV0FBTCxDQUFpQmdDLElBQXhCO0FBQ0Q7OzswQ0FFZ0M7QUFBQSxVQUFoQkMsTUFBZ0IsU0FBaEJBLE1BQWdCO0FBQUEsVUFBUkMsSUFBUSxTQUFSQSxJQUFRO0FBQy9CLFVBQU1DLFFBQVEsR0FBRyxLQUFLN0MsU0FBTCxDQUFlOEMsb0JBQWYsQ0FBb0NGLElBQUksSUFBSUQsTUFBTSxDQUFDL0MsR0FBbkQsQ0FBakI7QUFDQSw2Q0FBWWlELFFBQVo7QUFBc0JuRCxZQUFJLEVBQUUsS0FBS0EsSUFBTCxDQUFVZ0Q7QUFBdEM7QUFDRDs7O2tDQUVhO0FBQ1osYUFBTyxLQUFLaEQsSUFBTCxDQUFVZ0QsSUFBakI7QUFDRDs7OzRDQUVnQztBQUFBLFVBQWQ5QyxHQUFjLFNBQWRBLEdBQWM7QUFBQSxVQUFUbUQsS0FBUyxTQUFUQSxLQUFTO0FBQy9CO0FBQ0EsVUFBTUMsV0FBVyxHQUFHLEtBQUs3QyxRQUFMLENBQWM4QyxZQUFkLENBQTJCO0FBQUVyRCxXQUFHLEVBQUhBLEdBQUY7QUFBT21ELGFBQUssRUFBTEE7QUFBUCxPQUEzQixDQUFwQjs7QUFDQSxVQUFJQyxXQUFXLEtBQUssYUFBcEIsRUFBbUM7QUFBQSxvQ0FDTyxLQUFLaEQsU0FBTCxDQUFlOEMsb0JBQWYsQ0FBb0NsRCxHQUFwQyxDQURQO0FBQUEsWUFDekJzRCxZQUR5Qix5QkFDekJBLFlBRHlCO0FBQUEsWUFDWEMsYUFEVyx5QkFDWEEsYUFEVyxFQUVqQztBQUNBOzs7QUFDQSxZQUFJRCxZQUFZLElBQUksRUFBQ0MsYUFBRCxhQUFDQSxhQUFELHVCQUFDQSxhQUFhLENBQUVDLFVBQWhCLENBQXBCLEVBQWdEO0FBQzlDLGVBQUtwRCxTQUFMLENBQWVxRCxRQUFmLENBQXdCO0FBQ3RCZixjQUFFLEVBQUVZLFlBQVksQ0FBQ1osRUFESztBQUV0QkksZ0JBQUksRUFBRSxLQUFLMUMsU0FBTCxDQUFlc0QsZUFBZixDQUErQixZQUEvQjtBQUZnQixXQUF4QjtBQUlEOztBQUNELGFBQUtuRCxRQUFMLENBQWNvRCxZQUFkLENBQTJCUCxXQUEzQixFQUF3Q0QsS0FBeEM7QUFDRDtBQUNGLEssQ0FFRDs7Ozt5Q0FDdUI7QUFBQSxVQUFQUyxHQUFPLFNBQVBBLEdBQU87QUFDckIsV0FBS2hELGNBQUwsQ0FBb0JpRCxTQUFwQixHQUFnQ0QsR0FBaEM7QUFDRDs7OzBDQUVxQjtBQUFBOztBQUNwQkUsWUFBTSxDQUFDQyxPQUFQLENBQWVDLFNBQWYsQ0FBeUJDLFdBQXpCLENBQXFDLFVBQUNDLE9BQUQsRUFBVW5CLE1BQVYsRUFBa0JvQixZQUFsQixFQUFtQztBQUFBLFlBQzlEQyxNQUQ4RCxHQUNyQ0YsT0FEcUMsQ0FDOURFLE1BRDhEO0FBQUEsWUFDdERDLE1BRHNELEdBQ3JDSCxPQURxQyxDQUN0REcsTUFEc0Q7QUFBQSxZQUM5Q3ZCLElBRDhDLEdBQ3JDb0IsT0FEcUMsQ0FDOUNwQixJQUQ4QztBQUd0RSxZQUFNd0IsT0FBTyxHQUFHLE1BQUksQ0FBQ0YsTUFBRCxDQUFKLElBQWdCLE1BQWhDOztBQUNBLFlBQUksT0FBT0UsT0FBTyxDQUFDRCxNQUFELENBQWQsS0FBMkIsVUFBL0IsRUFBMkM7QUFDekNFLHNFQUFZLENBQUNELE9BQU8sQ0FBQ0QsTUFBRCxDQUFQLENBQWdCRyxJQUFoQixDQUFxQkYsT0FBckIsQ0FBRCxrQ0FDUHhCLElBRE87QUFFVkMsa0JBQU0sRUFBTkE7QUFGVSxhQUFaLENBR0cwQixJQUhILENBR1FOLFlBSFI7QUFJQSxpQkFBTyxJQUFQO0FBQ0Q7O0FBRUQsZUFBTyxLQUFQO0FBQ0QsT0FiRDtBQWVBTCxZQUFNLENBQUNZLE9BQVAsQ0FBZUMsU0FBZixDQUNHVixXQURILENBQ2U7QUFBQSxZQUFHVyxNQUFILFNBQUdBLE1BQUg7QUFBQSxlQUFnQixNQUFJLENBQUN4RSxTQUFMLENBQWV5RSxxQkFBZixDQUFxQ0QsTUFBckMsQ0FBaEI7QUFBQSxPQURmO0FBRUFkLFlBQU0sQ0FBQ2dCLFVBQVAsQ0FBa0JDLGVBQWxCLENBQWtDZCxXQUFsQyxDQUE4QyxVQUFDZSxPQUFELEVBQWE7QUFDekQsY0FBSSxDQUFDQyxnQkFBTCxDQUFzQkQsT0FBdEI7QUFDRCxPQUZELEVBRUc7QUFBRUUsWUFBSSxFQUFFLENBQUMsWUFBRCxDQUFSO0FBQXdCQyxhQUFLLEVBQUUsQ0FBQyxZQUFEO0FBQS9CLE9BRkg7QUFHQXJCLFlBQU0sQ0FBQ3ZCLElBQVAsQ0FBWTZDLFNBQVosQ0FBc0JuQixXQUF0QixDQUFrQyxVQUFDZCxLQUFELEVBQVFrQyxVQUFSLFVBQWdDO0FBQUEsWUFBVnJGLEdBQVUsVUFBVkEsR0FBVTs7QUFDaEUsY0FBSSxDQUFDaUYsZ0JBQUwsQ0FBc0I7QUFBRWpGLGFBQUcsRUFBSEEsR0FBRjtBQUFPbUQsZUFBSyxFQUFMQTtBQUFQLFNBQXRCO0FBQ0QsT0FGRDtBQUdEOzs7Ozs7Ozs7O0FBRXNCbUMsd0IsVUFBQUEsUSxFQUFVdkMsTSxVQUFBQSxNO0FBQ3ZCUCxtQixHQUFRTyxNLENBQVJQLEc7O3VCQUNhK0MsT0FBTyxDQUFDQyxhQUFSLENBQXNCQyxZQUF0QixDQUFtQztBQUFFdEMsdUJBQUssRUFBRVgsR0FBRyxDQUFDRTtBQUFiLGlCQUFuQyxDOzs7QUFBZmdELHNCO0FBQ0FDLHlCLEdBQVlELE1BQU0sQ0FBQ0UsTUFBUCxDQUFjO0FBQUEsc0JBQUc1RixHQUFILFVBQUdBLEdBQUg7QUFBQSx5QkFBYUEsR0FBRyxDQUFDNkYsUUFBSixDQUFhUCxRQUFiLENBQWI7QUFBQSxpQkFBZCxDO0FBQ2xCSyx5QkFBUyxDQUFDRyxHQUFWO0FBQUEsbU1BQWMsa0JBQU9DLEtBQVA7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0pDLG1DQURJLEdBQ1FELEtBRFIsQ0FDSkMsT0FESTtBQUFBO0FBQUE7QUFBQSxtQ0FHSlQsT0FBTyxDQUFDaEQsSUFBUixDQUFhMEQsV0FBYixDQUF5QnpELEdBQUcsQ0FBQ0UsRUFBN0IsRUFBaUM7QUFBRTJCLG9DQUFNLEVBQUU7QUFBViw2QkFBakMsRUFBcUQ7QUFBRTJCLHFDQUFPLEVBQVBBO0FBQUYsNkJBQXJELENBSEk7O0FBQUE7QUFBQTtBQUFBOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUNBS0pULE9BQU8sQ0FBQ2hELElBQVIsQ0FBYTJELGFBQWIsQ0FBMkIxRCxHQUFHLENBQUNFLEVBQS9CLEVBQW1DO0FBQUV5RCxrQ0FBSSxFQUFFLHFCQUFSO0FBQStCSCxxQ0FBTyxFQUFQQTtBQUEvQiw2QkFBbkMsQ0FMSTs7QUFBQTtBQUFBO0FBQUEsbUNBTUpULE9BQU8sQ0FBQ2hELElBQVIsQ0FBYTJELGFBQWIsQ0FBMkIxRCxHQUFHLENBQUNFLEVBQS9CLEVBQW1DO0FBQUV5RCxrQ0FBSSxFQUFFLG1CQUFSO0FBQTZCSCxxQ0FBTyxFQUFQQTtBQUE3Qiw2QkFBbkMsQ0FOSTs7QUFBQTtBQUFBO0FBQUEsbUNBT0pULE9BQU8sQ0FBQ2hELElBQVIsQ0FBYTJELGFBQWIsQ0FBMkIxRCxHQUFHLENBQUNFLEVBQS9CLEVBQW1DO0FBQUV5RCxrQ0FBSSxFQUFFLGlCQUFSO0FBQTJCSCxxQ0FBTyxFQUFQQTtBQUEzQiw2QkFBbkMsQ0FQSTs7QUFBQTtBQUFBO0FBQUEsbUNBU05ULE9BQU8sQ0FBQ2hELElBQVIsQ0FBYTBELFdBQWIsQ0FDSnpELEdBQUcsQ0FBQ0UsRUFEQSxFQUVKO0FBQ0UyQixvQ0FBTSxFQUFFLFNBRFY7QUFFRXZCLGtDQUFJLEVBQUU7QUFBRTlDLG1DQUFHLEVBQUV3QyxHQUFHLENBQUN4QztBQUFYO0FBRlIsNkJBRkksRUFNSjtBQUFFZ0cscUNBQU8sRUFBUEE7QUFBRiw2QkFOSSxDQVRNOztBQUFBO0FBQUEsOERBaUJMLElBakJLOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQUFkOztBQUFBO0FBQUE7QUFBQTtBQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFzQkpJLE1BQU0sQ0FBQ0MsR0FBUCxHQUFhLElBQUl4RyxHQUFKLEVBQWIsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzFLQTtBQUNBO0FBQ0E7O0lBRXFCZ0IsYztBQUNuQixnQ0FBbUM7QUFBQSxRQUFyQkYsaUJBQXFCLFFBQXJCQSxpQkFBcUI7O0FBQUE7O0FBQ2pDLFNBQUtBLGlCQUFMLEdBQXlCQSxpQkFBekI7QUFDQSxTQUFLMkYsV0FBTCxHQUFtQixFQUFuQjtBQUNEOzs7Ozs7Ozs7OztBQUVlL0UsbUIsU0FBQUEsRzs7dUJBQ1IsS0FBS2dGLFNBQUwsQ0FBZTtBQUFFaEYscUJBQUcsRUFBSEE7QUFBRixpQkFBZixDOzs7cUJBQ0YsS0FBSytFLFdBQUwsQ0FBaUIvRSxHQUFqQixDOzs7OztBQUNJaUYsMkIsR0FBYyxLQUFLQyxnQkFBTCxDQUFzQixLQUFLSCxXQUFMLENBQWlCL0UsR0FBakIsQ0FBdEIsQztBQUNabUYscUIsR0FBeUJGLFcsQ0FBekJFLEssRUFBT0MsSyxHQUFrQkgsVyxDQUFsQkcsSyxFQUFPQyxNLEdBQVdKLFcsQ0FBWEksTTs7dUJBQ09DLG9EQUFHLENBQUNDLG1CQUFKLENBQXdCRixNQUF4QixFQUFnQztBQUMzREYsdUJBQUssRUFBTEEsS0FEMkQ7QUFFM0RDLHVCQUFLLEVBQUxBLEtBRjJEO0FBRzNESSw2QkFBVyxFQUFFeEY7QUFIOEMsaUJBQWhDLEM7OztBQUF2QnlGLDhCO2lEQUtDLEtBQUtDLG9CQUFMLENBQTBCRCxjQUExQixFQUEwQyxLQUFLVixXQUFMLENBQWlCL0UsR0FBakIsQ0FBMUMsQzs7O2lEQUVGLEk7Ozs7Ozs7Ozs7Ozs7Ozs7Ozt3Q0FHVzJGLE0sRUFBUTtBQUMxQiw2Q0FBWUEsTUFBWjtBQUFvQmxILFdBQUcsWUFBS2tILE1BQU0sQ0FBQ2xILEdBQVosU0FBa0IsS0FBSzZELFNBQXZCO0FBQXZCO0FBQ0Q7Ozs7Ozs7Ozs7QUFFaUJ0QyxtQixTQUFBQSxHOzt1QkFDRyxLQUFLWixpQkFBTCxDQUF1QjtBQUFFK0Isb0JBQUUsRUFBRW5CLEdBQU47QUFBVzRGLDRCQUFVLEVBQUU7QUFBdkIsaUJBQXZCLEM7OztBQUFmRCxzQjs7QUFDSjtBQUNBLG9CQUFJQSxNQUFNLElBQUkzRixHQUFHLEtBQUssc0NBQXRCLEVBQThEO0FBQzVEMkYsd0JBQU0sR0FBRyxLQUFLRSxtQkFBTCxDQUF5QkYsTUFBekIsQ0FBVDtBQUNEOztBQUNLRyxzQixHQUFTLElBQUlDLDZEQUFKLENBQVdKLE1BQVgsQzs7dUJBQ1VHLE1BQU0sQ0FBQ0UsT0FBUCxFOzs7QUFBbkJDLDBCO2tEQUNDLEtBQUtDLFlBQUwsQ0FBa0I7QUFBRWxHLHFCQUFHLEVBQUhBLEdBQUY7QUFBT2lHLDRCQUFVLEVBQVZBO0FBQVAsaUJBQWxCLEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozt3Q0FHeUI7QUFBQSxVQUFuQmpHLEdBQW1CLFNBQW5CQSxHQUFtQjtBQUFBLFVBQWRpRyxVQUFjLFNBQWRBLFVBQWM7QUFBQSxVQUN4QkUsUUFEd0IsR0FDWEYsVUFEVyxDQUN4QkUsUUFEd0I7O0FBRWhDLFVBQUlBLFFBQVEsQ0FBQ3JGLE1BQWIsRUFBcUI7QUFBQTs7QUFDbkIsWUFBTXVFLE1BQU0sR0FBRywyQkFBS04sV0FBTCxpR0FBbUIvRSxHQUFuQixpRkFBeUJxRixNQUF6QixLQUFtQ2UsK0NBQU0sRUFBeEQ7QUFDQSxhQUFLckIsV0FBTCxDQUFpQi9FLEdBQWpCO0FBQTBCcUYsZ0JBQU0sRUFBTkE7QUFBMUIsV0FBcUNZLFVBQXJDO0FBQ0EsZUFBT0EsVUFBUDtBQUNEOztBQUNELGFBQU8sS0FBS2xCLFdBQUwsQ0FBaUIvRSxHQUFqQixDQUFQO0FBQ0EsYUFBTyxJQUFQO0FBQ0Q7OztxQ0FFZ0J1QixJLEVBQU07QUFBQSxVQUNiNEUsUUFEYSxHQUM4QjVFLElBRDlCLENBQ2I0RSxRQURhO0FBQUEsVUFDSGYsS0FERyxHQUM4QjdELElBRDlCLENBQ0g2RCxLQURHO0FBQUEsVUFDSWlCLGFBREosR0FDOEI5RSxJQUQ5QixDQUNJOEUsYUFESjtBQUFBLFVBQ21CaEIsTUFEbkIsR0FDOEI5RCxJQUQ5QixDQUNtQjhELE1BRG5CLEVBQ29DOztBQUN6RCxVQUFNRixLQUFLLEdBQUdnQixRQUFRLENBQUM1QixHQUFULENBQWEsVUFBQytCLE9BQUQsRUFBYTtBQUFBLFlBQzlCQyxLQUQ4QixHQUNZRCxPQURaLENBQzlCQyxLQUQ4QjtBQUFBLFlBQ3ZCQyxRQUR1QixHQUNZRixPQURaLENBQ3ZCRSxRQUR1QjtBQUFBLFlBQ2JDLEtBRGEsR0FDWUgsT0FEWixDQUNiRyxLQURhO0FBQUEsWUFDTkMsS0FETSxHQUNZSixPQURaLENBQ05JLEtBRE07QUFBQSxZQUNDQyxNQURELEdBQ1lMLE9BRFosQ0FDQ0ssTUFERCxFQUNxQjs7QUFDM0QsK0NBQ0tMLE9BREw7QUFFRUMsZUFBSyxFQUFFO0FBQUVLLGlCQUFLLEVBQUVMLEtBQVQ7QUFBZ0JDLG9CQUFRLEVBQUVBLFFBQVEsQ0FBQ0s7QUFBbkMsV0FGVDtBQUdFQyxjQUFJLEVBQUU7QUFBRUwsaUJBQUssRUFBTEEsS0FBRjtBQUFTQyxpQkFBSyxFQUFMQSxLQUFUO0FBQWdCQyxrQkFBTSxFQUFOQTtBQUFoQjtBQUhSO0FBS0QsT0FQYSxDQUFkO0FBUUEsYUFBTztBQUNMeEIsYUFBSyxFQUFMQSxLQURLO0FBRUxDLGFBQUssRUFBRTtBQUNMd0IsZUFBSyxFQUFFeEIsS0FERjtBQUVMb0Isa0JBQVEsRUFBRUgsYUFBYSxDQUFDUTtBQUZuQixTQUZGO0FBTUx4QixjQUFNLEVBQU5BO0FBTkssT0FBUDtBQVFEOzs7eUNBRW9COUQsSSxTQUF5QjtBQUFBLGlDQUFqQjRFLFFBQWlCO0FBQUEsVUFBakJBLFFBQWlCLCtCQUFOLEVBQU07QUFBQSxVQUNwQ2hCLEtBRG9DLEdBQ0k1RCxJQURKLENBQ3BDNEQsS0FEb0M7QUFBQSxVQUNYNEIsVUFEVyxHQUNJeEYsSUFESixDQUM3QnlGLGdCQUQ2QjtBQUU1QyxVQUFNQyxhQUFhLEdBQUc5QixLQUFLLENBQUNaLEdBQU4sQ0FBVSxVQUFDMkMsSUFBRCxFQUFVO0FBQUEsWUFDaENYLEtBRGdDLEdBQ2lCVyxJQURqQixDQUNoQ1gsS0FEZ0M7QUFBQSxZQUNQWSxVQURPLEdBQ2lCRCxJQURqQixDQUN6QkUsZ0JBRHlCO0FBQUEsWUFDUUMsSUFEUix5RkFDaUJILElBRGpCLGtDQUN1Qjs7O0FBRHZCLDZCQUVYZixRQUFRLENBQUNtQixJQUFULENBQWM7QUFBQSxjQUFHQyxHQUFILFNBQUdBLEdBQUg7QUFBQSxpQkFBYUEsR0FBRyxLQUFLRixJQUFJLENBQUNFLEdBQTFCO0FBQUEsU0FBZCxDQUZXO0FBQUEsWUFFaEM3SCxJQUZnQyxrQkFFaENBLElBRmdDO0FBQUEsWUFFMUJnSCxLQUYwQixrQkFFMUJBLEtBRjBCO0FBQUEsWUFFbkJqSSxHQUZtQixrQkFFbkJBLEdBRm1COztBQUd4QztBQUFTMEksb0JBQVUsRUFBVkEsVUFBVDtBQUFxQlosZUFBSyxFQUFFQSxLQUFLLENBQUNLO0FBQWxDLFdBQTRDUyxJQUE1QztBQUFrRDNILGNBQUksRUFBSkEsSUFBbEQ7QUFBd0RnSCxlQUFLLEVBQUxBLEtBQXhEO0FBQStEakksYUFBRyxFQUFIQTtBQUEvRCxXQUh3QyxDQUc4QjtBQUN2RSxPQUpxQixDQUF0QjtBQUtBLGFBQU87QUFBRXdJLHFCQUFhLEVBQWJBLGFBQUY7QUFBaUJGLGtCQUFVLEVBQVZBO0FBQWpCLE9BQVA7QUFDRDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0NDN0VIOztJQUNxQjlILFE7QUFDbkIsc0JBQWM7QUFBQTs7QUFDWixTQUFLdUksUUFBTCxHQUFnQjtBQUNkQyxpQkFBVyxFQUFFQywrREFBb0JBO0FBRG5CLEtBQWhCO0FBR0EsU0FBS0MsU0FBTCxHQUFpQjtBQUNmRixpQkFBVyxFQUFFO0FBREUsS0FBakI7QUFHRDs7Ozt1Q0FFNEI7QUFBQTs7QUFBQSxVQUFkaEosR0FBYyxRQUFkQSxHQUFjO0FBQUEsVUFBVG1ELEtBQVMsUUFBVEEsS0FBUztBQUMzQjtBQUNBLFVBQU1DLFdBQVcsR0FBR3RCLE1BQU0sQ0FDdkJxSCxJQURpQixDQUNaLEtBQUtKLFFBRE8sRUFFakJGLElBRmlCLENBRVosVUFBQzVILElBQUQ7QUFBQSxlQUFVLEtBQUksQ0FBQzhILFFBQUwsQ0FBYzlILElBQWQsRUFBb0JtSSxJQUFwQixDQUF5QixVQUFDQyxPQUFEO0FBQUEsaUJBQWFySixHQUFHLENBQUNzSixNQUFKLENBQVdELE9BQVgsSUFBc0IsQ0FBQyxDQUFwQztBQUFBLFNBQXpCLENBQVY7QUFBQSxPQUZZLENBQXBCOztBQUdBLFVBQUlqRyxXQUFKLEVBQWlCO0FBQ2Y7QUFDQSxhQUFLTyxZQUFMLENBQWtCUCxXQUFsQixFQUErQkQsS0FBL0IsRUFGZSxDQUdmO0FBQ0E7O0FBQ0EsWUFBTW9HLEtBQUssR0FBR0MsVUFBVSxDQUFDO0FBQUEsaUJBQU0sS0FBSSxDQUFDN0YsWUFBTCxDQUFrQlAsV0FBbEIsRUFBK0JELEtBQS9CLENBQU47QUFBQSxTQUFELEVBQThDLEtBQTlDLENBQXhCO0FBQ0EsYUFBSytGLFNBQUwsQ0FBZTlGLFdBQWYsRUFBNEJELEtBQTVCLElBQXFDO0FBQUVvRyxlQUFLLEVBQUxBO0FBQUYsU0FBckM7QUFDRDs7QUFDRCxhQUFPLEtBQUtFLFVBQUwsQ0FBZ0J0RyxLQUFoQixDQUFQO0FBQ0Q7OzsrQkFFVUEsSyxFQUFPO0FBQUEsa0JBQ01yQixNQUFNLENBQ3pCQyxPQURtQixDQUNYLEtBQUttSCxTQURNLEVBRW5CTCxJQUZtQixDQUVkO0FBQUE7QUFBQSxZQUFJSyxTQUFKOztBQUFBLGVBQW1CQSxTQUFTLENBQUMvRixLQUFELENBQTVCO0FBQUEsT0FGYyxLQUUwQixFQUhoQztBQUFBO0FBQUEsVUFDVEMsV0FEUzs7QUFJaEIsYUFBT0EsV0FBUDtBQUNEOzs7aUNBRVlBLFcsRUFBYUQsSyxFQUFPO0FBQUE7O0FBQy9CdUcsa0JBQVksMEJBQUMsS0FBS1IsU0FBTCxDQUFlOUYsV0FBZixFQUE0QkQsS0FBNUIsQ0FBRCwwREFBQyxzQkFBb0NvRyxLQUFyQyxDQUFaO0FBQ0EsYUFBTyxLQUFLTCxTQUFMLENBQWU5RixXQUFmLEVBQTRCRCxLQUE1QixDQUFQO0FBQ0Q7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQ3ZDa0J3RyxhO0FBQ25CLDJCQUF5QjtBQUFBLFFBQWJ6QyxNQUFhLHVFQUFKLEVBQUk7O0FBQUE7O0FBQ3ZCLFNBQUtBLE1BQUwsR0FBY0EsTUFBZDtBQUNEOzs7Ozs7Ozs7Ozs7dUJBR3NCMEMsT0FBTyxDQUFDQyxHQUFSLENBQ25CLEtBQUszQyxNQUFMLENBQVlwQixHQUFaO0FBQUEsa01BQWdCO0FBQUE7O0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBU2dFLGtDQUFULFFBQVNBLE1BQVQsc0JBQWlCcEYsT0FBakIsRUFBaUJBLE9BQWpCLDZCQUEyQixFQUEzQjtBQUFBO0FBQUEsbUNBQ2VhLE9BQU8sQ0FBQ2IsT0FBUixDQUFnQnFGLE1BQWhCLENBQXVCO0FBQUVELG9DQUFNLEVBQU5BO0FBQUYsNkJBQXZCLENBRGY7O0FBQUE7QUFDUkUsMENBRFE7QUFBQSw2REFFUHRGLE9BQU8sQ0FBQ3VGLEtBQVIsQ0FBYyxpQkFBbUM7QUFBQSxrQ0FBaENDLFdBQWdDLFNBQWhDQSxXQUFnQztBQUFBLGtDQUFuQkMsWUFBbUIsU0FBbkJBLFlBQW1CO0FBQ3RELGtDQUFNQyxjQUFjLEdBQUdKLGNBQWMsQ0FBQ1osSUFBZixDQUFvQixpQkFBcUI7QUFBQSxvQ0FBbEJuSSxJQUFrQixTQUFsQkEsSUFBa0I7QUFBQSxvQ0FBWmtILEtBQVksU0FBWkEsS0FBWTtBQUM5RCxvQ0FBTWtDLGFBQWEsR0FBR3BKLElBQUksQ0FBQ3FJLE1BQUwsQ0FBWVksV0FBWixJQUEyQixDQUFDLENBQTVCLElBQWlDL0IsS0FBSyxDQUFDbUIsTUFBTixDQUFhYSxZQUFiLElBQTZCLENBQUMsQ0FBckY7QUFDQSx1Q0FBT0UsYUFBUDtBQUNELCtCQUhzQixDQUF2QjtBQUlBLHFDQUFPRCxjQUFQO0FBQ0QsNkJBTk0sQ0FGTzs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFBaEI7O0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBRG1CLEM7OztBQUFmRSxzQjtrREFZQ0EsTUFBTSxDQUFDTCxLQUFQLENBQWEsVUFBQzlCLEtBQUQ7QUFBQSx5QkFBV0EsS0FBSyxLQUFLLElBQXJCO0FBQUEsaUJBQWIsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ2xCWDs7SUFFcUJwSCxPOzs7OztBQUNuQix5QkFBeUM7QUFBQTs7QUFBQSxRQUEzQkUsSUFBMkIsUUFBM0JBLElBQTJCO0FBQUEsUUFBckJqQixHQUFxQixRQUFyQkEsR0FBcUI7QUFBQSw0QkFBaEJrQixPQUFnQjtBQUFBLFFBQWhCQSxPQUFnQiw2QkFBTixFQUFNOztBQUFBOztBQUN2QztBQUNBLFVBQUtELElBQUwsR0FBWUEsSUFBWjtBQUNBLFVBQUtqQixHQUFMLEdBQVdBLEdBQVg7QUFDQSxVQUFLa0IsT0FBTDtBQUNFQyxvQkFBYyxFQUFFLElBQUksRUFBSixHQUFTLElBRDNCO0FBQ2lDO0FBQy9Cb0oseUJBQW1CLEVBQUUsS0FBSztBQUY1QixPQUdLckosT0FITDtBQUtBLFVBQUtzSixVQUFMLEdBQWtCLENBQWxCO0FBQ0EsVUFBS0MsV0FBTCxHQUFtQixJQUFuQjtBQUNBLFVBQUtDLGdCQUFMLEdBQXdCLElBQXhCOztBQUNBLFVBQUtDLElBQUw7O0FBWnVDO0FBYXhDOzs7Ozs7Ozs7Ozt1QkFHTyxLQUFLQyxJQUFMLEU7Ozs7dUJBQ0EsS0FBS25KLE1BQUwsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7dUJBSW1COEQsT0FBTyxDQUFDc0YsT0FBUixDQUFnQkMsS0FBaEIsQ0FBc0JDLEdBQXRCLENBQTBCLFdBQzlDLEtBQUs5SixJQUR5QyxzQkFFOUMsS0FBS0EsSUFGeUMsaUJBQTFCLEM7OztBQUFuQitKLDBCO0FBSU4scUJBQUtsSSxJQUFMLEdBQVlrSSxVQUFVLFdBQUksS0FBSy9KLElBQVQsV0FBVixJQUFtQyxLQUFLNkIsSUFBcEQ7QUFDQSxxQkFBSzBILFVBQUwsR0FBa0JRLFVBQVUsV0FBSSxLQUFLL0osSUFBVCxpQkFBVixJQUF5QyxDQUEzRDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O3VCQUlNc0UsT0FBTyxDQUFDc0YsT0FBUixDQUFnQkMsS0FBaEIsQ0FBc0JHLEdBQXRCLDRJQUNBLEtBQUtoSyxJQURMLFlBQ21CLEtBQUs2QixJQUR4QixpSEFFQSxLQUFLN0IsSUFGTCxrQkFFeUIsS0FBS3VKLFVBRjlCLDBCOzs7Ozs7Ozs7Ozs7Ozs7Ozs7MEJBTUYxSCxJLEVBQU07QUFDVixhQUFPQSxJQUFQO0FBQ0Q7Ozs7Ozs7Ozs7Ozs7O0FBRWE5QyxtQiw4REFBTSxLQUFLQSxHO0FBQUtrQix1Qiw4REFBVSxFOztBQUN0QyxvQkFBSSxLQUFLd0osZ0JBQVQsRUFBMkI7QUFDekIsdUJBQUtBLGdCQUFMLENBQXNCUSxLQUF0QjtBQUNEOztBQUNELHFCQUFLUixnQkFBTCxHQUF3QixJQUFJUyxlQUFKLEVBQXhCO0FBQ1FDLHNCLEdBQVcsS0FBS1YsZ0IsQ0FBaEJVLE07O3VCQUNlQyxLQUFLLENBQUNyTCxHQUFEO0FBQVFvTCx3QkFBTSxFQUFOQTtBQUFSLG1CQUFtQmxLLE9BQW5CLEU7OztBQUF0Qm9LLHdCO2tEQUNDQSxRQUFRLENBQUNDLElBQVQsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7OzswTUFHSUMsSzs7Ozs7OztnQ0FDcUMsS0FBS3RLLE8sRUFBN0NDLGMsaUJBQUFBLGMsRUFBZ0JvSixtQixpQkFBQUEsbUI7O3NCQUNwQmlCLEtBQUssSUFBSSxLQUFLaEIsVUFBTCxHQUFrQnJKLGNBQWxCLEdBQW1Dc0ssSUFBSSxDQUFDQyxHQUFMLEU7Ozs7Ozs7dUJBRXpCLEtBQUtDLE9BQUwsRTs7O0FBQWI3SSxvQjtBQUNOLHFCQUFLQSxJQUFMLEdBQVksS0FBSzhJLEtBQUwsQ0FBVzlJLElBQVgsQ0FBWjs7dUJBQ00sS0FBSytJLElBQUwsRTs7O0FBQ04scUJBQUtDLGtCQUFMLENBQXdCLEVBQXhCO2tEQUNPLEtBQUtoSixJOzs7OztrREFFTCxLQUFLZ0osa0JBQUwsQ0FBd0I7QUFBRUMsMEJBQVEsRUFBRXhCLG1CQUFaO0FBQWlDaUIsdUJBQUssRUFBRTtBQUF4QyxpQkFBeEIsQzs7O0FBR1gscUJBQUtNLGtCQUFMLENBQXdCO0FBQUVFLHNCQUFJLEVBQUUsS0FBS3hCO0FBQWIsaUJBQXhCO2tEQUNPLEk7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs4Q0FHZ0Y7QUFBQTs7QUFBQSw2QkFBcEV3QixJQUFvRTtBQUFBLFVBQXBFQSxJQUFvRSwyQkFBN0RQLElBQUksQ0FBQ0MsR0FBTCxFQUE2RDtBQUFBLGlDQUFqREssUUFBaUQ7QUFBQSxVQUFqREEsUUFBaUQsK0JBQXRDLEtBQUs3SyxPQUFMLENBQWFDLGNBQXlCO0FBQUEsVUFBVHFLLEtBQVMsU0FBVEEsS0FBUztBQUN2RlMsbUJBQWEsQ0FBQyxLQUFLeEIsV0FBTixDQUFiO0FBQ0EsV0FBS0QsVUFBTCxHQUFrQndCLElBQWxCO0FBQ0EsV0FBS3ZCLFdBQUwsR0FBbUJ5QixXQUFXLENBQUMsWUFBTTtBQUNuQ0QscUJBQWEsQ0FBQyxNQUFJLENBQUN4QixXQUFOLENBQWI7O0FBQ0EsY0FBSSxDQUFDaEosTUFBTCxDQUFZK0osS0FBWjtBQUNELE9BSDZCLEVBRzNCUSxJQUFJLEdBQUdELFFBQVAsR0FBa0JOLElBQUksQ0FBQ0MsR0FBTCxFQUhTLENBQTlCO0FBSUQ7Ozs7RUEzRWtDUyw2Qzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNGckM7QUFDQTtBQUNBO0FBQ0E7O0lBRXFCOUwsUzs7Ozs7QUFDbkIsMkJBQXVEO0FBQUE7O0FBQUEseUJBQXpDWSxJQUF5QztBQUFBLFFBQXpDQSxJQUF5QywwQkFBbEMsV0FBa0M7QUFBQSxRQUFyQmpCLElBQXFCLFFBQXJCQSxHQUFxQjtBQUFBLDRCQUFoQmtCLE9BQWdCO0FBQUEsUUFBaEJBLE9BQWdCLDZCQUFOLEVBQU07O0FBQUE7O0FBQ3JELDhCQUFNO0FBQUVELFVBQUksRUFBSkEsSUFBRjtBQUFRakIsU0FBRyxFQUFIQSxJQUFSO0FBQWFrQixhQUFPLEVBQVBBO0FBQWIsS0FBTjs7QUFEcUQsbU1BNktuQyxpQkFBNkI7QUFBQSxVQUExQndCLEVBQTBCLFNBQTFCQSxFQUEwQjtBQUFBLFVBQXRCMUMsR0FBc0IsU0FBdEJBLEdBQXNCO0FBQUEsVUFBakJtSCxVQUFpQixTQUFqQkEsVUFBaUI7QUFDL0MsVUFBSTVGLEdBQUcsR0FBR21CLEVBQVY7O0FBQ0EsVUFBSSxDQUFDQSxFQUFELElBQU8sQ0FBQzFDLEdBQVosRUFBaUI7QUFDZixlQUFPLElBQVA7QUFDRDs7QUFDRCxVQUFJLENBQUN1QixHQUFELElBQVF2QixHQUFaLEVBQWlCO0FBQUEsbUJBQ0UsSUFBSW9NLEdBQUosQ0FBUXBNLEdBQVIsQ0FERjtBQUFBLFlBQ1BxTSxJQURPLFFBQ1BBLElBRE87O0FBRWYsWUFBTXBKLFFBQVEsR0FBRyxNQUFLSCxJQUFMLENBQVUrRixJQUFWLENBQWU7QUFBQSxjQUFHaUIsTUFBSCxTQUFHQSxNQUFIO0FBQUEsaUJBQWdCdUMsSUFBSSxDQUFDeEcsUUFBTCxDQUFjaUUsTUFBZCxDQUFoQjtBQUFBLFNBQWYsQ0FBakI7O0FBQ0F2SSxXQUFHLEdBQUcwQixRQUFILGFBQUdBLFFBQUgsdUJBQUdBLFFBQVEsQ0FBRVAsRUFBaEI7QUFDRDs7QUFDRCxVQUFJbkIsR0FBSixFQUFTO0FBQUE7O0FBQ1AsWUFBTStLLGNBQWMsR0FBRyxNQUFLQyxPQUFMLENBQWF6SixJQUFiLENBQWtCK0YsSUFBbEIsQ0FBdUIsVUFBQzNCLE1BQUQ7QUFBQSxpQkFBWTNGLEdBQUcsS0FBSzJGLE1BQU0sQ0FBQzNGLEdBQTNCO0FBQUEsU0FBdkIsQ0FBdkI7O0FBQ0EsZUFBTzRGLFVBQVUsR0FBR21GLGNBQUgsYUFBR0EsY0FBSCxnREFBR0EsY0FBYyxDQUFFQyxPQUFuQiwwREFBRyxzQkFBMEJwRixVQUExQixDQUFILEdBQTJDbUYsY0FBNUQ7QUFDRDs7QUFDRCxhQUFPLElBQVA7QUFDRCxLQTVMc0Q7O0FBRXJELFVBQUt4SixJQUFMLEdBQVksRUFBWjtBQUNBLFVBQUswSixNQUFMLEdBQWMsRUFBZDtBQUNBLFVBQUtELE9BQUwsR0FBZSxJQUFJeEwsaURBQUosQ0FBWTtBQUN6QmYsU0FBRyxFQUFFeU0saUVBRG9CO0FBRXpCeEwsVUFBSSxFQUFFLGtCQUZtQjtBQUd6QkMsYUFBTyxFQUFFO0FBQ1BDLHNCQUFjLEVBQUUsSUFBSSxFQUFKLEdBQVMsRUFBVCxHQUFjLElBRHZCLENBQzZCOztBQUQ3QjtBQUhnQixLQUFaLENBQWY7O0FBT0EsVUFBS29MLE9BQUwsQ0FBYTlLLE1BQWIsQ0FBb0IsSUFBcEI7O0FBWHFEO0FBWXREOzs7OzRCQUVnQjtBQUFBLFVBQVhxQixJQUFXLHVFQUFKLEVBQUk7QUFDZixhQUFPQSxJQUFJLENBQUMxQyxTQUFaO0FBQ0Q7Ozs7eU1BRVlvTCxLOzs7Ozs7dU5BQ1FBLEs7OztBQUNuQixxQkFBS2tCLFlBQUw7aURBQ08sS0FBSzVKLEk7Ozs7Ozs7Ozs7Ozs7Ozs7OztpQ0FHRDZKLE8sRUFBUztBQUFBOztBQUNwQixXQUFLN0osSUFBTCxDQUFVZCxPQUFWLENBQWtCLGlCQUFZO0FBQUEsWUFBVFUsRUFBUyxTQUFUQSxFQUFTOztBQUM1QixZQUFNa0ssWUFBWSxHQUFHLE1BQUksQ0FBQ2xKLGVBQUwsRUFBckI7O0FBQ0EsY0FBSSxDQUFDOEksTUFBTCxDQUFZOUosRUFBWixJQUFrQmlLLE9BQU8sR0FBR0MsWUFBSCxHQUFtQixNQUFJLENBQUNKLE1BQUwsQ0FBWTlKLEVBQVosS0FBbUJrSyxZQUEvRDtBQUNELE9BSEQ7QUFJRDs7O3NDQUVpQztBQUFBLFVBQWxCM0wsSUFBa0IsdUVBQVgsU0FBVztBQUNoQyxVQUFNdUwsTUFBTSxHQUFHO0FBQ2IsbUJBQVM7QUFDUDdLLG1CQUFTLEVBQUUsS0FESjtBQUVQa0wsMEJBQWdCLEVBQUU7QUFGWCxTQURJO0FBS2JsTCxpQkFBUyxFQUFFO0FBQ1RBLG1CQUFTLEVBQUUsSUFERjtBQUVUNkIsb0JBQVUsRUFBRSxLQUZIO0FBR1RzSixvQkFBVSxFQUFFLEtBSEg7QUFJVEQsMEJBQWdCLEVBQUU7QUFKVCxTQUxFO0FBV2JDLGtCQUFVLEVBQUU7QUFDVm5MLG1CQUFTLEVBQUUsS0FERDtBQUVWbUwsb0JBQVUsRUFBRSxJQUZGO0FBR1ZELDBCQUFnQixFQUFFO0FBSFI7QUFYQyxPQUFmO0FBaUJBLGFBQU9MLE1BQU0sQ0FBQ3ZMLElBQUQsQ0FBYjtBQUNEOzs7b0NBRTJCO0FBQUEsVUFBakJ5QixFQUFpQixTQUFqQkEsRUFBaUI7QUFBQSxVQUFiMUMsR0FBYSxTQUFiQSxHQUFhO0FBQUEsVUFBUjhDLElBQVEsU0FBUkEsSUFBUTs7QUFDMUIsVUFBSTtBQUNGLFlBQU12QixHQUFHLEdBQUdtQixFQUFFLElBQUksS0FBS3FLLFFBQUwsQ0FBYy9NLEdBQWQsRUFBbUIwQyxFQUFyQztBQUNBLGFBQUs4SixNQUFMLENBQVlqTCxHQUFaLG9DQUF5QixLQUFLaUwsTUFBTCxDQUFZakwsR0FBWixLQUFvQixFQUE3QyxHQUFxRHVCLElBQXJEO0FBQ0EsYUFBS2tLLGlCQUFMLENBQXVCO0FBQUV0SyxZQUFFLEVBQUVuQixHQUFOO0FBQVd1QixjQUFJLEVBQUU7QUFBRVMseUJBQWEsRUFBRSxLQUFLaUosTUFBTCxDQUFZakwsR0FBWjtBQUFqQjtBQUFqQixTQUF2QjtBQUNBLGFBQUswTCxJQUFMLENBQVUsY0FBVixFQUEwQjtBQUFFMUwsYUFBRyxFQUFIQSxHQUFGO0FBQU9DLGVBQUssb0JBQU8sS0FBS2dMLE1BQUwsQ0FBWWpMLEdBQVosQ0FBUDtBQUFaLFNBQTFCO0FBQ0EsZUFBTyxLQUFLaUwsTUFBTCxDQUFZakwsR0FBWixDQUFQO0FBQ0QsT0FORCxDQU1FLE9BQU8yTCxDQUFQLEVBQVU7QUFDVixlQUFPLElBQVA7QUFDRDtBQUNGOzs7Ozs7Ozs7O0FBRXlCeEssa0IsU0FBQUEsRSxFQUFJSSxJLFNBQUFBLEk7O3VCQUNULEtBQUtSLGVBQUwsQ0FBcUJJLEVBQXJCLEM7OztBQUFiSCxvQjtrREFDQ3FILE9BQU8sQ0FBQ0MsR0FBUixDQUNMdEgsSUFBSSxDQUFDdUQsR0FBTDtBQUFBLGtNQUFTLGtCQUFPdEQsR0FBUDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsOERBQWUrQyxPQUFPLENBQUNoRCxJQUFSLENBQWEwRCxXQUFiLENBQXlCekQsR0FBRyxDQUFDRSxFQUE3QixFQUFpQztBQUN2RDJCLG9DQUFNLEVBQUUsUUFEK0M7QUFFdkR2QixrQ0FBSSxFQUFKQTtBQUZ1RCw2QkFBakMsQ0FBZjs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFBVDs7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkFESyxDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O21OQVFhSixFOzs7Ozs7Ozs7dUJBQ0Q2QyxPQUFPLENBQUNoRCxJQUFSLENBQWE0SyxLQUFiLENBQW1CLEVBQW5CLEM7OztBQUFiNUssb0I7a0RBQ0NBLElBQUksQ0FBQ3FELE1BQUwsQ0FBWTtBQUFBLHNCQUFHNUYsR0FBSCxTQUFHQSxHQUFIO0FBQUEseUJBQWEsQ0FBQyxNQUFJLENBQUMrTSxRQUFMLENBQWMvTSxHQUFkLEtBQXNCLEVBQXZCLEVBQTJCMEMsRUFBM0IsS0FBa0NBLEVBQS9DO0FBQUEsaUJBQVosQzs7Ozs7Ozs7Ozs7Ozs7Ozs7O3lDQUdrQjtBQUFBLFVBQVhBLEVBQVcsU0FBWEEsRUFBVztBQUFBLFVBQVAxQyxHQUFPLFNBQVBBLEdBQU87QUFDekIsVUFBTWtILE1BQU0sR0FBRyxLQUFLdkcsaUJBQUwsQ0FBdUI7QUFBRStCLFVBQUUsRUFBRkE7QUFBRixPQUF2QixDQUFmOztBQUNBLFVBQUl3RSxNQUFKLEVBQVk7QUFBQSxxQkFDU3BGLE1BQU0sQ0FDdEJDLE9BRGdCLENBQ1JtRixNQUFNLENBQUNrRyxLQUFQLElBQWdCLEVBRFIsRUFFaEJ2RSxJQUZnQixDQUVYO0FBQUE7QUFBQSxjQUFJUSxPQUFKOztBQUFBLGlCQUFpQnJKLEdBQUcsQ0FBQ3NKLE1BQUosQ0FBV0QsT0FBWCxJQUFzQixDQUFDLENBQXhDO0FBQUEsU0FGVyxLQUVtQyxFQUg1QztBQUFBO0FBQUEsWUFDSGdFLFFBREc7O0FBSVYsZUFBT0EsUUFBUDtBQUNEOztBQUNELGFBQU8sSUFBUDtBQUNEOzs7d0JBRUdyTixHLEVBQUs7QUFDUCxVQUFNaUQsUUFBUSxHQUFHLEtBQUs4SixRQUFMLENBQWMvTSxHQUFkLENBQWpCO0FBQ0EsYUFBTyxDQUFDaUQsUUFBRCxHQUFZLEtBQUtxSyxnQkFBTCxDQUFzQnROLEdBQXRCLENBQVosR0FBeUNpRCxRQUFoRDtBQUNEOzs7NEJBRU9QLEUsRUFBSTtBQUNWLGFBQU8sS0FBS0ksSUFBTCxDQUFVK0YsSUFBVixDQUFlO0FBQUEsWUFBTzBFLFVBQVAsVUFBRzdLLEVBQUg7QUFBQSxlQUF3QkEsRUFBRSxLQUFLNkssVUFBL0I7QUFBQSxPQUFmLENBQVA7QUFDRDs7O3FDQUVnQnZOLEcsRUFBSztBQUNwQixVQUFJO0FBQ0YsWUFBTWtILE1BQU0sR0FBRyxLQUFLcUYsT0FBTCxDQUFhekosSUFBYixDQUNaK0YsSUFEWSxDQUNQO0FBQUEsb0NBQUd1RSxLQUFIO0FBQUEsY0FBR0EsS0FBSCw2QkFBVyxFQUFYO0FBQUEsaUJBQW9CdEwsTUFBTSxDQUFDMEwsTUFBUCxDQUFjSixLQUFkLEVBQXFCaEUsSUFBckIsQ0FBMEIsVUFBQ0MsT0FBRDtBQUFBLG1CQUFhckosR0FBRyxDQUFDc0osTUFBSixDQUFXRCxPQUFYLElBQXNCLENBQUMsQ0FBcEM7QUFBQSxXQUExQixDQUFwQjtBQUFBLFNBRE8sQ0FBZjtBQUVBLGVBQU8sQ0FBQW5DLE1BQU0sU0FBTixJQUFBQSxNQUFNLFdBQU4sWUFBQUEsTUFBTSxDQUFFM0YsR0FBUixLQUFlLEtBQUtrTSxPQUFMLENBQWF2RyxNQUFiLGFBQWFBLE1BQWIsdUJBQWFBLE1BQU0sQ0FBRTNGLEdBQXJCLENBQXRCO0FBQ0QsT0FKRCxDQUlFLE9BQU8yTCxDQUFQLEVBQVU7QUFDVixlQUFPLElBQVA7QUFDRDtBQUNGOzs7NkJBRVFsTixHLEVBQUs7QUFDWixVQUFJO0FBQUEsb0JBQ2UsSUFBSW9NLEdBQUosQ0FBUXBNLEdBQVIsQ0FEZjtBQUFBLFlBQ01xTSxJQUROLFNBQ01BLElBRE47O0FBRUYsZUFBTyxLQUFLdkosSUFBTCxDQUNKK0YsSUFESSxDQUNDO0FBQUEsY0FBR2lCLE1BQUgsVUFBR0EsTUFBSDtBQUFBLGNBQVc0RCxZQUFYLFVBQVdBLFlBQVg7QUFBQSxpQkFBOEJyQixJQUFJLENBQUN4RyxRQUFMLENBQWNpRSxNQUFkLEtBQXlCOUosR0FBRyxDQUFDc0osTUFBSixDQUFXb0UsWUFBWCxJQUEyQixDQUFDLENBQW5GO0FBQUEsU0FERCxDQUFQO0FBRUQsT0FKRCxDQUlFLE9BQU9SLENBQVAsRUFBVTtBQUNWLGVBQU8sSUFBUDtBQUNEO0FBQ0Y7Ozs0Q0FFNEI7QUFBQSxVQUFYeEssRUFBVyxVQUFYQSxFQUFXO0FBQUEsVUFBUDFDLEdBQU8sVUFBUEEsR0FBTztBQUMzQixVQUFJc0QsWUFBWSxHQUFHLElBQW5CO0FBQ0EsVUFBSUMsYUFBYSxHQUFHLElBQXBCO0FBQ0EsVUFBSStJLGNBQWMsR0FBRyxJQUFyQjs7QUFDQSxVQUFJNUosRUFBSixFQUFRO0FBQ05ZLG9CQUFZLEdBQUcsS0FBS21LLE9BQUwsQ0FBYS9LLEVBQWIsQ0FBZjtBQUNEOztBQUNELFVBQUksQ0FBQ1ksWUFBRCxJQUFpQnRELEdBQXJCLEVBQTBCO0FBQ3hCc0Qsb0JBQVksR0FBRyxLQUFLeUosUUFBTCxDQUFjL00sR0FBZCxDQUFmO0FBQ0FzRCxvQkFBWSxHQUFHQSxZQUFZLElBQUksS0FBS2dLLGdCQUFMLENBQXNCdE4sR0FBdEIsQ0FBL0I7QUFDRDs7QUFDRCxVQUFJc0QsWUFBSixFQUFrQjtBQUNoQmdKLHNCQUFjLEdBQUcsS0FBS0MsT0FBTCxDQUFhekosSUFBYixDQUFrQitGLElBQWxCLENBQXVCLFVBQUMzQixNQUFEO0FBQUEsaUJBQVk1RCxZQUFZLENBQUNaLEVBQWIsS0FBb0J3RSxNQUFNLENBQUMzRixHQUF2QztBQUFBLFNBQXZCLENBQWpCO0FBQ0FnQyxxQkFBYSxHQUFHLEtBQUtpSixNQUFMLENBQVlsSixZQUFZLENBQUNaLEVBQXpCLENBQWhCO0FBQ0EsNkRBQVlZLFlBQVosR0FBNkJDLGFBQTdCO0FBQTRDMkQsZ0JBQU0sRUFBRW9GO0FBQXBEO0FBQ0Q7O0FBQ0QsYUFBTyxJQUFQO0FBQ0Q7Ozt5Q0FFb0J0TSxHLEVBQUs7QUFDeEIsVUFBTXNELFlBQVksR0FBRyxLQUFLeUgsR0FBTCxDQUFTL0ssR0FBVCxDQUFyQjtBQUNBLFVBQU11RCxhQUFhLEdBQUcsS0FBS2lKLE1BQUwsQ0FBWWxKLFlBQVosYUFBWUEsWUFBWix1QkFBWUEsWUFBWSxDQUFFWixFQUExQixDQUF0QjtBQUNBLGFBQU87QUFBRVksb0JBQVksRUFBWkEsWUFBRjtBQUFnQkMscUJBQWEsRUFBYkE7QUFBaEIsT0FBUDtBQUNEOzs7Ozs7Ozs7OztBQUV5QmIsa0IsVUFBQUEsRSxFQUFJc0osSSxVQUFBQSxJLGlDQUFNMkIsa0IsRUFBQUEsa0Isc0NBQXFCLEM7O3NCQUNuREEsa0JBQWtCLEdBQUcsR0FBckIsSUFBNEIsQ0FBQyxLQUFLbkIsTUFBTCxDQUFZOUosRUFBWixFQUFnQmYsUzs7Ozs7QUFDL0MscUJBQUs4QixRQUFMLENBQWM7QUFBRWYsb0JBQUUsRUFBRkEsRUFBRjtBQUFNSSxzQkFBSSxFQUFFO0FBQUU2SyxzQ0FBa0IsRUFBbEJBO0FBQUY7QUFBWixpQkFBZDs7dUJBQ01DLDREQUFZLENBQUM1QixJQUFJLEdBQUcsRUFBUixDOzs7a0RBQ1gsS0FBSzZCLGlCQUFMLENBQXVCO0FBQUVuTCxvQkFBRSxFQUFGQSxFQUFGO0FBQU1zSixzQkFBSSxFQUFKQSxJQUFOO0FBQVkyQixvQ0FBa0IsRUFBRUEsa0JBQWtCLEdBQUc7QUFBckQsaUJBQXZCLEM7OztrREFFRixJOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUdRakwsa0IsVUFBQUEsRTtBQUNmO2tDQUMrQixLQUFLSSxJQUFMLENBQVUrRixJQUFWLENBQWU7QUFBQSxzQkFBT3RILEdBQVAsVUFBR21CLEVBQUg7QUFBQSx5QkFBaUJuQixHQUFHLEtBQUttQixFQUF6QjtBQUFBLGlCQUFmLEMsRUFBUjFDLEcsbUJBQWY4TixhOzt1QkFDb0J2SSxPQUFPLENBQUNoRCxJQUFSLENBQWF3TCxNQUFiLENBQW9CO0FBQUUvTixxQkFBRyxFQUFIQSxHQUFGO0FBQU9nTyx3QkFBTSxFQUFFLEtBQWY7QUFBc0JDLHdCQUFNLEVBQUU7QUFBOUIsaUJBQXBCLEM7Ozs7QUFBaEI5SyxxQix5QkFBSlQsRTtBQUNSLHFCQUFLZSxRQUFMLENBQWM7QUFBRWYsb0JBQUUsRUFBRkEsRUFBRjtBQUFNSSxzQkFBSSxFQUFFO0FBQUVVLDhCQUFVLEVBQUU7QUFBZDtBQUFaLGlCQUFkO0FBQ0EwSyx1QkFBTyxDQUFDbEMsSUFBUixDQUFhLFVBQWI7O3VCQUNNLEtBQUs2QixpQkFBTCxDQUF1QjtBQUFFbkwsb0JBQUUsRUFBRkEsRUFBRjtBQUFNc0osc0JBQUksRUFBRTtBQUFaLGlCQUF2QixDOzs7QUFDTmtDLHVCQUFPLENBQUNDLE9BQVIsQ0FBZ0IsVUFBaEI7Ozt1QkFFOEI1SSxPQUFPLENBQUNoRCxJQUFSLENBQWF3SSxHQUFiLENBQWlCNUgsS0FBakIsQzs7O0FBQXRCaUwsNkI7O29CQUNEQSxhQUFhLENBQUNKLE07Ozs7Ozt1QkFDWHpJLE9BQU8sQ0FBQ2hELElBQVIsQ0FBYThMLE1BQWIsQ0FBb0JsTCxLQUFwQixDOzs7Ozs7Ozs7QUFHUitLLHVCQUFPLENBQUNJLEdBQVI7Ozs7dUJBRUksS0FBS0Msb0JBQUwsQ0FBMEI3TCxFQUExQixDOzs7QUFDTixxQkFBS2UsUUFBTCxDQUFjO0FBQUVmLG9CQUFFLEVBQUZBLEVBQUY7QUFBTUksc0JBQUksRUFBRTtBQUFFVSw4QkFBVSxFQUFFLEtBQWQ7QUFBcUJtSyxzQ0FBa0IsRUFBRTtBQUF6QztBQUFaLGlCQUFkOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O3dOQW9CeUJqTCxFOzs7Ozs7QUFDbkI0Siw4QixHQUFpQixLQUFLQyxPQUFMLENBQWF6SixJQUFiLENBQWtCK0YsSUFBbEIsQ0FBdUI7QUFBQSxzQkFBR3RILEdBQUgsVUFBR0EsR0FBSDtBQUFBLHlCQUFhQSxHQUFHLEtBQUttQixFQUFyQjtBQUFBLGlCQUF2QixDOztxQkFDbkI0SixjOzs7OztBQUNJa0MsdUIsR0FBVSxJQUFJN0UsdURBQUosQ0FBa0IyQyxjQUFjLENBQUNDLE9BQWYsQ0FBdUJrQyxhQUF6QyxDOzt1QkFDYUQsT0FBTyxDQUFDRSxLQUFSLEU7OztBQUF2QnRFLDhCO0FBQ0E1SSxxQixHQUFRLEtBQUtnTCxNQUFMLENBQVk5SixFQUFaLEM7O0FBQ2Qsb0JBQUkwSCxjQUFjLElBQUksQ0FBQzVJLEtBQUssQ0FBQ0csU0FBN0IsRUFBd0M7QUFDdEMsdUJBQUs4QixRQUFMLENBQWM7QUFBRWYsc0JBQUUsRUFBRkEsRUFBRjtBQUFNSSx3QkFBSSxFQUFFLEtBQUtZLGVBQUwsQ0FBcUIsV0FBckI7QUFBWixtQkFBZDtBQUNEOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O3lOQUl1QmtCLE07Ozs7OztBQUNsQmtGLHNCLEdBQWlCbEYsTSxDQUFqQmtGLE0sRUFBUTdJLEksR0FBUzJELE0sQ0FBVDNELEksRUFDaEI7QUFDQTs7QUFDTXFMLDhCLEdBQWlCLEtBQUtDLE9BQUwsQ0FBYXpKLElBQWIsQ0FBa0IrRixJQUFsQixDQUF1QixVQUFDM0IsTUFBRCxFQUFZO0FBQUE7O0FBQ3hELHNCQUFNeUgsbUJBQW1CLEdBQUcsQ0FBQXpILE1BQU0sU0FBTixJQUFBQSxNQUFNLFdBQU4sK0JBQUFBLE1BQU0sQ0FBRXFGLE9BQVIsb0VBQWlCa0MsYUFBakIsS0FBa0MsRUFBOUQ7QUFDQSx5QkFBT0UsbUJBQW1CLENBQUN2RixJQUFwQixDQUF5QixrQkFBd0M7QUFBQSx3QkFBN0J3RixhQUE2QixVQUFyQzlFLE1BQXFDO0FBQUEsd0JBQWRwRixPQUFjLFVBQWRBLE9BQWM7O0FBQ3RFLHdCQUFJb0YsTUFBTSxDQUFDUixNQUFQLENBQWNzRixhQUFkLElBQStCLENBQUMsQ0FBcEMsRUFBdUM7QUFDckMsNkJBQU9sSyxPQUFPLENBQUMwRSxJQUFSLENBQWE7QUFBQSw0QkFBR2MsV0FBSCxVQUFHQSxXQUFIO0FBQUEsK0JBQXFCakosSUFBSSxDQUFDcUksTUFBTCxDQUFZWSxXQUFaLElBQTJCLENBQUMsQ0FBakQ7QUFBQSx1QkFBYixDQUFQO0FBQ0Q7O0FBQ0QsMkJBQU8sS0FBUDtBQUNELG1CQUxNLENBQVA7QUFNRCxpQkFSc0IsQzs7cUJBU25Cb0MsYzs7Ozs7QUFDVzVKLGtCLEdBQU80SixjLENBQVovSyxHO0FBQ0ZpTix1QixHQUFVLElBQUk3RSx1REFBSixDQUFrQjJDLGNBQWMsQ0FBQ0MsT0FBZixDQUF1QmtDLGFBQXpDLEM7O3VCQUNhRCxPQUFPLENBQUNFLEtBQVIsRTs7O0FBQXZCdEUsOEI7QUFDQTVJLHFCLEdBQVEsS0FBS2dMLE1BQUwsQ0FBWTlKLEVBQVosQzs7c0JBQ1ZsQixLQUFLLENBQUNHLFNBQU4sS0FBb0J5SSxjOzs7OztxQkFJbEJBLGM7Ozs7O0FBQ0YscUJBQUszRyxRQUFMLENBQWM7QUFBRWYsb0JBQUUsRUFBRkEsRUFBRjtBQUFNSSxzQkFBSSxFQUFFLEtBQUtZLGVBQUwsQ0FBcUIsV0FBckI7QUFBWixpQkFBZDtrREFDTyxJOzs7a0RBSU4sSzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztFQXZPNEIzQyxpRDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDTHZDOztJQUVNWixXO0FBQ0oseUJBQWM7QUFBQTs7QUFDWixTQUFLME8sY0FBTCxHQUFzQixFQUF0QjtBQUNEOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFFc0IvTCxvQiwyREFBTyxFO0FBQ3BCRCxrQixHQUE0QkMsSSxDQUE1QkQsRSxFQUFJRSxNLEdBQXdCRCxJLENBQXhCQyxNLEVBQVFILFcsR0FBZ0JFLEksQ0FBaEJGLFc7O3NCQUVoQkMsRUFBRSxLQUFLLE87Ozs7O0FBQ1QscUJBQUtpTSx1QkFBTCxDQUE2QmhNLElBQTdCOzs7O3NCQUtFRCxFQUFFLEtBQUssUzs7Ozs7QUFDSE0scUIsR0FBUVAsV0FBVyxLQUFJRyxNQUFKLGFBQUlBLE1BQUosc0NBQUlBLE1BQU0sQ0FBRVAsR0FBWixnREFBSSxZQUFhRSxFQUFqQixDOztBQUV6QixvQkFBSVMsS0FBSyxJQUFJLEtBQUswTCxjQUFsQixFQUFrQztBQUNoQ25GLDhCQUFZLENBQUMsS0FBS21GLGNBQUwsQ0FBb0IxTCxLQUFwQixDQUFELENBQVo7QUFDRDs7QUFFRCxxQkFBSzRMLHlCQUFMLGlDQUNLak0sSUFETDtBQUVFSyx1QkFBSyxFQUFMQTtBQUZGO0FBS002TCx1QixHQUFVeEYsVUFBVSxDQUFDLFlBQU07QUFDL0IsdUJBQUksQ0FBQ3VGLHlCQUFMLENBQStCO0FBQUU1TCx5QkFBSyxFQUFMQTtBQUFGLG1CQUEvQjs7QUFDQSx5QkFBTyxLQUFJLENBQUMwTCxjQUFMLENBQW9CMUwsS0FBcEIsQ0FBUDtBQUNELGlCQUh5QixFQUd2QixLQUh1QixDO0FBSzFCLHFCQUFLMEwsY0FBTCxDQUFvQjFMLEtBQXBCLElBQTZCNkwsT0FBN0I7Ozs7QUFLRmQsdUJBQU8sQ0FBQ2UsSUFBUiwrQ0FBb0RwTSxFQUFwRDs7Ozs7Ozs7Ozs7Ozs7Ozs7OzRDQUdzQkMsSSxFQUFNO0FBQzVCZ0IsWUFBTSxDQUFDQyxPQUFQLENBQWVrQyxXQUFmLENBQTJCO0FBQ3pCNUIsY0FBTSxFQUFFLHlCQURpQjtBQUV6QnZCLFlBQUksRUFBSkE7QUFGeUIsT0FBM0I7QUFJRDs7Ozs7Ozs7Ozs7O0FBRStCQSxvQiw4REFBTyxFO0FBQy9Cb00sNkIsR0FBZ0IsRTs7QUFFdEIsb0JBQUlDLEtBQUssQ0FBQ0MsT0FBTixDQUFjdE0sSUFBSSxDQUFDbEIsTUFBbkIsQ0FBSixFQUFnQztBQUM5QmtCLHNCQUFJLENBQUNsQixNQUFMLENBQVlJLE9BQVosQ0FBb0IsZ0JBQTRCO0FBQUEsd0JBQXpCRyxPQUF5QixRQUF6QkEsT0FBeUI7QUFBQSx3QkFBaEJDLFNBQWdCLFFBQWhCQSxTQUFnQjtBQUM5QzhNLGlDQUFhLENBQUNoTixJQUFkLENBQW1CbU4sd0RBQWEsQ0FBQ2xOLE9BQUQsQ0FBYixDQUF1QkMsU0FBdkIsQ0FBbkI7QUFDRCxtQkFGRDtBQUdEOztBQUVEMEIsc0JBQU0sQ0FBQ3ZCLElBQVAsQ0FBWTBELFdBQVosQ0FBd0JuRCxJQUFJLENBQUNLLEtBQTdCLEVBQW9DO0FBQ2xDa0Isd0JBQU0sRUFBRSwyQkFEMEI7QUFFbEN2QixzQkFBSSxrQ0FDQ0EsSUFERDtBQUVGb00saUNBQWEsRUFBYkE7QUFGRTtBQUY4QixpQkFBcEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQVVXL08sMEVBQWYsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDbkVBOztJQUVxQkosSTs7Ozs7QUFDbkIsc0JBQWtEO0FBQUE7O0FBQUEseUJBQXBDa0IsSUFBb0M7QUFBQSxRQUFwQ0EsSUFBb0MsMEJBQTdCLE1BQTZCO0FBQUEsUUFBckJqQixHQUFxQixRQUFyQkEsR0FBcUI7QUFBQSw0QkFBaEJrQixPQUFnQjtBQUFBLFFBQWhCQSxPQUFnQiw2QkFBTixFQUFNOztBQUFBOztBQUNoRCw4QkFBTTtBQUFFRCxVQUFJLEVBQUpBLElBQUY7QUFBUWpCLFNBQUcsRUFBSEEsR0FBUjtBQUFha0IsYUFBTyxFQUFQQTtBQUFiLEtBQU47QUFDQSxVQUFLNEIsSUFBTCxHQUFZLEVBQVo7QUFDQSxVQUFLNUIsT0FBTDtBQUNFQyxvQkFBYyxFQUFFLEtBQUssRUFBTCxHQUFVLElBRDVCO0FBQ2tDO0FBQ2hDb0oseUJBQW1CLEVBQUUsS0FBSztBQUY1QixPQUdLckosT0FITDtBQUhnRDtBQVFqRDs7OzswQkFFSzRCLEksRUFBTTtBQUNWO0FBQ0EsYUFBTztBQUNMSixVQUFFLEVBQUVJLElBQUksQ0FBQ0osRUFESjtBQUVMNE0sYUFBSyxFQUFFeE0sSUFBSSxDQUFDd00sS0FGUDtBQUdMQyxhQUFLLEVBQUV6TSxJQUFJLENBQUN5TSxLQUhQO0FBSUxDLGlCQUFTLEVBQUUxTSxJQUFJLENBQUMyTSxVQUpYO0FBS0xDLGdCQUFRLEVBQUU1TSxJQUFJLENBQUM2TSxTQUxWO0FBTUxDLHNCQUFjLEVBQUU5TSxJQUFJLENBQUMrTSxnQkFOaEI7QUFPTEMsc0JBQWMsRUFBRWhOLElBQUksQ0FBQ2lOLGVBUGhCO0FBUUxDLHNCQUFjLEVBQUVsTixJQUFJLENBQUNtTixlQVJoQjtBQVNMQyxrQkFBVSxFQUFFcE4sSUFBSSxDQUFDcU47QUFUWixPQUFQO0FBV0Q7Ozs7RUF4QitCcFAsZ0Q7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0NDQWxDOztJQUNxQkwsZTs7Ozs7QUFDbkIsaUNBQW1DO0FBQUE7O0FBQUEsUUFBckJDLGlCQUFxQixRQUFyQkEsaUJBQXFCOztBQUFBOztBQUNqQztBQUNBLFVBQUt5UCxrQkFBTCxHQUEwQixFQUExQjtBQUNBLFVBQUt6UCxpQkFBTCxHQUF5QkEsaUJBQXpCO0FBQ0EsVUFBSzBQLGVBQUwsR0FBdUIsQ0FBQyxXQUFELEVBQWMsTUFBZCxDQUF2QjtBQUppQztBQUtsQzs7Ozs7Ozs7Ozs7QUFFYzlPLG1CLFNBQUFBLEcsRUFBS0csTSxTQUFBQSxNO0FBQ2xCLHFCQUFLME8sa0JBQUwsQ0FBd0I3TyxHQUF4QixJQUErQixLQUFLNk8sa0JBQUwsQ0FBd0I3TyxHQUF4QixLQUFnQyxFQUEvRDtBQUNBTyxzQkFBTSxDQUFDd08sTUFBUCxDQUFjLEtBQUtGLGtCQUFMLENBQXdCN08sR0FBeEIsQ0FBZCxFQUE0Q0csTUFBNUM7O3VCQUNxQixLQUFLNk8saUJBQUwsQ0FBdUI7QUFBRWhQLHFCQUFHLEVBQUhBO0FBQUYsaUJBQXZCLEM7OztBQUFmSyxzQjtBQUNOLHFCQUFLcUwsSUFBTCxDQUFVLFFBQVYsRUFBb0I7QUFBRTFMLHFCQUFHLEVBQUhBLEdBQUY7QUFBT0ssd0JBQU0sRUFBTkE7QUFBUCxpQkFBcEI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7dU5BR3dCTCxHOzs7Ozs7O3VCQUNILEtBQUtaLGlCQUFMLENBQXVCO0FBQUUrQixvQkFBRSxFQUFFbkIsR0FBTjtBQUFXNEYsNEJBQVUsRUFBRTtBQUF2QixpQkFBdkIsQzs7Ozs7Ozs7OzsrQkFBZ0UsRTs7O0FBQS9FRCxzQjtBQUNBc0osOEIsR0FBaUIxTyxNQUFNLENBQUMwTCxNQUFQLENBQWN0RyxNQUFkLEVBQXNCcEIsR0FBdEIsQ0FBMEI7QUFBQSxzQkFBR1ksS0FBSCxTQUFHQSxLQUFIO0FBQUEseUJBQWU1RSxNQUFNLENBQUNxSCxJQUFQLENBQVl6QyxLQUFaLENBQWY7QUFBQSxpQkFBMUIsRUFBNkQrSixJQUE3RCxFO2tEQUNoQixLQUFLSixlQUFMLENBQXFCSyxNQUFyQixDQUE0QkYsY0FBNUIsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBR2lCalAsbUIsU0FBQUEsRzs7dUJBQ0gsS0FBS29QLG1CQUFMLENBQXlCcFAsR0FBekIsQzs7O0FBQWZLLHNCO2tEQUNDRSxNQUFNLENBQUM4TyxXQUFQLENBQW1CaFAsTUFBTSxDQUFDa0UsR0FBUCxDQUFXLFVBQUM3RSxJQUFEO0FBQUE7O0FBQUEseUJBQVUsQ0FDN0NBLElBRDZDLEVBRTdDLENBQUMsMkJBQUMsTUFBSSxDQUFDbVAsa0JBQU4sb0ZBQUMsc0JBQTBCN08sR0FBMUIsQ0FBRCwyREFBQyx1QkFBaUNOLElBQWpDLENBQUQsQ0FGNEMsQ0FBVjtBQUFBLGlCQUFYLENBQW5CLEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQU1nQk0sbUIsU0FBQUEsRzs7dUJBQ0YsS0FBS29QLG1CQUFMLENBQXlCcFAsR0FBekIsQzs7O0FBQWZLLHNCO2tEQUNDQSxNQUFNLENBQUNxSSxLQUFQLENBQWEsVUFBQ2hKLElBQUQ7QUFBQSx5QkFBVSxNQUFJLENBQUNtUCxrQkFBTCxDQUF3QjdPLEdBQXhCLEVBQTZCTixJQUE3QixDQUFWO0FBQUEsaUJBQWIsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztFQS9Ca0NrTCw2Qzs7Ozs7Ozs7Ozs7Ozs7QUNIN0M7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQ0EsSUFBTTBFLFFBQVEsR0FBRyxxQ0FBakI7QUFDQSxJQUFNQyxNQUFNLEdBQUcsY0FBZjtBQUNBLElBQU03USxRQUFRLGFBQU00USxRQUFOLFFBQWQ7QUFDQSxJQUFNdlEsYUFBYSxHQUFHLGlCQUF0QjtBQUNBLElBQU1tTSxxQkFBcUIsR0FBRyxlQUE5QjtBQUNBLElBQU1zRSxVQUFVLEdBQUcsY0FBbkI7QUFDQSxJQUFNL1AsZ0JBQWdCLEdBQUcsWUFBekI7QUFDQSxJQUFNZ1EsaUJBQWlCLEdBQUc7QUFDeEJDLGFBQVcsRUFBRSwrQkFEVztBQUV4QkMsYUFBVyxFQUFFO0FBRlcsQ0FBMUI7QUFJQSxJQUFNakksb0JBQW9CLEdBQUcsQ0FDM0IsV0FEMkIsRUFFM0IsWUFGMkIsRUFHM0IsWUFIMkIsRUFJM0IsV0FKMkIsRUFLM0IsU0FMMkIsRUFNM0Isc0JBTjJCLENBQTdCO0FBUUEsSUFBTW9HLGFBQWEsR0FBRztBQUNwQnhOLGVBQWEsRUFBRTtBQUNiRixhQUFTLEVBQUUsMkNBREU7QUFFYndQLFFBQUksRUFBRSx3Q0FGTztBQUdiQyxZQUFRLEVBQUUsd0NBSEc7QUFJYkMsU0FBSyxFQUFFO0FBSk07QUFESyxDQUF0Qjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3BCQTtBQUNBOztJQUVNQyxHOzs7Ozs7O0FBQ0o7d0NBRW9CMUssTSxFQUFROUQsSSxFQUFNO0FBQ2hDO0FBQ0E7QUFDQTtBQUVBLGFBQU8sS0FBS3lPLElBQUwsMEJBQTRCM0ssTUFBNUIsR0FBc0M7QUFDM0M0SyxjQUFNLEVBQUUsTUFEbUM7QUFFM0NDLFlBQUksRUFBRUMsSUFBSSxDQUFDQyxTQUFMLENBQWU3TyxJQUFmO0FBRnFDLE9BQXRDLENBQVA7QUFJRDs7O3lCQUVJOE8sUSxFQUF3QjtBQUFBLFVBQWQxUSxPQUFjLHVFQUFKLEVBQUk7QUFDM0IsVUFBTWxCLEdBQUcsYUFBTTZRLG1EQUFOLFNBQWlCZSxRQUFqQixDQUFUO0FBRUEsYUFBT3ZHLEtBQUssQ0FBQ3JMLEdBQUQsRUFBTWtCLE9BQU4sQ0FBTCxDQUFvQnVELElBQXBCLENBQXlCLFVBQUM2RyxRQUFELEVBQWM7QUFDNUM7QUFDQTtBQUVBLFlBQUksQ0FBQ0EsUUFBUSxDQUFDdUcsRUFBZCxFQUFrQjtBQUNoQixpQkFBT2pJLE9BQU8sQ0FBQ2tJLE1BQVIsQ0FDTCxJQUFJQyxLQUFKLHdDQUEwQ3pHLFFBQVEsQ0FBQzBHLE1BQW5ELEVBREssQ0FBUDtBQUdEOztBQUVELGVBQU8xRyxRQUFRLENBQUNDLElBQVQsRUFBUDtBQUNELE9BWE0sV0FXRSxVQUFDMEcsS0FBRCxFQUFXO0FBQ2xCO0FBRUE3TCxjQUFNLENBQUNDLEdBQVAsQ0FBV25HLFdBQVgsQ0FBdUJ5QyxnQkFBdkIsQ0FBd0M7QUFDdENFLFlBQUUsRUFBRSxPQURrQztBQUV0QztBQUNBb1AsZUFBSyxFQUFFO0FBQ0xoUixnQkFBSSxFQUFFZ1IsS0FBSyxDQUFDaFIsSUFEUDtBQUVMaUQsbUJBQU8sRUFBRStOLEtBQUssQ0FBQy9OO0FBRlY7QUFIK0IsU0FBeEM7QUFRRCxPQXRCTSxDQUFQO0FBdUJEOzs7Ozs7QUFHSCxJQUFNMkMsR0FBRyxHQUFHLElBQUl5SyxHQUFKLEVBQVo7QUFFZXpLLGtFQUFmLEUiLCJmaWxlIjoiYmcvYnVuZGxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gaW5zdGFsbCBhIEpTT05QIGNhbGxiYWNrIGZvciBjaHVuayBsb2FkaW5nXG4gXHRmdW5jdGlvbiB3ZWJwYWNrSnNvbnBDYWxsYmFjayhkYXRhKSB7XG4gXHRcdHZhciBjaHVua0lkcyA9IGRhdGFbMF07XG4gXHRcdHZhciBtb3JlTW9kdWxlcyA9IGRhdGFbMV07XG4gXHRcdHZhciBleGVjdXRlTW9kdWxlcyA9IGRhdGFbMl07XG5cbiBcdFx0Ly8gYWRkIFwibW9yZU1vZHVsZXNcIiB0byB0aGUgbW9kdWxlcyBvYmplY3QsXG4gXHRcdC8vIHRoZW4gZmxhZyBhbGwgXCJjaHVua0lkc1wiIGFzIGxvYWRlZCBhbmQgZmlyZSBjYWxsYmFja1xuIFx0XHR2YXIgbW9kdWxlSWQsIGNodW5rSWQsIGkgPSAwLCByZXNvbHZlcyA9IFtdO1xuIFx0XHRmb3IoO2kgPCBjaHVua0lkcy5sZW5ndGg7IGkrKykge1xuIFx0XHRcdGNodW5rSWQgPSBjaHVua0lkc1tpXTtcbiBcdFx0XHRpZihPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwoaW5zdGFsbGVkQ2h1bmtzLCBjaHVua0lkKSAmJiBpbnN0YWxsZWRDaHVua3NbY2h1bmtJZF0pIHtcbiBcdFx0XHRcdHJlc29sdmVzLnB1c2goaW5zdGFsbGVkQ2h1bmtzW2NodW5rSWRdWzBdKTtcbiBcdFx0XHR9XG4gXHRcdFx0aW5zdGFsbGVkQ2h1bmtzW2NodW5rSWRdID0gMDtcbiBcdFx0fVxuIFx0XHRmb3IobW9kdWxlSWQgaW4gbW9yZU1vZHVsZXMpIHtcbiBcdFx0XHRpZihPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwobW9yZU1vZHVsZXMsIG1vZHVsZUlkKSkge1xuIFx0XHRcdFx0bW9kdWxlc1ttb2R1bGVJZF0gPSBtb3JlTW9kdWxlc1ttb2R1bGVJZF07XG4gXHRcdFx0fVxuIFx0XHR9XG4gXHRcdGlmKHBhcmVudEpzb25wRnVuY3Rpb24pIHBhcmVudEpzb25wRnVuY3Rpb24oZGF0YSk7XG5cbiBcdFx0d2hpbGUocmVzb2x2ZXMubGVuZ3RoKSB7XG4gXHRcdFx0cmVzb2x2ZXMuc2hpZnQoKSgpO1xuIFx0XHR9XG5cbiBcdFx0Ly8gYWRkIGVudHJ5IG1vZHVsZXMgZnJvbSBsb2FkZWQgY2h1bmsgdG8gZGVmZXJyZWQgbGlzdFxuIFx0XHRkZWZlcnJlZE1vZHVsZXMucHVzaC5hcHBseShkZWZlcnJlZE1vZHVsZXMsIGV4ZWN1dGVNb2R1bGVzIHx8IFtdKTtcblxuIFx0XHQvLyBydW4gZGVmZXJyZWQgbW9kdWxlcyB3aGVuIGFsbCBjaHVua3MgcmVhZHlcbiBcdFx0cmV0dXJuIGNoZWNrRGVmZXJyZWRNb2R1bGVzKCk7XG4gXHR9O1xuIFx0ZnVuY3Rpb24gY2hlY2tEZWZlcnJlZE1vZHVsZXMoKSB7XG4gXHRcdHZhciByZXN1bHQ7XG4gXHRcdGZvcih2YXIgaSA9IDA7IGkgPCBkZWZlcnJlZE1vZHVsZXMubGVuZ3RoOyBpKyspIHtcbiBcdFx0XHR2YXIgZGVmZXJyZWRNb2R1bGUgPSBkZWZlcnJlZE1vZHVsZXNbaV07XG4gXHRcdFx0dmFyIGZ1bGZpbGxlZCA9IHRydWU7XG4gXHRcdFx0Zm9yKHZhciBqID0gMTsgaiA8IGRlZmVycmVkTW9kdWxlLmxlbmd0aDsgaisrKSB7XG4gXHRcdFx0XHR2YXIgZGVwSWQgPSBkZWZlcnJlZE1vZHVsZVtqXTtcbiBcdFx0XHRcdGlmKGluc3RhbGxlZENodW5rc1tkZXBJZF0gIT09IDApIGZ1bGZpbGxlZCA9IGZhbHNlO1xuIFx0XHRcdH1cbiBcdFx0XHRpZihmdWxmaWxsZWQpIHtcbiBcdFx0XHRcdGRlZmVycmVkTW9kdWxlcy5zcGxpY2UoaS0tLCAxKTtcbiBcdFx0XHRcdHJlc3VsdCA9IF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gZGVmZXJyZWRNb2R1bGVbMF0pO1xuIFx0XHRcdH1cbiBcdFx0fVxuXG4gXHRcdHJldHVybiByZXN1bHQ7XG4gXHR9XG5cbiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIG9iamVjdCB0byBzdG9yZSBsb2FkZWQgYW5kIGxvYWRpbmcgY2h1bmtzXG4gXHQvLyB1bmRlZmluZWQgPSBjaHVuayBub3QgbG9hZGVkLCBudWxsID0gY2h1bmsgcHJlbG9hZGVkL3ByZWZldGNoZWRcbiBcdC8vIFByb21pc2UgPSBjaHVuayBsb2FkaW5nLCAwID0gY2h1bmsgbG9hZGVkXG4gXHR2YXIgaW5zdGFsbGVkQ2h1bmtzID0ge1xuIFx0XHRcImJnXCI6IDBcbiBcdH07XG5cbiBcdHZhciBkZWZlcnJlZE1vZHVsZXMgPSBbXTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7IGVudW1lcmFibGU6IHRydWUsIGdldDogZ2V0dGVyIH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSBmdW5jdGlvbihleHBvcnRzKSB7XG4gXHRcdGlmKHR5cGVvZiBTeW1ib2wgIT09ICd1bmRlZmluZWQnICYmIFN5bWJvbC50b1N0cmluZ1RhZykge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBTeW1ib2wudG9TdHJpbmdUYWcsIHsgdmFsdWU6ICdNb2R1bGUnIH0pO1xuIFx0XHR9XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG4gXHR9O1xuXG4gXHQvLyBjcmVhdGUgYSBmYWtlIG5hbWVzcGFjZSBvYmplY3RcbiBcdC8vIG1vZGUgJiAxOiB2YWx1ZSBpcyBhIG1vZHVsZSBpZCwgcmVxdWlyZSBpdFxuIFx0Ly8gbW9kZSAmIDI6IG1lcmdlIGFsbCBwcm9wZXJ0aWVzIG9mIHZhbHVlIGludG8gdGhlIG5zXG4gXHQvLyBtb2RlICYgNDogcmV0dXJuIHZhbHVlIHdoZW4gYWxyZWFkeSBucyBvYmplY3RcbiBcdC8vIG1vZGUgJiA4fDE6IGJlaGF2ZSBsaWtlIHJlcXVpcmVcbiBcdF9fd2VicGFja19yZXF1aXJlX18udCA9IGZ1bmN0aW9uKHZhbHVlLCBtb2RlKSB7XG4gXHRcdGlmKG1vZGUgJiAxKSB2YWx1ZSA9IF9fd2VicGFja19yZXF1aXJlX18odmFsdWUpO1xuIFx0XHRpZihtb2RlICYgOCkgcmV0dXJuIHZhbHVlO1xuIFx0XHRpZigobW9kZSAmIDQpICYmIHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcgJiYgdmFsdWUgJiYgdmFsdWUuX19lc01vZHVsZSkgcmV0dXJuIHZhbHVlO1xuIFx0XHR2YXIgbnMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIobnMpO1xuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkobnMsICdkZWZhdWx0JywgeyBlbnVtZXJhYmxlOiB0cnVlLCB2YWx1ZTogdmFsdWUgfSk7XG4gXHRcdGlmKG1vZGUgJiAyICYmIHR5cGVvZiB2YWx1ZSAhPSAnc3RyaW5nJykgZm9yKHZhciBrZXkgaW4gdmFsdWUpIF9fd2VicGFja19yZXF1aXJlX18uZChucywga2V5LCBmdW5jdGlvbihrZXkpIHsgcmV0dXJuIHZhbHVlW2tleV07IH0uYmluZChudWxsLCBrZXkpKTtcbiBcdFx0cmV0dXJuIG5zO1xuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuIFx0dmFyIGpzb25wQXJyYXkgPSB3aW5kb3dbXCJ3ZWJwYWNrSnNvbnBcIl0gPSB3aW5kb3dbXCJ3ZWJwYWNrSnNvbnBcIl0gfHwgW107XG4gXHR2YXIgb2xkSnNvbnBGdW5jdGlvbiA9IGpzb25wQXJyYXkucHVzaC5iaW5kKGpzb25wQXJyYXkpO1xuIFx0anNvbnBBcnJheS5wdXNoID0gd2VicGFja0pzb25wQ2FsbGJhY2s7XG4gXHRqc29ucEFycmF5ID0ganNvbnBBcnJheS5zbGljZSgpO1xuIFx0Zm9yKHZhciBpID0gMDsgaSA8IGpzb25wQXJyYXkubGVuZ3RoOyBpKyspIHdlYnBhY2tKc29ucENhbGxiYWNrKGpzb25wQXJyYXlbaV0pO1xuIFx0dmFyIHBhcmVudEpzb25wRnVuY3Rpb24gPSBvbGRKc29ucEZ1bmN0aW9uO1xuXG5cbiBcdC8vIGFkZCBlbnRyeSBtb2R1bGUgdG8gZGVmZXJyZWQgbGlzdFxuIFx0ZGVmZXJyZWRNb2R1bGVzLnB1c2goW1wiLi9zb3VyY2UvYmcvYXBwLmpzXCIsXCJ2ZW5kb3JzXCJdKTtcbiBcdC8vIHJ1biBkZWZlcnJlZCBtb2R1bGVzIHdoZW4gcmVhZHlcbiBcdHJldHVybiBjaGVja0RlZmVycmVkTW9kdWxlcygpO1xuIiwidmFyIHN1cGVyUHJvcEJhc2UgPSByZXF1aXJlKFwiLi9zdXBlclByb3BCYXNlXCIpO1xuXG5mdW5jdGlvbiBfZ2V0KHRhcmdldCwgcHJvcGVydHksIHJlY2VpdmVyKSB7XG4gIGlmICh0eXBlb2YgUmVmbGVjdCAhPT0gXCJ1bmRlZmluZWRcIiAmJiBSZWZsZWN0LmdldCkge1xuICAgIG1vZHVsZS5leHBvcnRzID0gX2dldCA9IFJlZmxlY3QuZ2V0O1xuICB9IGVsc2Uge1xuICAgIG1vZHVsZS5leHBvcnRzID0gX2dldCA9IGZ1bmN0aW9uIF9nZXQodGFyZ2V0LCBwcm9wZXJ0eSwgcmVjZWl2ZXIpIHtcbiAgICAgIHZhciBiYXNlID0gc3VwZXJQcm9wQmFzZSh0YXJnZXQsIHByb3BlcnR5KTtcbiAgICAgIGlmICghYmFzZSkgcmV0dXJuO1xuICAgICAgdmFyIGRlc2MgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9yKGJhc2UsIHByb3BlcnR5KTtcblxuICAgICAgaWYgKGRlc2MuZ2V0KSB7XG4gICAgICAgIHJldHVybiBkZXNjLmdldC5jYWxsKHJlY2VpdmVyKTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIGRlc2MudmFsdWU7XG4gICAgfTtcbiAgfVxuXG4gIHJldHVybiBfZ2V0KHRhcmdldCwgcHJvcGVydHksIHJlY2VpdmVyIHx8IHRhcmdldCk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gX2dldDsiLCJ2YXIgb2JqZWN0V2l0aG91dFByb3BlcnRpZXNMb29zZSA9IHJlcXVpcmUoXCIuL29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzTG9vc2VcIik7XG5cbmZ1bmN0aW9uIF9vYmplY3RXaXRob3V0UHJvcGVydGllcyhzb3VyY2UsIGV4Y2x1ZGVkKSB7XG4gIGlmIChzb3VyY2UgPT0gbnVsbCkgcmV0dXJuIHt9O1xuICB2YXIgdGFyZ2V0ID0gb2JqZWN0V2l0aG91dFByb3BlcnRpZXNMb29zZShzb3VyY2UsIGV4Y2x1ZGVkKTtcbiAgdmFyIGtleSwgaTtcblxuICBpZiAoT2JqZWN0LmdldE93blByb3BlcnR5U3ltYm9scykge1xuICAgIHZhciBzb3VyY2VTeW1ib2xLZXlzID0gT2JqZWN0LmdldE93blByb3BlcnR5U3ltYm9scyhzb3VyY2UpO1xuXG4gICAgZm9yIChpID0gMDsgaSA8IHNvdXJjZVN5bWJvbEtleXMubGVuZ3RoOyBpKyspIHtcbiAgICAgIGtleSA9IHNvdXJjZVN5bWJvbEtleXNbaV07XG4gICAgICBpZiAoZXhjbHVkZWQuaW5kZXhPZihrZXkpID49IDApIGNvbnRpbnVlO1xuICAgICAgaWYgKCFPYmplY3QucHJvdG90eXBlLnByb3BlcnR5SXNFbnVtZXJhYmxlLmNhbGwoc291cmNlLCBrZXkpKSBjb250aW51ZTtcbiAgICAgIHRhcmdldFtrZXldID0gc291cmNlW2tleV07XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIHRhcmdldDtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBfb2JqZWN0V2l0aG91dFByb3BlcnRpZXM7IiwiZnVuY3Rpb24gX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzTG9vc2Uoc291cmNlLCBleGNsdWRlZCkge1xuICBpZiAoc291cmNlID09IG51bGwpIHJldHVybiB7fTtcbiAgdmFyIHRhcmdldCA9IHt9O1xuICB2YXIgc291cmNlS2V5cyA9IE9iamVjdC5rZXlzKHNvdXJjZSk7XG4gIHZhciBrZXksIGk7XG5cbiAgZm9yIChpID0gMDsgaSA8IHNvdXJjZUtleXMubGVuZ3RoOyBpKyspIHtcbiAgICBrZXkgPSBzb3VyY2VLZXlzW2ldO1xuICAgIGlmIChleGNsdWRlZC5pbmRleE9mKGtleSkgPj0gMCkgY29udGludWU7XG4gICAgdGFyZ2V0W2tleV0gPSBzb3VyY2Vba2V5XTtcbiAgfVxuXG4gIHJldHVybiB0YXJnZXQ7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gX29iamVjdFdpdGhvdXRQcm9wZXJ0aWVzTG9vc2U7IiwidmFyIGdldFByb3RvdHlwZU9mID0gcmVxdWlyZShcIi4vZ2V0UHJvdG90eXBlT2ZcIik7XG5cbmZ1bmN0aW9uIF9zdXBlclByb3BCYXNlKG9iamVjdCwgcHJvcGVydHkpIHtcbiAgd2hpbGUgKCFPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSkpIHtcbiAgICBvYmplY3QgPSBnZXRQcm90b3R5cGVPZihvYmplY3QpO1xuICAgIGlmIChvYmplY3QgPT09IG51bGwpIGJyZWFrO1xuICB9XG5cbiAgcmV0dXJuIG9iamVjdDtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBfc3VwZXJQcm9wQmFzZTsiLCIvKipcbiAqIENvbnZlcnQgYXJyYXkgb2YgMTYgYnl0ZSB2YWx1ZXMgdG8gVVVJRCBzdHJpbmcgZm9ybWF0IG9mIHRoZSBmb3JtOlxuICogWFhYWFhYWFgtWFhYWC1YWFhYLVhYWFgtWFhYWFhYWFhYWFhYXG4gKi9cbnZhciBieXRlVG9IZXggPSBbXTtcblxuZm9yICh2YXIgaSA9IDA7IGkgPCAyNTY7ICsraSkge1xuICBieXRlVG9IZXhbaV0gPSAoaSArIDB4MTAwKS50b1N0cmluZygxNikuc3Vic3RyKDEpO1xufVxuXG5mdW5jdGlvbiBieXRlc1RvVXVpZChidWYsIG9mZnNldCkge1xuICB2YXIgaSA9IG9mZnNldCB8fCAwO1xuICB2YXIgYnRoID0gYnl0ZVRvSGV4OyAvLyBqb2luIHVzZWQgdG8gZml4IG1lbW9yeSBpc3N1ZSBjYXVzZWQgYnkgY29uY2F0ZW5hdGlvbjogaHR0cHM6Ly9idWdzLmNocm9taXVtLm9yZy9wL3Y4L2lzc3Vlcy9kZXRhaWw/aWQ9MzE3NSNjNFxuXG4gIHJldHVybiBbYnRoW2J1ZltpKytdXSwgYnRoW2J1ZltpKytdXSwgYnRoW2J1ZltpKytdXSwgYnRoW2J1ZltpKytdXSwgJy0nLCBidGhbYnVmW2krK11dLCBidGhbYnVmW2krK11dLCAnLScsIGJ0aFtidWZbaSsrXV0sIGJ0aFtidWZbaSsrXV0sICctJywgYnRoW2J1ZltpKytdXSwgYnRoW2J1ZltpKytdXSwgJy0nLCBidGhbYnVmW2krK11dLCBidGhbYnVmW2krK11dLCBidGhbYnVmW2krK11dLCBidGhbYnVmW2krK11dLCBidGhbYnVmW2krK11dLCBidGhbYnVmW2krK11dXS5qb2luKCcnKTtcbn1cblxuZXhwb3J0IGRlZmF1bHQgYnl0ZXNUb1V1aWQ7IiwiZXhwb3J0IHsgZGVmYXVsdCBhcyB2MSB9IGZyb20gJy4vdjEuanMnO1xuZXhwb3J0IHsgZGVmYXVsdCBhcyB2MyB9IGZyb20gJy4vdjMuanMnO1xuZXhwb3J0IHsgZGVmYXVsdCBhcyB2NCB9IGZyb20gJy4vdjQuanMnO1xuZXhwb3J0IHsgZGVmYXVsdCBhcyB2NSB9IGZyb20gJy4vdjUuanMnOyIsIi8qXG4gKiBCcm93c2VyLWNvbXBhdGlibGUgSmF2YVNjcmlwdCBNRDVcbiAqXG4gKiBNb2RpZmljYXRpb24gb2YgSmF2YVNjcmlwdCBNRDVcbiAqIGh0dHBzOi8vZ2l0aHViLmNvbS9ibHVlaW1wL0phdmFTY3JpcHQtTUQ1XG4gKlxuICogQ29weXJpZ2h0IDIwMTEsIFNlYmFzdGlhbiBUc2NoYW5cbiAqIGh0dHBzOi8vYmx1ZWltcC5uZXRcbiAqXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgTUlUIGxpY2Vuc2U6XG4gKiBodHRwczovL29wZW5zb3VyY2Uub3JnL2xpY2Vuc2VzL01JVFxuICpcbiAqIEJhc2VkIG9uXG4gKiBBIEphdmFTY3JpcHQgaW1wbGVtZW50YXRpb24gb2YgdGhlIFJTQSBEYXRhIFNlY3VyaXR5LCBJbmMuIE1ENSBNZXNzYWdlXG4gKiBEaWdlc3QgQWxnb3JpdGhtLCBhcyBkZWZpbmVkIGluIFJGQyAxMzIxLlxuICogVmVyc2lvbiAyLjIgQ29weXJpZ2h0IChDKSBQYXVsIEpvaG5zdG9uIDE5OTkgLSAyMDA5XG4gKiBPdGhlciBjb250cmlidXRvcnM6IEdyZWcgSG9sdCwgQW5kcmV3IEtlcGVydCwgWWRuYXIsIExvc3RpbmV0XG4gKiBEaXN0cmlidXRlZCB1bmRlciB0aGUgQlNEIExpY2Vuc2VcbiAqIFNlZSBodHRwOi8vcGFqaG9tZS5vcmcudWsvY3J5cHQvbWQ1IGZvciBtb3JlIGluZm8uXG4gKi9cbmZ1bmN0aW9uIG1kNShieXRlcykge1xuICBpZiAodHlwZW9mIGJ5dGVzID09ICdzdHJpbmcnKSB7XG4gICAgdmFyIG1zZyA9IHVuZXNjYXBlKGVuY29kZVVSSUNvbXBvbmVudChieXRlcykpOyAvLyBVVEY4IGVzY2FwZVxuXG4gICAgYnl0ZXMgPSBuZXcgQXJyYXkobXNnLmxlbmd0aCk7XG5cbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IG1zZy5sZW5ndGg7IGkrKykge1xuICAgICAgYnl0ZXNbaV0gPSBtc2cuY2hhckNvZGVBdChpKTtcbiAgICB9XG4gIH1cblxuICByZXR1cm4gbWQ1VG9IZXhFbmNvZGVkQXJyYXkod29yZHNUb01kNShieXRlc1RvV29yZHMoYnl0ZXMpLCBieXRlcy5sZW5ndGggKiA4KSk7XG59XG4vKlxuICogQ29udmVydCBhbiBhcnJheSBvZiBsaXR0bGUtZW5kaWFuIHdvcmRzIHRvIGFuIGFycmF5IG9mIGJ5dGVzXG4gKi9cblxuXG5mdW5jdGlvbiBtZDVUb0hleEVuY29kZWRBcnJheShpbnB1dCkge1xuICB2YXIgaTtcbiAgdmFyIHg7XG4gIHZhciBvdXRwdXQgPSBbXTtcbiAgdmFyIGxlbmd0aDMyID0gaW5wdXQubGVuZ3RoICogMzI7XG4gIHZhciBoZXhUYWIgPSAnMDEyMzQ1Njc4OWFiY2RlZic7XG4gIHZhciBoZXg7XG5cbiAgZm9yIChpID0gMDsgaSA8IGxlbmd0aDMyOyBpICs9IDgpIHtcbiAgICB4ID0gaW5wdXRbaSA+PiA1XSA+Pj4gaSAlIDMyICYgMHhmZjtcbiAgICBoZXggPSBwYXJzZUludChoZXhUYWIuY2hhckF0KHggPj4+IDQgJiAweDBmKSArIGhleFRhYi5jaGFyQXQoeCAmIDB4MGYpLCAxNik7XG4gICAgb3V0cHV0LnB1c2goaGV4KTtcbiAgfVxuXG4gIHJldHVybiBvdXRwdXQ7XG59XG4vKlxuICogQ2FsY3VsYXRlIHRoZSBNRDUgb2YgYW4gYXJyYXkgb2YgbGl0dGxlLWVuZGlhbiB3b3JkcywgYW5kIGEgYml0IGxlbmd0aC5cbiAqL1xuXG5cbmZ1bmN0aW9uIHdvcmRzVG9NZDUoeCwgbGVuKSB7XG4gIC8qIGFwcGVuZCBwYWRkaW5nICovXG4gIHhbbGVuID4+IDVdIHw9IDB4ODAgPDwgbGVuICUgMzI7XG4gIHhbKGxlbiArIDY0ID4+PiA5IDw8IDQpICsgMTRdID0gbGVuO1xuICB2YXIgaTtcbiAgdmFyIG9sZGE7XG4gIHZhciBvbGRiO1xuICB2YXIgb2xkYztcbiAgdmFyIG9sZGQ7XG4gIHZhciBhID0gMTczMjU4NDE5MztcbiAgdmFyIGIgPSAtMjcxNzMzODc5O1xuICB2YXIgYyA9IC0xNzMyNTg0MTk0O1xuICB2YXIgZCA9IDI3MTczMzg3ODtcblxuICBmb3IgKGkgPSAwOyBpIDwgeC5sZW5ndGg7IGkgKz0gMTYpIHtcbiAgICBvbGRhID0gYTtcbiAgICBvbGRiID0gYjtcbiAgICBvbGRjID0gYztcbiAgICBvbGRkID0gZDtcbiAgICBhID0gbWQ1ZmYoYSwgYiwgYywgZCwgeFtpXSwgNywgLTY4MDg3NjkzNik7XG4gICAgZCA9IG1kNWZmKGQsIGEsIGIsIGMsIHhbaSArIDFdLCAxMiwgLTM4OTU2NDU4Nik7XG4gICAgYyA9IG1kNWZmKGMsIGQsIGEsIGIsIHhbaSArIDJdLCAxNywgNjA2MTA1ODE5KTtcbiAgICBiID0gbWQ1ZmYoYiwgYywgZCwgYSwgeFtpICsgM10sIDIyLCAtMTA0NDUyNTMzMCk7XG4gICAgYSA9IG1kNWZmKGEsIGIsIGMsIGQsIHhbaSArIDRdLCA3LCAtMTc2NDE4ODk3KTtcbiAgICBkID0gbWQ1ZmYoZCwgYSwgYiwgYywgeFtpICsgNV0sIDEyLCAxMjAwMDgwNDI2KTtcbiAgICBjID0gbWQ1ZmYoYywgZCwgYSwgYiwgeFtpICsgNl0sIDE3LCAtMTQ3MzIzMTM0MSk7XG4gICAgYiA9IG1kNWZmKGIsIGMsIGQsIGEsIHhbaSArIDddLCAyMiwgLTQ1NzA1OTgzKTtcbiAgICBhID0gbWQ1ZmYoYSwgYiwgYywgZCwgeFtpICsgOF0sIDcsIDE3NzAwMzU0MTYpO1xuICAgIGQgPSBtZDVmZihkLCBhLCBiLCBjLCB4W2kgKyA5XSwgMTIsIC0xOTU4NDE0NDE3KTtcbiAgICBjID0gbWQ1ZmYoYywgZCwgYSwgYiwgeFtpICsgMTBdLCAxNywgLTQyMDYzKTtcbiAgICBiID0gbWQ1ZmYoYiwgYywgZCwgYSwgeFtpICsgMTFdLCAyMiwgLTE5OTA0MDQxNjIpO1xuICAgIGEgPSBtZDVmZihhLCBiLCBjLCBkLCB4W2kgKyAxMl0sIDcsIDE4MDQ2MDM2ODIpO1xuICAgIGQgPSBtZDVmZihkLCBhLCBiLCBjLCB4W2kgKyAxM10sIDEyLCAtNDAzNDExMDEpO1xuICAgIGMgPSBtZDVmZihjLCBkLCBhLCBiLCB4W2kgKyAxNF0sIDE3LCAtMTUwMjAwMjI5MCk7XG4gICAgYiA9IG1kNWZmKGIsIGMsIGQsIGEsIHhbaSArIDE1XSwgMjIsIDEyMzY1MzUzMjkpO1xuICAgIGEgPSBtZDVnZyhhLCBiLCBjLCBkLCB4W2kgKyAxXSwgNSwgLTE2NTc5NjUxMCk7XG4gICAgZCA9IG1kNWdnKGQsIGEsIGIsIGMsIHhbaSArIDZdLCA5LCAtMTA2OTUwMTYzMik7XG4gICAgYyA9IG1kNWdnKGMsIGQsIGEsIGIsIHhbaSArIDExXSwgMTQsIDY0MzcxNzcxMyk7XG4gICAgYiA9IG1kNWdnKGIsIGMsIGQsIGEsIHhbaV0sIDIwLCAtMzczODk3MzAyKTtcbiAgICBhID0gbWQ1Z2coYSwgYiwgYywgZCwgeFtpICsgNV0sIDUsIC03MDE1NTg2OTEpO1xuICAgIGQgPSBtZDVnZyhkLCBhLCBiLCBjLCB4W2kgKyAxMF0sIDksIDM4MDE2MDgzKTtcbiAgICBjID0gbWQ1Z2coYywgZCwgYSwgYiwgeFtpICsgMTVdLCAxNCwgLTY2MDQ3ODMzNSk7XG4gICAgYiA9IG1kNWdnKGIsIGMsIGQsIGEsIHhbaSArIDRdLCAyMCwgLTQwNTUzNzg0OCk7XG4gICAgYSA9IG1kNWdnKGEsIGIsIGMsIGQsIHhbaSArIDldLCA1LCA1Njg0NDY0MzgpO1xuICAgIGQgPSBtZDVnZyhkLCBhLCBiLCBjLCB4W2kgKyAxNF0sIDksIC0xMDE5ODAzNjkwKTtcbiAgICBjID0gbWQ1Z2coYywgZCwgYSwgYiwgeFtpICsgM10sIDE0LCAtMTg3MzYzOTYxKTtcbiAgICBiID0gbWQ1Z2coYiwgYywgZCwgYSwgeFtpICsgOF0sIDIwLCAxMTYzNTMxNTAxKTtcbiAgICBhID0gbWQ1Z2coYSwgYiwgYywgZCwgeFtpICsgMTNdLCA1LCAtMTQ0NDY4MTQ2Nyk7XG4gICAgZCA9IG1kNWdnKGQsIGEsIGIsIGMsIHhbaSArIDJdLCA5LCAtNTE0MDM3ODQpO1xuICAgIGMgPSBtZDVnZyhjLCBkLCBhLCBiLCB4W2kgKyA3XSwgMTQsIDE3MzUzMjg0NzMpO1xuICAgIGIgPSBtZDVnZyhiLCBjLCBkLCBhLCB4W2kgKyAxMl0sIDIwLCAtMTkyNjYwNzczNCk7XG4gICAgYSA9IG1kNWhoKGEsIGIsIGMsIGQsIHhbaSArIDVdLCA0LCAtMzc4NTU4KTtcbiAgICBkID0gbWQ1aGgoZCwgYSwgYiwgYywgeFtpICsgOF0sIDExLCAtMjAyMjU3NDQ2Myk7XG4gICAgYyA9IG1kNWhoKGMsIGQsIGEsIGIsIHhbaSArIDExXSwgMTYsIDE4MzkwMzA1NjIpO1xuICAgIGIgPSBtZDVoaChiLCBjLCBkLCBhLCB4W2kgKyAxNF0sIDIzLCAtMzUzMDk1NTYpO1xuICAgIGEgPSBtZDVoaChhLCBiLCBjLCBkLCB4W2kgKyAxXSwgNCwgLTE1MzA5OTIwNjApO1xuICAgIGQgPSBtZDVoaChkLCBhLCBiLCBjLCB4W2kgKyA0XSwgMTEsIDEyNzI4OTMzNTMpO1xuICAgIGMgPSBtZDVoaChjLCBkLCBhLCBiLCB4W2kgKyA3XSwgMTYsIC0xNTU0OTc2MzIpO1xuICAgIGIgPSBtZDVoaChiLCBjLCBkLCBhLCB4W2kgKyAxMF0sIDIzLCAtMTA5NDczMDY0MCk7XG4gICAgYSA9IG1kNWhoKGEsIGIsIGMsIGQsIHhbaSArIDEzXSwgNCwgNjgxMjc5MTc0KTtcbiAgICBkID0gbWQ1aGgoZCwgYSwgYiwgYywgeFtpXSwgMTEsIC0zNTg1MzcyMjIpO1xuICAgIGMgPSBtZDVoaChjLCBkLCBhLCBiLCB4W2kgKyAzXSwgMTYsIC03MjI1MjE5NzkpO1xuICAgIGIgPSBtZDVoaChiLCBjLCBkLCBhLCB4W2kgKyA2XSwgMjMsIDc2MDI5MTg5KTtcbiAgICBhID0gbWQ1aGgoYSwgYiwgYywgZCwgeFtpICsgOV0sIDQsIC02NDAzNjQ0ODcpO1xuICAgIGQgPSBtZDVoaChkLCBhLCBiLCBjLCB4W2kgKyAxMl0sIDExLCAtNDIxODE1ODM1KTtcbiAgICBjID0gbWQ1aGgoYywgZCwgYSwgYiwgeFtpICsgMTVdLCAxNiwgNTMwNzQyNTIwKTtcbiAgICBiID0gbWQ1aGgoYiwgYywgZCwgYSwgeFtpICsgMl0sIDIzLCAtOTk1MzM4NjUxKTtcbiAgICBhID0gbWQ1aWkoYSwgYiwgYywgZCwgeFtpXSwgNiwgLTE5ODYzMDg0NCk7XG4gICAgZCA9IG1kNWlpKGQsIGEsIGIsIGMsIHhbaSArIDddLCAxMCwgMTEyNjg5MTQxNSk7XG4gICAgYyA9IG1kNWlpKGMsIGQsIGEsIGIsIHhbaSArIDE0XSwgMTUsIC0xNDE2MzU0OTA1KTtcbiAgICBiID0gbWQ1aWkoYiwgYywgZCwgYSwgeFtpICsgNV0sIDIxLCAtNTc0MzQwNTUpO1xuICAgIGEgPSBtZDVpaShhLCBiLCBjLCBkLCB4W2kgKyAxMl0sIDYsIDE3MDA0ODU1NzEpO1xuICAgIGQgPSBtZDVpaShkLCBhLCBiLCBjLCB4W2kgKyAzXSwgMTAsIC0xODk0OTg2NjA2KTtcbiAgICBjID0gbWQ1aWkoYywgZCwgYSwgYiwgeFtpICsgMTBdLCAxNSwgLTEwNTE1MjMpO1xuICAgIGIgPSBtZDVpaShiLCBjLCBkLCBhLCB4W2kgKyAxXSwgMjEsIC0yMDU0OTIyNzk5KTtcbiAgICBhID0gbWQ1aWkoYSwgYiwgYywgZCwgeFtpICsgOF0sIDYsIDE4NzMzMTMzNTkpO1xuICAgIGQgPSBtZDVpaShkLCBhLCBiLCBjLCB4W2kgKyAxNV0sIDEwLCAtMzA2MTE3NDQpO1xuICAgIGMgPSBtZDVpaShjLCBkLCBhLCBiLCB4W2kgKyA2XSwgMTUsIC0xNTYwMTk4MzgwKTtcbiAgICBiID0gbWQ1aWkoYiwgYywgZCwgYSwgeFtpICsgMTNdLCAyMSwgMTMwOTE1MTY0OSk7XG4gICAgYSA9IG1kNWlpKGEsIGIsIGMsIGQsIHhbaSArIDRdLCA2LCAtMTQ1NTIzMDcwKTtcbiAgICBkID0gbWQ1aWkoZCwgYSwgYiwgYywgeFtpICsgMTFdLCAxMCwgLTExMjAyMTAzNzkpO1xuICAgIGMgPSBtZDVpaShjLCBkLCBhLCBiLCB4W2kgKyAyXSwgMTUsIDcxODc4NzI1OSk7XG4gICAgYiA9IG1kNWlpKGIsIGMsIGQsIGEsIHhbaSArIDldLCAyMSwgLTM0MzQ4NTU1MSk7XG4gICAgYSA9IHNhZmVBZGQoYSwgb2xkYSk7XG4gICAgYiA9IHNhZmVBZGQoYiwgb2xkYik7XG4gICAgYyA9IHNhZmVBZGQoYywgb2xkYyk7XG4gICAgZCA9IHNhZmVBZGQoZCwgb2xkZCk7XG4gIH1cblxuICByZXR1cm4gW2EsIGIsIGMsIGRdO1xufVxuLypcbiAqIENvbnZlcnQgYW4gYXJyYXkgYnl0ZXMgdG8gYW4gYXJyYXkgb2YgbGl0dGxlLWVuZGlhbiB3b3Jkc1xuICogQ2hhcmFjdGVycyA+MjU1IGhhdmUgdGhlaXIgaGlnaC1ieXRlIHNpbGVudGx5IGlnbm9yZWQuXG4gKi9cblxuXG5mdW5jdGlvbiBieXRlc1RvV29yZHMoaW5wdXQpIHtcbiAgdmFyIGk7XG4gIHZhciBvdXRwdXQgPSBbXTtcbiAgb3V0cHV0WyhpbnB1dC5sZW5ndGggPj4gMikgLSAxXSA9IHVuZGVmaW5lZDtcblxuICBmb3IgKGkgPSAwOyBpIDwgb3V0cHV0Lmxlbmd0aDsgaSArPSAxKSB7XG4gICAgb3V0cHV0W2ldID0gMDtcbiAgfVxuXG4gIHZhciBsZW5ndGg4ID0gaW5wdXQubGVuZ3RoICogODtcblxuICBmb3IgKGkgPSAwOyBpIDwgbGVuZ3RoODsgaSArPSA4KSB7XG4gICAgb3V0cHV0W2kgPj4gNV0gfD0gKGlucHV0W2kgLyA4XSAmIDB4ZmYpIDw8IGkgJSAzMjtcbiAgfVxuXG4gIHJldHVybiBvdXRwdXQ7XG59XG4vKlxuICogQWRkIGludGVnZXJzLCB3cmFwcGluZyBhdCAyXjMyLiBUaGlzIHVzZXMgMTYtYml0IG9wZXJhdGlvbnMgaW50ZXJuYWxseVxuICogdG8gd29yayBhcm91bmQgYnVncyBpbiBzb21lIEpTIGludGVycHJldGVycy5cbiAqL1xuXG5cbmZ1bmN0aW9uIHNhZmVBZGQoeCwgeSkge1xuICB2YXIgbHN3ID0gKHggJiAweGZmZmYpICsgKHkgJiAweGZmZmYpO1xuICB2YXIgbXN3ID0gKHggPj4gMTYpICsgKHkgPj4gMTYpICsgKGxzdyA+PiAxNik7XG4gIHJldHVybiBtc3cgPDwgMTYgfCBsc3cgJiAweGZmZmY7XG59XG4vKlxuICogQml0d2lzZSByb3RhdGUgYSAzMi1iaXQgbnVtYmVyIHRvIHRoZSBsZWZ0LlxuICovXG5cblxuZnVuY3Rpb24gYml0Um90YXRlTGVmdChudW0sIGNudCkge1xuICByZXR1cm4gbnVtIDw8IGNudCB8IG51bSA+Pj4gMzIgLSBjbnQ7XG59XG4vKlxuICogVGhlc2UgZnVuY3Rpb25zIGltcGxlbWVudCB0aGUgZm91ciBiYXNpYyBvcGVyYXRpb25zIHRoZSBhbGdvcml0aG0gdXNlcy5cbiAqL1xuXG5cbmZ1bmN0aW9uIG1kNWNtbihxLCBhLCBiLCB4LCBzLCB0KSB7XG4gIHJldHVybiBzYWZlQWRkKGJpdFJvdGF0ZUxlZnQoc2FmZUFkZChzYWZlQWRkKGEsIHEpLCBzYWZlQWRkKHgsIHQpKSwgcyksIGIpO1xufVxuXG5mdW5jdGlvbiBtZDVmZihhLCBiLCBjLCBkLCB4LCBzLCB0KSB7XG4gIHJldHVybiBtZDVjbW4oYiAmIGMgfCB+YiAmIGQsIGEsIGIsIHgsIHMsIHQpO1xufVxuXG5mdW5jdGlvbiBtZDVnZyhhLCBiLCBjLCBkLCB4LCBzLCB0KSB7XG4gIHJldHVybiBtZDVjbW4oYiAmIGQgfCBjICYgfmQsIGEsIGIsIHgsIHMsIHQpO1xufVxuXG5mdW5jdGlvbiBtZDVoaChhLCBiLCBjLCBkLCB4LCBzLCB0KSB7XG4gIHJldHVybiBtZDVjbW4oYiBeIGMgXiBkLCBhLCBiLCB4LCBzLCB0KTtcbn1cblxuZnVuY3Rpb24gbWQ1aWkoYSwgYiwgYywgZCwgeCwgcywgdCkge1xuICByZXR1cm4gbWQ1Y21uKGMgXiAoYiB8IH5kKSwgYSwgYiwgeCwgcywgdCk7XG59XG5cbmV4cG9ydCBkZWZhdWx0IG1kNTsiLCIvLyBVbmlxdWUgSUQgY3JlYXRpb24gcmVxdWlyZXMgYSBoaWdoIHF1YWxpdHkgcmFuZG9tICMgZ2VuZXJhdG9yLiBJbiB0aGUgYnJvd3NlciB3ZSB0aGVyZWZvcmVcbi8vIHJlcXVpcmUgdGhlIGNyeXB0byBBUEkgYW5kIGRvIG5vdCBzdXBwb3J0IGJ1aWx0LWluIGZhbGxiYWNrIHRvIGxvd2VyIHF1YWxpdHkgcmFuZG9tIG51bWJlclxuLy8gZ2VuZXJhdG9ycyAobGlrZSBNYXRoLnJhbmRvbSgpKS5cbi8vIGdldFJhbmRvbVZhbHVlcyBuZWVkcyB0byBiZSBpbnZva2VkIGluIGEgY29udGV4dCB3aGVyZSBcInRoaXNcIiBpcyBhIENyeXB0byBpbXBsZW1lbnRhdGlvbi4gQWxzbyxcbi8vIGZpbmQgdGhlIGNvbXBsZXRlIGltcGxlbWVudGF0aW9uIG9mIGNyeXB0byAobXNDcnlwdG8pIG9uIElFMTEuXG52YXIgZ2V0UmFuZG9tVmFsdWVzID0gdHlwZW9mIGNyeXB0byAhPSAndW5kZWZpbmVkJyAmJiBjcnlwdG8uZ2V0UmFuZG9tVmFsdWVzICYmIGNyeXB0by5nZXRSYW5kb21WYWx1ZXMuYmluZChjcnlwdG8pIHx8IHR5cGVvZiBtc0NyeXB0byAhPSAndW5kZWZpbmVkJyAmJiB0eXBlb2YgbXNDcnlwdG8uZ2V0UmFuZG9tVmFsdWVzID09ICdmdW5jdGlvbicgJiYgbXNDcnlwdG8uZ2V0UmFuZG9tVmFsdWVzLmJpbmQobXNDcnlwdG8pO1xudmFyIHJuZHM4ID0gbmV3IFVpbnQ4QXJyYXkoMTYpOyAvLyBlc2xpbnQtZGlzYWJsZS1saW5lIG5vLXVuZGVmXG5cbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIHJuZygpIHtcbiAgaWYgKCFnZXRSYW5kb21WYWx1ZXMpIHtcbiAgICB0aHJvdyBuZXcgRXJyb3IoJ2NyeXB0by5nZXRSYW5kb21WYWx1ZXMoKSBub3Qgc3VwcG9ydGVkLiBTZWUgaHR0cHM6Ly9naXRodWIuY29tL3V1aWRqcy91dWlkI2dldHJhbmRvbXZhbHVlcy1ub3Qtc3VwcG9ydGVkJyk7XG4gIH1cblxuICByZXR1cm4gZ2V0UmFuZG9tVmFsdWVzKHJuZHM4KTtcbn0iLCIvLyBBZGFwdGVkIGZyb20gQ2hyaXMgVmVuZXNzJyBTSEExIGNvZGUgYXRcbi8vIGh0dHA6Ly93d3cubW92YWJsZS10eXBlLmNvLnVrL3NjcmlwdHMvc2hhMS5odG1sXG5mdW5jdGlvbiBmKHMsIHgsIHksIHopIHtcbiAgc3dpdGNoIChzKSB7XG4gICAgY2FzZSAwOlxuICAgICAgcmV0dXJuIHggJiB5IF4gfnggJiB6O1xuXG4gICAgY2FzZSAxOlxuICAgICAgcmV0dXJuIHggXiB5IF4gejtcblxuICAgIGNhc2UgMjpcbiAgICAgIHJldHVybiB4ICYgeSBeIHggJiB6IF4geSAmIHo7XG5cbiAgICBjYXNlIDM6XG4gICAgICByZXR1cm4geCBeIHkgXiB6O1xuICB9XG59XG5cbmZ1bmN0aW9uIFJPVEwoeCwgbikge1xuICByZXR1cm4geCA8PCBuIHwgeCA+Pj4gMzIgLSBuO1xufVxuXG5mdW5jdGlvbiBzaGExKGJ5dGVzKSB7XG4gIHZhciBLID0gWzB4NWE4Mjc5OTksIDB4NmVkOWViYTEsIDB4OGYxYmJjZGMsIDB4Y2E2MmMxZDZdO1xuICB2YXIgSCA9IFsweDY3NDUyMzAxLCAweGVmY2RhYjg5LCAweDk4YmFkY2ZlLCAweDEwMzI1NDc2LCAweGMzZDJlMWYwXTtcblxuICBpZiAodHlwZW9mIGJ5dGVzID09ICdzdHJpbmcnKSB7XG4gICAgdmFyIG1zZyA9IHVuZXNjYXBlKGVuY29kZVVSSUNvbXBvbmVudChieXRlcykpOyAvLyBVVEY4IGVzY2FwZVxuXG4gICAgYnl0ZXMgPSBuZXcgQXJyYXkobXNnLmxlbmd0aCk7XG5cbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IG1zZy5sZW5ndGg7IGkrKykge1xuICAgICAgYnl0ZXNbaV0gPSBtc2cuY2hhckNvZGVBdChpKTtcbiAgICB9XG4gIH1cblxuICBieXRlcy5wdXNoKDB4ODApO1xuICB2YXIgbCA9IGJ5dGVzLmxlbmd0aCAvIDQgKyAyO1xuICB2YXIgTiA9IE1hdGguY2VpbChsIC8gMTYpO1xuICB2YXIgTSA9IG5ldyBBcnJheShOKTtcblxuICBmb3IgKHZhciBpID0gMDsgaSA8IE47IGkrKykge1xuICAgIE1baV0gPSBuZXcgQXJyYXkoMTYpO1xuXG4gICAgZm9yICh2YXIgaiA9IDA7IGogPCAxNjsgaisrKSB7XG4gICAgICBNW2ldW2pdID0gYnl0ZXNbaSAqIDY0ICsgaiAqIDRdIDw8IDI0IHwgYnl0ZXNbaSAqIDY0ICsgaiAqIDQgKyAxXSA8PCAxNiB8IGJ5dGVzW2kgKiA2NCArIGogKiA0ICsgMl0gPDwgOCB8IGJ5dGVzW2kgKiA2NCArIGogKiA0ICsgM107XG4gICAgfVxuICB9XG5cbiAgTVtOIC0gMV1bMTRdID0gKGJ5dGVzLmxlbmd0aCAtIDEpICogOCAvIE1hdGgucG93KDIsIDMyKTtcbiAgTVtOIC0gMV1bMTRdID0gTWF0aC5mbG9vcihNW04gLSAxXVsxNF0pO1xuICBNW04gLSAxXVsxNV0gPSAoYnl0ZXMubGVuZ3RoIC0gMSkgKiA4ICYgMHhmZmZmZmZmZjtcblxuICBmb3IgKHZhciBpID0gMDsgaSA8IE47IGkrKykge1xuICAgIHZhciBXID0gbmV3IEFycmF5KDgwKTtcblxuICAgIGZvciAodmFyIHQgPSAwOyB0IDwgMTY7IHQrKykge1xuICAgICAgV1t0XSA9IE1baV1bdF07XG4gICAgfVxuXG4gICAgZm9yICh2YXIgdCA9IDE2OyB0IDwgODA7IHQrKykge1xuICAgICAgV1t0XSA9IFJPVEwoV1t0IC0gM10gXiBXW3QgLSA4XSBeIFdbdCAtIDE0XSBeIFdbdCAtIDE2XSwgMSk7XG4gICAgfVxuXG4gICAgdmFyIGEgPSBIWzBdO1xuICAgIHZhciBiID0gSFsxXTtcbiAgICB2YXIgYyA9IEhbMl07XG4gICAgdmFyIGQgPSBIWzNdO1xuICAgIHZhciBlID0gSFs0XTtcblxuICAgIGZvciAodmFyIHQgPSAwOyB0IDwgODA7IHQrKykge1xuICAgICAgdmFyIHMgPSBNYXRoLmZsb29yKHQgLyAyMCk7XG4gICAgICB2YXIgVCA9IFJPVEwoYSwgNSkgKyBmKHMsIGIsIGMsIGQpICsgZSArIEtbc10gKyBXW3RdID4+PiAwO1xuICAgICAgZSA9IGQ7XG4gICAgICBkID0gYztcbiAgICAgIGMgPSBST1RMKGIsIDMwKSA+Pj4gMDtcbiAgICAgIGIgPSBhO1xuICAgICAgYSA9IFQ7XG4gICAgfVxuXG4gICAgSFswXSA9IEhbMF0gKyBhID4+PiAwO1xuICAgIEhbMV0gPSBIWzFdICsgYiA+Pj4gMDtcbiAgICBIWzJdID0gSFsyXSArIGMgPj4+IDA7XG4gICAgSFszXSA9IEhbM10gKyBkID4+PiAwO1xuICAgIEhbNF0gPSBIWzRdICsgZSA+Pj4gMDtcbiAgfVxuXG4gIHJldHVybiBbSFswXSA+PiAyNCAmIDB4ZmYsIEhbMF0gPj4gMTYgJiAweGZmLCBIWzBdID4+IDggJiAweGZmLCBIWzBdICYgMHhmZiwgSFsxXSA+PiAyNCAmIDB4ZmYsIEhbMV0gPj4gMTYgJiAweGZmLCBIWzFdID4+IDggJiAweGZmLCBIWzFdICYgMHhmZiwgSFsyXSA+PiAyNCAmIDB4ZmYsIEhbMl0gPj4gMTYgJiAweGZmLCBIWzJdID4+IDggJiAweGZmLCBIWzJdICYgMHhmZiwgSFszXSA+PiAyNCAmIDB4ZmYsIEhbM10gPj4gMTYgJiAweGZmLCBIWzNdID4+IDggJiAweGZmLCBIWzNdICYgMHhmZiwgSFs0XSA+PiAyNCAmIDB4ZmYsIEhbNF0gPj4gMTYgJiAweGZmLCBIWzRdID4+IDggJiAweGZmLCBIWzRdICYgMHhmZl07XG59XG5cbmV4cG9ydCBkZWZhdWx0IHNoYTE7IiwiaW1wb3J0IHJuZyBmcm9tICcuL3JuZy5qcyc7XG5pbXBvcnQgYnl0ZXNUb1V1aWQgZnJvbSAnLi9ieXRlc1RvVXVpZC5qcyc7IC8vICoqYHYxKClgIC0gR2VuZXJhdGUgdGltZS1iYXNlZCBVVUlEKipcbi8vXG4vLyBJbnNwaXJlZCBieSBodHRwczovL2dpdGh1Yi5jb20vTGlvc0svVVVJRC5qc1xuLy8gYW5kIGh0dHA6Ly9kb2NzLnB5dGhvbi5vcmcvbGlicmFyeS91dWlkLmh0bWxcblxudmFyIF9ub2RlSWQ7XG5cbnZhciBfY2xvY2tzZXE7IC8vIFByZXZpb3VzIHV1aWQgY3JlYXRpb24gdGltZVxuXG5cbnZhciBfbGFzdE1TZWNzID0gMDtcbnZhciBfbGFzdE5TZWNzID0gMDsgLy8gU2VlIGh0dHBzOi8vZ2l0aHViLmNvbS91dWlkanMvdXVpZCBmb3IgQVBJIGRldGFpbHNcblxuZnVuY3Rpb24gdjEob3B0aW9ucywgYnVmLCBvZmZzZXQpIHtcbiAgdmFyIGkgPSBidWYgJiYgb2Zmc2V0IHx8IDA7XG4gIHZhciBiID0gYnVmIHx8IFtdO1xuICBvcHRpb25zID0gb3B0aW9ucyB8fCB7fTtcbiAgdmFyIG5vZGUgPSBvcHRpb25zLm5vZGUgfHwgX25vZGVJZDtcbiAgdmFyIGNsb2Nrc2VxID0gb3B0aW9ucy5jbG9ja3NlcSAhPT0gdW5kZWZpbmVkID8gb3B0aW9ucy5jbG9ja3NlcSA6IF9jbG9ja3NlcTsgLy8gbm9kZSBhbmQgY2xvY2tzZXEgbmVlZCB0byBiZSBpbml0aWFsaXplZCB0byByYW5kb20gdmFsdWVzIGlmIHRoZXkncmUgbm90XG4gIC8vIHNwZWNpZmllZC4gIFdlIGRvIHRoaXMgbGF6aWx5IHRvIG1pbmltaXplIGlzc3VlcyByZWxhdGVkIHRvIGluc3VmZmljaWVudFxuICAvLyBzeXN0ZW0gZW50cm9weS4gIFNlZSAjMTg5XG5cbiAgaWYgKG5vZGUgPT0gbnVsbCB8fCBjbG9ja3NlcSA9PSBudWxsKSB7XG4gICAgdmFyIHNlZWRCeXRlcyA9IG9wdGlvbnMucmFuZG9tIHx8IChvcHRpb25zLnJuZyB8fCBybmcpKCk7XG5cbiAgICBpZiAobm9kZSA9PSBudWxsKSB7XG4gICAgICAvLyBQZXIgNC41LCBjcmVhdGUgYW5kIDQ4LWJpdCBub2RlIGlkLCAoNDcgcmFuZG9tIGJpdHMgKyBtdWx0aWNhc3QgYml0ID0gMSlcbiAgICAgIG5vZGUgPSBfbm9kZUlkID0gW3NlZWRCeXRlc1swXSB8IDB4MDEsIHNlZWRCeXRlc1sxXSwgc2VlZEJ5dGVzWzJdLCBzZWVkQnl0ZXNbM10sIHNlZWRCeXRlc1s0XSwgc2VlZEJ5dGVzWzVdXTtcbiAgICB9XG5cbiAgICBpZiAoY2xvY2tzZXEgPT0gbnVsbCkge1xuICAgICAgLy8gUGVyIDQuMi4yLCByYW5kb21pemUgKDE0IGJpdCkgY2xvY2tzZXFcbiAgICAgIGNsb2Nrc2VxID0gX2Nsb2Nrc2VxID0gKHNlZWRCeXRlc1s2XSA8PCA4IHwgc2VlZEJ5dGVzWzddKSAmIDB4M2ZmZjtcbiAgICB9XG4gIH0gLy8gVVVJRCB0aW1lc3RhbXBzIGFyZSAxMDAgbmFuby1zZWNvbmQgdW5pdHMgc2luY2UgdGhlIEdyZWdvcmlhbiBlcG9jaCxcbiAgLy8gKDE1ODItMTAtMTUgMDA6MDApLiAgSlNOdW1iZXJzIGFyZW4ndCBwcmVjaXNlIGVub3VnaCBmb3IgdGhpcywgc29cbiAgLy8gdGltZSBpcyBoYW5kbGVkIGludGVybmFsbHkgYXMgJ21zZWNzJyAoaW50ZWdlciBtaWxsaXNlY29uZHMpIGFuZCAnbnNlY3MnXG4gIC8vICgxMDAtbmFub3NlY29uZHMgb2Zmc2V0IGZyb20gbXNlY3MpIHNpbmNlIHVuaXggZXBvY2gsIDE5NzAtMDEtMDEgMDA6MDAuXG5cblxuICB2YXIgbXNlY3MgPSBvcHRpb25zLm1zZWNzICE9PSB1bmRlZmluZWQgPyBvcHRpb25zLm1zZWNzIDogbmV3IERhdGUoKS5nZXRUaW1lKCk7IC8vIFBlciA0LjIuMS4yLCB1c2UgY291bnQgb2YgdXVpZCdzIGdlbmVyYXRlZCBkdXJpbmcgdGhlIGN1cnJlbnQgY2xvY2tcbiAgLy8gY3ljbGUgdG8gc2ltdWxhdGUgaGlnaGVyIHJlc29sdXRpb24gY2xvY2tcblxuICB2YXIgbnNlY3MgPSBvcHRpb25zLm5zZWNzICE9PSB1bmRlZmluZWQgPyBvcHRpb25zLm5zZWNzIDogX2xhc3ROU2VjcyArIDE7IC8vIFRpbWUgc2luY2UgbGFzdCB1dWlkIGNyZWF0aW9uIChpbiBtc2VjcylcblxuICB2YXIgZHQgPSBtc2VjcyAtIF9sYXN0TVNlY3MgKyAobnNlY3MgLSBfbGFzdE5TZWNzKSAvIDEwMDAwOyAvLyBQZXIgNC4yLjEuMiwgQnVtcCBjbG9ja3NlcSBvbiBjbG9jayByZWdyZXNzaW9uXG5cbiAgaWYgKGR0IDwgMCAmJiBvcHRpb25zLmNsb2Nrc2VxID09PSB1bmRlZmluZWQpIHtcbiAgICBjbG9ja3NlcSA9IGNsb2Nrc2VxICsgMSAmIDB4M2ZmZjtcbiAgfSAvLyBSZXNldCBuc2VjcyBpZiBjbG9jayByZWdyZXNzZXMgKG5ldyBjbG9ja3NlcSkgb3Igd2UndmUgbW92ZWQgb250byBhIG5ld1xuICAvLyB0aW1lIGludGVydmFsXG5cblxuICBpZiAoKGR0IDwgMCB8fCBtc2VjcyA+IF9sYXN0TVNlY3MpICYmIG9wdGlvbnMubnNlY3MgPT09IHVuZGVmaW5lZCkge1xuICAgIG5zZWNzID0gMDtcbiAgfSAvLyBQZXIgNC4yLjEuMiBUaHJvdyBlcnJvciBpZiB0b28gbWFueSB1dWlkcyBhcmUgcmVxdWVzdGVkXG5cblxuICBpZiAobnNlY3MgPj0gMTAwMDApIHtcbiAgICB0aHJvdyBuZXcgRXJyb3IoXCJ1dWlkLnYxKCk6IENhbid0IGNyZWF0ZSBtb3JlIHRoYW4gMTBNIHV1aWRzL3NlY1wiKTtcbiAgfVxuXG4gIF9sYXN0TVNlY3MgPSBtc2VjcztcbiAgX2xhc3ROU2VjcyA9IG5zZWNzO1xuICBfY2xvY2tzZXEgPSBjbG9ja3NlcTsgLy8gUGVyIDQuMS40IC0gQ29udmVydCBmcm9tIHVuaXggZXBvY2ggdG8gR3JlZ29yaWFuIGVwb2NoXG5cbiAgbXNlY3MgKz0gMTIyMTkyOTI4MDAwMDA7IC8vIGB0aW1lX2xvd2BcblxuICB2YXIgdGwgPSAoKG1zZWNzICYgMHhmZmZmZmZmKSAqIDEwMDAwICsgbnNlY3MpICUgMHgxMDAwMDAwMDA7XG4gIGJbaSsrXSA9IHRsID4+PiAyNCAmIDB4ZmY7XG4gIGJbaSsrXSA9IHRsID4+PiAxNiAmIDB4ZmY7XG4gIGJbaSsrXSA9IHRsID4+PiA4ICYgMHhmZjtcbiAgYltpKytdID0gdGwgJiAweGZmOyAvLyBgdGltZV9taWRgXG5cbiAgdmFyIHRtaCA9IG1zZWNzIC8gMHgxMDAwMDAwMDAgKiAxMDAwMCAmIDB4ZmZmZmZmZjtcbiAgYltpKytdID0gdG1oID4+PiA4ICYgMHhmZjtcbiAgYltpKytdID0gdG1oICYgMHhmZjsgLy8gYHRpbWVfaGlnaF9hbmRfdmVyc2lvbmBcblxuICBiW2krK10gPSB0bWggPj4+IDI0ICYgMHhmIHwgMHgxMDsgLy8gaW5jbHVkZSB2ZXJzaW9uXG5cbiAgYltpKytdID0gdG1oID4+PiAxNiAmIDB4ZmY7IC8vIGBjbG9ja19zZXFfaGlfYW5kX3Jlc2VydmVkYCAoUGVyIDQuMi4yIC0gaW5jbHVkZSB2YXJpYW50KVxuXG4gIGJbaSsrXSA9IGNsb2Nrc2VxID4+PiA4IHwgMHg4MDsgLy8gYGNsb2NrX3NlcV9sb3dgXG5cbiAgYltpKytdID0gY2xvY2tzZXEgJiAweGZmOyAvLyBgbm9kZWBcblxuICBmb3IgKHZhciBuID0gMDsgbiA8IDY7ICsrbikge1xuICAgIGJbaSArIG5dID0gbm9kZVtuXTtcbiAgfVxuXG4gIHJldHVybiBidWYgPyBidWYgOiBieXRlc1RvVXVpZChiKTtcbn1cblxuZXhwb3J0IGRlZmF1bHQgdjE7IiwiaW1wb3J0IHYzNSBmcm9tICcuL3YzNS5qcyc7XG5pbXBvcnQgbWQ1IGZyb20gJy4vbWQ1LmpzJztcbnZhciB2MyA9IHYzNSgndjMnLCAweDMwLCBtZDUpO1xuZXhwb3J0IGRlZmF1bHQgdjM7IiwiaW1wb3J0IGJ5dGVzVG9VdWlkIGZyb20gJy4vYnl0ZXNUb1V1aWQuanMnO1xuXG5mdW5jdGlvbiB1dWlkVG9CeXRlcyh1dWlkKSB7XG4gIC8vIE5vdGU6IFdlIGFzc3VtZSB3ZSdyZSBiZWluZyBwYXNzZWQgYSB2YWxpZCB1dWlkIHN0cmluZ1xuICB2YXIgYnl0ZXMgPSBbXTtcbiAgdXVpZC5yZXBsYWNlKC9bYS1mQS1GMC05XXsyfS9nLCBmdW5jdGlvbiAoaGV4KSB7XG4gICAgYnl0ZXMucHVzaChwYXJzZUludChoZXgsIDE2KSk7XG4gIH0pO1xuICByZXR1cm4gYnl0ZXM7XG59XG5cbmZ1bmN0aW9uIHN0cmluZ1RvQnl0ZXMoc3RyKSB7XG4gIHN0ciA9IHVuZXNjYXBlKGVuY29kZVVSSUNvbXBvbmVudChzdHIpKTsgLy8gVVRGOCBlc2NhcGVcblxuICB2YXIgYnl0ZXMgPSBuZXcgQXJyYXkoc3RyLmxlbmd0aCk7XG5cbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBzdHIubGVuZ3RoOyBpKyspIHtcbiAgICBieXRlc1tpXSA9IHN0ci5jaGFyQ29kZUF0KGkpO1xuICB9XG5cbiAgcmV0dXJuIGJ5dGVzO1xufVxuXG5leHBvcnQgdmFyIEROUyA9ICc2YmE3YjgxMC05ZGFkLTExZDEtODBiNC0wMGMwNGZkNDMwYzgnO1xuZXhwb3J0IHZhciBVUkwgPSAnNmJhN2I4MTEtOWRhZC0xMWQxLTgwYjQtMDBjMDRmZDQzMGM4JztcbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIChuYW1lLCB2ZXJzaW9uLCBoYXNoZnVuYykge1xuICB2YXIgZ2VuZXJhdGVVVUlEID0gZnVuY3Rpb24gZ2VuZXJhdGVVVUlEKHZhbHVlLCBuYW1lc3BhY2UsIGJ1Ziwgb2Zmc2V0KSB7XG4gICAgdmFyIG9mZiA9IGJ1ZiAmJiBvZmZzZXQgfHwgMDtcbiAgICBpZiAodHlwZW9mIHZhbHVlID09ICdzdHJpbmcnKSB2YWx1ZSA9IHN0cmluZ1RvQnl0ZXModmFsdWUpO1xuICAgIGlmICh0eXBlb2YgbmFtZXNwYWNlID09ICdzdHJpbmcnKSBuYW1lc3BhY2UgPSB1dWlkVG9CeXRlcyhuYW1lc3BhY2UpO1xuICAgIGlmICghQXJyYXkuaXNBcnJheSh2YWx1ZSkpIHRocm93IFR5cGVFcnJvcigndmFsdWUgbXVzdCBiZSBhbiBhcnJheSBvZiBieXRlcycpO1xuICAgIGlmICghQXJyYXkuaXNBcnJheShuYW1lc3BhY2UpIHx8IG5hbWVzcGFjZS5sZW5ndGggIT09IDE2KSB0aHJvdyBUeXBlRXJyb3IoJ25hbWVzcGFjZSBtdXN0IGJlIHV1aWQgc3RyaW5nIG9yIGFuIEFycmF5IG9mIDE2IGJ5dGUgdmFsdWVzJyk7IC8vIFBlciA0LjNcblxuICAgIHZhciBieXRlcyA9IGhhc2hmdW5jKG5hbWVzcGFjZS5jb25jYXQodmFsdWUpKTtcbiAgICBieXRlc1s2XSA9IGJ5dGVzWzZdICYgMHgwZiB8IHZlcnNpb247XG4gICAgYnl0ZXNbOF0gPSBieXRlc1s4XSAmIDB4M2YgfCAweDgwO1xuXG4gICAgaWYgKGJ1Zikge1xuICAgICAgZm9yICh2YXIgaWR4ID0gMDsgaWR4IDwgMTY7ICsraWR4KSB7XG4gICAgICAgIGJ1ZltvZmYgKyBpZHhdID0gYnl0ZXNbaWR4XTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICByZXR1cm4gYnVmIHx8IGJ5dGVzVG9VdWlkKGJ5dGVzKTtcbiAgfTsgLy8gRnVuY3Rpb24jbmFtZSBpcyBub3Qgc2V0dGFibGUgb24gc29tZSBwbGF0Zm9ybXMgKCMyNzApXG5cblxuICB0cnkge1xuICAgIGdlbmVyYXRlVVVJRC5uYW1lID0gbmFtZTtcbiAgfSBjYXRjaCAoZXJyKSB7fSAvLyBGb3IgQ29tbW9uSlMgZGVmYXVsdCBleHBvcnQgc3VwcG9ydFxuXG5cbiAgZ2VuZXJhdGVVVUlELkROUyA9IEROUztcbiAgZ2VuZXJhdGVVVUlELlVSTCA9IFVSTDtcbiAgcmV0dXJuIGdlbmVyYXRlVVVJRDtcbn0iLCJpbXBvcnQgcm5nIGZyb20gJy4vcm5nLmpzJztcbmltcG9ydCBieXRlc1RvVXVpZCBmcm9tICcuL2J5dGVzVG9VdWlkLmpzJztcblxuZnVuY3Rpb24gdjQob3B0aW9ucywgYnVmLCBvZmZzZXQpIHtcbiAgdmFyIGkgPSBidWYgJiYgb2Zmc2V0IHx8IDA7XG5cbiAgaWYgKHR5cGVvZiBvcHRpb25zID09ICdzdHJpbmcnKSB7XG4gICAgYnVmID0gb3B0aW9ucyA9PT0gJ2JpbmFyeScgPyBuZXcgQXJyYXkoMTYpIDogbnVsbDtcbiAgICBvcHRpb25zID0gbnVsbDtcbiAgfVxuXG4gIG9wdGlvbnMgPSBvcHRpb25zIHx8IHt9O1xuICB2YXIgcm5kcyA9IG9wdGlvbnMucmFuZG9tIHx8IChvcHRpb25zLnJuZyB8fCBybmcpKCk7IC8vIFBlciA0LjQsIHNldCBiaXRzIGZvciB2ZXJzaW9uIGFuZCBgY2xvY2tfc2VxX2hpX2FuZF9yZXNlcnZlZGBcblxuICBybmRzWzZdID0gcm5kc1s2XSAmIDB4MGYgfCAweDQwO1xuICBybmRzWzhdID0gcm5kc1s4XSAmIDB4M2YgfCAweDgwOyAvLyBDb3B5IGJ5dGVzIHRvIGJ1ZmZlciwgaWYgcHJvdmlkZWRcblxuICBpZiAoYnVmKSB7XG4gICAgZm9yICh2YXIgaWkgPSAwOyBpaSA8IDE2OyArK2lpKSB7XG4gICAgICBidWZbaSArIGlpXSA9IHJuZHNbaWldO1xuICAgIH1cbiAgfVxuXG4gIHJldHVybiBidWYgfHwgYnl0ZXNUb1V1aWQocm5kcyk7XG59XG5cbmV4cG9ydCBkZWZhdWx0IHY0OyIsImltcG9ydCB2MzUgZnJvbSAnLi92MzUuanMnO1xuaW1wb3J0IHNoYTEgZnJvbSAnLi9zaGExLmpzJztcbnZhciB2NSA9IHYzNSgndjUnLCAweDUwLCBzaGExKTtcbmV4cG9ydCBkZWZhdWx0IHY1OyIsImltcG9ydCBGZXRjaGVyIGZyb20gJy4vZmV0Y2hlcic7XG5pbXBvcnQgVXNlciBmcm9tICcuL3VzZXInO1xuaW1wb3J0IE5vdGlmaWNhdG9yIGZyb20gJy4vbm90aWZpY2F0b3InO1xuaW1wb3J0IE1lcmNoYW50cyBmcm9tICcuL21lcmNoYW50cyc7XG5pbXBvcnQgQ2FydENvbnRyb2xsZXIgZnJvbSAnLi9jYXJ0Q29udHJvbGxlcic7XG5pbXBvcnQgQ2FzaGJhY2sgZnJvbSAnLi9jYXNoYmFjayc7XG5pbXBvcnQgVmFsaWRhdGlvblN0b3JlIGZyb20gJy4vdmFsaWRhdGlvbi1zdG9yZSc7XG5pbXBvcnQgeyBhc3luY1dyYXBwZXIgfSBmcm9tICcuLi91dGlscyc7XG5pbXBvcnQgeyBVU0VSX1VSTCwgTUVSQ0hBTlRTX1VSTCwgU0VSUF9DT05GSUdTX1VSTCB9IGZyb20gJy4uL2NvbnN0YW50cyc7XG5pbXBvcnQgaW5pdFNlbnRyeSBmcm9tICcuLi9tb2R1bGVzL3NlbnRyeSc7XG5cbmluaXRTZW50cnkoKTtcblxuY2xhc3MgQXBwIHtcbiAgY29uc3RydWN0b3IoKSB7XG4gICAgdGhpcy51c2VyID0gbmV3IFVzZXIoeyB1cmw6IFVTRVJfVVJMIH0pO1xuICAgIHRoaXMubm90aWZpY2F0b3IgPSBuZXcgTm90aWZpY2F0b3IoKTtcbiAgICB0aGlzLm1lcmNoYW50cyA9IG5ldyBNZXJjaGFudHMoeyB1cmw6IE1FUkNIQU5UU19VUkwgfSk7XG4gICAgdGhpcy5jYXNoYmFjayA9IG5ldyBDYXNoYmFjaygpO1xuICAgIHRoaXMudmFsaWRhdGlvblN0b3JlID0gbmV3IFZhbGlkYXRpb25TdG9yZSh7XG4gICAgICBnZXRNZXJjaGFudENvbmZpZzogdGhpcy5tZXJjaGFudHMuZ2V0TWVyY2hhbnRDb25maWcsXG4gICAgfSk7XG4gICAgdGhpcy5jYXJ0Q29udHJvbGxlciA9IG5ldyBDYXJ0Q29udHJvbGxlcih7XG4gICAgICBnZXRNZXJjaGFudENvbmZpZzogdGhpcy5tZXJjaGFudHMuZ2V0TWVyY2hhbnRDb25maWcsXG4gICAgfSk7XG4gICAgdGhpcy5zZXJwQ29uZmlncyA9IG5ldyBGZXRjaGVyKHtcbiAgICAgIHVybDogU0VSUF9DT05GSUdTX1VSTCxcbiAgICAgIG5hbWU6ICdzZXJwQ29uZmlncycsXG4gICAgICBvcHRpb25zOiB7XG4gICAgICAgIHVwZGF0ZUludGVydmFsOiA1ICogNjAgKiA2MCAqIDEwMDAsIC8vIDUgaHJzXG4gICAgICB9LFxuICAgIH0pO1xuICAgIHRoaXMuc2V0TWVzc2FnZXNMaXN0ZW5lcigpO1xuICAgIHRoaXMuc2V0RXZlbnRzTGlzdGVuZXIoKTtcbiAgfVxuXG4gIHNldEV2ZW50c0xpc3RlbmVyKCkge1xuICAgIHRoaXMubWVyY2hhbnRzLm9uKCd1cGRhdGU6c3RhdGUnLCAoeyBtaWQsIHN0YXRlIH0pID0+IHtcbiAgICAgIHRoaXMudmFsaWRhdGlvblN0b3JlLnVwZGF0ZSh7XG4gICAgICAgIG1pZCxcbiAgICAgICAgcmVzdWx0OiB7XG4gICAgICAgICAgYWN0aXZhdGVkOiBzdGF0ZS5hY3RpdmF0ZWQsXG4gICAgICAgIH0sXG4gICAgICB9KTtcbiAgICB9KTtcblxuICAgIHRoaXMudmFsaWRhdGlvblN0b3JlLm9uKCd1cGRhdGUnLCBhc3luYyAoeyBtaWQsIHBhcmFtcyB9KSA9PiB7XG4gICAgICBjb25zdCBpbnZhbGlkUGFyYW1zID0gW107XG5cbiAgICAgIE9iamVjdFxuICAgICAgICAuZW50cmllcyhwYXJhbXMpXG4gICAgICAgIC5mb3JFYWNoKChbbmFtZSwgaXNWYWxpZF0pID0+IHtcbiAgICAgICAgICBpZiAoIWlzVmFsaWQpIHtcbiAgICAgICAgICAgIGludmFsaWRQYXJhbXMucHVzaCh7XG4gICAgICAgICAgICAgIHNlY3Rpb246ICdpbnZhbGlkUGFyYW1zJyxcbiAgICAgICAgICAgICAgcGFyYW1ldGVyOiBuYW1lLFxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgfVxuICAgICAgICB9KTtcblxuICAgICAgaWYgKGludmFsaWRQYXJhbXMubGVuZ3RoID4gMCkge1xuICAgICAgICBjb25zdCB0YWJzID0gYXdhaXQgdGhpcy5tZXJjaGFudHMuZ2V0TWVyY2hhbnRUYWJzKG1pZCk7XG5cbiAgICAgICAgdGFicy5mb3JFYWNoKCh0YWIpID0+IHtcbiAgICAgICAgICBjb25zdCBpc1NwZWNpYWxQYWdlID0gdGhpcy5tZXJjaGFudHMuaXNTcGVjaWFsUGFnZSh7XG4gICAgICAgICAgICBpZDogbWlkLFxuICAgICAgICAgICAgdXJsOiB0YWIudXJsLFxuICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgaWYgKGlzU3BlY2lhbFBhZ2UgIT09ICdjaGVja291dCcpIHtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICB0aGlzLm5vdGlmaWNhdG9yLnNlbmROb3RpZmljYXRpb24oe1xuICAgICAgICAgICAgZm9yY2VkVGFiSWQ6IHRhYi5pZCxcbiAgICAgICAgICAgIHRvOiAnY29udGVudCcsXG4gICAgICAgICAgICBwYXJhbXM6IGludmFsaWRQYXJhbXMsXG4gICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgICAgfVxuICAgIH0pO1xuICB9XG5cbiAgZ2V0U0VSUENvbmZpZygpIHtcbiAgICByZXR1cm4gdGhpcy5zZXJwQ29uZmlncy5kYXRhO1xuICB9XG5cbiAgZ2V0Q29udGVudERhdGEoeyBzZW5kZXIsIGhyZWYgfSkge1xuICAgIGNvbnN0IG1lcmNoYW50ID0gdGhpcy5tZXJjaGFudHMuZ2V0TWVyY2hhbnRXaXRoU3RhdGUoaHJlZiB8fCBzZW5kZXIudXJsKTtcbiAgICByZXR1cm4geyAuLi5tZXJjaGFudCwgdXNlcjogdGhpcy51c2VyLmRhdGEgfTtcbiAgfVxuXG4gIGdldFVzZXJJbmZvKCkge1xuICAgIHJldHVybiB0aGlzLnVzZXIuZGF0YTtcbiAgfVxuXG4gIGNoZWNrQXR0cmlidXRpb24oeyB1cmwsIHRhYklkIH0pIHtcbiAgICAvLyBDaGVjayBpZiBhY3RpdmF0aW9uL3N1cHByZXNzaW9uIHByb2NjZXNzIGdvZXMgaW4gdGFiXG4gICAgY29uc3QgcHJvY2Vzc05hbWUgPSB0aGlzLmNhc2hiYWNrLmNoZWNrUHJvY2Vzcyh7IHVybCwgdGFiSWQgfSk7XG4gICAgaWYgKHByb2Nlc3NOYW1lID09PSAnYXR0cmlidXRpb24nKSB7XG4gICAgICBjb25zdCB7IG1lcmNoYW50RGF0YSwgbWVyY2hhbnRTdGF0ZSB9ID0gdGhpcy5tZXJjaGFudHMuZ2V0TWVyY2hhbnRXaXRoU3RhdGUodXJsKTtcbiAgICAgIC8vIElmIHVzZXIgb24gdGhlIG1lcmNoYW50IHBhZ2UgdGhlblxuICAgICAgLy8gM3JkLXBhcnR5IGxpbmsgd2FzIGFjdGl2YXRlZCBpbiB0aGlzIHRhYiBiZWZvcmUgc28gbWVyY2hhbnQgc2hvdWxkIGJlIGRpc2FibGVkXG4gICAgICBpZiAobWVyY2hhbnREYXRhICYmICFtZXJjaGFudFN0YXRlPy5hY3RpdmF0aW9uKSB7XG4gICAgICAgIHRoaXMubWVyY2hhbnRzLnNldFN0YXRlKHtcbiAgICAgICAgICBpZDogbWVyY2hhbnREYXRhLmlkLFxuICAgICAgICAgIGRhdGE6IHRoaXMubWVyY2hhbnRzLmdldERlZmF1bHRTdGF0ZSgnc3VwcHJlc3NlZCcpLFxuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICAgIHRoaXMuY2FzaGJhY2suY2xlYXJQcm9jZXNzKHByb2Nlc3NOYW1lLCB0YWJJZCk7XG4gICAgfVxuICB9XG5cbiAgLy8gSGFyZGNvZGVkIHRvIHNhdmUgY3VycnlzIHRva2VuXG4gIHNhdmVDdXJyeXNCaWQoeyBiaWQgfSkge1xuICAgIHRoaXMuY2FydENvbnRyb2xsZXIuY3VycnlzQmlkID0gYmlkO1xuICB9XG5cbiAgc2V0TWVzc2FnZXNMaXN0ZW5lcigpIHtcbiAgICBjaHJvbWUucnVudGltZS5vbk1lc3NhZ2UuYWRkTGlzdGVuZXIoKG1lc3NhZ2UsIHNlbmRlciwgc2VuZFJlc3BvbnNlKSA9PiB7XG4gICAgICBjb25zdCB7IG1vZHVsZSwgYWN0aW9uLCBkYXRhIH0gPSBtZXNzYWdlO1xuXG4gICAgICBjb25zdCBjb250ZXh0ID0gdGhpc1ttb2R1bGVdIHx8IHRoaXM7XG4gICAgICBpZiAodHlwZW9mIGNvbnRleHRbYWN0aW9uXSA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICBhc3luY1dyYXBwZXIoY29udGV4dFthY3Rpb25dLmJpbmQoY29udGV4dCksIHtcbiAgICAgICAgICAuLi5kYXRhLFxuICAgICAgICAgIHNlbmRlcixcbiAgICAgICAgfSkudGhlbihzZW5kUmVzcG9uc2UpO1xuICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH0pO1xuXG4gICAgY2hyb21lLmNvb2tpZXMub25DaGFuZ2VkXG4gICAgICAuYWRkTGlzdGVuZXIoKHsgY29va2llIH0pID0+IHRoaXMubWVyY2hhbnRzLmNoZWNrQWN0aXZhdGlvblN0YXR1cyhjb29raWUpKTtcbiAgICBjaHJvbWUud2ViUmVxdWVzdC5vbkJlZm9yZVJlcXVlc3QuYWRkTGlzdGVuZXIoKGRldGFpbHMpID0+IHtcbiAgICAgIHRoaXMuY2hlY2tBdHRyaWJ1dGlvbihkZXRhaWxzKTtcbiAgICB9LCB7IHVybHM6IFsnPGFsbF91cmxzPiddLCB0eXBlczogWydtYWluX2ZyYW1lJ10gfSk7XG4gICAgY2hyb21lLnRhYnMub25VcGRhdGVkLmFkZExpc3RlbmVyKCh0YWJJZCwgY2hhbmdlSW5mbywgeyB1cmwgfSkgPT4ge1xuICAgICAgdGhpcy5jaGVja0F0dHJpYnV0aW9uKHsgdXJsLCB0YWJJZCB9KTtcbiAgICB9KTtcbiAgfVxuXG4gIGFzeW5jIGluamVjdEZyYW1lQXBwKHsgZnJhbWVVcmwsIHNlbmRlciB9KSB7XG4gICAgY29uc3QgeyB0YWIgfSA9IHNlbmRlcjtcbiAgICBjb25zdCBmcmFtZXMgPSBhd2FpdCBicm93c2VyLndlYk5hdmlnYXRpb24uZ2V0QWxsRnJhbWVzKHsgdGFiSWQ6IHRhYi5pZCB9KTtcbiAgICBjb25zdCBhcHBGcmFtZXMgPSBmcmFtZXMuZmlsdGVyKCh7IHVybCB9KSA9PiB1cmwuaW5jbHVkZXMoZnJhbWVVcmwpKTtcbiAgICBhcHBGcmFtZXMubWFwKGFzeW5jIChmcmFtZSkgPT4ge1xuICAgICAgY29uc3QgeyBmcmFtZUlkIH0gPSBmcmFtZTtcbiAgICAgIHRyeSB7XG4gICAgICAgIGF3YWl0IGJyb3dzZXIudGFicy5zZW5kTWVzc2FnZSh0YWIuaWQsIHsgYWN0aW9uOiAncGluZycgfSwgeyBmcmFtZUlkIH0pO1xuICAgICAgfSBjYXRjaCAoZSkge1xuICAgICAgICBhd2FpdCBicm93c2VyLnRhYnMuZXhlY3V0ZVNjcmlwdCh0YWIuaWQsIHsgZmlsZTogJ2Jyb3dzZXItcG9seWZpbGwuanMnLCBmcmFtZUlkIH0pO1xuICAgICAgICBhd2FpdCBicm93c2VyLnRhYnMuZXhlY3V0ZVNjcmlwdCh0YWIuaWQsIHsgZmlsZTogJ3ZlbmRvcnMvYnVuZGxlLmpzJywgZnJhbWVJZCB9KTtcbiAgICAgICAgYXdhaXQgYnJvd3Nlci50YWJzLmV4ZWN1dGVTY3JpcHQodGFiLmlkLCB7IGZpbGU6ICdmcmFtZS9idW5kbGUuanMnLCBmcmFtZUlkIH0pO1xuICAgICAgfVxuICAgICAgYXdhaXQgYnJvd3Nlci50YWJzLnNlbmRNZXNzYWdlKFxuICAgICAgICB0YWIuaWQsXG4gICAgICAgIHtcbiAgICAgICAgICBhY3Rpb246ICdwcm9jZXNzJyxcbiAgICAgICAgICBkYXRhOiB7IHVybDogdGFiLnVybCB9LFxuICAgICAgICB9LFxuICAgICAgICB7IGZyYW1lSWQgfSxcbiAgICAgICk7XG4gICAgICByZXR1cm4gdHJ1ZTtcbiAgICB9KTtcbiAgfVxufVxuXG53aW5kb3cuYXBwID0gbmV3IEFwcCgpO1xuIiwiaW1wb3J0IHsgdjQgYXMgdXVpZHY0IH0gZnJvbSAndXVpZCc7XG5pbXBvcnQgUGFyc2VyIGZyb20gJy4uL21vZHVsZXMvcGFyc2VyL2luZGV4JztcbmltcG9ydCBhcGkgZnJvbSAnLi4vbW9kdWxlcy9hcGknO1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBDYXJ0Q29udHJvbGxlciB7XG4gIGNvbnN0cnVjdG9yKHsgZ2V0TWVyY2hhbnRDb25maWcgfSkge1xuICAgIHRoaXMuZ2V0TWVyY2hhbnRDb25maWcgPSBnZXRNZXJjaGFudENvbmZpZztcbiAgICB0aGlzLnBhcnNlZENhcnRzID0ge307XG4gIH1cblxuICBhc3luYyBwcm9jZXNzKHsgbWlkIH0pIHtcbiAgICBhd2FpdCB0aGlzLnBhcnNlQ2FydCh7IG1pZCB9KTtcbiAgICBpZiAodGhpcy5wYXJzZWRDYXJ0c1ttaWRdKSB7XG4gICAgICBjb25zdCBoYW5kbGVkRGF0YSA9IHRoaXMuaGFuZGxlUGFyc2VkRGF0YSh0aGlzLnBhcnNlZENhcnRzW21pZF0pO1xuICAgICAgY29uc3QgeyBpdGVtcywgdG90YWwsIGNhcnRJZCB9ID0gaGFuZGxlZERhdGE7XG4gICAgICBjb25zdCBkaXNjb3VudGVkRGF0YSA9IGF3YWl0IGFwaS5nZXREaXNjb3VudEJ5Q2FydElkKGNhcnRJZCwge1xuICAgICAgICBpdGVtcyxcbiAgICAgICAgdG90YWwsXG4gICAgICAgIG1lcmNoYW50X2lkOiBtaWQsXG4gICAgICB9KTtcbiAgICAgIHJldHVybiB0aGlzLmhhbmRsZURpc2NvdW50ZWREYXRhKGRpc2NvdW50ZWREYXRhLCB0aGlzLnBhcnNlZENhcnRzW21pZF0pO1xuICAgIH1cbiAgICByZXR1cm4gbnVsbDtcbiAgfVxuXG4gIHByb2Nlc3NDYXJyeXNDb25maWcoY29uZmlnKSB7XG4gICAgcmV0dXJuIHsgLi4uY29uZmlnLCB1cmw6IGAke2NvbmZpZy51cmx9JHt0aGlzLmN1cnJ5c0JpZH1gIH07XG4gIH1cblxuICBhc3luYyBwYXJzZUNhcnQoeyBtaWQgfSkge1xuICAgIGxldCBjb25maWcgPSBhd2FpdCB0aGlzLmdldE1lcmNoYW50Q29uZmlnKHsgaWQ6IG1pZCwgbW9kdWxlTmFtZTogJ2NhcnRQYXJzZXInIH0pO1xuICAgIC8vIEhhcmRjb2RlZCB0byB1c2UgY3VycnlzIHRva2VuIGluIHJlcXVlc3RcbiAgICBpZiAoY29uZmlnICYmIG1pZCA9PT0gJ2YxYTdlMDMwLTY1YTYtNDI2My1hN2ZkLTU2MDBkYmYyZGNhZicpIHtcbiAgICAgIGNvbmZpZyA9IHRoaXMucHJvY2Vzc0NhcnJ5c0NvbmZpZyhjb25maWcpO1xuICAgIH1cbiAgICBjb25zdCBwYXJzZXIgPSBuZXcgUGFyc2VyKGNvbmZpZyk7XG4gICAgY29uc3QgcGFyc2VkRGF0YSA9IGF3YWl0IHBhcnNlci5wcm9jZXNzKCk7XG4gICAgcmV0dXJuIHRoaXMuc2F2ZUNhcnREYXRhKHsgbWlkLCBwYXJzZWREYXRhIH0pO1xuICB9XG5cbiAgc2F2ZUNhcnREYXRhKHsgbWlkLCBwYXJzZWREYXRhIH0pIHtcbiAgICBjb25zdCB7IHByb2R1Y3RzIH0gPSBwYXJzZWREYXRhO1xuICAgIGlmIChwcm9kdWN0cy5sZW5ndGgpIHtcbiAgICAgIGNvbnN0IGNhcnRJZCA9IHRoaXMucGFyc2VkQ2FydHM/LlttaWRdPy5jYXJ0SWQgfHwgdXVpZHY0KCk7XG4gICAgICB0aGlzLnBhcnNlZENhcnRzW21pZF0gPSB7IGNhcnRJZCwgLi4ucGFyc2VkRGF0YSB9O1xuICAgICAgcmV0dXJuIHBhcnNlZERhdGE7XG4gICAgfVxuICAgIGRlbGV0ZSB0aGlzLnBhcnNlZENhcnRzW21pZF07XG4gICAgcmV0dXJuIG51bGw7XG4gIH1cblxuICBoYW5kbGVQYXJzZWREYXRhKGRhdGEpIHtcbiAgICBjb25zdCB7IHByb2R1Y3RzLCB0b3RhbCwgdG90YWxDdXJyZW5jeSwgY2FydElkIH0gPSBkYXRhOyAvLyBlc2xpbnQtZGlzYWJsZS1saW5lXG4gICAgY29uc3QgaXRlbXMgPSBwcm9kdWN0cy5tYXAoKHByb2R1Y3QpID0+IHtcbiAgICAgIGNvbnN0IHsgcHJpY2UsIGN1cnJlbmN5LCBicmFuZCwgaW1hZ2UsIGNvbG91ciB9ID0gcHJvZHVjdDsgLy8gZXNsaW50LWRpc2FibGUtbGluZVxuICAgICAgcmV0dXJuIHtcbiAgICAgICAgLi4ucHJvZHVjdCxcbiAgICAgICAgcHJpY2U6IHsgdmFsdWU6IHByaWNlLCBjdXJyZW5jeTogY3VycmVuY3kuY29kZSB9LFxuICAgICAgICBtZXRhOiB7IGJyYW5kLCBpbWFnZSwgY29sb3VyIH0sXG4gICAgICB9O1xuICAgIH0pO1xuICAgIHJldHVybiB7XG4gICAgICBpdGVtcyxcbiAgICAgIHRvdGFsOiB7XG4gICAgICAgIHZhbHVlOiB0b3RhbCxcbiAgICAgICAgY3VycmVuY3k6IHRvdGFsQ3VycmVuY3kuY29kZSxcbiAgICAgIH0sXG4gICAgICBjYXJ0SWQsXG4gICAgfTtcbiAgfVxuXG4gIGhhbmRsZURpc2NvdW50ZWREYXRhKGRhdGEsIHsgcHJvZHVjdHMgPSBbXSB9KSB7XG4gICAgY29uc3QgeyBpdGVtcywgZGlzY291bnRlZF90b3RhbDogZGlzY29Ub3RhbCB9ID0gZGF0YTtcbiAgICBjb25zdCBkaXNjb1Byb2R1Y3RzID0gaXRlbXMubWFwKChpdGVtKSA9PiB7XG4gICAgICBjb25zdCB7IHByaWNlLCBkaXNjb3VudGVkX3ByaWNlOiBkaXNjb1ByaWNlLCAuLi5yZXN0IH0gPSBpdGVtOyAvLyBlc2xpbnQtZGlzYWJsZS1saW5lXG4gICAgICBjb25zdCB7IG5hbWUsIGltYWdlLCB1cmwgfSA9IHByb2R1Y3RzLmZpbmQoKHsgc2t1IH0pID0+IHNrdSA9PT0gcmVzdC5za3UpO1xuICAgICAgcmV0dXJuIHsgZGlzY29QcmljZSwgcHJpY2U6IHByaWNlLnZhbHVlLCAuLi5yZXN0LCBuYW1lLCBpbWFnZSwgdXJsIH07IC8vIGVzbGludC1kaXNhYmxlLWxpbmVcbiAgICB9KTtcbiAgICByZXR1cm4geyBkaXNjb1Byb2R1Y3RzLCBkaXNjb1RvdGFsIH07XG4gIH1cbn1cbiIsImltcG9ydCB7IFNVUFBSRVNTSU9OX1BBVFRFUk5TIH0gZnJvbSAnLi4vY29uc3RhbnRzJztcblxuLy8gVGhpcyBjbGFzcyBtYW5hZ2VzIHByb2Nlc3NlcyBsaWtlIGFjdGl2YXRpb24vc3VwcHJlc3Npb24gc3RhcnRlZCBpbiB0YWJcbmV4cG9ydCBkZWZhdWx0IGNsYXNzIENhc2hiYWNrIHtcbiAgY29uc3RydWN0b3IoKSB7XG4gICAgdGhpcy5wYXR0ZXJucyA9IHtcbiAgICAgIGF0dHJpYnV0aW9uOiBTVVBQUkVTU0lPTl9QQVRURVJOUyxcbiAgICB9O1xuICAgIHRoaXMucHJvY2Vzc2VzID0ge1xuICAgICAgYXR0cmlidXRpb246IHt9LFxuICAgIH07XG4gIH1cblxuICBjaGVja1Byb2Nlc3MoeyB1cmwsIHRhYklkIH0pIHtcbiAgICAvLyBDaGVjayBpZiB1c2VyIGdvZXMgdGhyb3VnaCBhZmZpbGlhdGUgbGluay8zcmQgcGFydHkgbGlua1xuICAgIGNvbnN0IHByb2Nlc3NOYW1lID0gT2JqZWN0XG4gICAgICAua2V5cyh0aGlzLnBhdHRlcm5zKVxuICAgICAgLmZpbmQoKG5hbWUpID0+IHRoaXMucGF0dGVybnNbbmFtZV0uc29tZSgocGF0dGVybikgPT4gdXJsLnNlYXJjaChwYXR0ZXJuKSA+IC0xKSk7XG4gICAgaWYgKHByb2Nlc3NOYW1lKSB7XG4gICAgICAvLyBDbGVhciBwcmV2aW91cyBzdGF0ZSBpbiB0YWJcbiAgICAgIHRoaXMuY2xlYXJQcm9jZXNzKHByb2Nlc3NOYW1lLCB0YWJJZCk7XG4gICAgICAvLyBNYXJrIHRoYXQgc29tZSBvZiBwcm9jZXNzIHN0YXJ0ZWQgaW4gdGFiIHdpdGggdGFiSWRcbiAgICAgIC8vIFRpbWVyIHdpbGwgcmVmcmVzaCBzdGF0ZSBhZnRlciAzMCBzZWNvbmRzXG4gICAgICBjb25zdCB0aW1lciA9IHNldFRpbWVvdXQoKCkgPT4gdGhpcy5jbGVhclByb2Nlc3MocHJvY2Vzc05hbWUsIHRhYklkKSwgMzAwMDApO1xuICAgICAgdGhpcy5wcm9jZXNzZXNbcHJvY2Vzc05hbWVdW3RhYklkXSA9IHsgdGltZXIgfTtcbiAgICB9XG4gICAgcmV0dXJuIHRoaXMuZ2V0UHJvY2Vzcyh0YWJJZCk7XG4gIH1cblxuICBnZXRQcm9jZXNzKHRhYklkKSB7XG4gICAgY29uc3QgW3Byb2Nlc3NOYW1lXSA9IE9iamVjdFxuICAgICAgLmVudHJpZXModGhpcy5wcm9jZXNzZXMpXG4gICAgICAuZmluZCgoWywgcHJvY2Vzc2VzXSkgPT4gcHJvY2Vzc2VzW3RhYklkXSkgfHwgW107XG4gICAgcmV0dXJuIHByb2Nlc3NOYW1lO1xuICB9XG5cbiAgY2xlYXJQcm9jZXNzKHByb2Nlc3NOYW1lLCB0YWJJZCkge1xuICAgIGNsZWFyVGltZW91dCh0aGlzLnByb2Nlc3Nlc1twcm9jZXNzTmFtZV1bdGFiSWRdPy50aW1lcik7XG4gICAgZGVsZXRlIHRoaXMucHJvY2Vzc2VzW3Byb2Nlc3NOYW1lXVt0YWJJZF07XG4gIH1cbn1cbiIsImV4cG9ydCBkZWZhdWx0IGNsYXNzIENvb2tpZUNoZWNrZXIge1xuICBjb25zdHJ1Y3Rvcihjb25maWcgPSBbXSkge1xuICAgIHRoaXMuY29uZmlnID0gY29uZmlnO1xuICB9XG5cbiAgYXN5bmMgY2hlY2soKSB7XG4gICAgY29uc3QgY2hlY2tzID0gYXdhaXQgUHJvbWlzZS5hbGwoXG4gICAgICB0aGlzLmNvbmZpZy5tYXAoYXN5bmMgKHsgZG9tYWluLCBjb29raWVzID0gW10gfSkgPT4ge1xuICAgICAgICBjb25zdCBicm93c2VyQ29va2llcyA9IGF3YWl0IGJyb3dzZXIuY29va2llcy5nZXRBbGwoeyBkb21haW4gfSk7XG4gICAgICAgIHJldHVybiBjb29raWVzLmV2ZXJ5KCh7IG5hbWVQYXR0ZXJuLCB2YWx1ZVBhdHRlcm4gfSkgPT4ge1xuICAgICAgICAgIGNvbnN0IGlzQ29va2llc1ZhbGlkID0gYnJvd3NlckNvb2tpZXMuc29tZSgoeyBuYW1lLCB2YWx1ZSB9KSA9PiB7XG4gICAgICAgICAgICBjb25zdCBpc0Nvb2tpZVZhbGlkID0gbmFtZS5zZWFyY2gobmFtZVBhdHRlcm4pID4gLTEgJiYgdmFsdWUuc2VhcmNoKHZhbHVlUGF0dGVybikgPiAtMTtcbiAgICAgICAgICAgIHJldHVybiBpc0Nvb2tpZVZhbGlkO1xuICAgICAgICAgIH0pO1xuICAgICAgICAgIHJldHVybiBpc0Nvb2tpZXNWYWxpZDtcbiAgICAgICAgfSk7XG4gICAgICB9KSxcbiAgICApO1xuICAgIHJldHVybiBjaGVja3MuZXZlcnkoKHZhbHVlKSA9PiB2YWx1ZSA9PT0gdHJ1ZSk7XG4gIH1cbn1cbiIsImltcG9ydCBFdmVudEVtaXR0ZXIgZnJvbSAnZXZlbnRzJztcblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgRmV0Y2hlciBleHRlbmRzIEV2ZW50RW1pdHRlciB7XG4gIGNvbnN0cnVjdG9yKHsgbmFtZSwgdXJsLCBvcHRpb25zID0ge30gfSkge1xuICAgIHN1cGVyKCk7XG4gICAgdGhpcy5uYW1lID0gbmFtZTtcbiAgICB0aGlzLnVybCA9IHVybDtcbiAgICB0aGlzLm9wdGlvbnMgPSB7XG4gICAgICB1cGRhdGVJbnRlcnZhbDogNSAqIDYwICogMTAwMCwgLy8gNSBtaW5cbiAgICAgIHVwZGF0ZUVycm9ySW50ZXJ2YWw6IDMwICogMTAwMCwgLy8gMzAgc2VjXG4gICAgICAuLi5vcHRpb25zLFxuICAgIH07XG4gICAgdGhpcy51cGRhdGVUaW1lID0gMDtcbiAgICB0aGlzLnVwZGF0ZVRpbWVyID0gbnVsbDtcbiAgICB0aGlzLnVwZGF0ZUNvbnRyb2xsZXIgPSBudWxsO1xuICAgIHRoaXMuaW5pdCgpO1xuICB9XG5cbiAgYXN5bmMgaW5pdCgpIHtcbiAgICBhd2FpdCB0aGlzLmxvYWQoKTtcbiAgICBhd2FpdCB0aGlzLnVwZGF0ZSgpO1xuICB9XG5cbiAgYXN5bmMgbG9hZCgpIHtcbiAgICBjb25zdCBjYWNoZWREYXRhID0gYXdhaXQgYnJvd3Nlci5zdG9yYWdlLmxvY2FsLmdldChbXG4gICAgICBgJHt0aGlzLm5hbWV9LmRhdGFgLFxuICAgICAgYCR7dGhpcy5uYW1lfS51cGRhdGVUaW1lYCxcbiAgICBdKTtcbiAgICB0aGlzLmRhdGEgPSBjYWNoZWREYXRhW2Ake3RoaXMubmFtZX0uZGF0YWBdIHx8IHRoaXMuZGF0YTtcbiAgICB0aGlzLnVwZGF0ZVRpbWUgPSBjYWNoZWREYXRhW2Ake3RoaXMubmFtZX0udXBkYXRlVGltZWBdIHx8IDA7XG4gIH1cblxuICBhc3luYyBzYXZlKCkge1xuICAgIGF3YWl0IGJyb3dzZXIuc3RvcmFnZS5sb2NhbC5zZXQoe1xuICAgICAgW2Ake3RoaXMubmFtZX0uZGF0YWBdOiB0aGlzLmRhdGEsXG4gICAgICBbYCR7dGhpcy5uYW1lfS51cGRhdGVUaW1lYF06IHRoaXMudXBkYXRlVGltZSxcbiAgICB9KTtcbiAgfVxuXG4gIHBhcnNlKGRhdGEpIHtcbiAgICByZXR1cm4gZGF0YTtcbiAgfVxuXG4gIGFzeW5jIHJlcXVlc3QodXJsID0gdGhpcy51cmwsIG9wdGlvbnMgPSB7fSkge1xuICAgIGlmICh0aGlzLnVwZGF0ZUNvbnRyb2xsZXIpIHtcbiAgICAgIHRoaXMudXBkYXRlQ29udHJvbGxlci5hYm9ydCgpO1xuICAgIH1cbiAgICB0aGlzLnVwZGF0ZUNvbnRyb2xsZXIgPSBuZXcgQWJvcnRDb250cm9sbGVyKCk7XG4gICAgY29uc3QgeyBzaWduYWwgfSA9IHRoaXMudXBkYXRlQ29udHJvbGxlcjtcbiAgICBjb25zdCByZXNwb25zZSA9IGF3YWl0IGZldGNoKHVybCwgeyBzaWduYWwsIC4uLm9wdGlvbnMgfSk7XG4gICAgcmV0dXJuIHJlc3BvbnNlLmpzb24oKTtcbiAgfVxuXG4gIGFzeW5jIHVwZGF0ZShmb3JjZSkge1xuICAgIGNvbnN0IHsgdXBkYXRlSW50ZXJ2YWwsIHVwZGF0ZUVycm9ySW50ZXJ2YWwgfSA9IHRoaXMub3B0aW9ucztcbiAgICBpZiAoZm9yY2UgfHwgdGhpcy51cGRhdGVUaW1lICsgdXBkYXRlSW50ZXJ2YWwgPCBEYXRlLm5vdygpKSB7XG4gICAgICB0cnkge1xuICAgICAgICBjb25zdCBkYXRhID0gYXdhaXQgdGhpcy5yZXF1ZXN0KCk7XG4gICAgICAgIHRoaXMuZGF0YSA9IHRoaXMucGFyc2UoZGF0YSk7XG4gICAgICAgIGF3YWl0IHRoaXMuc2F2ZSgpO1xuICAgICAgICB0aGlzLnJlZnJlc2hVcGRhdGVUaW1lcih7fSk7XG4gICAgICAgIHJldHVybiB0aGlzLmRhdGE7XG4gICAgICB9IGNhdGNoIChlKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnJlZnJlc2hVcGRhdGVUaW1lcih7IGludGVydmFsOiB1cGRhdGVFcnJvckludGVydmFsLCBmb3JjZTogdHJ1ZSB9KTtcbiAgICAgIH1cbiAgICB9XG4gICAgdGhpcy5yZWZyZXNoVXBkYXRlVGltZXIoeyB0aW1lOiB0aGlzLnVwZGF0ZVRpbWUgfSk7XG4gICAgcmV0dXJuIG51bGw7XG4gIH1cblxuICByZWZyZXNoVXBkYXRlVGltZXIoeyB0aW1lID0gRGF0ZS5ub3coKSwgaW50ZXJ2YWwgPSB0aGlzLm9wdGlvbnMudXBkYXRlSW50ZXJ2YWwsIGZvcmNlIH0pIHtcbiAgICBjbGVhckludGVydmFsKHRoaXMudXBkYXRlVGltZXIpO1xuICAgIHRoaXMudXBkYXRlVGltZSA9IHRpbWU7XG4gICAgdGhpcy51cGRhdGVUaW1lciA9IHNldEludGVydmFsKCgpID0+IHtcbiAgICAgIGNsZWFySW50ZXJ2YWwodGhpcy51cGRhdGVUaW1lcik7XG4gICAgICB0aGlzLnVwZGF0ZShmb3JjZSk7XG4gICAgfSwgdGltZSArIGludGVydmFsIC0gRGF0ZS5ub3coKSk7XG4gIH1cbn1cbiIsImltcG9ydCBGZXRjaGVyIGZyb20gJy4vZmV0Y2hlcic7XG5pbXBvcnQgQ29va2llQ2hlY2tlciBmcm9tICcuL2Nvb2tpZUNoZWNrZXInO1xuaW1wb3J0IHsgYXN5bmNUaW1lb3V0IH0gZnJvbSAnLi4vdXRpbHMnO1xuaW1wb3J0IHsgTUVSQ0hBTlRTX0NPTkZJR1NfVVJMIH0gZnJvbSAnLi4vY29uc3RhbnRzJztcblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgTWVyY2hhbnRzIGV4dGVuZHMgRmV0Y2hlciB7XG4gIGNvbnN0cnVjdG9yKHsgbmFtZSA9ICdtZXJjaGFudHMnLCB1cmwsIG9wdGlvbnMgPSB7fSB9KSB7XG4gICAgc3VwZXIoeyBuYW1lLCB1cmwsIG9wdGlvbnMgfSk7XG4gICAgdGhpcy5kYXRhID0gW107XG4gICAgdGhpcy5zdGF0ZXMgPSB7fTtcbiAgICB0aGlzLmNvbmZpZ3MgPSBuZXcgRmV0Y2hlcih7XG4gICAgICB1cmw6IE1FUkNIQU5UU19DT05GSUdTX1VSTCxcbiAgICAgIG5hbWU6ICdtZXJjaGFudHNDb25maWdzJyxcbiAgICAgIG9wdGlvbnM6IHtcbiAgICAgICAgdXBkYXRlSW50ZXJ2YWw6IDUgKiA2MCAqIDYwICogMTAwMCwgLy8gNSBocnNcbiAgICAgIH0sXG4gICAgfSk7XG4gICAgdGhpcy5jb25maWdzLnVwZGF0ZSh0cnVlKTtcbiAgfVxuXG4gIHBhcnNlKGRhdGEgPSB7fSkge1xuICAgIHJldHVybiBkYXRhLm1lcmNoYW50cztcbiAgfVxuXG4gIGFzeW5jIHVwZGF0ZShmb3JjZSkge1xuICAgIGF3YWl0IHN1cGVyLnVwZGF0ZShmb3JjZSk7XG4gICAgdGhpcy51cGRhdGVTdGF0ZXMoKTtcbiAgICByZXR1cm4gdGhpcy5kYXRhO1xuICB9XG5cbiAgdXBkYXRlU3RhdGVzKHJlZnJlc2gpIHtcbiAgICB0aGlzLmRhdGEuZm9yRWFjaCgoeyBpZCB9KSA9PiB7XG4gICAgICBjb25zdCBkZWZhdWx0U3RhdGUgPSB0aGlzLmdldERlZmF1bHRTdGF0ZSgpO1xuICAgICAgdGhpcy5zdGF0ZXNbaWRdID0gcmVmcmVzaCA/IGRlZmF1bHRTdGF0ZSA6ICh0aGlzLnN0YXRlc1tpZF0gfHwgZGVmYXVsdFN0YXRlKTtcbiAgICB9KTtcbiAgfVxuXG4gIGdldERlZmF1bHRTdGF0ZShuYW1lID0gJ2RlZmF1bHQnKSB7XG4gICAgY29uc3Qgc3RhdGVzID0ge1xuICAgICAgZGVmYXVsdDoge1xuICAgICAgICBhY3RpdmF0ZWQ6IGZhbHNlLFxuICAgICAgICBzaG93Tm90aWZpY2F0aW9uOiB0cnVlLFxuICAgICAgfSxcbiAgICAgIGFjdGl2YXRlZDoge1xuICAgICAgICBhY3RpdmF0ZWQ6IHRydWUsXG4gICAgICAgIGFjdGl2YXRpb246IGZhbHNlLFxuICAgICAgICBzdXBwcmVzc2VkOiBmYWxzZSxcbiAgICAgICAgc2hvd05vdGlmaWNhdGlvbjogdHJ1ZSxcbiAgICAgIH0sXG4gICAgICBzdXBwcmVzc2VkOiB7XG4gICAgICAgIGFjdGl2YXRlZDogZmFsc2UsXG4gICAgICAgIHN1cHByZXNzZWQ6IHRydWUsXG4gICAgICAgIHNob3dOb3RpZmljYXRpb246IGZhbHNlLFxuICAgICAgfSxcbiAgICB9O1xuICAgIHJldHVybiBzdGF0ZXNbbmFtZV07XG4gIH1cblxuICBzZXRTdGF0ZSh7IGlkLCB1cmwsIGRhdGEgfSkge1xuICAgIHRyeSB7XG4gICAgICBjb25zdCBtaWQgPSBpZCB8fCB0aGlzLmdldEJ5VXJsKHVybCkuaWQ7XG4gICAgICB0aGlzLnN0YXRlc1ttaWRdID0geyAuLi4odGhpcy5zdGF0ZXNbbWlkXSB8fCB7fSksIC4uLmRhdGEgfTtcbiAgICAgIHRoaXMudXBkYXRlQ29udGVudERhdGEoeyBpZDogbWlkLCBkYXRhOiB7IG1lcmNoYW50U3RhdGU6IHRoaXMuc3RhdGVzW21pZF0gfSB9KTtcbiAgICAgIHRoaXMuZW1pdCgndXBkYXRlOnN0YXRlJywgeyBtaWQsIHN0YXRlOiB7IC4uLnRoaXMuc3RhdGVzW21pZF0gfSB9KTtcbiAgICAgIHJldHVybiB0aGlzLnN0YXRlc1ttaWRdO1xuICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgIHJldHVybiBudWxsO1xuICAgIH1cbiAgfVxuXG4gIGFzeW5jIHVwZGF0ZUNvbnRlbnREYXRhKHsgaWQsIGRhdGEgfSkge1xuICAgIGNvbnN0IHRhYnMgPSBhd2FpdCB0aGlzLmdldE1lcmNoYW50VGFicyhpZCk7XG4gICAgcmV0dXJuIFByb21pc2UuYWxsKFxuICAgICAgdGFicy5tYXAoYXN5bmMgKHRhYikgPT4gYnJvd3Nlci50YWJzLnNlbmRNZXNzYWdlKHRhYi5pZCwge1xuICAgICAgICBhY3Rpb246ICd1cGRhdGUnLFxuICAgICAgICBkYXRhLFxuICAgICAgfSkpLFxuICAgICk7XG4gIH1cblxuICBhc3luYyBnZXRNZXJjaGFudFRhYnMoaWQpIHtcbiAgICBjb25zdCB0YWJzID0gYXdhaXQgYnJvd3Nlci50YWJzLnF1ZXJ5KHt9KTtcbiAgICByZXR1cm4gdGFicy5maWx0ZXIoKHsgdXJsIH0pID0+ICh0aGlzLmdldEJ5VXJsKHVybCkgfHwge30pLmlkID09PSBpZCk7XG4gIH1cblxuICBpc1NwZWNpYWxQYWdlKHsgaWQsIHVybCB9KSB7XG4gICAgY29uc3QgY29uZmlnID0gdGhpcy5nZXRNZXJjaGFudENvbmZpZyh7IGlkIH0pO1xuICAgIGlmIChjb25maWcpIHtcbiAgICAgIGNvbnN0IFtwYWdlTmFtZV0gPSBPYmplY3RcbiAgICAgICAgLmVudHJpZXMoY29uZmlnLnBhZ2VzIHx8IHt9KVxuICAgICAgICAuZmluZCgoWywgcGF0dGVybl0pID0+IHVybC5zZWFyY2gocGF0dGVybikgPiAtMSkgfHwgW107XG4gICAgICByZXR1cm4gcGFnZU5hbWU7XG4gICAgfVxuICAgIHJldHVybiBudWxsO1xuICB9XG5cbiAgZ2V0KHVybCkge1xuICAgIGNvbnN0IG1lcmNoYW50ID0gdGhpcy5nZXRCeVVybCh1cmwpO1xuICAgIHJldHVybiAhbWVyY2hhbnQgPyB0aGlzLmdldEJ5U3BlY2lhbFBhZ2UodXJsKSA6IG1lcmNoYW50O1xuICB9XG5cbiAgZ2V0QnlJZChpZCkge1xuICAgIHJldHVybiB0aGlzLmRhdGEuZmluZCgoeyBpZDogbWVyY2hhbnRJZCB9KSA9PiBpZCA9PT0gbWVyY2hhbnRJZCk7XG4gIH1cblxuICBnZXRCeVNwZWNpYWxQYWdlKHVybCkge1xuICAgIHRyeSB7XG4gICAgICBjb25zdCBjb25maWcgPSB0aGlzLmNvbmZpZ3MuZGF0YVxuICAgICAgICAuZmluZCgoeyBwYWdlcyA9IHt9IH0pID0+IE9iamVjdC52YWx1ZXMocGFnZXMpLnNvbWUoKHBhdHRlcm4pID0+IHVybC5zZWFyY2gocGF0dGVybikgPiAtMSkpO1xuICAgICAgcmV0dXJuIGNvbmZpZz8ubWlkICYmIHRoaXMuZ2V0QnlJZChjb25maWc/Lm1pZCk7XG4gICAgfSBjYXRjaCAoZSkge1xuICAgICAgcmV0dXJuIG51bGw7XG4gICAgfVxuICB9XG5cbiAgZ2V0QnlVcmwodXJsKSB7XG4gICAgdHJ5IHtcbiAgICAgIGNvbnN0IHsgaG9zdCB9ID0gbmV3IFVSTCh1cmwpO1xuICAgICAgcmV0dXJuIHRoaXMuZGF0YVxuICAgICAgICAuZmluZCgoeyBkb21haW4sIG1hdGNoUGF0dGVybiB9KSA9PiBob3N0LmluY2x1ZGVzKGRvbWFpbikgJiYgdXJsLnNlYXJjaChtYXRjaFBhdHRlcm4pID4gLTEpO1xuICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgIHJldHVybiBudWxsO1xuICAgIH1cbiAgfVxuXG4gIGdldE1lcmNoYW50SW5mbyh7IGlkLCB1cmwgfSkge1xuICAgIGxldCBtZXJjaGFudERhdGEgPSBudWxsO1xuICAgIGxldCBtZXJjaGFudFN0YXRlID0gbnVsbDtcbiAgICBsZXQgbWVyY2hhbnRDb25maWcgPSBudWxsO1xuICAgIGlmIChpZCkge1xuICAgICAgbWVyY2hhbnREYXRhID0gdGhpcy5nZXRCeUlkKGlkKTtcbiAgICB9XG4gICAgaWYgKCFtZXJjaGFudERhdGEgJiYgdXJsKSB7XG4gICAgICBtZXJjaGFudERhdGEgPSB0aGlzLmdldEJ5VXJsKHVybCk7XG4gICAgICBtZXJjaGFudERhdGEgPSBtZXJjaGFudERhdGEgfHwgdGhpcy5nZXRCeVNwZWNpYWxQYWdlKHVybCk7XG4gICAgfVxuICAgIGlmIChtZXJjaGFudERhdGEpIHtcbiAgICAgIG1lcmNoYW50Q29uZmlnID0gdGhpcy5jb25maWdzLmRhdGEuZmluZCgoY29uZmlnKSA9PiBtZXJjaGFudERhdGEuaWQgPT09IGNvbmZpZy5taWQpO1xuICAgICAgbWVyY2hhbnRTdGF0ZSA9IHRoaXMuc3RhdGVzW21lcmNoYW50RGF0YS5pZF07XG4gICAgICByZXR1cm4geyAuLi5tZXJjaGFudERhdGEsIC4uLm1lcmNoYW50U3RhdGUsIGNvbmZpZzogbWVyY2hhbnRDb25maWcgfTtcbiAgICB9XG4gICAgcmV0dXJuIG51bGw7XG4gIH1cblxuICBnZXRNZXJjaGFudFdpdGhTdGF0ZSh1cmwpIHtcbiAgICBjb25zdCBtZXJjaGFudERhdGEgPSB0aGlzLmdldCh1cmwpO1xuICAgIGNvbnN0IG1lcmNoYW50U3RhdGUgPSB0aGlzLnN0YXRlc1ttZXJjaGFudERhdGE/LmlkXTtcbiAgICByZXR1cm4geyBtZXJjaGFudERhdGEsIG1lcmNoYW50U3RhdGUgfTtcbiAgfVxuXG4gIGFzeW5jIGFjdGl2YXRpb25UaW1lb3V0KHsgaWQsIHRpbWUsIGFjdGl2YXRpb25Qcm9ncmVzcyA9IDAgfSkge1xuICAgIGlmIChhY3RpdmF0aW9uUHJvZ3Jlc3MgPCAxMDAgJiYgIXRoaXMuc3RhdGVzW2lkXS5hY3RpdmF0ZWQpIHtcbiAgICAgIHRoaXMuc2V0U3RhdGUoeyBpZCwgZGF0YTogeyBhY3RpdmF0aW9uUHJvZ3Jlc3MgfSB9KTtcbiAgICAgIGF3YWl0IGFzeW5jVGltZW91dCh0aW1lIC8gMjApO1xuICAgICAgcmV0dXJuIHRoaXMuYWN0aXZhdGlvblRpbWVvdXQoeyBpZCwgdGltZSwgYWN0aXZhdGlvblByb2dyZXNzOiBhY3RpdmF0aW9uUHJvZ3Jlc3MgKyA1IH0pO1xuICAgIH1cbiAgICByZXR1cm4gdHJ1ZTtcbiAgfVxuXG4gIGFzeW5jIGFjdGl2YXRlKHsgaWQgfSkge1xuICAgIC8vIFRPRE86IEFjdGl2YXRlIG9ubHkgaWYgYWN0aXZhdGlvbiBzdGF0ZSBmYWxzZVxuICAgIGNvbnN0IHsgYWN0aXZhdGlvblVybDogdXJsIH0gPSB0aGlzLmRhdGEuZmluZCgoeyBpZDogbWlkIH0pID0+IG1pZCA9PT0gaWQpO1xuICAgIGNvbnN0IHsgaWQ6IHRhYklkIH0gPSBhd2FpdCBicm93c2VyLnRhYnMuY3JlYXRlKHsgdXJsLCBhY3RpdmU6IGZhbHNlLCBwaW5uZWQ6IHRydWUgfSk7XG4gICAgdGhpcy5zZXRTdGF0ZSh7IGlkLCBkYXRhOiB7IGFjdGl2YXRpb246IHRydWUgfSB9KTtcbiAgICBjb25zb2xlLnRpbWUoJ2FjdGl2YXRlJyk7XG4gICAgYXdhaXQgdGhpcy5hY3RpdmF0aW9uVGltZW91dCh7IGlkLCB0aW1lOiAxMDAwMCB9KTtcbiAgICBjb25zb2xlLnRpbWVFbmQoJ2FjdGl2YXRlJyk7XG4gICAgdHJ5IHtcbiAgICAgIGNvbnN0IGFjdGl2YXRpb25UYWIgPSBhd2FpdCBicm93c2VyLnRhYnMuZ2V0KHRhYklkKTtcbiAgICAgIGlmICghYWN0aXZhdGlvblRhYi5hY3RpdmUpIHtcbiAgICAgICAgYXdhaXQgYnJvd3Nlci50YWJzLnJlbW92ZSh0YWJJZCk7XG4gICAgICB9XG4gICAgfSBjYXRjaCAoZSkge1xuICAgICAgY29uc29sZS5sb2coZSk7XG4gICAgfVxuICAgIGF3YWl0IHRoaXMuY2hlY2tNZXJjaGFudENvb2tpZXMoaWQpO1xuICAgIHRoaXMuc2V0U3RhdGUoeyBpZCwgZGF0YTogeyBhY3RpdmF0aW9uOiBmYWxzZSwgYWN0aXZhdGlvblByb2dyZXNzOiAwIH0gfSk7XG4gIH1cblxuICBnZXRNZXJjaGFudENvbmZpZyA9ICh7IGlkLCB1cmwsIG1vZHVsZU5hbWUgfSkgPT4ge1xuICAgIGxldCBtaWQgPSBpZDtcbiAgICBpZiAoIWlkICYmICF1cmwpIHtcbiAgICAgIHJldHVybiBudWxsO1xuICAgIH1cbiAgICBpZiAoIW1pZCAmJiB1cmwpIHtcbiAgICAgIGNvbnN0IHsgaG9zdCB9ID0gbmV3IFVSTCh1cmwpO1xuICAgICAgY29uc3QgbWVyY2hhbnQgPSB0aGlzLmRhdGEuZmluZCgoeyBkb21haW4gfSkgPT4gaG9zdC5pbmNsdWRlcyhkb21haW4pKTtcbiAgICAgIG1pZCA9IG1lcmNoYW50Py5pZDtcbiAgICB9XG4gICAgaWYgKG1pZCkge1xuICAgICAgY29uc3QgbWVyY2hhbnRDb25maWcgPSB0aGlzLmNvbmZpZ3MuZGF0YS5maW5kKChjb25maWcpID0+IG1pZCA9PT0gY29uZmlnLm1pZCk7XG4gICAgICByZXR1cm4gbW9kdWxlTmFtZSA/IG1lcmNoYW50Q29uZmlnPy5jb25maWdzPy5bbW9kdWxlTmFtZV0gOiBtZXJjaGFudENvbmZpZztcbiAgICB9XG4gICAgcmV0dXJuIG51bGw7XG4gIH1cblxuICBhc3luYyBjaGVja01lcmNoYW50Q29va2llcyhpZCkge1xuICAgIGNvbnN0IG1lcmNoYW50Q29uZmlnID0gdGhpcy5jb25maWdzLmRhdGEuZmluZCgoeyBtaWQgfSkgPT4gbWlkID09PSBpZCk7XG4gICAgaWYgKG1lcmNoYW50Q29uZmlnKSB7XG4gICAgICBjb25zdCBjaGVja2VyID0gbmV3IENvb2tpZUNoZWNrZXIobWVyY2hhbnRDb25maWcuY29uZmlncy5jb29raWVDaGVja2VyKTtcbiAgICAgIGNvbnN0IGlzQ29va2llc1ZhbGlkID0gYXdhaXQgY2hlY2tlci5jaGVjaygpO1xuICAgICAgY29uc3Qgc3RhdGUgPSB0aGlzLnN0YXRlc1tpZF07XG4gICAgICBpZiAoaXNDb29raWVzVmFsaWQgJiYgIXN0YXRlLmFjdGl2YXRlZCkge1xuICAgICAgICB0aGlzLnNldFN0YXRlKHsgaWQsIGRhdGE6IHRoaXMuZ2V0RGVmYXVsdFN0YXRlKCdhY3RpdmF0ZWQnKSB9KTtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBhc3luYyBjaGVja0FjdGl2YXRpb25TdGF0dXMoY29va2llKSB7XG4gICAgY29uc3QgeyBkb21haW4sIG5hbWUgfSA9IGNvb2tpZTtcbiAgICAvLyBGaW5kIG1lcmNoYW50IGNvbmZpZyBieSBjaGFuZ2VkIGNvb2tpZVxuICAgIC8vIElmIGNvbmZpZyBmb3VuZCB0aGFuIGFmZmlsaWF0ZSBjb29raWUgY2hhbmdlZFxuICAgIGNvbnN0IG1lcmNoYW50Q29uZmlnID0gdGhpcy5jb25maWdzLmRhdGEuZmluZCgoY29uZmlnKSA9PiB7XG4gICAgICBjb25zdCBjb29raWVDaGVja2VyQ29uZmlnID0gY29uZmlnPy5jb25maWdzPy5jb29raWVDaGVja2VyIHx8IFtdO1xuICAgICAgcmV0dXJuIGNvb2tpZUNoZWNrZXJDb25maWcuc29tZSgoeyBkb21haW46IGRvbWFpblBhdHRlcm4sIGNvb2tpZXMgfSkgPT4ge1xuICAgICAgICBpZiAoZG9tYWluLnNlYXJjaChkb21haW5QYXR0ZXJuKSA+IC0xKSB7XG4gICAgICAgICAgcmV0dXJuIGNvb2tpZXMuc29tZSgoeyBuYW1lUGF0dGVybiB9KSA9PiBuYW1lLnNlYXJjaChuYW1lUGF0dGVybikgPiAtMSk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgfSk7XG4gICAgfSk7XG4gICAgaWYgKG1lcmNoYW50Q29uZmlnKSB7XG4gICAgICBjb25zdCB7IG1pZDogaWQgfSA9IG1lcmNoYW50Q29uZmlnO1xuICAgICAgY29uc3QgY2hlY2tlciA9IG5ldyBDb29raWVDaGVja2VyKG1lcmNoYW50Q29uZmlnLmNvbmZpZ3MuY29va2llQ2hlY2tlcik7XG4gICAgICBjb25zdCBpc0Nvb2tpZXNWYWxpZCA9IGF3YWl0IGNoZWNrZXIuY2hlY2soKTtcbiAgICAgIGNvbnN0IHN0YXRlID0gdGhpcy5zdGF0ZXNbaWRdO1xuICAgICAgaWYgKHN0YXRlLmFjdGl2YXRlZCAhPT0gaXNDb29raWVzVmFsaWQpIHtcbiAgICAgICAgLyogY29uc3QgW3N0YXR1c10gPSBPYmplY3RcbiAgICAgICAgICAuZW50cmllcyh7IGFjdGl2YXRlZDogaXNDb29raWVzVmFsaWQsIHN1cHByZXNzZWQ6IHN0YXRlLmFjdGl2YXRlZCAmJiAhaXNDb29raWVzVmFsaWQgfSlcbiAgICAgICAgICAuZmluZCgoWywgY29uZGl0aW9uXSkgPT4gY29uZGl0aW9uKTsgKi9cbiAgICAgICAgaWYgKGlzQ29va2llc1ZhbGlkKSB7XG4gICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7IGlkLCBkYXRhOiB0aGlzLmdldERlZmF1bHRTdGF0ZSgnYWN0aXZhdGVkJykgfSk7XG4gICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gICAgcmV0dXJuIGZhbHNlO1xuICB9XG59XG4iLCJpbXBvcnQgeyBOT1RJRklDQVRJT05TIH0gZnJvbSAnLi4vY29uc3RhbnRzJztcblxuY2xhc3MgTm90aWZpY2F0b3Ige1xuICBjb25zdHJ1Y3RvcigpIHtcbiAgICB0aGlzLnRpbWVySWRCeVRhYklkID0ge307XG4gIH1cblxuICBhc3luYyBzZW5kTm90aWZpY2F0aW9uKGRhdGEgPSB7fSkge1xuICAgIGNvbnN0IHsgdG8sIHNlbmRlciwgZm9yY2VkVGFiSWQgfSA9IGRhdGE7XG5cbiAgICBpZiAodG8gPT09ICdwb3B1cCcpIHtcbiAgICAgIHRoaXMuc2VuZE5vdGlmaWNhdGlvblRvUG9wdXAoZGF0YSk7XG5cbiAgICAgIHJldHVybjtcbiAgICB9XG5cbiAgICBpZiAodG8gPT09ICdjb250ZW50Jykge1xuICAgICAgY29uc3QgdGFiSWQgPSBmb3JjZWRUYWJJZCB8fCBzZW5kZXI/LnRhYj8uaWQ7XG5cbiAgICAgIGlmICh0YWJJZCBpbiB0aGlzLnRpbWVySWRCeVRhYklkKSB7XG4gICAgICAgIGNsZWFyVGltZW91dCh0aGlzLnRpbWVySWRCeVRhYklkW3RhYklkXSk7XG4gICAgICB9XG5cbiAgICAgIHRoaXMuc2VuZE5vdGlmaWNhdGlvblRvQ29udGVudCh7XG4gICAgICAgIC4uLmRhdGEsXG4gICAgICAgIHRhYklkLFxuICAgICAgfSk7XG5cbiAgICAgIGNvbnN0IHRpbWVySWQgPSBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgICAgdGhpcy5zZW5kTm90aWZpY2F0aW9uVG9Db250ZW50KHsgdGFiSWQgfSk7XG4gICAgICAgIGRlbGV0ZSB0aGlzLnRpbWVySWRCeVRhYklkW3RhYklkXTtcbiAgICAgIH0sIDYwMDAwKTtcblxuICAgICAgdGhpcy50aW1lcklkQnlUYWJJZFt0YWJJZF0gPSB0aW1lcklkO1xuXG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgY29uc29sZS53YXJuKGBzZW5kTm90aWZpY2F0aW9uIHdpdGggaW52YWxpZCAndG8nOiAke3RvfWApO1xuICB9XG5cbiAgc2VuZE5vdGlmaWNhdGlvblRvUG9wdXAoZGF0YSkge1xuICAgIGNocm9tZS5ydW50aW1lLnNlbmRNZXNzYWdlKHtcbiAgICAgIGFjdGlvbjogJ3NlbmROb3RpZmljYXRpb25Ub1BvcHVwJyxcbiAgICAgIGRhdGEsXG4gICAgfSk7XG4gIH1cblxuICBhc3luYyBzZW5kTm90aWZpY2F0aW9uVG9Db250ZW50KGRhdGEgPSB7fSkge1xuICAgIGNvbnN0IG5vdGlmaWNhdGlvbnMgPSBbXTtcblxuICAgIGlmIChBcnJheS5pc0FycmF5KGRhdGEucGFyYW1zKSkge1xuICAgICAgZGF0YS5wYXJhbXMuZm9yRWFjaCgoeyBzZWN0aW9uLCBwYXJhbWV0ZXIgfSkgPT4ge1xuICAgICAgICBub3RpZmljYXRpb25zLnB1c2goTk9USUZJQ0FUSU9OU1tzZWN0aW9uXVtwYXJhbWV0ZXJdKTtcbiAgICAgIH0pO1xuICAgIH1cblxuICAgIGNocm9tZS50YWJzLnNlbmRNZXNzYWdlKGRhdGEudGFiSWQsIHtcbiAgICAgIGFjdGlvbjogJ3NlbmROb3RpZmljYXRpb25Ub0NvbnRlbnQnLFxuICAgICAgZGF0YToge1xuICAgICAgICAuLi5kYXRhLFxuICAgICAgICBub3RpZmljYXRpb25zLFxuICAgICAgfSxcbiAgICB9KTtcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBOb3RpZmljYXRvcjtcbiIsImltcG9ydCBGZXRjaGVyIGZyb20gJy4vZmV0Y2hlcic7XG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFVzZXIgZXh0ZW5kcyBGZXRjaGVyIHtcbiAgY29uc3RydWN0b3IoeyBuYW1lID0gJ3VzZXInLCB1cmwsIG9wdGlvbnMgPSB7fSB9KSB7XG4gICAgc3VwZXIoeyBuYW1lLCB1cmwsIG9wdGlvbnMgfSk7XG4gICAgdGhpcy5kYXRhID0ge307XG4gICAgdGhpcy5vcHRpb25zID0ge1xuICAgICAgdXBkYXRlSW50ZXJ2YWw6IDQ1ICogNjAgKiAxMDAwLCAvLyA0NSBtaW5cbiAgICAgIHVwZGF0ZUVycm9ySW50ZXJ2YWw6IDYwICogMTAwMCwgLy8gNjAgc2VjXG4gICAgICAuLi5vcHRpb25zLFxuICAgIH07XG4gIH1cblxuICBwYXJzZShkYXRhKSB7XG4gICAgLy8gQWxzbyBoYXZlIGNhcmRfdHlwZSBhbmQgY3JlYXRlZFxuICAgIHJldHVybiB7XG4gICAgICBpZDogZGF0YS5pZCxcbiAgICAgIGVtYWlsOiBkYXRhLmVtYWlsLFxuICAgICAgcGhvbmU6IGRhdGEucGhvbmUsXG4gICAgICBmaXJzdE5hbWU6IGRhdGEuZmlyc3RfbmFtZSxcbiAgICAgIGxhc3ROYW1lOiBkYXRhLmxhc3RfbmFtZSxcbiAgICAgIGNhcmRMYXN0RGlnaXRzOiBkYXRhLmNhcmRfbGFzdF9kaWdpdHMsXG4gICAgICBlbWFpbENvbmZpcm1lZDogZGF0YS5lbWFpbF9jb25maXJtZWQsXG4gICAgICBwaG9uZUNvbmZpcm1lZDogZGF0YS5waG9uZV9jb25maXJtZWQsXG4gICAgICBjYXJkTGlua2VkOiBkYXRhLmNhcmRfbGlua2VkLFxuICAgIH07XG4gIH1cbn1cbiIsImltcG9ydCBFdmVudEVtaXR0ZXIgZnJvbSAnZXZlbnRzJztcblxuLy8gVGhpcyBjbGFzcyBzdG9yZXMgaW5mb3JtYXRpb24gYWJvdXQgdmFsaWRhdGVkIGNhcnRzXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBWYWxpZGF0aW9uU3RvcmUgZXh0ZW5kcyBFdmVudEVtaXR0ZXIge1xuICBjb25zdHJ1Y3Rvcih7IGdldE1lcmNoYW50Q29uZmlnIH0pIHtcbiAgICBzdXBlcigpO1xuICAgIHRoaXMudmFsaWRhdGVkTWVyY2hhbnRzID0ge307XG4gICAgdGhpcy5nZXRNZXJjaGFudENvbmZpZyA9IGdldE1lcmNoYW50Q29uZmlnO1xuICAgIHRoaXMuYmFzaWNQYXJhbWV0ZXJzID0gWydhY3RpdmF0ZWQnLCAnY2FyZCddO1xuICB9XG5cbiAgYXN5bmMgdXBkYXRlKHsgbWlkLCByZXN1bHQgfSkge1xuICAgIHRoaXMudmFsaWRhdGVkTWVyY2hhbnRzW21pZF0gPSB0aGlzLnZhbGlkYXRlZE1lcmNoYW50c1ttaWRdIHx8IHt9O1xuICAgIE9iamVjdC5hc3NpZ24odGhpcy52YWxpZGF0ZWRNZXJjaGFudHNbbWlkXSwgcmVzdWx0KTtcbiAgICBjb25zdCBwYXJhbXMgPSBhd2FpdCB0aGlzLnZhbGlkYXRlQWxsUGFyYW1zKHsgbWlkIH0pO1xuICAgIHRoaXMuZW1pdCgndXBkYXRlJywgeyBtaWQsIHBhcmFtcyB9KTtcbiAgfVxuXG4gIGFzeW5jIGdldFZhbGlkYXRpb25QYXJhbXMobWlkKSB7XG4gICAgY29uc3QgY29uZmlnID0gYXdhaXQgdGhpcy5nZXRNZXJjaGFudENvbmZpZyh7IGlkOiBtaWQsIG1vZHVsZU5hbWU6ICd2YWxpZGF0b3InIH0pIHx8IFtdO1xuICAgIGNvbnN0IG1lcmNoYW50UGFyYW1zID0gT2JqZWN0LnZhbHVlcyhjb25maWcpLm1hcCgoeyBpdGVtcyB9KSA9PiBPYmplY3Qua2V5cyhpdGVtcykpLmZsYXQoKTtcbiAgICByZXR1cm4gdGhpcy5iYXNpY1BhcmFtZXRlcnMuY29uY2F0KG1lcmNoYW50UGFyYW1zKTtcbiAgfVxuXG4gIGFzeW5jIHZhbGlkYXRlQWxsUGFyYW1zKHsgbWlkIH0pIHtcbiAgICBjb25zdCBwYXJhbXMgPSBhd2FpdCB0aGlzLmdldFZhbGlkYXRpb25QYXJhbXMobWlkKTtcbiAgICByZXR1cm4gT2JqZWN0LmZyb21FbnRyaWVzKHBhcmFtcy5tYXAoKG5hbWUpID0+IFtcbiAgICAgIG5hbWUsXG4gICAgICAhIXRoaXMudmFsaWRhdGVkTWVyY2hhbnRzPy5bbWlkXT8uW25hbWVdLFxuICAgIF0pKTtcbiAgfVxuXG4gIGFzeW5jIGlzQWxsUGFyYW1zVmFsaWQoeyBtaWQgfSkge1xuICAgIGNvbnN0IHBhcmFtcyA9IGF3YWl0IHRoaXMuZ2V0VmFsaWRhdGlvblBhcmFtcyhtaWQpO1xuICAgIHJldHVybiBwYXJhbXMuZXZlcnkoKG5hbWUpID0+IHRoaXMudmFsaWRhdGVkTWVyY2hhbnRzW21pZF1bbmFtZV0pO1xuICB9XG59XG4iLCIvLyBUT0RPOiBNYWtlIGR5bmFtaWMgYXBpIHVybCBmb3IgZGV2L3Byb2RcbmNvbnN0IEFQSV9ST09UID0gJ2h0dHBzOi8vYXBpLWRldi5nZXRkaXNjby5jb20vYXBpL3YxJztcbmNvbnN0IERPTUFJTiA9ICdnZXRkaXNjby5jb20nO1xuY29uc3QgVVNFUl9VUkwgPSBgJHtBUElfUk9PVH0vbWVgO1xuY29uc3QgTUVSQ0hBTlRTX1VSTCA9ICcvbWVyY2hhbnRzLmpzb24nO1xuY29uc3QgTUVSQ0hBTlRTX0NPTkZJR1NfVVJMID0gJy9jb25maWdzLmpzb24nO1xuY29uc3QgT0ZGRVJTX1VSTCA9ICcvb2ZmZXJzLmpzb24nO1xuY29uc3QgU0VSUF9DT05GSUdTX1VSTCA9ICcvc2VycC5qc29uJztcbmNvbnN0IENIRUNLX0NBUkRfRVJST1JTID0ge1xuICB3cm9uZ051bWJlcjogJ1lvdSBlbnRlcmVkIHdyb25nIGNhcmQgbnVtYmVyJyxcbiAgd3JvbmdQYXJhbXM6ICdZb3UgZW50ZXIgd3JvbmcgY2FyZCBudW1iZXInLFxufTtcbmNvbnN0IFNVUFBSRVNTSU9OX1BBVFRFUk5TID0gW1xuICAnYXdpbjEuY29tJyxcbiAgJ2txenlmai5jb20nLFxuICAnZG90b21pLmNvbScsXG4gICdlbWpjZC5jb20nLFxuICAnYWZzcmM9MScsXG4gICdvaG9zdD13d3cuZ29vZ2xlLmNvbScsXG5dO1xuY29uc3QgTk9USUZJQ0FUSU9OUyA9IHtcbiAgaW52YWxpZFBhcmFtczoge1xuICAgIGFjdGl2YXRlZDogJ1BsZWFzZSBhY3RpdmF0ZSBtZXJjaGFudCB0byBnZXQgZGlzY291bnQuJyxcbiAgICBjYXJkOiAnUGxlYXNlIHVzZSBEaXNjbyBjYXJkIHRvIGdldCBkaXNjb3VudC4nLFxuICAgIGRlbGl2ZXJ5OiAnUGxlYXNlIGNob29zZSBkZWxpdmVyeSB0byBnZXQgZGlzY291bnQnLFxuICAgIHByb21vOiAnWW91IGNhbiBub3QgZ2V0IGRpc2NvdW50IGlmIHlvdSB1c2UgcHJvbW8gY29kZScsXG4gIH0sXG59O1xuXG5leHBvcnQge1xuICBBUElfUk9PVCxcbiAgVVNFUl9VUkwsXG4gIENIRUNLX0NBUkRfRVJST1JTLFxuICBET01BSU4sXG4gIE9GRkVSU19VUkwsXG4gIE1FUkNIQU5UU19VUkwsXG4gIE1FUkNIQU5UU19DT05GSUdTX1VSTCxcbiAgU1VQUFJFU1NJT05fUEFUVEVSTlMsXG4gIFNFUlBfQ09ORklHU19VUkwsXG4gIE5PVElGSUNBVElPTlMsXG59O1xuIiwiLyogZXNsaW50LWRpc2FibGUgY2xhc3MtbWV0aG9kcy11c2UtdGhpcyAqL1xuaW1wb3J0IHsgQVBJX1JPT1QgfSBmcm9tICcuLi9jb25zdGFudHMnO1xuXG5jbGFzcyBBUEkge1xuICAvLyBjb25zdHJ1Y3RvcigpIHt9XG5cbiAgZ2V0RGlzY291bnRCeUNhcnRJZChjYXJ0SWQsIGRhdGEpIHtcbiAgICAvLyBkYXRhLml0ZW1zXG4gICAgLy8gZGF0YS50b3RhbFxuICAgIC8vIGRhdGEubWVyY2hhbnRfaWRcblxuICAgIHJldHVybiB0aGlzLmNhbGwoYC9kaXNjb3VudC9jYXJ0LyR7Y2FydElkfWAsIHtcbiAgICAgIG1ldGhvZDogJ1BPU1QnLFxuICAgICAgYm9keTogSlNPTi5zdHJpbmdpZnkoZGF0YSksXG4gICAgfSk7XG4gIH1cblxuICBjYWxsKGVuZHBvaW50LCBvcHRpb25zID0ge30pIHtcbiAgICBjb25zdCB1cmwgPSBgJHtBUElfUk9PVH0ke2VuZHBvaW50fWA7XG5cbiAgICByZXR1cm4gZmV0Y2godXJsLCBvcHRpb25zKS50aGVuKChyZXNwb25zZSkgPT4ge1xuICAgICAgLy8gcmVzcG9uc2Uuc3RhdHVzIOKAkyBIVFRQIGNvZGUgb2YgdGhlIHJlc3BvbnNlXG4gICAgICAvLyByZXNwb25zZS5vayDigJMgdHJ1ZSBpZiB0aGUgc3RhdHVzIGlzIDIwMC0yOTlcblxuICAgICAgaWYgKCFyZXNwb25zZS5vaykge1xuICAgICAgICByZXR1cm4gUHJvbWlzZS5yZWplY3QoXG4gICAgICAgICAgbmV3IEVycm9yKGBCYWQgc3RhdHVzIGZvciBhcGkgcmVzcG9uc2U6ICR7cmVzcG9uc2Uuc3RhdHVzfWApLFxuICAgICAgICApO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gcmVzcG9uc2UuanNvbigpO1xuICAgIH0pLmNhdGNoKChlcnJvcikgPT4ge1xuICAgICAgLy8gVE9ETzogQWRkIGxvZ2dlclxuXG4gICAgICB3aW5kb3cuYXBwLm5vdGlmaWNhdG9yLnNlbmROb3RpZmljYXRpb24oe1xuICAgICAgICB0bzogJ3BvcHVwJyxcbiAgICAgICAgLy8gRXJyb3IgLT4gT2JqZWN0XG4gICAgICAgIGVycm9yOiB7XG4gICAgICAgICAgbmFtZTogZXJyb3IubmFtZSxcbiAgICAgICAgICBtZXNzYWdlOiBlcnJvci5tZXNzYWdlLFxuICAgICAgICB9LFxuICAgICAgfSk7XG4gICAgfSk7XG4gIH1cbn1cblxuY29uc3QgYXBpID0gbmV3IEFQSSgpO1xuXG5leHBvcnQgZGVmYXVsdCBhcGk7XG4iXSwic291cmNlUm9vdCI6IiJ9