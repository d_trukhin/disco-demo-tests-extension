/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(Object.prototype.hasOwnProperty.call(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"frame": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push(["./source/content/frame.js","vendors"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "./source/content/cardChecker/index.js":
/*!*********************************************!*\
  !*** ./source/content/cardChecker/index.js ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return CardChecker; });
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/defineProperty */ "./node_modules/@babel/runtime/helpers/defineProperty.js");
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/asyncToGenerator */ "./node_modules/@babel/runtime/helpers/asyncToGenerator.js");
/* harmony import */ var _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ "./node_modules/@babel/runtime/helpers/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime/helpers/createClass */ "./node_modules/@babel/runtime/helpers/createClass.js");
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @babel/runtime/helpers/inherits */ "./node_modules/@babel/runtime/helpers/inherits.js");
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @babel/runtime/helpers/possibleConstructorReturn */ "./node_modules/@babel/runtime/helpers/possibleConstructorReturn.js");
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @babel/runtime/helpers/getPrototypeOf */ "./node_modules/@babel/runtime/helpers/getPrototypeOf.js");
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var events__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! events */ "./node_modules/events/events.js");
/* harmony import */ var events__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(events__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _modules_parser_index__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../modules/parser/index */ "./source/modules/parser/index.js");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../utils */ "./source/utils.js");









function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_7___default()(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_7___default()(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_6___default()(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }





var CardChecker = /*#__PURE__*/function (_EventEmitter) {
  _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_5___default()(CardChecker, _EventEmitter);

  var _super = _createSuper(CardChecker);

  function CardChecker() {
    var _this;

    var config = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    var user = arguments.length > 1 ? arguments[1] : undefined;

    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_3___default()(this, CardChecker);

    _this = _super.call(this);
    _this.config = config;
    _this.user = user;
    ['isValid', 'errorName', 'parsedData'].forEach(function (name) {
      return _this.defineObservedProperty(name);
    });

    _this.setEvents();

    return _this;
  }

  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_4___default()(CardChecker, [{
    key: "setEvents",
    value: function () {
      var _setEvents = _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2___default()( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function _callee() {
        var _this2 = this;

        var elements;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                elements = [];
                Object.values(this.config.items).forEach(function (_ref) {
                  var selector = _ref.selector;
                  var el = document.querySelector(selector);

                  if (el) {
                    elements.push(el);
                  }
                });

                if (!elements.length) {
                  _context.next = 4;
                  break;
                }

                return _context.abrupt("return", elements.forEach(function (el) {
                  return el.addEventListener('input', _this2.process.bind(_this2));
                }));

              case 4:
                _context.next = 6;
                return Object(_utils__WEBPACK_IMPORTED_MODULE_10__["asyncTimeout"])(200);

              case 6:
                return _context.abrupt("return", this.setEvents());

              case 7:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function setEvents() {
        return _setEvents.apply(this, arguments);
      }

      return setEvents;
    }()
  }, {
    key: "process",
    value: function process() {
      if (this.user && this.isCardFieldAvailable()) {
        var parser = new _modules_parser_index__WEBPACK_IMPORTED_MODULE_9__["default"](this.config);
        var parsedData = this.handleData(parser.parseItems());
        var cardNumber = parsedData.cardNumber,
            partNumber = parsedData.partNumber;
        var isValid = !!(cardNumber || partNumber);

        if (isValid) {
          isValid = this.checkIfCardPaymentSelected(); // console.log('checkIfCardPaymentSelected', isValid);

          isValid = partNumber ? this.validatePartNumber(parsedData) : isValid; // console.log(`validatePartNumber: ${partNumber}`, isValid);

          isValid = cardNumber ? this.validateFullNumber(parsedData) : isValid; // console.log(`validateFullNumber: ${cardNumber}`, isValid);
        }

        this.isValid = isValid;

        if (this.isValid) {
          this.parsedData = parsedData;
        }

        return this.parsedData;
      }

      return null;
    }
  }, {
    key: "isCardFieldAvailable",
    value: function isCardFieldAvailable() {
      var cardNumber = this.config.items.cardNumber;
      return document.querySelector(cardNumber === null || cardNumber === void 0 ? void 0 : cardNumber.selector);
    } // Usually use for guest checkout

  }, {
    key: "validateFullNumber",
    value: function validateFullNumber() {
      var parsedData = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      var rx = new RegExp("".concat(this.user.cardLastDigits, "$"));
      return parsedData.cardNumber.search(rx) > -1;
    } // Usually use for signed in user

  }, {
    key: "validatePartNumber",
    value: function validatePartNumber() {
      var parsedData = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      if (this.config.partNumber) {
        var partNumber = parsedData.partNumber;
        return this.user.cardLastDigits === partNumber;
      }

      return true;
    } // check f user selected pament type = card

  }, {
    key: "checkIfCardPaymentSelected",
    value: function checkIfCardPaymentSelected() {
      if (this.config.paymentSelector) {
        var selector = this.config.paymentSelector.selector;
        return document.querySelector(selector);
      }

      return true;
    }
  }, {
    key: "handleData",
    value: function handleData() {
      var parsedData = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      var expiryMonth = parsedData.expiryMonth,
          expiryYear = parsedData.expiryYear;
      var expiry = "".concat(expiryMonth, "/").concat(expiryYear);
      return _objectSpread({
        expiry: expiry
      }, parsedData);
    }
  }, {
    key: "defineObservedProperty",
    value: function defineObservedProperty(name) {
      var _this3 = this;

      Object.defineProperty(this, name, {
        get: function get() {
          return _this3["_".concat(name)];
        },
        set: function set(value) {
          if (_this3["_".concat(name)] !== value) {
            _this3["_".concat(name)] = value;

            _this3.emit("update:".concat(name), value);
          }
        }
      });
    }
  }]);

  return CardChecker;
}(events__WEBPACK_IMPORTED_MODULE_8___default.a);



/***/ }),

/***/ "./source/content/frame.js":
/*!*********************************!*\
  !*** ./source/content/frame.js ***!
  \*********************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/defineProperty */ "./node_modules/@babel/runtime/helpers/defineProperty.js");
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/asyncToGenerator */ "./node_modules/@babel/runtime/helpers/asyncToGenerator.js");
/* harmony import */ var _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ "./node_modules/@babel/runtime/helpers/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime/helpers/createClass */ "./node_modules/@babel/runtime/helpers/createClass.js");
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../utils */ "./source/utils.js");
/* harmony import */ var _cardChecker_index__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./cardChecker/index */ "./source/content/cardChecker/index.js");






function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }




var Frame = /*#__PURE__*/function () {
  function Frame() {
    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_3___default()(this, Frame);

    this.merchant = null;
    this.modules = {};
    this.setMessagesListener();
  }

  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_4___default()(Frame, [{
    key: "process",
    value: function () {
      var _process = _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2___default()( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function _callee(_ref) {
        var url;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                url = _ref.url;

                if (!(!this.merchant && !this.user && !this.modules.cardChecker)) {
                  _context.next = 7;
                  break;
                }

                _context.next = 4;
                return this.updateMerchantInfo(url);

              case 4:
                _context.next = 6;
                return this.updateUserInfo();

              case 6:
                if (this.merchant) {
                  this.initCardChecker();
                }

              case 7:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function process(_x) {
        return _process.apply(this, arguments);
      }

      return process;
    }()
  }, {
    key: "initCardChecker",
    value: function initCardChecker() {
      var _this = this;

      var cardChecker = this.merchant.config.configs.cardChecker;

      if (cardChecker) {
        this.modules.cardChecker = new _cardChecker_index__WEBPACK_IMPORTED_MODULE_6__["default"](cardChecker, this.user);
        this.modules.cardChecker.on('update:isValid', /*#__PURE__*/function () {
          var _ref2 = _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2___default()( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function _callee2(card) {
            return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    _context2.next = 2;
                    return browser.runtime.sendMessage({
                      module: 'validationStore',
                      action: 'update',
                      data: {
                        mid: _this.merchant.id,
                        result: {
                          card: card
                        }
                      }
                    });

                  case 2:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2);
          }));

          return function (_x2) {
            return _ref2.apply(this, arguments);
          };
        }());
      }
    }
  }, {
    key: "updateMerchantInfo",
    value: function () {
      var _updateMerchantInfo = _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2___default()( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function _callee3(url) {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.next = 2;
                return browser.runtime.sendMessage({
                  module: 'merchants',
                  action: 'getMerchantInfo',
                  data: {
                    url: url
                  }
                });

              case 2:
                this.merchant = _context3.sent;

              case 3:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this);
      }));

      function updateMerchantInfo(_x3) {
        return _updateMerchantInfo.apply(this, arguments);
      }

      return updateMerchantInfo;
    }()
  }, {
    key: "updateUserInfo",
    value: function () {
      var _updateUserInfo = _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2___default()( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function _callee4() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.next = 2;
                return browser.runtime.sendMessage({
                  action: 'getUserInfo'
                });

              case 2:
                this.user = _context4.sent;

              case 3:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, this);
      }));

      function updateUserInfo() {
        return _updateUserInfo.apply(this, arguments);
      }

      return updateUserInfo;
    }()
  }, {
    key: "ping",
    value: function ping() {
      return true;
    }
  }, {
    key: "setMessagesListener",
    value: function setMessagesListener() {
      var _this2 = this;

      chrome.runtime.onMessage.addListener(function (message, sender, sendResponse) {
        var action = message.action,
            data = message.data;

        if (typeof _this2[action] === 'function') {
          Object(_utils__WEBPACK_IMPORTED_MODULE_5__["asyncWrapper"])(_this2[action].bind(_this2), _objectSpread(_objectSpread({}, data), {}, {
            sender: sender
          })).then(sendResponse);
        }

        return true;
      });
    }
  }]);

  return Frame;
}();

window.frame = new Frame();

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc291cmNlL2NvbnRlbnQvY2FyZENoZWNrZXIvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vc291cmNlL2NvbnRlbnQvZnJhbWUuanMiXSwibmFtZXMiOlsiQ2FyZENoZWNrZXIiLCJjb25maWciLCJ1c2VyIiwiZm9yRWFjaCIsIm5hbWUiLCJkZWZpbmVPYnNlcnZlZFByb3BlcnR5Iiwic2V0RXZlbnRzIiwiZWxlbWVudHMiLCJPYmplY3QiLCJ2YWx1ZXMiLCJpdGVtcyIsInNlbGVjdG9yIiwiZWwiLCJkb2N1bWVudCIsInF1ZXJ5U2VsZWN0b3IiLCJwdXNoIiwibGVuZ3RoIiwiYWRkRXZlbnRMaXN0ZW5lciIsInByb2Nlc3MiLCJiaW5kIiwiYXN5bmNUaW1lb3V0IiwiaXNDYXJkRmllbGRBdmFpbGFibGUiLCJwYXJzZXIiLCJQYXJzZXIiLCJwYXJzZWREYXRhIiwiaGFuZGxlRGF0YSIsInBhcnNlSXRlbXMiLCJjYXJkTnVtYmVyIiwicGFydE51bWJlciIsImlzVmFsaWQiLCJjaGVja0lmQ2FyZFBheW1lbnRTZWxlY3RlZCIsInZhbGlkYXRlUGFydE51bWJlciIsInZhbGlkYXRlRnVsbE51bWJlciIsInJ4IiwiUmVnRXhwIiwiY2FyZExhc3REaWdpdHMiLCJzZWFyY2giLCJwYXltZW50U2VsZWN0b3IiLCJleHBpcnlNb250aCIsImV4cGlyeVllYXIiLCJleHBpcnkiLCJkZWZpbmVQcm9wZXJ0eSIsImdldCIsInNldCIsInZhbHVlIiwiZW1pdCIsIkV2ZW50RW1pdHRlciIsIkZyYW1lIiwibWVyY2hhbnQiLCJtb2R1bGVzIiwic2V0TWVzc2FnZXNMaXN0ZW5lciIsInVybCIsImNhcmRDaGVja2VyIiwidXBkYXRlTWVyY2hhbnRJbmZvIiwidXBkYXRlVXNlckluZm8iLCJpbml0Q2FyZENoZWNrZXIiLCJjb25maWdzIiwib24iLCJjYXJkIiwiYnJvd3NlciIsInJ1bnRpbWUiLCJzZW5kTWVzc2FnZSIsIm1vZHVsZSIsImFjdGlvbiIsImRhdGEiLCJtaWQiLCJpZCIsInJlc3VsdCIsImNocm9tZSIsIm9uTWVzc2FnZSIsImFkZExpc3RlbmVyIiwibWVzc2FnZSIsInNlbmRlciIsInNlbmRSZXNwb25zZSIsImFzeW5jV3JhcHBlciIsInRoZW4iLCJ3aW5kb3ciLCJmcmFtZSJdLCJtYXBwaW5ncyI6IjtRQUFBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsUUFBUSxvQkFBb0I7UUFDNUI7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQSxpQkFBaUIsNEJBQTRCO1FBQzdDO1FBQ0E7UUFDQSxrQkFBa0IsMkJBQTJCO1FBQzdDO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTs7UUFFQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7OztRQUdBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwwQ0FBMEMsZ0NBQWdDO1FBQzFFO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0Esd0RBQXdELGtCQUFrQjtRQUMxRTtRQUNBLGlEQUFpRCxjQUFjO1FBQy9EOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQSx5Q0FBeUMsaUNBQWlDO1FBQzFFLGdIQUFnSCxtQkFBbUIsRUFBRTtRQUNySTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDJCQUEyQiwwQkFBMEIsRUFBRTtRQUN2RCxpQ0FBaUMsZUFBZTtRQUNoRDtRQUNBO1FBQ0E7O1FBRUE7UUFDQSxzREFBc0QsK0RBQStEOztRQUVySDtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0EsZ0JBQWdCLHVCQUF1QjtRQUN2Qzs7O1FBR0E7UUFDQTtRQUNBO1FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN2SkE7QUFDQTtBQUNBOztJQUVxQkEsVzs7Ozs7QUFDbkIseUJBQStCO0FBQUE7O0FBQUEsUUFBbkJDLE1BQW1CLHVFQUFWLEVBQVU7QUFBQSxRQUFOQyxJQUFNOztBQUFBOztBQUM3QjtBQUNBLFVBQUtELE1BQUwsR0FBY0EsTUFBZDtBQUNBLFVBQUtDLElBQUwsR0FBWUEsSUFBWjtBQUNBLEtBQUMsU0FBRCxFQUFZLFdBQVosRUFBeUIsWUFBekIsRUFDR0MsT0FESCxDQUNXLFVBQUNDLElBQUQ7QUFBQSxhQUFVLE1BQUtDLHNCQUFMLENBQTRCRCxJQUE1QixDQUFWO0FBQUEsS0FEWDs7QUFFQSxVQUFLRSxTQUFMOztBQU42QjtBQU85Qjs7Ozs7Ozs7Ozs7OztBQUdPQyx3QixHQUFXLEU7QUFDakJDLHNCQUFNLENBQ0hDLE1BREgsQ0FDVSxLQUFLUixNQUFMLENBQVlTLEtBRHRCLEVBRUdQLE9BRkgsQ0FFVyxnQkFBa0I7QUFBQSxzQkFBZlEsUUFBZSxRQUFmQSxRQUFlO0FBQ3pCLHNCQUFNQyxFQUFFLEdBQUdDLFFBQVEsQ0FBQ0MsYUFBVCxDQUF1QkgsUUFBdkIsQ0FBWDs7QUFDQSxzQkFBSUMsRUFBSixFQUFRO0FBQ05MLDRCQUFRLENBQUNRLElBQVQsQ0FBY0gsRUFBZDtBQUNEO0FBQ0YsaUJBUEg7O3FCQVFJTCxRQUFRLENBQUNTLE07Ozs7O2lEQUNKVCxRQUFRLENBQUNKLE9BQVQsQ0FBaUIsVUFBQ1MsRUFBRDtBQUFBLHlCQUFRQSxFQUFFLENBQUNLLGdCQUFILENBQW9CLE9BQXBCLEVBQTZCLE1BQUksQ0FBQ0MsT0FBTCxDQUFhQyxJQUFiLENBQWtCLE1BQWxCLENBQTdCLENBQVI7QUFBQSxpQkFBakIsQzs7Ozt1QkFFSEMsNERBQVksQ0FBQyxHQUFELEM7OztpREFDWCxLQUFLZCxTQUFMLEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs4QkFHQztBQUNSLFVBQUksS0FBS0osSUFBTCxJQUFhLEtBQUttQixvQkFBTCxFQUFqQixFQUE4QztBQUM1QyxZQUFNQyxNQUFNLEdBQUcsSUFBSUMsNkRBQUosQ0FBVyxLQUFLdEIsTUFBaEIsQ0FBZjtBQUNBLFlBQU11QixVQUFVLEdBQUcsS0FBS0MsVUFBTCxDQUFnQkgsTUFBTSxDQUFDSSxVQUFQLEVBQWhCLENBQW5CO0FBRjRDLFlBR3BDQyxVQUhvQyxHQUdUSCxVQUhTLENBR3BDRyxVQUhvQztBQUFBLFlBR3hCQyxVQUh3QixHQUdUSixVQUhTLENBR3hCSSxVQUh3QjtBQUk1QyxZQUFJQyxPQUFPLEdBQUcsQ0FBQyxFQUFFRixVQUFVLElBQUlDLFVBQWhCLENBQWY7O0FBQ0EsWUFBSUMsT0FBSixFQUFhO0FBQ1hBLGlCQUFPLEdBQUcsS0FBS0MsMEJBQUwsRUFBVixDQURXLENBRVg7O0FBQ0FELGlCQUFPLEdBQUdELFVBQVUsR0FBRyxLQUFLRyxrQkFBTCxDQUF3QlAsVUFBeEIsQ0FBSCxHQUF5Q0ssT0FBN0QsQ0FIVyxDQUlYOztBQUNBQSxpQkFBTyxHQUFHRixVQUFVLEdBQUcsS0FBS0ssa0JBQUwsQ0FBd0JSLFVBQXhCLENBQUgsR0FBeUNLLE9BQTdELENBTFcsQ0FNWDtBQUNEOztBQUNELGFBQUtBLE9BQUwsR0FBZUEsT0FBZjs7QUFDQSxZQUFJLEtBQUtBLE9BQVQsRUFBa0I7QUFDaEIsZUFBS0wsVUFBTCxHQUFrQkEsVUFBbEI7QUFDRDs7QUFDRCxlQUFPLEtBQUtBLFVBQVo7QUFDRDs7QUFDRCxhQUFPLElBQVA7QUFDRDs7OzJDQUVzQjtBQUFBLFVBQ2JHLFVBRGEsR0FDRSxLQUFLMUIsTUFBTCxDQUFZUyxLQURkLENBQ2JpQixVQURhO0FBRXJCLGFBQU9kLFFBQVEsQ0FBQ0MsYUFBVCxDQUF1QmEsVUFBdkIsYUFBdUJBLFVBQXZCLHVCQUF1QkEsVUFBVSxDQUFFaEIsUUFBbkMsQ0FBUDtBQUNELEssQ0FFRDs7Ozt5Q0FDb0M7QUFBQSxVQUFqQmEsVUFBaUIsdUVBQUosRUFBSTtBQUNsQyxVQUFNUyxFQUFFLEdBQUcsSUFBSUMsTUFBSixXQUFjLEtBQUtoQyxJQUFMLENBQVVpQyxjQUF4QixPQUFYO0FBQ0EsYUFBT1gsVUFBVSxDQUFDRyxVQUFYLENBQXNCUyxNQUF0QixDQUE2QkgsRUFBN0IsSUFBbUMsQ0FBQyxDQUEzQztBQUNELEssQ0FFRDs7Ozt5Q0FDb0M7QUFBQSxVQUFqQlQsVUFBaUIsdUVBQUosRUFBSTs7QUFDbEMsVUFBSSxLQUFLdkIsTUFBTCxDQUFZMkIsVUFBaEIsRUFBNEI7QUFBQSxZQUNsQkEsVUFEa0IsR0FDSEosVUFERyxDQUNsQkksVUFEa0I7QUFFMUIsZUFBTyxLQUFLMUIsSUFBTCxDQUFVaUMsY0FBVixLQUE2QlAsVUFBcEM7QUFDRDs7QUFDRCxhQUFPLElBQVA7QUFDRCxLLENBRUQ7Ozs7aURBQzZCO0FBQzNCLFVBQUksS0FBSzNCLE1BQUwsQ0FBWW9DLGVBQWhCLEVBQWlDO0FBQUEsWUFDdkIxQixRQUR1QixHQUNWLEtBQUtWLE1BQUwsQ0FBWW9DLGVBREYsQ0FDdkIxQixRQUR1QjtBQUUvQixlQUFPRSxRQUFRLENBQUNDLGFBQVQsQ0FBdUJILFFBQXZCLENBQVA7QUFDRDs7QUFDRCxhQUFPLElBQVA7QUFDRDs7O2lDQUUyQjtBQUFBLFVBQWpCYSxVQUFpQix1RUFBSixFQUFJO0FBQUEsVUFDbEJjLFdBRGtCLEdBQ1VkLFVBRFYsQ0FDbEJjLFdBRGtCO0FBQUEsVUFDTEMsVUFESyxHQUNVZixVQURWLENBQ0xlLFVBREs7QUFFMUIsVUFBTUMsTUFBTSxhQUFNRixXQUFOLGNBQXFCQyxVQUFyQixDQUFaO0FBQ0E7QUFBU0MsY0FBTSxFQUFOQTtBQUFULFNBQW9CaEIsVUFBcEI7QUFDRDs7OzJDQUVzQnBCLEksRUFBTTtBQUFBOztBQUMzQkksWUFBTSxDQUFDaUMsY0FBUCxDQUFzQixJQUF0QixFQUE0QnJDLElBQTVCLEVBQWtDO0FBQ2hDc0MsV0FBRyxFQUFFO0FBQUEsaUJBQU0sTUFBSSxZQUFLdEMsSUFBTCxFQUFWO0FBQUEsU0FEMkI7QUFFaEN1QyxXQUFHLEVBQUUsYUFBQ0MsS0FBRCxFQUFXO0FBQ2QsY0FBSSxNQUFJLFlBQUt4QyxJQUFMLEVBQUosS0FBcUJ3QyxLQUF6QixFQUFnQztBQUM5QixrQkFBSSxZQUFLeEMsSUFBTCxFQUFKLEdBQW1Cd0MsS0FBbkI7O0FBQ0Esa0JBQUksQ0FBQ0MsSUFBTCxrQkFBb0J6QyxJQUFwQixHQUE0QndDLEtBQTVCO0FBQ0Q7QUFDRjtBQVArQixPQUFsQztBQVNEOzs7O0VBL0ZzQ0UsNkM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNKekM7QUFDQTs7SUFFTUMsSztBQUNKLG1CQUFjO0FBQUE7O0FBQ1osU0FBS0MsUUFBTCxHQUFnQixJQUFoQjtBQUNBLFNBQUtDLE9BQUwsR0FBZSxFQUFmO0FBQ0EsU0FBS0MsbUJBQUw7QUFDRDs7Ozs7Ozs7Ozs7QUFFZUMsbUIsUUFBQUEsRzs7c0JBQ1YsQ0FBQyxLQUFLSCxRQUFOLElBQWtCLENBQUMsS0FBSzlDLElBQXhCLElBQWdDLENBQUMsS0FBSytDLE9BQUwsQ0FBYUcsVzs7Ozs7O3VCQUMxQyxLQUFLQyxrQkFBTCxDQUF3QkYsR0FBeEIsQzs7Ozt1QkFDQSxLQUFLRyxjQUFMLEU7OztBQUNOLG9CQUFJLEtBQUtOLFFBQVQsRUFBbUI7QUFDakIsdUJBQUtPLGVBQUw7QUFDRDs7Ozs7Ozs7Ozs7Ozs7Ozs7O3NDQUlhO0FBQUE7O0FBQUEsVUFDUkgsV0FEUSxHQUNRLEtBQUtKLFFBQUwsQ0FBYy9DLE1BQWQsQ0FBcUJ1RCxPQUQ3QixDQUNSSixXQURROztBQUVoQixVQUFJQSxXQUFKLEVBQWlCO0FBQ2YsYUFBS0gsT0FBTCxDQUFhRyxXQUFiLEdBQTJCLElBQUlwRCwwREFBSixDQUFnQm9ELFdBQWhCLEVBQTZCLEtBQUtsRCxJQUFsQyxDQUEzQjtBQUNBLGFBQUsrQyxPQUFMLENBQWFHLFdBQWIsQ0FDR0ssRUFESCxDQUNNLGdCQUROO0FBQUEsMExBQ3dCLGtCQUFPQyxJQUFQO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDJCQUNkQyxPQUFPLENBQUNDLE9BQVIsQ0FDSEMsV0FERyxDQUNTO0FBQ1hDLDRCQUFNLEVBQUUsaUJBREc7QUFFWEMsNEJBQU0sRUFBRSxRQUZHO0FBR1hDLDBCQUFJLEVBQUU7QUFBRUMsMkJBQUcsRUFBRSxLQUFJLENBQUNqQixRQUFMLENBQWNrQixFQUFyQjtBQUF5QkMsOEJBQU0sRUFBRTtBQUFFVCw4QkFBSSxFQUFKQTtBQUFGO0FBQWpDO0FBSEsscUJBRFQsQ0FEYzs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxXQUR4Qjs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQVNEO0FBQ0Y7Ozs7c05BRXdCUCxHOzs7Ozs7dUJBQ0RRLE9BQU8sQ0FBQ0MsT0FBUixDQUFnQkMsV0FBaEIsQ0FBNEI7QUFDaERDLHdCQUFNLEVBQUUsV0FEd0M7QUFDM0JDLHdCQUFNLEVBQUUsaUJBRG1CO0FBQ0FDLHNCQUFJLEVBQUU7QUFBRWIsdUJBQUcsRUFBSEE7QUFBRjtBQUROLGlCQUE1QixDOzs7QUFBdEIscUJBQUtILFE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7dUJBTWFXLE9BQU8sQ0FBQ0MsT0FBUixDQUFnQkMsV0FBaEIsQ0FBNEI7QUFBRUUsd0JBQU0sRUFBRTtBQUFWLGlCQUE1QixDOzs7QUFBbEIscUJBQUs3RCxJOzs7Ozs7Ozs7Ozs7Ozs7Ozs7MkJBR0E7QUFDTCxhQUFPLElBQVA7QUFDRDs7OzBDQUVxQjtBQUFBOztBQUNwQmtFLFlBQU0sQ0FBQ1IsT0FBUCxDQUFlUyxTQUFmLENBQXlCQyxXQUF6QixDQUFxQyxVQUFDQyxPQUFELEVBQVVDLE1BQVYsRUFBa0JDLFlBQWxCLEVBQW1DO0FBQUEsWUFDOURWLE1BRDhELEdBQzdDUSxPQUQ2QyxDQUM5RFIsTUFEOEQ7QUFBQSxZQUN0REMsSUFEc0QsR0FDN0NPLE9BRDZDLENBQ3REUCxJQURzRDs7QUFFdEUsWUFBSSxPQUFPLE1BQUksQ0FBQ0QsTUFBRCxDQUFYLEtBQXdCLFVBQTVCLEVBQXdDO0FBQ3RDVyxxRUFBWSxDQUFDLE1BQUksQ0FBQ1gsTUFBRCxDQUFKLENBQWE1QyxJQUFiLENBQWtCLE1BQWxCLENBQUQsa0NBQStCNkMsSUFBL0I7QUFBcUNRLGtCQUFNLEVBQU5BO0FBQXJDLGFBQVosQ0FBMkRHLElBQTNELENBQWdFRixZQUFoRTtBQUNEOztBQUNELGVBQU8sSUFBUDtBQUNELE9BTkQ7QUFPRDs7Ozs7O0FBR0hHLE1BQU0sQ0FBQ0MsS0FBUCxHQUFlLElBQUk5QixLQUFKLEVBQWYsQyIsImZpbGUiOiJmcmFtZS9idW5kbGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBpbnN0YWxsIGEgSlNPTlAgY2FsbGJhY2sgZm9yIGNodW5rIGxvYWRpbmdcbiBcdGZ1bmN0aW9uIHdlYnBhY2tKc29ucENhbGxiYWNrKGRhdGEpIHtcbiBcdFx0dmFyIGNodW5rSWRzID0gZGF0YVswXTtcbiBcdFx0dmFyIG1vcmVNb2R1bGVzID0gZGF0YVsxXTtcbiBcdFx0dmFyIGV4ZWN1dGVNb2R1bGVzID0gZGF0YVsyXTtcblxuIFx0XHQvLyBhZGQgXCJtb3JlTW9kdWxlc1wiIHRvIHRoZSBtb2R1bGVzIG9iamVjdCxcbiBcdFx0Ly8gdGhlbiBmbGFnIGFsbCBcImNodW5rSWRzXCIgYXMgbG9hZGVkIGFuZCBmaXJlIGNhbGxiYWNrXG4gXHRcdHZhciBtb2R1bGVJZCwgY2h1bmtJZCwgaSA9IDAsIHJlc29sdmVzID0gW107XG4gXHRcdGZvcig7aSA8IGNodW5rSWRzLmxlbmd0aDsgaSsrKSB7XG4gXHRcdFx0Y2h1bmtJZCA9IGNodW5rSWRzW2ldO1xuIFx0XHRcdGlmKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChpbnN0YWxsZWRDaHVua3MsIGNodW5rSWQpICYmIGluc3RhbGxlZENodW5rc1tjaHVua0lkXSkge1xuIFx0XHRcdFx0cmVzb2x2ZXMucHVzaChpbnN0YWxsZWRDaHVua3NbY2h1bmtJZF1bMF0pO1xuIFx0XHRcdH1cbiBcdFx0XHRpbnN0YWxsZWRDaHVua3NbY2h1bmtJZF0gPSAwO1xuIFx0XHR9XG4gXHRcdGZvcihtb2R1bGVJZCBpbiBtb3JlTW9kdWxlcykge1xuIFx0XHRcdGlmKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChtb3JlTW9kdWxlcywgbW9kdWxlSWQpKSB7XG4gXHRcdFx0XHRtb2R1bGVzW21vZHVsZUlkXSA9IG1vcmVNb2R1bGVzW21vZHVsZUlkXTtcbiBcdFx0XHR9XG4gXHRcdH1cbiBcdFx0aWYocGFyZW50SnNvbnBGdW5jdGlvbikgcGFyZW50SnNvbnBGdW5jdGlvbihkYXRhKTtcblxuIFx0XHR3aGlsZShyZXNvbHZlcy5sZW5ndGgpIHtcbiBcdFx0XHRyZXNvbHZlcy5zaGlmdCgpKCk7XG4gXHRcdH1cblxuIFx0XHQvLyBhZGQgZW50cnkgbW9kdWxlcyBmcm9tIGxvYWRlZCBjaHVuayB0byBkZWZlcnJlZCBsaXN0XG4gXHRcdGRlZmVycmVkTW9kdWxlcy5wdXNoLmFwcGx5KGRlZmVycmVkTW9kdWxlcywgZXhlY3V0ZU1vZHVsZXMgfHwgW10pO1xuXG4gXHRcdC8vIHJ1biBkZWZlcnJlZCBtb2R1bGVzIHdoZW4gYWxsIGNodW5rcyByZWFkeVxuIFx0XHRyZXR1cm4gY2hlY2tEZWZlcnJlZE1vZHVsZXMoKTtcbiBcdH07XG4gXHRmdW5jdGlvbiBjaGVja0RlZmVycmVkTW9kdWxlcygpIHtcbiBcdFx0dmFyIHJlc3VsdDtcbiBcdFx0Zm9yKHZhciBpID0gMDsgaSA8IGRlZmVycmVkTW9kdWxlcy5sZW5ndGg7IGkrKykge1xuIFx0XHRcdHZhciBkZWZlcnJlZE1vZHVsZSA9IGRlZmVycmVkTW9kdWxlc1tpXTtcbiBcdFx0XHR2YXIgZnVsZmlsbGVkID0gdHJ1ZTtcbiBcdFx0XHRmb3IodmFyIGogPSAxOyBqIDwgZGVmZXJyZWRNb2R1bGUubGVuZ3RoOyBqKyspIHtcbiBcdFx0XHRcdHZhciBkZXBJZCA9IGRlZmVycmVkTW9kdWxlW2pdO1xuIFx0XHRcdFx0aWYoaW5zdGFsbGVkQ2h1bmtzW2RlcElkXSAhPT0gMCkgZnVsZmlsbGVkID0gZmFsc2U7XG4gXHRcdFx0fVxuIFx0XHRcdGlmKGZ1bGZpbGxlZCkge1xuIFx0XHRcdFx0ZGVmZXJyZWRNb2R1bGVzLnNwbGljZShpLS0sIDEpO1xuIFx0XHRcdFx0cmVzdWx0ID0gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSBkZWZlcnJlZE1vZHVsZVswXSk7XG4gXHRcdFx0fVxuIFx0XHR9XG5cbiBcdFx0cmV0dXJuIHJlc3VsdDtcbiBcdH1cblxuIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gb2JqZWN0IHRvIHN0b3JlIGxvYWRlZCBhbmQgbG9hZGluZyBjaHVua3NcbiBcdC8vIHVuZGVmaW5lZCA9IGNodW5rIG5vdCBsb2FkZWQsIG51bGwgPSBjaHVuayBwcmVsb2FkZWQvcHJlZmV0Y2hlZFxuIFx0Ly8gUHJvbWlzZSA9IGNodW5rIGxvYWRpbmcsIDAgPSBjaHVuayBsb2FkZWRcbiBcdHZhciBpbnN0YWxsZWRDaHVua3MgPSB7XG4gXHRcdFwiZnJhbWVcIjogMFxuIFx0fTtcblxuIFx0dmFyIGRlZmVycmVkTW9kdWxlcyA9IFtdO1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBnZXR0ZXIgfSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG4gXHRcdH1cbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGNyZWF0ZSBhIGZha2UgbmFtZXNwYWNlIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDE6IHZhbHVlIGlzIGEgbW9kdWxlIGlkLCByZXF1aXJlIGl0XG4gXHQvLyBtb2RlICYgMjogbWVyZ2UgYWxsIHByb3BlcnRpZXMgb2YgdmFsdWUgaW50byB0aGUgbnNcbiBcdC8vIG1vZGUgJiA0OiByZXR1cm4gdmFsdWUgd2hlbiBhbHJlYWR5IG5zIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDh8MTogYmVoYXZlIGxpa2UgcmVxdWlyZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy50ID0gZnVuY3Rpb24odmFsdWUsIG1vZGUpIHtcbiBcdFx0aWYobW9kZSAmIDEpIHZhbHVlID0gX193ZWJwYWNrX3JlcXVpcmVfXyh2YWx1ZSk7XG4gXHRcdGlmKG1vZGUgJiA4KSByZXR1cm4gdmFsdWU7XG4gXHRcdGlmKChtb2RlICYgNCkgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJiB2YWx1ZS5fX2VzTW9kdWxlKSByZXR1cm4gdmFsdWU7XG4gXHRcdHZhciBucyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18ucihucyk7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShucywgJ2RlZmF1bHQnLCB7IGVudW1lcmFibGU6IHRydWUsIHZhbHVlOiB2YWx1ZSB9KTtcbiBcdFx0aWYobW9kZSAmIDIgJiYgdHlwZW9mIHZhbHVlICE9ICdzdHJpbmcnKSBmb3IodmFyIGtleSBpbiB2YWx1ZSkgX193ZWJwYWNrX3JlcXVpcmVfXy5kKG5zLCBrZXksIGZ1bmN0aW9uKGtleSkgeyByZXR1cm4gdmFsdWVba2V5XTsgfS5iaW5kKG51bGwsIGtleSkpO1xuIFx0XHRyZXR1cm4gbnM7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG4gXHR2YXIganNvbnBBcnJheSA9IHdpbmRvd1tcIndlYnBhY2tKc29ucFwiXSA9IHdpbmRvd1tcIndlYnBhY2tKc29ucFwiXSB8fCBbXTtcbiBcdHZhciBvbGRKc29ucEZ1bmN0aW9uID0ganNvbnBBcnJheS5wdXNoLmJpbmQoanNvbnBBcnJheSk7XG4gXHRqc29ucEFycmF5LnB1c2ggPSB3ZWJwYWNrSnNvbnBDYWxsYmFjaztcbiBcdGpzb25wQXJyYXkgPSBqc29ucEFycmF5LnNsaWNlKCk7XG4gXHRmb3IodmFyIGkgPSAwOyBpIDwganNvbnBBcnJheS5sZW5ndGg7IGkrKykgd2VicGFja0pzb25wQ2FsbGJhY2soanNvbnBBcnJheVtpXSk7XG4gXHR2YXIgcGFyZW50SnNvbnBGdW5jdGlvbiA9IG9sZEpzb25wRnVuY3Rpb247XG5cblxuIFx0Ly8gYWRkIGVudHJ5IG1vZHVsZSB0byBkZWZlcnJlZCBsaXN0XG4gXHRkZWZlcnJlZE1vZHVsZXMucHVzaChbXCIuL3NvdXJjZS9jb250ZW50L2ZyYW1lLmpzXCIsXCJ2ZW5kb3JzXCJdKTtcbiBcdC8vIHJ1biBkZWZlcnJlZCBtb2R1bGVzIHdoZW4gcmVhZHlcbiBcdHJldHVybiBjaGVja0RlZmVycmVkTW9kdWxlcygpO1xuIiwiaW1wb3J0IEV2ZW50RW1pdHRlciBmcm9tICdldmVudHMnO1xuaW1wb3J0IFBhcnNlciBmcm9tICcuLi8uLi9tb2R1bGVzL3BhcnNlci9pbmRleCc7XG5pbXBvcnQgeyBhc3luY1RpbWVvdXQgfSBmcm9tICcuLi8uLi91dGlscyc7XG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIENhcmRDaGVja2VyIGV4dGVuZHMgRXZlbnRFbWl0dGVyIHtcbiAgY29uc3RydWN0b3IoY29uZmlnID0ge30sIHVzZXIpIHtcbiAgICBzdXBlcigpO1xuICAgIHRoaXMuY29uZmlnID0gY29uZmlnO1xuICAgIHRoaXMudXNlciA9IHVzZXI7XG4gICAgWydpc1ZhbGlkJywgJ2Vycm9yTmFtZScsICdwYXJzZWREYXRhJ11cbiAgICAgIC5mb3JFYWNoKChuYW1lKSA9PiB0aGlzLmRlZmluZU9ic2VydmVkUHJvcGVydHkobmFtZSkpO1xuICAgIHRoaXMuc2V0RXZlbnRzKCk7XG4gIH1cblxuICBhc3luYyBzZXRFdmVudHMoKSB7XG4gICAgY29uc3QgZWxlbWVudHMgPSBbXTtcbiAgICBPYmplY3RcbiAgICAgIC52YWx1ZXModGhpcy5jb25maWcuaXRlbXMpXG4gICAgICAuZm9yRWFjaCgoeyBzZWxlY3RvciB9KSA9PiB7XG4gICAgICAgIGNvbnN0IGVsID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihzZWxlY3Rvcik7XG4gICAgICAgIGlmIChlbCkge1xuICAgICAgICAgIGVsZW1lbnRzLnB1c2goZWwpO1xuICAgICAgICB9XG4gICAgICB9KTtcbiAgICBpZiAoZWxlbWVudHMubGVuZ3RoKSB7XG4gICAgICByZXR1cm4gZWxlbWVudHMuZm9yRWFjaCgoZWwpID0+IGVsLmFkZEV2ZW50TGlzdGVuZXIoJ2lucHV0JywgdGhpcy5wcm9jZXNzLmJpbmQodGhpcykpKTtcbiAgICB9XG4gICAgYXdhaXQgYXN5bmNUaW1lb3V0KDIwMCk7XG4gICAgcmV0dXJuIHRoaXMuc2V0RXZlbnRzKCk7XG4gIH1cblxuICBwcm9jZXNzKCkge1xuICAgIGlmICh0aGlzLnVzZXIgJiYgdGhpcy5pc0NhcmRGaWVsZEF2YWlsYWJsZSgpKSB7XG4gICAgICBjb25zdCBwYXJzZXIgPSBuZXcgUGFyc2VyKHRoaXMuY29uZmlnKTtcbiAgICAgIGNvbnN0IHBhcnNlZERhdGEgPSB0aGlzLmhhbmRsZURhdGEocGFyc2VyLnBhcnNlSXRlbXMoKSk7XG4gICAgICBjb25zdCB7IGNhcmROdW1iZXIsIHBhcnROdW1iZXIgfSA9IHBhcnNlZERhdGE7XG4gICAgICBsZXQgaXNWYWxpZCA9ICEhKGNhcmROdW1iZXIgfHwgcGFydE51bWJlcik7XG4gICAgICBpZiAoaXNWYWxpZCkge1xuICAgICAgICBpc1ZhbGlkID0gdGhpcy5jaGVja0lmQ2FyZFBheW1lbnRTZWxlY3RlZCgpO1xuICAgICAgICAvLyBjb25zb2xlLmxvZygnY2hlY2tJZkNhcmRQYXltZW50U2VsZWN0ZWQnLCBpc1ZhbGlkKTtcbiAgICAgICAgaXNWYWxpZCA9IHBhcnROdW1iZXIgPyB0aGlzLnZhbGlkYXRlUGFydE51bWJlcihwYXJzZWREYXRhKSA6IGlzVmFsaWQ7XG4gICAgICAgIC8vIGNvbnNvbGUubG9nKGB2YWxpZGF0ZVBhcnROdW1iZXI6ICR7cGFydE51bWJlcn1gLCBpc1ZhbGlkKTtcbiAgICAgICAgaXNWYWxpZCA9IGNhcmROdW1iZXIgPyB0aGlzLnZhbGlkYXRlRnVsbE51bWJlcihwYXJzZWREYXRhKSA6IGlzVmFsaWQ7XG4gICAgICAgIC8vIGNvbnNvbGUubG9nKGB2YWxpZGF0ZUZ1bGxOdW1iZXI6ICR7Y2FyZE51bWJlcn1gLCBpc1ZhbGlkKTtcbiAgICAgIH1cbiAgICAgIHRoaXMuaXNWYWxpZCA9IGlzVmFsaWQ7XG4gICAgICBpZiAodGhpcy5pc1ZhbGlkKSB7XG4gICAgICAgIHRoaXMucGFyc2VkRGF0YSA9IHBhcnNlZERhdGE7XG4gICAgICB9XG4gICAgICByZXR1cm4gdGhpcy5wYXJzZWREYXRhO1xuICAgIH1cbiAgICByZXR1cm4gbnVsbDtcbiAgfVxuXG4gIGlzQ2FyZEZpZWxkQXZhaWxhYmxlKCkge1xuICAgIGNvbnN0IHsgY2FyZE51bWJlciB9ID0gdGhpcy5jb25maWcuaXRlbXM7XG4gICAgcmV0dXJuIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoY2FyZE51bWJlcj8uc2VsZWN0b3IpO1xuICB9XG5cbiAgLy8gVXN1YWxseSB1c2UgZm9yIGd1ZXN0IGNoZWNrb3V0XG4gIHZhbGlkYXRlRnVsbE51bWJlcihwYXJzZWREYXRhID0ge30pIHtcbiAgICBjb25zdCByeCA9IG5ldyBSZWdFeHAoYCR7dGhpcy51c2VyLmNhcmRMYXN0RGlnaXRzfSRgKTtcbiAgICByZXR1cm4gcGFyc2VkRGF0YS5jYXJkTnVtYmVyLnNlYXJjaChyeCkgPiAtMTtcbiAgfVxuXG4gIC8vIFVzdWFsbHkgdXNlIGZvciBzaWduZWQgaW4gdXNlclxuICB2YWxpZGF0ZVBhcnROdW1iZXIocGFyc2VkRGF0YSA9IHt9KSB7XG4gICAgaWYgKHRoaXMuY29uZmlnLnBhcnROdW1iZXIpIHtcbiAgICAgIGNvbnN0IHsgcGFydE51bWJlciB9ID0gcGFyc2VkRGF0YTtcbiAgICAgIHJldHVybiB0aGlzLnVzZXIuY2FyZExhc3REaWdpdHMgPT09IHBhcnROdW1iZXI7XG4gICAgfVxuICAgIHJldHVybiB0cnVlO1xuICB9XG5cbiAgLy8gY2hlY2sgZiB1c2VyIHNlbGVjdGVkIHBhbWVudCB0eXBlID0gY2FyZFxuICBjaGVja0lmQ2FyZFBheW1lbnRTZWxlY3RlZCgpIHtcbiAgICBpZiAodGhpcy5jb25maWcucGF5bWVudFNlbGVjdG9yKSB7XG4gICAgICBjb25zdCB7IHNlbGVjdG9yIH0gPSB0aGlzLmNvbmZpZy5wYXltZW50U2VsZWN0b3I7XG4gICAgICByZXR1cm4gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihzZWxlY3Rvcik7XG4gICAgfVxuICAgIHJldHVybiB0cnVlO1xuICB9XG5cbiAgaGFuZGxlRGF0YShwYXJzZWREYXRhID0ge30pIHtcbiAgICBjb25zdCB7IGV4cGlyeU1vbnRoLCBleHBpcnlZZWFyIH0gPSBwYXJzZWREYXRhO1xuICAgIGNvbnN0IGV4cGlyeSA9IGAke2V4cGlyeU1vbnRofS8ke2V4cGlyeVllYXJ9YDtcbiAgICByZXR1cm4geyBleHBpcnksIC4uLnBhcnNlZERhdGEgfTtcbiAgfVxuXG4gIGRlZmluZU9ic2VydmVkUHJvcGVydHkobmFtZSkge1xuICAgIE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0aGlzLCBuYW1lLCB7XG4gICAgICBnZXQ6ICgpID0+IHRoaXNbYF8ke25hbWV9YF0sXG4gICAgICBzZXQ6ICh2YWx1ZSkgPT4ge1xuICAgICAgICBpZiAodGhpc1tgXyR7bmFtZX1gXSAhPT0gdmFsdWUpIHtcbiAgICAgICAgICB0aGlzW2BfJHtuYW1lfWBdID0gdmFsdWU7XG4gICAgICAgICAgdGhpcy5lbWl0KGB1cGRhdGU6JHtuYW1lfWAsIHZhbHVlKTtcbiAgICAgICAgfVxuICAgICAgfSxcbiAgICB9KTtcbiAgfVxufVxuIiwiaW1wb3J0IHsgYXN5bmNXcmFwcGVyIH0gZnJvbSAnLi4vdXRpbHMnO1xuaW1wb3J0IENhcmRDaGVja2VyIGZyb20gJy4vY2FyZENoZWNrZXIvaW5kZXgnO1xuXG5jbGFzcyBGcmFtZSB7XG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIHRoaXMubWVyY2hhbnQgPSBudWxsO1xuICAgIHRoaXMubW9kdWxlcyA9IHt9O1xuICAgIHRoaXMuc2V0TWVzc2FnZXNMaXN0ZW5lcigpO1xuICB9XG5cbiAgYXN5bmMgcHJvY2Vzcyh7IHVybCB9KSB7XG4gICAgaWYgKCF0aGlzLm1lcmNoYW50ICYmICF0aGlzLnVzZXIgJiYgIXRoaXMubW9kdWxlcy5jYXJkQ2hlY2tlcikge1xuICAgICAgYXdhaXQgdGhpcy51cGRhdGVNZXJjaGFudEluZm8odXJsKTtcbiAgICAgIGF3YWl0IHRoaXMudXBkYXRlVXNlckluZm8oKTtcbiAgICAgIGlmICh0aGlzLm1lcmNoYW50KSB7XG4gICAgICAgIHRoaXMuaW5pdENhcmRDaGVja2VyKCk7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgaW5pdENhcmRDaGVja2VyKCkge1xuICAgIGNvbnN0IHsgY2FyZENoZWNrZXIgfSA9IHRoaXMubWVyY2hhbnQuY29uZmlnLmNvbmZpZ3M7XG4gICAgaWYgKGNhcmRDaGVja2VyKSB7XG4gICAgICB0aGlzLm1vZHVsZXMuY2FyZENoZWNrZXIgPSBuZXcgQ2FyZENoZWNrZXIoY2FyZENoZWNrZXIsIHRoaXMudXNlcik7XG4gICAgICB0aGlzLm1vZHVsZXMuY2FyZENoZWNrZXJcbiAgICAgICAgLm9uKCd1cGRhdGU6aXNWYWxpZCcsIGFzeW5jIChjYXJkKSA9PiB7XG4gICAgICAgICAgYXdhaXQgYnJvd3Nlci5ydW50aW1lXG4gICAgICAgICAgICAuc2VuZE1lc3NhZ2Uoe1xuICAgICAgICAgICAgICBtb2R1bGU6ICd2YWxpZGF0aW9uU3RvcmUnLFxuICAgICAgICAgICAgICBhY3Rpb246ICd1cGRhdGUnLFxuICAgICAgICAgICAgICBkYXRhOiB7IG1pZDogdGhpcy5tZXJjaGFudC5pZCwgcmVzdWx0OiB7IGNhcmQgfSB9LFxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgIH1cbiAgfVxuXG4gIGFzeW5jIHVwZGF0ZU1lcmNoYW50SW5mbyh1cmwpIHtcbiAgICB0aGlzLm1lcmNoYW50ID0gYXdhaXQgYnJvd3Nlci5ydW50aW1lLnNlbmRNZXNzYWdlKHtcbiAgICAgIG1vZHVsZTogJ21lcmNoYW50cycsIGFjdGlvbjogJ2dldE1lcmNoYW50SW5mbycsIGRhdGE6IHsgdXJsIH0sXG4gICAgfSk7XG4gIH1cblxuICBhc3luYyB1cGRhdGVVc2VySW5mbygpIHtcbiAgICB0aGlzLnVzZXIgPSBhd2FpdCBicm93c2VyLnJ1bnRpbWUuc2VuZE1lc3NhZ2UoeyBhY3Rpb246ICdnZXRVc2VySW5mbycgfSk7XG4gIH1cblxuICBwaW5nKCkge1xuICAgIHJldHVybiB0cnVlO1xuICB9XG5cbiAgc2V0TWVzc2FnZXNMaXN0ZW5lcigpIHtcbiAgICBjaHJvbWUucnVudGltZS5vbk1lc3NhZ2UuYWRkTGlzdGVuZXIoKG1lc3NhZ2UsIHNlbmRlciwgc2VuZFJlc3BvbnNlKSA9PiB7XG4gICAgICBjb25zdCB7IGFjdGlvbiwgZGF0YSB9ID0gbWVzc2FnZTtcbiAgICAgIGlmICh0eXBlb2YgdGhpc1thY3Rpb25dID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgIGFzeW5jV3JhcHBlcih0aGlzW2FjdGlvbl0uYmluZCh0aGlzKSwgeyAuLi5kYXRhLCBzZW5kZXIgfSkudGhlbihzZW5kUmVzcG9uc2UpO1xuICAgICAgfVxuICAgICAgcmV0dXJuIHRydWU7XG4gICAgfSk7XG4gIH1cbn1cblxud2luZG93LmZyYW1lID0gbmV3IEZyYW1lKCk7XG4iXSwic291cmNlUm9vdCI6IiJ9