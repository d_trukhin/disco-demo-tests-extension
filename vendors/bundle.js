(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["vendors"],{

/***/ "./node_modules/@babel/runtime/helpers/arrayLikeToArray.js":
/*!*****************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/arrayLikeToArray.js ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _arrayLikeToArray(arr, len) {
  if (len == null || len > arr.length) len = arr.length;

  for (var i = 0, arr2 = new Array(len); i < len; i++) {
    arr2[i] = arr[i];
  }

  return arr2;
}

module.exports = _arrayLikeToArray;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/arrayWithHoles.js":
/*!***************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/arrayWithHoles.js ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _arrayWithHoles(arr) {
  if (Array.isArray(arr)) return arr;
}

module.exports = _arrayWithHoles;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/arrayWithoutHoles.js":
/*!******************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/arrayWithoutHoles.js ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var arrayLikeToArray = __webpack_require__(/*! ./arrayLikeToArray */ "./node_modules/@babel/runtime/helpers/arrayLikeToArray.js");

function _arrayWithoutHoles(arr) {
  if (Array.isArray(arr)) return arrayLikeToArray(arr);
}

module.exports = _arrayWithoutHoles;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/assertThisInitialized.js":
/*!**********************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/assertThisInitialized.js ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

module.exports = _assertThisInitialized;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/asyncToGenerator.js":
/*!*****************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/asyncToGenerator.js ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) {
  try {
    var info = gen[key](arg);
    var value = info.value;
  } catch (error) {
    reject(error);
    return;
  }

  if (info.done) {
    resolve(value);
  } else {
    Promise.resolve(value).then(_next, _throw);
  }
}

function _asyncToGenerator(fn) {
  return function () {
    var self = this,
        args = arguments;
    return new Promise(function (resolve, reject) {
      var gen = fn.apply(self, args);

      function _next(value) {
        asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value);
      }

      function _throw(err) {
        asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err);
      }

      _next(undefined);
    });
  };
}

module.exports = _asyncToGenerator;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/classCallCheck.js":
/*!***************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/classCallCheck.js ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

module.exports = _classCallCheck;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/createClass.js":
/*!************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/createClass.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

module.exports = _createClass;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/defineProperty.js":
/*!***************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/defineProperty.js ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

module.exports = _defineProperty;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/getPrototypeOf.js":
/*!***************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/getPrototypeOf.js ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _getPrototypeOf(o) {
  module.exports = _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

module.exports = _getPrototypeOf;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/inherits.js":
/*!*********************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/inherits.js ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var setPrototypeOf = __webpack_require__(/*! ./setPrototypeOf */ "./node_modules/@babel/runtime/helpers/setPrototypeOf.js");

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) setPrototypeOf(subClass, superClass);
}

module.exports = _inherits;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/iterableToArray.js":
/*!****************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/iterableToArray.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _iterableToArray(iter) {
  if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter);
}

module.exports = _iterableToArray;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/iterableToArrayLimit.js":
/*!*********************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/iterableToArrayLimit.js ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _iterableToArrayLimit(arr, i) {
  if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return;
  var _arr = [];
  var _n = true;
  var _d = false;
  var _e = undefined;

  try {
    for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {
      _arr.push(_s.value);

      if (i && _arr.length === i) break;
    }
  } catch (err) {
    _d = true;
    _e = err;
  } finally {
    try {
      if (!_n && _i["return"] != null) _i["return"]();
    } finally {
      if (_d) throw _e;
    }
  }

  return _arr;
}

module.exports = _iterableToArrayLimit;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/nonIterableRest.js":
/*!****************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/nonIterableRest.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _nonIterableRest() {
  throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
}

module.exports = _nonIterableRest;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/nonIterableSpread.js":
/*!******************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/nonIterableSpread.js ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _nonIterableSpread() {
  throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
}

module.exports = _nonIterableSpread;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/possibleConstructorReturn.js":
/*!**************************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/possibleConstructorReturn.js ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var _typeof = __webpack_require__(/*! ../helpers/typeof */ "./node_modules/@babel/runtime/helpers/typeof.js");

var assertThisInitialized = __webpack_require__(/*! ./assertThisInitialized */ "./node_modules/@babel/runtime/helpers/assertThisInitialized.js");

function _possibleConstructorReturn(self, call) {
  if (call && (_typeof(call) === "object" || typeof call === "function")) {
    return call;
  }

  return assertThisInitialized(self);
}

module.exports = _possibleConstructorReturn;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/setPrototypeOf.js":
/*!***************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/setPrototypeOf.js ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _setPrototypeOf(o, p) {
  module.exports = _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

module.exports = _setPrototypeOf;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/slicedToArray.js":
/*!**************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/slicedToArray.js ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var arrayWithHoles = __webpack_require__(/*! ./arrayWithHoles */ "./node_modules/@babel/runtime/helpers/arrayWithHoles.js");

var iterableToArrayLimit = __webpack_require__(/*! ./iterableToArrayLimit */ "./node_modules/@babel/runtime/helpers/iterableToArrayLimit.js");

var unsupportedIterableToArray = __webpack_require__(/*! ./unsupportedIterableToArray */ "./node_modules/@babel/runtime/helpers/unsupportedIterableToArray.js");

var nonIterableRest = __webpack_require__(/*! ./nonIterableRest */ "./node_modules/@babel/runtime/helpers/nonIterableRest.js");

function _slicedToArray(arr, i) {
  return arrayWithHoles(arr) || iterableToArrayLimit(arr, i) || unsupportedIterableToArray(arr, i) || nonIterableRest();
}

module.exports = _slicedToArray;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/toConsumableArray.js":
/*!******************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/toConsumableArray.js ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var arrayWithoutHoles = __webpack_require__(/*! ./arrayWithoutHoles */ "./node_modules/@babel/runtime/helpers/arrayWithoutHoles.js");

var iterableToArray = __webpack_require__(/*! ./iterableToArray */ "./node_modules/@babel/runtime/helpers/iterableToArray.js");

var unsupportedIterableToArray = __webpack_require__(/*! ./unsupportedIterableToArray */ "./node_modules/@babel/runtime/helpers/unsupportedIterableToArray.js");

var nonIterableSpread = __webpack_require__(/*! ./nonIterableSpread */ "./node_modules/@babel/runtime/helpers/nonIterableSpread.js");

function _toConsumableArray(arr) {
  return arrayWithoutHoles(arr) || iterableToArray(arr) || unsupportedIterableToArray(arr) || nonIterableSpread();
}

module.exports = _toConsumableArray;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/typeof.js":
/*!*******************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/typeof.js ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _typeof(obj) {
  "@babel/helpers - typeof";

  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    module.exports = _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    module.exports = _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

module.exports = _typeof;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/unsupportedIterableToArray.js":
/*!***************************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/unsupportedIterableToArray.js ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var arrayLikeToArray = __webpack_require__(/*! ./arrayLikeToArray */ "./node_modules/@babel/runtime/helpers/arrayLikeToArray.js");

function _unsupportedIterableToArray(o, minLen) {
  if (!o) return;
  if (typeof o === "string") return arrayLikeToArray(o, minLen);
  var n = Object.prototype.toString.call(o).slice(8, -1);
  if (n === "Object" && o.constructor) n = o.constructor.name;
  if (n === "Map" || n === "Set") return Array.from(o);
  if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return arrayLikeToArray(o, minLen);
}

module.exports = _unsupportedIterableToArray;

/***/ }),

/***/ "./node_modules/@babel/runtime/regenerator/index.js":
/*!**********************************************************!*\
  !*** ./node_modules/@babel/runtime/regenerator/index.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! regenerator-runtime */ "./node_modules/regenerator-runtime/runtime.js");


/***/ }),

/***/ "./node_modules/@sentry/browser/esm/backend.js":
/*!*****************************************************!*\
  !*** ./node_modules/@sentry/browser/esm/backend.js ***!
  \*****************************************************/
/*! exports provided: BrowserBackend */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BrowserBackend", function() { return BrowserBackend; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _sentry_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @sentry/core */ "./node_modules/@sentry/core/esm/index.js");
/* harmony import */ var _sentry_types__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @sentry/types */ "./node_modules/@sentry/types/esm/index.js");
/* harmony import */ var _sentry_utils__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @sentry/utils */ "./node_modules/@sentry/utils/esm/index.js");
/* harmony import */ var _eventbuilder__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./eventbuilder */ "./node_modules/@sentry/browser/esm/eventbuilder.js");
/* harmony import */ var _transports__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./transports */ "./node_modules/@sentry/browser/esm/transports/index.js");






/**
 * The Sentry Browser SDK Backend.
 * @hidden
 */
var BrowserBackend = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](BrowserBackend, _super);
    function BrowserBackend() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * @inheritDoc
     */
    BrowserBackend.prototype._setupTransport = function () {
        if (!this._options.dsn) {
            // We return the noop transport here in case there is no Dsn.
            return _super.prototype._setupTransport.call(this);
        }
        var transportOptions = tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, this._options.transportOptions, { dsn: this._options.dsn });
        if (this._options.transport) {
            return new this._options.transport(transportOptions);
        }
        if (Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_3__["supportsFetch"])()) {
            return new _transports__WEBPACK_IMPORTED_MODULE_5__["FetchTransport"](transportOptions);
        }
        return new _transports__WEBPACK_IMPORTED_MODULE_5__["XHRTransport"](transportOptions);
    };
    /**
     * @inheritDoc
     */
    BrowserBackend.prototype.eventFromException = function (exception, hint) {
        var syntheticException = (hint && hint.syntheticException) || undefined;
        var event = Object(_eventbuilder__WEBPACK_IMPORTED_MODULE_4__["eventFromUnknownInput"])(exception, syntheticException, {
            attachStacktrace: this._options.attachStacktrace,
        });
        Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_3__["addExceptionMechanism"])(event, {
            handled: true,
            type: 'generic',
        });
        event.level = _sentry_types__WEBPACK_IMPORTED_MODULE_2__["Severity"].Error;
        if (hint && hint.event_id) {
            event.event_id = hint.event_id;
        }
        return _sentry_utils__WEBPACK_IMPORTED_MODULE_3__["SyncPromise"].resolve(event);
    };
    /**
     * @inheritDoc
     */
    BrowserBackend.prototype.eventFromMessage = function (message, level, hint) {
        if (level === void 0) { level = _sentry_types__WEBPACK_IMPORTED_MODULE_2__["Severity"].Info; }
        var syntheticException = (hint && hint.syntheticException) || undefined;
        var event = Object(_eventbuilder__WEBPACK_IMPORTED_MODULE_4__["eventFromString"])(message, syntheticException, {
            attachStacktrace: this._options.attachStacktrace,
        });
        event.level = level;
        if (hint && hint.event_id) {
            event.event_id = hint.event_id;
        }
        return _sentry_utils__WEBPACK_IMPORTED_MODULE_3__["SyncPromise"].resolve(event);
    };
    return BrowserBackend;
}(_sentry_core__WEBPACK_IMPORTED_MODULE_1__["BaseBackend"]));

//# sourceMappingURL=backend.js.map

/***/ }),

/***/ "./node_modules/@sentry/browser/esm/client.js":
/*!****************************************************!*\
  !*** ./node_modules/@sentry/browser/esm/client.js ***!
  \****************************************************/
/*! exports provided: BrowserClient */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BrowserClient", function() { return BrowserClient; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _sentry_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @sentry/core */ "./node_modules/@sentry/core/esm/index.js");
/* harmony import */ var _sentry_utils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @sentry/utils */ "./node_modules/@sentry/utils/esm/index.js");
/* harmony import */ var _backend__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./backend */ "./node_modules/@sentry/browser/esm/backend.js");
/* harmony import */ var _integrations__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./integrations */ "./node_modules/@sentry/browser/esm/integrations/index.js");
/* harmony import */ var _version__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./version */ "./node_modules/@sentry/browser/esm/version.js");






/**
 * The Sentry Browser SDK Client.
 *
 * @see BrowserOptions for documentation on configuration options.
 * @see SentryClient for usage documentation.
 */
var BrowserClient = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](BrowserClient, _super);
    /**
     * Creates a new Browser SDK instance.
     *
     * @param options Configuration options for this SDK.
     */
    function BrowserClient(options) {
        if (options === void 0) { options = {}; }
        return _super.call(this, _backend__WEBPACK_IMPORTED_MODULE_3__["BrowserBackend"], options) || this;
    }
    /**
     * @inheritDoc
     */
    BrowserClient.prototype._prepareEvent = function (event, scope, hint) {
        event.platform = event.platform || 'javascript';
        event.sdk = tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, event.sdk, { name: _version__WEBPACK_IMPORTED_MODULE_5__["SDK_NAME"], packages: tslib__WEBPACK_IMPORTED_MODULE_0__["__spread"](((event.sdk && event.sdk.packages) || []), [
                {
                    name: 'npm:@sentry/browser',
                    version: _version__WEBPACK_IMPORTED_MODULE_5__["SDK_VERSION"],
                },
            ]), version: _version__WEBPACK_IMPORTED_MODULE_5__["SDK_VERSION"] });
        return _super.prototype._prepareEvent.call(this, event, scope, hint);
    };
    /**
     * @inheritDoc
     */
    BrowserClient.prototype._sendEvent = function (event) {
        var integration = this.getIntegration(_integrations__WEBPACK_IMPORTED_MODULE_4__["Breadcrumbs"]);
        if (integration) {
            integration.addSentryBreadcrumb(event);
        }
        _super.prototype._sendEvent.call(this, event);
    };
    /**
     * Show a report dialog to the user to send feedback to a specific event.
     *
     * @param options Set individual options for the dialog
     */
    BrowserClient.prototype.showReportDialog = function (options) {
        if (options === void 0) { options = {}; }
        // doesn't work without a document (React Native)
        var document = Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_2__["getGlobalObject"])().document;
        if (!document) {
            return;
        }
        if (!this._isEnabled()) {
            _sentry_utils__WEBPACK_IMPORTED_MODULE_2__["logger"].error('Trying to call showReportDialog with Sentry Client is disabled');
            return;
        }
        var dsn = options.dsn || this.getDsn();
        if (!options.eventId) {
            _sentry_utils__WEBPACK_IMPORTED_MODULE_2__["logger"].error('Missing `eventId` option in showReportDialog call');
            return;
        }
        if (!dsn) {
            _sentry_utils__WEBPACK_IMPORTED_MODULE_2__["logger"].error('Missing `Dsn` option in showReportDialog call');
            return;
        }
        var script = document.createElement('script');
        script.async = true;
        script.src = new _sentry_core__WEBPACK_IMPORTED_MODULE_1__["API"](dsn).getReportDialogEndpoint(options);
        if (options.onLoad) {
            script.onload = options.onLoad;
        }
        (document.head || document.body).appendChild(script);
    };
    return BrowserClient;
}(_sentry_core__WEBPACK_IMPORTED_MODULE_1__["BaseClient"]));

//# sourceMappingURL=client.js.map

/***/ }),

/***/ "./node_modules/@sentry/browser/esm/eventbuilder.js":
/*!**********************************************************!*\
  !*** ./node_modules/@sentry/browser/esm/eventbuilder.js ***!
  \**********************************************************/
/*! exports provided: eventFromUnknownInput, eventFromString */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "eventFromUnknownInput", function() { return eventFromUnknownInput; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "eventFromString", function() { return eventFromString; });
/* harmony import */ var _sentry_utils__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @sentry/utils */ "./node_modules/@sentry/utils/esm/index.js");
/* harmony import */ var _parsers__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./parsers */ "./node_modules/@sentry/browser/esm/parsers.js");
/* harmony import */ var _tracekit__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./tracekit */ "./node_modules/@sentry/browser/esm/tracekit.js");



/** JSDoc */
function eventFromUnknownInput(exception, syntheticException, options) {
    if (options === void 0) { options = {}; }
    var event;
    if (Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_0__["isErrorEvent"])(exception) && exception.error) {
        // If it is an ErrorEvent with `error` property, extract it to get actual Error
        var errorEvent = exception;
        exception = errorEvent.error; // tslint:disable-line:no-parameter-reassignment
        event = Object(_parsers__WEBPACK_IMPORTED_MODULE_1__["eventFromStacktrace"])(Object(_tracekit__WEBPACK_IMPORTED_MODULE_2__["computeStackTrace"])(exception));
        return event;
    }
    if (Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_0__["isDOMError"])(exception) || Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_0__["isDOMException"])(exception)) {
        // If it is a DOMError or DOMException (which are legacy APIs, but still supported in some browsers)
        // then we just extract the name and message, as they don't provide anything else
        // https://developer.mozilla.org/en-US/docs/Web/API/DOMError
        // https://developer.mozilla.org/en-US/docs/Web/API/DOMException
        var domException = exception;
        var name_1 = domException.name || (Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_0__["isDOMError"])(domException) ? 'DOMError' : 'DOMException');
        var message = domException.message ? name_1 + ": " + domException.message : name_1;
        event = eventFromString(message, syntheticException, options);
        Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_0__["addExceptionTypeValue"])(event, message);
        return event;
    }
    if (Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_0__["isError"])(exception)) {
        // we have a real Error object, do nothing
        event = Object(_parsers__WEBPACK_IMPORTED_MODULE_1__["eventFromStacktrace"])(Object(_tracekit__WEBPACK_IMPORTED_MODULE_2__["computeStackTrace"])(exception));
        return event;
    }
    if (Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_0__["isPlainObject"])(exception) || Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_0__["isEvent"])(exception)) {
        // If it is plain Object or Event, serialize it manually and extract options
        // This will allow us to group events based on top-level keys
        // which is much better than creating new group when any key/value change
        var objectException = exception;
        event = Object(_parsers__WEBPACK_IMPORTED_MODULE_1__["eventFromPlainObject"])(objectException, syntheticException, options.rejection);
        Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_0__["addExceptionMechanism"])(event, {
            synthetic: true,
        });
        return event;
    }
    // If none of previous checks were valid, then it means that it's not:
    // - an instance of DOMError
    // - an instance of DOMException
    // - an instance of Event
    // - an instance of Error
    // - a valid ErrorEvent (one with an error property)
    // - a plain Object
    //
    // So bail out and capture it as a simple message:
    event = eventFromString(exception, syntheticException, options);
    Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_0__["addExceptionTypeValue"])(event, "" + exception, undefined);
    Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_0__["addExceptionMechanism"])(event, {
        synthetic: true,
    });
    return event;
}
// this._options.attachStacktrace
/** JSDoc */
function eventFromString(input, syntheticException, options) {
    if (options === void 0) { options = {}; }
    var event = {
        message: input,
    };
    if (options.attachStacktrace && syntheticException) {
        var stacktrace = Object(_tracekit__WEBPACK_IMPORTED_MODULE_2__["computeStackTrace"])(syntheticException);
        var frames_1 = Object(_parsers__WEBPACK_IMPORTED_MODULE_1__["prepareFramesForEvent"])(stacktrace.stack);
        event.stacktrace = {
            frames: frames_1,
        };
    }
    return event;
}
//# sourceMappingURL=eventbuilder.js.map

/***/ }),

/***/ "./node_modules/@sentry/browser/esm/exports.js":
/*!*****************************************************!*\
  !*** ./node_modules/@sentry/browser/esm/exports.js ***!
  \*****************************************************/
/*! exports provided: Severity, Status, addGlobalEventProcessor, addBreadcrumb, captureException, captureEvent, captureMessage, configureScope, getHubFromCarrier, getCurrentHub, Hub, makeMain, Scope, startTransaction, setContext, setExtra, setExtras, setTag, setTags, setUser, withScope, BrowserClient, defaultIntegrations, forceLoad, init, lastEventId, onLoad, showReportDialog, flush, close, wrap, SDK_NAME, SDK_VERSION */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _sentry_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @sentry/types */ "./node_modules/@sentry/types/esm/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Severity", function() { return _sentry_types__WEBPACK_IMPORTED_MODULE_0__["Severity"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Status", function() { return _sentry_types__WEBPACK_IMPORTED_MODULE_0__["Status"]; });

/* harmony import */ var _sentry_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @sentry/core */ "./node_modules/@sentry/core/esm/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "addGlobalEventProcessor", function() { return _sentry_core__WEBPACK_IMPORTED_MODULE_1__["addGlobalEventProcessor"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "addBreadcrumb", function() { return _sentry_core__WEBPACK_IMPORTED_MODULE_1__["addBreadcrumb"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "captureException", function() { return _sentry_core__WEBPACK_IMPORTED_MODULE_1__["captureException"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "captureEvent", function() { return _sentry_core__WEBPACK_IMPORTED_MODULE_1__["captureEvent"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "captureMessage", function() { return _sentry_core__WEBPACK_IMPORTED_MODULE_1__["captureMessage"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "configureScope", function() { return _sentry_core__WEBPACK_IMPORTED_MODULE_1__["configureScope"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "getHubFromCarrier", function() { return _sentry_core__WEBPACK_IMPORTED_MODULE_1__["getHubFromCarrier"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "getCurrentHub", function() { return _sentry_core__WEBPACK_IMPORTED_MODULE_1__["getCurrentHub"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Hub", function() { return _sentry_core__WEBPACK_IMPORTED_MODULE_1__["Hub"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "makeMain", function() { return _sentry_core__WEBPACK_IMPORTED_MODULE_1__["makeMain"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Scope", function() { return _sentry_core__WEBPACK_IMPORTED_MODULE_1__["Scope"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "startTransaction", function() { return _sentry_core__WEBPACK_IMPORTED_MODULE_1__["startTransaction"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "setContext", function() { return _sentry_core__WEBPACK_IMPORTED_MODULE_1__["setContext"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "setExtra", function() { return _sentry_core__WEBPACK_IMPORTED_MODULE_1__["setExtra"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "setExtras", function() { return _sentry_core__WEBPACK_IMPORTED_MODULE_1__["setExtras"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "setTag", function() { return _sentry_core__WEBPACK_IMPORTED_MODULE_1__["setTag"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "setTags", function() { return _sentry_core__WEBPACK_IMPORTED_MODULE_1__["setTags"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "setUser", function() { return _sentry_core__WEBPACK_IMPORTED_MODULE_1__["setUser"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "withScope", function() { return _sentry_core__WEBPACK_IMPORTED_MODULE_1__["withScope"]; });

/* harmony import */ var _client__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./client */ "./node_modules/@sentry/browser/esm/client.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "BrowserClient", function() { return _client__WEBPACK_IMPORTED_MODULE_2__["BrowserClient"]; });

/* harmony import */ var _sdk__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./sdk */ "./node_modules/@sentry/browser/esm/sdk.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "defaultIntegrations", function() { return _sdk__WEBPACK_IMPORTED_MODULE_3__["defaultIntegrations"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "forceLoad", function() { return _sdk__WEBPACK_IMPORTED_MODULE_3__["forceLoad"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "init", function() { return _sdk__WEBPACK_IMPORTED_MODULE_3__["init"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "lastEventId", function() { return _sdk__WEBPACK_IMPORTED_MODULE_3__["lastEventId"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "onLoad", function() { return _sdk__WEBPACK_IMPORTED_MODULE_3__["onLoad"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "showReportDialog", function() { return _sdk__WEBPACK_IMPORTED_MODULE_3__["showReportDialog"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "flush", function() { return _sdk__WEBPACK_IMPORTED_MODULE_3__["flush"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "close", function() { return _sdk__WEBPACK_IMPORTED_MODULE_3__["close"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "wrap", function() { return _sdk__WEBPACK_IMPORTED_MODULE_3__["wrap"]; });

/* harmony import */ var _version__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./version */ "./node_modules/@sentry/browser/esm/version.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "SDK_NAME", function() { return _version__WEBPACK_IMPORTED_MODULE_4__["SDK_NAME"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "SDK_VERSION", function() { return _version__WEBPACK_IMPORTED_MODULE_4__["SDK_VERSION"]; });






//# sourceMappingURL=exports.js.map

/***/ }),

/***/ "./node_modules/@sentry/browser/esm/helpers.js":
/*!*****************************************************!*\
  !*** ./node_modules/@sentry/browser/esm/helpers.js ***!
  \*****************************************************/
/*! exports provided: shouldIgnoreOnError, ignoreNextOnError, wrap */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "shouldIgnoreOnError", function() { return shouldIgnoreOnError; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ignoreNextOnError", function() { return ignoreNextOnError; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "wrap", function() { return wrap; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _sentry_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @sentry/core */ "./node_modules/@sentry/core/esm/index.js");
/* harmony import */ var _sentry_utils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @sentry/utils */ "./node_modules/@sentry/utils/esm/index.js");



var ignoreOnError = 0;
/**
 * @hidden
 */
function shouldIgnoreOnError() {
    return ignoreOnError > 0;
}
/**
 * @hidden
 */
function ignoreNextOnError() {
    // onerror should trigger before setTimeout
    ignoreOnError += 1;
    setTimeout(function () {
        ignoreOnError -= 1;
    });
}
/**
 * Instruments the given function and sends an event to Sentry every time the
 * function throws an exception.
 *
 * @param fn A function to wrap.
 * @returns The wrapped function.
 * @hidden
 */
function wrap(fn, options, before) {
    if (options === void 0) { options = {}; }
    // tslint:disable-next-line:strict-type-predicates
    if (typeof fn !== 'function') {
        return fn;
    }
    try {
        // We don't wanna wrap it twice
        if (fn.__sentry__) {
            return fn;
        }
        // If this has already been wrapped in the past, return that wrapped function
        if (fn.__sentry_wrapped__) {
            return fn.__sentry_wrapped__;
        }
    }
    catch (e) {
        // Just accessing custom props in some Selenium environments
        // can cause a "Permission denied" exception (see raven-js#495).
        // Bail on wrapping and return the function as-is (defers to window.onerror).
        return fn;
    }
    var sentryWrapped = function () {
        var args = Array.prototype.slice.call(arguments);
        // tslint:disable:no-unsafe-any
        try {
            // tslint:disable-next-line:strict-type-predicates
            if (before && typeof before === 'function') {
                before.apply(this, arguments);
            }
            var wrappedArguments = args.map(function (arg) { return wrap(arg, options); });
            if (fn.handleEvent) {
                // Attempt to invoke user-land function
                // NOTE: If you are a Sentry user, and you are seeing this stack frame, it
                //       means the sentry.javascript SDK caught an error invoking your application code. This
                //       is expected behavior and NOT indicative of a bug with sentry.javascript.
                return fn.handleEvent.apply(this, wrappedArguments);
            }
            // Attempt to invoke user-land function
            // NOTE: If you are a Sentry user, and you are seeing this stack frame, it
            //       means the sentry.javascript SDK caught an error invoking your application code. This
            //       is expected behavior and NOT indicative of a bug with sentry.javascript.
            return fn.apply(this, wrappedArguments);
            // tslint:enable:no-unsafe-any
        }
        catch (ex) {
            ignoreNextOnError();
            Object(_sentry_core__WEBPACK_IMPORTED_MODULE_1__["withScope"])(function (scope) {
                scope.addEventProcessor(function (event) {
                    var processedEvent = tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, event);
                    if (options.mechanism) {
                        Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_2__["addExceptionTypeValue"])(processedEvent, undefined, undefined);
                        Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_2__["addExceptionMechanism"])(processedEvent, options.mechanism);
                    }
                    processedEvent.extra = tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, processedEvent.extra, { arguments: args });
                    return processedEvent;
                });
                Object(_sentry_core__WEBPACK_IMPORTED_MODULE_1__["captureException"])(ex);
            });
            throw ex;
        }
    };
    // Accessing some objects may throw
    // ref: https://github.com/getsentry/sentry-javascript/issues/1168
    try {
        for (var property in fn) {
            if (Object.prototype.hasOwnProperty.call(fn, property)) {
                sentryWrapped[property] = fn[property];
            }
        }
    }
    catch (_oO) { } // tslint:disable-line:no-empty
    fn.prototype = fn.prototype || {};
    sentryWrapped.prototype = fn.prototype;
    Object.defineProperty(fn, '__sentry_wrapped__', {
        enumerable: false,
        value: sentryWrapped,
    });
    // Signal that this function has been wrapped/filled already
    // for both debugging and to prevent it to being wrapped/filled twice
    Object.defineProperties(sentryWrapped, {
        __sentry__: {
            enumerable: false,
            value: true,
        },
        __sentry_original__: {
            enumerable: false,
            value: fn,
        },
    });
    // Restore original function name (not all browsers allow that)
    try {
        var descriptor = Object.getOwnPropertyDescriptor(sentryWrapped, 'name');
        if (descriptor.configurable) {
            Object.defineProperty(sentryWrapped, 'name', {
                get: function () {
                    return fn.name;
                },
            });
        }
    }
    catch (_oO) {
        /*no-empty*/
    }
    return sentryWrapped;
}
//# sourceMappingURL=helpers.js.map

/***/ }),

/***/ "./node_modules/@sentry/browser/esm/index.js":
/*!***************************************************!*\
  !*** ./node_modules/@sentry/browser/esm/index.js ***!
  \***************************************************/
/*! exports provided: Severity, Status, addGlobalEventProcessor, addBreadcrumb, captureException, captureEvent, captureMessage, configureScope, getHubFromCarrier, getCurrentHub, Hub, makeMain, Scope, startTransaction, setContext, setExtra, setExtras, setTag, setTags, setUser, withScope, BrowserClient, defaultIntegrations, forceLoad, init, lastEventId, onLoad, showReportDialog, flush, close, wrap, SDK_NAME, SDK_VERSION, Integrations, Transports */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Integrations", function() { return INTEGRATIONS; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _exports__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./exports */ "./node_modules/@sentry/browser/esm/exports.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Severity", function() { return _exports__WEBPACK_IMPORTED_MODULE_1__["Severity"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Status", function() { return _exports__WEBPACK_IMPORTED_MODULE_1__["Status"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "addGlobalEventProcessor", function() { return _exports__WEBPACK_IMPORTED_MODULE_1__["addGlobalEventProcessor"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "addBreadcrumb", function() { return _exports__WEBPACK_IMPORTED_MODULE_1__["addBreadcrumb"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "captureException", function() { return _exports__WEBPACK_IMPORTED_MODULE_1__["captureException"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "captureEvent", function() { return _exports__WEBPACK_IMPORTED_MODULE_1__["captureEvent"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "captureMessage", function() { return _exports__WEBPACK_IMPORTED_MODULE_1__["captureMessage"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "configureScope", function() { return _exports__WEBPACK_IMPORTED_MODULE_1__["configureScope"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "getHubFromCarrier", function() { return _exports__WEBPACK_IMPORTED_MODULE_1__["getHubFromCarrier"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "getCurrentHub", function() { return _exports__WEBPACK_IMPORTED_MODULE_1__["getCurrentHub"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Hub", function() { return _exports__WEBPACK_IMPORTED_MODULE_1__["Hub"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "makeMain", function() { return _exports__WEBPACK_IMPORTED_MODULE_1__["makeMain"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Scope", function() { return _exports__WEBPACK_IMPORTED_MODULE_1__["Scope"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "startTransaction", function() { return _exports__WEBPACK_IMPORTED_MODULE_1__["startTransaction"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "setContext", function() { return _exports__WEBPACK_IMPORTED_MODULE_1__["setContext"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "setExtra", function() { return _exports__WEBPACK_IMPORTED_MODULE_1__["setExtra"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "setExtras", function() { return _exports__WEBPACK_IMPORTED_MODULE_1__["setExtras"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "setTag", function() { return _exports__WEBPACK_IMPORTED_MODULE_1__["setTag"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "setTags", function() { return _exports__WEBPACK_IMPORTED_MODULE_1__["setTags"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "setUser", function() { return _exports__WEBPACK_IMPORTED_MODULE_1__["setUser"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "withScope", function() { return _exports__WEBPACK_IMPORTED_MODULE_1__["withScope"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "BrowserClient", function() { return _exports__WEBPACK_IMPORTED_MODULE_1__["BrowserClient"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "defaultIntegrations", function() { return _exports__WEBPACK_IMPORTED_MODULE_1__["defaultIntegrations"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "forceLoad", function() { return _exports__WEBPACK_IMPORTED_MODULE_1__["forceLoad"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "init", function() { return _exports__WEBPACK_IMPORTED_MODULE_1__["init"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "lastEventId", function() { return _exports__WEBPACK_IMPORTED_MODULE_1__["lastEventId"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "onLoad", function() { return _exports__WEBPACK_IMPORTED_MODULE_1__["onLoad"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "showReportDialog", function() { return _exports__WEBPACK_IMPORTED_MODULE_1__["showReportDialog"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "flush", function() { return _exports__WEBPACK_IMPORTED_MODULE_1__["flush"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "close", function() { return _exports__WEBPACK_IMPORTED_MODULE_1__["close"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "wrap", function() { return _exports__WEBPACK_IMPORTED_MODULE_1__["wrap"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "SDK_NAME", function() { return _exports__WEBPACK_IMPORTED_MODULE_1__["SDK_NAME"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "SDK_VERSION", function() { return _exports__WEBPACK_IMPORTED_MODULE_1__["SDK_VERSION"]; });

/* harmony import */ var _sentry_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @sentry/core */ "./node_modules/@sentry/core/esm/index.js");
/* harmony import */ var _sentry_utils__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @sentry/utils */ "./node_modules/@sentry/utils/esm/index.js");
/* harmony import */ var _integrations__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./integrations */ "./node_modules/@sentry/browser/esm/integrations/index.js");
/* harmony import */ var _transports__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./transports */ "./node_modules/@sentry/browser/esm/transports/index.js");
/* harmony reexport (module object) */ __webpack_require__.d(__webpack_exports__, "Transports", function() { return _transports__WEBPACK_IMPORTED_MODULE_5__; });






var windowIntegrations = {};
// This block is needed to add compatibility with the integrations packages when used with a CDN
// tslint:disable: no-unsafe-any
var _window = Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_3__["getGlobalObject"])();
if (_window.Sentry && _window.Sentry.Integrations) {
    windowIntegrations = _window.Sentry.Integrations;
}
// tslint:enable: no-unsafe-any
var INTEGRATIONS = tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, windowIntegrations, _sentry_core__WEBPACK_IMPORTED_MODULE_2__["Integrations"], _integrations__WEBPACK_IMPORTED_MODULE_4__);

//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./node_modules/@sentry/browser/esm/integrations/breadcrumbs.js":
/*!**********************************************************************!*\
  !*** ./node_modules/@sentry/browser/esm/integrations/breadcrumbs.js ***!
  \**********************************************************************/
/*! exports provided: Breadcrumbs */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Breadcrumbs", function() { return Breadcrumbs; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _sentry_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @sentry/core */ "./node_modules/@sentry/core/esm/index.js");
/* harmony import */ var _sentry_types__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @sentry/types */ "./node_modules/@sentry/types/esm/index.js");
/* harmony import */ var _sentry_utils__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @sentry/utils */ "./node_modules/@sentry/utils/esm/index.js");




/**
 * Default Breadcrumbs instrumentations
 * TODO: Deprecated - with v6, this will be renamed to `Instrument`
 */
var Breadcrumbs = /** @class */ (function () {
    /**
     * @inheritDoc
     */
    function Breadcrumbs(options) {
        /**
         * @inheritDoc
         */
        this.name = Breadcrumbs.id;
        this._options = tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({ console: true, dom: true, fetch: true, history: true, sentry: true, xhr: true }, options);
    }
    /**
     * Create a breadcrumb of `sentry` from the events themselves
     */
    Breadcrumbs.prototype.addSentryBreadcrumb = function (event) {
        if (!this._options.sentry) {
            return;
        }
        Object(_sentry_core__WEBPACK_IMPORTED_MODULE_1__["getCurrentHub"])().addBreadcrumb({
            category: "sentry." + (event.type === 'transaction' ? 'transaction' : 'event'),
            event_id: event.event_id,
            level: event.level,
            message: Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_3__["getEventDescription"])(event),
        }, {
            event: event,
        });
    };
    /**
     * Creates breadcrumbs from console API calls
     */
    Breadcrumbs.prototype._consoleBreadcrumb = function (handlerData) {
        var breadcrumb = {
            category: 'console',
            data: {
                arguments: handlerData.args,
                logger: 'console',
            },
            level: _sentry_types__WEBPACK_IMPORTED_MODULE_2__["Severity"].fromString(handlerData.level),
            message: Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_3__["safeJoin"])(handlerData.args, ' '),
        };
        if (handlerData.level === 'assert') {
            if (handlerData.args[0] === false) {
                breadcrumb.message = "Assertion failed: " + (Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_3__["safeJoin"])(handlerData.args.slice(1), ' ') || 'console.assert');
                breadcrumb.data.arguments = handlerData.args.slice(1);
            }
            else {
                // Don't capture a breadcrumb for passed assertions
                return;
            }
        }
        Object(_sentry_core__WEBPACK_IMPORTED_MODULE_1__["getCurrentHub"])().addBreadcrumb(breadcrumb, {
            input: handlerData.args,
            level: handlerData.level,
        });
    };
    /**
     * Creates breadcrumbs from DOM API calls
     */
    Breadcrumbs.prototype._domBreadcrumb = function (handlerData) {
        var target;
        // Accessing event.target can throw (see getsentry/raven-js#838, #768)
        try {
            target = handlerData.event.target
                ? Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_3__["htmlTreeAsString"])(handlerData.event.target)
                : Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_3__["htmlTreeAsString"])(handlerData.event);
        }
        catch (e) {
            target = '<unknown>';
        }
        if (target.length === 0) {
            return;
        }
        Object(_sentry_core__WEBPACK_IMPORTED_MODULE_1__["getCurrentHub"])().addBreadcrumb({
            category: "ui." + handlerData.name,
            message: target,
        }, {
            event: handlerData.event,
            name: handlerData.name,
        });
    };
    /**
     * Creates breadcrumbs from XHR API calls
     */
    Breadcrumbs.prototype._xhrBreadcrumb = function (handlerData) {
        if (handlerData.endTimestamp) {
            // We only capture complete, non-sentry requests
            if (handlerData.xhr.__sentry_own_request__) {
                return;
            }
            Object(_sentry_core__WEBPACK_IMPORTED_MODULE_1__["getCurrentHub"])().addBreadcrumb({
                category: 'xhr',
                data: handlerData.xhr.__sentry_xhr__,
                type: 'http',
            }, {
                xhr: handlerData.xhr,
            });
            return;
        }
    };
    /**
     * Creates breadcrumbs from fetch API calls
     */
    Breadcrumbs.prototype._fetchBreadcrumb = function (handlerData) {
        // We only capture complete fetch requests
        if (!handlerData.endTimestamp) {
            return;
        }
        if (handlerData.fetchData.url.match(/sentry_key/) && handlerData.fetchData.method === 'POST') {
            // We will not create breadcrumbs for fetch requests that contain `sentry_key` (internal sentry requests)
            return;
        }
        if (handlerData.error) {
            Object(_sentry_core__WEBPACK_IMPORTED_MODULE_1__["getCurrentHub"])().addBreadcrumb({
                category: 'fetch',
                data: handlerData.fetchData,
                level: _sentry_types__WEBPACK_IMPORTED_MODULE_2__["Severity"].Error,
                type: 'http',
            }, {
                data: handlerData.error,
                input: handlerData.args,
            });
        }
        else {
            Object(_sentry_core__WEBPACK_IMPORTED_MODULE_1__["getCurrentHub"])().addBreadcrumb({
                category: 'fetch',
                data: tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, handlerData.fetchData, { status_code: handlerData.response.status }),
                type: 'http',
            }, {
                input: handlerData.args,
                response: handlerData.response,
            });
        }
    };
    /**
     * Creates breadcrumbs from history API calls
     */
    Breadcrumbs.prototype._historyBreadcrumb = function (handlerData) {
        var global = Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_3__["getGlobalObject"])();
        var from = handlerData.from;
        var to = handlerData.to;
        var parsedLoc = Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_3__["parseUrl"])(global.location.href);
        var parsedFrom = Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_3__["parseUrl"])(from);
        var parsedTo = Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_3__["parseUrl"])(to);
        // Initial pushState doesn't provide `from` information
        if (!parsedFrom.path) {
            parsedFrom = parsedLoc;
        }
        // Use only the path component of the URL if the URL matches the current
        // document (almost all the time when using pushState)
        if (parsedLoc.protocol === parsedTo.protocol && parsedLoc.host === parsedTo.host) {
            // tslint:disable-next-line:no-parameter-reassignment
            to = parsedTo.relative;
        }
        if (parsedLoc.protocol === parsedFrom.protocol && parsedLoc.host === parsedFrom.host) {
            // tslint:disable-next-line:no-parameter-reassignment
            from = parsedFrom.relative;
        }
        Object(_sentry_core__WEBPACK_IMPORTED_MODULE_1__["getCurrentHub"])().addBreadcrumb({
            category: 'navigation',
            data: {
                from: from,
                to: to,
            },
        });
    };
    /**
     * Instrument browser built-ins w/ breadcrumb capturing
     *  - Console API
     *  - DOM API (click/typing)
     *  - XMLHttpRequest API
     *  - Fetch API
     *  - History API
     */
    Breadcrumbs.prototype.setupOnce = function () {
        var _this = this;
        if (this._options.console) {
            Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_3__["addInstrumentationHandler"])({
                callback: function () {
                    var args = [];
                    for (var _i = 0; _i < arguments.length; _i++) {
                        args[_i] = arguments[_i];
                    }
                    _this._consoleBreadcrumb.apply(_this, tslib__WEBPACK_IMPORTED_MODULE_0__["__spread"](args));
                },
                type: 'console',
            });
        }
        if (this._options.dom) {
            Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_3__["addInstrumentationHandler"])({
                callback: function () {
                    var args = [];
                    for (var _i = 0; _i < arguments.length; _i++) {
                        args[_i] = arguments[_i];
                    }
                    _this._domBreadcrumb.apply(_this, tslib__WEBPACK_IMPORTED_MODULE_0__["__spread"](args));
                },
                type: 'dom',
            });
        }
        if (this._options.xhr) {
            Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_3__["addInstrumentationHandler"])({
                callback: function () {
                    var args = [];
                    for (var _i = 0; _i < arguments.length; _i++) {
                        args[_i] = arguments[_i];
                    }
                    _this._xhrBreadcrumb.apply(_this, tslib__WEBPACK_IMPORTED_MODULE_0__["__spread"](args));
                },
                type: 'xhr',
            });
        }
        if (this._options.fetch) {
            Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_3__["addInstrumentationHandler"])({
                callback: function () {
                    var args = [];
                    for (var _i = 0; _i < arguments.length; _i++) {
                        args[_i] = arguments[_i];
                    }
                    _this._fetchBreadcrumb.apply(_this, tslib__WEBPACK_IMPORTED_MODULE_0__["__spread"](args));
                },
                type: 'fetch',
            });
        }
        if (this._options.history) {
            Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_3__["addInstrumentationHandler"])({
                callback: function () {
                    var args = [];
                    for (var _i = 0; _i < arguments.length; _i++) {
                        args[_i] = arguments[_i];
                    }
                    _this._historyBreadcrumb.apply(_this, tslib__WEBPACK_IMPORTED_MODULE_0__["__spread"](args));
                },
                type: 'history',
            });
        }
    };
    /**
     * @inheritDoc
     */
    Breadcrumbs.id = 'Breadcrumbs';
    return Breadcrumbs;
}());

//# sourceMappingURL=breadcrumbs.js.map

/***/ }),

/***/ "./node_modules/@sentry/browser/esm/integrations/globalhandlers.js":
/*!*************************************************************************!*\
  !*** ./node_modules/@sentry/browser/esm/integrations/globalhandlers.js ***!
  \*************************************************************************/
/*! exports provided: GlobalHandlers */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GlobalHandlers", function() { return GlobalHandlers; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _sentry_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @sentry/core */ "./node_modules/@sentry/core/esm/index.js");
/* harmony import */ var _sentry_types__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @sentry/types */ "./node_modules/@sentry/types/esm/index.js");
/* harmony import */ var _sentry_utils__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @sentry/utils */ "./node_modules/@sentry/utils/esm/index.js");
/* harmony import */ var _eventbuilder__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../eventbuilder */ "./node_modules/@sentry/browser/esm/eventbuilder.js");
/* harmony import */ var _helpers__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../helpers */ "./node_modules/@sentry/browser/esm/helpers.js");






/** Global handlers */
var GlobalHandlers = /** @class */ (function () {
    /** JSDoc */
    function GlobalHandlers(options) {
        /**
         * @inheritDoc
         */
        this.name = GlobalHandlers.id;
        /** JSDoc */
        this._onErrorHandlerInstalled = false;
        /** JSDoc */
        this._onUnhandledRejectionHandlerInstalled = false;
        this._options = tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({ onerror: true, onunhandledrejection: true }, options);
    }
    /**
     * @inheritDoc
     */
    GlobalHandlers.prototype.setupOnce = function () {
        Error.stackTraceLimit = 50;
        if (this._options.onerror) {
            _sentry_utils__WEBPACK_IMPORTED_MODULE_3__["logger"].log('Global Handler attached: onerror');
            this._installGlobalOnErrorHandler();
        }
        if (this._options.onunhandledrejection) {
            _sentry_utils__WEBPACK_IMPORTED_MODULE_3__["logger"].log('Global Handler attached: onunhandledrejection');
            this._installGlobalOnUnhandledRejectionHandler();
        }
    };
    /** JSDoc */
    GlobalHandlers.prototype._installGlobalOnErrorHandler = function () {
        var _this = this;
        if (this._onErrorHandlerInstalled) {
            return;
        }
        Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_3__["addInstrumentationHandler"])({
            callback: function (data) {
                var error = data.error;
                var currentHub = Object(_sentry_core__WEBPACK_IMPORTED_MODULE_1__["getCurrentHub"])();
                var hasIntegration = currentHub.getIntegration(GlobalHandlers);
                var isFailedOwnDelivery = error && error.__sentry_own_request__ === true;
                if (!hasIntegration || Object(_helpers__WEBPACK_IMPORTED_MODULE_5__["shouldIgnoreOnError"])() || isFailedOwnDelivery) {
                    return;
                }
                var client = currentHub.getClient();
                var event = Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_3__["isPrimitive"])(error)
                    ? _this._eventFromIncompleteOnError(data.msg, data.url, data.line, data.column)
                    : _this._enhanceEventWithInitialFrame(Object(_eventbuilder__WEBPACK_IMPORTED_MODULE_4__["eventFromUnknownInput"])(error, undefined, {
                        attachStacktrace: client && client.getOptions().attachStacktrace,
                        rejection: false,
                    }), data.url, data.line, data.column);
                Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_3__["addExceptionMechanism"])(event, {
                    handled: false,
                    type: 'onerror',
                });
                currentHub.captureEvent(event, {
                    originalException: error,
                });
            },
            type: 'error',
        });
        this._onErrorHandlerInstalled = true;
    };
    /** JSDoc */
    GlobalHandlers.prototype._installGlobalOnUnhandledRejectionHandler = function () {
        var _this = this;
        if (this._onUnhandledRejectionHandlerInstalled) {
            return;
        }
        Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_3__["addInstrumentationHandler"])({
            callback: function (e) {
                var error = e;
                // dig the object of the rejection out of known event types
                try {
                    // PromiseRejectionEvents store the object of the rejection under 'reason'
                    // see https://developer.mozilla.org/en-US/docs/Web/API/PromiseRejectionEvent
                    if ('reason' in e) {
                        error = e.reason;
                    }
                    // something, somewhere, (likely a browser extension) effectively casts PromiseRejectionEvents
                    // to CustomEvents, moving the `promise` and `reason` attributes of the PRE into
                    // the CustomEvent's `detail` attribute, since they're not part of CustomEvent's spec
                    // see https://developer.mozilla.org/en-US/docs/Web/API/CustomEvent and
                    // https://github.com/getsentry/sentry-javascript/issues/2380
                    else if ('detail' in e && 'reason' in e.detail) {
                        error = e.detail.reason;
                    }
                }
                catch (_oO) {
                    // no-empty
                }
                var currentHub = Object(_sentry_core__WEBPACK_IMPORTED_MODULE_1__["getCurrentHub"])();
                var hasIntegration = currentHub.getIntegration(GlobalHandlers);
                var isFailedOwnDelivery = error && error.__sentry_own_request__ === true;
                if (!hasIntegration || Object(_helpers__WEBPACK_IMPORTED_MODULE_5__["shouldIgnoreOnError"])() || isFailedOwnDelivery) {
                    return true;
                }
                var client = currentHub.getClient();
                var event = Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_3__["isPrimitive"])(error)
                    ? _this._eventFromIncompleteRejection(error)
                    : Object(_eventbuilder__WEBPACK_IMPORTED_MODULE_4__["eventFromUnknownInput"])(error, undefined, {
                        attachStacktrace: client && client.getOptions().attachStacktrace,
                        rejection: true,
                    });
                event.level = _sentry_types__WEBPACK_IMPORTED_MODULE_2__["Severity"].Error;
                Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_3__["addExceptionMechanism"])(event, {
                    handled: false,
                    type: 'onunhandledrejection',
                });
                currentHub.captureEvent(event, {
                    originalException: error,
                });
                return;
            },
            type: 'unhandledrejection',
        });
        this._onUnhandledRejectionHandlerInstalled = true;
    };
    /**
     * This function creates a stack from an old, error-less onerror handler.
     */
    GlobalHandlers.prototype._eventFromIncompleteOnError = function (msg, url, line, column) {
        var ERROR_TYPES_RE = /^(?:[Uu]ncaught (?:exception: )?)?(?:((?:Eval|Internal|Range|Reference|Syntax|Type|URI|)Error): )?(.*)$/i;
        // If 'message' is ErrorEvent, get real message from inside
        var message = Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_3__["isErrorEvent"])(msg) ? msg.message : msg;
        var name;
        if (Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_3__["isString"])(message)) {
            var groups = message.match(ERROR_TYPES_RE);
            if (groups) {
                name = groups[1];
                message = groups[2];
            }
        }
        var event = {
            exception: {
                values: [
                    {
                        type: name || 'Error',
                        value: message,
                    },
                ],
            },
        };
        return this._enhanceEventWithInitialFrame(event, url, line, column);
    };
    /**
     * This function creates an Event from an TraceKitStackTrace that has part of it missing.
     */
    GlobalHandlers.prototype._eventFromIncompleteRejection = function (error) {
        return {
            exception: {
                values: [
                    {
                        type: 'UnhandledRejection',
                        value: "Non-Error promise rejection captured with value: " + error,
                    },
                ],
            },
        };
    };
    /** JSDoc */
    GlobalHandlers.prototype._enhanceEventWithInitialFrame = function (event, url, line, column) {
        event.exception = event.exception || {};
        event.exception.values = event.exception.values || [];
        event.exception.values[0] = event.exception.values[0] || {};
        event.exception.values[0].stacktrace = event.exception.values[0].stacktrace || {};
        event.exception.values[0].stacktrace.frames = event.exception.values[0].stacktrace.frames || [];
        var colno = isNaN(parseInt(column, 10)) ? undefined : column;
        var lineno = isNaN(parseInt(line, 10)) ? undefined : line;
        var filename = Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_3__["isString"])(url) && url.length > 0 ? url : Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_3__["getLocationHref"])();
        if (event.exception.values[0].stacktrace.frames.length === 0) {
            event.exception.values[0].stacktrace.frames.push({
                colno: colno,
                filename: filename,
                function: '?',
                in_app: true,
                lineno: lineno,
            });
        }
        return event;
    };
    /**
     * @inheritDoc
     */
    GlobalHandlers.id = 'GlobalHandlers';
    return GlobalHandlers;
}());

//# sourceMappingURL=globalhandlers.js.map

/***/ }),

/***/ "./node_modules/@sentry/browser/esm/integrations/index.js":
/*!****************************************************************!*\
  !*** ./node_modules/@sentry/browser/esm/integrations/index.js ***!
  \****************************************************************/
/*! exports provided: GlobalHandlers, TryCatch, Breadcrumbs, LinkedErrors, UserAgent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _globalhandlers__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./globalhandlers */ "./node_modules/@sentry/browser/esm/integrations/globalhandlers.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "GlobalHandlers", function() { return _globalhandlers__WEBPACK_IMPORTED_MODULE_0__["GlobalHandlers"]; });

/* harmony import */ var _trycatch__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./trycatch */ "./node_modules/@sentry/browser/esm/integrations/trycatch.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "TryCatch", function() { return _trycatch__WEBPACK_IMPORTED_MODULE_1__["TryCatch"]; });

/* harmony import */ var _breadcrumbs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./breadcrumbs */ "./node_modules/@sentry/browser/esm/integrations/breadcrumbs.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Breadcrumbs", function() { return _breadcrumbs__WEBPACK_IMPORTED_MODULE_2__["Breadcrumbs"]; });

/* harmony import */ var _linkederrors__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./linkederrors */ "./node_modules/@sentry/browser/esm/integrations/linkederrors.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "LinkedErrors", function() { return _linkederrors__WEBPACK_IMPORTED_MODULE_3__["LinkedErrors"]; });

/* harmony import */ var _useragent__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./useragent */ "./node_modules/@sentry/browser/esm/integrations/useragent.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "UserAgent", function() { return _useragent__WEBPACK_IMPORTED_MODULE_4__["UserAgent"]; });






//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./node_modules/@sentry/browser/esm/integrations/linkederrors.js":
/*!***********************************************************************!*\
  !*** ./node_modules/@sentry/browser/esm/integrations/linkederrors.js ***!
  \***********************************************************************/
/*! exports provided: LinkedErrors */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LinkedErrors", function() { return LinkedErrors; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _sentry_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @sentry/core */ "./node_modules/@sentry/core/esm/index.js");
/* harmony import */ var _sentry_utils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @sentry/utils */ "./node_modules/@sentry/utils/esm/index.js");
/* harmony import */ var _parsers__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../parsers */ "./node_modules/@sentry/browser/esm/parsers.js");
/* harmony import */ var _tracekit__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../tracekit */ "./node_modules/@sentry/browser/esm/tracekit.js");





var DEFAULT_KEY = 'cause';
var DEFAULT_LIMIT = 5;
/** Adds SDK info to an event. */
var LinkedErrors = /** @class */ (function () {
    /**
     * @inheritDoc
     */
    function LinkedErrors(options) {
        if (options === void 0) { options = {}; }
        /**
         * @inheritDoc
         */
        this.name = LinkedErrors.id;
        this._key = options.key || DEFAULT_KEY;
        this._limit = options.limit || DEFAULT_LIMIT;
    }
    /**
     * @inheritDoc
     */
    LinkedErrors.prototype.setupOnce = function () {
        Object(_sentry_core__WEBPACK_IMPORTED_MODULE_1__["addGlobalEventProcessor"])(function (event, hint) {
            var self = Object(_sentry_core__WEBPACK_IMPORTED_MODULE_1__["getCurrentHub"])().getIntegration(LinkedErrors);
            if (self) {
                return self._handler(event, hint);
            }
            return event;
        });
    };
    /**
     * @inheritDoc
     */
    LinkedErrors.prototype._handler = function (event, hint) {
        if (!event.exception || !event.exception.values || !hint || !Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_2__["isInstanceOf"])(hint.originalException, Error)) {
            return event;
        }
        var linkedErrors = this._walkErrorTree(hint.originalException, this._key);
        event.exception.values = tslib__WEBPACK_IMPORTED_MODULE_0__["__spread"](linkedErrors, event.exception.values);
        return event;
    };
    /**
     * @inheritDoc
     */
    LinkedErrors.prototype._walkErrorTree = function (error, key, stack) {
        if (stack === void 0) { stack = []; }
        if (!Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_2__["isInstanceOf"])(error[key], Error) || stack.length + 1 >= this._limit) {
            return stack;
        }
        var stacktrace = Object(_tracekit__WEBPACK_IMPORTED_MODULE_4__["computeStackTrace"])(error[key]);
        var exception = Object(_parsers__WEBPACK_IMPORTED_MODULE_3__["exceptionFromStacktrace"])(stacktrace);
        return this._walkErrorTree(error[key], key, tslib__WEBPACK_IMPORTED_MODULE_0__["__spread"]([exception], stack));
    };
    /**
     * @inheritDoc
     */
    LinkedErrors.id = 'LinkedErrors';
    return LinkedErrors;
}());

//# sourceMappingURL=linkederrors.js.map

/***/ }),

/***/ "./node_modules/@sentry/browser/esm/integrations/trycatch.js":
/*!*******************************************************************!*\
  !*** ./node_modules/@sentry/browser/esm/integrations/trycatch.js ***!
  \*******************************************************************/
/*! exports provided: TryCatch */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TryCatch", function() { return TryCatch; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _sentry_utils__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @sentry/utils */ "./node_modules/@sentry/utils/esm/index.js");
/* harmony import */ var _helpers__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../helpers */ "./node_modules/@sentry/browser/esm/helpers.js");



var DEFAULT_EVENT_TARGET = [
    'EventTarget',
    'Window',
    'Node',
    'ApplicationCache',
    'AudioTrackList',
    'ChannelMergerNode',
    'CryptoOperation',
    'EventSource',
    'FileReader',
    'HTMLUnknownElement',
    'IDBDatabase',
    'IDBRequest',
    'IDBTransaction',
    'KeyOperation',
    'MediaController',
    'MessagePort',
    'ModalWindow',
    'Notification',
    'SVGElementInstance',
    'Screen',
    'TextTrack',
    'TextTrackCue',
    'TextTrackList',
    'WebSocket',
    'WebSocketWorker',
    'Worker',
    'XMLHttpRequest',
    'XMLHttpRequestEventTarget',
    'XMLHttpRequestUpload',
];
/** Wrap timer functions and event targets to catch errors and provide better meta data */
var TryCatch = /** @class */ (function () {
    /**
     * @inheritDoc
     */
    function TryCatch(options) {
        /**
         * @inheritDoc
         */
        this.name = TryCatch.id;
        this._options = tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({ XMLHttpRequest: true, eventTarget: true, requestAnimationFrame: true, setInterval: true, setTimeout: true }, options);
    }
    /** JSDoc */
    TryCatch.prototype._wrapTimeFunction = function (original) {
        return function () {
            var args = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                args[_i] = arguments[_i];
            }
            var originalCallback = args[0];
            args[0] = Object(_helpers__WEBPACK_IMPORTED_MODULE_2__["wrap"])(originalCallback, {
                mechanism: {
                    data: { function: Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_1__["getFunctionName"])(original) },
                    handled: true,
                    type: 'instrument',
                },
            });
            return original.apply(this, args);
        };
    };
    /** JSDoc */
    TryCatch.prototype._wrapRAF = function (original) {
        return function (callback) {
            return original.call(this, Object(_helpers__WEBPACK_IMPORTED_MODULE_2__["wrap"])(callback, {
                mechanism: {
                    data: {
                        function: 'requestAnimationFrame',
                        handler: Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_1__["getFunctionName"])(original),
                    },
                    handled: true,
                    type: 'instrument',
                },
            }));
        };
    };
    /** JSDoc */
    TryCatch.prototype._wrapEventTarget = function (target) {
        var global = Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_1__["getGlobalObject"])();
        var proto = global[target] && global[target].prototype;
        if (!proto || !proto.hasOwnProperty || !proto.hasOwnProperty('addEventListener')) {
            return;
        }
        Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_1__["fill"])(proto, 'addEventListener', function (original) {
            return function (eventName, fn, options) {
                try {
                    // tslint:disable-next-line:no-unbound-method strict-type-predicates
                    if (typeof fn.handleEvent === 'function') {
                        fn.handleEvent = Object(_helpers__WEBPACK_IMPORTED_MODULE_2__["wrap"])(fn.handleEvent.bind(fn), {
                            mechanism: {
                                data: {
                                    function: 'handleEvent',
                                    handler: Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_1__["getFunctionName"])(fn),
                                    target: target,
                                },
                                handled: true,
                                type: 'instrument',
                            },
                        });
                    }
                }
                catch (err) {
                    // can sometimes get 'Permission denied to access property "handle Event'
                }
                return original.call(this, eventName, Object(_helpers__WEBPACK_IMPORTED_MODULE_2__["wrap"])(fn, {
                    mechanism: {
                        data: {
                            function: 'addEventListener',
                            handler: Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_1__["getFunctionName"])(fn),
                            target: target,
                        },
                        handled: true,
                        type: 'instrument',
                    },
                }), options);
            };
        });
        Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_1__["fill"])(proto, 'removeEventListener', function (original) {
            return function (eventName, fn, options) {
                var callback = fn;
                try {
                    callback = callback && (callback.__sentry_wrapped__ || callback);
                }
                catch (e) {
                    // ignore, accessing __sentry_wrapped__ will throw in some Selenium environments
                }
                return original.call(this, eventName, callback, options);
            };
        });
    };
    /** JSDoc */
    TryCatch.prototype._wrapXHR = function (originalSend) {
        return function () {
            var args = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                args[_i] = arguments[_i];
            }
            var xhr = this; // tslint:disable-line:no-this-assignment
            var xmlHttpRequestProps = ['onload', 'onerror', 'onprogress', 'onreadystatechange'];
            xmlHttpRequestProps.forEach(function (prop) {
                if (prop in xhr && typeof xhr[prop] === 'function') {
                    Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_1__["fill"])(xhr, prop, function (original) {
                        var wrapOptions = {
                            mechanism: {
                                data: {
                                    function: prop,
                                    handler: Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_1__["getFunctionName"])(original),
                                },
                                handled: true,
                                type: 'instrument',
                            },
                        };
                        // If Instrument integration has been called before TryCatch, get the name of original function
                        if (original.__sentry_original__) {
                            wrapOptions.mechanism.data.handler = Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_1__["getFunctionName"])(original.__sentry_original__);
                        }
                        // Otherwise wrap directly
                        return Object(_helpers__WEBPACK_IMPORTED_MODULE_2__["wrap"])(original, wrapOptions);
                    });
                }
            });
            return originalSend.apply(this, args);
        };
    };
    /**
     * Wrap timer functions and event targets to catch errors
     * and provide better metadata.
     */
    TryCatch.prototype.setupOnce = function () {
        var global = Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_1__["getGlobalObject"])();
        if (this._options.setTimeout) {
            Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_1__["fill"])(global, 'setTimeout', this._wrapTimeFunction.bind(this));
        }
        if (this._options.setInterval) {
            Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_1__["fill"])(global, 'setInterval', this._wrapTimeFunction.bind(this));
        }
        if (this._options.requestAnimationFrame) {
            Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_1__["fill"])(global, 'requestAnimationFrame', this._wrapRAF.bind(this));
        }
        if (this._options.XMLHttpRequest && 'XMLHttpRequest' in global) {
            Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_1__["fill"])(XMLHttpRequest.prototype, 'send', this._wrapXHR.bind(this));
        }
        if (this._options.eventTarget) {
            var eventTarget = Array.isArray(this._options.eventTarget) ? this._options.eventTarget : DEFAULT_EVENT_TARGET;
            eventTarget.forEach(this._wrapEventTarget.bind(this));
        }
    };
    /**
     * @inheritDoc
     */
    TryCatch.id = 'TryCatch';
    return TryCatch;
}());

//# sourceMappingURL=trycatch.js.map

/***/ }),

/***/ "./node_modules/@sentry/browser/esm/integrations/useragent.js":
/*!********************************************************************!*\
  !*** ./node_modules/@sentry/browser/esm/integrations/useragent.js ***!
  \********************************************************************/
/*! exports provided: UserAgent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserAgent", function() { return UserAgent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _sentry_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @sentry/core */ "./node_modules/@sentry/core/esm/index.js");
/* harmony import */ var _sentry_utils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @sentry/utils */ "./node_modules/@sentry/utils/esm/index.js");



var global = Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_2__["getGlobalObject"])();
/** UserAgent */
var UserAgent = /** @class */ (function () {
    function UserAgent() {
        /**
         * @inheritDoc
         */
        this.name = UserAgent.id;
    }
    /**
     * @inheritDoc
     */
    UserAgent.prototype.setupOnce = function () {
        Object(_sentry_core__WEBPACK_IMPORTED_MODULE_1__["addGlobalEventProcessor"])(function (event) {
            if (Object(_sentry_core__WEBPACK_IMPORTED_MODULE_1__["getCurrentHub"])().getIntegration(UserAgent)) {
                if (!global.navigator || !global.location) {
                    return event;
                }
                var request = event.request || {};
                request.url = request.url || global.location.href;
                request.headers = request.headers || {};
                request.headers['User-Agent'] = global.navigator.userAgent;
                return tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, event, { request: request });
            }
            return event;
        });
    };
    /**
     * @inheritDoc
     */
    UserAgent.id = 'UserAgent';
    return UserAgent;
}());

//# sourceMappingURL=useragent.js.map

/***/ }),

/***/ "./node_modules/@sentry/browser/esm/parsers.js":
/*!*****************************************************!*\
  !*** ./node_modules/@sentry/browser/esm/parsers.js ***!
  \*****************************************************/
/*! exports provided: exceptionFromStacktrace, eventFromPlainObject, eventFromStacktrace, prepareFramesForEvent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "exceptionFromStacktrace", function() { return exceptionFromStacktrace; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "eventFromPlainObject", function() { return eventFromPlainObject; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "eventFromStacktrace", function() { return eventFromStacktrace; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "prepareFramesForEvent", function() { return prepareFramesForEvent; });
/* harmony import */ var _sentry_utils__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @sentry/utils */ "./node_modules/@sentry/utils/esm/index.js");
/* harmony import */ var _tracekit__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./tracekit */ "./node_modules/@sentry/browser/esm/tracekit.js");


var STACKTRACE_LIMIT = 50;
/**
 * This function creates an exception from an TraceKitStackTrace
 * @param stacktrace TraceKitStackTrace that will be converted to an exception
 * @hidden
 */
function exceptionFromStacktrace(stacktrace) {
    var frames = prepareFramesForEvent(stacktrace.stack);
    var exception = {
        type: stacktrace.name,
        value: stacktrace.message,
    };
    if (frames && frames.length) {
        exception.stacktrace = { frames: frames };
    }
    // tslint:disable-next-line:strict-type-predicates
    if (exception.type === undefined && exception.value === '') {
        exception.value = 'Unrecoverable error caught';
    }
    return exception;
}
/**
 * @hidden
 */
function eventFromPlainObject(exception, syntheticException, rejection) {
    var event = {
        exception: {
            values: [
                {
                    type: Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_0__["isEvent"])(exception) ? exception.constructor.name : rejection ? 'UnhandledRejection' : 'Error',
                    value: "Non-Error " + (rejection ? 'promise rejection' : 'exception') + " captured with keys: " + Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_0__["extractExceptionKeysForMessage"])(exception),
                },
            ],
        },
        extra: {
            __serialized__: Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_0__["normalizeToSize"])(exception),
        },
    };
    if (syntheticException) {
        var stacktrace = Object(_tracekit__WEBPACK_IMPORTED_MODULE_1__["computeStackTrace"])(syntheticException);
        var frames_1 = prepareFramesForEvent(stacktrace.stack);
        event.stacktrace = {
            frames: frames_1,
        };
    }
    return event;
}
/**
 * @hidden
 */
function eventFromStacktrace(stacktrace) {
    var exception = exceptionFromStacktrace(stacktrace);
    return {
        exception: {
            values: [exception],
        },
    };
}
/**
 * @hidden
 */
function prepareFramesForEvent(stack) {
    if (!stack || !stack.length) {
        return [];
    }
    var localStack = stack;
    var firstFrameFunction = localStack[0].func || '';
    var lastFrameFunction = localStack[localStack.length - 1].func || '';
    // If stack starts with one of our API calls, remove it (starts, meaning it's the top of the stack - aka last call)
    if (firstFrameFunction.indexOf('captureMessage') !== -1 || firstFrameFunction.indexOf('captureException') !== -1) {
        localStack = localStack.slice(1);
    }
    // If stack ends with one of our internal API calls, remove it (ends, meaning it's the bottom of the stack - aka top-most call)
    if (lastFrameFunction.indexOf('sentryWrapped') !== -1) {
        localStack = localStack.slice(0, -1);
    }
    // The frame where the crash happened, should be the last entry in the array
    return localStack
        .slice(0, STACKTRACE_LIMIT)
        .map(function (frame) { return ({
        colno: frame.column === null ? undefined : frame.column,
        filename: frame.url || localStack[0].url,
        function: frame.func || '?',
        in_app: true,
        lineno: frame.line === null ? undefined : frame.line,
    }); })
        .reverse();
}
//# sourceMappingURL=parsers.js.map

/***/ }),

/***/ "./node_modules/@sentry/browser/esm/sdk.js":
/*!*************************************************!*\
  !*** ./node_modules/@sentry/browser/esm/sdk.js ***!
  \*************************************************/
/*! exports provided: defaultIntegrations, init, showReportDialog, lastEventId, forceLoad, onLoad, flush, close, wrap */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "defaultIntegrations", function() { return defaultIntegrations; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "init", function() { return init; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "showReportDialog", function() { return showReportDialog; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "lastEventId", function() { return lastEventId; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "forceLoad", function() { return forceLoad; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "onLoad", function() { return onLoad; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "flush", function() { return flush; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "close", function() { return close; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "wrap", function() { return wrap; });
/* harmony import */ var _sentry_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @sentry/core */ "./node_modules/@sentry/core/esm/index.js");
/* harmony import */ var _sentry_utils__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @sentry/utils */ "./node_modules/@sentry/utils/esm/index.js");
/* harmony import */ var _client__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./client */ "./node_modules/@sentry/browser/esm/client.js");
/* harmony import */ var _helpers__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./helpers */ "./node_modules/@sentry/browser/esm/helpers.js");
/* harmony import */ var _integrations__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./integrations */ "./node_modules/@sentry/browser/esm/integrations/index.js");





var defaultIntegrations = [
    new _sentry_core__WEBPACK_IMPORTED_MODULE_0__["Integrations"].InboundFilters(),
    new _sentry_core__WEBPACK_IMPORTED_MODULE_0__["Integrations"].FunctionToString(),
    new _integrations__WEBPACK_IMPORTED_MODULE_4__["TryCatch"](),
    new _integrations__WEBPACK_IMPORTED_MODULE_4__["Breadcrumbs"](),
    new _integrations__WEBPACK_IMPORTED_MODULE_4__["GlobalHandlers"](),
    new _integrations__WEBPACK_IMPORTED_MODULE_4__["LinkedErrors"](),
    new _integrations__WEBPACK_IMPORTED_MODULE_4__["UserAgent"](),
];
/**
 * The Sentry Browser SDK Client.
 *
 * To use this SDK, call the {@link init} function as early as possible when
 * loading the web page. To set context information or send manual events, use
 * the provided methods.
 *
 * @example
 *
 * ```
 *
 * import { init } from '@sentry/browser';
 *
 * init({
 *   dsn: '__DSN__',
 *   // ...
 * });
 * ```
 *
 * @example
 * ```
 *
 * import { configureScope } from '@sentry/browser';
 * configureScope((scope: Scope) => {
 *   scope.setExtra({ battery: 0.7 });
 *   scope.setTag({ user_mode: 'admin' });
 *   scope.setUser({ id: '4711' });
 * });
 * ```
 *
 * @example
 * ```
 *
 * import { addBreadcrumb } from '@sentry/browser';
 * addBreadcrumb({
 *   message: 'My Breadcrumb',
 *   // ...
 * });
 * ```
 *
 * @example
 *
 * ```
 *
 * import * as Sentry from '@sentry/browser';
 * Sentry.captureMessage('Hello, world!');
 * Sentry.captureException(new Error('Good bye'));
 * Sentry.captureEvent({
 *   message: 'Manual',
 *   stacktrace: [
 *     // ...
 *   ],
 * });
 * ```
 *
 * @see {@link BrowserOptions} for documentation on configuration options.
 */
function init(options) {
    if (options === void 0) { options = {}; }
    if (options.defaultIntegrations === undefined) {
        options.defaultIntegrations = defaultIntegrations;
    }
    if (options.release === undefined) {
        var window_1 = Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_1__["getGlobalObject"])();
        // This supports the variable that sentry-webpack-plugin injects
        if (window_1.SENTRY_RELEASE && window_1.SENTRY_RELEASE.id) {
            options.release = window_1.SENTRY_RELEASE.id;
        }
    }
    Object(_sentry_core__WEBPACK_IMPORTED_MODULE_0__["initAndBind"])(_client__WEBPACK_IMPORTED_MODULE_2__["BrowserClient"], options);
}
/**
 * Present the user with a report dialog.
 *
 * @param options Everything is optional, we try to fetch all info need from the global scope.
 */
function showReportDialog(options) {
    if (options === void 0) { options = {}; }
    if (!options.eventId) {
        options.eventId = Object(_sentry_core__WEBPACK_IMPORTED_MODULE_0__["getCurrentHub"])().lastEventId();
    }
    var client = Object(_sentry_core__WEBPACK_IMPORTED_MODULE_0__["getCurrentHub"])().getClient();
    if (client) {
        client.showReportDialog(options);
    }
}
/**
 * This is the getter for lastEventId.
 *
 * @returns The last event id of a captured event.
 */
function lastEventId() {
    return Object(_sentry_core__WEBPACK_IMPORTED_MODULE_0__["getCurrentHub"])().lastEventId();
}
/**
 * This function is here to be API compatible with the loader.
 * @hidden
 */
function forceLoad() {
    // Noop
}
/**
 * This function is here to be API compatible with the loader.
 * @hidden
 */
function onLoad(callback) {
    callback();
}
/**
 * A promise that resolves when all current events have been sent.
 * If you provide a timeout and the queue takes longer to drain the promise returns false.
 *
 * @param timeout Maximum time in ms the client should wait.
 */
function flush(timeout) {
    var client = Object(_sentry_core__WEBPACK_IMPORTED_MODULE_0__["getCurrentHub"])().getClient();
    if (client) {
        return client.flush(timeout);
    }
    return _sentry_utils__WEBPACK_IMPORTED_MODULE_1__["SyncPromise"].reject(false);
}
/**
 * A promise that resolves when all current events have been sent.
 * If you provide a timeout and the queue takes longer to drain the promise returns false.
 *
 * @param timeout Maximum time in ms the client should wait.
 */
function close(timeout) {
    var client = Object(_sentry_core__WEBPACK_IMPORTED_MODULE_0__["getCurrentHub"])().getClient();
    if (client) {
        return client.close(timeout);
    }
    return _sentry_utils__WEBPACK_IMPORTED_MODULE_1__["SyncPromise"].reject(false);
}
/**
 * Wrap code within a try/catch block so the SDK is able to capture errors.
 *
 * @param fn A function to wrap.
 *
 * @returns The result of wrapped function call.
 */
function wrap(fn) {
    return Object(_helpers__WEBPACK_IMPORTED_MODULE_3__["wrap"])(fn)(); // tslint:disable-line:no-unsafe-any
}
//# sourceMappingURL=sdk.js.map

/***/ }),

/***/ "./node_modules/@sentry/browser/esm/tracekit.js":
/*!******************************************************!*\
  !*** ./node_modules/@sentry/browser/esm/tracekit.js ***!
  \******************************************************/
/*! exports provided: computeStackTrace */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "computeStackTrace", function() { return computeStackTrace; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
// tslint:disable:object-literal-sort-keys

// global reference to slice
var UNKNOWN_FUNCTION = '?';
// Chromium based browsers: Chrome, Brave, new Opera, new Edge
var chrome = /^\s*at (?:(.*?) ?\()?((?:file|https?|blob|chrome-extension|address|native|eval|webpack|<anonymous>|[-a-z]+:|.*bundle|\/).*?)(?::(\d+))?(?::(\d+))?\)?\s*$/i;
// gecko regex: `(?:bundle|\d+\.js)`: `bundle` is for react native, `\d+\.js` also but specifically for ram bundles because it
// generates filenames without a prefix like `file://` the filenames in the stacktrace are just 42.js
// We need this specific case for now because we want no other regex to match.
var gecko = /^\s*(.*?)(?:\((.*?)\))?(?:^|@)?((?:file|https?|blob|chrome|webpack|resource|moz-extension).*?:\/.*?|\[native code\]|[^@]*(?:bundle|\d+\.js))(?::(\d+))?(?::(\d+))?\s*$/i;
var winjs = /^\s*at (?:((?:\[object object\])?.+) )?\(?((?:file|ms-appx|https?|webpack|blob):.*?):(\d+)(?::(\d+))?\)?\s*$/i;
var geckoEval = /(\S+) line (\d+)(?: > eval line \d+)* > eval/i;
var chromeEval = /\((\S*)(?::(\d+))(?::(\d+))\)/;
/** JSDoc */
function computeStackTrace(ex) {
    // tslint:disable:no-unsafe-any
    var stack = null;
    var popSize = ex && ex.framesToPop;
    try {
        // This must be tried first because Opera 10 *destroys*
        // its stacktrace property if you try to access the stack
        // property first!!
        stack = computeStackTraceFromStacktraceProp(ex);
        if (stack) {
            return popFrames(stack, popSize);
        }
    }
    catch (e) {
        // no-empty
    }
    try {
        stack = computeStackTraceFromStackProp(ex);
        if (stack) {
            return popFrames(stack, popSize);
        }
    }
    catch (e) {
        // no-empty
    }
    return {
        message: extractMessage(ex),
        name: ex && ex.name,
        stack: [],
        failed: true,
    };
}
/** JSDoc */
// tslint:disable-next-line:cyclomatic-complexity
function computeStackTraceFromStackProp(ex) {
    // tslint:disable:no-conditional-assignment
    if (!ex || !ex.stack) {
        return null;
    }
    var stack = [];
    var lines = ex.stack.split('\n');
    var isEval;
    var submatch;
    var parts;
    var element;
    for (var i = 0; i < lines.length; ++i) {
        if ((parts = chrome.exec(lines[i]))) {
            var isNative = parts[2] && parts[2].indexOf('native') === 0; // start of line
            isEval = parts[2] && parts[2].indexOf('eval') === 0; // start of line
            if (isEval && (submatch = chromeEval.exec(parts[2]))) {
                // throw out eval line/column and use top-most line/column number
                parts[2] = submatch[1]; // url
                parts[3] = submatch[2]; // line
                parts[4] = submatch[3]; // column
            }
            element = {
                // working with the regexp above is super painful. it is quite a hack, but just stripping the `address at `
                // prefix here seems like the quickest solution for now.
                url: parts[2] && parts[2].indexOf('address at ') === 0 ? parts[2].substr('address at '.length) : parts[2],
                func: parts[1] || UNKNOWN_FUNCTION,
                args: isNative ? [parts[2]] : [],
                line: parts[3] ? +parts[3] : null,
                column: parts[4] ? +parts[4] : null,
            };
        }
        else if ((parts = winjs.exec(lines[i]))) {
            element = {
                url: parts[2],
                func: parts[1] || UNKNOWN_FUNCTION,
                args: [],
                line: +parts[3],
                column: parts[4] ? +parts[4] : null,
            };
        }
        else if ((parts = gecko.exec(lines[i]))) {
            isEval = parts[3] && parts[3].indexOf(' > eval') > -1;
            if (isEval && (submatch = geckoEval.exec(parts[3]))) {
                // throw out eval line/column and use top-most line number
                parts[1] = parts[1] || "eval";
                parts[3] = submatch[1];
                parts[4] = submatch[2];
                parts[5] = ''; // no column when eval
            }
            else if (i === 0 && !parts[5] && ex.columnNumber !== void 0) {
                // FireFox uses this awesome columnNumber property for its top frame
                // Also note, Firefox's column number is 0-based and everything else expects 1-based,
                // so adding 1
                // NOTE: this hack doesn't work if top-most frame is eval
                stack[0].column = ex.columnNumber + 1;
            }
            element = {
                url: parts[3],
                func: parts[1] || UNKNOWN_FUNCTION,
                args: parts[2] ? parts[2].split(',') : [],
                line: parts[4] ? +parts[4] : null,
                column: parts[5] ? +parts[5] : null,
            };
        }
        else {
            continue;
        }
        if (!element.func && element.line) {
            element.func = UNKNOWN_FUNCTION;
        }
        stack.push(element);
    }
    if (!stack.length) {
        return null;
    }
    return {
        message: extractMessage(ex),
        name: ex.name,
        stack: stack,
    };
}
/** JSDoc */
function computeStackTraceFromStacktraceProp(ex) {
    if (!ex || !ex.stacktrace) {
        return null;
    }
    // Access and store the stacktrace property before doing ANYTHING
    // else to it because Opera is not very good at providing it
    // reliably in other circumstances.
    var stacktrace = ex.stacktrace;
    var opera10Regex = / line (\d+).*script (?:in )?(\S+)(?:: in function (\S+))?$/i;
    var opera11Regex = / line (\d+), column (\d+)\s*(?:in (?:<anonymous function: ([^>]+)>|([^\)]+))\((.*)\))? in (.*):\s*$/i;
    var lines = stacktrace.split('\n');
    var stack = [];
    var parts;
    for (var line = 0; line < lines.length; line += 2) {
        // tslint:disable:no-conditional-assignment
        var element = null;
        if ((parts = opera10Regex.exec(lines[line]))) {
            element = {
                url: parts[2],
                func: parts[3],
                args: [],
                line: +parts[1],
                column: null,
            };
        }
        else if ((parts = opera11Regex.exec(lines[line]))) {
            element = {
                url: parts[6],
                func: parts[3] || parts[4],
                args: parts[5] ? parts[5].split(',') : [],
                line: +parts[1],
                column: +parts[2],
            };
        }
        if (element) {
            if (!element.func && element.line) {
                element.func = UNKNOWN_FUNCTION;
            }
            stack.push(element);
        }
    }
    if (!stack.length) {
        return null;
    }
    return {
        message: extractMessage(ex),
        name: ex.name,
        stack: stack,
    };
}
/** Remove N number of frames from the stack */
function popFrames(stacktrace, popSize) {
    try {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, stacktrace, { stack: stacktrace.stack.slice(popSize) });
    }
    catch (e) {
        return stacktrace;
    }
}
/**
 * There are cases where stacktrace.message is an Event object
 * https://github.com/getsentry/sentry-javascript/issues/1949
 * In this specific case we try to extract stacktrace.message.error.message
 */
function extractMessage(ex) {
    var message = ex && ex.message;
    if (!message) {
        return 'No error message';
    }
    if (message.error && typeof message.error.message === 'string') {
        return message.error.message;
    }
    return message;
}
//# sourceMappingURL=tracekit.js.map

/***/ }),

/***/ "./node_modules/@sentry/browser/esm/transports/base.js":
/*!*************************************************************!*\
  !*** ./node_modules/@sentry/browser/esm/transports/base.js ***!
  \*************************************************************/
/*! exports provided: BaseTransport */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BaseTransport", function() { return BaseTransport; });
/* harmony import */ var _sentry_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @sentry/core */ "./node_modules/@sentry/core/esm/index.js");
/* harmony import */ var _sentry_utils__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @sentry/utils */ "./node_modules/@sentry/utils/esm/index.js");


/** Base Transport class implementation */
var BaseTransport = /** @class */ (function () {
    function BaseTransport(options) {
        this.options = options;
        /** A simple buffer holding all requests. */
        this._buffer = new _sentry_utils__WEBPACK_IMPORTED_MODULE_1__["PromiseBuffer"](30);
        this._api = new _sentry_core__WEBPACK_IMPORTED_MODULE_0__["API"](this.options.dsn);
        // tslint:disable-next-line:deprecation
        this.url = this._api.getStoreEndpointWithUrlEncodedAuth();
    }
    /**
     * @inheritDoc
     */
    BaseTransport.prototype.sendEvent = function (_) {
        throw new _sentry_utils__WEBPACK_IMPORTED_MODULE_1__["SentryError"]('Transport Class has to implement `sendEvent` method');
    };
    /**
     * @inheritDoc
     */
    BaseTransport.prototype.close = function (timeout) {
        return this._buffer.drain(timeout);
    };
    return BaseTransport;
}());

//# sourceMappingURL=base.js.map

/***/ }),

/***/ "./node_modules/@sentry/browser/esm/transports/fetch.js":
/*!**************************************************************!*\
  !*** ./node_modules/@sentry/browser/esm/transports/fetch.js ***!
  \**************************************************************/
/*! exports provided: FetchTransport */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FetchTransport", function() { return FetchTransport; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _sentry_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @sentry/core */ "./node_modules/@sentry/core/esm/index.js");
/* harmony import */ var _sentry_types__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @sentry/types */ "./node_modules/@sentry/types/esm/index.js");
/* harmony import */ var _sentry_utils__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @sentry/utils */ "./node_modules/@sentry/utils/esm/index.js");
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./base */ "./node_modules/@sentry/browser/esm/transports/base.js");





var global = Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_3__["getGlobalObject"])();
/** `fetch` based transport */
var FetchTransport = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](FetchTransport, _super);
    function FetchTransport() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        /** Locks transport after receiving 429 response */
        _this._disabledUntil = new Date(Date.now());
        return _this;
    }
    /**
     * @inheritDoc
     */
    FetchTransport.prototype.sendEvent = function (event) {
        var _this = this;
        if (new Date(Date.now()) < this._disabledUntil) {
            return Promise.reject({
                event: event,
                reason: "Transport locked till " + this._disabledUntil + " due to too many requests.",
                status: 429,
            });
        }
        var sentryReq = Object(_sentry_core__WEBPACK_IMPORTED_MODULE_1__["eventToSentryRequest"])(event, this._api);
        var options = {
            body: sentryReq.body,
            method: 'POST',
            // Despite all stars in the sky saying that Edge supports old draft syntax, aka 'never', 'always', 'origin' and 'default
            // https://caniuse.com/#feat=referrer-policy
            // It doesn't. And it throw exception instead of ignoring this parameter...
            // REF: https://github.com/getsentry/raven-js/issues/1233
            referrerPolicy: (Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_3__["supportsReferrerPolicy"])() ? 'origin' : ''),
        };
        if (this.options.fetchParameters !== undefined) {
            Object.assign(options, this.options.fetchParameters);
        }
        if (this.options.headers !== undefined) {
            options.headers = this.options.headers;
        }
        return this._buffer.add(new _sentry_utils__WEBPACK_IMPORTED_MODULE_3__["SyncPromise"](function (resolve, reject) {
            global
                .fetch(sentryReq.url, options)
                .then(function (response) {
                var status = _sentry_types__WEBPACK_IMPORTED_MODULE_2__["Status"].fromHttpCode(response.status);
                if (status === _sentry_types__WEBPACK_IMPORTED_MODULE_2__["Status"].Success) {
                    resolve({ status: status });
                    return;
                }
                if (status === _sentry_types__WEBPACK_IMPORTED_MODULE_2__["Status"].RateLimit) {
                    var now = Date.now();
                    _this._disabledUntil = new Date(now + Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_3__["parseRetryAfterHeader"])(now, response.headers.get('Retry-After')));
                    _sentry_utils__WEBPACK_IMPORTED_MODULE_3__["logger"].warn("Too many requests, backing off till: " + _this._disabledUntil);
                }
                reject(response);
            })
                .catch(reject);
        }));
    };
    return FetchTransport;
}(_base__WEBPACK_IMPORTED_MODULE_4__["BaseTransport"]));

//# sourceMappingURL=fetch.js.map

/***/ }),

/***/ "./node_modules/@sentry/browser/esm/transports/index.js":
/*!**************************************************************!*\
  !*** ./node_modules/@sentry/browser/esm/transports/index.js ***!
  \**************************************************************/
/*! exports provided: BaseTransport, FetchTransport, XHRTransport */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./base */ "./node_modules/@sentry/browser/esm/transports/base.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "BaseTransport", function() { return _base__WEBPACK_IMPORTED_MODULE_0__["BaseTransport"]; });

/* harmony import */ var _fetch__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./fetch */ "./node_modules/@sentry/browser/esm/transports/fetch.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "FetchTransport", function() { return _fetch__WEBPACK_IMPORTED_MODULE_1__["FetchTransport"]; });

/* harmony import */ var _xhr__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./xhr */ "./node_modules/@sentry/browser/esm/transports/xhr.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "XHRTransport", function() { return _xhr__WEBPACK_IMPORTED_MODULE_2__["XHRTransport"]; });




//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./node_modules/@sentry/browser/esm/transports/xhr.js":
/*!************************************************************!*\
  !*** ./node_modules/@sentry/browser/esm/transports/xhr.js ***!
  \************************************************************/
/*! exports provided: XHRTransport */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "XHRTransport", function() { return XHRTransport; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _sentry_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @sentry/core */ "./node_modules/@sentry/core/esm/index.js");
/* harmony import */ var _sentry_types__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @sentry/types */ "./node_modules/@sentry/types/esm/index.js");
/* harmony import */ var _sentry_utils__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @sentry/utils */ "./node_modules/@sentry/utils/esm/index.js");
/* harmony import */ var _base__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./base */ "./node_modules/@sentry/browser/esm/transports/base.js");





/** `XHR` based transport */
var XHRTransport = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](XHRTransport, _super);
    function XHRTransport() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        /** Locks transport after receiving 429 response */
        _this._disabledUntil = new Date(Date.now());
        return _this;
    }
    /**
     * @inheritDoc
     */
    XHRTransport.prototype.sendEvent = function (event) {
        var _this = this;
        if (new Date(Date.now()) < this._disabledUntil) {
            return Promise.reject({
                event: event,
                reason: "Transport locked till " + this._disabledUntil + " due to too many requests.",
                status: 429,
            });
        }
        var sentryReq = Object(_sentry_core__WEBPACK_IMPORTED_MODULE_1__["eventToSentryRequest"])(event, this._api);
        return this._buffer.add(new _sentry_utils__WEBPACK_IMPORTED_MODULE_3__["SyncPromise"](function (resolve, reject) {
            var request = new XMLHttpRequest();
            request.onreadystatechange = function () {
                if (request.readyState !== 4) {
                    return;
                }
                var status = _sentry_types__WEBPACK_IMPORTED_MODULE_2__["Status"].fromHttpCode(request.status);
                if (status === _sentry_types__WEBPACK_IMPORTED_MODULE_2__["Status"].Success) {
                    resolve({ status: status });
                    return;
                }
                if (status === _sentry_types__WEBPACK_IMPORTED_MODULE_2__["Status"].RateLimit) {
                    var now = Date.now();
                    _this._disabledUntil = new Date(now + Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_3__["parseRetryAfterHeader"])(now, request.getResponseHeader('Retry-After')));
                    _sentry_utils__WEBPACK_IMPORTED_MODULE_3__["logger"].warn("Too many requests, backing off till: " + _this._disabledUntil);
                }
                reject(request);
            };
            request.open('POST', sentryReq.url);
            for (var header in _this.options.headers) {
                if (_this.options.headers.hasOwnProperty(header)) {
                    request.setRequestHeader(header, _this.options.headers[header]);
                }
            }
            request.send(sentryReq.body);
        }));
    };
    return XHRTransport;
}(_base__WEBPACK_IMPORTED_MODULE_4__["BaseTransport"]));

//# sourceMappingURL=xhr.js.map

/***/ }),

/***/ "./node_modules/@sentry/browser/esm/version.js":
/*!*****************************************************!*\
  !*** ./node_modules/@sentry/browser/esm/version.js ***!
  \*****************************************************/
/*! exports provided: SDK_NAME, SDK_VERSION */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SDK_NAME", function() { return SDK_NAME; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SDK_VERSION", function() { return SDK_VERSION; });
var SDK_NAME = 'sentry.javascript.browser';
var SDK_VERSION = '5.18.0';
//# sourceMappingURL=version.js.map

/***/ }),

/***/ "./node_modules/@sentry/core/esm/api.js":
/*!**********************************************!*\
  !*** ./node_modules/@sentry/core/esm/api.js ***!
  \**********************************************/
/*! exports provided: API */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "API", function() { return API; });
/* harmony import */ var _sentry_utils__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @sentry/utils */ "./node_modules/@sentry/utils/esm/index.js");

var SENTRY_API_VERSION = '7';
/** Helper class to provide urls to different Sentry endpoints. */
var API = /** @class */ (function () {
    /** Create a new instance of API */
    function API(dsn) {
        this.dsn = dsn;
        this._dsnObject = new _sentry_utils__WEBPACK_IMPORTED_MODULE_0__["Dsn"](dsn);
    }
    /** Returns the Dsn object. */
    API.prototype.getDsn = function () {
        return this._dsnObject;
    };
    /** Returns the prefix to construct Sentry ingestion API endpoints. */
    API.prototype.getBaseApiEndpoint = function () {
        var dsn = this._dsnObject;
        var protocol = dsn.protocol ? dsn.protocol + ":" : '';
        var port = dsn.port ? ":" + dsn.port : '';
        return protocol + "//" + dsn.host + port + (dsn.path ? "/" + dsn.path : '') + "/api/";
    };
    /** Returns the store endpoint URL. */
    API.prototype.getStoreEndpoint = function () {
        return this._getIngestEndpoint('store');
    };
    /** Returns the envelope endpoint URL. */
    API.prototype._getEnvelopeEndpoint = function () {
        return this._getIngestEndpoint('envelope');
    };
    /** Returns the ingest API endpoint for target. */
    API.prototype._getIngestEndpoint = function (target) {
        var base = this.getBaseApiEndpoint();
        var dsn = this._dsnObject;
        return "" + base + dsn.projectId + "/" + target + "/";
    };
    /**
     * Returns the store endpoint URL with auth in the query string.
     *
     * Sending auth as part of the query string and not as custom HTTP headers avoids CORS preflight requests.
     */
    API.prototype.getStoreEndpointWithUrlEncodedAuth = function () {
        return this.getStoreEndpoint() + "?" + this._encodedAuth();
    };
    /**
     * Returns the envelope endpoint URL with auth in the query string.
     *
     * Sending auth as part of the query string and not as custom HTTP headers avoids CORS preflight requests.
     */
    API.prototype.getEnvelopeEndpointWithUrlEncodedAuth = function () {
        return this._getEnvelopeEndpoint() + "?" + this._encodedAuth();
    };
    /** Returns a URL-encoded string with auth config suitable for a query string. */
    API.prototype._encodedAuth = function () {
        var dsn = this._dsnObject;
        var auth = {
            // We send only the minimum set of required information. See
            // https://github.com/getsentry/sentry-javascript/issues/2572.
            sentry_key: dsn.user,
            sentry_version: SENTRY_API_VERSION,
        };
        return Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_0__["urlEncode"])(auth);
    };
    /** Returns only the path component for the store endpoint. */
    API.prototype.getStoreEndpointPath = function () {
        var dsn = this._dsnObject;
        return (dsn.path ? "/" + dsn.path : '') + "/api/" + dsn.projectId + "/store/";
    };
    /**
     * Returns an object that can be used in request headers.
     * This is needed for node and the old /store endpoint in sentry
     */
    API.prototype.getRequestHeaders = function (clientName, clientVersion) {
        var dsn = this._dsnObject;
        var header = ["Sentry sentry_version=" + SENTRY_API_VERSION];
        header.push("sentry_client=" + clientName + "/" + clientVersion);
        header.push("sentry_key=" + dsn.user);
        if (dsn.pass) {
            header.push("sentry_secret=" + dsn.pass);
        }
        return {
            'Content-Type': 'application/json',
            'X-Sentry-Auth': header.join(', '),
        };
    };
    /** Returns the url to the report dialog endpoint. */
    API.prototype.getReportDialogEndpoint = function (dialogOptions) {
        if (dialogOptions === void 0) { dialogOptions = {}; }
        var dsn = this._dsnObject;
        var endpoint = this.getBaseApiEndpoint() + "embed/error-page/";
        var encodedOptions = [];
        encodedOptions.push("dsn=" + dsn.toString());
        for (var key in dialogOptions) {
            if (key === 'user') {
                if (!dialogOptions.user) {
                    continue;
                }
                if (dialogOptions.user.name) {
                    encodedOptions.push("name=" + encodeURIComponent(dialogOptions.user.name));
                }
                if (dialogOptions.user.email) {
                    encodedOptions.push("email=" + encodeURIComponent(dialogOptions.user.email));
                }
            }
            else {
                encodedOptions.push(encodeURIComponent(key) + "=" + encodeURIComponent(dialogOptions[key]));
            }
        }
        if (encodedOptions.length) {
            return endpoint + "?" + encodedOptions.join('&');
        }
        return endpoint;
    };
    return API;
}());

//# sourceMappingURL=api.js.map

/***/ }),

/***/ "./node_modules/@sentry/core/esm/basebackend.js":
/*!******************************************************!*\
  !*** ./node_modules/@sentry/core/esm/basebackend.js ***!
  \******************************************************/
/*! exports provided: BaseBackend */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BaseBackend", function() { return BaseBackend; });
/* harmony import */ var _sentry_utils__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @sentry/utils */ "./node_modules/@sentry/utils/esm/index.js");
/* harmony import */ var _transports_noop__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./transports/noop */ "./node_modules/@sentry/core/esm/transports/noop.js");


/**
 * This is the base implemention of a Backend.
 * @hidden
 */
var BaseBackend = /** @class */ (function () {
    /** Creates a new backend instance. */
    function BaseBackend(options) {
        this._options = options;
        if (!this._options.dsn) {
            _sentry_utils__WEBPACK_IMPORTED_MODULE_0__["logger"].warn('No DSN provided, backend will not do anything.');
        }
        this._transport = this._setupTransport();
    }
    /**
     * Sets up the transport so it can be used later to send requests.
     */
    BaseBackend.prototype._setupTransport = function () {
        return new _transports_noop__WEBPACK_IMPORTED_MODULE_1__["NoopTransport"]();
    };
    /**
     * @inheritDoc
     */
    BaseBackend.prototype.eventFromException = function (_exception, _hint) {
        throw new _sentry_utils__WEBPACK_IMPORTED_MODULE_0__["SentryError"]('Backend has to implement `eventFromException` method');
    };
    /**
     * @inheritDoc
     */
    BaseBackend.prototype.eventFromMessage = function (_message, _level, _hint) {
        throw new _sentry_utils__WEBPACK_IMPORTED_MODULE_0__["SentryError"]('Backend has to implement `eventFromMessage` method');
    };
    /**
     * @inheritDoc
     */
    BaseBackend.prototype.sendEvent = function (event) {
        this._transport.sendEvent(event).then(null, function (reason) {
            _sentry_utils__WEBPACK_IMPORTED_MODULE_0__["logger"].error("Error while sending event: " + reason);
        });
    };
    /**
     * @inheritDoc
     */
    BaseBackend.prototype.getTransport = function () {
        return this._transport;
    };
    return BaseBackend;
}());

//# sourceMappingURL=basebackend.js.map

/***/ }),

/***/ "./node_modules/@sentry/core/esm/baseclient.js":
/*!*****************************************************!*\
  !*** ./node_modules/@sentry/core/esm/baseclient.js ***!
  \*****************************************************/
/*! exports provided: BaseClient */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BaseClient", function() { return BaseClient; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _sentry_hub__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @sentry/hub */ "./node_modules/@sentry/hub/esm/index.js");
/* harmony import */ var _sentry_utils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @sentry/utils */ "./node_modules/@sentry/utils/esm/index.js");
/* harmony import */ var _integration__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./integration */ "./node_modules/@sentry/core/esm/integration.js");




/**
 * Base implementation for all JavaScript SDK clients.
 *
 * Call the constructor with the corresponding backend constructor and options
 * specific to the client subclass. To access these options later, use
 * {@link Client.getOptions}. Also, the Backend instance is available via
 * {@link Client.getBackend}.
 *
 * If a Dsn is specified in the options, it will be parsed and stored. Use
 * {@link Client.getDsn} to retrieve the Dsn at any moment. In case the Dsn is
 * invalid, the constructor will throw a {@link SentryException}. Note that
 * without a valid Dsn, the SDK will not send any events to Sentry.
 *
 * Before sending an event via the backend, it is passed through
 * {@link BaseClient.prepareEvent} to add SDK information and scope data
 * (breadcrumbs and context). To add more custom information, override this
 * method and extend the resulting prepared event.
 *
 * To issue automatically created events (e.g. via instrumentation), use
 * {@link Client.captureEvent}. It will prepare the event and pass it through
 * the callback lifecycle. To issue auto-breadcrumbs, use
 * {@link Client.addBreadcrumb}.
 *
 * @example
 * class NodeClient extends BaseClient<NodeBackend, NodeOptions> {
 *   public constructor(options: NodeOptions) {
 *     super(NodeBackend, options);
 *   }
 *
 *   // ...
 * }
 */
var BaseClient = /** @class */ (function () {
    /**
     * Initializes this client instance.
     *
     * @param backendClass A constructor function to create the backend.
     * @param options Options for the client.
     */
    function BaseClient(backendClass, options) {
        /** Array of used integrations. */
        this._integrations = {};
        /** Is the client still processing a call? */
        this._processing = false;
        this._backend = new backendClass(options);
        this._options = options;
        if (options.dsn) {
            this._dsn = new _sentry_utils__WEBPACK_IMPORTED_MODULE_2__["Dsn"](options.dsn);
        }
    }
    /**
     * @inheritDoc
     */
    BaseClient.prototype.captureException = function (exception, hint, scope) {
        var _this = this;
        var eventId = hint && hint.event_id;
        this._processing = true;
        this._getBackend()
            .eventFromException(exception, hint)
            .then(function (event) {
            eventId = _this.captureEvent(event, hint, scope);
        });
        return eventId;
    };
    /**
     * @inheritDoc
     */
    BaseClient.prototype.captureMessage = function (message, level, hint, scope) {
        var _this = this;
        var eventId = hint && hint.event_id;
        this._processing = true;
        var promisedEvent = Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_2__["isPrimitive"])(message)
            ? this._getBackend().eventFromMessage("" + message, level, hint)
            : this._getBackend().eventFromException(message, hint);
        promisedEvent.then(function (event) {
            eventId = _this.captureEvent(event, hint, scope);
        });
        return eventId;
    };
    /**
     * @inheritDoc
     */
    BaseClient.prototype.captureEvent = function (event, hint, scope) {
        var _this = this;
        var eventId = hint && hint.event_id;
        this._processing = true;
        this._processEvent(event, hint, scope)
            .then(function (finalEvent) {
            // We need to check for finalEvent in case beforeSend returned null
            eventId = finalEvent && finalEvent.event_id;
            _this._processing = false;
        })
            .then(null, function (reason) {
            _sentry_utils__WEBPACK_IMPORTED_MODULE_2__["logger"].error(reason);
            _this._processing = false;
        });
        return eventId;
    };
    /**
     * @inheritDoc
     */
    BaseClient.prototype.getDsn = function () {
        return this._dsn;
    };
    /**
     * @inheritDoc
     */
    BaseClient.prototype.getOptions = function () {
        return this._options;
    };
    /**
     * @inheritDoc
     */
    BaseClient.prototype.flush = function (timeout) {
        var _this = this;
        return this._isClientProcessing(timeout).then(function (status) {
            clearInterval(status.interval);
            return _this._getBackend()
                .getTransport()
                .close(timeout)
                .then(function (transportFlushed) { return status.ready && transportFlushed; });
        });
    };
    /**
     * @inheritDoc
     */
    BaseClient.prototype.close = function (timeout) {
        var _this = this;
        return this.flush(timeout).then(function (result) {
            _this.getOptions().enabled = false;
            return result;
        });
    };
    /**
     * Sets up the integrations
     */
    BaseClient.prototype.setupIntegrations = function () {
        if (this._isEnabled()) {
            this._integrations = Object(_integration__WEBPACK_IMPORTED_MODULE_3__["setupIntegrations"])(this._options);
        }
    };
    /**
     * @inheritDoc
     */
    BaseClient.prototype.getIntegration = function (integration) {
        try {
            return this._integrations[integration.id] || null;
        }
        catch (_oO) {
            _sentry_utils__WEBPACK_IMPORTED_MODULE_2__["logger"].warn("Cannot retrieve integration " + integration.id + " from the current Client");
            return null;
        }
    };
    /** Waits for the client to be done with processing. */
    BaseClient.prototype._isClientProcessing = function (timeout) {
        var _this = this;
        return new _sentry_utils__WEBPACK_IMPORTED_MODULE_2__["SyncPromise"](function (resolve) {
            var ticked = 0;
            var tick = 1;
            var interval = 0;
            clearInterval(interval);
            interval = setInterval(function () {
                if (!_this._processing) {
                    resolve({
                        interval: interval,
                        ready: true,
                    });
                }
                else {
                    ticked += tick;
                    if (timeout && ticked >= timeout) {
                        resolve({
                            interval: interval,
                            ready: false,
                        });
                    }
                }
            }, tick);
        });
    };
    /** Returns the current backend. */
    BaseClient.prototype._getBackend = function () {
        return this._backend;
    };
    /** Determines whether this SDK is enabled and a valid Dsn is present. */
    BaseClient.prototype._isEnabled = function () {
        return this.getOptions().enabled !== false && this._dsn !== undefined;
    };
    /**
     * Adds common information to events.
     *
     * The information includes release and environment from `options`,
     * breadcrumbs and context (extra, tags and user) from the scope.
     *
     * Information that is already present in the event is never overwritten. For
     * nested objects, such as the context, keys are merged.
     *
     * @param event The original event.
     * @param hint May contain additional information about the original exception.
     * @param scope A scope containing event metadata.
     * @returns A new event with more information.
     */
    BaseClient.prototype._prepareEvent = function (event, scope, hint) {
        var _this = this;
        var _a = this.getOptions().normalizeDepth, normalizeDepth = _a === void 0 ? 3 : _a;
        var prepared = tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, event, { event_id: event.event_id || (hint && hint.event_id ? hint.event_id : Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_2__["uuid4"])()), timestamp: event.timestamp || Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_2__["timestampWithMs"])() });
        this._applyClientOptions(prepared);
        this._applyIntegrationsMetadata(prepared);
        // If we have scope given to us, use it as the base for further modifications.
        // This allows us to prevent unnecessary copying of data if `captureContext` is not provided.
        var finalScope = scope;
        if (hint && hint.captureContext) {
            finalScope = _sentry_hub__WEBPACK_IMPORTED_MODULE_1__["Scope"].clone(finalScope).update(hint.captureContext);
        }
        // We prepare the result here with a resolved Event.
        var result = _sentry_utils__WEBPACK_IMPORTED_MODULE_2__["SyncPromise"].resolve(prepared);
        // This should be the last thing called, since we want that
        // {@link Hub.addEventProcessor} gets the finished prepared event.
        if (finalScope) {
            // In case we have a hub we reassign it.
            result = finalScope.applyToEvent(prepared, hint);
        }
        return result.then(function (evt) {
            // tslint:disable-next-line:strict-type-predicates
            if (typeof normalizeDepth === 'number' && normalizeDepth > 0) {
                return _this._normalizeEvent(evt, normalizeDepth);
            }
            return evt;
        });
    };
    /**
     * Applies `normalize` function on necessary `Event` attributes to make them safe for serialization.
     * Normalized keys:
     * - `breadcrumbs.data`
     * - `user`
     * - `contexts`
     * - `extra`
     * @param event Event
     * @returns Normalized event
     */
    BaseClient.prototype._normalizeEvent = function (event, depth) {
        if (!event) {
            return null;
        }
        // tslint:disable:no-unsafe-any
        var normalized = tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, event, (event.breadcrumbs && {
            breadcrumbs: event.breadcrumbs.map(function (b) { return (tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, b, (b.data && {
                data: Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_2__["normalize"])(b.data, depth),
            }))); }),
        }), (event.user && {
            user: Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_2__["normalize"])(event.user, depth),
        }), (event.contexts && {
            contexts: Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_2__["normalize"])(event.contexts, depth),
        }), (event.extra && {
            extra: Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_2__["normalize"])(event.extra, depth),
        }));
        // event.contexts.trace stores information about a Transaction. Similarly,
        // event.spans[] stores information about child Spans. Given that a
        // Transaction is conceptually a Span, normalization should apply to both
        // Transactions and Spans consistently.
        // For now the decision is to skip normalization of Transactions and Spans,
        // so this block overwrites the normalized event to add back the original
        // Transaction information prior to normalization.
        if (event.contexts && event.contexts.trace) {
            normalized.contexts.trace = event.contexts.trace;
        }
        return normalized;
    };
    /**
     *  Enhances event using the client configuration.
     *  It takes care of all "static" values like environment, release and `dist`,
     *  as well as truncating overly long values.
     * @param event event instance to be enhanced
     */
    BaseClient.prototype._applyClientOptions = function (event) {
        var _a = this.getOptions(), environment = _a.environment, release = _a.release, dist = _a.dist, _b = _a.maxValueLength, maxValueLength = _b === void 0 ? 250 : _b;
        if (event.environment === undefined && environment !== undefined) {
            event.environment = environment;
        }
        if (event.release === undefined && release !== undefined) {
            event.release = release;
        }
        if (event.dist === undefined && dist !== undefined) {
            event.dist = dist;
        }
        if (event.message) {
            event.message = Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_2__["truncate"])(event.message, maxValueLength);
        }
        var exception = event.exception && event.exception.values && event.exception.values[0];
        if (exception && exception.value) {
            exception.value = Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_2__["truncate"])(exception.value, maxValueLength);
        }
        var request = event.request;
        if (request && request.url) {
            request.url = Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_2__["truncate"])(request.url, maxValueLength);
        }
    };
    /**
     * This function adds all used integrations to the SDK info in the event.
     * @param sdkInfo The sdkInfo of the event that will be filled with all integrations.
     */
    BaseClient.prototype._applyIntegrationsMetadata = function (event) {
        var sdkInfo = event.sdk;
        var integrationsArray = Object.keys(this._integrations);
        if (sdkInfo && integrationsArray.length > 0) {
            sdkInfo.integrations = integrationsArray;
        }
    };
    /**
     * Tells the backend to send this event
     * @param event The Sentry event to send
     */
    BaseClient.prototype._sendEvent = function (event) {
        this._getBackend().sendEvent(event);
    };
    /**
     * Processes an event (either error or message) and sends it to Sentry.
     *
     * This also adds breadcrumbs and context information to the event. However,
     * platform specific meta data (such as the User's IP address) must be added
     * by the SDK implementor.
     *
     *
     * @param event The event to send to Sentry.
     * @param hint May contain additional information about the original exception.
     * @param scope A scope containing event metadata.
     * @returns A SyncPromise that resolves with the event or rejects in case event was/will not be send.
     */
    BaseClient.prototype._processEvent = function (event, hint, scope) {
        var _this = this;
        var _a = this.getOptions(), beforeSend = _a.beforeSend, sampleRate = _a.sampleRate;
        if (!this._isEnabled()) {
            return _sentry_utils__WEBPACK_IMPORTED_MODULE_2__["SyncPromise"].reject('SDK not enabled, will not send event.');
        }
        var isTransaction = event.type === 'transaction';
        // 1.0 === 100% events are sent
        // 0.0 === 0% events are sent
        // Sampling for transaction happens somewhere else
        if (!isTransaction && typeof sampleRate === 'number' && Math.random() > sampleRate) {
            return _sentry_utils__WEBPACK_IMPORTED_MODULE_2__["SyncPromise"].reject('This event has been sampled, will not send event.');
        }
        return new _sentry_utils__WEBPACK_IMPORTED_MODULE_2__["SyncPromise"](function (resolve, reject) {
            _this._prepareEvent(event, scope, hint)
                .then(function (prepared) {
                if (prepared === null) {
                    reject('An event processor returned null, will not send event.');
                    return;
                }
                var finalEvent = prepared;
                var isInternalException = hint && hint.data && hint.data.__sentry__ === true;
                // We skip beforeSend in case of transactions
                if (isInternalException || !beforeSend || isTransaction) {
                    _this._sendEvent(finalEvent);
                    resolve(finalEvent);
                    return;
                }
                var beforeSendResult = beforeSend(prepared, hint);
                // tslint:disable-next-line:strict-type-predicates
                if (typeof beforeSendResult === 'undefined') {
                    _sentry_utils__WEBPACK_IMPORTED_MODULE_2__["logger"].error('`beforeSend` method has to return `null` or a valid event.');
                }
                else if (Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_2__["isThenable"])(beforeSendResult)) {
                    _this._handleAsyncBeforeSend(beforeSendResult, resolve, reject);
                }
                else {
                    finalEvent = beforeSendResult;
                    if (finalEvent === null) {
                        _sentry_utils__WEBPACK_IMPORTED_MODULE_2__["logger"].log('`beforeSend` returned `null`, will not send event.');
                        resolve(null);
                        return;
                    }
                    // From here on we are really async
                    _this._sendEvent(finalEvent);
                    resolve(finalEvent);
                }
            })
                .then(null, function (reason) {
                _this.captureException(reason, {
                    data: {
                        __sentry__: true,
                    },
                    originalException: reason,
                });
                reject("Event processing pipeline threw an error, original event will not be sent. Details have been sent as a new event.\nReason: " + reason);
            });
        });
    };
    /**
     * Resolves before send Promise and calls resolve/reject on parent SyncPromise.
     */
    BaseClient.prototype._handleAsyncBeforeSend = function (beforeSend, resolve, reject) {
        var _this = this;
        beforeSend
            .then(function (processedEvent) {
            if (processedEvent === null) {
                reject('`beforeSend` returned `null`, will not send event.');
                return;
            }
            // From here on we are really async
            _this._sendEvent(processedEvent);
            resolve(processedEvent);
        })
            .then(null, function (e) {
            reject("beforeSend rejected with " + e);
        });
    };
    return BaseClient;
}());

//# sourceMappingURL=baseclient.js.map

/***/ }),

/***/ "./node_modules/@sentry/core/esm/index.js":
/*!************************************************!*\
  !*** ./node_modules/@sentry/core/esm/index.js ***!
  \************************************************/
/*! exports provided: addBreadcrumb, captureException, captureEvent, captureMessage, configureScope, startTransaction, setContext, setExtra, setExtras, setTag, setTags, setUser, withScope, addGlobalEventProcessor, getCurrentHub, getHubFromCarrier, Hub, makeMain, Scope, API, BaseClient, BaseBackend, eventToSentryRequest, initAndBind, NoopTransport, Integrations */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _sentry_minimal__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @sentry/minimal */ "./node_modules/@sentry/minimal/esm/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "addBreadcrumb", function() { return _sentry_minimal__WEBPACK_IMPORTED_MODULE_0__["addBreadcrumb"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "captureException", function() { return _sentry_minimal__WEBPACK_IMPORTED_MODULE_0__["captureException"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "captureEvent", function() { return _sentry_minimal__WEBPACK_IMPORTED_MODULE_0__["captureEvent"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "captureMessage", function() { return _sentry_minimal__WEBPACK_IMPORTED_MODULE_0__["captureMessage"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "configureScope", function() { return _sentry_minimal__WEBPACK_IMPORTED_MODULE_0__["configureScope"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "startTransaction", function() { return _sentry_minimal__WEBPACK_IMPORTED_MODULE_0__["startTransaction"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "setContext", function() { return _sentry_minimal__WEBPACK_IMPORTED_MODULE_0__["setContext"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "setExtra", function() { return _sentry_minimal__WEBPACK_IMPORTED_MODULE_0__["setExtra"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "setExtras", function() { return _sentry_minimal__WEBPACK_IMPORTED_MODULE_0__["setExtras"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "setTag", function() { return _sentry_minimal__WEBPACK_IMPORTED_MODULE_0__["setTag"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "setTags", function() { return _sentry_minimal__WEBPACK_IMPORTED_MODULE_0__["setTags"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "setUser", function() { return _sentry_minimal__WEBPACK_IMPORTED_MODULE_0__["setUser"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "withScope", function() { return _sentry_minimal__WEBPACK_IMPORTED_MODULE_0__["withScope"]; });

/* harmony import */ var _sentry_hub__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @sentry/hub */ "./node_modules/@sentry/hub/esm/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "addGlobalEventProcessor", function() { return _sentry_hub__WEBPACK_IMPORTED_MODULE_1__["addGlobalEventProcessor"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "getCurrentHub", function() { return _sentry_hub__WEBPACK_IMPORTED_MODULE_1__["getCurrentHub"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "getHubFromCarrier", function() { return _sentry_hub__WEBPACK_IMPORTED_MODULE_1__["getHubFromCarrier"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Hub", function() { return _sentry_hub__WEBPACK_IMPORTED_MODULE_1__["Hub"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "makeMain", function() { return _sentry_hub__WEBPACK_IMPORTED_MODULE_1__["makeMain"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Scope", function() { return _sentry_hub__WEBPACK_IMPORTED_MODULE_1__["Scope"]; });

/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./api */ "./node_modules/@sentry/core/esm/api.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "API", function() { return _api__WEBPACK_IMPORTED_MODULE_2__["API"]; });

/* harmony import */ var _baseclient__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./baseclient */ "./node_modules/@sentry/core/esm/baseclient.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "BaseClient", function() { return _baseclient__WEBPACK_IMPORTED_MODULE_3__["BaseClient"]; });

/* harmony import */ var _basebackend__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./basebackend */ "./node_modules/@sentry/core/esm/basebackend.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "BaseBackend", function() { return _basebackend__WEBPACK_IMPORTED_MODULE_4__["BaseBackend"]; });

/* harmony import */ var _request__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./request */ "./node_modules/@sentry/core/esm/request.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "eventToSentryRequest", function() { return _request__WEBPACK_IMPORTED_MODULE_5__["eventToSentryRequest"]; });

/* harmony import */ var _sdk__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./sdk */ "./node_modules/@sentry/core/esm/sdk.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "initAndBind", function() { return _sdk__WEBPACK_IMPORTED_MODULE_6__["initAndBind"]; });

/* harmony import */ var _transports_noop__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./transports/noop */ "./node_modules/@sentry/core/esm/transports/noop.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "NoopTransport", function() { return _transports_noop__WEBPACK_IMPORTED_MODULE_7__["NoopTransport"]; });

/* harmony import */ var _integrations__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./integrations */ "./node_modules/@sentry/core/esm/integrations/index.js");
/* harmony reexport (module object) */ __webpack_require__.d(__webpack_exports__, "Integrations", function() { return _integrations__WEBPACK_IMPORTED_MODULE_8__; });










//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./node_modules/@sentry/core/esm/integration.js":
/*!******************************************************!*\
  !*** ./node_modules/@sentry/core/esm/integration.js ***!
  \******************************************************/
/*! exports provided: installedIntegrations, getIntegrationsToSetup, setupIntegration, setupIntegrations */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "installedIntegrations", function() { return installedIntegrations; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getIntegrationsToSetup", function() { return getIntegrationsToSetup; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setupIntegration", function() { return setupIntegration; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setupIntegrations", function() { return setupIntegrations; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _sentry_hub__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @sentry/hub */ "./node_modules/@sentry/hub/esm/index.js");
/* harmony import */ var _sentry_utils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @sentry/utils */ "./node_modules/@sentry/utils/esm/index.js");



var installedIntegrations = [];
/** Gets integration to install */
function getIntegrationsToSetup(options) {
    var defaultIntegrations = (options.defaultIntegrations && tslib__WEBPACK_IMPORTED_MODULE_0__["__spread"](options.defaultIntegrations)) || [];
    var userIntegrations = options.integrations;
    var integrations = [];
    if (Array.isArray(userIntegrations)) {
        var userIntegrationsNames_1 = userIntegrations.map(function (i) { return i.name; });
        var pickedIntegrationsNames_1 = [];
        // Leave only unique default integrations, that were not overridden with provided user integrations
        defaultIntegrations.forEach(function (defaultIntegration) {
            if (userIntegrationsNames_1.indexOf(defaultIntegration.name) === -1 &&
                pickedIntegrationsNames_1.indexOf(defaultIntegration.name) === -1) {
                integrations.push(defaultIntegration);
                pickedIntegrationsNames_1.push(defaultIntegration.name);
            }
        });
        // Don't add same user integration twice
        userIntegrations.forEach(function (userIntegration) {
            if (pickedIntegrationsNames_1.indexOf(userIntegration.name) === -1) {
                integrations.push(userIntegration);
                pickedIntegrationsNames_1.push(userIntegration.name);
            }
        });
    }
    else if (typeof userIntegrations === 'function') {
        integrations = userIntegrations(defaultIntegrations);
        integrations = Array.isArray(integrations) ? integrations : [integrations];
    }
    else {
        integrations = tslib__WEBPACK_IMPORTED_MODULE_0__["__spread"](defaultIntegrations);
    }
    // Make sure that if present, `Debug` integration will always run last
    var integrationsNames = integrations.map(function (i) { return i.name; });
    var alwaysLastToRun = 'Debug';
    if (integrationsNames.indexOf(alwaysLastToRun) !== -1) {
        integrations.push.apply(integrations, tslib__WEBPACK_IMPORTED_MODULE_0__["__spread"](integrations.splice(integrationsNames.indexOf(alwaysLastToRun), 1)));
    }
    return integrations;
}
/** Setup given integration */
function setupIntegration(integration) {
    if (installedIntegrations.indexOf(integration.name) !== -1) {
        return;
    }
    integration.setupOnce(_sentry_hub__WEBPACK_IMPORTED_MODULE_1__["addGlobalEventProcessor"], _sentry_hub__WEBPACK_IMPORTED_MODULE_1__["getCurrentHub"]);
    installedIntegrations.push(integration.name);
    _sentry_utils__WEBPACK_IMPORTED_MODULE_2__["logger"].log("Integration installed: " + integration.name);
}
/**
 * Given a list of integration instances this installs them all. When `withDefaults` is set to `true` then all default
 * integrations are added unless they were already provided before.
 * @param integrations array of integration instances
 * @param withDefault should enable default integrations
 */
function setupIntegrations(options) {
    var integrations = {};
    getIntegrationsToSetup(options).forEach(function (integration) {
        integrations[integration.name] = integration;
        setupIntegration(integration);
    });
    return integrations;
}
//# sourceMappingURL=integration.js.map

/***/ }),

/***/ "./node_modules/@sentry/core/esm/integrations/functiontostring.js":
/*!************************************************************************!*\
  !*** ./node_modules/@sentry/core/esm/integrations/functiontostring.js ***!
  \************************************************************************/
/*! exports provided: FunctionToString */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FunctionToString", function() { return FunctionToString; });
var originalFunctionToString;
/** Patch toString calls to return proper name for wrapped functions */
var FunctionToString = /** @class */ (function () {
    function FunctionToString() {
        /**
         * @inheritDoc
         */
        this.name = FunctionToString.id;
    }
    /**
     * @inheritDoc
     */
    FunctionToString.prototype.setupOnce = function () {
        originalFunctionToString = Function.prototype.toString;
        Function.prototype.toString = function () {
            var args = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                args[_i] = arguments[_i];
            }
            var context = this.__sentry_original__ || this;
            // tslint:disable-next-line:no-unsafe-any
            return originalFunctionToString.apply(context, args);
        };
    };
    /**
     * @inheritDoc
     */
    FunctionToString.id = 'FunctionToString';
    return FunctionToString;
}());

//# sourceMappingURL=functiontostring.js.map

/***/ }),

/***/ "./node_modules/@sentry/core/esm/integrations/inboundfilters.js":
/*!**********************************************************************!*\
  !*** ./node_modules/@sentry/core/esm/integrations/inboundfilters.js ***!
  \**********************************************************************/
/*! exports provided: InboundFilters */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InboundFilters", function() { return InboundFilters; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _sentry_hub__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @sentry/hub */ "./node_modules/@sentry/hub/esm/index.js");
/* harmony import */ var _sentry_utils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @sentry/utils */ "./node_modules/@sentry/utils/esm/index.js");



// "Script error." is hard coded into browsers for errors that it can't read.
// this is the result of a script being pulled in from an external domain and CORS.
var DEFAULT_IGNORE_ERRORS = [/^Script error\.?$/, /^Javascript error: Script error\.? on line 0$/];
/** Inbound filters configurable by the user */
var InboundFilters = /** @class */ (function () {
    function InboundFilters(_options) {
        if (_options === void 0) { _options = {}; }
        this._options = _options;
        /**
         * @inheritDoc
         */
        this.name = InboundFilters.id;
    }
    /**
     * @inheritDoc
     */
    InboundFilters.prototype.setupOnce = function () {
        Object(_sentry_hub__WEBPACK_IMPORTED_MODULE_1__["addGlobalEventProcessor"])(function (event) {
            var hub = Object(_sentry_hub__WEBPACK_IMPORTED_MODULE_1__["getCurrentHub"])();
            if (!hub) {
                return event;
            }
            var self = hub.getIntegration(InboundFilters);
            if (self) {
                var client = hub.getClient();
                var clientOptions = client ? client.getOptions() : {};
                var options = self._mergeOptions(clientOptions);
                if (self._shouldDropEvent(event, options)) {
                    return null;
                }
            }
            return event;
        });
    };
    /** JSDoc */
    InboundFilters.prototype._shouldDropEvent = function (event, options) {
        if (this._isSentryError(event, options)) {
            _sentry_utils__WEBPACK_IMPORTED_MODULE_2__["logger"].warn("Event dropped due to being internal Sentry Error.\nEvent: " + Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_2__["getEventDescription"])(event));
            return true;
        }
        if (this._isIgnoredError(event, options)) {
            _sentry_utils__WEBPACK_IMPORTED_MODULE_2__["logger"].warn("Event dropped due to being matched by `ignoreErrors` option.\nEvent: " + Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_2__["getEventDescription"])(event));
            return true;
        }
        if (this._isDeniedUrl(event, options)) {
            _sentry_utils__WEBPACK_IMPORTED_MODULE_2__["logger"].warn("Event dropped due to being matched by `denyUrls` option.\nEvent: " + Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_2__["getEventDescription"])(event) + ".\nUrl: " + this._getEventFilterUrl(event));
            return true;
        }
        if (!this._isAllowedUrl(event, options)) {
            _sentry_utils__WEBPACK_IMPORTED_MODULE_2__["logger"].warn("Event dropped due to not being matched by `allowUrls` option.\nEvent: " + Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_2__["getEventDescription"])(event) + ".\nUrl: " + this._getEventFilterUrl(event));
            return true;
        }
        return false;
    };
    /** JSDoc */
    InboundFilters.prototype._isSentryError = function (event, options) {
        if (!options.ignoreInternal) {
            return false;
        }
        try {
            return ((event &&
                event.exception &&
                event.exception.values &&
                event.exception.values[0] &&
                event.exception.values[0].type === 'SentryError') ||
                false);
        }
        catch (_oO) {
            return false;
        }
    };
    /** JSDoc */
    InboundFilters.prototype._isIgnoredError = function (event, options) {
        if (!options.ignoreErrors || !options.ignoreErrors.length) {
            return false;
        }
        return this._getPossibleEventMessages(event).some(function (message) {
            // Not sure why TypeScript complains here...
            return options.ignoreErrors.some(function (pattern) { return Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_2__["isMatchingPattern"])(message, pattern); });
        });
    };
    /** JSDoc */
    InboundFilters.prototype._isDeniedUrl = function (event, options) {
        // TODO: Use Glob instead?
        if (!options.denyUrls || !options.denyUrls.length) {
            return false;
        }
        var url = this._getEventFilterUrl(event);
        return !url ? false : options.denyUrls.some(function (pattern) { return Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_2__["isMatchingPattern"])(url, pattern); });
    };
    /** JSDoc */
    InboundFilters.prototype._isAllowedUrl = function (event, options) {
        // TODO: Use Glob instead?
        if (!options.allowUrls || !options.allowUrls.length) {
            return true;
        }
        var url = this._getEventFilterUrl(event);
        return !url ? true : options.allowUrls.some(function (pattern) { return Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_2__["isMatchingPattern"])(url, pattern); });
    };
    /** JSDoc */
    InboundFilters.prototype._mergeOptions = function (clientOptions) {
        if (clientOptions === void 0) { clientOptions = {}; }
        // tslint:disable:deprecation
        return {
            allowUrls: tslib__WEBPACK_IMPORTED_MODULE_0__["__spread"]((this._options.whitelistUrls || []), (this._options.allowUrls || []), (clientOptions.whitelistUrls || []), (clientOptions.allowUrls || [])),
            denyUrls: tslib__WEBPACK_IMPORTED_MODULE_0__["__spread"]((this._options.blacklistUrls || []), (this._options.denyUrls || []), (clientOptions.blacklistUrls || []), (clientOptions.denyUrls || [])),
            ignoreErrors: tslib__WEBPACK_IMPORTED_MODULE_0__["__spread"]((this._options.ignoreErrors || []), (clientOptions.ignoreErrors || []), DEFAULT_IGNORE_ERRORS),
            ignoreInternal: typeof this._options.ignoreInternal !== 'undefined' ? this._options.ignoreInternal : true,
        };
    };
    /** JSDoc */
    InboundFilters.prototype._getPossibleEventMessages = function (event) {
        if (event.message) {
            return [event.message];
        }
        if (event.exception) {
            try {
                var _a = (event.exception.values && event.exception.values[0]) || {}, _b = _a.type, type = _b === void 0 ? '' : _b, _c = _a.value, value = _c === void 0 ? '' : _c;
                return ["" + value, type + ": " + value];
            }
            catch (oO) {
                _sentry_utils__WEBPACK_IMPORTED_MODULE_2__["logger"].error("Cannot extract message for event " + Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_2__["getEventDescription"])(event));
                return [];
            }
        }
        return [];
    };
    /** JSDoc */
    InboundFilters.prototype._getEventFilterUrl = function (event) {
        try {
            if (event.stacktrace) {
                var frames_1 = event.stacktrace.frames;
                return (frames_1 && frames_1[frames_1.length - 1].filename) || null;
            }
            if (event.exception) {
                var frames_2 = event.exception.values && event.exception.values[0].stacktrace && event.exception.values[0].stacktrace.frames;
                return (frames_2 && frames_2[frames_2.length - 1].filename) || null;
            }
            return null;
        }
        catch (oO) {
            _sentry_utils__WEBPACK_IMPORTED_MODULE_2__["logger"].error("Cannot extract url for event " + Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_2__["getEventDescription"])(event));
            return null;
        }
    };
    /**
     * @inheritDoc
     */
    InboundFilters.id = 'InboundFilters';
    return InboundFilters;
}());

//# sourceMappingURL=inboundfilters.js.map

/***/ }),

/***/ "./node_modules/@sentry/core/esm/integrations/index.js":
/*!*************************************************************!*\
  !*** ./node_modules/@sentry/core/esm/integrations/index.js ***!
  \*************************************************************/
/*! exports provided: FunctionToString, InboundFilters */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _functiontostring__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./functiontostring */ "./node_modules/@sentry/core/esm/integrations/functiontostring.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "FunctionToString", function() { return _functiontostring__WEBPACK_IMPORTED_MODULE_0__["FunctionToString"]; });

/* harmony import */ var _inboundfilters__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./inboundfilters */ "./node_modules/@sentry/core/esm/integrations/inboundfilters.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "InboundFilters", function() { return _inboundfilters__WEBPACK_IMPORTED_MODULE_1__["InboundFilters"]; });



//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./node_modules/@sentry/core/esm/request.js":
/*!**************************************************!*\
  !*** ./node_modules/@sentry/core/esm/request.js ***!
  \**************************************************/
/*! exports provided: eventToSentryRequest */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "eventToSentryRequest", function() { return eventToSentryRequest; });
/* harmony import */ var _sentry_utils__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @sentry/utils */ "./node_modules/@sentry/utils/esm/index.js");

/** Creates a SentryRequest from an event. */
function eventToSentryRequest(event, api) {
    var useEnvelope = event.type === 'transaction';
    var req = {
        body: JSON.stringify(event),
        url: useEnvelope ? api.getEnvelopeEndpointWithUrlEncodedAuth() : api.getStoreEndpointWithUrlEncodedAuth(),
    };
    // https://develop.sentry.dev/sdk/envelopes/
    // Since we don't need to manipulate envelopes nor store them, there is no
    // exported concept of an Envelope with operations including serialization and
    // deserialization. Instead, we only implement a minimal subset of the spec to
    // serialize events inline here.
    if (useEnvelope) {
        var envelopeHeaders = JSON.stringify({
            event_id: event.event_id,
            // We need to add * 1000 since we divide it by 1000 by default but JS works with ms precision
            // The reason we use timestampWithMs here is that all clocks across the SDK use the same clock
            sent_at: new Date(Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_0__["timestampWithMs"])() * 1000).toISOString(),
        });
        var itemHeaders = JSON.stringify({
            type: event.type,
        });
        // The trailing newline is optional. We intentionally don't send it to avoid
        // sending unnecessary bytes.
        //
        // const envelope = `${envelopeHeaders}\n${itemHeaders}\n${req.body}\n`;
        var envelope = envelopeHeaders + "\n" + itemHeaders + "\n" + req.body;
        req.body = envelope;
    }
    return req;
}
//# sourceMappingURL=request.js.map

/***/ }),

/***/ "./node_modules/@sentry/core/esm/sdk.js":
/*!**********************************************!*\
  !*** ./node_modules/@sentry/core/esm/sdk.js ***!
  \**********************************************/
/*! exports provided: initAndBind */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "initAndBind", function() { return initAndBind; });
/* harmony import */ var _sentry_hub__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @sentry/hub */ "./node_modules/@sentry/hub/esm/index.js");
/* harmony import */ var _sentry_utils__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @sentry/utils */ "./node_modules/@sentry/utils/esm/index.js");


/**
 * Internal function to create a new SDK client instance. The client is
 * installed and then bound to the current scope.
 *
 * @param clientClass The client class to instanciate.
 * @param options Options to pass to the client.
 */
function initAndBind(clientClass, options) {
    if (options.debug === true) {
        _sentry_utils__WEBPACK_IMPORTED_MODULE_1__["logger"].enable();
    }
    var hub = Object(_sentry_hub__WEBPACK_IMPORTED_MODULE_0__["getCurrentHub"])();
    var client = new clientClass(options);
    hub.bindClient(client);
}
//# sourceMappingURL=sdk.js.map

/***/ }),

/***/ "./node_modules/@sentry/core/esm/transports/noop.js":
/*!**********************************************************!*\
  !*** ./node_modules/@sentry/core/esm/transports/noop.js ***!
  \**********************************************************/
/*! exports provided: NoopTransport */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NoopTransport", function() { return NoopTransport; });
/* harmony import */ var _sentry_types__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @sentry/types */ "./node_modules/@sentry/types/esm/index.js");
/* harmony import */ var _sentry_utils__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @sentry/utils */ "./node_modules/@sentry/utils/esm/index.js");


/** Noop transport */
var NoopTransport = /** @class */ (function () {
    function NoopTransport() {
    }
    /**
     * @inheritDoc
     */
    NoopTransport.prototype.sendEvent = function (_) {
        return _sentry_utils__WEBPACK_IMPORTED_MODULE_1__["SyncPromise"].resolve({
            reason: "NoopTransport: Event has been skipped because no Dsn is configured.",
            status: _sentry_types__WEBPACK_IMPORTED_MODULE_0__["Status"].Skipped,
        });
    };
    /**
     * @inheritDoc
     */
    NoopTransport.prototype.close = function (_) {
        return _sentry_utils__WEBPACK_IMPORTED_MODULE_1__["SyncPromise"].resolve(true);
    };
    return NoopTransport;
}());

//# sourceMappingURL=noop.js.map

/***/ }),

/***/ "./node_modules/@sentry/hub/esm/hub.js":
/*!*********************************************!*\
  !*** ./node_modules/@sentry/hub/esm/hub.js ***!
  \*********************************************/
/*! exports provided: API_VERSION, Hub, getMainCarrier, makeMain, getCurrentHub, getHubFromCarrier, setHubOnCarrier */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "API_VERSION", function() { return API_VERSION; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Hub", function() { return Hub; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getMainCarrier", function() { return getMainCarrier; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "makeMain", function() { return makeMain; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getCurrentHub", function() { return getCurrentHub; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getHubFromCarrier", function() { return getHubFromCarrier; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setHubOnCarrier", function() { return setHubOnCarrier; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _sentry_utils__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @sentry/utils */ "./node_modules/@sentry/utils/esm/index.js");
/* harmony import */ var _scope__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./scope */ "./node_modules/@sentry/hub/esm/scope.js");



/**
 * API compatibility version of this hub.
 *
 * WARNING: This number should only be incresed when the global interface
 * changes a and new methods are introduced.
 *
 * @hidden
 */
var API_VERSION = 3;
/**
 * Default maximum number of breadcrumbs added to an event. Can be overwritten
 * with {@link Options.maxBreadcrumbs}.
 */
var DEFAULT_BREADCRUMBS = 100;
/**
 * Absolute maximum number of breadcrumbs added to an event. The
 * `maxBreadcrumbs` option cannot be higher than this value.
 */
var MAX_BREADCRUMBS = 100;
/**
 * @inheritDoc
 */
var Hub = /** @class */ (function () {
    /**
     * Creates a new instance of the hub, will push one {@link Layer} into the
     * internal stack on creation.
     *
     * @param client bound to the hub.
     * @param scope bound to the hub.
     * @param version number, higher number means higher priority.
     */
    function Hub(client, scope, _version) {
        if (scope === void 0) { scope = new _scope__WEBPACK_IMPORTED_MODULE_2__["Scope"](); }
        if (_version === void 0) { _version = API_VERSION; }
        this._version = _version;
        /** Is a {@link Layer}[] containing the client and scope */
        this._stack = [];
        this._stack.push({ client: client, scope: scope });
        this.bindClient(client);
    }
    /**
     * Internal helper function to call a method on the top client if it exists.
     *
     * @param method The method to call on the client.
     * @param args Arguments to pass to the client function.
     */
    Hub.prototype._invokeClient = function (method) {
        var _a;
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        var top = this.getStackTop();
        if (top && top.client && top.client[method]) {
            (_a = top.client)[method].apply(_a, tslib__WEBPACK_IMPORTED_MODULE_0__["__spread"](args, [top.scope]));
        }
    };
    /**
     * @inheritDoc
     */
    Hub.prototype.isOlderThan = function (version) {
        return this._version < version;
    };
    /**
     * @inheritDoc
     */
    Hub.prototype.bindClient = function (client) {
        var top = this.getStackTop();
        top.client = client;
        if (client && client.setupIntegrations) {
            client.setupIntegrations();
        }
    };
    /**
     * @inheritDoc
     */
    Hub.prototype.pushScope = function () {
        // We want to clone the content of prev scope
        var stack = this.getStack();
        var parentScope = stack.length > 0 ? stack[stack.length - 1].scope : undefined;
        var scope = _scope__WEBPACK_IMPORTED_MODULE_2__["Scope"].clone(parentScope);
        this.getStack().push({
            client: this.getClient(),
            scope: scope,
        });
        return scope;
    };
    /**
     * @inheritDoc
     */
    Hub.prototype.popScope = function () {
        return this.getStack().pop() !== undefined;
    };
    /**
     * @inheritDoc
     */
    Hub.prototype.withScope = function (callback) {
        var scope = this.pushScope();
        try {
            callback(scope);
        }
        finally {
            this.popScope();
        }
    };
    /**
     * @inheritDoc
     */
    Hub.prototype.getClient = function () {
        return this.getStackTop().client;
    };
    /** Returns the scope of the top stack. */
    Hub.prototype.getScope = function () {
        return this.getStackTop().scope;
    };
    /** Returns the scope stack for domains or the process. */
    Hub.prototype.getStack = function () {
        return this._stack;
    };
    /** Returns the topmost scope layer in the order domain > local > process. */
    Hub.prototype.getStackTop = function () {
        return this._stack[this._stack.length - 1];
    };
    /**
     * @inheritDoc
     */
    Hub.prototype.captureException = function (exception, hint) {
        var eventId = (this._lastEventId = Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_1__["uuid4"])());
        var finalHint = hint;
        // If there's no explicit hint provided, mimick the same thing that would happen
        // in the minimal itself to create a consistent behavior.
        // We don't do this in the client, as it's the lowest level API, and doing this,
        // would prevent user from having full control over direct calls.
        if (!hint) {
            var syntheticException = void 0;
            try {
                throw new Error('Sentry syntheticException');
            }
            catch (exception) {
                syntheticException = exception;
            }
            finalHint = {
                originalException: exception,
                syntheticException: syntheticException,
            };
        }
        this._invokeClient('captureException', exception, tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, finalHint, { event_id: eventId }));
        return eventId;
    };
    /**
     * @inheritDoc
     */
    Hub.prototype.captureMessage = function (message, level, hint) {
        var eventId = (this._lastEventId = Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_1__["uuid4"])());
        var finalHint = hint;
        // If there's no explicit hint provided, mimick the same thing that would happen
        // in the minimal itself to create a consistent behavior.
        // We don't do this in the client, as it's the lowest level API, and doing this,
        // would prevent user from having full control over direct calls.
        if (!hint) {
            var syntheticException = void 0;
            try {
                throw new Error(message);
            }
            catch (exception) {
                syntheticException = exception;
            }
            finalHint = {
                originalException: message,
                syntheticException: syntheticException,
            };
        }
        this._invokeClient('captureMessage', message, level, tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, finalHint, { event_id: eventId }));
        return eventId;
    };
    /**
     * @inheritDoc
     */
    Hub.prototype.captureEvent = function (event, hint) {
        var eventId = (this._lastEventId = Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_1__["uuid4"])());
        this._invokeClient('captureEvent', event, tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, hint, { event_id: eventId }));
        return eventId;
    };
    /**
     * @inheritDoc
     */
    Hub.prototype.lastEventId = function () {
        return this._lastEventId;
    };
    /**
     * @inheritDoc
     */
    Hub.prototype.addBreadcrumb = function (breadcrumb, hint) {
        var top = this.getStackTop();
        if (!top.scope || !top.client) {
            return;
        }
        var _a = (top.client.getOptions && top.client.getOptions()) || {}, _b = _a.beforeBreadcrumb, beforeBreadcrumb = _b === void 0 ? null : _b, _c = _a.maxBreadcrumbs, maxBreadcrumbs = _c === void 0 ? DEFAULT_BREADCRUMBS : _c;
        if (maxBreadcrumbs <= 0) {
            return;
        }
        var timestamp = Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_1__["timestampWithMs"])();
        var mergedBreadcrumb = tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({ timestamp: timestamp }, breadcrumb);
        var finalBreadcrumb = beforeBreadcrumb
            ? Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_1__["consoleSandbox"])(function () { return beforeBreadcrumb(mergedBreadcrumb, hint); })
            : mergedBreadcrumb;
        if (finalBreadcrumb === null) {
            return;
        }
        top.scope.addBreadcrumb(finalBreadcrumb, Math.min(maxBreadcrumbs, MAX_BREADCRUMBS));
    };
    /**
     * @inheritDoc
     */
    Hub.prototype.setUser = function (user) {
        var top = this.getStackTop();
        if (!top.scope) {
            return;
        }
        top.scope.setUser(user);
    };
    /**
     * @inheritDoc
     */
    Hub.prototype.setTags = function (tags) {
        var top = this.getStackTop();
        if (!top.scope) {
            return;
        }
        top.scope.setTags(tags);
    };
    /**
     * @inheritDoc
     */
    Hub.prototype.setExtras = function (extras) {
        var top = this.getStackTop();
        if (!top.scope) {
            return;
        }
        top.scope.setExtras(extras);
    };
    /**
     * @inheritDoc
     */
    Hub.prototype.setTag = function (key, value) {
        var top = this.getStackTop();
        if (!top.scope) {
            return;
        }
        top.scope.setTag(key, value);
    };
    /**
     * @inheritDoc
     */
    Hub.prototype.setExtra = function (key, extra) {
        var top = this.getStackTop();
        if (!top.scope) {
            return;
        }
        top.scope.setExtra(key, extra);
    };
    /**
     * @inheritDoc
     */
    Hub.prototype.setContext = function (name, context) {
        var top = this.getStackTop();
        if (!top.scope) {
            return;
        }
        top.scope.setContext(name, context);
    };
    /**
     * @inheritDoc
     */
    Hub.prototype.configureScope = function (callback) {
        var top = this.getStackTop();
        if (top.scope && top.client) {
            callback(top.scope);
        }
    };
    /**
     * @inheritDoc
     */
    Hub.prototype.run = function (callback) {
        var oldHub = makeMain(this);
        try {
            callback(this);
        }
        finally {
            makeMain(oldHub);
        }
    };
    /**
     * @inheritDoc
     */
    Hub.prototype.getIntegration = function (integration) {
        var client = this.getClient();
        if (!client) {
            return null;
        }
        try {
            return client.getIntegration(integration);
        }
        catch (_oO) {
            _sentry_utils__WEBPACK_IMPORTED_MODULE_1__["logger"].warn("Cannot retrieve integration " + integration.id + " from the current Hub");
            return null;
        }
    };
    /**
     * @inheritDoc
     */
    Hub.prototype.startSpan = function (context) {
        return this._callExtensionMethod('startSpan', context);
    };
    /**
     * @inheritDoc
     */
    Hub.prototype.startTransaction = function (context) {
        return this._callExtensionMethod('startTransaction', context);
    };
    /**
     * @inheritDoc
     */
    Hub.prototype.traceHeaders = function () {
        return this._callExtensionMethod('traceHeaders');
    };
    /**
     * Calls global extension method and binding current instance to the function call
     */
    // @ts-ignore
    Hub.prototype._callExtensionMethod = function (method) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        var carrier = getMainCarrier();
        var sentry = carrier.__SENTRY__;
        // tslint:disable-next-line: strict-type-predicates
        if (sentry && sentry.extensions && typeof sentry.extensions[method] === 'function') {
            return sentry.extensions[method].apply(this, args);
        }
        _sentry_utils__WEBPACK_IMPORTED_MODULE_1__["logger"].warn("Extension method " + method + " couldn't be found, doing nothing.");
    };
    return Hub;
}());

/** Returns the global shim registry. */
function getMainCarrier() {
    var carrier = Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_1__["getGlobalObject"])();
    carrier.__SENTRY__ = carrier.__SENTRY__ || {
        extensions: {},
        hub: undefined,
    };
    return carrier;
}
/**
 * Replaces the current main hub with the passed one on the global object
 *
 * @returns The old replaced hub
 */
function makeMain(hub) {
    var registry = getMainCarrier();
    var oldHub = getHubFromCarrier(registry);
    setHubOnCarrier(registry, hub);
    return oldHub;
}
/**
 * Returns the default hub instance.
 *
 * If a hub is already registered in the global carrier but this module
 * contains a more recent version, it replaces the registered version.
 * Otherwise, the currently registered hub will be returned.
 */
function getCurrentHub() {
    // Get main carrier (global for every environment)
    var registry = getMainCarrier();
    // If there's no hub, or its an old API, assign a new one
    if (!hasHubOnCarrier(registry) || getHubFromCarrier(registry).isOlderThan(API_VERSION)) {
        setHubOnCarrier(registry, new Hub());
    }
    // Prefer domains over global if they are there (applicable only to Node environment)
    if (Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_1__["isNodeEnv"])()) {
        return getHubFromActiveDomain(registry);
    }
    // Return hub that lives on a global object
    return getHubFromCarrier(registry);
}
/**
 * Try to read the hub from an active domain, fallback to the registry if one doesnt exist
 * @returns discovered hub
 */
function getHubFromActiveDomain(registry) {
    try {
        var property = 'domain';
        var carrier = getMainCarrier();
        var sentry = carrier.__SENTRY__;
        // tslint:disable-next-line: strict-type-predicates
        if (!sentry || !sentry.extensions || !sentry.extensions[property]) {
            return getHubFromCarrier(registry);
        }
        var domain = sentry.extensions[property];
        var activeDomain = domain.active;
        // If there no active domain, just return global hub
        if (!activeDomain) {
            return getHubFromCarrier(registry);
        }
        // If there's no hub on current domain, or its an old API, assign a new one
        if (!hasHubOnCarrier(activeDomain) || getHubFromCarrier(activeDomain).isOlderThan(API_VERSION)) {
            var registryHubTopStack = getHubFromCarrier(registry).getStackTop();
            setHubOnCarrier(activeDomain, new Hub(registryHubTopStack.client, _scope__WEBPACK_IMPORTED_MODULE_2__["Scope"].clone(registryHubTopStack.scope)));
        }
        // Return hub that lives on a domain
        return getHubFromCarrier(activeDomain);
    }
    catch (_Oo) {
        // Return hub that lives on a global object
        return getHubFromCarrier(registry);
    }
}
/**
 * This will tell whether a carrier has a hub on it or not
 * @param carrier object
 */
function hasHubOnCarrier(carrier) {
    if (carrier && carrier.__SENTRY__ && carrier.__SENTRY__.hub) {
        return true;
    }
    return false;
}
/**
 * This will create a new {@link Hub} and add to the passed object on
 * __SENTRY__.hub.
 * @param carrier object
 * @hidden
 */
function getHubFromCarrier(carrier) {
    if (carrier && carrier.__SENTRY__ && carrier.__SENTRY__.hub) {
        return carrier.__SENTRY__.hub;
    }
    carrier.__SENTRY__ = carrier.__SENTRY__ || {};
    carrier.__SENTRY__.hub = new Hub();
    return carrier.__SENTRY__.hub;
}
/**
 * This will set passed {@link Hub} on the passed object's __SENTRY__.hub attribute
 * @param carrier object
 * @param hub Hub
 */
function setHubOnCarrier(carrier, hub) {
    if (!carrier) {
        return false;
    }
    carrier.__SENTRY__ = carrier.__SENTRY__ || {};
    carrier.__SENTRY__.hub = hub;
    return true;
}
//# sourceMappingURL=hub.js.map

/***/ }),

/***/ "./node_modules/@sentry/hub/esm/index.js":
/*!***********************************************!*\
  !*** ./node_modules/@sentry/hub/esm/index.js ***!
  \***********************************************/
/*! exports provided: addGlobalEventProcessor, Scope, getCurrentHub, getHubFromCarrier, getMainCarrier, Hub, makeMain, setHubOnCarrier */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _scope__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./scope */ "./node_modules/@sentry/hub/esm/scope.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "addGlobalEventProcessor", function() { return _scope__WEBPACK_IMPORTED_MODULE_0__["addGlobalEventProcessor"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Scope", function() { return _scope__WEBPACK_IMPORTED_MODULE_0__["Scope"]; });

/* harmony import */ var _hub__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./hub */ "./node_modules/@sentry/hub/esm/hub.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "getCurrentHub", function() { return _hub__WEBPACK_IMPORTED_MODULE_1__["getCurrentHub"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "getHubFromCarrier", function() { return _hub__WEBPACK_IMPORTED_MODULE_1__["getHubFromCarrier"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "getMainCarrier", function() { return _hub__WEBPACK_IMPORTED_MODULE_1__["getMainCarrier"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Hub", function() { return _hub__WEBPACK_IMPORTED_MODULE_1__["Hub"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "makeMain", function() { return _hub__WEBPACK_IMPORTED_MODULE_1__["makeMain"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "setHubOnCarrier", function() { return _hub__WEBPACK_IMPORTED_MODULE_1__["setHubOnCarrier"]; });



//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./node_modules/@sentry/hub/esm/scope.js":
/*!***********************************************!*\
  !*** ./node_modules/@sentry/hub/esm/scope.js ***!
  \***********************************************/
/*! exports provided: Scope, addGlobalEventProcessor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Scope", function() { return Scope; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "addGlobalEventProcessor", function() { return addGlobalEventProcessor; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _sentry_utils__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @sentry/utils */ "./node_modules/@sentry/utils/esm/index.js");


/**
 * Holds additional event information. {@link Scope.applyToEvent} will be
 * called by the client before an event will be sent.
 */
var Scope = /** @class */ (function () {
    function Scope() {
        /** Flag if notifiying is happening. */
        this._notifyingListeners = false;
        /** Callback for client to receive scope changes. */
        this._scopeListeners = [];
        /** Callback list that will be called after {@link applyToEvent}. */
        this._eventProcessors = [];
        /** Array of breadcrumbs. */
        this._breadcrumbs = [];
        /** User */
        this._user = {};
        /** Tags */
        this._tags = {};
        /** Extra */
        this._extra = {};
        /** Contexts */
        this._contexts = {};
    }
    /**
     * Add internal on change listener. Used for sub SDKs that need to store the scope.
     * @hidden
     */
    Scope.prototype.addScopeListener = function (callback) {
        this._scopeListeners.push(callback);
    };
    /**
     * @inheritDoc
     */
    Scope.prototype.addEventProcessor = function (callback) {
        this._eventProcessors.push(callback);
        return this;
    };
    /**
     * This will be called on every set call.
     */
    Scope.prototype._notifyScopeListeners = function () {
        var _this = this;
        if (!this._notifyingListeners) {
            this._notifyingListeners = true;
            setTimeout(function () {
                _this._scopeListeners.forEach(function (callback) {
                    callback(_this);
                });
                _this._notifyingListeners = false;
            });
        }
    };
    /**
     * This will be called after {@link applyToEvent} is finished.
     */
    Scope.prototype._notifyEventProcessors = function (processors, event, hint, index) {
        var _this = this;
        if (index === void 0) { index = 0; }
        return new _sentry_utils__WEBPACK_IMPORTED_MODULE_1__["SyncPromise"](function (resolve, reject) {
            var processor = processors[index];
            // tslint:disable-next-line:strict-type-predicates
            if (event === null || typeof processor !== 'function') {
                resolve(event);
            }
            else {
                var result = processor(tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, event), hint);
                if (Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_1__["isThenable"])(result)) {
                    result
                        .then(function (final) { return _this._notifyEventProcessors(processors, final, hint, index + 1).then(resolve); })
                        .then(null, reject);
                }
                else {
                    _this._notifyEventProcessors(processors, result, hint, index + 1)
                        .then(resolve)
                        .then(null, reject);
                }
            }
        });
    };
    /**
     * @inheritDoc
     */
    Scope.prototype.setUser = function (user) {
        this._user = user || {};
        this._notifyScopeListeners();
        return this;
    };
    /**
     * @inheritDoc
     */
    Scope.prototype.setTags = function (tags) {
        this._tags = tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, this._tags, tags);
        this._notifyScopeListeners();
        return this;
    };
    /**
     * @inheritDoc
     */
    Scope.prototype.setTag = function (key, value) {
        var _a;
        this._tags = tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, this._tags, (_a = {}, _a[key] = value, _a));
        this._notifyScopeListeners();
        return this;
    };
    /**
     * @inheritDoc
     */
    Scope.prototype.setExtras = function (extras) {
        this._extra = tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, this._extra, extras);
        this._notifyScopeListeners();
        return this;
    };
    /**
     * @inheritDoc
     */
    Scope.prototype.setExtra = function (key, extra) {
        var _a;
        this._extra = tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, this._extra, (_a = {}, _a[key] = extra, _a));
        this._notifyScopeListeners();
        return this;
    };
    /**
     * @inheritDoc
     */
    Scope.prototype.setFingerprint = function (fingerprint) {
        this._fingerprint = fingerprint;
        this._notifyScopeListeners();
        return this;
    };
    /**
     * @inheritDoc
     */
    Scope.prototype.setLevel = function (level) {
        this._level = level;
        this._notifyScopeListeners();
        return this;
    };
    /**
     * @inheritDoc
     */
    Scope.prototype.setTransactionName = function (name) {
        this._transactionName = name;
        this._notifyScopeListeners();
        return this;
    };
    /**
     * Can be removed in major version.
     * @deprecated in favor of {@link this.setTransactionName}
     */
    Scope.prototype.setTransaction = function (name) {
        return this.setTransactionName(name);
    };
    /**
     * @inheritDoc
     */
    Scope.prototype.setContext = function (key, context) {
        var _a;
        this._contexts = tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, this._contexts, (_a = {}, _a[key] = context, _a));
        this._notifyScopeListeners();
        return this;
    };
    /**
     * @inheritDoc
     */
    Scope.prototype.setSpan = function (span) {
        this._span = span;
        this._notifyScopeListeners();
        return this;
    };
    /**
     * @inheritDoc
     */
    Scope.prototype.getSpan = function () {
        return this._span;
    };
    /**
     * @inheritDoc
     */
    Scope.prototype.getTransaction = function () {
        var span = this.getSpan();
        if (span && span.spanRecorder && span.spanRecorder.spans[0]) {
            return span.spanRecorder.spans[0];
        }
        return undefined;
    };
    /**
     * Inherit values from the parent scope.
     * @param scope to clone.
     */
    Scope.clone = function (scope) {
        var newScope = new Scope();
        if (scope) {
            newScope._breadcrumbs = tslib__WEBPACK_IMPORTED_MODULE_0__["__spread"](scope._breadcrumbs);
            newScope._tags = tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, scope._tags);
            newScope._extra = tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, scope._extra);
            newScope._contexts = tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, scope._contexts);
            newScope._user = scope._user;
            newScope._level = scope._level;
            newScope._span = scope._span;
            newScope._transactionName = scope._transactionName;
            newScope._fingerprint = scope._fingerprint;
            newScope._eventProcessors = tslib__WEBPACK_IMPORTED_MODULE_0__["__spread"](scope._eventProcessors);
        }
        return newScope;
    };
    /**
     * @inheritDoc
     */
    Scope.prototype.update = function (captureContext) {
        if (!captureContext) {
            return this;
        }
        if (typeof captureContext === 'function') {
            var updatedScope = captureContext(this);
            return updatedScope instanceof Scope ? updatedScope : this;
        }
        if (captureContext instanceof Scope) {
            this._tags = tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, this._tags, captureContext._tags);
            this._extra = tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, this._extra, captureContext._extra);
            this._contexts = tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, this._contexts, captureContext._contexts);
            if (captureContext._user) {
                this._user = captureContext._user;
            }
            if (captureContext._level) {
                this._level = captureContext._level;
            }
            if (captureContext._fingerprint) {
                this._fingerprint = captureContext._fingerprint;
            }
        }
        else if (Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_1__["isPlainObject"])(captureContext)) {
            // tslint:disable-next-line:no-parameter-reassignment
            captureContext = captureContext;
            this._tags = tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, this._tags, captureContext.tags);
            this._extra = tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, this._extra, captureContext.extra);
            this._contexts = tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, this._contexts, captureContext.contexts);
            if (captureContext.user) {
                this._user = captureContext.user;
            }
            if (captureContext.level) {
                this._level = captureContext.level;
            }
            if (captureContext.fingerprint) {
                this._fingerprint = captureContext.fingerprint;
            }
        }
        return this;
    };
    /**
     * @inheritDoc
     */
    Scope.prototype.clear = function () {
        this._breadcrumbs = [];
        this._tags = {};
        this._extra = {};
        this._user = {};
        this._contexts = {};
        this._level = undefined;
        this._transactionName = undefined;
        this._fingerprint = undefined;
        this._span = undefined;
        this._notifyScopeListeners();
        return this;
    };
    /**
     * @inheritDoc
     */
    Scope.prototype.addBreadcrumb = function (breadcrumb, maxBreadcrumbs) {
        var mergedBreadcrumb = tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({ timestamp: Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_1__["timestampWithMs"])() }, breadcrumb);
        this._breadcrumbs =
            maxBreadcrumbs !== undefined && maxBreadcrumbs >= 0
                ? tslib__WEBPACK_IMPORTED_MODULE_0__["__spread"](this._breadcrumbs, [mergedBreadcrumb]).slice(-maxBreadcrumbs)
                : tslib__WEBPACK_IMPORTED_MODULE_0__["__spread"](this._breadcrumbs, [mergedBreadcrumb]);
        this._notifyScopeListeners();
        return this;
    };
    /**
     * @inheritDoc
     */
    Scope.prototype.clearBreadcrumbs = function () {
        this._breadcrumbs = [];
        this._notifyScopeListeners();
        return this;
    };
    /**
     * Applies fingerprint from the scope to the event if there's one,
     * uses message if there's one instead or get rid of empty fingerprint
     */
    Scope.prototype._applyFingerprint = function (event) {
        // Make sure it's an array first and we actually have something in place
        event.fingerprint = event.fingerprint
            ? Array.isArray(event.fingerprint)
                ? event.fingerprint
                : [event.fingerprint]
            : [];
        // If we have something on the scope, then merge it with event
        if (this._fingerprint) {
            event.fingerprint = event.fingerprint.concat(this._fingerprint);
        }
        // If we have no data at all, remove empty array default
        if (event.fingerprint && !event.fingerprint.length) {
            delete event.fingerprint;
        }
    };
    /**
     * Applies the current context and fingerprint to the event.
     * Note that breadcrumbs will be added by the client.
     * Also if the event has already breadcrumbs on it, we do not merge them.
     * @param event Event
     * @param hint May contain additional informartion about the original exception.
     * @hidden
     */
    Scope.prototype.applyToEvent = function (event, hint) {
        if (this._extra && Object.keys(this._extra).length) {
            event.extra = tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, this._extra, event.extra);
        }
        if (this._tags && Object.keys(this._tags).length) {
            event.tags = tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, this._tags, event.tags);
        }
        if (this._user && Object.keys(this._user).length) {
            event.user = tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, this._user, event.user);
        }
        if (this._contexts && Object.keys(this._contexts).length) {
            event.contexts = tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, this._contexts, event.contexts);
        }
        if (this._level) {
            event.level = this._level;
        }
        if (this._transactionName) {
            event.transaction = this._transactionName;
        }
        // We want to set the trace context for normal events only if there isn't already
        // a trace context on the event. There is a product feature in place where we link
        // errors with transaction and it relys on that.
        if (this._span) {
            event.contexts = tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({ trace: this._span.getTraceContext() }, event.contexts);
        }
        this._applyFingerprint(event);
        event.breadcrumbs = tslib__WEBPACK_IMPORTED_MODULE_0__["__spread"]((event.breadcrumbs || []), this._breadcrumbs);
        event.breadcrumbs = event.breadcrumbs.length > 0 ? event.breadcrumbs : undefined;
        return this._notifyEventProcessors(tslib__WEBPACK_IMPORTED_MODULE_0__["__spread"](getGlobalEventProcessors(), this._eventProcessors), event, hint);
    };
    return Scope;
}());

/**
 * Retruns the global event processors.
 */
function getGlobalEventProcessors() {
    var global = Object(_sentry_utils__WEBPACK_IMPORTED_MODULE_1__["getGlobalObject"])();
    global.__SENTRY__ = global.__SENTRY__ || {};
    global.__SENTRY__.globalEventProcessors = global.__SENTRY__.globalEventProcessors || [];
    return global.__SENTRY__.globalEventProcessors;
}
/**
 * Add a EventProcessor to be kept globally.
 * @param callback EventProcessor to add
 */
function addGlobalEventProcessor(callback) {
    getGlobalEventProcessors().push(callback);
}
//# sourceMappingURL=scope.js.map

/***/ }),

/***/ "./node_modules/@sentry/minimal/esm/index.js":
/*!***************************************************!*\
  !*** ./node_modules/@sentry/minimal/esm/index.js ***!
  \***************************************************/
/*! exports provided: captureException, captureMessage, captureEvent, configureScope, addBreadcrumb, setContext, setExtras, setTags, setExtra, setTag, setUser, withScope, _callOnClient, startTransaction */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "captureException", function() { return captureException; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "captureMessage", function() { return captureMessage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "captureEvent", function() { return captureEvent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "configureScope", function() { return configureScope; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "addBreadcrumb", function() { return addBreadcrumb; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setContext", function() { return setContext; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setExtras", function() { return setExtras; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setTags", function() { return setTags; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setExtra", function() { return setExtra; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setTag", function() { return setTag; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setUser", function() { return setUser; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "withScope", function() { return withScope; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "_callOnClient", function() { return _callOnClient; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "startTransaction", function() { return startTransaction; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _sentry_hub__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @sentry/hub */ "./node_modules/@sentry/hub/esm/index.js");


/**
 * This calls a function on the current hub.
 * @param method function to call on hub.
 * @param args to pass to function.
 */
function callOnHub(method) {
    var args = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        args[_i - 1] = arguments[_i];
    }
    var hub = Object(_sentry_hub__WEBPACK_IMPORTED_MODULE_1__["getCurrentHub"])();
    if (hub && hub[method]) {
        // tslint:disable-next-line:no-unsafe-any
        return hub[method].apply(hub, tslib__WEBPACK_IMPORTED_MODULE_0__["__spread"](args));
    }
    throw new Error("No hub defined or " + method + " was not found on the hub, please open a bug report.");
}
/**
 * Captures an exception event and sends it to Sentry.
 *
 * @param exception An exception-like object.
 * @returns The generated eventId.
 */
function captureException(exception, captureContext) {
    var syntheticException;
    try {
        throw new Error('Sentry syntheticException');
    }
    catch (exception) {
        syntheticException = exception;
    }
    return callOnHub('captureException', exception, {
        captureContext: captureContext,
        originalException: exception,
        syntheticException: syntheticException,
    });
}
/**
 * Captures a message event and sends it to Sentry.
 *
 * @param message The message to send to Sentry.
 * @param level Define the level of the message.
 * @returns The generated eventId.
 */
function captureMessage(message, captureContext) {
    var syntheticException;
    try {
        throw new Error(message);
    }
    catch (exception) {
        syntheticException = exception;
    }
    // This is necessary to provide explicit scopes upgrade, without changing the original
    // arrity of the `captureMessage(message, level)` method.
    var level = typeof captureContext === 'string' ? captureContext : undefined;
    var context = typeof captureContext !== 'string' ? { captureContext: captureContext } : undefined;
    return callOnHub('captureMessage', message, level, tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({ originalException: message, syntheticException: syntheticException }, context));
}
/**
 * Captures a manually created event and sends it to Sentry.
 *
 * @param event The event to send to Sentry.
 * @returns The generated eventId.
 */
function captureEvent(event) {
    return callOnHub('captureEvent', event);
}
/**
 * Callback to set context information onto the scope.
 * @param callback Callback function that receives Scope.
 */
function configureScope(callback) {
    callOnHub('configureScope', callback);
}
/**
 * Records a new breadcrumb which will be attached to future events.
 *
 * Breadcrumbs will be added to subsequent events to provide more context on
 * user's actions prior to an error or crash.
 *
 * @param breadcrumb The breadcrumb to record.
 */
function addBreadcrumb(breadcrumb) {
    callOnHub('addBreadcrumb', breadcrumb);
}
/**
 * Sets context data with the given name.
 * @param name of the context
 * @param context Any kind of data. This data will be normalized.
 */
function setContext(name, context) {
    callOnHub('setContext', name, context);
}
/**
 * Set an object that will be merged sent as extra data with the event.
 * @param extras Extras object to merge into current context.
 */
function setExtras(extras) {
    callOnHub('setExtras', extras);
}
/**
 * Set an object that will be merged sent as tags data with the event.
 * @param tags Tags context object to merge into current context.
 */
function setTags(tags) {
    callOnHub('setTags', tags);
}
/**
 * Set key:value that will be sent as extra data with the event.
 * @param key String of extra
 * @param extra Any kind of data. This data will be normalized.
 */
function setExtra(key, extra) {
    callOnHub('setExtra', key, extra);
}
/**
 * Set key:value that will be sent as tags data with the event.
 * @param key String key of tag
 * @param value String value of tag
 */
function setTag(key, value) {
    callOnHub('setTag', key, value);
}
/**
 * Updates user context information for future events.
 *
 * @param user User context object to be set in the current context. Pass `null` to unset the user.
 */
function setUser(user) {
    callOnHub('setUser', user);
}
/**
 * Creates a new scope with and executes the given operation within.
 * The scope is automatically removed once the operation
 * finishes or throws.
 *
 * This is essentially a convenience function for:
 *
 *     pushScope();
 *     callback();
 *     popScope();
 *
 * @param callback that will be enclosed into push/popScope.
 */
function withScope(callback) {
    callOnHub('withScope', callback);
}
/**
 * Calls a function on the latest client. Use this with caution, it's meant as
 * in "internal" helper so we don't need to expose every possible function in
 * the shim. It is not guaranteed that the client actually implements the
 * function.
 *
 * @param method The method to call on the client/client.
 * @param args Arguments to pass to the client/fontend.
 * @hidden
 */
function _callOnClient(method) {
    var args = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        args[_i - 1] = arguments[_i];
    }
    callOnHub.apply(void 0, tslib__WEBPACK_IMPORTED_MODULE_0__["__spread"](['_invokeClient', method], args));
}
/**
 * Starts a new `Transaction` and returns it. This is the entry point to manual
 * tracing instrumentation.
 *
 * A tree structure can be built by adding child spans to the transaction, and
 * child spans to other spans. To start a new child span within the transaction
 * or any span, call the respective `.startChild()` method.
 *
 * Every child span must be finished before the transaction is finished,
 * otherwise the unfinished spans are discarded.
 *
 * The transaction must be finished with a call to its `.finish()` method, at
 * which point the transaction with all its finished child spans will be sent to
 * Sentry.
 *
 * @param context Properties of the new `Transaction`.
 */
function startTransaction(context) {
    return callOnHub('startTransaction', tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, context));
}
//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./node_modules/@sentry/types/esm/index.js":
/*!*************************************************!*\
  !*** ./node_modules/@sentry/types/esm/index.js ***!
  \*************************************************/
/*! exports provided: LogLevel, Severity, Status */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _loglevel__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./loglevel */ "./node_modules/@sentry/types/esm/loglevel.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "LogLevel", function() { return _loglevel__WEBPACK_IMPORTED_MODULE_0__["LogLevel"]; });

/* harmony import */ var _severity__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./severity */ "./node_modules/@sentry/types/esm/severity.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Severity", function() { return _severity__WEBPACK_IMPORTED_MODULE_1__["Severity"]; });

/* harmony import */ var _status__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./status */ "./node_modules/@sentry/types/esm/status.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Status", function() { return _status__WEBPACK_IMPORTED_MODULE_2__["Status"]; });




//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./node_modules/@sentry/types/esm/loglevel.js":
/*!****************************************************!*\
  !*** ./node_modules/@sentry/types/esm/loglevel.js ***!
  \****************************************************/
/*! exports provided: LogLevel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LogLevel", function() { return LogLevel; });
/** Console logging verbosity for the SDK. */
var LogLevel;
(function (LogLevel) {
    /** No logs will be generated. */
    LogLevel[LogLevel["None"] = 0] = "None";
    /** Only SDK internal errors will be logged. */
    LogLevel[LogLevel["Error"] = 1] = "Error";
    /** Information useful for debugging the SDK will be logged. */
    LogLevel[LogLevel["Debug"] = 2] = "Debug";
    /** All SDK actions will be logged. */
    LogLevel[LogLevel["Verbose"] = 3] = "Verbose";
})(LogLevel || (LogLevel = {}));
//# sourceMappingURL=loglevel.js.map

/***/ }),

/***/ "./node_modules/@sentry/types/esm/severity.js":
/*!****************************************************!*\
  !*** ./node_modules/@sentry/types/esm/severity.js ***!
  \****************************************************/
/*! exports provided: Severity */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Severity", function() { return Severity; });
/** JSDoc */
var Severity;
(function (Severity) {
    /** JSDoc */
    Severity["Fatal"] = "fatal";
    /** JSDoc */
    Severity["Error"] = "error";
    /** JSDoc */
    Severity["Warning"] = "warning";
    /** JSDoc */
    Severity["Log"] = "log";
    /** JSDoc */
    Severity["Info"] = "info";
    /** JSDoc */
    Severity["Debug"] = "debug";
    /** JSDoc */
    Severity["Critical"] = "critical";
})(Severity || (Severity = {}));
// tslint:disable:completed-docs
// tslint:disable:no-unnecessary-qualifier no-namespace
(function (Severity) {
    /**
     * Converts a string-based level into a {@link Severity}.
     *
     * @param level string representation of Severity
     * @returns Severity
     */
    function fromString(level) {
        switch (level) {
            case 'debug':
                return Severity.Debug;
            case 'info':
                return Severity.Info;
            case 'warn':
            case 'warning':
                return Severity.Warning;
            case 'error':
                return Severity.Error;
            case 'fatal':
                return Severity.Fatal;
            case 'critical':
                return Severity.Critical;
            case 'log':
            default:
                return Severity.Log;
        }
    }
    Severity.fromString = fromString;
})(Severity || (Severity = {}));
//# sourceMappingURL=severity.js.map

/***/ }),

/***/ "./node_modules/@sentry/types/esm/status.js":
/*!**************************************************!*\
  !*** ./node_modules/@sentry/types/esm/status.js ***!
  \**************************************************/
/*! exports provided: Status */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Status", function() { return Status; });
/** The status of an event. */
var Status;
(function (Status) {
    /** The status could not be determined. */
    Status["Unknown"] = "unknown";
    /** The event was skipped due to configuration or callbacks. */
    Status["Skipped"] = "skipped";
    /** The event was sent to Sentry successfully. */
    Status["Success"] = "success";
    /** The client is currently rate limited and will try again later. */
    Status["RateLimit"] = "rate_limit";
    /** The event could not be processed. */
    Status["Invalid"] = "invalid";
    /** A server-side error ocurred during submission. */
    Status["Failed"] = "failed";
})(Status || (Status = {}));
// tslint:disable:completed-docs
// tslint:disable:no-unnecessary-qualifier no-namespace
(function (Status) {
    /**
     * Converts a HTTP status code into a {@link Status}.
     *
     * @param code The HTTP response status code.
     * @returns The send status or {@link Status.Unknown}.
     */
    function fromHttpCode(code) {
        if (code >= 200 && code < 300) {
            return Status.Success;
        }
        if (code === 429) {
            return Status.RateLimit;
        }
        if (code >= 400 && code < 500) {
            return Status.Invalid;
        }
        if (code >= 500) {
            return Status.Failed;
        }
        return Status.Unknown;
    }
    Status.fromHttpCode = fromHttpCode;
})(Status || (Status = {}));
//# sourceMappingURL=status.js.map

/***/ }),

/***/ "./node_modules/@sentry/utils/esm/async.js":
/*!*************************************************!*\
  !*** ./node_modules/@sentry/utils/esm/async.js ***!
  \*************************************************/
/*! exports provided: forget */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "forget", function() { return forget; });
/**
 * Consumes the promise and logs the error when it rejects.
 * @param promise A promise to forget.
 */
function forget(promise) {
    promise.then(null, function (e) {
        // TODO: Use a better logging mechanism
        console.error(e);
    });
}
//# sourceMappingURL=async.js.map

/***/ }),

/***/ "./node_modules/@sentry/utils/esm/dsn.js":
/*!***********************************************!*\
  !*** ./node_modules/@sentry/utils/esm/dsn.js ***!
  \***********************************************/
/*! exports provided: Dsn */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Dsn", function() { return Dsn; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _error__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./error */ "./node_modules/@sentry/utils/esm/error.js");


/** Regular expression used to parse a Dsn. */
var DSN_REGEX = /^(?:(\w+):)\/\/(?:(\w+)(?::(\w+))?@)([\w\.-]+)(?::(\d+))?\/(.+)/;
/** Error message */
var ERROR_MESSAGE = 'Invalid Dsn';
/** The Sentry Dsn, identifying a Sentry instance and project. */
var Dsn = /** @class */ (function () {
    /** Creates a new Dsn component */
    function Dsn(from) {
        if (typeof from === 'string') {
            this._fromString(from);
        }
        else {
            this._fromComponents(from);
        }
        this._validate();
    }
    /**
     * Renders the string representation of this Dsn.
     *
     * By default, this will render the public representation without the password
     * component. To get the deprecated private representation, set `withPassword`
     * to true.
     *
     * @param withPassword When set to true, the password will be included.
     */
    Dsn.prototype.toString = function (withPassword) {
        if (withPassword === void 0) { withPassword = false; }
        // tslint:disable-next-line:no-this-assignment
        var _a = this, host = _a.host, path = _a.path, pass = _a.pass, port = _a.port, projectId = _a.projectId, protocol = _a.protocol, user = _a.user;
        return (protocol + "://" + user + (withPassword && pass ? ":" + pass : '') +
            ("@" + host + (port ? ":" + port : '') + "/" + (path ? path + "/" : path) + projectId));
    };
    /** Parses a string into this Dsn. */
    Dsn.prototype._fromString = function (str) {
        var match = DSN_REGEX.exec(str);
        if (!match) {
            throw new _error__WEBPACK_IMPORTED_MODULE_1__["SentryError"](ERROR_MESSAGE);
        }
        var _a = tslib__WEBPACK_IMPORTED_MODULE_0__["__read"](match.slice(1), 6), protocol = _a[0], user = _a[1], _b = _a[2], pass = _b === void 0 ? '' : _b, host = _a[3], _c = _a[4], port = _c === void 0 ? '' : _c, lastPath = _a[5];
        var path = '';
        var projectId = lastPath;
        var split = projectId.split('/');
        if (split.length > 1) {
            path = split.slice(0, -1).join('/');
            projectId = split.pop();
        }
        if (projectId) {
            var projectMatch = projectId.match(/^\d+/);
            if (projectMatch) {
                projectId = projectMatch[0];
            }
        }
        this._fromComponents({ host: host, pass: pass, path: path, projectId: projectId, port: port, protocol: protocol, user: user });
    };
    /** Maps Dsn components into this instance. */
    Dsn.prototype._fromComponents = function (components) {
        this.protocol = components.protocol;
        this.user = components.user;
        this.pass = components.pass || '';
        this.host = components.host;
        this.port = components.port || '';
        this.path = components.path || '';
        this.projectId = components.projectId;
    };
    /** Validates this Dsn and throws on error. */
    Dsn.prototype._validate = function () {
        var _this = this;
        ['protocol', 'user', 'host', 'projectId'].forEach(function (component) {
            if (!_this[component]) {
                throw new _error__WEBPACK_IMPORTED_MODULE_1__["SentryError"](ERROR_MESSAGE + ": " + component + " missing");
            }
        });
        if (!this.projectId.match(/^\d+$/)) {
            throw new _error__WEBPACK_IMPORTED_MODULE_1__["SentryError"](ERROR_MESSAGE + ": Invalid projectId " + this.projectId);
        }
        if (this.protocol !== 'http' && this.protocol !== 'https') {
            throw new _error__WEBPACK_IMPORTED_MODULE_1__["SentryError"](ERROR_MESSAGE + ": Invalid protocol " + this.protocol);
        }
        if (this.port && isNaN(parseInt(this.port, 10))) {
            throw new _error__WEBPACK_IMPORTED_MODULE_1__["SentryError"](ERROR_MESSAGE + ": Invalid port " + this.port);
        }
    };
    return Dsn;
}());

//# sourceMappingURL=dsn.js.map

/***/ }),

/***/ "./node_modules/@sentry/utils/esm/error.js":
/*!*************************************************!*\
  !*** ./node_modules/@sentry/utils/esm/error.js ***!
  \*************************************************/
/*! exports provided: SentryError */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SentryError", function() { return SentryError; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _polyfill__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./polyfill */ "./node_modules/@sentry/utils/esm/polyfill.js");


/** An error emitted by Sentry SDKs and related utilities. */
var SentryError = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](SentryError, _super);
    function SentryError(message) {
        var _newTarget = this.constructor;
        var _this = _super.call(this, message) || this;
        _this.message = message;
        // tslint:disable:no-unsafe-any
        _this.name = _newTarget.prototype.constructor.name;
        Object(_polyfill__WEBPACK_IMPORTED_MODULE_1__["setPrototypeOf"])(_this, _newTarget.prototype);
        return _this;
    }
    return SentryError;
}(Error));

//# sourceMappingURL=error.js.map

/***/ }),

/***/ "./node_modules/@sentry/utils/esm/index.js":
/*!*************************************************!*\
  !*** ./node_modules/@sentry/utils/esm/index.js ***!
  \*************************************************/
/*! exports provided: forget, SentryError, isError, isErrorEvent, isDOMError, isDOMException, isString, isPrimitive, isPlainObject, isEvent, isElement, isRegExp, isThenable, isSyntheticEvent, isInstanceOf, logger, Memo, dynamicRequire, isNodeEnv, getGlobalObject, uuid4, parseUrl, getEventDescription, consoleSandbox, addExceptionTypeValue, addExceptionMechanism, getLocationHref, htmlTreeAsString, crossPlatformPerformance, timestampWithMs, parseSemver, parseRetryAfterHeader, getFunctionName, addContextToFrame, fill, urlEncode, normalizeToSize, walk, normalize, extractExceptionKeysForMessage, dropUndefinedKeys, resolve, relative, normalizePath, isAbsolute, join, dirname, basename, PromiseBuffer, truncate, snipLine, safeJoin, isMatchingPattern, supportsErrorEvent, supportsDOMError, supportsDOMException, supportsFetch, supportsNativeFetch, supportsReportingObserver, supportsReferrerPolicy, supportsHistory, SyncPromise, addInstrumentationHandler, Dsn */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _async__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./async */ "./node_modules/@sentry/utils/esm/async.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "forget", function() { return _async__WEBPACK_IMPORTED_MODULE_0__["forget"]; });

/* harmony import */ var _error__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./error */ "./node_modules/@sentry/utils/esm/error.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "SentryError", function() { return _error__WEBPACK_IMPORTED_MODULE_1__["SentryError"]; });

/* harmony import */ var _is__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./is */ "./node_modules/@sentry/utils/esm/is.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "isError", function() { return _is__WEBPACK_IMPORTED_MODULE_2__["isError"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "isErrorEvent", function() { return _is__WEBPACK_IMPORTED_MODULE_2__["isErrorEvent"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "isDOMError", function() { return _is__WEBPACK_IMPORTED_MODULE_2__["isDOMError"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "isDOMException", function() { return _is__WEBPACK_IMPORTED_MODULE_2__["isDOMException"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "isString", function() { return _is__WEBPACK_IMPORTED_MODULE_2__["isString"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "isPrimitive", function() { return _is__WEBPACK_IMPORTED_MODULE_2__["isPrimitive"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "isPlainObject", function() { return _is__WEBPACK_IMPORTED_MODULE_2__["isPlainObject"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "isEvent", function() { return _is__WEBPACK_IMPORTED_MODULE_2__["isEvent"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "isElement", function() { return _is__WEBPACK_IMPORTED_MODULE_2__["isElement"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "isRegExp", function() { return _is__WEBPACK_IMPORTED_MODULE_2__["isRegExp"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "isThenable", function() { return _is__WEBPACK_IMPORTED_MODULE_2__["isThenable"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "isSyntheticEvent", function() { return _is__WEBPACK_IMPORTED_MODULE_2__["isSyntheticEvent"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "isInstanceOf", function() { return _is__WEBPACK_IMPORTED_MODULE_2__["isInstanceOf"]; });

/* harmony import */ var _logger__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./logger */ "./node_modules/@sentry/utils/esm/logger.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "logger", function() { return _logger__WEBPACK_IMPORTED_MODULE_3__["logger"]; });

/* harmony import */ var _memo__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./memo */ "./node_modules/@sentry/utils/esm/memo.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Memo", function() { return _memo__WEBPACK_IMPORTED_MODULE_4__["Memo"]; });

/* harmony import */ var _misc__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./misc */ "./node_modules/@sentry/utils/esm/misc.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "dynamicRequire", function() { return _misc__WEBPACK_IMPORTED_MODULE_5__["dynamicRequire"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "isNodeEnv", function() { return _misc__WEBPACK_IMPORTED_MODULE_5__["isNodeEnv"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "getGlobalObject", function() { return _misc__WEBPACK_IMPORTED_MODULE_5__["getGlobalObject"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "uuid4", function() { return _misc__WEBPACK_IMPORTED_MODULE_5__["uuid4"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "parseUrl", function() { return _misc__WEBPACK_IMPORTED_MODULE_5__["parseUrl"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "getEventDescription", function() { return _misc__WEBPACK_IMPORTED_MODULE_5__["getEventDescription"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "consoleSandbox", function() { return _misc__WEBPACK_IMPORTED_MODULE_5__["consoleSandbox"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "addExceptionTypeValue", function() { return _misc__WEBPACK_IMPORTED_MODULE_5__["addExceptionTypeValue"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "addExceptionMechanism", function() { return _misc__WEBPACK_IMPORTED_MODULE_5__["addExceptionMechanism"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "getLocationHref", function() { return _misc__WEBPACK_IMPORTED_MODULE_5__["getLocationHref"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "htmlTreeAsString", function() { return _misc__WEBPACK_IMPORTED_MODULE_5__["htmlTreeAsString"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "crossPlatformPerformance", function() { return _misc__WEBPACK_IMPORTED_MODULE_5__["crossPlatformPerformance"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "timestampWithMs", function() { return _misc__WEBPACK_IMPORTED_MODULE_5__["timestampWithMs"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "parseSemver", function() { return _misc__WEBPACK_IMPORTED_MODULE_5__["parseSemver"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "parseRetryAfterHeader", function() { return _misc__WEBPACK_IMPORTED_MODULE_5__["parseRetryAfterHeader"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "getFunctionName", function() { return _misc__WEBPACK_IMPORTED_MODULE_5__["getFunctionName"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "addContextToFrame", function() { return _misc__WEBPACK_IMPORTED_MODULE_5__["addContextToFrame"]; });

/* harmony import */ var _object__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./object */ "./node_modules/@sentry/utils/esm/object.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "fill", function() { return _object__WEBPACK_IMPORTED_MODULE_6__["fill"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "urlEncode", function() { return _object__WEBPACK_IMPORTED_MODULE_6__["urlEncode"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "normalizeToSize", function() { return _object__WEBPACK_IMPORTED_MODULE_6__["normalizeToSize"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "walk", function() { return _object__WEBPACK_IMPORTED_MODULE_6__["walk"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "normalize", function() { return _object__WEBPACK_IMPORTED_MODULE_6__["normalize"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "extractExceptionKeysForMessage", function() { return _object__WEBPACK_IMPORTED_MODULE_6__["extractExceptionKeysForMessage"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "dropUndefinedKeys", function() { return _object__WEBPACK_IMPORTED_MODULE_6__["dropUndefinedKeys"]; });

/* harmony import */ var _path__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./path */ "./node_modules/@sentry/utils/esm/path.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "resolve", function() { return _path__WEBPACK_IMPORTED_MODULE_7__["resolve"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "relative", function() { return _path__WEBPACK_IMPORTED_MODULE_7__["relative"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "normalizePath", function() { return _path__WEBPACK_IMPORTED_MODULE_7__["normalizePath"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "isAbsolute", function() { return _path__WEBPACK_IMPORTED_MODULE_7__["isAbsolute"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "join", function() { return _path__WEBPACK_IMPORTED_MODULE_7__["join"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "dirname", function() { return _path__WEBPACK_IMPORTED_MODULE_7__["dirname"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "basename", function() { return _path__WEBPACK_IMPORTED_MODULE_7__["basename"]; });

/* harmony import */ var _promisebuffer__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./promisebuffer */ "./node_modules/@sentry/utils/esm/promisebuffer.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "PromiseBuffer", function() { return _promisebuffer__WEBPACK_IMPORTED_MODULE_8__["PromiseBuffer"]; });

/* harmony import */ var _string__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./string */ "./node_modules/@sentry/utils/esm/string.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "truncate", function() { return _string__WEBPACK_IMPORTED_MODULE_9__["truncate"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "snipLine", function() { return _string__WEBPACK_IMPORTED_MODULE_9__["snipLine"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "safeJoin", function() { return _string__WEBPACK_IMPORTED_MODULE_9__["safeJoin"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "isMatchingPattern", function() { return _string__WEBPACK_IMPORTED_MODULE_9__["isMatchingPattern"]; });

/* harmony import */ var _supports__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./supports */ "./node_modules/@sentry/utils/esm/supports.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "supportsErrorEvent", function() { return _supports__WEBPACK_IMPORTED_MODULE_10__["supportsErrorEvent"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "supportsDOMError", function() { return _supports__WEBPACK_IMPORTED_MODULE_10__["supportsDOMError"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "supportsDOMException", function() { return _supports__WEBPACK_IMPORTED_MODULE_10__["supportsDOMException"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "supportsFetch", function() { return _supports__WEBPACK_IMPORTED_MODULE_10__["supportsFetch"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "supportsNativeFetch", function() { return _supports__WEBPACK_IMPORTED_MODULE_10__["supportsNativeFetch"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "supportsReportingObserver", function() { return _supports__WEBPACK_IMPORTED_MODULE_10__["supportsReportingObserver"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "supportsReferrerPolicy", function() { return _supports__WEBPACK_IMPORTED_MODULE_10__["supportsReferrerPolicy"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "supportsHistory", function() { return _supports__WEBPACK_IMPORTED_MODULE_10__["supportsHistory"]; });

/* harmony import */ var _syncpromise__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./syncpromise */ "./node_modules/@sentry/utils/esm/syncpromise.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "SyncPromise", function() { return _syncpromise__WEBPACK_IMPORTED_MODULE_11__["SyncPromise"]; });

/* harmony import */ var _instrument__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./instrument */ "./node_modules/@sentry/utils/esm/instrument.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "addInstrumentationHandler", function() { return _instrument__WEBPACK_IMPORTED_MODULE_12__["addInstrumentationHandler"]; });

/* harmony import */ var _dsn__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./dsn */ "./node_modules/@sentry/utils/esm/dsn.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Dsn", function() { return _dsn__WEBPACK_IMPORTED_MODULE_13__["Dsn"]; });















//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./node_modules/@sentry/utils/esm/instrument.js":
/*!******************************************************!*\
  !*** ./node_modules/@sentry/utils/esm/instrument.js ***!
  \******************************************************/
/*! exports provided: addInstrumentationHandler */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "addInstrumentationHandler", function() { return addInstrumentationHandler; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _is__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./is */ "./node_modules/@sentry/utils/esm/is.js");
/* harmony import */ var _logger__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./logger */ "./node_modules/@sentry/utils/esm/logger.js");
/* harmony import */ var _misc__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./misc */ "./node_modules/@sentry/utils/esm/misc.js");
/* harmony import */ var _object__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./object */ "./node_modules/@sentry/utils/esm/object.js");
/* harmony import */ var _supports__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./supports */ "./node_modules/@sentry/utils/esm/supports.js");
/* tslint:disable:only-arrow-functions no-unsafe-any */






var global = Object(_misc__WEBPACK_IMPORTED_MODULE_3__["getGlobalObject"])();
/**
 * Instrument native APIs to call handlers that can be used to create breadcrumbs, APM spans etc.
 *  - Console API
 *  - Fetch API
 *  - XHR API
 *  - History API
 *  - DOM API (click/typing)
 *  - Error API
 *  - UnhandledRejection API
 */
var handlers = {};
var instrumented = {};
/** Instruments given API */
function instrument(type) {
    if (instrumented[type]) {
        return;
    }
    instrumented[type] = true;
    switch (type) {
        case 'console':
            instrumentConsole();
            break;
        case 'dom':
            instrumentDOM();
            break;
        case 'xhr':
            instrumentXHR();
            break;
        case 'fetch':
            instrumentFetch();
            break;
        case 'history':
            instrumentHistory();
            break;
        case 'error':
            instrumentError();
            break;
        case 'unhandledrejection':
            instrumentUnhandledRejection();
            break;
        default:
            _logger__WEBPACK_IMPORTED_MODULE_2__["logger"].warn('unknown instrumentation type:', type);
    }
}
/**
 * Add handler that will be called when given type of instrumentation triggers.
 * Use at your own risk, this might break without changelog notice, only used internally.
 * @hidden
 */
function addInstrumentationHandler(handler) {
    // tslint:disable-next-line:strict-type-predicates
    if (!handler || typeof handler.type !== 'string' || typeof handler.callback !== 'function') {
        return;
    }
    handlers[handler.type] = handlers[handler.type] || [];
    handlers[handler.type].push(handler.callback);
    instrument(handler.type);
}
/** JSDoc */
function triggerHandlers(type, data) {
    var e_1, _a;
    if (!type || !handlers[type]) {
        return;
    }
    try {
        for (var _b = tslib__WEBPACK_IMPORTED_MODULE_0__["__values"](handlers[type] || []), _c = _b.next(); !_c.done; _c = _b.next()) {
            var handler = _c.value;
            try {
                handler(data);
            }
            catch (e) {
                _logger__WEBPACK_IMPORTED_MODULE_2__["logger"].error("Error while triggering instrumentation handler.\nType: " + type + "\nName: " + Object(_misc__WEBPACK_IMPORTED_MODULE_3__["getFunctionName"])(handler) + "\nError: " + e);
            }
        }
    }
    catch (e_1_1) { e_1 = { error: e_1_1 }; }
    finally {
        try {
            if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
        }
        finally { if (e_1) throw e_1.error; }
    }
}
/** JSDoc */
function instrumentConsole() {
    if (!('console' in global)) {
        return;
    }
    ['debug', 'info', 'warn', 'error', 'log', 'assert'].forEach(function (level) {
        if (!(level in global.console)) {
            return;
        }
        Object(_object__WEBPACK_IMPORTED_MODULE_4__["fill"])(global.console, level, function (originalConsoleLevel) {
            return function () {
                var args = [];
                for (var _i = 0; _i < arguments.length; _i++) {
                    args[_i] = arguments[_i];
                }
                triggerHandlers('console', { args: args, level: level });
                // this fails for some browsers. :(
                if (originalConsoleLevel) {
                    Function.prototype.apply.call(originalConsoleLevel, global.console, args);
                }
            };
        });
    });
}
/** JSDoc */
function instrumentFetch() {
    if (!Object(_supports__WEBPACK_IMPORTED_MODULE_5__["supportsNativeFetch"])()) {
        return;
    }
    Object(_object__WEBPACK_IMPORTED_MODULE_4__["fill"])(global, 'fetch', function (originalFetch) {
        return function () {
            var args = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                args[_i] = arguments[_i];
            }
            var commonHandlerData = {
                args: args,
                fetchData: {
                    method: getFetchMethod(args),
                    url: getFetchUrl(args),
                },
                startTimestamp: Date.now(),
            };
            triggerHandlers('fetch', tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, commonHandlerData));
            return originalFetch.apply(global, args).then(function (response) {
                triggerHandlers('fetch', tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, commonHandlerData, { endTimestamp: Date.now(), response: response }));
                return response;
            }, function (error) {
                triggerHandlers('fetch', tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, commonHandlerData, { endTimestamp: Date.now(), error: error }));
                throw error;
            });
        };
    });
}
/** Extract `method` from fetch call arguments */
function getFetchMethod(fetchArgs) {
    if (fetchArgs === void 0) { fetchArgs = []; }
    if ('Request' in global && Object(_is__WEBPACK_IMPORTED_MODULE_1__["isInstanceOf"])(fetchArgs[0], Request) && fetchArgs[0].method) {
        return String(fetchArgs[0].method).toUpperCase();
    }
    if (fetchArgs[1] && fetchArgs[1].method) {
        return String(fetchArgs[1].method).toUpperCase();
    }
    return 'GET';
}
/** Extract `url` from fetch call arguments */
function getFetchUrl(fetchArgs) {
    if (fetchArgs === void 0) { fetchArgs = []; }
    if (typeof fetchArgs[0] === 'string') {
        return fetchArgs[0];
    }
    if ('Request' in global && Object(_is__WEBPACK_IMPORTED_MODULE_1__["isInstanceOf"])(fetchArgs[0], Request)) {
        return fetchArgs[0].url;
    }
    return String(fetchArgs[0]);
}
/** JSDoc */
function instrumentXHR() {
    if (!('XMLHttpRequest' in global)) {
        return;
    }
    var xhrproto = XMLHttpRequest.prototype;
    Object(_object__WEBPACK_IMPORTED_MODULE_4__["fill"])(xhrproto, 'open', function (originalOpen) {
        return function () {
            var args = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                args[_i] = arguments[_i];
            }
            var xhr = this; // tslint:disable-line:no-this-assignment
            var url = args[1];
            xhr.__sentry_xhr__ = {
                method: Object(_is__WEBPACK_IMPORTED_MODULE_1__["isString"])(args[0]) ? args[0].toUpperCase() : args[0],
                url: args[1],
            };
            // if Sentry key appears in URL, don't capture it as a request
            if (Object(_is__WEBPACK_IMPORTED_MODULE_1__["isString"])(url) && xhr.__sentry_xhr__.method === 'POST' && url.match(/sentry_key/)) {
                xhr.__sentry_own_request__ = true;
            }
            var onreadystatechangeHandler = function () {
                if (xhr.readyState === 4) {
                    try {
                        // touching statusCode in some platforms throws
                        // an exception
                        if (xhr.__sentry_xhr__) {
                            xhr.__sentry_xhr__.status_code = xhr.status;
                        }
                    }
                    catch (e) {
                        /* do nothing */
                    }
                    triggerHandlers('xhr', {
                        args: args,
                        endTimestamp: Date.now(),
                        startTimestamp: Date.now(),
                        xhr: xhr,
                    });
                }
            };
            if ('onreadystatechange' in xhr && typeof xhr.onreadystatechange === 'function') {
                Object(_object__WEBPACK_IMPORTED_MODULE_4__["fill"])(xhr, 'onreadystatechange', function (original) {
                    return function () {
                        var readyStateArgs = [];
                        for (var _i = 0; _i < arguments.length; _i++) {
                            readyStateArgs[_i] = arguments[_i];
                        }
                        onreadystatechangeHandler();
                        return original.apply(xhr, readyStateArgs);
                    };
                });
            }
            else {
                xhr.addEventListener('readystatechange', onreadystatechangeHandler);
            }
            return originalOpen.apply(xhr, args);
        };
    });
    Object(_object__WEBPACK_IMPORTED_MODULE_4__["fill"])(xhrproto, 'send', function (originalSend) {
        return function () {
            var args = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                args[_i] = arguments[_i];
            }
            triggerHandlers('xhr', {
                args: args,
                startTimestamp: Date.now(),
                xhr: this,
            });
            return originalSend.apply(this, args);
        };
    });
}
var lastHref;
/** JSDoc */
function instrumentHistory() {
    if (!Object(_supports__WEBPACK_IMPORTED_MODULE_5__["supportsHistory"])()) {
        return;
    }
    var oldOnPopState = global.onpopstate;
    global.onpopstate = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        var to = global.location.href;
        // keep track of the current URL state, as we always receive only the updated state
        var from = lastHref;
        lastHref = to;
        triggerHandlers('history', {
            from: from,
            to: to,
        });
        if (oldOnPopState) {
            return oldOnPopState.apply(this, args);
        }
    };
    /** @hidden */
    function historyReplacementFunction(originalHistoryFunction) {
        return function () {
            var args = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                args[_i] = arguments[_i];
            }
            var url = args.length > 2 ? args[2] : undefined;
            if (url) {
                // coerce to string (this is what pushState does)
                var from = lastHref;
                var to = String(url);
                // keep track of the current URL state, as we always receive only the updated state
                lastHref = to;
                triggerHandlers('history', {
                    from: from,
                    to: to,
                });
            }
            return originalHistoryFunction.apply(this, args);
        };
    }
    Object(_object__WEBPACK_IMPORTED_MODULE_4__["fill"])(global.history, 'pushState', historyReplacementFunction);
    Object(_object__WEBPACK_IMPORTED_MODULE_4__["fill"])(global.history, 'replaceState', historyReplacementFunction);
}
/** JSDoc */
function instrumentDOM() {
    if (!('document' in global)) {
        return;
    }
    // Capture breadcrumbs from any click that is unhandled / bubbled up all the way
    // to the document. Do this before we instrument addEventListener.
    global.document.addEventListener('click', domEventHandler('click', triggerHandlers.bind(null, 'dom')), false);
    global.document.addEventListener('keypress', keypressEventHandler(triggerHandlers.bind(null, 'dom')), false);
    // After hooking into document bubbled up click and keypresses events, we also hook into user handled click & keypresses.
    ['EventTarget', 'Node'].forEach(function (target) {
        var proto = global[target] && global[target].prototype;
        if (!proto || !proto.hasOwnProperty || !proto.hasOwnProperty('addEventListener')) {
            return;
        }
        Object(_object__WEBPACK_IMPORTED_MODULE_4__["fill"])(proto, 'addEventListener', function (original) {
            return function (eventName, fn, options) {
                if (fn && fn.handleEvent) {
                    if (eventName === 'click') {
                        Object(_object__WEBPACK_IMPORTED_MODULE_4__["fill"])(fn, 'handleEvent', function (innerOriginal) {
                            return function (event) {
                                domEventHandler('click', triggerHandlers.bind(null, 'dom'))(event);
                                return innerOriginal.call(this, event);
                            };
                        });
                    }
                    if (eventName === 'keypress') {
                        Object(_object__WEBPACK_IMPORTED_MODULE_4__["fill"])(fn, 'handleEvent', function (innerOriginal) {
                            return function (event) {
                                keypressEventHandler(triggerHandlers.bind(null, 'dom'))(event);
                                return innerOriginal.call(this, event);
                            };
                        });
                    }
                }
                else {
                    if (eventName === 'click') {
                        domEventHandler('click', triggerHandlers.bind(null, 'dom'), true)(this);
                    }
                    if (eventName === 'keypress') {
                        keypressEventHandler(triggerHandlers.bind(null, 'dom'))(this);
                    }
                }
                return original.call(this, eventName, fn, options);
            };
        });
        Object(_object__WEBPACK_IMPORTED_MODULE_4__["fill"])(proto, 'removeEventListener', function (original) {
            return function (eventName, fn, options) {
                var callback = fn;
                try {
                    callback = callback && (callback.__sentry_wrapped__ || callback);
                }
                catch (e) {
                    // ignore, accessing __sentry_wrapped__ will throw in some Selenium environments
                }
                return original.call(this, eventName, callback, options);
            };
        });
    });
}
var debounceDuration = 1000;
var debounceTimer = 0;
var keypressTimeout;
var lastCapturedEvent;
/**
 * Wraps addEventListener to capture UI breadcrumbs
 * @param name the event name (e.g. "click")
 * @param handler function that will be triggered
 * @param debounce decides whether it should wait till another event loop
 * @returns wrapped breadcrumb events handler
 * @hidden
 */
function domEventHandler(name, handler, debounce) {
    if (debounce === void 0) { debounce = false; }
    return function (event) {
        // reset keypress timeout; e.g. triggering a 'click' after
        // a 'keypress' will reset the keypress debounce so that a new
        // set of keypresses can be recorded
        keypressTimeout = undefined;
        // It's possible this handler might trigger multiple times for the same
        // event (e.g. event propagation through node ancestors). Ignore if we've
        // already captured the event.
        if (!event || lastCapturedEvent === event) {
            return;
        }
        lastCapturedEvent = event;
        if (debounceTimer) {
            clearTimeout(debounceTimer);
        }
        if (debounce) {
            debounceTimer = setTimeout(function () {
                handler({ event: event, name: name });
            });
        }
        else {
            handler({ event: event, name: name });
        }
    };
}
/**
 * Wraps addEventListener to capture keypress UI events
 * @param handler function that will be triggered
 * @returns wrapped keypress events handler
 * @hidden
 */
function keypressEventHandler(handler) {
    // TODO: if somehow user switches keypress target before
    //       debounce timeout is triggered, we will only capture
    //       a single breadcrumb from the FIRST target (acceptable?)
    return function (event) {
        var target;
        try {
            target = event.target;
        }
        catch (e) {
            // just accessing event properties can throw an exception in some rare circumstances
            // see: https://github.com/getsentry/raven-js/issues/838
            return;
        }
        var tagName = target && target.tagName;
        // only consider keypress events on actual input elements
        // this will disregard keypresses targeting body (e.g. tabbing
        // through elements, hotkeys, etc)
        if (!tagName || (tagName !== 'INPUT' && tagName !== 'TEXTAREA' && !target.isContentEditable)) {
            return;
        }
        // record first keypress in a series, but ignore subsequent
        // keypresses until debounce clears
        if (!keypressTimeout) {
            domEventHandler('input', handler)(event);
        }
        clearTimeout(keypressTimeout);
        keypressTimeout = setTimeout(function () {
            keypressTimeout = undefined;
        }, debounceDuration);
    };
}
var _oldOnErrorHandler = null;
/** JSDoc */
function instrumentError() {
    _oldOnErrorHandler = global.onerror;
    global.onerror = function (msg, url, line, column, error) {
        triggerHandlers('error', {
            column: column,
            error: error,
            line: line,
            msg: msg,
            url: url,
        });
        if (_oldOnErrorHandler) {
            return _oldOnErrorHandler.apply(this, arguments);
        }
        return false;
    };
}
var _oldOnUnhandledRejectionHandler = null;
/** JSDoc */
function instrumentUnhandledRejection() {
    _oldOnUnhandledRejectionHandler = global.onunhandledrejection;
    global.onunhandledrejection = function (e) {
        triggerHandlers('unhandledrejection', e);
        if (_oldOnUnhandledRejectionHandler) {
            return _oldOnUnhandledRejectionHandler.apply(this, arguments);
        }
        return true;
    };
}
//# sourceMappingURL=instrument.js.map

/***/ }),

/***/ "./node_modules/@sentry/utils/esm/is.js":
/*!**********************************************!*\
  !*** ./node_modules/@sentry/utils/esm/is.js ***!
  \**********************************************/
/*! exports provided: isError, isErrorEvent, isDOMError, isDOMException, isString, isPrimitive, isPlainObject, isEvent, isElement, isRegExp, isThenable, isSyntheticEvent, isInstanceOf */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isError", function() { return isError; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isErrorEvent", function() { return isErrorEvent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isDOMError", function() { return isDOMError; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isDOMException", function() { return isDOMException; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isString", function() { return isString; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isPrimitive", function() { return isPrimitive; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isPlainObject", function() { return isPlainObject; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isEvent", function() { return isEvent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isElement", function() { return isElement; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isRegExp", function() { return isRegExp; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isThenable", function() { return isThenable; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isSyntheticEvent", function() { return isSyntheticEvent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isInstanceOf", function() { return isInstanceOf; });
/**
 * Checks whether given value's type is one of a few Error or Error-like
 * {@link isError}.
 *
 * @param wat A value to be checked.
 * @returns A boolean representing the result.
 */
function isError(wat) {
    switch (Object.prototype.toString.call(wat)) {
        case '[object Error]':
            return true;
        case '[object Exception]':
            return true;
        case '[object DOMException]':
            return true;
        default:
            return isInstanceOf(wat, Error);
    }
}
/**
 * Checks whether given value's type is ErrorEvent
 * {@link isErrorEvent}.
 *
 * @param wat A value to be checked.
 * @returns A boolean representing the result.
 */
function isErrorEvent(wat) {
    return Object.prototype.toString.call(wat) === '[object ErrorEvent]';
}
/**
 * Checks whether given value's type is DOMError
 * {@link isDOMError}.
 *
 * @param wat A value to be checked.
 * @returns A boolean representing the result.
 */
function isDOMError(wat) {
    return Object.prototype.toString.call(wat) === '[object DOMError]';
}
/**
 * Checks whether given value's type is DOMException
 * {@link isDOMException}.
 *
 * @param wat A value to be checked.
 * @returns A boolean representing the result.
 */
function isDOMException(wat) {
    return Object.prototype.toString.call(wat) === '[object DOMException]';
}
/**
 * Checks whether given value's type is a string
 * {@link isString}.
 *
 * @param wat A value to be checked.
 * @returns A boolean representing the result.
 */
function isString(wat) {
    return Object.prototype.toString.call(wat) === '[object String]';
}
/**
 * Checks whether given value's is a primitive (undefined, null, number, boolean, string)
 * {@link isPrimitive}.
 *
 * @param wat A value to be checked.
 * @returns A boolean representing the result.
 */
function isPrimitive(wat) {
    return wat === null || (typeof wat !== 'object' && typeof wat !== 'function');
}
/**
 * Checks whether given value's type is an object literal
 * {@link isPlainObject}.
 *
 * @param wat A value to be checked.
 * @returns A boolean representing the result.
 */
function isPlainObject(wat) {
    return Object.prototype.toString.call(wat) === '[object Object]';
}
/**
 * Checks whether given value's type is an Event instance
 * {@link isEvent}.
 *
 * @param wat A value to be checked.
 * @returns A boolean representing the result.
 */
function isEvent(wat) {
    // tslint:disable-next-line:strict-type-predicates
    return typeof Event !== 'undefined' && isInstanceOf(wat, Event);
}
/**
 * Checks whether given value's type is an Element instance
 * {@link isElement}.
 *
 * @param wat A value to be checked.
 * @returns A boolean representing the result.
 */
function isElement(wat) {
    // tslint:disable-next-line:strict-type-predicates
    return typeof Element !== 'undefined' && isInstanceOf(wat, Element);
}
/**
 * Checks whether given value's type is an regexp
 * {@link isRegExp}.
 *
 * @param wat A value to be checked.
 * @returns A boolean representing the result.
 */
function isRegExp(wat) {
    return Object.prototype.toString.call(wat) === '[object RegExp]';
}
/**
 * Checks whether given value has a then function.
 * @param wat A value to be checked.
 */
function isThenable(wat) {
    // tslint:disable:no-unsafe-any
    return Boolean(wat && wat.then && typeof wat.then === 'function');
    // tslint:enable:no-unsafe-any
}
/**
 * Checks whether given value's type is a SyntheticEvent
 * {@link isSyntheticEvent}.
 *
 * @param wat A value to be checked.
 * @returns A boolean representing the result.
 */
function isSyntheticEvent(wat) {
    // tslint:disable-next-line:no-unsafe-any
    return isPlainObject(wat) && 'nativeEvent' in wat && 'preventDefault' in wat && 'stopPropagation' in wat;
}
/**
 * Checks whether given value's type is an instance of provided constructor.
 * {@link isInstanceOf}.
 *
 * @param wat A value to be checked.
 * @param base A constructor to be used in a check.
 * @returns A boolean representing the result.
 */
function isInstanceOf(wat, base) {
    try {
        // tslint:disable-next-line:no-unsafe-any
        return wat instanceof base;
    }
    catch (_e) {
        return false;
    }
}
//# sourceMappingURL=is.js.map

/***/ }),

/***/ "./node_modules/@sentry/utils/esm/logger.js":
/*!**************************************************!*\
  !*** ./node_modules/@sentry/utils/esm/logger.js ***!
  \**************************************************/
/*! exports provided: logger */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "logger", function() { return logger; });
/* harmony import */ var _misc__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./misc */ "./node_modules/@sentry/utils/esm/misc.js");

// TODO: Implement different loggers for different environments
var global = Object(_misc__WEBPACK_IMPORTED_MODULE_0__["getGlobalObject"])();
/** Prefix for logging strings */
var PREFIX = 'Sentry Logger ';
/** JSDoc */
var Logger = /** @class */ (function () {
    /** JSDoc */
    function Logger() {
        this._enabled = false;
    }
    /** JSDoc */
    Logger.prototype.disable = function () {
        this._enabled = false;
    };
    /** JSDoc */
    Logger.prototype.enable = function () {
        this._enabled = true;
    };
    /** JSDoc */
    Logger.prototype.log = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        if (!this._enabled) {
            return;
        }
        Object(_misc__WEBPACK_IMPORTED_MODULE_0__["consoleSandbox"])(function () {
            global.console.log(PREFIX + "[Log]: " + args.join(' ')); // tslint:disable-line:no-console
        });
    };
    /** JSDoc */
    Logger.prototype.warn = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        if (!this._enabled) {
            return;
        }
        Object(_misc__WEBPACK_IMPORTED_MODULE_0__["consoleSandbox"])(function () {
            global.console.warn(PREFIX + "[Warn]: " + args.join(' ')); // tslint:disable-line:no-console
        });
    };
    /** JSDoc */
    Logger.prototype.error = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        if (!this._enabled) {
            return;
        }
        Object(_misc__WEBPACK_IMPORTED_MODULE_0__["consoleSandbox"])(function () {
            global.console.error(PREFIX + "[Error]: " + args.join(' ')); // tslint:disable-line:no-console
        });
    };
    return Logger;
}());
// Ensure we only have a single logger instance, even if multiple versions of @sentry/utils are being used
global.__SENTRY__ = global.__SENTRY__ || {};
var logger = global.__SENTRY__.logger || (global.__SENTRY__.logger = new Logger());

//# sourceMappingURL=logger.js.map

/***/ }),

/***/ "./node_modules/@sentry/utils/esm/memo.js":
/*!************************************************!*\
  !*** ./node_modules/@sentry/utils/esm/memo.js ***!
  \************************************************/
/*! exports provided: Memo */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Memo", function() { return Memo; });
// tslint:disable:no-unsafe-any
/**
 * Memo class used for decycle json objects. Uses WeakSet if available otherwise array.
 */
var Memo = /** @class */ (function () {
    function Memo() {
        // tslint:disable-next-line
        this._hasWeakSet = typeof WeakSet === 'function';
        this._inner = this._hasWeakSet ? new WeakSet() : [];
    }
    /**
     * Sets obj to remember.
     * @param obj Object to remember
     */
    Memo.prototype.memoize = function (obj) {
        if (this._hasWeakSet) {
            if (this._inner.has(obj)) {
                return true;
            }
            this._inner.add(obj);
            return false;
        }
        // tslint:disable-next-line:prefer-for-of
        for (var i = 0; i < this._inner.length; i++) {
            var value = this._inner[i];
            if (value === obj) {
                return true;
            }
        }
        this._inner.push(obj);
        return false;
    };
    /**
     * Removes object from internal storage.
     * @param obj Object to forget
     */
    Memo.prototype.unmemoize = function (obj) {
        if (this._hasWeakSet) {
            this._inner.delete(obj);
        }
        else {
            for (var i = 0; i < this._inner.length; i++) {
                if (this._inner[i] === obj) {
                    this._inner.splice(i, 1);
                    break;
                }
            }
        }
    };
    return Memo;
}());

//# sourceMappingURL=memo.js.map

/***/ }),

/***/ "./node_modules/@sentry/utils/esm/misc.js":
/*!************************************************!*\
  !*** ./node_modules/@sentry/utils/esm/misc.js ***!
  \************************************************/
/*! exports provided: dynamicRequire, isNodeEnv, getGlobalObject, uuid4, parseUrl, getEventDescription, consoleSandbox, addExceptionTypeValue, addExceptionMechanism, getLocationHref, htmlTreeAsString, crossPlatformPerformance, timestampWithMs, parseSemver, parseRetryAfterHeader, getFunctionName, addContextToFrame */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(process, global, module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "dynamicRequire", function() { return dynamicRequire; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isNodeEnv", function() { return isNodeEnv; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getGlobalObject", function() { return getGlobalObject; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "uuid4", function() { return uuid4; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "parseUrl", function() { return parseUrl; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getEventDescription", function() { return getEventDescription; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "consoleSandbox", function() { return consoleSandbox; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "addExceptionTypeValue", function() { return addExceptionTypeValue; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "addExceptionMechanism", function() { return addExceptionMechanism; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getLocationHref", function() { return getLocationHref; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "htmlTreeAsString", function() { return htmlTreeAsString; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "crossPlatformPerformance", function() { return crossPlatformPerformance; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "timestampWithMs", function() { return timestampWithMs; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "parseSemver", function() { return parseSemver; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "parseRetryAfterHeader", function() { return parseRetryAfterHeader; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getFunctionName", function() { return getFunctionName; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "addContextToFrame", function() { return addContextToFrame; });
/* harmony import */ var _is__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./is */ "./node_modules/@sentry/utils/esm/is.js");
/* harmony import */ var _string__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./string */ "./node_modules/@sentry/utils/esm/string.js");


/**
 * Requires a module which is protected against bundler minification.
 *
 * @param request The module path to resolve
 */
function dynamicRequire(mod, request) {
    // tslint:disable-next-line: no-unsafe-any
    return mod.require(request);
}
/**
 * Checks whether we're in the Node.js or Browser environment
 *
 * @returns Answer to given question
 */
function isNodeEnv() {
    // tslint:disable:strict-type-predicates
    return Object.prototype.toString.call(typeof process !== 'undefined' ? process : 0) === '[object process]';
}
var fallbackGlobalObject = {};
/**
 * Safely get global scope object
 *
 * @returns Global scope object
 */
function getGlobalObject() {
    return (isNodeEnv()
        ? global
        : typeof window !== 'undefined'
            ? window
            : typeof self !== 'undefined'
                ? self
                : fallbackGlobalObject);
}
/**
 * UUID4 generator
 *
 * @returns string Generated UUID4.
 */
function uuid4() {
    var global = getGlobalObject();
    var crypto = global.crypto || global.msCrypto;
    if (!(crypto === void 0) && crypto.getRandomValues) {
        // Use window.crypto API if available
        var arr = new Uint16Array(8);
        crypto.getRandomValues(arr);
        // set 4 in byte 7
        // tslint:disable-next-line:no-bitwise
        arr[3] = (arr[3] & 0xfff) | 0x4000;
        // set 2 most significant bits of byte 9 to '10'
        // tslint:disable-next-line:no-bitwise
        arr[4] = (arr[4] & 0x3fff) | 0x8000;
        var pad = function (num) {
            var v = num.toString(16);
            while (v.length < 4) {
                v = "0" + v;
            }
            return v;
        };
        return (pad(arr[0]) + pad(arr[1]) + pad(arr[2]) + pad(arr[3]) + pad(arr[4]) + pad(arr[5]) + pad(arr[6]) + pad(arr[7]));
    }
    // http://stackoverflow.com/questions/105034/how-to-create-a-guid-uuid-in-javascript/2117523#2117523
    return 'xxxxxxxxxxxx4xxxyxxxxxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        // tslint:disable-next-line:no-bitwise
        var r = (Math.random() * 16) | 0;
        // tslint:disable-next-line:no-bitwise
        var v = c === 'x' ? r : (r & 0x3) | 0x8;
        return v.toString(16);
    });
}
/**
 * Parses string form of URL into an object
 * // borrowed from https://tools.ietf.org/html/rfc3986#appendix-B
 * // intentionally using regex and not <a/> href parsing trick because React Native and other
 * // environments where DOM might not be available
 * @returns parsed URL object
 */
function parseUrl(url) {
    if (!url) {
        return {};
    }
    var match = url.match(/^(([^:\/?#]+):)?(\/\/([^\/?#]*))?([^?#]*)(\?([^#]*))?(#(.*))?$/);
    if (!match) {
        return {};
    }
    // coerce to undefined values to empty string so we don't get 'undefined'
    var query = match[6] || '';
    var fragment = match[8] || '';
    return {
        host: match[4],
        path: match[5],
        protocol: match[2],
        relative: match[5] + query + fragment,
    };
}
/**
 * Extracts either message or type+value from an event that can be used for user-facing logs
 * @returns event's description
 */
function getEventDescription(event) {
    if (event.message) {
        return event.message;
    }
    if (event.exception && event.exception.values && event.exception.values[0]) {
        var exception = event.exception.values[0];
        if (exception.type && exception.value) {
            return exception.type + ": " + exception.value;
        }
        return exception.type || exception.value || event.event_id || '<unknown>';
    }
    return event.event_id || '<unknown>';
}
/** JSDoc */
function consoleSandbox(callback) {
    var global = getGlobalObject();
    var levels = ['debug', 'info', 'warn', 'error', 'log', 'assert'];
    if (!('console' in global)) {
        return callback();
    }
    var originalConsole = global.console;
    var wrappedLevels = {};
    // Restore all wrapped console methods
    levels.forEach(function (level) {
        if (level in global.console && originalConsole[level].__sentry_original__) {
            wrappedLevels[level] = originalConsole[level];
            originalConsole[level] = originalConsole[level].__sentry_original__;
        }
    });
    // Perform callback manipulations
    var result = callback();
    // Revert restoration to wrapped state
    Object.keys(wrappedLevels).forEach(function (level) {
        originalConsole[level] = wrappedLevels[level];
    });
    return result;
}
/**
 * Adds exception values, type and value to an synthetic Exception.
 * @param event The event to modify.
 * @param value Value of the exception.
 * @param type Type of the exception.
 * @hidden
 */
function addExceptionTypeValue(event, value, type) {
    event.exception = event.exception || {};
    event.exception.values = event.exception.values || [];
    event.exception.values[0] = event.exception.values[0] || {};
    event.exception.values[0].value = event.exception.values[0].value || value || '';
    event.exception.values[0].type = event.exception.values[0].type || type || 'Error';
}
/**
 * Adds exception mechanism to a given event.
 * @param event The event to modify.
 * @param mechanism Mechanism of the mechanism.
 * @hidden
 */
function addExceptionMechanism(event, mechanism) {
    if (mechanism === void 0) { mechanism = {}; }
    // TODO: Use real type with `keyof Mechanism` thingy and maybe make it better?
    try {
        // @ts-ignore
        // tslint:disable:no-non-null-assertion
        event.exception.values[0].mechanism = event.exception.values[0].mechanism || {};
        Object.keys(mechanism).forEach(function (key) {
            // @ts-ignore
            event.exception.values[0].mechanism[key] = mechanism[key];
        });
    }
    catch (_oO) {
        // no-empty
    }
}
/**
 * A safe form of location.href
 */
function getLocationHref() {
    try {
        return document.location.href;
    }
    catch (oO) {
        return '';
    }
}
/**
 * Given a child DOM element, returns a query-selector statement describing that
 * and its ancestors
 * e.g. [HTMLElement] => body > div > input#foo.btn[name=baz]
 * @returns generated DOM path
 */
function htmlTreeAsString(elem) {
    // try/catch both:
    // - accessing event.target (see getsentry/raven-js#838, #768)
    // - `htmlTreeAsString` because it's complex, and just accessing the DOM incorrectly
    // - can throw an exception in some circumstances.
    try {
        var currentElem = elem;
        var MAX_TRAVERSE_HEIGHT = 5;
        var MAX_OUTPUT_LEN = 80;
        var out = [];
        var height = 0;
        var len = 0;
        var separator = ' > ';
        var sepLength = separator.length;
        var nextStr = void 0;
        while (currentElem && height++ < MAX_TRAVERSE_HEIGHT) {
            nextStr = _htmlElementAsString(currentElem);
            // bail out if
            // - nextStr is the 'html' element
            // - the length of the string that would be created exceeds MAX_OUTPUT_LEN
            //   (ignore this limit if we are on the first iteration)
            if (nextStr === 'html' || (height > 1 && len + out.length * sepLength + nextStr.length >= MAX_OUTPUT_LEN)) {
                break;
            }
            out.push(nextStr);
            len += nextStr.length;
            currentElem = currentElem.parentNode;
        }
        return out.reverse().join(separator);
    }
    catch (_oO) {
        return '<unknown>';
    }
}
/**
 * Returns a simple, query-selector representation of a DOM element
 * e.g. [HTMLElement] => input#foo.btn[name=baz]
 * @returns generated DOM path
 */
function _htmlElementAsString(el) {
    var elem = el;
    var out = [];
    var className;
    var classes;
    var key;
    var attr;
    var i;
    if (!elem || !elem.tagName) {
        return '';
    }
    out.push(elem.tagName.toLowerCase());
    if (elem.id) {
        out.push("#" + elem.id);
    }
    className = elem.className;
    if (className && Object(_is__WEBPACK_IMPORTED_MODULE_0__["isString"])(className)) {
        classes = className.split(/\s+/);
        for (i = 0; i < classes.length; i++) {
            out.push("." + classes[i]);
        }
    }
    var allowedAttrs = ['type', 'name', 'title', 'alt'];
    for (i = 0; i < allowedAttrs.length; i++) {
        key = allowedAttrs[i];
        attr = elem.getAttribute(key);
        if (attr) {
            out.push("[" + key + "=\"" + attr + "\"]");
        }
    }
    return out.join('');
}
var INITIAL_TIME = Date.now();
var prevNow = 0;
var performanceFallback = {
    now: function () {
        var now = Date.now() - INITIAL_TIME;
        if (now < prevNow) {
            now = prevNow;
        }
        prevNow = now;
        return now;
    },
    timeOrigin: INITIAL_TIME,
};
var crossPlatformPerformance = (function () {
    if (isNodeEnv()) {
        try {
            var perfHooks = dynamicRequire(module, 'perf_hooks');
            return perfHooks.performance;
        }
        catch (_) {
            return performanceFallback;
        }
    }
    var performance = getGlobalObject().performance;
    if (!performance || !performance.now) {
        return performanceFallback;
    }
    // Polyfill for performance.timeOrigin.
    //
    // While performance.timing.navigationStart is deprecated in favor of performance.timeOrigin, performance.timeOrigin
    // is not as widely supported. Namely, performance.timeOrigin is undefined in Safari as of writing.
    // tslint:disable-next-line:strict-type-predicates
    if (performance.timeOrigin === undefined) {
        // As of writing, performance.timing is not available in Web Workers in mainstream browsers, so it is not always a
        // valid fallback. In the absence of a initial time provided by the browser, fallback to INITIAL_TIME.
        // @ts-ignore
        // tslint:disable-next-line:deprecation
        performance.timeOrigin = (performance.timing && performance.timing.navigationStart) || INITIAL_TIME;
    }
    return performance;
})();
/**
 * Returns a timestamp in seconds with milliseconds precision since the UNIX epoch calculated with the monotonic clock.
 */
function timestampWithMs() {
    return (crossPlatformPerformance.timeOrigin + crossPlatformPerformance.now()) / 1000;
}
// https://semver.org/#is-there-a-suggested-regular-expression-regex-to-check-a-semver-string
var SEMVER_REGEXP = /^(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)(?:-((?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+([0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?$/;
/**
 * Parses input into a SemVer interface
 * @param input string representation of a semver version
 */
function parseSemver(input) {
    var match = input.match(SEMVER_REGEXP) || [];
    var major = parseInt(match[1], 10);
    var minor = parseInt(match[2], 10);
    var patch = parseInt(match[3], 10);
    return {
        buildmetadata: match[5],
        major: isNaN(major) ? undefined : major,
        minor: isNaN(minor) ? undefined : minor,
        patch: isNaN(patch) ? undefined : patch,
        prerelease: match[4],
    };
}
var defaultRetryAfter = 60 * 1000; // 60 seconds
/**
 * Extracts Retry-After value from the request header or returns default value
 * @param now current unix timestamp
 * @param header string representation of 'Retry-After' header
 */
function parseRetryAfterHeader(now, header) {
    if (!header) {
        return defaultRetryAfter;
    }
    var headerDelay = parseInt("" + header, 10);
    if (!isNaN(headerDelay)) {
        return headerDelay * 1000;
    }
    var headerDate = Date.parse("" + header);
    if (!isNaN(headerDate)) {
        return headerDate - now;
    }
    return defaultRetryAfter;
}
var defaultFunctionName = '<anonymous>';
/**
 * Safely extract function name from itself
 */
function getFunctionName(fn) {
    try {
        if (!fn || typeof fn !== 'function') {
            return defaultFunctionName;
        }
        return fn.name || defaultFunctionName;
    }
    catch (e) {
        // Just accessing custom props in some Selenium environments
        // can cause a "Permission denied" exception (see raven-js#495).
        return defaultFunctionName;
    }
}
/**
 * This function adds context (pre/post/line) lines to the provided frame
 *
 * @param lines string[] containing all lines
 * @param frame StackFrame that will be mutated
 * @param linesOfContext number of context lines we want to add pre/post
 */
function addContextToFrame(lines, frame, linesOfContext) {
    if (linesOfContext === void 0) { linesOfContext = 5; }
    var lineno = frame.lineno || 0;
    var maxLines = lines.length;
    var sourceLine = Math.max(Math.min(maxLines, lineno - 1), 0);
    frame.pre_context = lines
        .slice(Math.max(0, sourceLine - linesOfContext), sourceLine)
        .map(function (line) { return Object(_string__WEBPACK_IMPORTED_MODULE_1__["snipLine"])(line, 0); });
    frame.context_line = Object(_string__WEBPACK_IMPORTED_MODULE_1__["snipLine"])(lines[Math.min(maxLines - 1, sourceLine)], frame.colno || 0);
    frame.post_context = lines
        .slice(Math.min(sourceLine + 1, maxLines), sourceLine + 1 + linesOfContext)
        .map(function (line) { return Object(_string__WEBPACK_IMPORTED_MODULE_1__["snipLine"])(line, 0); });
}
//# sourceMappingURL=misc.js.map
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../process/browser.js */ "./node_modules/process/browser.js"), __webpack_require__(/*! ./../../../webpack/buildin/global.js */ "./node_modules/webpack/buildin/global.js"), __webpack_require__(/*! ./../../../webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ }),

/***/ "./node_modules/@sentry/utils/esm/object.js":
/*!**************************************************!*\
  !*** ./node_modules/@sentry/utils/esm/object.js ***!
  \**************************************************/
/*! exports provided: fill, urlEncode, normalizeToSize, walk, normalize, extractExceptionKeysForMessage, dropUndefinedKeys */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(global) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fill", function() { return fill; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "urlEncode", function() { return urlEncode; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "normalizeToSize", function() { return normalizeToSize; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "walk", function() { return walk; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "normalize", function() { return normalize; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "extractExceptionKeysForMessage", function() { return extractExceptionKeysForMessage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "dropUndefinedKeys", function() { return dropUndefinedKeys; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _is__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./is */ "./node_modules/@sentry/utils/esm/is.js");
/* harmony import */ var _memo__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./memo */ "./node_modules/@sentry/utils/esm/memo.js");
/* harmony import */ var _misc__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./misc */ "./node_modules/@sentry/utils/esm/misc.js");
/* harmony import */ var _string__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./string */ "./node_modules/@sentry/utils/esm/string.js");





/**
 * Wrap a given object method with a higher-order function
 *
 * @param source An object that contains a method to be wrapped.
 * @param name A name of method to be wrapped.
 * @param replacement A function that should be used to wrap a given method.
 * @returns void
 */
function fill(source, name, replacement) {
    if (!(name in source)) {
        return;
    }
    var original = source[name];
    var wrapped = replacement(original);
    // Make sure it's a function first, as we need to attach an empty prototype for `defineProperties` to work
    // otherwise it'll throw "TypeError: Object.defineProperties called on non-object"
    // tslint:disable-next-line:strict-type-predicates
    if (typeof wrapped === 'function') {
        try {
            wrapped.prototype = wrapped.prototype || {};
            Object.defineProperties(wrapped, {
                __sentry_original__: {
                    enumerable: false,
                    value: original,
                },
            });
        }
        catch (_Oo) {
            // This can throw if multiple fill happens on a global object like XMLHttpRequest
            // Fixes https://github.com/getsentry/sentry-javascript/issues/2043
        }
    }
    source[name] = wrapped;
}
/**
 * Encodes given object into url-friendly format
 *
 * @param object An object that contains serializable values
 * @returns string Encoded
 */
function urlEncode(object) {
    return Object.keys(object)
        .map(
    // tslint:disable-next-line:no-unsafe-any
    function (key) { return encodeURIComponent(key) + "=" + encodeURIComponent(object[key]); })
        .join('&');
}
/**
 * Transforms any object into an object literal with all it's attributes
 * attached to it.
 *
 * @param value Initial source that we have to transform in order to be usable by the serializer
 */
function getWalkSource(value) {
    if (Object(_is__WEBPACK_IMPORTED_MODULE_1__["isError"])(value)) {
        var error = value;
        var err = {
            message: error.message,
            name: error.name,
            stack: error.stack,
        };
        for (var i in error) {
            if (Object.prototype.hasOwnProperty.call(error, i)) {
                err[i] = error[i];
            }
        }
        return err;
    }
    if (Object(_is__WEBPACK_IMPORTED_MODULE_1__["isEvent"])(value)) {
        var event_1 = value;
        var source = {};
        source.type = event_1.type;
        // Accessing event.target can throw (see getsentry/raven-js#838, #768)
        try {
            source.target = Object(_is__WEBPACK_IMPORTED_MODULE_1__["isElement"])(event_1.target)
                ? Object(_misc__WEBPACK_IMPORTED_MODULE_3__["htmlTreeAsString"])(event_1.target)
                : Object.prototype.toString.call(event_1.target);
        }
        catch (_oO) {
            source.target = '<unknown>';
        }
        try {
            source.currentTarget = Object(_is__WEBPACK_IMPORTED_MODULE_1__["isElement"])(event_1.currentTarget)
                ? Object(_misc__WEBPACK_IMPORTED_MODULE_3__["htmlTreeAsString"])(event_1.currentTarget)
                : Object.prototype.toString.call(event_1.currentTarget);
        }
        catch (_oO) {
            source.currentTarget = '<unknown>';
        }
        // tslint:disable-next-line:strict-type-predicates
        if (typeof CustomEvent !== 'undefined' && Object(_is__WEBPACK_IMPORTED_MODULE_1__["isInstanceOf"])(value, CustomEvent)) {
            source.detail = event_1.detail;
        }
        for (var i in event_1) {
            if (Object.prototype.hasOwnProperty.call(event_1, i)) {
                source[i] = event_1;
            }
        }
        return source;
    }
    return value;
}
/** Calculates bytes size of input string */
function utf8Length(value) {
    // tslint:disable-next-line:no-bitwise
    return ~-encodeURI(value).split(/%..|./).length;
}
/** Calculates bytes size of input object */
function jsonSize(value) {
    return utf8Length(JSON.stringify(value));
}
/** JSDoc */
function normalizeToSize(object, 
// Default Node.js REPL depth
depth, 
// 100kB, as 200kB is max payload size, so half sounds reasonable
maxSize) {
    if (depth === void 0) { depth = 3; }
    if (maxSize === void 0) { maxSize = 100 * 1024; }
    var serialized = normalize(object, depth);
    if (jsonSize(serialized) > maxSize) {
        return normalizeToSize(object, depth - 1, maxSize);
    }
    return serialized;
}
/** Transforms any input value into a string form, either primitive value or a type of the input */
function serializeValue(value) {
    var type = Object.prototype.toString.call(value);
    // Node.js REPL notation
    if (typeof value === 'string') {
        return value;
    }
    if (type === '[object Object]') {
        return '[Object]';
    }
    if (type === '[object Array]') {
        return '[Array]';
    }
    var normalized = normalizeValue(value);
    return Object(_is__WEBPACK_IMPORTED_MODULE_1__["isPrimitive"])(normalized) ? normalized : type;
}
/**
 * normalizeValue()
 *
 * Takes unserializable input and make it serializable friendly
 *
 * - translates undefined/NaN values to "[undefined]"/"[NaN]" respectively,
 * - serializes Error objects
 * - filter global objects
 */
// tslint:disable-next-line:cyclomatic-complexity
function normalizeValue(value, key) {
    if (key === 'domain' && value && typeof value === 'object' && value._events) {
        return '[Domain]';
    }
    if (key === 'domainEmitter') {
        return '[DomainEmitter]';
    }
    if (typeof global !== 'undefined' && value === global) {
        return '[Global]';
    }
    if (typeof window !== 'undefined' && value === window) {
        return '[Window]';
    }
    if (typeof document !== 'undefined' && value === document) {
        return '[Document]';
    }
    // React's SyntheticEvent thingy
    if (Object(_is__WEBPACK_IMPORTED_MODULE_1__["isSyntheticEvent"])(value)) {
        return '[SyntheticEvent]';
    }
    // tslint:disable-next-line:no-tautology-expression
    if (typeof value === 'number' && value !== value) {
        return '[NaN]';
    }
    if (value === void 0) {
        return '[undefined]';
    }
    if (typeof value === 'function') {
        return "[Function: " + Object(_misc__WEBPACK_IMPORTED_MODULE_3__["getFunctionName"])(value) + "]";
    }
    return value;
}
/**
 * Walks an object to perform a normalization on it
 *
 * @param key of object that's walked in current iteration
 * @param value object to be walked
 * @param depth Optional number indicating how deep should walking be performed
 * @param memo Optional Memo class handling decycling
 */
function walk(key, value, depth, memo) {
    if (depth === void 0) { depth = +Infinity; }
    if (memo === void 0) { memo = new _memo__WEBPACK_IMPORTED_MODULE_2__["Memo"](); }
    // If we reach the maximum depth, serialize whatever has left
    if (depth === 0) {
        return serializeValue(value);
    }
    // If value implements `toJSON` method, call it and return early
    // tslint:disable:no-unsafe-any
    if (value !== null && value !== undefined && typeof value.toJSON === 'function') {
        return value.toJSON();
    }
    // tslint:enable:no-unsafe-any
    // If normalized value is a primitive, there are no branches left to walk, so we can just bail out, as theres no point in going down that branch any further
    var normalized = normalizeValue(value, key);
    if (Object(_is__WEBPACK_IMPORTED_MODULE_1__["isPrimitive"])(normalized)) {
        return normalized;
    }
    // Create source that we will use for next itterations, either objectified error object (Error type with extracted keys:value pairs) or the input itself
    var source = getWalkSource(value);
    // Create an accumulator that will act as a parent for all future itterations of that branch
    var acc = Array.isArray(value) ? [] : {};
    // If we already walked that branch, bail out, as it's circular reference
    if (memo.memoize(value)) {
        return '[Circular ~]';
    }
    // Walk all keys of the source
    for (var innerKey in source) {
        // Avoid iterating over fields in the prototype if they've somehow been exposed to enumeration.
        if (!Object.prototype.hasOwnProperty.call(source, innerKey)) {
            continue;
        }
        // Recursively walk through all the child nodes
        acc[innerKey] = walk(innerKey, source[innerKey], depth - 1, memo);
    }
    // Once walked through all the branches, remove the parent from memo storage
    memo.unmemoize(value);
    // Return accumulated values
    return acc;
}
/**
 * normalize()
 *
 * - Creates a copy to prevent original input mutation
 * - Skip non-enumerablers
 * - Calls `toJSON` if implemented
 * - Removes circular references
 * - Translates non-serializeable values (undefined/NaN/Functions) to serializable format
 * - Translates known global objects/Classes to a string representations
 * - Takes care of Error objects serialization
 * - Optionally limit depth of final output
 */
function normalize(input, depth) {
    try {
        // tslint:disable-next-line:no-unsafe-any
        return JSON.parse(JSON.stringify(input, function (key, value) { return walk(key, value, depth); }));
    }
    catch (_oO) {
        return '**non-serializable**';
    }
}
/**
 * Given any captured exception, extract its keys and create a sorted
 * and truncated list that will be used inside the event message.
 * eg. `Non-error exception captured with keys: foo, bar, baz`
 */
function extractExceptionKeysForMessage(exception, maxLength) {
    if (maxLength === void 0) { maxLength = 40; }
    // tslint:disable:strict-type-predicates
    var keys = Object.keys(getWalkSource(exception));
    keys.sort();
    if (!keys.length) {
        return '[object has no keys]';
    }
    if (keys[0].length >= maxLength) {
        return Object(_string__WEBPACK_IMPORTED_MODULE_4__["truncate"])(keys[0], maxLength);
    }
    for (var includedKeys = keys.length; includedKeys > 0; includedKeys--) {
        var serialized = keys.slice(0, includedKeys).join(', ');
        if (serialized.length > maxLength) {
            continue;
        }
        if (includedKeys === keys.length) {
            return serialized;
        }
        return Object(_string__WEBPACK_IMPORTED_MODULE_4__["truncate"])(serialized, maxLength);
    }
    return '';
}
/**
 * Given any object, return the new object with removed keys that value was `undefined`.
 * Works recursively on objects and arrays.
 */
function dropUndefinedKeys(val) {
    var e_1, _a;
    if (Object(_is__WEBPACK_IMPORTED_MODULE_1__["isPlainObject"])(val)) {
        var obj = val;
        var rv = {};
        try {
            for (var _b = tslib__WEBPACK_IMPORTED_MODULE_0__["__values"](Object.keys(obj)), _c = _b.next(); !_c.done; _c = _b.next()) {
                var key = _c.value;
                if (typeof obj[key] !== 'undefined') {
                    rv[key] = dropUndefinedKeys(obj[key]);
                }
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
            }
            finally { if (e_1) throw e_1.error; }
        }
        return rv;
    }
    if (Array.isArray(val)) {
        return val.map(dropUndefinedKeys);
    }
    return val;
}
//# sourceMappingURL=object.js.map
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../webpack/buildin/global.js */ "./node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./node_modules/@sentry/utils/esm/path.js":
/*!************************************************!*\
  !*** ./node_modules/@sentry/utils/esm/path.js ***!
  \************************************************/
/*! exports provided: resolve, relative, normalizePath, isAbsolute, join, dirname, basename */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "resolve", function() { return resolve; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "relative", function() { return relative; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "normalizePath", function() { return normalizePath; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isAbsolute", function() { return isAbsolute; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "join", function() { return join; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "dirname", function() { return dirname; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "basename", function() { return basename; });
// Slightly modified (no IE8 support, ES6) and transcribed to TypeScript
// https://raw.githubusercontent.com/calvinmetcalf/rollup-plugin-node-builtins/master/src/es6/path.js
/** JSDoc */
function normalizeArray(parts, allowAboveRoot) {
    // if the path tries to go above the root, `up` ends up > 0
    var up = 0;
    for (var i = parts.length - 1; i >= 0; i--) {
        var last = parts[i];
        if (last === '.') {
            parts.splice(i, 1);
        }
        else if (last === '..') {
            parts.splice(i, 1);
            up++;
        }
        else if (up) {
            parts.splice(i, 1);
            up--;
        }
    }
    // if the path is allowed to go above the root, restore leading ..s
    if (allowAboveRoot) {
        for (; up--; up) {
            parts.unshift('..');
        }
    }
    return parts;
}
// Split a filename into [root, dir, basename, ext], unix version
// 'root' is just a slash, or nothing.
var splitPathRe = /^(\/?|)([\s\S]*?)((?:\.{1,2}|[^\/]+?|)(\.[^.\/]*|))(?:[\/]*)$/;
/** JSDoc */
function splitPath(filename) {
    var parts = splitPathRe.exec(filename);
    return parts ? parts.slice(1) : [];
}
// path.resolve([from ...], to)
// posix version
/** JSDoc */
function resolve() {
    var args = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        args[_i] = arguments[_i];
    }
    var resolvedPath = '';
    var resolvedAbsolute = false;
    for (var i = args.length - 1; i >= -1 && !resolvedAbsolute; i--) {
        var path = i >= 0 ? args[i] : '/';
        // Skip empty entries
        if (!path) {
            continue;
        }
        resolvedPath = path + "/" + resolvedPath;
        resolvedAbsolute = path.charAt(0) === '/';
    }
    // At this point the path should be resolved to a full absolute path, but
    // handle relative paths to be safe (might happen when process.cwd() fails)
    // Normalize the path
    resolvedPath = normalizeArray(resolvedPath.split('/').filter(function (p) { return !!p; }), !resolvedAbsolute).join('/');
    return (resolvedAbsolute ? '/' : '') + resolvedPath || '.';
}
/** JSDoc */
function trim(arr) {
    var start = 0;
    for (; start < arr.length; start++) {
        if (arr[start] !== '') {
            break;
        }
    }
    var end = arr.length - 1;
    for (; end >= 0; end--) {
        if (arr[end] !== '') {
            break;
        }
    }
    if (start > end) {
        return [];
    }
    return arr.slice(start, end - start + 1);
}
// path.relative(from, to)
// posix version
/** JSDoc */
function relative(from, to) {
    // tslint:disable:no-parameter-reassignment
    from = resolve(from).substr(1);
    to = resolve(to).substr(1);
    var fromParts = trim(from.split('/'));
    var toParts = trim(to.split('/'));
    var length = Math.min(fromParts.length, toParts.length);
    var samePartsLength = length;
    for (var i = 0; i < length; i++) {
        if (fromParts[i] !== toParts[i]) {
            samePartsLength = i;
            break;
        }
    }
    var outputParts = [];
    for (var i = samePartsLength; i < fromParts.length; i++) {
        outputParts.push('..');
    }
    outputParts = outputParts.concat(toParts.slice(samePartsLength));
    return outputParts.join('/');
}
// path.normalize(path)
// posix version
/** JSDoc */
function normalizePath(path) {
    var isPathAbsolute = isAbsolute(path);
    var trailingSlash = path.substr(-1) === '/';
    // Normalize the path
    var normalizedPath = normalizeArray(path.split('/').filter(function (p) { return !!p; }), !isPathAbsolute).join('/');
    if (!normalizedPath && !isPathAbsolute) {
        normalizedPath = '.';
    }
    if (normalizedPath && trailingSlash) {
        normalizedPath += '/';
    }
    return (isPathAbsolute ? '/' : '') + normalizedPath;
}
// posix version
/** JSDoc */
function isAbsolute(path) {
    return path.charAt(0) === '/';
}
// posix version
/** JSDoc */
function join() {
    var args = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        args[_i] = arguments[_i];
    }
    return normalizePath(args.join('/'));
}
/** JSDoc */
function dirname(path) {
    var result = splitPath(path);
    var root = result[0];
    var dir = result[1];
    if (!root && !dir) {
        // No dirname whatsoever
        return '.';
    }
    if (dir) {
        // It has a dirname, strip trailing slash
        dir = dir.substr(0, dir.length - 1);
    }
    return root + dir;
}
/** JSDoc */
function basename(path, ext) {
    var f = splitPath(path)[2];
    if (ext && f.substr(ext.length * -1) === ext) {
        f = f.substr(0, f.length - ext.length);
    }
    return f;
}
//# sourceMappingURL=path.js.map

/***/ }),

/***/ "./node_modules/@sentry/utils/esm/polyfill.js":
/*!****************************************************!*\
  !*** ./node_modules/@sentry/utils/esm/polyfill.js ***!
  \****************************************************/
/*! exports provided: setPrototypeOf */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setPrototypeOf", function() { return setPrototypeOf; });
var setPrototypeOf = Object.setPrototypeOf || ({ __proto__: [] } instanceof Array ? setProtoOf : mixinProperties); // tslint:disable-line:no-unbound-method
/**
 * setPrototypeOf polyfill using __proto__
 */
function setProtoOf(obj, proto) {
    // @ts-ignore
    obj.__proto__ = proto;
    return obj;
}
/**
 * setPrototypeOf polyfill using mixin
 */
function mixinProperties(obj, proto) {
    for (var prop in proto) {
        if (!obj.hasOwnProperty(prop)) {
            // @ts-ignore
            obj[prop] = proto[prop];
        }
    }
    return obj;
}
//# sourceMappingURL=polyfill.js.map

/***/ }),

/***/ "./node_modules/@sentry/utils/esm/promisebuffer.js":
/*!*********************************************************!*\
  !*** ./node_modules/@sentry/utils/esm/promisebuffer.js ***!
  \*********************************************************/
/*! exports provided: PromiseBuffer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PromiseBuffer", function() { return PromiseBuffer; });
/* harmony import */ var _error__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./error */ "./node_modules/@sentry/utils/esm/error.js");
/* harmony import */ var _syncpromise__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./syncpromise */ "./node_modules/@sentry/utils/esm/syncpromise.js");


/** A simple queue that holds promises. */
var PromiseBuffer = /** @class */ (function () {
    function PromiseBuffer(_limit) {
        this._limit = _limit;
        /** Internal set of queued Promises */
        this._buffer = [];
    }
    /**
     * Says if the buffer is ready to take more requests
     */
    PromiseBuffer.prototype.isReady = function () {
        return this._limit === undefined || this.length() < this._limit;
    };
    /**
     * Add a promise to the queue.
     *
     * @param task Can be any PromiseLike<T>
     * @returns The original promise.
     */
    PromiseBuffer.prototype.add = function (task) {
        var _this = this;
        if (!this.isReady()) {
            return _syncpromise__WEBPACK_IMPORTED_MODULE_1__["SyncPromise"].reject(new _error__WEBPACK_IMPORTED_MODULE_0__["SentryError"]('Not adding Promise due to buffer limit reached.'));
        }
        if (this._buffer.indexOf(task) === -1) {
            this._buffer.push(task);
        }
        task
            .then(function () { return _this.remove(task); })
            .then(null, function () {
            return _this.remove(task).then(null, function () {
                // We have to add this catch here otherwise we have an unhandledPromiseRejection
                // because it's a new Promise chain.
            });
        });
        return task;
    };
    /**
     * Remove a promise to the queue.
     *
     * @param task Can be any PromiseLike<T>
     * @returns Removed promise.
     */
    PromiseBuffer.prototype.remove = function (task) {
        var removedTask = this._buffer.splice(this._buffer.indexOf(task), 1)[0];
        return removedTask;
    };
    /**
     * This function returns the number of unresolved promises in the queue.
     */
    PromiseBuffer.prototype.length = function () {
        return this._buffer.length;
    };
    /**
     * This will drain the whole queue, returns true if queue is empty or drained.
     * If timeout is provided and the queue takes longer to drain, the promise still resolves but with false.
     *
     * @param timeout Number in ms to wait until it resolves with false.
     */
    PromiseBuffer.prototype.drain = function (timeout) {
        var _this = this;
        return new _syncpromise__WEBPACK_IMPORTED_MODULE_1__["SyncPromise"](function (resolve) {
            var capturedSetTimeout = setTimeout(function () {
                if (timeout && timeout > 0) {
                    resolve(false);
                }
            }, timeout);
            _syncpromise__WEBPACK_IMPORTED_MODULE_1__["SyncPromise"].all(_this._buffer)
                .then(function () {
                clearTimeout(capturedSetTimeout);
                resolve(true);
            })
                .then(null, function () {
                resolve(true);
            });
        });
    };
    return PromiseBuffer;
}());

//# sourceMappingURL=promisebuffer.js.map

/***/ }),

/***/ "./node_modules/@sentry/utils/esm/string.js":
/*!**************************************************!*\
  !*** ./node_modules/@sentry/utils/esm/string.js ***!
  \**************************************************/
/*! exports provided: truncate, snipLine, safeJoin, isMatchingPattern */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "truncate", function() { return truncate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "snipLine", function() { return snipLine; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "safeJoin", function() { return safeJoin; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isMatchingPattern", function() { return isMatchingPattern; });
/* harmony import */ var _is__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./is */ "./node_modules/@sentry/utils/esm/is.js");

/**
 * Truncates given string to the maximum characters count
 *
 * @param str An object that contains serializable values
 * @param max Maximum number of characters in truncated string
 * @returns string Encoded
 */
function truncate(str, max) {
    if (max === void 0) { max = 0; }
    // tslint:disable-next-line:strict-type-predicates
    if (typeof str !== 'string' || max === 0) {
        return str;
    }
    return str.length <= max ? str : str.substr(0, max) + "...";
}
/**
 * This is basically just `trim_line` from
 * https://github.com/getsentry/sentry/blob/master/src/sentry/lang/javascript/processor.py#L67
 *
 * @param str An object that contains serializable values
 * @param max Maximum number of characters in truncated string
 * @returns string Encoded
 */
function snipLine(line, colno) {
    var newLine = line;
    var ll = newLine.length;
    if (ll <= 150) {
        return newLine;
    }
    if (colno > ll) {
        colno = ll; // tslint:disable-line:no-parameter-reassignment
    }
    var start = Math.max(colno - 60, 0);
    if (start < 5) {
        start = 0;
    }
    var end = Math.min(start + 140, ll);
    if (end > ll - 5) {
        end = ll;
    }
    if (end === ll) {
        start = Math.max(end - 140, 0);
    }
    newLine = newLine.slice(start, end);
    if (start > 0) {
        newLine = "'{snip} " + newLine;
    }
    if (end < ll) {
        newLine += ' {snip}';
    }
    return newLine;
}
/**
 * Join values in array
 * @param input array of values to be joined together
 * @param delimiter string to be placed in-between values
 * @returns Joined values
 */
function safeJoin(input, delimiter) {
    if (!Array.isArray(input)) {
        return '';
    }
    var output = [];
    // tslint:disable-next-line:prefer-for-of
    for (var i = 0; i < input.length; i++) {
        var value = input[i];
        try {
            output.push(String(value));
        }
        catch (e) {
            output.push('[value cannot be serialized]');
        }
    }
    return output.join(delimiter);
}
/**
 * Checks if the value matches a regex or includes the string
 * @param value The string value to be checked against
 * @param pattern Either a regex or a string that must be contained in value
 */
function isMatchingPattern(value, pattern) {
    if (!Object(_is__WEBPACK_IMPORTED_MODULE_0__["isString"])(value)) {
        return false;
    }
    if (Object(_is__WEBPACK_IMPORTED_MODULE_0__["isRegExp"])(pattern)) {
        return pattern.test(value);
    }
    if (typeof pattern === 'string') {
        return value.indexOf(pattern) !== -1;
    }
    return false;
}
//# sourceMappingURL=string.js.map

/***/ }),

/***/ "./node_modules/@sentry/utils/esm/supports.js":
/*!****************************************************!*\
  !*** ./node_modules/@sentry/utils/esm/supports.js ***!
  \****************************************************/
/*! exports provided: supportsErrorEvent, supportsDOMError, supportsDOMException, supportsFetch, supportsNativeFetch, supportsReportingObserver, supportsReferrerPolicy, supportsHistory */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "supportsErrorEvent", function() { return supportsErrorEvent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "supportsDOMError", function() { return supportsDOMError; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "supportsDOMException", function() { return supportsDOMException; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "supportsFetch", function() { return supportsFetch; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "supportsNativeFetch", function() { return supportsNativeFetch; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "supportsReportingObserver", function() { return supportsReportingObserver; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "supportsReferrerPolicy", function() { return supportsReferrerPolicy; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "supportsHistory", function() { return supportsHistory; });
/* harmony import */ var _logger__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./logger */ "./node_modules/@sentry/utils/esm/logger.js");
/* harmony import */ var _misc__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./misc */ "./node_modules/@sentry/utils/esm/misc.js");


/**
 * Tells whether current environment supports ErrorEvent objects
 * {@link supportsErrorEvent}.
 *
 * @returns Answer to the given question.
 */
function supportsErrorEvent() {
    try {
        // tslint:disable:no-unused-expression
        new ErrorEvent('');
        return true;
    }
    catch (e) {
        return false;
    }
}
/**
 * Tells whether current environment supports DOMError objects
 * {@link supportsDOMError}.
 *
 * @returns Answer to the given question.
 */
function supportsDOMError() {
    try {
        // It really needs 1 argument, not 0.
        // Chrome: VM89:1 Uncaught TypeError: Failed to construct 'DOMError':
        // 1 argument required, but only 0 present.
        // @ts-ignore
        // tslint:disable:no-unused-expression
        new DOMError('');
        return true;
    }
    catch (e) {
        return false;
    }
}
/**
 * Tells whether current environment supports DOMException objects
 * {@link supportsDOMException}.
 *
 * @returns Answer to the given question.
 */
function supportsDOMException() {
    try {
        // tslint:disable:no-unused-expression
        new DOMException('');
        return true;
    }
    catch (e) {
        return false;
    }
}
/**
 * Tells whether current environment supports Fetch API
 * {@link supportsFetch}.
 *
 * @returns Answer to the given question.
 */
function supportsFetch() {
    if (!('fetch' in Object(_misc__WEBPACK_IMPORTED_MODULE_1__["getGlobalObject"])())) {
        return false;
    }
    try {
        // tslint:disable-next-line:no-unused-expression
        new Headers();
        // tslint:disable-next-line:no-unused-expression
        new Request('');
        // tslint:disable-next-line:no-unused-expression
        new Response();
        return true;
    }
    catch (e) {
        return false;
    }
}
/**
 * isNativeFetch checks if the given function is a native implementation of fetch()
 */
function isNativeFetch(func) {
    return func && /^function fetch\(\)\s+\{\s+\[native code\]\s+\}$/.test(func.toString());
}
/**
 * Tells whether current environment supports Fetch API natively
 * {@link supportsNativeFetch}.
 *
 * @returns true if `window.fetch` is natively implemented, false otherwise
 */
function supportsNativeFetch() {
    if (!supportsFetch()) {
        return false;
    }
    var global = Object(_misc__WEBPACK_IMPORTED_MODULE_1__["getGlobalObject"])();
    // Fast path to avoid DOM I/O
    // tslint:disable-next-line:no-unbound-method
    if (isNativeFetch(global.fetch)) {
        return true;
    }
    // window.fetch is implemented, but is polyfilled or already wrapped (e.g: by a chrome extension)
    // so create a "pure" iframe to see if that has native fetch
    var result = false;
    var doc = global.document;
    // tslint:disable-next-line:no-unbound-method deprecation
    if (doc && typeof doc.createElement === "function") {
        try {
            var sandbox = doc.createElement('iframe');
            sandbox.hidden = true;
            doc.head.appendChild(sandbox);
            if (sandbox.contentWindow && sandbox.contentWindow.fetch) {
                // tslint:disable-next-line:no-unbound-method
                result = isNativeFetch(sandbox.contentWindow.fetch);
            }
            doc.head.removeChild(sandbox);
        }
        catch (err) {
            _logger__WEBPACK_IMPORTED_MODULE_0__["logger"].warn('Could not create sandbox iframe for pure fetch check, bailing to window.fetch: ', err);
        }
    }
    return result;
}
/**
 * Tells whether current environment supports ReportingObserver API
 * {@link supportsReportingObserver}.
 *
 * @returns Answer to the given question.
 */
function supportsReportingObserver() {
    // tslint:disable-next-line: no-unsafe-any
    return 'ReportingObserver' in Object(_misc__WEBPACK_IMPORTED_MODULE_1__["getGlobalObject"])();
}
/**
 * Tells whether current environment supports Referrer Policy API
 * {@link supportsReferrerPolicy}.
 *
 * @returns Answer to the given question.
 */
function supportsReferrerPolicy() {
    // Despite all stars in the sky saying that Edge supports old draft syntax, aka 'never', 'always', 'origin' and 'default
    // https://caniuse.com/#feat=referrer-policy
    // It doesn't. And it throw exception instead of ignoring this parameter...
    // REF: https://github.com/getsentry/raven-js/issues/1233
    if (!supportsFetch()) {
        return false;
    }
    try {
        // tslint:disable:no-unused-expression
        new Request('_', {
            referrerPolicy: 'origin',
        });
        return true;
    }
    catch (e) {
        return false;
    }
}
/**
 * Tells whether current environment supports History API
 * {@link supportsHistory}.
 *
 * @returns Answer to the given question.
 */
function supportsHistory() {
    // NOTE: in Chrome App environment, touching history.pushState, *even inside
    //       a try/catch block*, will cause Chrome to output an error to console.error
    // borrowed from: https://github.com/angular/angular.js/pull/13945/files
    var global = Object(_misc__WEBPACK_IMPORTED_MODULE_1__["getGlobalObject"])();
    var chrome = global.chrome;
    // tslint:disable-next-line:no-unsafe-any
    var isChromePackagedApp = chrome && chrome.app && chrome.app.runtime;
    var hasHistoryApi = 'history' in global && !!global.history.pushState && !!global.history.replaceState;
    return !isChromePackagedApp && hasHistoryApi;
}
//# sourceMappingURL=supports.js.map

/***/ }),

/***/ "./node_modules/@sentry/utils/esm/syncpromise.js":
/*!*******************************************************!*\
  !*** ./node_modules/@sentry/utils/esm/syncpromise.js ***!
  \*******************************************************/
/*! exports provided: SyncPromise */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SyncPromise", function() { return SyncPromise; });
/* harmony import */ var _is__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./is */ "./node_modules/@sentry/utils/esm/is.js");

/** SyncPromise internal states */
var States;
(function (States) {
    /** Pending */
    States["PENDING"] = "PENDING";
    /** Resolved / OK */
    States["RESOLVED"] = "RESOLVED";
    /** Rejected / Error */
    States["REJECTED"] = "REJECTED";
})(States || (States = {}));
/**
 * Thenable class that behaves like a Promise and follows it's interface
 * but is not async internally
 */
var SyncPromise = /** @class */ (function () {
    function SyncPromise(executor) {
        var _this = this;
        this._state = States.PENDING;
        this._handlers = [];
        /** JSDoc */
        this._resolve = function (value) {
            _this._setResult(States.RESOLVED, value);
        };
        /** JSDoc */
        this._reject = function (reason) {
            _this._setResult(States.REJECTED, reason);
        };
        /** JSDoc */
        this._setResult = function (state, value) {
            if (_this._state !== States.PENDING) {
                return;
            }
            if (Object(_is__WEBPACK_IMPORTED_MODULE_0__["isThenable"])(value)) {
                value.then(_this._resolve, _this._reject);
                return;
            }
            _this._state = state;
            _this._value = value;
            _this._executeHandlers();
        };
        // TODO: FIXME
        /** JSDoc */
        this._attachHandler = function (handler) {
            _this._handlers = _this._handlers.concat(handler);
            _this._executeHandlers();
        };
        /** JSDoc */
        this._executeHandlers = function () {
            if (_this._state === States.PENDING) {
                return;
            }
            var cachedHandlers = _this._handlers.slice();
            _this._handlers = [];
            cachedHandlers.forEach(function (handler) {
                if (handler.done) {
                    return;
                }
                if (_this._state === States.RESOLVED) {
                    if (handler.onfulfilled) {
                        handler.onfulfilled(_this._value);
                    }
                }
                if (_this._state === States.REJECTED) {
                    if (handler.onrejected) {
                        handler.onrejected(_this._value);
                    }
                }
                handler.done = true;
            });
        };
        try {
            executor(this._resolve, this._reject);
        }
        catch (e) {
            this._reject(e);
        }
    }
    /** JSDoc */
    SyncPromise.prototype.toString = function () {
        return '[object SyncPromise]';
    };
    /** JSDoc */
    SyncPromise.resolve = function (value) {
        return new SyncPromise(function (resolve) {
            resolve(value);
        });
    };
    /** JSDoc */
    SyncPromise.reject = function (reason) {
        return new SyncPromise(function (_, reject) {
            reject(reason);
        });
    };
    /** JSDoc */
    SyncPromise.all = function (collection) {
        return new SyncPromise(function (resolve, reject) {
            if (!Array.isArray(collection)) {
                reject(new TypeError("Promise.all requires an array as input."));
                return;
            }
            if (collection.length === 0) {
                resolve([]);
                return;
            }
            var counter = collection.length;
            var resolvedCollection = [];
            collection.forEach(function (item, index) {
                SyncPromise.resolve(item)
                    .then(function (value) {
                    resolvedCollection[index] = value;
                    counter -= 1;
                    if (counter !== 0) {
                        return;
                    }
                    resolve(resolvedCollection);
                })
                    .then(null, reject);
            });
        });
    };
    /** JSDoc */
    SyncPromise.prototype.then = function (onfulfilled, onrejected) {
        var _this = this;
        return new SyncPromise(function (resolve, reject) {
            _this._attachHandler({
                done: false,
                onfulfilled: function (result) {
                    if (!onfulfilled) {
                        // TODO: ¯\_(ツ)_/¯
                        // TODO: FIXME
                        resolve(result);
                        return;
                    }
                    try {
                        resolve(onfulfilled(result));
                        return;
                    }
                    catch (e) {
                        reject(e);
                        return;
                    }
                },
                onrejected: function (reason) {
                    if (!onrejected) {
                        reject(reason);
                        return;
                    }
                    try {
                        resolve(onrejected(reason));
                        return;
                    }
                    catch (e) {
                        reject(e);
                        return;
                    }
                },
            });
        });
    };
    /** JSDoc */
    SyncPromise.prototype.catch = function (onrejected) {
        return this.then(function (val) { return val; }, onrejected);
    };
    /** JSDoc */
    SyncPromise.prototype.finally = function (onfinally) {
        var _this = this;
        return new SyncPromise(function (resolve, reject) {
            var val;
            var isRejected;
            return _this.then(function (value) {
                isRejected = false;
                val = value;
                if (onfinally) {
                    onfinally();
                }
            }, function (reason) {
                isRejected = true;
                val = reason;
                if (onfinally) {
                    onfinally();
                }
            }).then(function () {
                if (isRejected) {
                    reject(val);
                    return;
                }
                resolve(val);
            });
        });
    };
    return SyncPromise;
}());

//# sourceMappingURL=syncpromise.js.map

/***/ }),

/***/ "./node_modules/events/events.js":
/*!***************************************!*\
  !*** ./node_modules/events/events.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.



var R = typeof Reflect === 'object' ? Reflect : null
var ReflectApply = R && typeof R.apply === 'function'
  ? R.apply
  : function ReflectApply(target, receiver, args) {
    return Function.prototype.apply.call(target, receiver, args);
  }

var ReflectOwnKeys
if (R && typeof R.ownKeys === 'function') {
  ReflectOwnKeys = R.ownKeys
} else if (Object.getOwnPropertySymbols) {
  ReflectOwnKeys = function ReflectOwnKeys(target) {
    return Object.getOwnPropertyNames(target)
      .concat(Object.getOwnPropertySymbols(target));
  };
} else {
  ReflectOwnKeys = function ReflectOwnKeys(target) {
    return Object.getOwnPropertyNames(target);
  };
}

function ProcessEmitWarning(warning) {
  if (console && console.warn) console.warn(warning);
}

var NumberIsNaN = Number.isNaN || function NumberIsNaN(value) {
  return value !== value;
}

function EventEmitter() {
  EventEmitter.init.call(this);
}
module.exports = EventEmitter;

// Backwards-compat with node 0.10.x
EventEmitter.EventEmitter = EventEmitter;

EventEmitter.prototype._events = undefined;
EventEmitter.prototype._eventsCount = 0;
EventEmitter.prototype._maxListeners = undefined;

// By default EventEmitters will print a warning if more than 10 listeners are
// added to it. This is a useful default which helps finding memory leaks.
var defaultMaxListeners = 10;

function checkListener(listener) {
  if (typeof listener !== 'function') {
    throw new TypeError('The "listener" argument must be of type Function. Received type ' + typeof listener);
  }
}

Object.defineProperty(EventEmitter, 'defaultMaxListeners', {
  enumerable: true,
  get: function() {
    return defaultMaxListeners;
  },
  set: function(arg) {
    if (typeof arg !== 'number' || arg < 0 || NumberIsNaN(arg)) {
      throw new RangeError('The value of "defaultMaxListeners" is out of range. It must be a non-negative number. Received ' + arg + '.');
    }
    defaultMaxListeners = arg;
  }
});

EventEmitter.init = function() {

  if (this._events === undefined ||
      this._events === Object.getPrototypeOf(this)._events) {
    this._events = Object.create(null);
    this._eventsCount = 0;
  }

  this._maxListeners = this._maxListeners || undefined;
};

// Obviously not all Emitters should be limited to 10. This function allows
// that to be increased. Set to zero for unlimited.
EventEmitter.prototype.setMaxListeners = function setMaxListeners(n) {
  if (typeof n !== 'number' || n < 0 || NumberIsNaN(n)) {
    throw new RangeError('The value of "n" is out of range. It must be a non-negative number. Received ' + n + '.');
  }
  this._maxListeners = n;
  return this;
};

function _getMaxListeners(that) {
  if (that._maxListeners === undefined)
    return EventEmitter.defaultMaxListeners;
  return that._maxListeners;
}

EventEmitter.prototype.getMaxListeners = function getMaxListeners() {
  return _getMaxListeners(this);
};

EventEmitter.prototype.emit = function emit(type) {
  var args = [];
  for (var i = 1; i < arguments.length; i++) args.push(arguments[i]);
  var doError = (type === 'error');

  var events = this._events;
  if (events !== undefined)
    doError = (doError && events.error === undefined);
  else if (!doError)
    return false;

  // If there is no 'error' event listener then throw.
  if (doError) {
    var er;
    if (args.length > 0)
      er = args[0];
    if (er instanceof Error) {
      // Note: The comments on the `throw` lines are intentional, they show
      // up in Node's output if this results in an unhandled exception.
      throw er; // Unhandled 'error' event
    }
    // At least give some kind of context to the user
    var err = new Error('Unhandled error.' + (er ? ' (' + er.message + ')' : ''));
    err.context = er;
    throw err; // Unhandled 'error' event
  }

  var handler = events[type];

  if (handler === undefined)
    return false;

  if (typeof handler === 'function') {
    ReflectApply(handler, this, args);
  } else {
    var len = handler.length;
    var listeners = arrayClone(handler, len);
    for (var i = 0; i < len; ++i)
      ReflectApply(listeners[i], this, args);
  }

  return true;
};

function _addListener(target, type, listener, prepend) {
  var m;
  var events;
  var existing;

  checkListener(listener);

  events = target._events;
  if (events === undefined) {
    events = target._events = Object.create(null);
    target._eventsCount = 0;
  } else {
    // To avoid recursion in the case that type === "newListener"! Before
    // adding it to the listeners, first emit "newListener".
    if (events.newListener !== undefined) {
      target.emit('newListener', type,
                  listener.listener ? listener.listener : listener);

      // Re-assign `events` because a newListener handler could have caused the
      // this._events to be assigned to a new object
      events = target._events;
    }
    existing = events[type];
  }

  if (existing === undefined) {
    // Optimize the case of one listener. Don't need the extra array object.
    existing = events[type] = listener;
    ++target._eventsCount;
  } else {
    if (typeof existing === 'function') {
      // Adding the second element, need to change to array.
      existing = events[type] =
        prepend ? [listener, existing] : [existing, listener];
      // If we've already got an array, just append.
    } else if (prepend) {
      existing.unshift(listener);
    } else {
      existing.push(listener);
    }

    // Check for listener leak
    m = _getMaxListeners(target);
    if (m > 0 && existing.length > m && !existing.warned) {
      existing.warned = true;
      // No error code for this since it is a Warning
      // eslint-disable-next-line no-restricted-syntax
      var w = new Error('Possible EventEmitter memory leak detected. ' +
                          existing.length + ' ' + String(type) + ' listeners ' +
                          'added. Use emitter.setMaxListeners() to ' +
                          'increase limit');
      w.name = 'MaxListenersExceededWarning';
      w.emitter = target;
      w.type = type;
      w.count = existing.length;
      ProcessEmitWarning(w);
    }
  }

  return target;
}

EventEmitter.prototype.addListener = function addListener(type, listener) {
  return _addListener(this, type, listener, false);
};

EventEmitter.prototype.on = EventEmitter.prototype.addListener;

EventEmitter.prototype.prependListener =
    function prependListener(type, listener) {
      return _addListener(this, type, listener, true);
    };

function onceWrapper() {
  if (!this.fired) {
    this.target.removeListener(this.type, this.wrapFn);
    this.fired = true;
    if (arguments.length === 0)
      return this.listener.call(this.target);
    return this.listener.apply(this.target, arguments);
  }
}

function _onceWrap(target, type, listener) {
  var state = { fired: false, wrapFn: undefined, target: target, type: type, listener: listener };
  var wrapped = onceWrapper.bind(state);
  wrapped.listener = listener;
  state.wrapFn = wrapped;
  return wrapped;
}

EventEmitter.prototype.once = function once(type, listener) {
  checkListener(listener);
  this.on(type, _onceWrap(this, type, listener));
  return this;
};

EventEmitter.prototype.prependOnceListener =
    function prependOnceListener(type, listener) {
      checkListener(listener);
      this.prependListener(type, _onceWrap(this, type, listener));
      return this;
    };

// Emits a 'removeListener' event if and only if the listener was removed.
EventEmitter.prototype.removeListener =
    function removeListener(type, listener) {
      var list, events, position, i, originalListener;

      checkListener(listener);

      events = this._events;
      if (events === undefined)
        return this;

      list = events[type];
      if (list === undefined)
        return this;

      if (list === listener || list.listener === listener) {
        if (--this._eventsCount === 0)
          this._events = Object.create(null);
        else {
          delete events[type];
          if (events.removeListener)
            this.emit('removeListener', type, list.listener || listener);
        }
      } else if (typeof list !== 'function') {
        position = -1;

        for (i = list.length - 1; i >= 0; i--) {
          if (list[i] === listener || list[i].listener === listener) {
            originalListener = list[i].listener;
            position = i;
            break;
          }
        }

        if (position < 0)
          return this;

        if (position === 0)
          list.shift();
        else {
          spliceOne(list, position);
        }

        if (list.length === 1)
          events[type] = list[0];

        if (events.removeListener !== undefined)
          this.emit('removeListener', type, originalListener || listener);
      }

      return this;
    };

EventEmitter.prototype.off = EventEmitter.prototype.removeListener;

EventEmitter.prototype.removeAllListeners =
    function removeAllListeners(type) {
      var listeners, events, i;

      events = this._events;
      if (events === undefined)
        return this;

      // not listening for removeListener, no need to emit
      if (events.removeListener === undefined) {
        if (arguments.length === 0) {
          this._events = Object.create(null);
          this._eventsCount = 0;
        } else if (events[type] !== undefined) {
          if (--this._eventsCount === 0)
            this._events = Object.create(null);
          else
            delete events[type];
        }
        return this;
      }

      // emit removeListener for all listeners on all events
      if (arguments.length === 0) {
        var keys = Object.keys(events);
        var key;
        for (i = 0; i < keys.length; ++i) {
          key = keys[i];
          if (key === 'removeListener') continue;
          this.removeAllListeners(key);
        }
        this.removeAllListeners('removeListener');
        this._events = Object.create(null);
        this._eventsCount = 0;
        return this;
      }

      listeners = events[type];

      if (typeof listeners === 'function') {
        this.removeListener(type, listeners);
      } else if (listeners !== undefined) {
        // LIFO order
        for (i = listeners.length - 1; i >= 0; i--) {
          this.removeListener(type, listeners[i]);
        }
      }

      return this;
    };

function _listeners(target, type, unwrap) {
  var events = target._events;

  if (events === undefined)
    return [];

  var evlistener = events[type];
  if (evlistener === undefined)
    return [];

  if (typeof evlistener === 'function')
    return unwrap ? [evlistener.listener || evlistener] : [evlistener];

  return unwrap ?
    unwrapListeners(evlistener) : arrayClone(evlistener, evlistener.length);
}

EventEmitter.prototype.listeners = function listeners(type) {
  return _listeners(this, type, true);
};

EventEmitter.prototype.rawListeners = function rawListeners(type) {
  return _listeners(this, type, false);
};

EventEmitter.listenerCount = function(emitter, type) {
  if (typeof emitter.listenerCount === 'function') {
    return emitter.listenerCount(type);
  } else {
    return listenerCount.call(emitter, type);
  }
};

EventEmitter.prototype.listenerCount = listenerCount;
function listenerCount(type) {
  var events = this._events;

  if (events !== undefined) {
    var evlistener = events[type];

    if (typeof evlistener === 'function') {
      return 1;
    } else if (evlistener !== undefined) {
      return evlistener.length;
    }
  }

  return 0;
}

EventEmitter.prototype.eventNames = function eventNames() {
  return this._eventsCount > 0 ? ReflectOwnKeys(this._events) : [];
};

function arrayClone(arr, n) {
  var copy = new Array(n);
  for (var i = 0; i < n; ++i)
    copy[i] = arr[i];
  return copy;
}

function spliceOne(list, index) {
  for (; index + 1 < list.length; index++)
    list[index] = list[index + 1];
  list.pop();
}

function unwrapListeners(arr) {
  var ret = new Array(arr.length);
  for (var i = 0; i < ret.length; ++i) {
    ret[i] = arr[i].listener || arr[i];
  }
  return ret;
}


/***/ }),

/***/ "./node_modules/process/browser.js":
/*!*****************************************!*\
  !*** ./node_modules/process/browser.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// shim for using process in browser
var process = module.exports = {};

// cached from whatever global is present so that test runners that stub it
// don't break things.  But we need to wrap it in a try catch in case it is
// wrapped in strict mode code which doesn't define any globals.  It's inside a
// function because try/catches deoptimize in certain engines.

var cachedSetTimeout;
var cachedClearTimeout;

function defaultSetTimout() {
    throw new Error('setTimeout has not been defined');
}
function defaultClearTimeout () {
    throw new Error('clearTimeout has not been defined');
}
(function () {
    try {
        if (typeof setTimeout === 'function') {
            cachedSetTimeout = setTimeout;
        } else {
            cachedSetTimeout = defaultSetTimout;
        }
    } catch (e) {
        cachedSetTimeout = defaultSetTimout;
    }
    try {
        if (typeof clearTimeout === 'function') {
            cachedClearTimeout = clearTimeout;
        } else {
            cachedClearTimeout = defaultClearTimeout;
        }
    } catch (e) {
        cachedClearTimeout = defaultClearTimeout;
    }
} ())
function runTimeout(fun) {
    if (cachedSetTimeout === setTimeout) {
        //normal enviroments in sane situations
        return setTimeout(fun, 0);
    }
    // if setTimeout wasn't available but was latter defined
    if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
        cachedSetTimeout = setTimeout;
        return setTimeout(fun, 0);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedSetTimeout(fun, 0);
    } catch(e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally
            return cachedSetTimeout.call(null, fun, 0);
        } catch(e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error
            return cachedSetTimeout.call(this, fun, 0);
        }
    }


}
function runClearTimeout(marker) {
    if (cachedClearTimeout === clearTimeout) {
        //normal enviroments in sane situations
        return clearTimeout(marker);
    }
    // if clearTimeout wasn't available but was latter defined
    if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
        cachedClearTimeout = clearTimeout;
        return clearTimeout(marker);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedClearTimeout(marker);
    } catch (e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally
            return cachedClearTimeout.call(null, marker);
        } catch (e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.
            // Some versions of I.E. have different rules for clearTimeout vs setTimeout
            return cachedClearTimeout.call(this, marker);
        }
    }



}
var queue = [];
var draining = false;
var currentQueue;
var queueIndex = -1;

function cleanUpNextTick() {
    if (!draining || !currentQueue) {
        return;
    }
    draining = false;
    if (currentQueue.length) {
        queue = currentQueue.concat(queue);
    } else {
        queueIndex = -1;
    }
    if (queue.length) {
        drainQueue();
    }
}

function drainQueue() {
    if (draining) {
        return;
    }
    var timeout = runTimeout(cleanUpNextTick);
    draining = true;

    var len = queue.length;
    while(len) {
        currentQueue = queue;
        queue = [];
        while (++queueIndex < len) {
            if (currentQueue) {
                currentQueue[queueIndex].run();
            }
        }
        queueIndex = -1;
        len = queue.length;
    }
    currentQueue = null;
    draining = false;
    runClearTimeout(timeout);
}

process.nextTick = function (fun) {
    var args = new Array(arguments.length - 1);
    if (arguments.length > 1) {
        for (var i = 1; i < arguments.length; i++) {
            args[i - 1] = arguments[i];
        }
    }
    queue.push(new Item(fun, args));
    if (queue.length === 1 && !draining) {
        runTimeout(drainQueue);
    }
};

// v8 likes predictible objects
function Item(fun, array) {
    this.fun = fun;
    this.array = array;
}
Item.prototype.run = function () {
    this.fun.apply(null, this.array);
};
process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];
process.version = ''; // empty string to avoid regexp issues
process.versions = {};

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;
process.prependListener = noop;
process.prependOnceListener = noop;

process.listeners = function (name) { return [] }

process.binding = function (name) {
    throw new Error('process.binding is not supported');
};

process.cwd = function () { return '/' };
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};
process.umask = function() { return 0; };


/***/ }),

/***/ "./node_modules/regenerator-runtime/runtime.js":
/*!*****************************************************!*\
  !*** ./node_modules/regenerator-runtime/runtime.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/**
 * Copyright (c) 2014-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

var runtime = (function (exports) {
  "use strict";

  var Op = Object.prototype;
  var hasOwn = Op.hasOwnProperty;
  var undefined; // More compressible than void 0.
  var $Symbol = typeof Symbol === "function" ? Symbol : {};
  var iteratorSymbol = $Symbol.iterator || "@@iterator";
  var asyncIteratorSymbol = $Symbol.asyncIterator || "@@asyncIterator";
  var toStringTagSymbol = $Symbol.toStringTag || "@@toStringTag";

  function wrap(innerFn, outerFn, self, tryLocsList) {
    // If outerFn provided and outerFn.prototype is a Generator, then outerFn.prototype instanceof Generator.
    var protoGenerator = outerFn && outerFn.prototype instanceof Generator ? outerFn : Generator;
    var generator = Object.create(protoGenerator.prototype);
    var context = new Context(tryLocsList || []);

    // The ._invoke method unifies the implementations of the .next,
    // .throw, and .return methods.
    generator._invoke = makeInvokeMethod(innerFn, self, context);

    return generator;
  }
  exports.wrap = wrap;

  // Try/catch helper to minimize deoptimizations. Returns a completion
  // record like context.tryEntries[i].completion. This interface could
  // have been (and was previously) designed to take a closure to be
  // invoked without arguments, but in all the cases we care about we
  // already have an existing method we want to call, so there's no need
  // to create a new function object. We can even get away with assuming
  // the method takes exactly one argument, since that happens to be true
  // in every case, so we don't have to touch the arguments object. The
  // only additional allocation required is the completion record, which
  // has a stable shape and so hopefully should be cheap to allocate.
  function tryCatch(fn, obj, arg) {
    try {
      return { type: "normal", arg: fn.call(obj, arg) };
    } catch (err) {
      return { type: "throw", arg: err };
    }
  }

  var GenStateSuspendedStart = "suspendedStart";
  var GenStateSuspendedYield = "suspendedYield";
  var GenStateExecuting = "executing";
  var GenStateCompleted = "completed";

  // Returning this object from the innerFn has the same effect as
  // breaking out of the dispatch switch statement.
  var ContinueSentinel = {};

  // Dummy constructor functions that we use as the .constructor and
  // .constructor.prototype properties for functions that return Generator
  // objects. For full spec compliance, you may wish to configure your
  // minifier not to mangle the names of these two functions.
  function Generator() {}
  function GeneratorFunction() {}
  function GeneratorFunctionPrototype() {}

  // This is a polyfill for %IteratorPrototype% for environments that
  // don't natively support it.
  var IteratorPrototype = {};
  IteratorPrototype[iteratorSymbol] = function () {
    return this;
  };

  var getProto = Object.getPrototypeOf;
  var NativeIteratorPrototype = getProto && getProto(getProto(values([])));
  if (NativeIteratorPrototype &&
      NativeIteratorPrototype !== Op &&
      hasOwn.call(NativeIteratorPrototype, iteratorSymbol)) {
    // This environment has a native %IteratorPrototype%; use it instead
    // of the polyfill.
    IteratorPrototype = NativeIteratorPrototype;
  }

  var Gp = GeneratorFunctionPrototype.prototype =
    Generator.prototype = Object.create(IteratorPrototype);
  GeneratorFunction.prototype = Gp.constructor = GeneratorFunctionPrototype;
  GeneratorFunctionPrototype.constructor = GeneratorFunction;
  GeneratorFunctionPrototype[toStringTagSymbol] =
    GeneratorFunction.displayName = "GeneratorFunction";

  // Helper for defining the .next, .throw, and .return methods of the
  // Iterator interface in terms of a single ._invoke method.
  function defineIteratorMethods(prototype) {
    ["next", "throw", "return"].forEach(function(method) {
      prototype[method] = function(arg) {
        return this._invoke(method, arg);
      };
    });
  }

  exports.isGeneratorFunction = function(genFun) {
    var ctor = typeof genFun === "function" && genFun.constructor;
    return ctor
      ? ctor === GeneratorFunction ||
        // For the native GeneratorFunction constructor, the best we can
        // do is to check its .name property.
        (ctor.displayName || ctor.name) === "GeneratorFunction"
      : false;
  };

  exports.mark = function(genFun) {
    if (Object.setPrototypeOf) {
      Object.setPrototypeOf(genFun, GeneratorFunctionPrototype);
    } else {
      genFun.__proto__ = GeneratorFunctionPrototype;
      if (!(toStringTagSymbol in genFun)) {
        genFun[toStringTagSymbol] = "GeneratorFunction";
      }
    }
    genFun.prototype = Object.create(Gp);
    return genFun;
  };

  // Within the body of any async function, `await x` is transformed to
  // `yield regeneratorRuntime.awrap(x)`, so that the runtime can test
  // `hasOwn.call(value, "__await")` to determine if the yielded value is
  // meant to be awaited.
  exports.awrap = function(arg) {
    return { __await: arg };
  };

  function AsyncIterator(generator, PromiseImpl) {
    function invoke(method, arg, resolve, reject) {
      var record = tryCatch(generator[method], generator, arg);
      if (record.type === "throw") {
        reject(record.arg);
      } else {
        var result = record.arg;
        var value = result.value;
        if (value &&
            typeof value === "object" &&
            hasOwn.call(value, "__await")) {
          return PromiseImpl.resolve(value.__await).then(function(value) {
            invoke("next", value, resolve, reject);
          }, function(err) {
            invoke("throw", err, resolve, reject);
          });
        }

        return PromiseImpl.resolve(value).then(function(unwrapped) {
          // When a yielded Promise is resolved, its final value becomes
          // the .value of the Promise<{value,done}> result for the
          // current iteration.
          result.value = unwrapped;
          resolve(result);
        }, function(error) {
          // If a rejected Promise was yielded, throw the rejection back
          // into the async generator function so it can be handled there.
          return invoke("throw", error, resolve, reject);
        });
      }
    }

    var previousPromise;

    function enqueue(method, arg) {
      function callInvokeWithMethodAndArg() {
        return new PromiseImpl(function(resolve, reject) {
          invoke(method, arg, resolve, reject);
        });
      }

      return previousPromise =
        // If enqueue has been called before, then we want to wait until
        // all previous Promises have been resolved before calling invoke,
        // so that results are always delivered in the correct order. If
        // enqueue has not been called before, then it is important to
        // call invoke immediately, without waiting on a callback to fire,
        // so that the async generator function has the opportunity to do
        // any necessary setup in a predictable way. This predictability
        // is why the Promise constructor synchronously invokes its
        // executor callback, and why async functions synchronously
        // execute code before the first await. Since we implement simple
        // async functions in terms of async generators, it is especially
        // important to get this right, even though it requires care.
        previousPromise ? previousPromise.then(
          callInvokeWithMethodAndArg,
          // Avoid propagating failures to Promises returned by later
          // invocations of the iterator.
          callInvokeWithMethodAndArg
        ) : callInvokeWithMethodAndArg();
    }

    // Define the unified helper method that is used to implement .next,
    // .throw, and .return (see defineIteratorMethods).
    this._invoke = enqueue;
  }

  defineIteratorMethods(AsyncIterator.prototype);
  AsyncIterator.prototype[asyncIteratorSymbol] = function () {
    return this;
  };
  exports.AsyncIterator = AsyncIterator;

  // Note that simple async functions are implemented on top of
  // AsyncIterator objects; they just return a Promise for the value of
  // the final result produced by the iterator.
  exports.async = function(innerFn, outerFn, self, tryLocsList, PromiseImpl) {
    if (PromiseImpl === void 0) PromiseImpl = Promise;

    var iter = new AsyncIterator(
      wrap(innerFn, outerFn, self, tryLocsList),
      PromiseImpl
    );

    return exports.isGeneratorFunction(outerFn)
      ? iter // If outerFn is a generator, return the full iterator.
      : iter.next().then(function(result) {
          return result.done ? result.value : iter.next();
        });
  };

  function makeInvokeMethod(innerFn, self, context) {
    var state = GenStateSuspendedStart;

    return function invoke(method, arg) {
      if (state === GenStateExecuting) {
        throw new Error("Generator is already running");
      }

      if (state === GenStateCompleted) {
        if (method === "throw") {
          throw arg;
        }

        // Be forgiving, per 25.3.3.3.3 of the spec:
        // https://people.mozilla.org/~jorendorff/es6-draft.html#sec-generatorresume
        return doneResult();
      }

      context.method = method;
      context.arg = arg;

      while (true) {
        var delegate = context.delegate;
        if (delegate) {
          var delegateResult = maybeInvokeDelegate(delegate, context);
          if (delegateResult) {
            if (delegateResult === ContinueSentinel) continue;
            return delegateResult;
          }
        }

        if (context.method === "next") {
          // Setting context._sent for legacy support of Babel's
          // function.sent implementation.
          context.sent = context._sent = context.arg;

        } else if (context.method === "throw") {
          if (state === GenStateSuspendedStart) {
            state = GenStateCompleted;
            throw context.arg;
          }

          context.dispatchException(context.arg);

        } else if (context.method === "return") {
          context.abrupt("return", context.arg);
        }

        state = GenStateExecuting;

        var record = tryCatch(innerFn, self, context);
        if (record.type === "normal") {
          // If an exception is thrown from innerFn, we leave state ===
          // GenStateExecuting and loop back for another invocation.
          state = context.done
            ? GenStateCompleted
            : GenStateSuspendedYield;

          if (record.arg === ContinueSentinel) {
            continue;
          }

          return {
            value: record.arg,
            done: context.done
          };

        } else if (record.type === "throw") {
          state = GenStateCompleted;
          // Dispatch the exception by looping back around to the
          // context.dispatchException(context.arg) call above.
          context.method = "throw";
          context.arg = record.arg;
        }
      }
    };
  }

  // Call delegate.iterator[context.method](context.arg) and handle the
  // result, either by returning a { value, done } result from the
  // delegate iterator, or by modifying context.method and context.arg,
  // setting context.delegate to null, and returning the ContinueSentinel.
  function maybeInvokeDelegate(delegate, context) {
    var method = delegate.iterator[context.method];
    if (method === undefined) {
      // A .throw or .return when the delegate iterator has no .throw
      // method always terminates the yield* loop.
      context.delegate = null;

      if (context.method === "throw") {
        // Note: ["return"] must be used for ES3 parsing compatibility.
        if (delegate.iterator["return"]) {
          // If the delegate iterator has a return method, give it a
          // chance to clean up.
          context.method = "return";
          context.arg = undefined;
          maybeInvokeDelegate(delegate, context);

          if (context.method === "throw") {
            // If maybeInvokeDelegate(context) changed context.method from
            // "return" to "throw", let that override the TypeError below.
            return ContinueSentinel;
          }
        }

        context.method = "throw";
        context.arg = new TypeError(
          "The iterator does not provide a 'throw' method");
      }

      return ContinueSentinel;
    }

    var record = tryCatch(method, delegate.iterator, context.arg);

    if (record.type === "throw") {
      context.method = "throw";
      context.arg = record.arg;
      context.delegate = null;
      return ContinueSentinel;
    }

    var info = record.arg;

    if (! info) {
      context.method = "throw";
      context.arg = new TypeError("iterator result is not an object");
      context.delegate = null;
      return ContinueSentinel;
    }

    if (info.done) {
      // Assign the result of the finished delegate to the temporary
      // variable specified by delegate.resultName (see delegateYield).
      context[delegate.resultName] = info.value;

      // Resume execution at the desired location (see delegateYield).
      context.next = delegate.nextLoc;

      // If context.method was "throw" but the delegate handled the
      // exception, let the outer generator proceed normally. If
      // context.method was "next", forget context.arg since it has been
      // "consumed" by the delegate iterator. If context.method was
      // "return", allow the original .return call to continue in the
      // outer generator.
      if (context.method !== "return") {
        context.method = "next";
        context.arg = undefined;
      }

    } else {
      // Re-yield the result returned by the delegate method.
      return info;
    }

    // The delegate iterator is finished, so forget it and continue with
    // the outer generator.
    context.delegate = null;
    return ContinueSentinel;
  }

  // Define Generator.prototype.{next,throw,return} in terms of the
  // unified ._invoke helper method.
  defineIteratorMethods(Gp);

  Gp[toStringTagSymbol] = "Generator";

  // A Generator should always return itself as the iterator object when the
  // @@iterator function is called on it. Some browsers' implementations of the
  // iterator prototype chain incorrectly implement this, causing the Generator
  // object to not be returned from this call. This ensures that doesn't happen.
  // See https://github.com/facebook/regenerator/issues/274 for more details.
  Gp[iteratorSymbol] = function() {
    return this;
  };

  Gp.toString = function() {
    return "[object Generator]";
  };

  function pushTryEntry(locs) {
    var entry = { tryLoc: locs[0] };

    if (1 in locs) {
      entry.catchLoc = locs[1];
    }

    if (2 in locs) {
      entry.finallyLoc = locs[2];
      entry.afterLoc = locs[3];
    }

    this.tryEntries.push(entry);
  }

  function resetTryEntry(entry) {
    var record = entry.completion || {};
    record.type = "normal";
    delete record.arg;
    entry.completion = record;
  }

  function Context(tryLocsList) {
    // The root entry object (effectively a try statement without a catch
    // or a finally block) gives us a place to store values thrown from
    // locations where there is no enclosing try statement.
    this.tryEntries = [{ tryLoc: "root" }];
    tryLocsList.forEach(pushTryEntry, this);
    this.reset(true);
  }

  exports.keys = function(object) {
    var keys = [];
    for (var key in object) {
      keys.push(key);
    }
    keys.reverse();

    // Rather than returning an object with a next method, we keep
    // things simple and return the next function itself.
    return function next() {
      while (keys.length) {
        var key = keys.pop();
        if (key in object) {
          next.value = key;
          next.done = false;
          return next;
        }
      }

      // To avoid creating an additional object, we just hang the .value
      // and .done properties off the next function object itself. This
      // also ensures that the minifier will not anonymize the function.
      next.done = true;
      return next;
    };
  };

  function values(iterable) {
    if (iterable) {
      var iteratorMethod = iterable[iteratorSymbol];
      if (iteratorMethod) {
        return iteratorMethod.call(iterable);
      }

      if (typeof iterable.next === "function") {
        return iterable;
      }

      if (!isNaN(iterable.length)) {
        var i = -1, next = function next() {
          while (++i < iterable.length) {
            if (hasOwn.call(iterable, i)) {
              next.value = iterable[i];
              next.done = false;
              return next;
            }
          }

          next.value = undefined;
          next.done = true;

          return next;
        };

        return next.next = next;
      }
    }

    // Return an iterator with no values.
    return { next: doneResult };
  }
  exports.values = values;

  function doneResult() {
    return { value: undefined, done: true };
  }

  Context.prototype = {
    constructor: Context,

    reset: function(skipTempReset) {
      this.prev = 0;
      this.next = 0;
      // Resetting context._sent for legacy support of Babel's
      // function.sent implementation.
      this.sent = this._sent = undefined;
      this.done = false;
      this.delegate = null;

      this.method = "next";
      this.arg = undefined;

      this.tryEntries.forEach(resetTryEntry);

      if (!skipTempReset) {
        for (var name in this) {
          // Not sure about the optimal order of these conditions:
          if (name.charAt(0) === "t" &&
              hasOwn.call(this, name) &&
              !isNaN(+name.slice(1))) {
            this[name] = undefined;
          }
        }
      }
    },

    stop: function() {
      this.done = true;

      var rootEntry = this.tryEntries[0];
      var rootRecord = rootEntry.completion;
      if (rootRecord.type === "throw") {
        throw rootRecord.arg;
      }

      return this.rval;
    },

    dispatchException: function(exception) {
      if (this.done) {
        throw exception;
      }

      var context = this;
      function handle(loc, caught) {
        record.type = "throw";
        record.arg = exception;
        context.next = loc;

        if (caught) {
          // If the dispatched exception was caught by a catch block,
          // then let that catch block handle the exception normally.
          context.method = "next";
          context.arg = undefined;
        }

        return !! caught;
      }

      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        var record = entry.completion;

        if (entry.tryLoc === "root") {
          // Exception thrown outside of any try block that could handle
          // it, so set the completion value of the entire function to
          // throw the exception.
          return handle("end");
        }

        if (entry.tryLoc <= this.prev) {
          var hasCatch = hasOwn.call(entry, "catchLoc");
          var hasFinally = hasOwn.call(entry, "finallyLoc");

          if (hasCatch && hasFinally) {
            if (this.prev < entry.catchLoc) {
              return handle(entry.catchLoc, true);
            } else if (this.prev < entry.finallyLoc) {
              return handle(entry.finallyLoc);
            }

          } else if (hasCatch) {
            if (this.prev < entry.catchLoc) {
              return handle(entry.catchLoc, true);
            }

          } else if (hasFinally) {
            if (this.prev < entry.finallyLoc) {
              return handle(entry.finallyLoc);
            }

          } else {
            throw new Error("try statement without catch or finally");
          }
        }
      }
    },

    abrupt: function(type, arg) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.tryLoc <= this.prev &&
            hasOwn.call(entry, "finallyLoc") &&
            this.prev < entry.finallyLoc) {
          var finallyEntry = entry;
          break;
        }
      }

      if (finallyEntry &&
          (type === "break" ||
           type === "continue") &&
          finallyEntry.tryLoc <= arg &&
          arg <= finallyEntry.finallyLoc) {
        // Ignore the finally entry if control is not jumping to a
        // location outside the try/catch block.
        finallyEntry = null;
      }

      var record = finallyEntry ? finallyEntry.completion : {};
      record.type = type;
      record.arg = arg;

      if (finallyEntry) {
        this.method = "next";
        this.next = finallyEntry.finallyLoc;
        return ContinueSentinel;
      }

      return this.complete(record);
    },

    complete: function(record, afterLoc) {
      if (record.type === "throw") {
        throw record.arg;
      }

      if (record.type === "break" ||
          record.type === "continue") {
        this.next = record.arg;
      } else if (record.type === "return") {
        this.rval = this.arg = record.arg;
        this.method = "return";
        this.next = "end";
      } else if (record.type === "normal" && afterLoc) {
        this.next = afterLoc;
      }

      return ContinueSentinel;
    },

    finish: function(finallyLoc) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.finallyLoc === finallyLoc) {
          this.complete(entry.completion, entry.afterLoc);
          resetTryEntry(entry);
          return ContinueSentinel;
        }
      }
    },

    "catch": function(tryLoc) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.tryLoc === tryLoc) {
          var record = entry.completion;
          if (record.type === "throw") {
            var thrown = record.arg;
            resetTryEntry(entry);
          }
          return thrown;
        }
      }

      // The context.catch method must only be called with a location
      // argument that corresponds to a known catch block.
      throw new Error("illegal catch attempt");
    },

    delegateYield: function(iterable, resultName, nextLoc) {
      this.delegate = {
        iterator: values(iterable),
        resultName: resultName,
        nextLoc: nextLoc
      };

      if (this.method === "next") {
        // Deliberately forget the last sent value so that we don't
        // accidentally pass it on to the delegate.
        this.arg = undefined;
      }

      return ContinueSentinel;
    }
  };

  // Regardless of whether this script is executing as a CommonJS module
  // or not, return the runtime object so that we can declare the variable
  // regeneratorRuntime in the outer scope, which allows this module to be
  // injected easily by `bin/regenerator --include-runtime script.js`.
  return exports;

}(
  // If this script is executing as a CommonJS module, use module.exports
  // as the regeneratorRuntime namespace. Otherwise create a new empty
  // object. Either way, the resulting object will be used to initialize
  // the regeneratorRuntime variable at the top of this file.
   true ? module.exports : undefined
));

try {
  regeneratorRuntime = runtime;
} catch (accidentalStrictMode) {
  // This module should not be running in strict mode, so the above
  // assignment should always work unless something is misconfigured. Just
  // in case runtime.js accidentally runs in strict mode, we can escape
  // strict mode using a global Function call. This could conceivably fail
  // if a Content Security Policy forbids using Function, but in that case
  // the proper solution is to fix the accidental strict mode problem. If
  // you've misconfigured your bundler to force strict mode and applied a
  // CSP to forbid Function, and you're not willing to fix either of those
  // problems, please detail your unique predicament in a GitHub issue.
  Function("r", "regeneratorRuntime = r")(runtime);
}


/***/ }),

/***/ "./node_modules/tslib/tslib.es6.js":
/*!*****************************************!*\
  !*** ./node_modules/tslib/tslib.es6.js ***!
  \*****************************************/
/*! exports provided: __extends, __assign, __rest, __decorate, __param, __metadata, __awaiter, __generator, __exportStar, __values, __read, __spread, __spreadArrays, __await, __asyncGenerator, __asyncDelegator, __asyncValues, __makeTemplateObject, __importStar, __importDefault, __classPrivateFieldGet, __classPrivateFieldSet */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__extends", function() { return __extends; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__assign", function() { return __assign; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__rest", function() { return __rest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__decorate", function() { return __decorate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__param", function() { return __param; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__metadata", function() { return __metadata; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__awaiter", function() { return __awaiter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__generator", function() { return __generator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__exportStar", function() { return __exportStar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__values", function() { return __values; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__read", function() { return __read; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spread", function() { return __spread; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spreadArrays", function() { return __spreadArrays; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__await", function() { return __await; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncGenerator", function() { return __asyncGenerator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncDelegator", function() { return __asyncDelegator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncValues", function() { return __asyncValues; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__makeTemplateObject", function() { return __makeTemplateObject; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importStar", function() { return __importStar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importDefault", function() { return __importDefault; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__classPrivateFieldGet", function() { return __classPrivateFieldGet; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__classPrivateFieldSet", function() { return __classPrivateFieldSet; });
/*! *****************************************************************************
Copyright (c) Microsoft Corporation. All rights reserved.
Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at http://www.apache.org/licenses/LICENSE-2.0

THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
MERCHANTABLITY OR NON-INFRINGEMENT.

See the Apache Version 2.0 License for specific language governing permissions
and limitations under the License.
***************************************************************************** */
/* global Reflect, Promise */

var extendStatics = function(d, b) {
    extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return extendStatics(d, b);
};

function __extends(d, b) {
    extendStatics(d, b);
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
}

var __assign = function() {
    __assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
    }
    return __assign.apply(this, arguments);
}

function __rest(s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
}

function __decorate(decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
}

function __param(paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
}

function __metadata(metadataKey, metadataValue) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
}

function __awaiter(thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}

function __generator(thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
}

function __exportStar(m, exports) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}

function __values(o) {
    var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
    if (m) return m.call(o);
    if (o && typeof o.length === "number") return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
    throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
}

function __read(o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
}

function __spread() {
    for (var ar = [], i = 0; i < arguments.length; i++)
        ar = ar.concat(__read(arguments[i]));
    return ar;
}

function __spreadArrays() {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};

function __await(v) {
    return this instanceof __await ? (this.v = v, this) : new __await(v);
}

function __asyncGenerator(thisArg, _arguments, generator) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var g = generator.apply(thisArg, _arguments || []), i, q = [];
    return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
    function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
    function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
    function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
    function fulfill(value) { resume("next", value); }
    function reject(value) { resume("throw", value); }
    function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
}

function __asyncDelegator(o) {
    var i, p;
    return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
    function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
}

function __asyncValues(o) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var m = o[Symbol.asyncIterator], i;
    return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
    function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
    function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
}

function __makeTemplateObject(cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};

function __importStar(mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result.default = mod;
    return result;
}

function __importDefault(mod) {
    return (mod && mod.__esModule) ? mod : { default: mod };
}

function __classPrivateFieldGet(receiver, privateMap) {
    if (!privateMap.has(receiver)) {
        throw new TypeError("attempted to get private field on non-instance");
    }
    return privateMap.get(receiver);
}

function __classPrivateFieldSet(receiver, privateMap, value) {
    if (!privateMap.has(receiver)) {
        throw new TypeError("attempted to set private field on non-instance");
    }
    privateMap.set(receiver, value);
    return value;
}


/***/ }),

/***/ "./node_modules/webpack/buildin/global.js":
/*!***********************************!*\
  !*** (webpack)/buildin/global.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || new Function("return this")();
} catch (e) {
	// This works if the window reference is available
	if (typeof window === "object") g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),

/***/ "./node_modules/webpack/buildin/harmony-module.js":
/*!*******************************************!*\
  !*** (webpack)/buildin/harmony-module.js ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = function(originalModule) {
	if (!originalModule.webpackPolyfill) {
		var module = Object.create(originalModule);
		// module.parent = undefined by default
		if (!module.children) module.children = [];
		Object.defineProperty(module, "loaded", {
			enumerable: true,
			get: function() {
				return module.l;
			}
		});
		Object.defineProperty(module, "id", {
			enumerable: true,
			get: function() {
				return module.i;
			}
		});
		Object.defineProperty(module, "exports", {
			enumerable: true
		});
		module.webpackPolyfill = 1;
	}
	return module;
};


/***/ }),

/***/ "./source/modules/parser/html-helpers.js":
/*!***********************************************!*\
  !*** ./source/modules/parser/html-helpers.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../utils */ "./source/modules/utils.js");


var cardNumber = function cardNumber(str) {
  return str === null || str === void 0 ? void 0 : str.replace(/\s/g, '');
};

var image = function image(str) {
  return str === null || str === void 0 ? void 0 : str.replace('chrome-extension://', 'https://');
};

var quantity = function quantity(str) {
  return parseInt(str, 10);
};

var numericSku = function numericSku(str) {
  return ["".concat(str).match(/[0-9]{4,}/)].join('');
};

/* harmony default export */ __webpack_exports__["default"] = ({
  image: image,
  quantity: quantity,
  numericSku: numericSku,
  cardNumber: cardNumber,
  price: _utils__WEBPACK_IMPORTED_MODULE_0__["parsePrice"],
  total: _utils__WEBPACK_IMPORTED_MODULE_0__["parsePrice"],
  currency: _utils__WEBPACK_IMPORTED_MODULE_0__["parseCurrency"],
  totalCurrency: _utils__WEBPACK_IMPORTED_MODULE_0__["parseCurrency"]
});

/***/ }),

/***/ "./source/modules/parser/index.js":
/*!****************************************!*\
  !*** ./source/modules/parser/index.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Parser; });
/* harmony import */ var _babel_runtime_helpers_toConsumableArray__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/toConsumableArray */ "./node_modules/@babel/runtime/helpers/toConsumableArray.js");
/* harmony import */ var _babel_runtime_helpers_toConsumableArray__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_toConsumableArray__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/slicedToArray */ "./node_modules/@babel/runtime/helpers/slicedToArray.js");
/* harmony import */ var _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime/helpers/defineProperty */ "./node_modules/@babel/runtime/helpers/defineProperty.js");
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime/helpers/asyncToGenerator */ "./node_modules/@babel/runtime/helpers/asyncToGenerator.js");
/* harmony import */ var _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ "./node_modules/@babel/runtime/helpers/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @babel/runtime/helpers/createClass */ "./node_modules/@babel/runtime/helpers/createClass.js");
/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @babel/runtime/helpers/inherits */ "./node_modules/@babel/runtime/helpers/inherits.js");
/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @babel/runtime/helpers/possibleConstructorReturn */ "./node_modules/@babel/runtime/helpers/possibleConstructorReturn.js");
/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @babel/runtime/helpers/getPrototypeOf */ "./node_modules/@babel/runtime/helpers/getPrototypeOf.js");
/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var events__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! events */ "./node_modules/events/events.js");
/* harmony import */ var events__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(events__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var _html_helpers__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./html-helpers */ "./source/modules/parser/html-helpers.js");
/* harmony import */ var _json_helpers__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./json-helpers */ "./source/modules/parser/json-helpers.js");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../utils */ "./source/modules/utils.js");











function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_3___default()(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_9___default()(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_9___default()(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_8___default()(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }






var Parser = /*#__PURE__*/function (_EventEmitter) {
  _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_7___default()(Parser, _EventEmitter);

  var _super = _createSuper(Parser);

  function Parser() {
    var _this;

    var config = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_5___default()(this, Parser);

    _this = _super.call(this);
    _this.config = config;

    _this.defineObservedProperty('parsedData');

    return _this;
  }

  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_6___default()(Parser, [{
    key: "fetchData",
    value: function () {
      var _fetchData = _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_4___default()( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function _callee(url, options) {
        var signal, response, _Object$entries$find, _Object$entries$find2, parseType, data;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                if (this.abortController) {
                  this.abortController.abort();
                }

                this.abortController = new AbortController();
                signal = this.abortController.signal;
                _context.next = 5;
                return fetch(url, _objectSpread({
                  signal: signal
                }, options));

              case 5:
                response = _context.sent;

                if (response.ok) {
                  _context.next = 8;
                  break;
                }

                throw new Error('Parser fetch failed.');

              case 8:
                _Object$entries$find = Object.entries({
                  json: 'application/json',
                  text: 'text'
                }).find(function (_ref) {
                  var _ref2 = _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_2___default()(_ref, 2),
                      value = _ref2[1];

                  return response.headers.get('content-type').includes(value);
                }), _Object$entries$find2 = _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_2___default()(_Object$entries$find, 1), parseType = _Object$entries$find2[0];
                _context.next = 11;
                return response[parseType]();

              case 11:
                data = _context.sent;

                if (!(parseType === 'text')) {
                  _context.next = 14;
                  break;
                }

                return _context.abrupt("return", Object(_utils__WEBPACK_IMPORTED_MODULE_13__["parseHtml"])(data));

              case 14:
                return _context.abrupt("return", data);

              case 15:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function fetchData(_x, _x2) {
        return _fetchData.apply(this, arguments);
      }

      return fetchData;
    }()
  }, {
    key: "parseItem",
    value: function parseItem(_ref3) {
      var name = _ref3.name,
          params = _ref3.params,
          _ref3$parent = _ref3.parent,
          parent = _ref3$parent === void 0 ? document : _ref3$parent;
      var isHtml = (parent.documentElement || parent) instanceof HTMLElement;
      var str = isHtml ? Object(_utils__WEBPACK_IMPORTED_MODULE_13__["parseNode"])(params, parent) : Object(_utils__WEBPACK_IMPORTED_MODULE_13__["get"])(parent, params.path);
      var jsonHelper = params.jsonHelper,
          htmlHelper = params.htmlHelper;
      var helperName = (isHtml ? htmlHelper : jsonHelper) || name;
      var helper = (isHtml ? _html_helpers__WEBPACK_IMPORTED_MODULE_11__["default"] : _json_helpers__WEBPACK_IMPORTED_MODULE_12__["default"])[helperName];
      return [name, helper ? helper(str) : str];
    }
  }, {
    key: "parseContainer",
    value: function parseContainer(_ref4) {
      var _this2 = this;

      var _ref4$parent = _ref4.parent,
          parent = _ref4$parent === void 0 ? document : _ref4$parent,
          _ref4$rules = _ref4.rules,
          rules = _ref4$rules === void 0 ? {} : _ref4$rules;
      var entries = Object.entries(rules).map(function (_ref5) {
        var _ref6 = _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_2___default()(_ref5, 2),
            name = _ref6[0],
            params = _ref6[1];

        return _this2.parseItem({
          name: name,
          params: params,
          parent: parent
        });
      });
      return Object.fromEntries(entries.map(function (_ref7) {
        var _rules$name;

        var _ref8 = _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_2___default()(_ref7, 2),
            name = _ref8[0],
            value = _ref8[1];

        var str = rules === null || rules === void 0 ? void 0 : (_rules$name = rules[name]) === null || _rules$name === void 0 ? void 0 : _rules$name.content;
        return [name, str ? Object(_utils__WEBPACK_IMPORTED_MODULE_13__["substitute"])({
          str: str,
          entries: entries
        }) : value];
      }));
    }
  }, {
    key: "parseItems",
    value: function parseItems() {
      var _this3 = this;

      var doc = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : document;
      var _this$config$items = this.config.items,
          items = _this$config$items === void 0 ? {} : _this$config$items;
      var isHtml = (doc.documentElement || doc) instanceof HTMLElement;
      return Object.fromEntries(Object.entries(items).map(function (_ref9) {
        var _ref10 = _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_2___default()(_ref9, 2),
            name = _ref10[0],
            _ref10$ = _ref10[1],
            config = _ref10$ === void 0 ? {} : _ref10$;

        var container = config.container,
            rules = config.rules;

        if (container) {
          var selector = container.selector,
              path = container.path;
          var parents = isHtml ? _babel_runtime_helpers_toConsumableArray__WEBPACK_IMPORTED_MODULE_0___default()(doc.querySelectorAll(selector)) : Object(_utils__WEBPACK_IMPORTED_MODULE_13__["get"])(doc, path) || [];
          return [name, parents.map(function (parent) {
            return _this3.parseContainer({
              parent: parent,
              rules: rules
            });
          })];
        }

        return _this3.parseItem({
          name: name,
          params: config,
          parent: doc
        });
      }));
    }
  }, {
    key: "process",
    value: function () {
      var _process = _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_4___default()( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.mark(function _callee2() {
        var data, parsedData;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_1___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.next = 2;
                return this.fetchData(this.config.url);

              case 2:
                data = _context2.sent;
                console.time('process');
                parsedData = this.parseItems(data);
                console.timeEnd('process');

                if (parsedData) {
                  this.parsedData = parsedData;
                }

                return _context2.abrupt("return", this.parsedData);

              case 8:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      function process() {
        return _process.apply(this, arguments);
      }

      return process;
    }()
  }, {
    key: "defineObservedProperty",
    value: function defineObservedProperty(name) {
      var _this4 = this;

      Object.defineProperty(this, name, {
        get: function get() {
          return _this4["_".concat(name)];
        },
        set: function set(value) {
          if (_this4["_".concat(name)] !== value) {
            _this4["_".concat(name)] = value;

            _this4.emit("update:".concat(name), value);
          }
        }
      });
    }
  }]);

  return Parser;
}(events__WEBPACK_IMPORTED_MODULE_10___default.a);



/***/ }),

/***/ "./source/modules/parser/json-helpers.js":
/*!***********************************************!*\
  !*** ./source/modules/parser/json-helpers.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../utils */ "./source/modules/utils.js");


var cardNumber = function cardNumber(str) {
  return str.replace(/\s/g, '');
};

var image = function image(str) {
  return str === null || str === void 0 ? void 0 : str.replace(/chrome-extension:\/\/|^\/\//, 'https://');
};

var quantity = function quantity(str) {
  return parseInt(str, 10);
};

var numericSku = function numericSku(str) {
  return ["".concat(str).match(/[0-9]{4,}/)].join('');
};

var currysPrice = function currysPrice(num) {
  return num / 100;
};

/* harmony default export */ __webpack_exports__["default"] = ({
  image: image,
  quantity: quantity,
  numericSku: numericSku,
  cardNumber: cardNumber,
  currysPrice: currysPrice,
  price: _utils__WEBPACK_IMPORTED_MODULE_0__["parsePrice"],
  total: _utils__WEBPACK_IMPORTED_MODULE_0__["parsePrice"],
  currency: _utils__WEBPACK_IMPORTED_MODULE_0__["parseCurrency"],
  totalCurrency: _utils__WEBPACK_IMPORTED_MODULE_0__["parseCurrency"]
});

/***/ }),

/***/ "./source/modules/sentry.js":
/*!**********************************!*\
  !*** ./source/modules/sentry.js ***!
  \**********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _sentry_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @sentry/browser */ "./node_modules/@sentry/browser/esm/index.js");


function initSentry() {
  var beforeInitFunction = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : function () {};
  beforeInitFunction(_sentry_browser__WEBPACK_IMPORTED_MODULE_0__);
  _sentry_browser__WEBPACK_IMPORTED_MODULE_0__["init"]({
    dsn: 'https://40f02373830d409f9a44237b2472821b@o411935.ingest.sentry.io/5287852'
  });
}

/* harmony default export */ __webpack_exports__["default"] = (initSentry);

/***/ }),

/***/ "./source/modules/utils.js":
/*!*********************************!*\
  !*** ./source/modules/utils.js ***!
  \*********************************/
/*! exports provided: validateObject, match, parsePrice, getCurrency, generatePrice, parseCurrency, complexPrice, parseNode, parseHtml, calculateDiscount, asyncTimeout, createHiddenFrame, copyToClipboard, asyncWrapper, createShadowRoot, get, substitute */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "validateObject", function() { return validateObject; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "match", function() { return match; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "parsePrice", function() { return parsePrice; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getCurrency", function() { return getCurrency; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "generatePrice", function() { return generatePrice; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "parseCurrency", function() { return parseCurrency; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "complexPrice", function() { return complexPrice; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "parseNode", function() { return parseNode; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "parseHtml", function() { return parseHtml; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "calculateDiscount", function() { return calculateDiscount; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "asyncTimeout", function() { return asyncTimeout; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createHiddenFrame", function() { return createHiddenFrame; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "copyToClipboard", function() { return copyToClipboard; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "asyncWrapper", function() { return asyncWrapper; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createShadowRoot", function() { return createShadowRoot; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "get", function() { return get; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "substitute", function() { return substitute; });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/asyncToGenerator */ "./node_modules/@babel/runtime/helpers/asyncToGenerator.js");
/* harmony import */ var _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _babel_runtime_helpers_toConsumableArray__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/toConsumableArray */ "./node_modules/@babel/runtime/helpers/toConsumableArray.js");
/* harmony import */ var _babel_runtime_helpers_toConsumableArray__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_toConsumableArray__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime/helpers/slicedToArray */ "./node_modules/@babel/runtime/helpers/slicedToArray.js");
/* harmony import */ var _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_3__);




var validateObject = function validateObject() {
  var fields = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
  var object = arguments.length > 1 ? arguments[1] : undefined;
  return fields.every(function (name) {
    return typeof object[name] !== 'undefined';
  });
};
var match = function match(str, matcher) {
  try {
    return str.search(matcher) > -1;
  } catch (e) {
    return str.indexOf(matcher) > 0;
  }
};
var parsePrice = function parsePrice(price) {
  var str = "".concat(price).replace(/[$€£￡]/g, '').replace(/[^\d.,\n]/g, '').trim();
  var commaIndex = str.indexOf(',');
  var dotIndex = str.indexOf('.');

  if (commaIndex > dotIndex) {
    str = str.replace('.', '').replace(',', '.');
  } else if (commaIndex < dotIndex || dotIndex < 0) {
    str = str.replace(',', '');
  }

  return parseFloat(str);
};
var getCurrency = function getCurrency(matcher) {
  var result = [['en-US', 'USD', '$'], ['de-DE', 'EUR', '€'], ['en-US', 'GBP', '£￡']].find(function (_ref) {
    var _ref2 = _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_3___default()(_ref, 3),
        code = _ref2[1],
        matchSymbol = _ref2[2];

    return code === matcher || matchSymbol.includes(matcher);
  });

  if (result) {
    var _result = _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_3___default()(result, 2),
        language = _result[0],
        code = _result[1];

    return {
      language: language,
      code: code
    };
  }

  return null;
};
var generatePrice = function generatePrice(data) {
  var value = data.value,
      currency = data.currency;

  var _getCurrency = getCurrency(currency),
      language = _getCurrency.language,
      code = _getCurrency.code;

  var params = {
    style: 'currency',
    currency: code
  };
  return new Intl.NumberFormat(language, params).format(value);
};
var parseCurrency = function parseCurrency(str) {
  var parsedStr = ["".concat(str).match(/[$€£￡]|GBP|USD|EUR/)].join();
  return getCurrency(parsedStr);
};
var complexPrice = function complexPrice(str) {
  return {
    price: "".concat(str).split('-').map(function (price) {
      return parsePrice(price);
    }),
    currency: parseCurrency(str)
  };
};
var parseNode = function parseNode(_ref3) {
  var selector = _ref3.selector,
      attribute = _ref3.attribute,
      el = _ref3.el;
  var parentNode = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : document;
  var node = el || parentNode.querySelector(selector);

  if (node) {
    var textNode = _babel_runtime_helpers_toConsumableArray__WEBPACK_IMPORTED_MODULE_2___default()(node.childNodes).find(function (child) {
      return child.nodeName.includes('wholeText');
    });

    return "".concat(node.getAttribute(attribute) || textNode && textNode.wholeText || node.innerText || node.value || node.content || node.src);
  }

  return null;
};
var parseHtml = function parseHtml(htmlText) {
  return new DOMParser().parseFromString(htmlText, 'text/html');
};
var calculateDiscount = function calculateDiscount(_ref4) {
  var currency = _ref4.currency,
      price = _ref4.price,
      rebate = _ref4.rebate;
  var discountedPrice = parseInt(price - price * rebate / 100, 10);
  var params = {
    minimumFractionDigits: 2,
    maximumFractionDigits: 2
  };
  return "".concat(currency).concat(discountedPrice.toLocaleString(params), ".00");
};
var asyncTimeout = /*#__PURE__*/function () {
  var _ref5 = _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1___default()( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(t) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            return _context.abrupt("return", new Promise(function (r) {
              return setTimeout(function () {
                return r(true);
              }, t);
            }));

          case 1:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function asyncTimeout(_x) {
    return _ref5.apply(this, arguments);
  };
}();
var createHiddenFrame = function createHiddenFrame(url) {
  var frame = document.createElement('iframe');
  frame.style = 'width: 1px; height: 1px; opacity: 0; position: fixed; bottom: 0; right: 0;';
  frame.src = url;
  return frame;
};
var copyToClipboard = function copyToClipboard(str) {
  var i = document.createElement('input');
  i.style = 'position: absolute; bottom: -99999999; right: -999999999;';
  document.body.appendChild(i);
  i.value = str;
  i.select();
  document.execCommand('Copy');
  document.body.removeChild(i);
};
var asyncWrapper = /*#__PURE__*/function () {
  var _ref6 = _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1___default()( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2(method, args) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            return _context2.abrupt("return", method(args));

          case 1:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));

  return function asyncWrapper(_x2, _x3) {
    return _ref6.apply(this, arguments);
  };
}();
var createShadowRoot = function createShadowRoot(id) {
  var wrapper = document.createElement('div');
  wrapper.id = id;
  document.body.append(wrapper);
  var shadowroot = wrapper.attachShadow({
    mode: 'open'
  });
  shadowroot.innerHTML = "\n    <style>@import url(\"".concat(chrome.extension.getURL('content/styles.css'), "\");</style>\n    <div id=\"").concat(id, "\" />");
  return shadowroot;
};
var get = function get(object, path) {
  var arrayPath = typeof path === 'string' ? path.split('.') : path;

  if (Array.isArray(object) && Number.isNaN(parseInt(arrayPath[0], 10))) {
    return object.map(function (objectData) {
      return get(objectData, _babel_runtime_helpers_toConsumableArray__WEBPACK_IMPORTED_MODULE_2___default()(arrayPath));
    }).flat(Infinity);
  }

  var data = object[arrayPath[0]];
  arrayPath.shift();

  if (data && arrayPath.length) {
    return get(data, arrayPath);
  }

  return data;
};
var substitute = function substitute(_ref7) {
  var str = _ref7.str,
      obj = _ref7.obj,
      _ref7$entries = _ref7.entries,
      entries = _ref7$entries === void 0 ? Object.entries(obj) : _ref7$entries;
  var strResult = str;

  if (entries) {
    strResult = entries.reduce(function (result, _ref8) {
      var _ref9 = _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_3___default()(_ref8, 2),
          name = _ref9[0],
          value = _ref9[1];

      return result.replace("%".concat(name), value);
    }, strResult);
  }

  return strResult;
};

/***/ }),

/***/ "./source/utils.js":
/*!*************************!*\
  !*** ./source/utils.js ***!
  \*************************/
/*! exports provided: validateObject, match, parsePrice, getCurrency, parseCurrency, complexPrice, parseNode, parseHtml, calculateDiscount, asyncTimeout, createHiddenFrame, copyToClipboard, asyncWrapper, createShadowRoot, get, substitute */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "validateObject", function() { return validateObject; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "match", function() { return match; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "parsePrice", function() { return parsePrice; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getCurrency", function() { return getCurrency; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "parseCurrency", function() { return parseCurrency; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "complexPrice", function() { return complexPrice; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "parseNode", function() { return parseNode; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "parseHtml", function() { return parseHtml; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "calculateDiscount", function() { return calculateDiscount; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "asyncTimeout", function() { return asyncTimeout; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createHiddenFrame", function() { return createHiddenFrame; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "copyToClipboard", function() { return copyToClipboard; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "asyncWrapper", function() { return asyncWrapper; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createShadowRoot", function() { return createShadowRoot; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "get", function() { return get; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "substitute", function() { return substitute; });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/asyncToGenerator */ "./node_modules/@babel/runtime/helpers/asyncToGenerator.js");
/* harmony import */ var _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _babel_runtime_helpers_toConsumableArray__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/toConsumableArray */ "./node_modules/@babel/runtime/helpers/toConsumableArray.js");
/* harmony import */ var _babel_runtime_helpers_toConsumableArray__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_toConsumableArray__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime/helpers/slicedToArray */ "./node_modules/@babel/runtime/helpers/slicedToArray.js");
/* harmony import */ var _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_3__);




var validateObject = function validateObject() {
  var fields = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
  var object = arguments.length > 1 ? arguments[1] : undefined;
  return fields.every(function (name) {
    return typeof object[name] !== 'undefined';
  });
};
var match = function match(str, matcher) {
  try {
    return str.search(matcher) > -1;
  } catch (e) {
    return str.indexOf(matcher) > 0;
  }
};
var parsePrice = function parsePrice(price) {
  var str = "".concat(price).replace(/[$€£￡]/g, '').replace(/[^\d.,\n]/g, '').trim();
  var commaIndex = str.indexOf(',');
  var dotIndex = str.indexOf('.');

  if (commaIndex > dotIndex) {
    str = str.replace('.', '').replace(',', '.');
  } else if (commaIndex < dotIndex || dotIndex < 0) {
    str = str.replace(',', '');
  }

  return parseFloat(str);
};
var getCurrency = function getCurrency(matcher) {
  var result = [['en-US', 'USD', '$'], ['de-DE', 'EUR', '€'], ['en-US', 'GBP', '£￡']].find(function (_ref) {
    var _ref2 = _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_3___default()(_ref, 3),
        code = _ref2[1],
        matchSymbol = _ref2[2];

    return code === matcher || matchSymbol.includes(matcher);
  });

  if (result) {
    var _result = _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_3___default()(result, 2),
        language = _result[0],
        code = _result[1];

    return {
      language: language,
      code: code
    };
  }

  return null;
};
var parseCurrency = function parseCurrency(str) {
  var parsedStr = ["".concat(str).match(/[$€£￡]/)].join();
  return getCurrency(parsedStr);
};
var complexPrice = function complexPrice(str) {
  return {
    price: "".concat(str).split('-').map(function (price) {
      return parsePrice(price);
    }),
    currency: parseCurrency(str)
  };
};
var parseNode = function parseNode(_ref3) {
  var selector = _ref3.selector,
      attribute = _ref3.attribute,
      el = _ref3.el;
  var parentNode = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : document;
  var node = el || parentNode.querySelector(selector);

  if (node) {
    var textNode = _babel_runtime_helpers_toConsumableArray__WEBPACK_IMPORTED_MODULE_2___default()(node.childNodes).find(function (child) {
      return child.nodeName.includes('wholeText');
    });

    return "".concat(node.getAttribute(attribute) || textNode && textNode.wholeText || node.innerText || node.value || node.content || node.src);
  }

  return null;
};
var parseHtml = function parseHtml(htmlText) {
  return new DOMParser().parseFromString(htmlText, 'text/html');
};
var calculateDiscount = function calculateDiscount(_ref4) {
  var currency = _ref4.currency,
      price = _ref4.price,
      rebate = _ref4.rebate;
  var discountedPrice = parseInt(price - price * rebate / 100, 10);
  var params = {
    minimumFractionDigits: 2,
    maximumFractionDigits: 2
  };
  return "".concat(currency).concat(discountedPrice.toLocaleString(params), ".00");
};
var asyncTimeout = /*#__PURE__*/function () {
  var _ref5 = _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1___default()( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(t) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            return _context.abrupt("return", new Promise(function (r) {
              return setTimeout(function () {
                return r(true);
              }, t);
            }));

          case 1:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function asyncTimeout(_x) {
    return _ref5.apply(this, arguments);
  };
}();
var createHiddenFrame = function createHiddenFrame(url) {
  var frame = document.createElement('iframe');
  frame.style = 'width: 1px; height: 1px; opacity: 0; position: fixed; bottom: 0; right: 0;';
  frame.src = url;
  return frame;
};
var copyToClipboard = function copyToClipboard(str) {
  var i = document.createElement('input');
  i.style = 'position: absolute; bottom: -99999999; right: -999999999;';
  document.body.appendChild(i);
  i.value = str;
  i.select();
  document.execCommand('Copy');
  document.body.removeChild(i);
};
var asyncWrapper = /*#__PURE__*/function () {
  var _ref6 = _babel_runtime_helpers_asyncToGenerator__WEBPACK_IMPORTED_MODULE_1___default()( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2(method, args) {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            return _context2.abrupt("return", method(args));

          case 1:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));

  return function asyncWrapper(_x2, _x3) {
    return _ref6.apply(this, arguments);
  };
}();
var createShadowRoot = function createShadowRoot(id) {
  var wrapper = document.createElement('div');
  wrapper.id = id;
  document.body.append(wrapper);
  var shadowroot = wrapper.attachShadow({
    mode: 'open'
  });
  shadowroot.innerHTML = "\n    <style>@import url(\"".concat(chrome.extension.getURL('content/styles.css'), "\");</style>\n    <div id=\"").concat(id, "\" />");
  return shadowroot;
};
var get = function get(object, path) {
  var arrayPath = typeof path === 'string' ? path.split('.') : path;

  if (Array.isArray(object) && Number.isNaN(parseInt(arrayPath[0], 10))) {
    return object.map(function (objectData) {
      return get(objectData, _babel_runtime_helpers_toConsumableArray__WEBPACK_IMPORTED_MODULE_2___default()(arrayPath));
    }).flat(Infinity);
  }

  var data = object[arrayPath[0]];
  arrayPath.shift();

  if (data && arrayPath.length) {
    return get(data, arrayPath);
  }

  return data;
};
var substitute = function substitute(str, obj) {
  var result = str;

  if (obj) {
    Object.entries(obj).forEach(function (_ref7) {
      var _ref8 = _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_3___default()(_ref7, 2),
          name = _ref8[0],
          value = _ref8[1];

      result = str.replace("%".concat(name), value);
    });
  }

  return result;
};

/***/ })

}]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvQGJhYmVsL3J1bnRpbWUvaGVscGVycy9hcnJheUxpa2VUb0FycmF5LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9AYmFiZWwvcnVudGltZS9oZWxwZXJzL2FycmF5V2l0aEhvbGVzLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9AYmFiZWwvcnVudGltZS9oZWxwZXJzL2FycmF5V2l0aG91dEhvbGVzLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9AYmFiZWwvcnVudGltZS9oZWxwZXJzL2Fzc2VydFRoaXNJbml0aWFsaXplZC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvQGJhYmVsL3J1bnRpbWUvaGVscGVycy9hc3luY1RvR2VuZXJhdG9yLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9AYmFiZWwvcnVudGltZS9oZWxwZXJzL2NsYXNzQ2FsbENoZWNrLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9AYmFiZWwvcnVudGltZS9oZWxwZXJzL2NyZWF0ZUNsYXNzLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9AYmFiZWwvcnVudGltZS9oZWxwZXJzL2RlZmluZVByb3BlcnR5LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9AYmFiZWwvcnVudGltZS9oZWxwZXJzL2dldFByb3RvdHlwZU9mLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9AYmFiZWwvcnVudGltZS9oZWxwZXJzL2luaGVyaXRzLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9AYmFiZWwvcnVudGltZS9oZWxwZXJzL2l0ZXJhYmxlVG9BcnJheS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvQGJhYmVsL3J1bnRpbWUvaGVscGVycy9pdGVyYWJsZVRvQXJyYXlMaW1pdC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvQGJhYmVsL3J1bnRpbWUvaGVscGVycy9ub25JdGVyYWJsZVJlc3QuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL0BiYWJlbC9ydW50aW1lL2hlbHBlcnMvbm9uSXRlcmFibGVTcHJlYWQuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL0BiYWJlbC9ydW50aW1lL2hlbHBlcnMvcG9zc2libGVDb25zdHJ1Y3RvclJldHVybi5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvQGJhYmVsL3J1bnRpbWUvaGVscGVycy9zZXRQcm90b3R5cGVPZi5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvQGJhYmVsL3J1bnRpbWUvaGVscGVycy9zbGljZWRUb0FycmF5LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9AYmFiZWwvcnVudGltZS9oZWxwZXJzL3RvQ29uc3VtYWJsZUFycmF5LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9AYmFiZWwvcnVudGltZS9oZWxwZXJzL3R5cGVvZi5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvQGJhYmVsL3J1bnRpbWUvaGVscGVycy91bnN1cHBvcnRlZEl0ZXJhYmxlVG9BcnJheS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvQGJhYmVsL3J1bnRpbWUvcmVnZW5lcmF0b3IvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL0BzZW50cnkvYnJvd3Nlci9lc20vYmFja2VuZC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvQHNlbnRyeS9icm93c2VyL2VzbS9jbGllbnQuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL0BzZW50cnkvYnJvd3Nlci9lc20vZXZlbnRidWlsZGVyLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9Ac2VudHJ5L2Jyb3dzZXIvZXNtL2V4cG9ydHMuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL0BzZW50cnkvYnJvd3Nlci9lc20vaGVscGVycy5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvQHNlbnRyeS9icm93c2VyL2VzbS9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvQHNlbnRyeS9icm93c2VyL2VzbS9pbnRlZ3JhdGlvbnMvYnJlYWRjcnVtYnMuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL0BzZW50cnkvYnJvd3Nlci9lc20vaW50ZWdyYXRpb25zL2dsb2JhbGhhbmRsZXJzLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9Ac2VudHJ5L2Jyb3dzZXIvZXNtL2ludGVncmF0aW9ucy9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvQHNlbnRyeS9icm93c2VyL2VzbS9pbnRlZ3JhdGlvbnMvbGlua2VkZXJyb3JzLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9Ac2VudHJ5L2Jyb3dzZXIvZXNtL2ludGVncmF0aW9ucy90cnljYXRjaC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvQHNlbnRyeS9icm93c2VyL2VzbS9pbnRlZ3JhdGlvbnMvdXNlcmFnZW50LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9Ac2VudHJ5L2Jyb3dzZXIvZXNtL3BhcnNlcnMuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL0BzZW50cnkvYnJvd3Nlci9lc20vc2RrLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9Ac2VudHJ5L2Jyb3dzZXIvZXNtL3RyYWNla2l0LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9Ac2VudHJ5L2Jyb3dzZXIvZXNtL3RyYW5zcG9ydHMvYmFzZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvQHNlbnRyeS9icm93c2VyL2VzbS90cmFuc3BvcnRzL2ZldGNoLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9Ac2VudHJ5L2Jyb3dzZXIvZXNtL3RyYW5zcG9ydHMvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL0BzZW50cnkvYnJvd3Nlci9lc20vdHJhbnNwb3J0cy94aHIuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL0BzZW50cnkvYnJvd3Nlci9lc20vdmVyc2lvbi5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvQHNlbnRyeS9jb3JlL2VzbS9hcGkuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL0BzZW50cnkvY29yZS9lc20vYmFzZWJhY2tlbmQuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL0BzZW50cnkvY29yZS9lc20vYmFzZWNsaWVudC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvQHNlbnRyeS9jb3JlL2VzbS9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvQHNlbnRyeS9jb3JlL2VzbS9pbnRlZ3JhdGlvbi5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvQHNlbnRyeS9jb3JlL2VzbS9pbnRlZ3JhdGlvbnMvZnVuY3Rpb250b3N0cmluZy5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvQHNlbnRyeS9jb3JlL2VzbS9pbnRlZ3JhdGlvbnMvaW5ib3VuZGZpbHRlcnMuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL0BzZW50cnkvY29yZS9lc20vaW50ZWdyYXRpb25zL2luZGV4LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9Ac2VudHJ5L2NvcmUvZXNtL3JlcXVlc3QuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL0BzZW50cnkvY29yZS9lc20vc2RrLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9Ac2VudHJ5L2NvcmUvZXNtL3RyYW5zcG9ydHMvbm9vcC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvQHNlbnRyeS9odWIvZXNtL2h1Yi5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvQHNlbnRyeS9odWIvZXNtL2luZGV4LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9Ac2VudHJ5L2h1Yi9lc20vc2NvcGUuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL0BzZW50cnkvbWluaW1hbC9lc20vaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL0BzZW50cnkvdHlwZXMvZXNtL2luZGV4LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9Ac2VudHJ5L3R5cGVzL2VzbS9sb2dsZXZlbC5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvQHNlbnRyeS90eXBlcy9lc20vc2V2ZXJpdHkuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL0BzZW50cnkvdHlwZXMvZXNtL3N0YXR1cy5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvQHNlbnRyeS91dGlscy9lc20vYXN5bmMuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL0BzZW50cnkvdXRpbHMvZXNtL2Rzbi5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvQHNlbnRyeS91dGlscy9lc20vZXJyb3IuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL0BzZW50cnkvdXRpbHMvZXNtL2luZGV4LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9Ac2VudHJ5L3V0aWxzL2VzbS9pbnN0cnVtZW50LmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9Ac2VudHJ5L3V0aWxzL2VzbS9pcy5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvQHNlbnRyeS91dGlscy9lc20vbG9nZ2VyLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9Ac2VudHJ5L3V0aWxzL2VzbS9tZW1vLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9Ac2VudHJ5L3V0aWxzL2VzbS9taXNjLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9Ac2VudHJ5L3V0aWxzL2VzbS9vYmplY3QuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL0BzZW50cnkvdXRpbHMvZXNtL3BhdGguanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL0BzZW50cnkvdXRpbHMvZXNtL3BvbHlmaWxsLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9Ac2VudHJ5L3V0aWxzL2VzbS9wcm9taXNlYnVmZmVyLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9Ac2VudHJ5L3V0aWxzL2VzbS9zdHJpbmcuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL0BzZW50cnkvdXRpbHMvZXNtL3N1cHBvcnRzLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9Ac2VudHJ5L3V0aWxzL2VzbS9zeW5jcHJvbWlzZS5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvZXZlbnRzL2V2ZW50cy5qcyIsIndlYnBhY2s6Ly8vLi9ub2RlX21vZHVsZXMvcHJvY2Vzcy9icm93c2VyLmpzIiwid2VicGFjazovLy8uL25vZGVfbW9kdWxlcy9yZWdlbmVyYXRvci1ydW50aW1lL3J1bnRpbWUuanMiLCJ3ZWJwYWNrOi8vLy4vbm9kZV9tb2R1bGVzL3RzbGliL3RzbGliLmVzNi5qcyIsIndlYnBhY2s6Ly8vKHdlYnBhY2spL2J1aWxkaW4vZ2xvYmFsLmpzIiwid2VicGFjazovLy8od2VicGFjaykvYnVpbGRpbi9oYXJtb255LW1vZHVsZS5qcyIsIndlYnBhY2s6Ly8vLi9zb3VyY2UvbW9kdWxlcy9wYXJzZXIvaHRtbC1oZWxwZXJzLmpzIiwid2VicGFjazovLy8uL3NvdXJjZS9tb2R1bGVzL3BhcnNlci9pbmRleC5qcyIsIndlYnBhY2s6Ly8vLi9zb3VyY2UvbW9kdWxlcy9wYXJzZXIvanNvbi1oZWxwZXJzLmpzIiwid2VicGFjazovLy8uL3NvdXJjZS9tb2R1bGVzL3NlbnRyeS5qcyIsIndlYnBhY2s6Ly8vLi9zb3VyY2UvbW9kdWxlcy91dGlscy5qcyIsIndlYnBhY2s6Ly8vLi9zb3VyY2UvdXRpbHMuanMiXSwibmFtZXMiOlsiY2FyZE51bWJlciIsInN0ciIsInJlcGxhY2UiLCJpbWFnZSIsInF1YW50aXR5IiwicGFyc2VJbnQiLCJudW1lcmljU2t1IiwibWF0Y2giLCJqb2luIiwicHJpY2UiLCJwYXJzZVByaWNlIiwidG90YWwiLCJjdXJyZW5jeSIsInBhcnNlQ3VycmVuY3kiLCJ0b3RhbEN1cnJlbmN5IiwiUGFyc2VyIiwiY29uZmlnIiwiZGVmaW5lT2JzZXJ2ZWRQcm9wZXJ0eSIsInVybCIsIm9wdGlvbnMiLCJhYm9ydENvbnRyb2xsZXIiLCJhYm9ydCIsIkFib3J0Q29udHJvbGxlciIsInNpZ25hbCIsImZldGNoIiwicmVzcG9uc2UiLCJvayIsIkVycm9yIiwiT2JqZWN0IiwiZW50cmllcyIsImpzb24iLCJ0ZXh0IiwiZmluZCIsInZhbHVlIiwiaGVhZGVycyIsImdldCIsImluY2x1ZGVzIiwicGFyc2VUeXBlIiwiZGF0YSIsInBhcnNlSHRtbCIsIm5hbWUiLCJwYXJhbXMiLCJwYXJlbnQiLCJkb2N1bWVudCIsImlzSHRtbCIsImRvY3VtZW50RWxlbWVudCIsIkhUTUxFbGVtZW50IiwicGFyc2VOb2RlIiwicGF0aCIsImpzb25IZWxwZXIiLCJodG1sSGVscGVyIiwiaGVscGVyTmFtZSIsImhlbHBlciIsImh0bWxIZWxwZXJzIiwianNvbkhlbHBlcnMiLCJydWxlcyIsIm1hcCIsInBhcnNlSXRlbSIsImZyb21FbnRyaWVzIiwiY29udGVudCIsInN1YnN0aXR1dGUiLCJkb2MiLCJpdGVtcyIsImNvbnRhaW5lciIsInNlbGVjdG9yIiwicGFyZW50cyIsInF1ZXJ5U2VsZWN0b3JBbGwiLCJwYXJzZUNvbnRhaW5lciIsImZldGNoRGF0YSIsImNvbnNvbGUiLCJ0aW1lIiwicGFyc2VkRGF0YSIsInBhcnNlSXRlbXMiLCJ0aW1lRW5kIiwiZGVmaW5lUHJvcGVydHkiLCJzZXQiLCJlbWl0IiwiRXZlbnRFbWl0dGVyIiwiY3VycnlzUHJpY2UiLCJudW0iLCJpbml0U2VudHJ5IiwiYmVmb3JlSW5pdEZ1bmN0aW9uIiwiU2VudHJ5IiwiZHNuIiwidmFsaWRhdGVPYmplY3QiLCJmaWVsZHMiLCJvYmplY3QiLCJldmVyeSIsIm1hdGNoZXIiLCJzZWFyY2giLCJlIiwiaW5kZXhPZiIsInRyaW0iLCJjb21tYUluZGV4IiwiZG90SW5kZXgiLCJwYXJzZUZsb2F0IiwiZ2V0Q3VycmVuY3kiLCJyZXN1bHQiLCJjb2RlIiwibWF0Y2hTeW1ib2wiLCJsYW5ndWFnZSIsImdlbmVyYXRlUHJpY2UiLCJzdHlsZSIsIkludGwiLCJOdW1iZXJGb3JtYXQiLCJmb3JtYXQiLCJwYXJzZWRTdHIiLCJjb21wbGV4UHJpY2UiLCJzcGxpdCIsImF0dHJpYnV0ZSIsImVsIiwicGFyZW50Tm9kZSIsIm5vZGUiLCJxdWVyeVNlbGVjdG9yIiwidGV4dE5vZGUiLCJjaGlsZE5vZGVzIiwiY2hpbGQiLCJub2RlTmFtZSIsImdldEF0dHJpYnV0ZSIsIndob2xlVGV4dCIsImlubmVyVGV4dCIsInNyYyIsImh0bWxUZXh0IiwiRE9NUGFyc2VyIiwicGFyc2VGcm9tU3RyaW5nIiwiY2FsY3VsYXRlRGlzY291bnQiLCJyZWJhdGUiLCJkaXNjb3VudGVkUHJpY2UiLCJtaW5pbXVtRnJhY3Rpb25EaWdpdHMiLCJtYXhpbXVtRnJhY3Rpb25EaWdpdHMiLCJ0b0xvY2FsZVN0cmluZyIsImFzeW5jVGltZW91dCIsInQiLCJQcm9taXNlIiwiciIsInNldFRpbWVvdXQiLCJjcmVhdGVIaWRkZW5GcmFtZSIsImZyYW1lIiwiY3JlYXRlRWxlbWVudCIsImNvcHlUb0NsaXBib2FyZCIsImkiLCJib2R5IiwiYXBwZW5kQ2hpbGQiLCJzZWxlY3QiLCJleGVjQ29tbWFuZCIsInJlbW92ZUNoaWxkIiwiYXN5bmNXcmFwcGVyIiwibWV0aG9kIiwiYXJncyIsImNyZWF0ZVNoYWRvd1Jvb3QiLCJpZCIsIndyYXBwZXIiLCJhcHBlbmQiLCJzaGFkb3dyb290IiwiYXR0YWNoU2hhZG93IiwibW9kZSIsImlubmVySFRNTCIsImNocm9tZSIsImV4dGVuc2lvbiIsImdldFVSTCIsImFycmF5UGF0aCIsIkFycmF5IiwiaXNBcnJheSIsIk51bWJlciIsImlzTmFOIiwib2JqZWN0RGF0YSIsImZsYXQiLCJJbmZpbml0eSIsInNoaWZ0IiwibGVuZ3RoIiwib2JqIiwic3RyUmVzdWx0IiwicmVkdWNlIiwiZm9yRWFjaCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBQUE7QUFDQTs7QUFFQSx3Q0FBd0MsU0FBUztBQUNqRDtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsbUM7Ozs7Ozs7Ozs7O0FDVkE7QUFDQTtBQUNBOztBQUVBLGlDOzs7Ozs7Ozs7OztBQ0pBLHVCQUF1QixtQkFBTyxDQUFDLHFGQUFvQjs7QUFFbkQ7QUFDQTtBQUNBOztBQUVBLG9DOzs7Ozs7Ozs7OztBQ05BO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsd0M7Ozs7Ozs7Ozs7O0FDUkE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7O0FBRUEsbUM7Ozs7Ozs7Ozs7O0FDcENBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsaUM7Ozs7Ozs7Ozs7O0FDTkE7QUFDQSxpQkFBaUIsa0JBQWtCO0FBQ25DO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsOEI7Ozs7Ozs7Ozs7O0FDaEJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLEdBQUc7QUFDSDtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsaUM7Ozs7Ozs7Ozs7O0FDZkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLGlDOzs7Ozs7Ozs7OztBQ1BBLHFCQUFxQixtQkFBTyxDQUFDLGlGQUFrQjs7QUFFL0M7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7O0FBRUEsMkI7Ozs7Ozs7Ozs7O0FDakJBO0FBQ0E7QUFDQTs7QUFFQSxrQzs7Ozs7Ozs7Ozs7QUNKQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSw2Q0FBNkMsK0JBQStCO0FBQzVFOztBQUVBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLHVDOzs7Ozs7Ozs7OztBQzNCQTtBQUNBO0FBQ0E7O0FBRUEsa0M7Ozs7Ozs7Ozs7O0FDSkE7QUFDQTtBQUNBOztBQUVBLG9DOzs7Ozs7Ozs7OztBQ0pBLGNBQWMsbUJBQU8sQ0FBQywwRUFBbUI7O0FBRXpDLDRCQUE0QixtQkFBTyxDQUFDLCtGQUF5Qjs7QUFFN0Q7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSw0Qzs7Ozs7Ozs7Ozs7QUNaQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUEsaUM7Ozs7Ozs7Ozs7O0FDVEEscUJBQXFCLG1CQUFPLENBQUMsaUZBQWtCOztBQUUvQywyQkFBMkIsbUJBQU8sQ0FBQyw2RkFBd0I7O0FBRTNELGlDQUFpQyxtQkFBTyxDQUFDLHlHQUE4Qjs7QUFFdkUsc0JBQXNCLG1CQUFPLENBQUMsbUZBQW1COztBQUVqRDtBQUNBO0FBQ0E7O0FBRUEsZ0M7Ozs7Ozs7Ozs7O0FDWkEsd0JBQXdCLG1CQUFPLENBQUMsdUZBQXFCOztBQUVyRCxzQkFBc0IsbUJBQU8sQ0FBQyxtRkFBbUI7O0FBRWpELGlDQUFpQyxtQkFBTyxDQUFDLHlHQUE4Qjs7QUFFdkUsd0JBQXdCLG1CQUFPLENBQUMsdUZBQXFCOztBQUVyRDtBQUNBO0FBQ0E7O0FBRUEsb0M7Ozs7Ozs7Ozs7O0FDWkE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBLHlCOzs7Ozs7Ozs7OztBQ2hCQSx1QkFBdUIsbUJBQU8sQ0FBQyxxRkFBb0I7O0FBRW5EO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsNkM7Ozs7Ozs7Ozs7O0FDWEEsaUJBQWlCLG1CQUFPLENBQUMsMEVBQXFCOzs7Ozs7Ozs7Ozs7O0FDQTlDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBaUM7QUFDVTtBQUNGO0FBQ3lDO0FBQ1Y7QUFDWjtBQUM1RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSSwrQ0FBaUI7QUFDckI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtCQUErQiw4Q0FBZ0IsR0FBRyxtQ0FBbUMseUJBQXlCO0FBQzlHO0FBQ0E7QUFDQTtBQUNBLFlBQVksbUVBQWE7QUFDekIsdUJBQXVCLDBEQUFjO0FBQ3JDO0FBQ0EsbUJBQW1CLHdEQUFZO0FBQy9CO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG9CQUFvQiwyRUFBcUI7QUFDekM7QUFDQSxTQUFTO0FBQ1QsUUFBUSwyRUFBcUI7QUFDN0I7QUFDQTtBQUNBLFNBQVM7QUFDVCxzQkFBc0Isc0RBQVE7QUFDOUI7QUFDQTtBQUNBO0FBQ0EsZUFBZSx5REFBVztBQUMxQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0JBQStCLFNBQVMsc0RBQVEsTUFBTTtBQUN0RDtBQUNBLG9CQUFvQixxRUFBZTtBQUNuQztBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWUseURBQVc7QUFDMUI7QUFDQTtBQUNBLENBQUMsQ0FBQyx3REFBVztBQUNhO0FBQzFCLG1DOzs7Ozs7Ozs7Ozs7QUNwRUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFpQztBQUNjO0FBQ1M7QUFDYjtBQUNFO0FBQ0s7QUFDbEQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJLCtDQUFpQjtBQUNyQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQ0FBaUMsY0FBYztBQUMvQyxpQ0FBaUMsdURBQWM7QUFDL0M7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esb0JBQW9CLDhDQUFnQixHQUFHLGNBQWMsT0FBTyxpREFBUSxZQUFZLDhDQUFnQjtBQUNoRztBQUNBO0FBQ0EsNkJBQTZCLG9EQUFXO0FBQ3hDLGlCQUFpQjtBQUNqQix5QkFBeUIsb0RBQVcsRUFBRTtBQUN0QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw4Q0FBOEMseURBQVc7QUFDekQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlDQUFpQyxjQUFjO0FBQy9DO0FBQ0EsdUJBQXVCLHFFQUFlO0FBQ3RDO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsWUFBWSxvREFBTTtBQUNsQjtBQUNBO0FBQ0E7QUFDQTtBQUNBLFlBQVksb0RBQU07QUFDbEI7QUFDQTtBQUNBO0FBQ0EsWUFBWSxvREFBTTtBQUNsQjtBQUNBO0FBQ0E7QUFDQTtBQUNBLHlCQUF5QixnREFBRztBQUM1QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDLENBQUMsdURBQVU7QUFDYTtBQUN6QixrQzs7Ozs7Ozs7Ozs7O0FDbEZBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUF5SjtBQUM1RDtBQUM5QztBQUMvQztBQUNPO0FBQ1AsNkJBQTZCLGNBQWM7QUFDM0M7QUFDQSxRQUFRLGtFQUFZO0FBQ3BCO0FBQ0E7QUFDQSxxQ0FBcUM7QUFDckMsZ0JBQWdCLG9FQUFtQixDQUFDLG1FQUFpQjtBQUNyRDtBQUNBO0FBQ0EsUUFBUSxnRUFBVSxlQUFlLG9FQUFjO0FBQy9DO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwyQ0FBMkMsZ0VBQVU7QUFDckQ7QUFDQTtBQUNBLFFBQVEsMkVBQXFCO0FBQzdCO0FBQ0E7QUFDQSxRQUFRLDZEQUFPO0FBQ2Y7QUFDQSxnQkFBZ0Isb0VBQW1CLENBQUMsbUVBQWlCO0FBQ3JEO0FBQ0E7QUFDQSxRQUFRLG1FQUFhLGVBQWUsNkRBQU87QUFDM0M7QUFDQTtBQUNBO0FBQ0E7QUFDQSxnQkFBZ0IscUVBQW9CO0FBQ3BDLFFBQVEsMkVBQXFCO0FBQzdCO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUksMkVBQXFCO0FBQ3pCLElBQUksMkVBQXFCO0FBQ3pCO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ087QUFDUCw2QkFBNkIsY0FBYztBQUMzQztBQUNBO0FBQ0E7QUFDQTtBQUNBLHlCQUF5QixtRUFBaUI7QUFDMUMsdUJBQXVCLHNFQUFxQjtBQUM1QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx3Qzs7Ozs7Ozs7Ozs7O0FDMUVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFrRDtBQUNxTztBQUM5TztBQUMrRTtBQUN0RTtBQUNsRCxtQzs7Ozs7Ozs7Ozs7O0FDTEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBaUM7QUFDMEI7QUFDa0I7QUFDN0U7QUFDQTtBQUNBO0FBQ0E7QUFDTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNPO0FBQ1AsNkJBQTZCLGNBQWM7QUFDM0M7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw0REFBNEQsMkJBQTJCLEVBQUU7QUFDekY7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxZQUFZLDhEQUFTO0FBQ3JCO0FBQ0EseUNBQXlDLDhDQUFnQixHQUFHO0FBQzVEO0FBQ0Esd0JBQXdCLDJFQUFxQjtBQUM3Qyx3QkFBd0IsMkVBQXFCO0FBQzdDO0FBQ0EsMkNBQTJDLDhDQUFnQixHQUFHLHlCQUF5QixrQkFBa0I7QUFDekc7QUFDQSxpQkFBaUI7QUFDakIsZ0JBQWdCLHFFQUFnQjtBQUNoQyxhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCLEVBQUU7QUFDbkI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNULEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQixhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQzs7Ozs7Ozs7Ozs7O0FDdElBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFpQztBQUNQO0FBQ3NDO0FBQ2hCO0FBQ007QUFDWDtBQUMzQztBQUNBO0FBQ0E7QUFDQSxjQUFjLHFFQUFlO0FBQzdCO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUJBQW1CLDhDQUFnQixHQUFHLHNCQUFzQix5REFBZ0IsRUFBRSwwQ0FBbUI7QUFDN0M7QUFDcEQsaUM7Ozs7Ozs7Ozs7OztBQ2hCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBaUM7QUFDWTtBQUNKO0FBQzhGO0FBQ3ZJO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esd0JBQXdCLDhDQUFnQixFQUFFLGdGQUFnRjtBQUMxSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsUUFBUSxrRUFBYTtBQUNyQjtBQUNBO0FBQ0E7QUFDQSxxQkFBcUIseUVBQW1CO0FBQ3hDLFNBQVM7QUFDVDtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYixtQkFBbUIsc0RBQVE7QUFDM0IscUJBQXFCLDhEQUFRO0FBQzdCO0FBQ0E7QUFDQTtBQUNBLDZEQUE2RCw4REFBUTtBQUNyRTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFFBQVEsa0VBQWE7QUFDckI7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxrQkFBa0Isc0VBQWdCO0FBQ2xDLGtCQUFrQixzRUFBZ0I7QUFDbEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxRQUFRLGtFQUFhO0FBQ3JCO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFlBQVksa0VBQWE7QUFDekI7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsWUFBWSxrRUFBYTtBQUN6QjtBQUNBO0FBQ0EsdUJBQXVCLHNEQUFRO0FBQy9CO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBLFlBQVksa0VBQWE7QUFDekI7QUFDQSxzQkFBc0IsOENBQWdCLEdBQUcsMEJBQTBCLDJDQUEyQztBQUM5RztBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQixxRUFBZTtBQUNwQztBQUNBO0FBQ0Esd0JBQXdCLDhEQUFRO0FBQ2hDLHlCQUF5Qiw4REFBUTtBQUNqQyx1QkFBdUIsOERBQVE7QUFDL0I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFFBQVEsa0VBQWE7QUFDckI7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2IsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFlBQVksK0VBQXlCO0FBQ3JDO0FBQ0E7QUFDQSxvQ0FBb0MsdUJBQXVCO0FBQzNEO0FBQ0E7QUFDQSwwREFBMEQsOENBQWdCO0FBQzFFLGlCQUFpQjtBQUNqQjtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0EsWUFBWSwrRUFBeUI7QUFDckM7QUFDQTtBQUNBLG9DQUFvQyx1QkFBdUI7QUFDM0Q7QUFDQTtBQUNBLHNEQUFzRCw4Q0FBZ0I7QUFDdEUsaUJBQWlCO0FBQ2pCO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQSxZQUFZLCtFQUF5QjtBQUNyQztBQUNBO0FBQ0Esb0NBQW9DLHVCQUF1QjtBQUMzRDtBQUNBO0FBQ0Esc0RBQXNELDhDQUFnQjtBQUN0RSxpQkFBaUI7QUFDakI7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBLFlBQVksK0VBQXlCO0FBQ3JDO0FBQ0E7QUFDQSxvQ0FBb0MsdUJBQXVCO0FBQzNEO0FBQ0E7QUFDQSx3REFBd0QsOENBQWdCO0FBQ3hFLGlCQUFpQjtBQUNqQjtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0EsWUFBWSwrRUFBeUI7QUFDckM7QUFDQTtBQUNBLG9DQUFvQyx1QkFBdUI7QUFDM0Q7QUFDQTtBQUNBLDBEQUEwRCw4Q0FBZ0I7QUFDMUUsaUJBQWlCO0FBQ2pCO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQztBQUNzQjtBQUN2Qix1Qzs7Ozs7Ozs7Ozs7O0FDM1BBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBaUM7QUFDWTtBQUNKO0FBQ3VHO0FBQ3hGO0FBQ1A7QUFDakQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esd0JBQXdCLDhDQUFnQixFQUFFLDRDQUE0QztBQUN0RjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFlBQVksb0RBQU07QUFDbEI7QUFDQTtBQUNBO0FBQ0EsWUFBWSxvREFBTTtBQUNsQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxRQUFRLCtFQUF5QjtBQUNqQztBQUNBO0FBQ0EsaUNBQWlDLGtFQUFhO0FBQzlDO0FBQ0E7QUFDQSx1Q0FBdUMsb0VBQW1CO0FBQzFEO0FBQ0E7QUFDQTtBQUNBLDRCQUE0QixpRUFBVztBQUN2QztBQUNBLDBEQUEwRCwyRUFBcUI7QUFDL0U7QUFDQTtBQUNBLHFCQUFxQjtBQUNyQixnQkFBZ0IsMkVBQXFCO0FBQ3JDO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQixhQUFhO0FBQ2I7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFFBQVEsK0VBQXlCO0FBQ2pDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlDQUFpQyxrRUFBYTtBQUM5QztBQUNBO0FBQ0EsdUNBQXVDLG9FQUFtQjtBQUMxRDtBQUNBO0FBQ0E7QUFDQSw0QkFBNEIsaUVBQVc7QUFDdkM7QUFDQSxzQkFBc0IsMkVBQXFCO0FBQzNDO0FBQ0E7QUFDQSxxQkFBcUI7QUFDckIsOEJBQThCLHNEQUFRO0FBQ3RDLGdCQUFnQiwyRUFBcUI7QUFDckM7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0EsYUFBYTtBQUNiO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxzQkFBc0Isa0VBQVk7QUFDbEM7QUFDQSxZQUFZLDhEQUFRO0FBQ3BCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQjtBQUNyQjtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQjtBQUNyQjtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsdUJBQXVCLDhEQUFRLGdDQUFnQyxxRUFBZTtBQUM5RTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQztBQUN5QjtBQUMxQiwwQzs7Ozs7Ozs7Ozs7O0FDak1BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQWtEO0FBQ1o7QUFDTTtBQUNFO0FBQ047QUFDeEMsaUM7Ozs7Ozs7Ozs7OztBQ0xBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQWlDO0FBQ3FDO0FBQ3pCO0FBQ1E7QUFDTDtBQUNoRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUNBQWlDLGNBQWM7QUFDL0M7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFFBQVEsNEVBQXVCO0FBQy9CLHVCQUF1QixrRUFBYTtBQUNwQztBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUVBQXFFLGtFQUFZO0FBQ2pGO0FBQ0E7QUFDQTtBQUNBLGlDQUFpQyw4Q0FBZ0I7QUFDakQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0JBQStCLFlBQVk7QUFDM0MsYUFBYSxrRUFBWTtBQUN6QjtBQUNBO0FBQ0EseUJBQXlCLG1FQUFpQjtBQUMxQyx3QkFBd0Isd0VBQXVCO0FBQy9DLG9EQUFvRCw4Q0FBZ0I7QUFDcEU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQztBQUN1QjtBQUN4Qix3Qzs7Ozs7Ozs7Ozs7O0FDL0RBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBaUM7QUFDc0M7QUFDckM7QUFDbEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHdCQUF3Qiw4Q0FBZ0IsRUFBRSw0R0FBNEc7QUFDdEo7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDRCQUE0Qix1QkFBdUI7QUFDbkQ7QUFDQTtBQUNBO0FBQ0Esc0JBQXNCLHFEQUFJO0FBQzFCO0FBQ0EsMkJBQTJCLFdBQVcscUVBQWUsWUFBWTtBQUNqRTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx1Q0FBdUMscURBQUk7QUFDM0M7QUFDQTtBQUNBO0FBQ0EsaUNBQWlDLHFFQUFlO0FBQ2hELHFCQUFxQjtBQUNyQjtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQixxRUFBZTtBQUNwQztBQUNBO0FBQ0E7QUFDQTtBQUNBLFFBQVEsMERBQUk7QUFDWjtBQUNBO0FBQ0E7QUFDQTtBQUNBLHlDQUF5QyxxREFBSTtBQUM3QztBQUNBO0FBQ0E7QUFDQSw2Q0FBNkMscUVBQWU7QUFDNUQ7QUFDQSxpQ0FBaUM7QUFDakM7QUFDQTtBQUNBLDZCQUE2QjtBQUM3Qix5QkFBeUI7QUFDekI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHNEQUFzRCxxREFBSTtBQUMxRDtBQUNBO0FBQ0E7QUFDQSxxQ0FBcUMscUVBQWU7QUFDcEQ7QUFDQSx5QkFBeUI7QUFDekI7QUFDQTtBQUNBLHFCQUFxQjtBQUNyQixpQkFBaUI7QUFDakI7QUFDQSxTQUFTO0FBQ1QsUUFBUSwwREFBSTtBQUNaO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw0QkFBNEIsdUJBQXVCO0FBQ25EO0FBQ0E7QUFDQSwyQkFBMkI7QUFDM0I7QUFDQTtBQUNBO0FBQ0Esb0JBQW9CLDBEQUFJO0FBQ3hCO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsNkNBQTZDLHFFQUFlO0FBQzVELGlDQUFpQztBQUNqQztBQUNBO0FBQ0EsNkJBQTZCO0FBQzdCO0FBQ0E7QUFDQTtBQUNBLGlFQUFpRSxxRUFBZTtBQUNoRjtBQUNBO0FBQ0EsK0JBQStCLHFEQUFJO0FBQ25DLHFCQUFxQjtBQUNyQjtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCLHFFQUFlO0FBQ3BDO0FBQ0EsWUFBWSwwREFBSTtBQUNoQjtBQUNBO0FBQ0EsWUFBWSwwREFBSTtBQUNoQjtBQUNBO0FBQ0EsWUFBWSwwREFBSTtBQUNoQjtBQUNBO0FBQ0EsWUFBWSwwREFBSTtBQUNoQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQztBQUNtQjtBQUNwQixvQzs7Ozs7Ozs7Ozs7O0FDck1BO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBaUM7QUFDcUM7QUFDdEI7QUFDaEQsYUFBYSxxRUFBZTtBQUM1QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxRQUFRLDRFQUF1QjtBQUMvQixnQkFBZ0Isa0VBQWE7QUFDN0I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx1QkFBdUIsOENBQWdCLEdBQUcsVUFBVSxtQkFBbUI7QUFDdkU7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDO0FBQ29CO0FBQ3JCLHFDOzs7Ozs7Ozs7Ozs7QUNyQ0E7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBeUY7QUFDMUM7QUFDL0M7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ087QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxnQ0FBZ0M7QUFDaEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMEJBQTBCLDZEQUFPO0FBQ2pDLHNIQUFzSCxvRkFBOEI7QUFDcEosaUJBQWlCO0FBQ2pCO0FBQ0EsU0FBUztBQUNUO0FBQ0EsNEJBQTRCLHFFQUFlO0FBQzNDLFNBQVM7QUFDVDtBQUNBO0FBQ0EseUJBQXlCLG1FQUFpQjtBQUMxQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtCQUErQjtBQUMvQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSyxFQUFFLEVBQUU7QUFDVDtBQUNBO0FBQ0EsbUM7Ozs7Ozs7Ozs7OztBQzFGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBNEY7QUFDL0I7QUFDcEI7QUFDUTtBQUMrQztBQUN6RjtBQUNQLFFBQVEseURBQWdCO0FBQ3hCLFFBQVEseURBQWdCO0FBQ3hCLFFBQVEsc0RBQVE7QUFDaEIsUUFBUSx5REFBVztBQUNuQixRQUFRLDREQUFjO0FBQ3RCLFFBQVEsMERBQVk7QUFDcEIsUUFBUSx1REFBUztBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBLDhCQUE4QixXQUFXO0FBQ3pDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsV0FBVyxPQUFPO0FBQ2xCO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLGlCQUFpQjtBQUM1QjtBQUNBLHFCQUFxQixlQUFlO0FBQ3BDLG1CQUFtQixxQkFBcUI7QUFDeEMsb0JBQW9CLGFBQWE7QUFDakMsSUFBSTtBQUNKO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXLGdCQUFnQjtBQUMzQjtBQUNBO0FBQ0E7QUFDQSxJQUFJO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUk7QUFDSjtBQUNBO0FBQ0EsU0FBUyxxQkFBcUI7QUFDOUI7QUFDTztBQUNQLDZCQUE2QixjQUFjO0FBQzNDO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsdUJBQXVCLHFFQUFlO0FBQ3RDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJLGdFQUFXLENBQUMscURBQWE7QUFDN0I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ087QUFDUCw2QkFBNkIsY0FBYztBQUMzQztBQUNBLDBCQUEwQixrRUFBYTtBQUN2QztBQUNBLGlCQUFpQixrRUFBYTtBQUM5QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDTztBQUNQLFdBQVcsa0VBQWE7QUFDeEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ087QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ087QUFDUCxpQkFBaUIsa0VBQWE7QUFDOUI7QUFDQTtBQUNBO0FBQ0EsV0FBVyx5REFBVztBQUN0QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNPO0FBQ1AsaUJBQWlCLGtFQUFhO0FBQzlCO0FBQ0E7QUFDQTtBQUNBLFdBQVcseURBQVc7QUFDdEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNPO0FBQ1AsV0FBVyxxREFBWSxPQUFPO0FBQzlCO0FBQ0EsK0I7Ozs7Ozs7Ozs7OztBQzlKQTtBQUFBO0FBQUE7QUFBQTtBQUNpQztBQUNqQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQkFBbUIsa0JBQWtCO0FBQ3JDO0FBQ0Esd0VBQXdFO0FBQ3hFLGdFQUFnRTtBQUNoRTtBQUNBO0FBQ0EsdUNBQXVDO0FBQ3ZDLHVDQUF1QztBQUN2Qyx1Q0FBdUM7QUFDdkM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsOEJBQThCO0FBQzlCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxzQkFBc0IscUJBQXFCO0FBQzNDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWUsOENBQWdCLEdBQUcsZUFBZSx5Q0FBeUM7QUFDMUY7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG9DOzs7Ozs7Ozs7Ozs7QUM1TUE7QUFBQTtBQUFBO0FBQUE7QUFBbUM7QUFDd0I7QUFDM0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDJCQUEyQiwyREFBYTtBQUN4Qyx3QkFBd0IsZ0RBQUc7QUFDM0I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxrQkFBa0IseURBQVc7QUFDN0I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7QUFDd0I7QUFDekIsZ0M7Ozs7Ozs7Ozs7OztBQzNCQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFpQztBQUNtQjtBQUNiO0FBQzZFO0FBQzdFO0FBQ3ZDLGFBQWEscUVBQWU7QUFDNUI7QUFDQTtBQUNBLElBQUksK0NBQWlCO0FBQ3JCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0Esd0JBQXdCLHlFQUFvQjtBQUM1QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDZCQUE2Qiw0RUFBc0I7QUFDbkQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxvQ0FBb0MseURBQVc7QUFDL0M7QUFDQTtBQUNBO0FBQ0EsNkJBQTZCLG9EQUFNO0FBQ25DLCtCQUErQixvREFBTTtBQUNyQyw2QkFBNkIsaUJBQWlCO0FBQzlDO0FBQ0E7QUFDQSwrQkFBK0Isb0RBQU07QUFDckM7QUFDQSwwREFBMEQsMkVBQXFCO0FBQy9FLG9CQUFvQixvREFBTTtBQUMxQjtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxDQUFDLENBQUMsbURBQWE7QUFDVztBQUMxQixpQzs7Ozs7Ozs7Ozs7O0FDakVBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQXVDO0FBQ0U7QUFDSjtBQUNyQyxpQzs7Ozs7Ozs7Ozs7O0FDSEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBaUM7QUFDbUI7QUFDYjtBQUNvQztBQUNwQztBQUN2QztBQUNBO0FBQ0EsSUFBSSwrQ0FBaUI7QUFDckI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQSx3QkFBd0IseUVBQW9CO0FBQzVDLG9DQUFvQyx5REFBVztBQUMvQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsNkJBQTZCLG9EQUFNO0FBQ25DLCtCQUErQixvREFBTTtBQUNyQyw2QkFBNkIsaUJBQWlCO0FBQzlDO0FBQ0E7QUFDQSwrQkFBK0Isb0RBQU07QUFDckM7QUFDQSwwREFBMEQsMkVBQXFCO0FBQy9FLG9CQUFvQixvREFBTTtBQUMxQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsQ0FBQyxDQUFDLG1EQUFhO0FBQ1M7QUFDeEIsK0I7Ozs7Ozs7Ozs7OztBQ3pEQTtBQUFBO0FBQUE7QUFBTztBQUNBO0FBQ1AsbUM7Ozs7Ozs7Ozs7OztBQ0ZBO0FBQUE7QUFBQTtBQUErQztBQUMvQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw4QkFBOEIsaURBQUc7QUFDakM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZSwrREFBUztBQUN4QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHVDQUF1QyxvQkFBb0I7QUFDM0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7QUFDYztBQUNmLCtCOzs7Ozs7Ozs7Ozs7QUNsSEE7QUFBQTtBQUFBO0FBQUE7QUFBb0Q7QUFDRjtBQUNsRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxZQUFZLG9EQUFNO0FBQ2xCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUJBQW1CLDhEQUFhO0FBQ2hDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxrQkFBa0IseURBQVc7QUFDN0I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGtCQUFrQix5REFBVztBQUM3QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxZQUFZLG9EQUFNO0FBQ2xCLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQztBQUNzQjtBQUN2Qix1Qzs7Ozs7Ozs7Ozs7O0FDbERBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFpQztBQUNHO0FBQzRGO0FBQzlFO0FBQ2xEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJLHdCQUF3QjtBQUM1QixJQUFJLHdCQUF3QjtBQUM1QjtBQUNBO0FBQ0EsSUFBSSxvQkFBb0I7QUFDeEIsMENBQTBDLHNCQUFzQjtBQUNoRTtBQUNBO0FBQ0E7QUFDQSxJQUFJLDhCQUE4QjtBQUNsQztBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUksMEJBQTBCO0FBQzlCO0FBQ0EsSUFBSSwyQkFBMkI7QUFDL0I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw0QkFBNEIsaURBQUc7QUFDL0I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsNEJBQTRCLGlFQUFXO0FBQ3ZDO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQSxZQUFZLG9EQUFNO0FBQ2xCO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1EQUFtRCx5Q0FBeUMsRUFBRTtBQUM5RixTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlDQUFpQyxzRUFBaUI7QUFDbEQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxZQUFZLG9EQUFNO0FBQ2xCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1CQUFtQix5REFBVztBQUM5QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUI7QUFDckI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx5QkFBeUI7QUFDekI7QUFDQTtBQUNBLGFBQWE7QUFDYixTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHVCQUF1Qiw4Q0FBZ0IsR0FBRyxVQUFVLHNFQUFzRSwyREFBSyxtQ0FBbUMscUVBQWUsSUFBSTtBQUNyTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx5QkFBeUIsaURBQUs7QUFDOUI7QUFDQTtBQUNBLHFCQUFxQix5REFBVztBQUNoQztBQUNBLFlBQVksNEJBQTRCO0FBQ3hDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EseUJBQXlCLDhDQUFnQixHQUFHO0FBQzVDLDZEQUE2RCxTQUFTLDhDQUFnQixHQUFHO0FBQ3pGLHNCQUFzQiwrREFBUztBQUMvQixhQUFhLElBQUksRUFBRTtBQUNuQixTQUFTO0FBQ1Qsa0JBQWtCLCtEQUFTO0FBQzNCLFNBQVM7QUFDVCxzQkFBc0IsK0RBQVM7QUFDL0IsU0FBUztBQUNULG1CQUFtQiwrREFBUztBQUM1QixTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsNEJBQTRCLDhEQUFRO0FBQ3BDO0FBQ0E7QUFDQTtBQUNBLDhCQUE4Qiw4REFBUTtBQUN0QztBQUNBO0FBQ0E7QUFDQSwwQkFBMEIsOERBQVE7QUFDbEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQkFBbUIseURBQVc7QUFDOUI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUJBQW1CLHlEQUFXO0FBQzlCO0FBQ0EsbUJBQW1CLHlEQUFXO0FBQzlCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxvQkFBb0Isb0RBQU07QUFDMUI7QUFDQSx5QkFBeUIsZ0VBQVU7QUFDbkM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHdCQUF3QixvREFBTTtBQUM5QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQjtBQUNyQjtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBLGFBQWE7QUFDYixTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBLENBQUM7QUFDcUI7QUFDdEIsc0M7Ozs7Ozs7Ozs7OztBQzdaQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUF5TTtBQUMzRjtBQUNsRjtBQUNjO0FBQ0U7QUFDSztBQUNiO0FBQ2M7QUFDSDtBQUN2QjtBQUN4QixpQzs7Ozs7Ozs7Ozs7O0FDVkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFpQztBQUNvQztBQUM5QjtBQUNoQztBQUNQO0FBQ087QUFDUCw4REFBOEQsOENBQWdCO0FBQzlFO0FBQ0E7QUFDQTtBQUNBLHlFQUF5RSxlQUFlLEVBQUU7QUFDMUY7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsdUJBQXVCLDhDQUFnQjtBQUN2QztBQUNBO0FBQ0EsMkRBQTJELGVBQWUsRUFBRTtBQUM1RTtBQUNBO0FBQ0EsOENBQThDLDhDQUFnQjtBQUM5RDtBQUNBO0FBQ0E7QUFDQTtBQUNPO0FBQ1A7QUFDQTtBQUNBO0FBQ0EsMEJBQTBCLG1FQUF1QixFQUFFLHlEQUFhO0FBQ2hFO0FBQ0EsSUFBSSxvREFBTTtBQUNWO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ087QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0EsdUM7Ozs7Ozs7Ozs7OztBQ2xFQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw0QkFBNEIsdUJBQXVCO0FBQ25EO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7QUFDMkI7QUFDNUIsNEM7Ozs7Ozs7Ozs7OztBQy9CQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQWlDO0FBQ29DO0FBQ1U7QUFDL0U7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esa0NBQWtDLGVBQWU7QUFDakQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxRQUFRLDJFQUF1QjtBQUMvQixzQkFBc0IsaUVBQWE7QUFDbkM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQSxZQUFZLG9EQUFNLHFFQUFxRSx5RUFBbUI7QUFDMUc7QUFDQTtBQUNBO0FBQ0EsWUFBWSxvREFBTSxnRkFBZ0YseUVBQW1CO0FBQ3JIO0FBQ0E7QUFDQTtBQUNBLFlBQVksb0RBQU0sNEVBQTRFLHlFQUFtQjtBQUNqSDtBQUNBO0FBQ0E7QUFDQSxZQUFZLG9EQUFNLGlGQUFpRix5RUFBbUI7QUFDdEg7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpRUFBaUUsUUFBUSx1RUFBaUIsbUJBQW1CLEVBQUU7QUFDL0csU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx3RUFBd0UsUUFBUSx1RUFBaUIsZUFBZSxFQUFFO0FBQ2xIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx3RUFBd0UsUUFBUSx1RUFBaUIsZUFBZSxFQUFFO0FBQ2xIO0FBQ0E7QUFDQTtBQUNBLHVDQUF1QyxvQkFBb0I7QUFDM0Q7QUFDQTtBQUNBLHVCQUF1Qiw4Q0FBZ0I7QUFDdkMsc0JBQXNCLDhDQUFnQjtBQUN0QywwQkFBMEIsOENBQWdCO0FBQzFDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esb0ZBQW9GO0FBQ3BGO0FBQ0E7QUFDQTtBQUNBLGdCQUFnQixvREFBTSw2Q0FBNkMseUVBQW1CO0FBQ3RGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsWUFBWSxvREFBTSx5Q0FBeUMseUVBQW1CO0FBQzlFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDO0FBQ3lCO0FBQzFCLDBDOzs7Ozs7Ozs7Ozs7QUMzSkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBc0Q7QUFDSjtBQUNsRCxpQzs7Ozs7Ozs7Ozs7O0FDRkE7QUFBQTtBQUFBO0FBQWdEO0FBQ2hEO0FBQ087QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw4QkFBOEIscUVBQWU7QUFDN0MsU0FBUztBQUNUO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0EsK0JBQStCLGdCQUFnQixJQUFJLFlBQVksSUFBSSxTQUFTO0FBQzVFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQzs7Ozs7Ozs7Ozs7O0FDaENBO0FBQUE7QUFBQTtBQUFBO0FBQTRDO0FBQ0w7QUFDdkM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDTztBQUNQO0FBQ0EsUUFBUSxvREFBTTtBQUNkO0FBQ0EsY0FBYyxpRUFBYTtBQUMzQjtBQUNBO0FBQ0E7QUFDQSwrQjs7Ozs7Ozs7Ozs7O0FDakJBO0FBQUE7QUFBQTtBQUFBO0FBQXVDO0FBQ0s7QUFDNUM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWUseURBQVc7QUFDMUI7QUFDQSxvQkFBb0Isb0RBQU07QUFDMUIsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlLHlEQUFXO0FBQzFCO0FBQ0E7QUFDQSxDQUFDO0FBQ3dCO0FBQ3pCLGdDOzs7Ozs7Ozs7Ozs7QUN4QkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFpQztBQUMwRTtBQUMzRTtBQUNoQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ087QUFDUDtBQUNBO0FBQ0EsU0FBUyw2QkFBNkI7QUFDdEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EseURBQXlELFlBQVk7QUFDckU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrQkFBK0IsYUFBYSw0Q0FBSyxHQUFHO0FBQ3BELGtDQUFrQyx3QkFBd0I7QUFDMUQ7QUFDQSxrQkFBa0IsWUFBWTtBQUM5QjtBQUNBLDBCQUEwQiwrQkFBK0I7QUFDekQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHdCQUF3Qix1QkFBdUI7QUFDL0M7QUFDQTtBQUNBO0FBQ0E7QUFDQSxnREFBZ0QsOENBQWdCO0FBQ2hFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esb0JBQW9CLDRDQUFLO0FBQ3pCO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwyQ0FBMkMsMkRBQUs7QUFDaEQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMERBQTBELDhDQUFnQixHQUFHLGNBQWMsb0JBQW9CO0FBQy9HO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDJDQUEyQywyREFBSztBQUNoRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw2REFBNkQsOENBQWdCLEdBQUcsY0FBYyxvQkFBb0I7QUFDbEg7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMkNBQTJDLDJEQUFLO0FBQ2hELGtEQUFrRCw4Q0FBZ0IsR0FBRyxTQUFTLG9CQUFvQjtBQUNsRztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHlFQUF5RTtBQUN6RTtBQUNBO0FBQ0E7QUFDQSx3QkFBd0IscUVBQWU7QUFDdkMsK0JBQStCLDhDQUFnQixFQUFFLHVCQUF1QjtBQUN4RTtBQUNBLGNBQWMsb0VBQWMsY0FBYyxpREFBaUQsRUFBRTtBQUM3RjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxZQUFZLG9EQUFNO0FBQ2xCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHdCQUF3Qix1QkFBdUI7QUFDL0M7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFFBQVEsb0RBQU07QUFDZDtBQUNBO0FBQ0EsQ0FBQztBQUNjO0FBQ2Y7QUFDTztBQUNQLGtCQUFrQixxRUFBZTtBQUNqQztBQUNBLHNCQUFzQjtBQUN0QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxRQUFRLCtEQUFTO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDhFQUE4RSw0Q0FBSztBQUNuRjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsMkJBQTJCLFVBQVU7QUFDckM7QUFDQTtBQUNBO0FBQ0E7QUFDTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx5QkFBeUIsVUFBVTtBQUNuQztBQUNBO0FBQ0E7QUFDTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0I7Ozs7Ozs7Ozs7OztBQzNjQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUF5RDtBQUNnRDtBQUN6RyxpQzs7Ozs7Ozs7Ozs7O0FDRkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFpQztBQUN3RTtBQUN6RztBQUNBLHdDQUF3Qyx5QkFBeUI7QUFDakU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFEQUFxRCxtQkFBbUI7QUFDeEU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBLGtDQUFrQyxtQkFBbUI7QUFDckQ7QUFDQTtBQUNBO0FBQ0EsK0JBQStCLFdBQVc7QUFDMUMsbUJBQW1CLHlEQUFXO0FBQzlCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHVDQUF1Qyw4Q0FBZ0IsR0FBRztBQUMxRCxvQkFBb0IsZ0VBQVU7QUFDOUI7QUFDQSxnREFBZ0QsdUZBQXVGLEVBQUU7QUFDekk7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQiw4Q0FBZ0IsR0FBRztBQUN4QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCLDhDQUFnQixHQUFHLHNCQUFzQjtBQUM5RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHNCQUFzQiw4Q0FBZ0IsR0FBRztBQUN6QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esc0JBQXNCLDhDQUFnQixHQUFHLHVCQUF1QjtBQUNoRTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZ0NBQWdDO0FBQ2hDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHlCQUF5Qiw4Q0FBZ0IsR0FBRywwQkFBMEI7QUFDdEU7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxvQ0FBb0MsOENBQWdCO0FBQ3BELDZCQUE2Qiw4Q0FBZ0IsR0FBRztBQUNoRCw4QkFBOEIsOENBQWdCLEdBQUc7QUFDakQsaUNBQWlDLDhDQUFnQixHQUFHO0FBQ3BEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx3Q0FBd0MsOENBQWdCO0FBQ3hEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHlCQUF5Qiw4Q0FBZ0IsR0FBRztBQUM1QywwQkFBMEIsOENBQWdCLEdBQUc7QUFDN0MsNkJBQTZCLDhDQUFnQixHQUFHO0FBQ2hEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCLG1FQUFhO0FBQzlCO0FBQ0E7QUFDQSx5QkFBeUIsOENBQWdCLEdBQUc7QUFDNUMsMEJBQTBCLDhDQUFnQixHQUFHO0FBQzdDLDZCQUE2Qiw4Q0FBZ0IsR0FBRztBQUNoRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsK0JBQStCLDhDQUFnQixFQUFFLFlBQVkscUVBQWUsSUFBSTtBQUNoRjtBQUNBO0FBQ0Esa0JBQWtCLDhDQUFnQjtBQUNsQyxrQkFBa0IsOENBQWdCO0FBQ2xDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwwQkFBMEIsOENBQWdCLEdBQUc7QUFDN0M7QUFDQTtBQUNBLHlCQUF5Qiw4Q0FBZ0IsR0FBRztBQUM1QztBQUNBO0FBQ0EseUJBQXlCLDhDQUFnQixHQUFHO0FBQzVDO0FBQ0E7QUFDQSw2QkFBNkIsOENBQWdCLEdBQUc7QUFDaEQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDZCQUE2Qiw4Q0FBZ0IsRUFBRSxzQ0FBc0M7QUFDckY7QUFDQTtBQUNBLDRCQUE0Qiw4Q0FBZ0I7QUFDNUM7QUFDQSwyQ0FBMkMsOENBQWdCO0FBQzNEO0FBQ0E7QUFDQSxDQUFDO0FBQ2dCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCLHFFQUFlO0FBQ2hDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDTztBQUNQO0FBQ0E7QUFDQSxpQzs7Ozs7Ozs7Ozs7O0FDM1dBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBaUM7QUFDVztBQUM1QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG9CQUFvQix1QkFBdUI7QUFDM0M7QUFDQTtBQUNBLGNBQWMsaUVBQWE7QUFDM0I7QUFDQTtBQUNBLHNDQUFzQyw4Q0FBZ0I7QUFDdEQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ087QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esd0RBQXdELGlDQUFpQztBQUN6Rix1REFBdUQsOENBQWdCLEVBQUUscUVBQXFFO0FBQzlJO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ087QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ087QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ087QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ087QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ087QUFDUDtBQUNBLG9CQUFvQix1QkFBdUI7QUFDM0M7QUFDQTtBQUNBLDRCQUE0Qiw4Q0FBZ0I7QUFDNUM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ087QUFDUCx5Q0FBeUMsOENBQWdCLEdBQUc7QUFDNUQ7QUFDQSxpQzs7Ozs7Ozs7Ozs7O0FDMUxBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQXNDO0FBQ0E7QUFDSjtBQUNsQyxpQzs7Ozs7Ozs7Ozs7O0FDSEE7QUFBQTtBQUFBO0FBQ087QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDLDRCQUE0QjtBQUM3QixvQzs7Ozs7Ozs7Ozs7O0FDWkE7QUFBQTtBQUFBO0FBQ087QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDLDRCQUE0QjtBQUM3QjtBQUNBO0FBQ0E7QUFDQTtBQUNBLDZDQUE2QyxlQUFlO0FBQzVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQyw0QkFBNEI7QUFDN0Isb0M7Ozs7Ozs7Ozs7OztBQ2pEQTtBQUFBO0FBQUE7QUFDTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQyx3QkFBd0I7QUFDekI7QUFDQTtBQUNBO0FBQ0E7QUFDQSwyQ0FBMkMsYUFBYTtBQUN4RDtBQUNBO0FBQ0Esb0NBQW9DLHFCQUFxQjtBQUN6RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQyx3QkFBd0I7QUFDekIsa0M7Ozs7Ozs7Ozs7OztBQzFDQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDTztBQUNQO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBLGlDOzs7Ozs7Ozs7Ozs7QUNWQTtBQUFBO0FBQUE7QUFBQTtBQUFpQztBQUNLO0FBQ3RDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxzQ0FBc0Msc0JBQXNCO0FBQzVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHNCQUFzQixrREFBVztBQUNqQztBQUNBLGlCQUFpQiw0Q0FBYztBQUMvQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDhCQUE4Qix1R0FBdUc7QUFDckk7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwwQkFBMEIsa0RBQVc7QUFDckM7QUFDQSxTQUFTO0FBQ1Q7QUFDQSxzQkFBc0Isa0RBQVc7QUFDakM7QUFDQTtBQUNBLHNCQUFzQixrREFBVztBQUNqQztBQUNBO0FBQ0Esc0JBQXNCLGtEQUFXO0FBQ2pDO0FBQ0E7QUFDQTtBQUNBLENBQUM7QUFDYztBQUNmLCtCOzs7Ozs7Ozs7Ozs7QUN2RkE7QUFBQTtBQUFBO0FBQUE7QUFBaUM7QUFDVztBQUM1QztBQUNBO0FBQ0EsSUFBSSwrQ0FBaUI7QUFDckI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsUUFBUSxnRUFBYztBQUN0QjtBQUNBO0FBQ0E7QUFDQSxDQUFDO0FBQ3NCO0FBQ3ZCLGlDOzs7Ozs7Ozs7Ozs7QUNqQkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUF3QjtBQUNBO0FBQ0g7QUFDSTtBQUNGO0FBQ0E7QUFDRTtBQUNGO0FBQ1M7QUFDUDtBQUNFO0FBQ0c7QUFDRDtBQUNQO0FBQ3RCLGlDOzs7Ozs7Ozs7Ozs7QUNkQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDaUM7QUFDYTtBQUNaO0FBQ3dCO0FBQzFCO0FBQ2tDO0FBQ2xFLGFBQWEsNkRBQWU7QUFDNUI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFlBQVksOENBQU07QUFDbEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHNCQUFzQiw4Q0FBZ0IsdUNBQXVDLFVBQVU7QUFDdkY7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGdCQUFnQiw4Q0FBTSx1RkFBdUYsNkRBQWU7QUFDNUg7QUFDQTtBQUNBO0FBQ0EsbUJBQW1CLFFBQVEsZ0JBQWdCO0FBQzNDO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCLDBCQUEwQjtBQUMzQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsUUFBUSxvREFBSTtBQUNaO0FBQ0E7QUFDQSxnQ0FBZ0MsdUJBQXVCO0FBQ3ZEO0FBQ0E7QUFDQSw0Q0FBNEMsMkJBQTJCO0FBQ3ZFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1QsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLFNBQVMscUVBQW1CO0FBQzVCO0FBQ0E7QUFDQSxJQUFJLG9EQUFJO0FBQ1I7QUFDQTtBQUNBLDRCQUE0Qix1QkFBdUI7QUFDbkQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7QUFDQTtBQUNBLHFDQUFxQyw4Q0FBZ0IsR0FBRztBQUN4RDtBQUNBLHlDQUF5Qyw4Q0FBZ0IsR0FBRyxzQkFBc0IsK0NBQStDO0FBQ2pJO0FBQ0EsYUFBYTtBQUNiLHlDQUF5Qyw4Q0FBZ0IsR0FBRyxzQkFBc0IseUNBQXlDO0FBQzNIO0FBQ0EsYUFBYTtBQUNiO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLCtCQUErQixnQkFBZ0I7QUFDL0MsK0JBQStCLHdEQUFZO0FBQzNDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtCQUErQixnQkFBZ0I7QUFDL0M7QUFDQTtBQUNBO0FBQ0EsK0JBQStCLHdEQUFZO0FBQzNDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSSxvREFBSTtBQUNSO0FBQ0E7QUFDQSw0QkFBNEIsdUJBQXVCO0FBQ25EO0FBQ0E7QUFDQSwyQkFBMkI7QUFDM0I7QUFDQTtBQUNBLHdCQUF3QixvREFBUTtBQUNoQztBQUNBO0FBQ0E7QUFDQSxnQkFBZ0Isb0RBQVE7QUFDeEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUI7QUFDckI7QUFDQTtBQUNBO0FBQ0EsZ0JBQWdCLG9EQUFJO0FBQ3BCO0FBQ0E7QUFDQSx3Q0FBd0MsdUJBQXVCO0FBQy9EO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMLElBQUksb0RBQUk7QUFDUjtBQUNBO0FBQ0EsNEJBQTRCLHVCQUF1QjtBQUNuRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVMsaUVBQWU7QUFDeEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHdCQUF3Qix1QkFBdUI7QUFDL0M7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw0QkFBNEIsdUJBQXVCO0FBQ25EO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUksb0RBQUk7QUFDUixJQUFJLG9EQUFJO0FBQ1I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxRQUFRLG9EQUFJO0FBQ1o7QUFDQTtBQUNBO0FBQ0Esd0JBQXdCLG9EQUFJO0FBQzVCO0FBQ0E7QUFDQTtBQUNBO0FBQ0EseUJBQXlCO0FBQ3pCO0FBQ0E7QUFDQSx3QkFBd0Isb0RBQUk7QUFDNUI7QUFDQTtBQUNBO0FBQ0E7QUFDQSx5QkFBeUI7QUFDekI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNULFFBQVEsb0RBQUk7QUFDWjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVCxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDhCQUE4QixrQkFBa0I7QUFDaEQ7QUFDQSxrQ0FBa0M7QUFDbEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EseUJBQXlCLDJCQUEyQjtBQUNwRCxhQUFhO0FBQ2I7QUFDQTtBQUNBLHFCQUFxQiwyQkFBMkI7QUFDaEQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxzQzs7Ozs7Ozs7Ozs7O0FDMWNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0EsSUFBSSxjQUFjO0FBQ2xCO0FBQ0E7QUFDQTtBQUNBO0FBQ087QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUksbUJBQW1CO0FBQ3ZCO0FBQ0E7QUFDQTtBQUNBO0FBQ087QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUksaUJBQWlCO0FBQ3JCO0FBQ0E7QUFDQTtBQUNBO0FBQ087QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUkscUJBQXFCO0FBQ3pCO0FBQ0E7QUFDQTtBQUNBO0FBQ087QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUksZUFBZTtBQUNuQjtBQUNBO0FBQ0E7QUFDQTtBQUNPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJLGtCQUFrQjtBQUN0QjtBQUNBO0FBQ0E7QUFDQTtBQUNPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJLG9CQUFvQjtBQUN4QjtBQUNBO0FBQ0E7QUFDQTtBQUNPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJLGNBQWM7QUFDbEI7QUFDQTtBQUNBO0FBQ0E7QUFDTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJLGdCQUFnQjtBQUNwQjtBQUNBO0FBQ0E7QUFDQTtBQUNPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUksZUFBZTtBQUNuQjtBQUNBO0FBQ0E7QUFDQTtBQUNPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ087QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJLHVCQUF1QjtBQUMzQjtBQUNBO0FBQ0E7QUFDQTtBQUNPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUksbUJBQW1CO0FBQ3ZCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw4Qjs7Ozs7Ozs7Ozs7O0FDcEpBO0FBQUE7QUFBQTtBQUF5RDtBQUN6RDtBQUNBLGFBQWEsNkRBQWU7QUFDNUI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx3QkFBd0IsdUJBQXVCO0FBQy9DO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxRQUFRLDREQUFjO0FBQ3RCLG9FQUFvRTtBQUNwRSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQSx3QkFBd0IsdUJBQXVCO0FBQy9DO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxRQUFRLDREQUFjO0FBQ3RCLHNFQUFzRTtBQUN0RSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQSx3QkFBd0IsdUJBQXVCO0FBQy9DO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxRQUFRLDREQUFjO0FBQ3RCLHdFQUF3RTtBQUN4RSxTQUFTO0FBQ1Q7QUFDQTtBQUNBLENBQUM7QUFDRDtBQUNBO0FBQ0E7QUFDa0I7QUFDbEIsa0M7Ozs7Ozs7Ozs7OztBQ2hFQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHVCQUF1Qix3QkFBd0I7QUFDL0M7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDJCQUEyQix3QkFBd0I7QUFDbkQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7QUFDZTtBQUNoQixnQzs7Ozs7Ozs7Ozs7O0FDcERBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBZ0M7QUFDSTtBQUNwQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ087QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ087QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ087QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ087QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ087QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ087QUFDUCwrQkFBK0IsZ0JBQWdCO0FBQy9DO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQixvREFBUTtBQUM3QjtBQUNBLG1CQUFtQixvQkFBb0I7QUFDdkM7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlLHlCQUF5QjtBQUN4QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLENBQUM7QUFDRDtBQUNBO0FBQ0E7QUFDTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGtDQUFrQztBQUNsQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ087QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ087QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNPO0FBQ1Asb0NBQW9DLG9CQUFvQjtBQUN4RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsOEJBQThCLFFBQVEsd0RBQVEsVUFBVSxFQUFFO0FBQzFELHlCQUF5Qix3REFBUTtBQUNqQztBQUNBO0FBQ0EsOEJBQThCLFFBQVEsd0RBQVEsVUFBVSxFQUFFO0FBQzFEO0FBQ0EsZ0M7Ozs7Ozs7Ozs7Ozs7QUNoWUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBaUM7QUFDOEU7QUFDakY7QUFDNkI7QUFDdkI7QUFDcEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ087QUFDUDtBQUNBO0FBQ0E7QUFDQSxvQkFBb0Isd0VBQXdFLEVBQUU7QUFDOUY7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsUUFBUSxtREFBTztBQUNmO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsUUFBUSxtREFBTztBQUNmO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw0QkFBNEIscURBQVM7QUFDckMsa0JBQWtCLDhEQUFnQjtBQUNsQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBbUMscURBQVM7QUFDNUMsa0JBQWtCLDhEQUFnQjtBQUNsQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxrREFBa0Qsd0RBQVk7QUFDOUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ087QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBLDJCQUEyQixXQUFXO0FBQ3RDLDZCQUE2QixzQkFBc0I7QUFDbkQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFdBQVcsdURBQVc7QUFDdEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxRQUFRLDREQUFnQjtBQUN4QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLCtCQUErQiw2REFBZTtBQUM5QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ087QUFDUCwyQkFBMkIsbUJBQW1CO0FBQzlDLDBCQUEwQixZQUFZLDBDQUFJLEdBQUc7QUFDN0M7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsUUFBUSx1REFBVztBQUNuQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDTztBQUNQO0FBQ0E7QUFDQSx1RUFBdUUsZ0NBQWdDLEVBQUU7QUFDekc7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDTztBQUNQLCtCQUErQixnQkFBZ0I7QUFDL0M7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlLHdEQUFRO0FBQ3ZCO0FBQ0Esd0NBQXdDLGtCQUFrQjtBQUMxRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWUsd0RBQVE7QUFDdkI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDTztBQUNQO0FBQ0EsUUFBUSx5REFBYTtBQUNyQjtBQUNBO0FBQ0E7QUFDQSwwQkFBMEIsOENBQWdCLG1DQUFtQyxVQUFVO0FBQ3ZGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHVCQUF1QixRQUFRLGdCQUFnQjtBQUMvQztBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQiwwQkFBMEI7QUFDL0M7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGtDOzs7Ozs7Ozs7Ozs7O0FDNVRBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxrQ0FBa0MsUUFBUTtBQUMxQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxjQUFjLE1BQU07QUFDcEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwyQ0FBMkMsSUFBSTtBQUMvQztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ087QUFDUDtBQUNBLG9CQUFvQix1QkFBdUI7QUFDM0M7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQ0FBaUMsOEJBQThCO0FBQy9EO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrRUFBK0UsWUFBWSxFQUFFO0FBQzdGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxVQUFVLG9CQUFvQjtBQUM5QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsVUFBVSxVQUFVO0FBQ3BCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQkFBbUIsWUFBWTtBQUMvQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQ0FBaUMsc0JBQXNCO0FBQ3ZEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDTztBQUNQO0FBQ0E7QUFDQTtBQUNBLDZFQUE2RSxZQUFZLEVBQUU7QUFDM0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ087QUFDUDtBQUNBLG9CQUFvQix1QkFBdUI7QUFDM0M7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZ0M7Ozs7Ozs7Ozs7OztBQzdKQTtBQUFBO0FBQU8sZ0RBQWdELGdCQUFnQixrREFBa0Q7QUFDekg7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG9DOzs7Ozs7Ozs7Ozs7QUNyQkE7QUFBQTtBQUFBO0FBQUE7QUFBc0M7QUFDTTtBQUM1QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1CQUFtQix3REFBVyxZQUFZLGtEQUFXO0FBQ3JEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSwrQkFBK0IsMkJBQTJCLEVBQUU7QUFDNUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2IsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQkFBbUIsd0RBQVc7QUFDOUI7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2IsWUFBWSx3REFBVztBQUN2QjtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBLGFBQWE7QUFDYixTQUFTO0FBQ1Q7QUFDQTtBQUNBLENBQUM7QUFDd0I7QUFDekIseUM7Ozs7Ozs7Ozs7OztBQ2xGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBMEM7QUFDMUM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDTztBQUNQLHlCQUF5QixTQUFTO0FBQ2xDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1CQUFtQjtBQUNuQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCLEtBQUs7QUFDMUI7QUFDQTtBQUNBLHNCQUFzQixLQUFLO0FBQzNCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1CQUFtQixrQkFBa0I7QUFDckM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ087QUFDUCxTQUFTLG9EQUFRO0FBQ2pCO0FBQ0E7QUFDQSxRQUFRLG9EQUFRO0FBQ2hCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esa0M7Ozs7Ozs7Ozs7OztBQzdGQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQWtDO0FBQ087QUFDekM7QUFDQTtBQUNBLElBQUkseUJBQXlCO0FBQzdCO0FBQ0E7QUFDQTtBQUNPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUksdUJBQXVCO0FBQzNCO0FBQ0E7QUFDQTtBQUNPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSSwyQkFBMkI7QUFDL0I7QUFDQTtBQUNBO0FBQ087QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSSxvQkFBb0I7QUFDeEI7QUFDQTtBQUNBO0FBQ087QUFDUCxxQkFBcUIsNkRBQWU7QUFDcEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSw0Q0FBNEMsdUJBQXVCO0FBQ25FO0FBQ0E7QUFDQTtBQUNBLElBQUksMEJBQTBCO0FBQzlCO0FBQ0E7QUFDQTtBQUNPO0FBQ1A7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCLDZEQUFlO0FBQ2hDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsWUFBWSw4Q0FBTTtBQUNsQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJLGdDQUFnQztBQUNwQztBQUNBO0FBQ0E7QUFDTztBQUNQO0FBQ0Esa0NBQWtDLDZEQUFlO0FBQ2pEO0FBQ0E7QUFDQTtBQUNBLElBQUksNkJBQTZCO0FBQ2pDO0FBQ0E7QUFDQTtBQUNPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSSxzQkFBc0I7QUFDMUI7QUFDQTtBQUNBO0FBQ087QUFDUDtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsNkRBQWU7QUFDaEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0Esb0M7Ozs7Ozs7Ozs7OztBQzdLQTtBQUFBO0FBQUE7QUFBa0M7QUFDbEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQyx3QkFBd0I7QUFDekI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxnQkFBZ0Isc0RBQVU7QUFDMUI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7QUFDQSxhQUFhO0FBQ2IsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCLGFBQWE7QUFDYixTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0EseUNBQXlDLFlBQVksRUFBRTtBQUN2RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2IsU0FBUztBQUNUO0FBQ0E7QUFDQSxDQUFDO0FBQ3NCO0FBQ3ZCLHVDOzs7Ozs7Ozs7Ozs7QUNsTUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFYTs7QUFFYjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsQ0FBQztBQUNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQztBQUNEO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxpQkFBaUIsc0JBQXNCO0FBQ3ZDOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWU7QUFDZjtBQUNBO0FBQ0E7QUFDQTtBQUNBLGNBQWM7QUFDZDs7QUFFQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBLG1CQUFtQixTQUFTO0FBQzVCO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0EsS0FBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxlQUFlO0FBQ2Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7O0FBRUEsaUNBQWlDLFFBQVE7QUFDekM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLG1CQUFtQixpQkFBaUI7QUFDcEM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQSxzQ0FBc0MsUUFBUTtBQUM5QztBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsaUJBQWlCLE9BQU87QUFDeEI7QUFDQTtBQUNBOztBQUVBO0FBQ0EsUUFBUSx5QkFBeUI7QUFDakM7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSxpQkFBaUIsZ0JBQWdCO0FBQ2pDO0FBQ0E7QUFDQTtBQUNBOzs7Ozs7Ozs7Ozs7QUM3YkE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7O0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLHVCQUF1QixzQkFBc0I7QUFDN0M7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUI7QUFDckI7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLHFDQUFxQzs7QUFFckM7QUFDQTtBQUNBOztBQUVBLDJCQUEyQjtBQUMzQjtBQUNBO0FBQ0E7QUFDQSw0QkFBNEIsVUFBVTs7Ozs7Ozs7Ozs7O0FDdkx0QztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsZ0JBQWdCO0FBQ2hCO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxjQUFjO0FBQ2QsS0FBSztBQUNMLGNBQWM7QUFDZDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHlEQUF5RDtBQUN6RDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsWUFBWTtBQUNaOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxPQUFPO0FBQ1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxXQUFXO0FBQ1g7QUFDQSxXQUFXO0FBQ1g7O0FBRUE7QUFDQTtBQUNBLHdDQUF3QyxXQUFXO0FBQ25EO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSwyQkFBMkI7QUFDM0I7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBLFNBQVM7QUFDVDtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0Esb0NBQW9DLGNBQWM7QUFDbEQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxLQUFLO0FBQ0w7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsaUNBQWlDLGtCQUFrQjtBQUNuRDtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0EsaUJBQWlCOztBQUVqQjtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSx3QkFBd0IsaUJBQWlCO0FBQ3pDO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0EsWUFBWTtBQUNaO0FBQ0E7O0FBRUE7QUFDQSxZQUFZO0FBQ1o7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLOztBQUVMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLEtBQUs7O0FBRUw7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQSw4Q0FBOEMsUUFBUTtBQUN0RDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7O0FBRUEsV0FBVztBQUNYO0FBQ0E7QUFDQTs7QUFFQSxXQUFXO0FBQ1g7QUFDQTtBQUNBOztBQUVBLFdBQVc7QUFDWDtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7O0FBRUw7QUFDQSw4Q0FBOEMsUUFBUTtBQUN0RDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBLEtBQUs7O0FBRUw7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsT0FBTztBQUNQO0FBQ0E7QUFDQTtBQUNBLE9BQU87QUFDUDtBQUNBOztBQUVBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBLDhDQUE4QyxRQUFRO0FBQ3REO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBLDhDQUE4QyxRQUFRO0FBQ3REO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7O0FBRUw7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsQ0FBQztBQUNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsRUFBRSxLQUEwQixvQkFBb0IsU0FBRTtBQUNsRDs7QUFFQTtBQUNBO0FBQ0EsQ0FBQztBQUNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7QUN4dEJBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0EsK0RBQStEO0FBQy9EO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLFVBQVUsZ0JBQWdCLHNDQUFzQyxpQkFBaUIsRUFBRTtBQUNuRix5QkFBeUIsdURBQXVEO0FBQ2hGO0FBQ0E7O0FBRU87QUFDUDtBQUNBLG1CQUFtQixzQkFBc0I7QUFDekM7QUFDQTs7QUFFTztBQUNQO0FBQ0EsZ0RBQWdELE9BQU87QUFDdkQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBLDREQUE0RCxjQUFjO0FBQzFFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRU87QUFDUDtBQUNBO0FBQ0EsNENBQTRDLFFBQVE7QUFDcEQ7QUFDQTs7QUFFTztBQUNQLG1DQUFtQyxvQ0FBb0M7QUFDdkU7O0FBRU87QUFDUDtBQUNBOztBQUVPO0FBQ1AsMkJBQTJCLCtEQUErRCxnQkFBZ0IsRUFBRSxFQUFFO0FBQzlHO0FBQ0EsbUNBQW1DLE1BQU0sNkJBQTZCLEVBQUUsWUFBWSxXQUFXLEVBQUU7QUFDakcsa0NBQWtDLE1BQU0saUNBQWlDLEVBQUUsWUFBWSxXQUFXLEVBQUU7QUFDcEcsK0JBQStCLHFGQUFxRjtBQUNwSDtBQUNBLEtBQUs7QUFDTDs7QUFFTztBQUNQLGFBQWEsNkJBQTZCLDBCQUEwQixhQUFhLEVBQUUscUJBQXFCO0FBQ3hHLGdCQUFnQixxREFBcUQsb0VBQW9FLGFBQWEsRUFBRTtBQUN4SixzQkFBc0Isc0JBQXNCLHFCQUFxQixHQUFHO0FBQ3BFO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHVDQUF1QztBQUN2QyxrQ0FBa0MsU0FBUztBQUMzQyxrQ0FBa0MsV0FBVyxVQUFVO0FBQ3ZELHlDQUF5QyxjQUFjO0FBQ3ZEO0FBQ0EsNkdBQTZHLE9BQU8sVUFBVTtBQUM5SCxnRkFBZ0YsaUJBQWlCLE9BQU87QUFDeEcsd0RBQXdELGdCQUFnQixRQUFRLE9BQU87QUFDdkYsOENBQThDLGdCQUFnQixnQkFBZ0IsT0FBTztBQUNyRjtBQUNBLGlDQUFpQztBQUNqQztBQUNBO0FBQ0EsU0FBUyxZQUFZLGFBQWEsT0FBTyxFQUFFLFVBQVUsV0FBVztBQUNoRSxtQ0FBbUMsU0FBUztBQUM1QztBQUNBOztBQUVPO0FBQ1A7QUFDQTs7QUFFTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxvQkFBb0I7QUFDcEI7QUFDQTtBQUNBO0FBQ0E7O0FBRU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxtQkFBbUIsTUFBTSxnQkFBZ0I7QUFDekM7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUIsc0JBQXNCO0FBQ3ZDO0FBQ0E7QUFDQTs7QUFFTztBQUNQLDRCQUE0QixzQkFBc0I7QUFDbEQ7QUFDQTtBQUNBOztBQUVPO0FBQ1AsaURBQWlELFFBQVE7QUFDekQsd0NBQXdDLFFBQVE7QUFDaEQsd0RBQXdELFFBQVE7QUFDaEU7QUFDQTtBQUNBOztBQUVPO0FBQ1A7QUFDQTs7QUFFTztBQUNQO0FBQ0E7QUFDQSxpQkFBaUIsc0ZBQXNGLGFBQWEsRUFBRTtBQUN0SCxzQkFBc0IsZ0NBQWdDLHFDQUFxQywwQ0FBMEMsRUFBRSxFQUFFLEdBQUc7QUFDNUksMkJBQTJCLE1BQU0sZUFBZSxFQUFFLFlBQVksb0JBQW9CLEVBQUU7QUFDcEYsc0JBQXNCLG9HQUFvRztBQUMxSCw2QkFBNkIsdUJBQXVCO0FBQ3BELDRCQUE0Qix3QkFBd0I7QUFDcEQsMkJBQTJCLHlEQUF5RDtBQUNwRjs7QUFFTztBQUNQO0FBQ0EsaUJBQWlCLDRDQUE0QyxTQUFTLEVBQUUscURBQXFELGFBQWEsRUFBRTtBQUM1SSx5QkFBeUIsNkJBQTZCLG9CQUFvQixnREFBZ0QsZ0JBQWdCLEVBQUUsS0FBSztBQUNqSjs7QUFFTztBQUNQO0FBQ0E7QUFDQSwyR0FBMkcsc0ZBQXNGLGFBQWEsRUFBRTtBQUNoTixzQkFBc0IsOEJBQThCLGdEQUFnRCx1REFBdUQsRUFBRSxFQUFFLEdBQUc7QUFDbEssNENBQTRDLHNDQUFzQyxVQUFVLG9CQUFvQixFQUFFLEVBQUUsVUFBVTtBQUM5SDs7QUFFTztBQUNQLGdDQUFnQyx1Q0FBdUMsYUFBYSxFQUFFLEVBQUUsT0FBTyxrQkFBa0I7QUFDakg7QUFDQTs7QUFFTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFTztBQUNQLDRDQUE0QztBQUM1Qzs7QUFFTztBQUNQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRU87QUFDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7OztBQ3BOQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBLENBQUM7QUFDRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLDRDQUE0Qzs7QUFFNUM7Ozs7Ozs7Ozs7OztBQ25CQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEdBQUc7QUFDSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsR0FBRztBQUNIO0FBQ0E7QUFDQSxHQUFHO0FBQ0g7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7QUN2QkE7QUFBQTtBQUFBOztBQUVBLElBQU1BLFVBQVUsR0FBRyxTQUFiQSxVQUFhLENBQUNDLEdBQUQ7QUFBQSxTQUFTQSxHQUFULGFBQVNBLEdBQVQsdUJBQVNBLEdBQUcsQ0FBRUMsT0FBTCxDQUFhLEtBQWIsRUFBb0IsRUFBcEIsQ0FBVDtBQUFBLENBQW5COztBQUNBLElBQU1DLEtBQUssR0FBRyxTQUFSQSxLQUFRLENBQUNGLEdBQUQ7QUFBQSxTQUFTQSxHQUFULGFBQVNBLEdBQVQsdUJBQVNBLEdBQUcsQ0FBRUMsT0FBTCxDQUFhLHFCQUFiLEVBQW9DLFVBQXBDLENBQVQ7QUFBQSxDQUFkOztBQUNBLElBQU1FLFFBQVEsR0FBRyxTQUFYQSxRQUFXLENBQUNILEdBQUQ7QUFBQSxTQUFTSSxRQUFRLENBQUNKLEdBQUQsRUFBTSxFQUFOLENBQWpCO0FBQUEsQ0FBakI7O0FBQ0EsSUFBTUssVUFBVSxHQUFHLFNBQWJBLFVBQWEsQ0FBQ0wsR0FBRDtBQUFBLFNBQVMsQ0FBQyxVQUFHQSxHQUFILEVBQVNNLEtBQVQsQ0FBZSxXQUFmLENBQUQsRUFBOEJDLElBQTlCLENBQW1DLEVBQW5DLENBQVQ7QUFBQSxDQUFuQjs7QUFFZTtBQUNiTCxPQUFLLEVBQUxBLEtBRGE7QUFFYkMsVUFBUSxFQUFSQSxRQUZhO0FBR2JFLFlBQVUsRUFBVkEsVUFIYTtBQUliTixZQUFVLEVBQVZBLFVBSmE7QUFLYlMsT0FBSyxFQUFFQyxpREFMTTtBQU1iQyxPQUFLLEVBQUVELGlEQU5NO0FBT2JFLFVBQVEsRUFBRUMsb0RBUEc7QUFRYkMsZUFBYSxFQUFFRCxvREFBYUE7QUFSZixDQUFmLEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNQQTtBQUNBO0FBQ0E7QUFDQTs7SUFJcUJFLE07Ozs7O0FBQ25CLG9CQUF5QjtBQUFBOztBQUFBLFFBQWJDLE1BQWEsdUVBQUosRUFBSTs7QUFBQTs7QUFDdkI7QUFDQSxVQUFLQSxNQUFMLEdBQWNBLE1BQWQ7O0FBQ0EsVUFBS0Msc0JBQUwsQ0FBNEIsWUFBNUI7O0FBSHVCO0FBSXhCOzs7Ozs0TUFFZUMsRyxFQUFLQyxPOzs7Ozs7O0FBQ25CLG9CQUFJLEtBQUtDLGVBQVQsRUFBMEI7QUFDeEIsdUJBQUtBLGVBQUwsQ0FBcUJDLEtBQXJCO0FBQ0Q7O0FBQ0QscUJBQUtELGVBQUwsR0FBdUIsSUFBSUUsZUFBSixFQUF2QjtBQUNRQyxzQixHQUFXLEtBQUtILGUsQ0FBaEJHLE07O3VCQUNlQyxLQUFLLENBQUNOLEdBQUQ7QUFBUUssd0JBQU0sRUFBTkE7QUFBUixtQkFBbUJKLE9BQW5CLEU7OztBQUF0Qk0sd0I7O29CQUNEQSxRQUFRLENBQUNDLEU7Ozs7O3NCQUNOLElBQUlDLEtBQUosQ0FBVSxzQkFBVixDOzs7dUNBRVlDLE1BQU0sQ0FBQ0MsT0FBUCxDQUFlO0FBQ2pDQyxzQkFBSSxFQUFFLGtCQUQyQjtBQUVqQ0Msc0JBQUksRUFBRTtBQUYyQixpQkFBZixFQUdqQkMsSUFIaUIsQ0FHWjtBQUFBO0FBQUEsc0JBQUlDLEtBQUo7O0FBQUEseUJBQWVSLFFBQVEsQ0FBQ1MsT0FBVCxDQUFpQkMsR0FBakIsQ0FBcUIsY0FBckIsRUFBcUNDLFFBQXJDLENBQThDSCxLQUE5QyxDQUFmO0FBQUEsaUJBSFksQyxnSUFBYkksUzs7dUJBSVlaLFFBQVEsQ0FBQ1ksU0FBRCxDQUFSLEU7OztBQUFiQyxvQjs7c0JBQ0ZELFNBQVMsS0FBSyxNOzs7OztpREFDVEUseURBQVMsQ0FBQ0QsSUFBRCxDOzs7aURBRVhBLEk7Ozs7Ozs7Ozs7Ozs7Ozs7OztxQ0FHc0M7QUFBQSxVQUFuQ0UsSUFBbUMsU0FBbkNBLElBQW1DO0FBQUEsVUFBN0JDLE1BQTZCLFNBQTdCQSxNQUE2QjtBQUFBLCtCQUFyQkMsTUFBcUI7QUFBQSxVQUFyQkEsTUFBcUIsNkJBQVpDLFFBQVk7QUFDN0MsVUFBTUMsTUFBTSxHQUFHLENBQUNGLE1BQU0sQ0FBQ0csZUFBUCxJQUEwQkgsTUFBM0IsYUFBOENJLFdBQTdEO0FBQ0EsVUFBTTdDLEdBQUcsR0FBRzJDLE1BQU0sR0FBR0cseURBQVMsQ0FBQ04sTUFBRCxFQUFTQyxNQUFULENBQVosR0FBK0JQLG1EQUFHLENBQUNPLE1BQUQsRUFBU0QsTUFBTSxDQUFDTyxJQUFoQixDQUFwRDtBQUY2QyxVQUdyQ0MsVUFIcUMsR0FHVlIsTUFIVSxDQUdyQ1EsVUFIcUM7QUFBQSxVQUd6QkMsVUFIeUIsR0FHVlQsTUFIVSxDQUd6QlMsVUFIeUI7QUFJN0MsVUFBTUMsVUFBVSxHQUFHLENBQUNQLE1BQU0sR0FBR00sVUFBSCxHQUFnQkQsVUFBdkIsS0FBc0NULElBQXpEO0FBQ0EsVUFBTVksTUFBTSxHQUFHLENBQUNSLE1BQU0sR0FBR1Msc0RBQUgsR0FBaUJDLHNEQUF4QixFQUFxQ0gsVUFBckMsQ0FBZjtBQUNBLGFBQU8sQ0FBQ1gsSUFBRCxFQUFPWSxNQUFNLEdBQUdBLE1BQU0sQ0FBQ25ELEdBQUQsQ0FBVCxHQUFpQkEsR0FBOUIsQ0FBUDtBQUNEOzs7MENBRWlEO0FBQUE7O0FBQUEsK0JBQWpDeUMsTUFBaUM7QUFBQSxVQUFqQ0EsTUFBaUMsNkJBQXhCQyxRQUF3QjtBQUFBLDhCQUFkWSxLQUFjO0FBQUEsVUFBZEEsS0FBYyw0QkFBTixFQUFNO0FBQ2hELFVBQU0xQixPQUFPLEdBQUdELE1BQU0sQ0FBQ0MsT0FBUCxDQUFlMEIsS0FBZixFQUNiQyxHQURhLENBQ1Q7QUFBQTtBQUFBLFlBQUVoQixJQUFGO0FBQUEsWUFBUUMsTUFBUjs7QUFBQSxlQUFvQixNQUFJLENBQUNnQixTQUFMLENBQWU7QUFBRWpCLGNBQUksRUFBSkEsSUFBRjtBQUFRQyxnQkFBTSxFQUFOQSxNQUFSO0FBQWdCQyxnQkFBTSxFQUFOQTtBQUFoQixTQUFmLENBQXBCO0FBQUEsT0FEUyxDQUFoQjtBQUVBLGFBQU9kLE1BQU0sQ0FBQzhCLFdBQVAsQ0FDTDdCLE9BQU8sQ0FDSjJCLEdBREgsQ0FDTyxpQkFBbUI7QUFBQTs7QUFBQTtBQUFBLFlBQWpCaEIsSUFBaUI7QUFBQSxZQUFYUCxLQUFXOztBQUN0QixZQUFNaEMsR0FBRyxHQUFHc0QsS0FBSCxhQUFHQSxLQUFILHNDQUFHQSxLQUFLLENBQUdmLElBQUgsQ0FBUixnREFBRyxZQUFlbUIsT0FBM0I7QUFDQSxlQUFPLENBQUNuQixJQUFELEVBQU92QyxHQUFHLEdBQUcyRCwwREFBVSxDQUFDO0FBQUUzRCxhQUFHLEVBQUhBLEdBQUY7QUFBTzRCLGlCQUFPLEVBQVBBO0FBQVAsU0FBRCxDQUFiLEdBQWtDSSxLQUE1QyxDQUFQO0FBQ0QsT0FKSCxDQURLLENBQVA7QUFPRDs7O2lDQUUwQjtBQUFBOztBQUFBLFVBQWhCNEIsR0FBZ0IsdUVBQVZsQixRQUFVO0FBQUEsK0JBQ0YsS0FBSzNCLE1BREgsQ0FDakI4QyxLQURpQjtBQUFBLFVBQ2pCQSxLQURpQixtQ0FDVCxFQURTO0FBRXpCLFVBQU1sQixNQUFNLEdBQUcsQ0FBQ2lCLEdBQUcsQ0FBQ2hCLGVBQUosSUFBdUJnQixHQUF4QixhQUF3Q2YsV0FBdkQ7QUFDQSxhQUFPbEIsTUFBTSxDQUFDOEIsV0FBUCxDQUNMOUIsTUFBTSxDQUNIQyxPQURILENBQ1dpQyxLQURYLEVBRUdOLEdBRkgsQ0FFTyxpQkFBeUI7QUFBQTtBQUFBLFlBQXZCaEIsSUFBdUI7QUFBQTtBQUFBLFlBQWpCeEIsTUFBaUIsd0JBQVIsRUFBUTs7QUFBQSxZQUNwQitDLFNBRG9CLEdBQ0MvQyxNQURELENBQ3BCK0MsU0FEb0I7QUFBQSxZQUNUUixLQURTLEdBQ0N2QyxNQURELENBQ1R1QyxLQURTOztBQUU1QixZQUFJUSxTQUFKLEVBQWU7QUFBQSxjQUNMQyxRQURLLEdBQ2NELFNBRGQsQ0FDTEMsUUFESztBQUFBLGNBQ0toQixJQURMLEdBQ2NlLFNBRGQsQ0FDS2YsSUFETDtBQUViLGNBQU1pQixPQUFPLEdBQUdyQixNQUFNLEdBQUcsZ0ZBQUlpQixHQUFHLENBQUNLLGdCQUFKLENBQXFCRixRQUFyQixDQUFQLElBQTBDN0IsbURBQUcsQ0FBQzBCLEdBQUQsRUFBTWIsSUFBTixDQUFILElBQWtCLEVBQWxGO0FBQ0EsaUJBQU8sQ0FBQ1IsSUFBRCxFQUFPeUIsT0FBTyxDQUFDVCxHQUFSLENBQVksVUFBQ2QsTUFBRDtBQUFBLG1CQUFZLE1BQUksQ0FBQ3lCLGNBQUwsQ0FBb0I7QUFBRXpCLG9CQUFNLEVBQU5BLE1BQUY7QUFBVWEsbUJBQUssRUFBTEE7QUFBVixhQUFwQixDQUFaO0FBQUEsV0FBWixDQUFQLENBQVA7QUFDRDs7QUFDRCxlQUFPLE1BQUksQ0FBQ0UsU0FBTCxDQUFlO0FBQUVqQixjQUFJLEVBQUpBLElBQUY7QUFBUUMsZ0JBQU0sRUFBRXpCLE1BQWhCO0FBQXdCMEIsZ0JBQU0sRUFBRW1CO0FBQWhDLFNBQWYsQ0FBUDtBQUNELE9BVkgsQ0FESyxDQUFQO0FBYUQ7Ozs7Ozs7Ozs7O3VCQUdvQixLQUFLTyxTQUFMLENBQWUsS0FBS3BELE1BQUwsQ0FBWUUsR0FBM0IsQzs7O0FBQWJvQixvQjtBQUNOK0IsdUJBQU8sQ0FBQ0MsSUFBUixDQUFhLFNBQWI7QUFDTUMsMEIsR0FBYSxLQUFLQyxVQUFMLENBQWdCbEMsSUFBaEIsQztBQUNuQitCLHVCQUFPLENBQUNJLE9BQVIsQ0FBZ0IsU0FBaEI7O0FBQ0Esb0JBQUlGLFVBQUosRUFBZ0I7QUFDZCx1QkFBS0EsVUFBTCxHQUFrQkEsVUFBbEI7QUFDRDs7a0RBQ00sS0FBS0EsVTs7Ozs7Ozs7Ozs7Ozs7Ozs7OzJDQUdTL0IsSSxFQUFNO0FBQUE7O0FBQzNCWixZQUFNLENBQUM4QyxjQUFQLENBQXNCLElBQXRCLEVBQTRCbEMsSUFBNUIsRUFBa0M7QUFDaENMLFdBQUcsRUFBRTtBQUFBLGlCQUFNLE1BQUksWUFBS0ssSUFBTCxFQUFWO0FBQUEsU0FEMkI7QUFFaENtQyxXQUFHLEVBQUUsYUFBQzFDLEtBQUQsRUFBVztBQUNkLGNBQUksTUFBSSxZQUFLTyxJQUFMLEVBQUosS0FBcUJQLEtBQXpCLEVBQWdDO0FBQzlCLGtCQUFJLFlBQUtPLElBQUwsRUFBSixHQUFtQlAsS0FBbkI7O0FBQ0Esa0JBQUksQ0FBQzJDLElBQUwsa0JBQW9CcEMsSUFBcEIsR0FBNEJQLEtBQTVCO0FBQ0Q7QUFDRjtBQVArQixPQUFsQztBQVNEOzs7O0VBeEZpQzRDLDhDOzs7Ozs7Ozs7Ozs7OztBQ1BwQztBQUFBO0FBQUE7O0FBRUEsSUFBTTdFLFVBQVUsR0FBRyxTQUFiQSxVQUFhLENBQUNDLEdBQUQ7QUFBQSxTQUFTQSxHQUFHLENBQUNDLE9BQUosQ0FBWSxLQUFaLEVBQW1CLEVBQW5CLENBQVQ7QUFBQSxDQUFuQjs7QUFDQSxJQUFNQyxLQUFLLEdBQUcsU0FBUkEsS0FBUSxDQUFDRixHQUFEO0FBQUEsU0FBU0EsR0FBVCxhQUFTQSxHQUFULHVCQUFTQSxHQUFHLENBQUVDLE9BQUwsQ0FBYSw2QkFBYixFQUE0QyxVQUE1QyxDQUFUO0FBQUEsQ0FBZDs7QUFDQSxJQUFNRSxRQUFRLEdBQUcsU0FBWEEsUUFBVyxDQUFDSCxHQUFEO0FBQUEsU0FBU0ksUUFBUSxDQUFDSixHQUFELEVBQU0sRUFBTixDQUFqQjtBQUFBLENBQWpCOztBQUNBLElBQU1LLFVBQVUsR0FBRyxTQUFiQSxVQUFhLENBQUNMLEdBQUQ7QUFBQSxTQUFTLENBQUMsVUFBR0EsR0FBSCxFQUFTTSxLQUFULENBQWUsV0FBZixDQUFELEVBQThCQyxJQUE5QixDQUFtQyxFQUFuQyxDQUFUO0FBQUEsQ0FBbkI7O0FBQ0EsSUFBTXNFLFdBQVcsR0FBRyxTQUFkQSxXQUFjLENBQUNDLEdBQUQ7QUFBQSxTQUFTQSxHQUFHLEdBQUcsR0FBZjtBQUFBLENBQXBCOztBQUVlO0FBQ2I1RSxPQUFLLEVBQUxBLEtBRGE7QUFFYkMsVUFBUSxFQUFSQSxRQUZhO0FBR2JFLFlBQVUsRUFBVkEsVUFIYTtBQUliTixZQUFVLEVBQVZBLFVBSmE7QUFLYjhFLGFBQVcsRUFBWEEsV0FMYTtBQU1ickUsT0FBSyxFQUFFQyxpREFOTTtBQU9iQyxPQUFLLEVBQUVELGlEQVBNO0FBUWJFLFVBQVEsRUFBRUMsb0RBUkc7QUFTYkMsZUFBYSxFQUFFRCxvREFBYUE7QUFUZixDQUFmLEU7Ozs7Ozs7Ozs7OztBQ1JBO0FBQUE7QUFBQTs7QUFFQSxTQUFTbUUsVUFBVCxHQUFtRDtBQUFBLE1BQS9CQyxrQkFBK0IsdUVBQVYsWUFBTSxDQUFFLENBQUU7QUFDakRBLG9CQUFrQixDQUFDQyw0Q0FBRCxDQUFsQjtBQUVBQSxzREFBQSxDQUFZO0FBQ1ZDLE9BQUcsRUFBRTtBQURLLEdBQVo7QUFHRDs7QUFFY0gseUVBQWYsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDVk8sSUFBTUksY0FBYyxHQUFHLFNBQWpCQSxjQUFpQjtBQUFBLE1BQUNDLE1BQUQsdUVBQVUsRUFBVjtBQUFBLE1BQWNDLE1BQWQ7QUFBQSxTQUF5QkQsTUFBTSxDQUMxREUsS0FEb0QsQ0FDOUMsVUFBQy9DLElBQUQ7QUFBQSxXQUFVLE9BQU84QyxNQUFNLENBQUM5QyxJQUFELENBQWIsS0FBd0IsV0FBbEM7QUFBQSxHQUQ4QyxDQUF6QjtBQUFBLENBQXZCO0FBR0EsSUFBTWpDLEtBQUssR0FBRyxTQUFSQSxLQUFRLENBQUNOLEdBQUQsRUFBTXVGLE9BQU4sRUFBa0I7QUFDckMsTUFBSTtBQUNGLFdBQU92RixHQUFHLENBQUN3RixNQUFKLENBQVdELE9BQVgsSUFBc0IsQ0FBQyxDQUE5QjtBQUNELEdBRkQsQ0FFRSxPQUFPRSxDQUFQLEVBQVU7QUFDVixXQUFPekYsR0FBRyxDQUFDMEYsT0FBSixDQUFZSCxPQUFaLElBQXVCLENBQTlCO0FBQ0Q7QUFDRixDQU5NO0FBUUEsSUFBTTlFLFVBQVUsR0FBRyxTQUFiQSxVQUFhLENBQUNELEtBQUQsRUFBVztBQUNuQyxNQUFJUixHQUFHLEdBQUcsVUFBR1EsS0FBSCxFQUNQUCxPQURPLENBQ0MsU0FERCxFQUNZLEVBRFosRUFFUEEsT0FGTyxDQUVDLFlBRkQsRUFFZSxFQUZmLEVBR1AwRixJQUhPLEVBQVY7QUFJQSxNQUFNQyxVQUFVLEdBQUc1RixHQUFHLENBQUMwRixPQUFKLENBQVksR0FBWixDQUFuQjtBQUNBLE1BQU1HLFFBQVEsR0FBRzdGLEdBQUcsQ0FBQzBGLE9BQUosQ0FBWSxHQUFaLENBQWpCOztBQUNBLE1BQUlFLFVBQVUsR0FBR0MsUUFBakIsRUFBMkI7QUFDekI3RixPQUFHLEdBQUdBLEdBQUcsQ0FBQ0MsT0FBSixDQUFZLEdBQVosRUFBaUIsRUFBakIsRUFBcUJBLE9BQXJCLENBQTZCLEdBQTdCLEVBQWtDLEdBQWxDLENBQU47QUFDRCxHQUZELE1BRU8sSUFBSTJGLFVBQVUsR0FBR0MsUUFBYixJQUF5QkEsUUFBUSxHQUFHLENBQXhDLEVBQTJDO0FBQ2hEN0YsT0FBRyxHQUFHQSxHQUFHLENBQUNDLE9BQUosQ0FBWSxHQUFaLEVBQWlCLEVBQWpCLENBQU47QUFDRDs7QUFDRCxTQUFPNkYsVUFBVSxDQUFDOUYsR0FBRCxDQUFqQjtBQUNELENBYk07QUFlQSxJQUFNK0YsV0FBVyxHQUFHLFNBQWRBLFdBQWMsQ0FBQ1IsT0FBRCxFQUFhO0FBQ3RDLE1BQU1TLE1BQU0sR0FBRyxDQUNiLENBQUMsT0FBRCxFQUFVLEtBQVYsRUFBaUIsR0FBakIsQ0FEYSxFQUViLENBQUMsT0FBRCxFQUFVLEtBQVYsRUFBaUIsR0FBakIsQ0FGYSxFQUdiLENBQUMsT0FBRCxFQUFVLEtBQVYsRUFBaUIsSUFBakIsQ0FIYSxFQUliakUsSUFKYSxDQUlSO0FBQUE7QUFBQSxRQUFJa0UsSUFBSjtBQUFBLFFBQVVDLFdBQVY7O0FBQUEsV0FBMkJELElBQUksS0FBS1YsT0FBVCxJQUFvQlcsV0FBVyxDQUFDL0QsUUFBWixDQUFxQm9ELE9BQXJCLENBQS9DO0FBQUEsR0FKUSxDQUFmOztBQUtBLE1BQUlTLE1BQUosRUFBWTtBQUFBLDhGQUNlQSxNQURmO0FBQUEsUUFDSEcsUUFERztBQUFBLFFBQ09GLElBRFA7O0FBRVYsV0FBTztBQUFFRSxjQUFRLEVBQVJBLFFBQUY7QUFBWUYsVUFBSSxFQUFKQTtBQUFaLEtBQVA7QUFDRDs7QUFDRCxTQUFPLElBQVA7QUFDRCxDQVhNO0FBYUEsSUFBTUcsYUFBYSxHQUFHLFNBQWhCQSxhQUFnQixDQUFDL0QsSUFBRCxFQUFVO0FBQUEsTUFDN0JMLEtBRDZCLEdBQ1RLLElBRFMsQ0FDN0JMLEtBRDZCO0FBQUEsTUFDdEJyQixRQURzQixHQUNUMEIsSUFEUyxDQUN0QjFCLFFBRHNCOztBQUFBLHFCQUVWb0YsV0FBVyxDQUFDcEYsUUFBRCxDQUZEO0FBQUEsTUFFN0J3RixRQUY2QixnQkFFN0JBLFFBRjZCO0FBQUEsTUFFbkJGLElBRm1CLGdCQUVuQkEsSUFGbUI7O0FBR3JDLE1BQU16RCxNQUFNLEdBQUc7QUFBRTZELFNBQUssRUFBRSxVQUFUO0FBQXFCMUYsWUFBUSxFQUFFc0Y7QUFBL0IsR0FBZjtBQUNBLFNBQU8sSUFBSUssSUFBSSxDQUFDQyxZQUFULENBQXNCSixRQUF0QixFQUFnQzNELE1BQWhDLEVBQXdDZ0UsTUFBeEMsQ0FBK0N4RSxLQUEvQyxDQUFQO0FBQ0QsQ0FMTTtBQU9BLElBQU1wQixhQUFhLEdBQUcsU0FBaEJBLGFBQWdCLENBQUNaLEdBQUQsRUFBUztBQUNwQyxNQUFNeUcsU0FBUyxHQUFHLENBQUMsVUFBR3pHLEdBQUgsRUFBU00sS0FBVCxDQUFlLG9CQUFmLENBQUQsRUFBdUNDLElBQXZDLEVBQWxCO0FBQ0EsU0FBT3dGLFdBQVcsQ0FBQ1UsU0FBRCxDQUFsQjtBQUNELENBSE07QUFLQSxJQUFNQyxZQUFZLEdBQUcsU0FBZkEsWUFBZSxDQUFDMUcsR0FBRDtBQUFBLFNBQVU7QUFDcENRLFNBQUssRUFBRSxVQUFHUixHQUFILEVBQVMyRyxLQUFULENBQWUsR0FBZixFQUFvQnBELEdBQXBCLENBQXdCLFVBQUMvQyxLQUFEO0FBQUEsYUFBV0MsVUFBVSxDQUFDRCxLQUFELENBQXJCO0FBQUEsS0FBeEIsQ0FENkI7QUFFcENHLFlBQVEsRUFBRUMsYUFBYSxDQUFDWixHQUFEO0FBRmEsR0FBVjtBQUFBLENBQXJCO0FBS0EsSUFBTThDLFNBQVMsR0FBRyxTQUFaQSxTQUFZLFFBQXdEO0FBQUEsTUFBckRpQixRQUFxRCxTQUFyREEsUUFBcUQ7QUFBQSxNQUEzQzZDLFNBQTJDLFNBQTNDQSxTQUEyQztBQUFBLE1BQWhDQyxFQUFnQyxTQUFoQ0EsRUFBZ0M7QUFBQSxNQUExQkMsVUFBMEIsdUVBQWJwRSxRQUFhO0FBQy9FLE1BQU1xRSxJQUFJLEdBQUdGLEVBQUUsSUFBSUMsVUFBVSxDQUFDRSxhQUFYLENBQXlCakQsUUFBekIsQ0FBbkI7O0FBQ0EsTUFBSWdELElBQUosRUFBVTtBQUNSLFFBQU1FLFFBQVEsR0FBRyxnRkFBSUYsSUFBSSxDQUFDRyxVQUFULEVBQXFCbkYsSUFBckIsQ0FBMEIsVUFBQ29GLEtBQUQ7QUFBQSxhQUFXQSxLQUFLLENBQUNDLFFBQU4sQ0FBZWpGLFFBQWYsQ0FBd0IsV0FBeEIsQ0FBWDtBQUFBLEtBQTFCLENBQWpCOztBQUNBLHFCQUNFNEUsSUFBSSxDQUFDTSxZQUFMLENBQWtCVCxTQUFsQixLQUNJSyxRQUFRLElBQUlBLFFBQVEsQ0FBQ0ssU0FEekIsSUFFR1AsSUFBSSxDQUFDUSxTQUZSLElBR0dSLElBQUksQ0FBQy9FLEtBSFIsSUFJRytFLElBQUksQ0FBQ3JELE9BSlIsSUFLR3FELElBQUksQ0FBQ1MsR0FOVjtBQVFEOztBQUNELFNBQU8sSUFBUDtBQUNELENBZE07QUFnQkEsSUFBTWxGLFNBQVMsR0FBRyxTQUFaQSxTQUFZLENBQUNtRixRQUFEO0FBQUEsU0FBYyxJQUFJQyxTQUFKLEdBQWdCQyxlQUFoQixDQUFnQ0YsUUFBaEMsRUFBMEMsV0FBMUMsQ0FBZDtBQUFBLENBQWxCO0FBRUEsSUFBTUcsaUJBQWlCLEdBQUcsU0FBcEJBLGlCQUFvQixRQUFpQztBQUFBLE1BQTlCakgsUUFBOEIsU0FBOUJBLFFBQThCO0FBQUEsTUFBcEJILEtBQW9CLFNBQXBCQSxLQUFvQjtBQUFBLE1BQWJxSCxNQUFhLFNBQWJBLE1BQWE7QUFDaEUsTUFBTUMsZUFBZSxHQUFHMUgsUUFBUSxDQUFDSSxLQUFLLEdBQUlBLEtBQUssR0FBR3FILE1BQVQsR0FBbUIsR0FBNUIsRUFBaUMsRUFBakMsQ0FBaEM7QUFDQSxNQUFNckYsTUFBTSxHQUFHO0FBQUV1Rix5QkFBcUIsRUFBRSxDQUF6QjtBQUE0QkMseUJBQXFCLEVBQUU7QUFBbkQsR0FBZjtBQUNBLG1CQUFVckgsUUFBVixTQUFxQm1ILGVBQWUsQ0FBQ0csY0FBaEIsQ0FBK0J6RixNQUEvQixDQUFyQjtBQUNELENBSk07QUFNQSxJQUFNMEYsWUFBWTtBQUFBLGtMQUFHLGlCQUFPQyxDQUFQO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSw2Q0FBYSxJQUFJQyxPQUFKLENBQVksVUFBQ0MsQ0FBRDtBQUFBLHFCQUFPQyxVQUFVLENBQUM7QUFBQSx1QkFBTUQsQ0FBQyxDQUFDLElBQUQsQ0FBUDtBQUFBLGVBQUQsRUFBZ0JGLENBQWhCLENBQWpCO0FBQUEsYUFBWixDQUFiOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEdBQUg7O0FBQUEsa0JBQVpELFlBQVk7QUFBQTtBQUFBO0FBQUEsR0FBbEI7QUFFQSxJQUFNSyxpQkFBaUIsR0FBRyxTQUFwQkEsaUJBQW9CLENBQUN0SCxHQUFELEVBQVM7QUFDeEMsTUFBTXVILEtBQUssR0FBRzlGLFFBQVEsQ0FBQytGLGFBQVQsQ0FBdUIsUUFBdkIsQ0FBZDtBQUNBRCxPQUFLLENBQUNuQyxLQUFOLEdBQWMsNEVBQWQ7QUFDQW1DLE9BQUssQ0FBQ2hCLEdBQU4sR0FBWXZHLEdBQVo7QUFDQSxTQUFPdUgsS0FBUDtBQUNELENBTE07QUFPQSxJQUFNRSxlQUFlLEdBQUcsU0FBbEJBLGVBQWtCLENBQUMxSSxHQUFELEVBQVM7QUFDdEMsTUFBTTJJLENBQUMsR0FBR2pHLFFBQVEsQ0FBQytGLGFBQVQsQ0FBdUIsT0FBdkIsQ0FBVjtBQUNBRSxHQUFDLENBQUN0QyxLQUFGLEdBQVUsMkRBQVY7QUFDQTNELFVBQVEsQ0FBQ2tHLElBQVQsQ0FBY0MsV0FBZCxDQUEwQkYsQ0FBMUI7QUFDQUEsR0FBQyxDQUFDM0csS0FBRixHQUFVaEMsR0FBVjtBQUNBMkksR0FBQyxDQUFDRyxNQUFGO0FBQ0FwRyxVQUFRLENBQUNxRyxXQUFULENBQXFCLE1BQXJCO0FBQ0FyRyxVQUFRLENBQUNrRyxJQUFULENBQWNJLFdBQWQsQ0FBMEJMLENBQTFCO0FBQ0QsQ0FSTTtBQVVBLElBQU1NLFlBQVk7QUFBQSxrTEFBRyxrQkFBT0MsTUFBUCxFQUFlQyxJQUFmO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSw4Q0FBd0JELE1BQU0sQ0FBQ0MsSUFBRCxDQUE5Qjs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxHQUFIOztBQUFBLGtCQUFaRixZQUFZO0FBQUE7QUFBQTtBQUFBLEdBQWxCO0FBRUEsSUFBTUcsZ0JBQWdCLEdBQUcsU0FBbkJBLGdCQUFtQixDQUFDQyxFQUFELEVBQVE7QUFDdEMsTUFBTUMsT0FBTyxHQUFHNUcsUUFBUSxDQUFDK0YsYUFBVCxDQUF1QixLQUF2QixDQUFoQjtBQUNBYSxTQUFPLENBQUNELEVBQVIsR0FBYUEsRUFBYjtBQUNBM0csVUFBUSxDQUFDa0csSUFBVCxDQUFjVyxNQUFkLENBQXFCRCxPQUFyQjtBQUNBLE1BQU1FLFVBQVUsR0FBR0YsT0FBTyxDQUFDRyxZQUFSLENBQXFCO0FBQUVDLFFBQUksRUFBRTtBQUFSLEdBQXJCLENBQW5CO0FBQ0FGLFlBQVUsQ0FBQ0csU0FBWCx3Q0FDd0JDLE1BQU0sQ0FBQ0MsU0FBUCxDQUFpQkMsTUFBakIsQ0FBd0Isb0JBQXhCLENBRHhCLHlDQUVhVCxFQUZiO0FBR0EsU0FBT0csVUFBUDtBQUNELENBVE07QUFXQSxJQUFNdEgsR0FBRyxHQUFHLFNBQU5BLEdBQU0sQ0FBQ21ELE1BQUQsRUFBU3RDLElBQVQsRUFBa0I7QUFDbkMsTUFBTWdILFNBQVMsR0FBRyxPQUFPaEgsSUFBUCxLQUFnQixRQUFoQixHQUEyQkEsSUFBSSxDQUFDNEQsS0FBTCxDQUFXLEdBQVgsQ0FBM0IsR0FBNkM1RCxJQUEvRDs7QUFDQSxNQUFJaUgsS0FBSyxDQUFDQyxPQUFOLENBQWM1RSxNQUFkLEtBQXlCNkUsTUFBTSxDQUFDQyxLQUFQLENBQWEvSixRQUFRLENBQUMySixTQUFTLENBQUMsQ0FBRCxDQUFWLEVBQWUsRUFBZixDQUFyQixDQUE3QixFQUF1RTtBQUNyRSxXQUFPMUUsTUFBTSxDQUNWOUIsR0FESSxDQUNBLFVBQUM2RyxVQUFEO0FBQUEsYUFBZ0JsSSxHQUFHLENBQUNrSSxVQUFELGtGQUFpQkwsU0FBakIsRUFBbkI7QUFBQSxLQURBLEVBRUpNLElBRkksQ0FFQ0MsUUFGRCxDQUFQO0FBR0Q7O0FBQ0QsTUFBTWpJLElBQUksR0FBR2dELE1BQU0sQ0FBQzBFLFNBQVMsQ0FBQyxDQUFELENBQVYsQ0FBbkI7QUFDQUEsV0FBUyxDQUFDUSxLQUFWOztBQUNBLE1BQUlsSSxJQUFJLElBQUkwSCxTQUFTLENBQUNTLE1BQXRCLEVBQThCO0FBQzVCLFdBQU90SSxHQUFHLENBQUNHLElBQUQsRUFBTzBILFNBQVAsQ0FBVjtBQUNEOztBQUNELFNBQU8xSCxJQUFQO0FBQ0QsQ0FiTTtBQWVBLElBQU1zQixVQUFVLEdBQUcsU0FBYkEsVUFBYSxRQUFpRDtBQUFBLE1BQTlDM0QsR0FBOEMsU0FBOUNBLEdBQThDO0FBQUEsTUFBekN5SyxHQUF5QyxTQUF6Q0EsR0FBeUM7QUFBQSw0QkFBcEM3SSxPQUFvQztBQUFBLE1BQXBDQSxPQUFvQyw4QkFBMUJELE1BQU0sQ0FBQ0MsT0FBUCxDQUFlNkksR0FBZixDQUEwQjtBQUN6RSxNQUFJQyxTQUFTLEdBQUcxSyxHQUFoQjs7QUFDQSxNQUFJNEIsT0FBSixFQUFhO0FBQ1g4SSxhQUFTLEdBQUc5SSxPQUFPLENBQ2hCK0ksTUFEUyxDQUNGLFVBQUMzRSxNQUFEO0FBQUE7QUFBQSxVQUFVekQsSUFBVjtBQUFBLFVBQWdCUCxLQUFoQjs7QUFBQSxhQUEyQmdFLE1BQU0sQ0FBQy9GLE9BQVAsWUFBbUJzQyxJQUFuQixHQUEyQlAsS0FBM0IsQ0FBM0I7QUFBQSxLQURFLEVBQzREMEksU0FENUQsQ0FBWjtBQUVEOztBQUNELFNBQU9BLFNBQVA7QUFDRCxDQVBNLEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDL0hBLElBQU12RixjQUFjLEdBQUcsU0FBakJBLGNBQWlCO0FBQUEsTUFBQ0MsTUFBRCx1RUFBVSxFQUFWO0FBQUEsTUFBY0MsTUFBZDtBQUFBLFNBQXlCRCxNQUFNLENBQzFERSxLQURvRCxDQUM5QyxVQUFDL0MsSUFBRDtBQUFBLFdBQVUsT0FBTzhDLE1BQU0sQ0FBQzlDLElBQUQsQ0FBYixLQUF3QixXQUFsQztBQUFBLEdBRDhDLENBQXpCO0FBQUEsQ0FBdkI7QUFHQSxJQUFNakMsS0FBSyxHQUFHLFNBQVJBLEtBQVEsQ0FBQ04sR0FBRCxFQUFNdUYsT0FBTixFQUFrQjtBQUNyQyxNQUFJO0FBQ0YsV0FBT3ZGLEdBQUcsQ0FBQ3dGLE1BQUosQ0FBV0QsT0FBWCxJQUFzQixDQUFDLENBQTlCO0FBQ0QsR0FGRCxDQUVFLE9BQU9FLENBQVAsRUFBVTtBQUNWLFdBQU96RixHQUFHLENBQUMwRixPQUFKLENBQVlILE9BQVosSUFBdUIsQ0FBOUI7QUFDRDtBQUNGLENBTk07QUFRQSxJQUFNOUUsVUFBVSxHQUFHLFNBQWJBLFVBQWEsQ0FBQ0QsS0FBRCxFQUFXO0FBQ25DLE1BQUlSLEdBQUcsR0FBRyxVQUFHUSxLQUFILEVBQ1BQLE9BRE8sQ0FDQyxTQURELEVBQ1ksRUFEWixFQUVQQSxPQUZPLENBRUMsWUFGRCxFQUVlLEVBRmYsRUFHUDBGLElBSE8sRUFBVjtBQUlBLE1BQU1DLFVBQVUsR0FBRzVGLEdBQUcsQ0FBQzBGLE9BQUosQ0FBWSxHQUFaLENBQW5CO0FBQ0EsTUFBTUcsUUFBUSxHQUFHN0YsR0FBRyxDQUFDMEYsT0FBSixDQUFZLEdBQVosQ0FBakI7O0FBQ0EsTUFBSUUsVUFBVSxHQUFHQyxRQUFqQixFQUEyQjtBQUN6QjdGLE9BQUcsR0FBR0EsR0FBRyxDQUFDQyxPQUFKLENBQVksR0FBWixFQUFpQixFQUFqQixFQUFxQkEsT0FBckIsQ0FBNkIsR0FBN0IsRUFBa0MsR0FBbEMsQ0FBTjtBQUNELEdBRkQsTUFFTyxJQUFJMkYsVUFBVSxHQUFHQyxRQUFiLElBQXlCQSxRQUFRLEdBQUcsQ0FBeEMsRUFBMkM7QUFDaEQ3RixPQUFHLEdBQUdBLEdBQUcsQ0FBQ0MsT0FBSixDQUFZLEdBQVosRUFBaUIsRUFBakIsQ0FBTjtBQUNEOztBQUNELFNBQU82RixVQUFVLENBQUM5RixHQUFELENBQWpCO0FBQ0QsQ0FiTTtBQWVBLElBQU0rRixXQUFXLEdBQUcsU0FBZEEsV0FBYyxDQUFDUixPQUFELEVBQWE7QUFDdEMsTUFBTVMsTUFBTSxHQUFHLENBQ2IsQ0FBQyxPQUFELEVBQVUsS0FBVixFQUFpQixHQUFqQixDQURhLEVBRWIsQ0FBQyxPQUFELEVBQVUsS0FBVixFQUFpQixHQUFqQixDQUZhLEVBR2IsQ0FBQyxPQUFELEVBQVUsS0FBVixFQUFpQixJQUFqQixDQUhhLEVBSWJqRSxJQUphLENBSVI7QUFBQTtBQUFBLFFBQUlrRSxJQUFKO0FBQUEsUUFBVUMsV0FBVjs7QUFBQSxXQUEyQkQsSUFBSSxLQUFLVixPQUFULElBQW9CVyxXQUFXLENBQUMvRCxRQUFaLENBQXFCb0QsT0FBckIsQ0FBL0M7QUFBQSxHQUpRLENBQWY7O0FBS0EsTUFBSVMsTUFBSixFQUFZO0FBQUEsOEZBQ2VBLE1BRGY7QUFBQSxRQUNIRyxRQURHO0FBQUEsUUFDT0YsSUFEUDs7QUFFVixXQUFPO0FBQUVFLGNBQVEsRUFBUkEsUUFBRjtBQUFZRixVQUFJLEVBQUpBO0FBQVosS0FBUDtBQUNEOztBQUNELFNBQU8sSUFBUDtBQUNELENBWE07QUFhQSxJQUFNckYsYUFBYSxHQUFHLFNBQWhCQSxhQUFnQixDQUFDWixHQUFELEVBQVM7QUFDcEMsTUFBTXlHLFNBQVMsR0FBRyxDQUFDLFVBQUd6RyxHQUFILEVBQVNNLEtBQVQsQ0FBZSxRQUFmLENBQUQsRUFBMkJDLElBQTNCLEVBQWxCO0FBQ0EsU0FBT3dGLFdBQVcsQ0FBQ1UsU0FBRCxDQUFsQjtBQUNELENBSE07QUFLQSxJQUFNQyxZQUFZLEdBQUcsU0FBZkEsWUFBZSxDQUFDMUcsR0FBRDtBQUFBLFNBQVU7QUFDcENRLFNBQUssRUFBRSxVQUFHUixHQUFILEVBQVMyRyxLQUFULENBQWUsR0FBZixFQUFvQnBELEdBQXBCLENBQXdCLFVBQUMvQyxLQUFEO0FBQUEsYUFBV0MsVUFBVSxDQUFDRCxLQUFELENBQXJCO0FBQUEsS0FBeEIsQ0FENkI7QUFFcENHLFlBQVEsRUFBRUMsYUFBYSxDQUFDWixHQUFEO0FBRmEsR0FBVjtBQUFBLENBQXJCO0FBS0EsSUFBTThDLFNBQVMsR0FBRyxTQUFaQSxTQUFZLFFBQXdEO0FBQUEsTUFBckRpQixRQUFxRCxTQUFyREEsUUFBcUQ7QUFBQSxNQUEzQzZDLFNBQTJDLFNBQTNDQSxTQUEyQztBQUFBLE1BQWhDQyxFQUFnQyxTQUFoQ0EsRUFBZ0M7QUFBQSxNQUExQkMsVUFBMEIsdUVBQWJwRSxRQUFhO0FBQy9FLE1BQU1xRSxJQUFJLEdBQUdGLEVBQUUsSUFBSUMsVUFBVSxDQUFDRSxhQUFYLENBQXlCakQsUUFBekIsQ0FBbkI7O0FBQ0EsTUFBSWdELElBQUosRUFBVTtBQUNSLFFBQU1FLFFBQVEsR0FBRyxnRkFBSUYsSUFBSSxDQUFDRyxVQUFULEVBQXFCbkYsSUFBckIsQ0FBMEIsVUFBQ29GLEtBQUQ7QUFBQSxhQUFXQSxLQUFLLENBQUNDLFFBQU4sQ0FBZWpGLFFBQWYsQ0FBd0IsV0FBeEIsQ0FBWDtBQUFBLEtBQTFCLENBQWpCOztBQUNBLHFCQUNFNEUsSUFBSSxDQUFDTSxZQUFMLENBQWtCVCxTQUFsQixLQUNJSyxRQUFRLElBQUlBLFFBQVEsQ0FBQ0ssU0FEekIsSUFFR1AsSUFBSSxDQUFDUSxTQUZSLElBR0dSLElBQUksQ0FBQy9FLEtBSFIsSUFJRytFLElBQUksQ0FBQ3JELE9BSlIsSUFLR3FELElBQUksQ0FBQ1MsR0FOVjtBQVFEOztBQUNELFNBQU8sSUFBUDtBQUNELENBZE07QUFnQkEsSUFBTWxGLFNBQVMsR0FBRyxTQUFaQSxTQUFZLENBQUNtRixRQUFEO0FBQUEsU0FBYyxJQUFJQyxTQUFKLEdBQWdCQyxlQUFoQixDQUFnQ0YsUUFBaEMsRUFBMEMsV0FBMUMsQ0FBZDtBQUFBLENBQWxCO0FBRUEsSUFBTUcsaUJBQWlCLEdBQUcsU0FBcEJBLGlCQUFvQixRQUFpQztBQUFBLE1BQTlCakgsUUFBOEIsU0FBOUJBLFFBQThCO0FBQUEsTUFBcEJILEtBQW9CLFNBQXBCQSxLQUFvQjtBQUFBLE1BQWJxSCxNQUFhLFNBQWJBLE1BQWE7QUFDaEUsTUFBTUMsZUFBZSxHQUFHMUgsUUFBUSxDQUFDSSxLQUFLLEdBQUlBLEtBQUssR0FBR3FILE1BQVQsR0FBbUIsR0FBNUIsRUFBaUMsRUFBakMsQ0FBaEM7QUFDQSxNQUFNckYsTUFBTSxHQUFHO0FBQUV1Rix5QkFBcUIsRUFBRSxDQUF6QjtBQUE0QkMseUJBQXFCLEVBQUU7QUFBbkQsR0FBZjtBQUNBLG1CQUFVckgsUUFBVixTQUFxQm1ILGVBQWUsQ0FBQ0csY0FBaEIsQ0FBK0J6RixNQUEvQixDQUFyQjtBQUNELENBSk07QUFNQSxJQUFNMEYsWUFBWTtBQUFBLGtMQUFHLGlCQUFPQyxDQUFQO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSw2Q0FBYSxJQUFJQyxPQUFKLENBQVksVUFBQ0MsQ0FBRDtBQUFBLHFCQUFPQyxVQUFVLENBQUM7QUFBQSx1QkFBTUQsQ0FBQyxDQUFDLElBQUQsQ0FBUDtBQUFBLGVBQUQsRUFBZ0JGLENBQWhCLENBQWpCO0FBQUEsYUFBWixDQUFiOztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLEdBQUg7O0FBQUEsa0JBQVpELFlBQVk7QUFBQTtBQUFBO0FBQUEsR0FBbEI7QUFFQSxJQUFNSyxpQkFBaUIsR0FBRyxTQUFwQkEsaUJBQW9CLENBQUN0SCxHQUFELEVBQVM7QUFDeEMsTUFBTXVILEtBQUssR0FBRzlGLFFBQVEsQ0FBQytGLGFBQVQsQ0FBdUIsUUFBdkIsQ0FBZDtBQUNBRCxPQUFLLENBQUNuQyxLQUFOLEdBQWMsNEVBQWQ7QUFDQW1DLE9BQUssQ0FBQ2hCLEdBQU4sR0FBWXZHLEdBQVo7QUFDQSxTQUFPdUgsS0FBUDtBQUNELENBTE07QUFPQSxJQUFNRSxlQUFlLEdBQUcsU0FBbEJBLGVBQWtCLENBQUMxSSxHQUFELEVBQVM7QUFDdEMsTUFBTTJJLENBQUMsR0FBR2pHLFFBQVEsQ0FBQytGLGFBQVQsQ0FBdUIsT0FBdkIsQ0FBVjtBQUNBRSxHQUFDLENBQUN0QyxLQUFGLEdBQVUsMkRBQVY7QUFDQTNELFVBQVEsQ0FBQ2tHLElBQVQsQ0FBY0MsV0FBZCxDQUEwQkYsQ0FBMUI7QUFDQUEsR0FBQyxDQUFDM0csS0FBRixHQUFVaEMsR0FBVjtBQUNBMkksR0FBQyxDQUFDRyxNQUFGO0FBQ0FwRyxVQUFRLENBQUNxRyxXQUFULENBQXFCLE1BQXJCO0FBQ0FyRyxVQUFRLENBQUNrRyxJQUFULENBQWNJLFdBQWQsQ0FBMEJMLENBQTFCO0FBQ0QsQ0FSTTtBQVVBLElBQU1NLFlBQVk7QUFBQSxrTEFBRyxrQkFBT0MsTUFBUCxFQUFlQyxJQUFmO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSw4Q0FBd0JELE1BQU0sQ0FBQ0MsSUFBRCxDQUE5Qjs7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxHQUFIOztBQUFBLGtCQUFaRixZQUFZO0FBQUE7QUFBQTtBQUFBLEdBQWxCO0FBRUEsSUFBTUcsZ0JBQWdCLEdBQUcsU0FBbkJBLGdCQUFtQixDQUFDQyxFQUFELEVBQVE7QUFDdEMsTUFBTUMsT0FBTyxHQUFHNUcsUUFBUSxDQUFDK0YsYUFBVCxDQUF1QixLQUF2QixDQUFoQjtBQUNBYSxTQUFPLENBQUNELEVBQVIsR0FBYUEsRUFBYjtBQUNBM0csVUFBUSxDQUFDa0csSUFBVCxDQUFjVyxNQUFkLENBQXFCRCxPQUFyQjtBQUNBLE1BQU1FLFVBQVUsR0FBR0YsT0FBTyxDQUFDRyxZQUFSLENBQXFCO0FBQUVDLFFBQUksRUFBRTtBQUFSLEdBQXJCLENBQW5CO0FBQ0FGLFlBQVUsQ0FBQ0csU0FBWCx3Q0FDd0JDLE1BQU0sQ0FBQ0MsU0FBUCxDQUFpQkMsTUFBakIsQ0FBd0Isb0JBQXhCLENBRHhCLHlDQUVhVCxFQUZiO0FBR0EsU0FBT0csVUFBUDtBQUNELENBVE07QUFXQSxJQUFNdEgsR0FBRyxHQUFHLFNBQU5BLEdBQU0sQ0FBQ21ELE1BQUQsRUFBU3RDLElBQVQsRUFBa0I7QUFDbkMsTUFBTWdILFNBQVMsR0FBRyxPQUFPaEgsSUFBUCxLQUFnQixRQUFoQixHQUEyQkEsSUFBSSxDQUFDNEQsS0FBTCxDQUFXLEdBQVgsQ0FBM0IsR0FBNkM1RCxJQUEvRDs7QUFDQSxNQUFJaUgsS0FBSyxDQUFDQyxPQUFOLENBQWM1RSxNQUFkLEtBQXlCNkUsTUFBTSxDQUFDQyxLQUFQLENBQWEvSixRQUFRLENBQUMySixTQUFTLENBQUMsQ0FBRCxDQUFWLEVBQWUsRUFBZixDQUFyQixDQUE3QixFQUF1RTtBQUNyRSxXQUFPMUUsTUFBTSxDQUNWOUIsR0FESSxDQUNBLFVBQUM2RyxVQUFEO0FBQUEsYUFBZ0JsSSxHQUFHLENBQUNrSSxVQUFELGtGQUFpQkwsU0FBakIsRUFBbkI7QUFBQSxLQURBLEVBRUpNLElBRkksQ0FFQ0MsUUFGRCxDQUFQO0FBR0Q7O0FBQ0QsTUFBTWpJLElBQUksR0FBR2dELE1BQU0sQ0FBQzBFLFNBQVMsQ0FBQyxDQUFELENBQVYsQ0FBbkI7QUFDQUEsV0FBUyxDQUFDUSxLQUFWOztBQUNBLE1BQUlsSSxJQUFJLElBQUkwSCxTQUFTLENBQUNTLE1BQXRCLEVBQThCO0FBQzVCLFdBQU90SSxHQUFHLENBQUNHLElBQUQsRUFBTzBILFNBQVAsQ0FBVjtBQUNEOztBQUNELFNBQU8xSCxJQUFQO0FBQ0QsQ0FiTTtBQWVBLElBQU1zQixVQUFVLEdBQUcsU0FBYkEsVUFBYSxDQUFDM0QsR0FBRCxFQUFNeUssR0FBTixFQUFjO0FBQ3RDLE1BQUl6RSxNQUFNLEdBQUdoRyxHQUFiOztBQUNBLE1BQUl5SyxHQUFKLEVBQVM7QUFDUDlJLFVBQU0sQ0FBQ0MsT0FBUCxDQUFlNkksR0FBZixFQUFvQkcsT0FBcEIsQ0FBNEIsaUJBQW1CO0FBQUE7QUFBQSxVQUFqQnJJLElBQWlCO0FBQUEsVUFBWFAsS0FBVzs7QUFDN0NnRSxZQUFNLEdBQUdoRyxHQUFHLENBQUNDLE9BQUosWUFBZ0JzQyxJQUFoQixHQUF3QlAsS0FBeEIsQ0FBVDtBQUNELEtBRkQ7QUFHRDs7QUFDRCxTQUFPZ0UsTUFBUDtBQUNELENBUk0sQyIsImZpbGUiOiJ2ZW5kb3JzL2J1bmRsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImZ1bmN0aW9uIF9hcnJheUxpa2VUb0FycmF5KGFyciwgbGVuKSB7XG4gIGlmIChsZW4gPT0gbnVsbCB8fCBsZW4gPiBhcnIubGVuZ3RoKSBsZW4gPSBhcnIubGVuZ3RoO1xuXG4gIGZvciAodmFyIGkgPSAwLCBhcnIyID0gbmV3IEFycmF5KGxlbik7IGkgPCBsZW47IGkrKykge1xuICAgIGFycjJbaV0gPSBhcnJbaV07XG4gIH1cblxuICByZXR1cm4gYXJyMjtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBfYXJyYXlMaWtlVG9BcnJheTsiLCJmdW5jdGlvbiBfYXJyYXlXaXRoSG9sZXMoYXJyKSB7XG4gIGlmIChBcnJheS5pc0FycmF5KGFycikpIHJldHVybiBhcnI7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gX2FycmF5V2l0aEhvbGVzOyIsInZhciBhcnJheUxpa2VUb0FycmF5ID0gcmVxdWlyZShcIi4vYXJyYXlMaWtlVG9BcnJheVwiKTtcblxuZnVuY3Rpb24gX2FycmF5V2l0aG91dEhvbGVzKGFycikge1xuICBpZiAoQXJyYXkuaXNBcnJheShhcnIpKSByZXR1cm4gYXJyYXlMaWtlVG9BcnJheShhcnIpO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IF9hcnJheVdpdGhvdXRIb2xlczsiLCJmdW5jdGlvbiBfYXNzZXJ0VGhpc0luaXRpYWxpemVkKHNlbGYpIHtcbiAgaWYgKHNlbGYgPT09IHZvaWQgMCkge1xuICAgIHRocm93IG5ldyBSZWZlcmVuY2VFcnJvcihcInRoaXMgaGFzbid0IGJlZW4gaW5pdGlhbGlzZWQgLSBzdXBlcigpIGhhc24ndCBiZWVuIGNhbGxlZFwiKTtcbiAgfVxuXG4gIHJldHVybiBzZWxmO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IF9hc3NlcnRUaGlzSW5pdGlhbGl6ZWQ7IiwiZnVuY3Rpb24gYXN5bmNHZW5lcmF0b3JTdGVwKGdlbiwgcmVzb2x2ZSwgcmVqZWN0LCBfbmV4dCwgX3Rocm93LCBrZXksIGFyZykge1xuICB0cnkge1xuICAgIHZhciBpbmZvID0gZ2VuW2tleV0oYXJnKTtcbiAgICB2YXIgdmFsdWUgPSBpbmZvLnZhbHVlO1xuICB9IGNhdGNoIChlcnJvcikge1xuICAgIHJlamVjdChlcnJvcik7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgaWYgKGluZm8uZG9uZSkge1xuICAgIHJlc29sdmUodmFsdWUpO1xuICB9IGVsc2Uge1xuICAgIFByb21pc2UucmVzb2x2ZSh2YWx1ZSkudGhlbihfbmV4dCwgX3Rocm93KTtcbiAgfVxufVxuXG5mdW5jdGlvbiBfYXN5bmNUb0dlbmVyYXRvcihmbikge1xuICByZXR1cm4gZnVuY3Rpb24gKCkge1xuICAgIHZhciBzZWxmID0gdGhpcyxcbiAgICAgICAgYXJncyA9IGFyZ3VtZW50cztcbiAgICByZXR1cm4gbmV3IFByb21pc2UoZnVuY3Rpb24gKHJlc29sdmUsIHJlamVjdCkge1xuICAgICAgdmFyIGdlbiA9IGZuLmFwcGx5KHNlbGYsIGFyZ3MpO1xuXG4gICAgICBmdW5jdGlvbiBfbmV4dCh2YWx1ZSkge1xuICAgICAgICBhc3luY0dlbmVyYXRvclN0ZXAoZ2VuLCByZXNvbHZlLCByZWplY3QsIF9uZXh0LCBfdGhyb3csIFwibmV4dFwiLCB2YWx1ZSk7XG4gICAgICB9XG5cbiAgICAgIGZ1bmN0aW9uIF90aHJvdyhlcnIpIHtcbiAgICAgICAgYXN5bmNHZW5lcmF0b3JTdGVwKGdlbiwgcmVzb2x2ZSwgcmVqZWN0LCBfbmV4dCwgX3Rocm93LCBcInRocm93XCIsIGVycik7XG4gICAgICB9XG5cbiAgICAgIF9uZXh0KHVuZGVmaW5lZCk7XG4gICAgfSk7XG4gIH07XG59XG5cbm1vZHVsZS5leHBvcnRzID0gX2FzeW5jVG9HZW5lcmF0b3I7IiwiZnVuY3Rpb24gX2NsYXNzQ2FsbENoZWNrKGluc3RhbmNlLCBDb25zdHJ1Y3Rvcikge1xuICBpZiAoIShpbnN0YW5jZSBpbnN0YW5jZW9mIENvbnN0cnVjdG9yKSkge1xuICAgIHRocm93IG5ldyBUeXBlRXJyb3IoXCJDYW5ub3QgY2FsbCBhIGNsYXNzIGFzIGEgZnVuY3Rpb25cIik7XG4gIH1cbn1cblxubW9kdWxlLmV4cG9ydHMgPSBfY2xhc3NDYWxsQ2hlY2s7IiwiZnVuY3Rpb24gX2RlZmluZVByb3BlcnRpZXModGFyZ2V0LCBwcm9wcykge1xuICBmb3IgKHZhciBpID0gMDsgaSA8IHByb3BzLmxlbmd0aDsgaSsrKSB7XG4gICAgdmFyIGRlc2NyaXB0b3IgPSBwcm9wc1tpXTtcbiAgICBkZXNjcmlwdG9yLmVudW1lcmFibGUgPSBkZXNjcmlwdG9yLmVudW1lcmFibGUgfHwgZmFsc2U7XG4gICAgZGVzY3JpcHRvci5jb25maWd1cmFibGUgPSB0cnVlO1xuICAgIGlmIChcInZhbHVlXCIgaW4gZGVzY3JpcHRvcikgZGVzY3JpcHRvci53cml0YWJsZSA9IHRydWU7XG4gICAgT2JqZWN0LmRlZmluZVByb3BlcnR5KHRhcmdldCwgZGVzY3JpcHRvci5rZXksIGRlc2NyaXB0b3IpO1xuICB9XG59XG5cbmZ1bmN0aW9uIF9jcmVhdGVDbGFzcyhDb25zdHJ1Y3RvciwgcHJvdG9Qcm9wcywgc3RhdGljUHJvcHMpIHtcbiAgaWYgKHByb3RvUHJvcHMpIF9kZWZpbmVQcm9wZXJ0aWVzKENvbnN0cnVjdG9yLnByb3RvdHlwZSwgcHJvdG9Qcm9wcyk7XG4gIGlmIChzdGF0aWNQcm9wcykgX2RlZmluZVByb3BlcnRpZXMoQ29uc3RydWN0b3IsIHN0YXRpY1Byb3BzKTtcbiAgcmV0dXJuIENvbnN0cnVjdG9yO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IF9jcmVhdGVDbGFzczsiLCJmdW5jdGlvbiBfZGVmaW5lUHJvcGVydHkob2JqLCBrZXksIHZhbHVlKSB7XG4gIGlmIChrZXkgaW4gb2JqKSB7XG4gICAgT2JqZWN0LmRlZmluZVByb3BlcnR5KG9iaiwga2V5LCB7XG4gICAgICB2YWx1ZTogdmFsdWUsXG4gICAgICBlbnVtZXJhYmxlOiB0cnVlLFxuICAgICAgY29uZmlndXJhYmxlOiB0cnVlLFxuICAgICAgd3JpdGFibGU6IHRydWVcbiAgICB9KTtcbiAgfSBlbHNlIHtcbiAgICBvYmpba2V5XSA9IHZhbHVlO1xuICB9XG5cbiAgcmV0dXJuIG9iajtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBfZGVmaW5lUHJvcGVydHk7IiwiZnVuY3Rpb24gX2dldFByb3RvdHlwZU9mKG8pIHtcbiAgbW9kdWxlLmV4cG9ydHMgPSBfZ2V0UHJvdG90eXBlT2YgPSBPYmplY3Quc2V0UHJvdG90eXBlT2YgPyBPYmplY3QuZ2V0UHJvdG90eXBlT2YgOiBmdW5jdGlvbiBfZ2V0UHJvdG90eXBlT2Yobykge1xuICAgIHJldHVybiBvLl9fcHJvdG9fXyB8fCBPYmplY3QuZ2V0UHJvdG90eXBlT2Yobyk7XG4gIH07XG4gIHJldHVybiBfZ2V0UHJvdG90eXBlT2Yobyk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gX2dldFByb3RvdHlwZU9mOyIsInZhciBzZXRQcm90b3R5cGVPZiA9IHJlcXVpcmUoXCIuL3NldFByb3RvdHlwZU9mXCIpO1xuXG5mdW5jdGlvbiBfaW5oZXJpdHMoc3ViQ2xhc3MsIHN1cGVyQ2xhc3MpIHtcbiAgaWYgKHR5cGVvZiBzdXBlckNsYXNzICE9PSBcImZ1bmN0aW9uXCIgJiYgc3VwZXJDbGFzcyAhPT0gbnVsbCkge1xuICAgIHRocm93IG5ldyBUeXBlRXJyb3IoXCJTdXBlciBleHByZXNzaW9uIG11c3QgZWl0aGVyIGJlIG51bGwgb3IgYSBmdW5jdGlvblwiKTtcbiAgfVxuXG4gIHN1YkNsYXNzLnByb3RvdHlwZSA9IE9iamVjdC5jcmVhdGUoc3VwZXJDbGFzcyAmJiBzdXBlckNsYXNzLnByb3RvdHlwZSwge1xuICAgIGNvbnN0cnVjdG9yOiB7XG4gICAgICB2YWx1ZTogc3ViQ2xhc3MsXG4gICAgICB3cml0YWJsZTogdHJ1ZSxcbiAgICAgIGNvbmZpZ3VyYWJsZTogdHJ1ZVxuICAgIH1cbiAgfSk7XG4gIGlmIChzdXBlckNsYXNzKSBzZXRQcm90b3R5cGVPZihzdWJDbGFzcywgc3VwZXJDbGFzcyk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gX2luaGVyaXRzOyIsImZ1bmN0aW9uIF9pdGVyYWJsZVRvQXJyYXkoaXRlcikge1xuICBpZiAodHlwZW9mIFN5bWJvbCAhPT0gXCJ1bmRlZmluZWRcIiAmJiBTeW1ib2wuaXRlcmF0b3IgaW4gT2JqZWN0KGl0ZXIpKSByZXR1cm4gQXJyYXkuZnJvbShpdGVyKTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBfaXRlcmFibGVUb0FycmF5OyIsImZ1bmN0aW9uIF9pdGVyYWJsZVRvQXJyYXlMaW1pdChhcnIsIGkpIHtcbiAgaWYgKHR5cGVvZiBTeW1ib2wgPT09IFwidW5kZWZpbmVkXCIgfHwgIShTeW1ib2wuaXRlcmF0b3IgaW4gT2JqZWN0KGFycikpKSByZXR1cm47XG4gIHZhciBfYXJyID0gW107XG4gIHZhciBfbiA9IHRydWU7XG4gIHZhciBfZCA9IGZhbHNlO1xuICB2YXIgX2UgPSB1bmRlZmluZWQ7XG5cbiAgdHJ5IHtcbiAgICBmb3IgKHZhciBfaSA9IGFycltTeW1ib2wuaXRlcmF0b3JdKCksIF9zOyAhKF9uID0gKF9zID0gX2kubmV4dCgpKS5kb25lKTsgX24gPSB0cnVlKSB7XG4gICAgICBfYXJyLnB1c2goX3MudmFsdWUpO1xuXG4gICAgICBpZiAoaSAmJiBfYXJyLmxlbmd0aCA9PT0gaSkgYnJlYWs7XG4gICAgfVxuICB9IGNhdGNoIChlcnIpIHtcbiAgICBfZCA9IHRydWU7XG4gICAgX2UgPSBlcnI7XG4gIH0gZmluYWxseSB7XG4gICAgdHJ5IHtcbiAgICAgIGlmICghX24gJiYgX2lbXCJyZXR1cm5cIl0gIT0gbnVsbCkgX2lbXCJyZXR1cm5cIl0oKTtcbiAgICB9IGZpbmFsbHkge1xuICAgICAgaWYgKF9kKSB0aHJvdyBfZTtcbiAgICB9XG4gIH1cblxuICByZXR1cm4gX2Fycjtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBfaXRlcmFibGVUb0FycmF5TGltaXQ7IiwiZnVuY3Rpb24gX25vbkl0ZXJhYmxlUmVzdCgpIHtcbiAgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkludmFsaWQgYXR0ZW1wdCB0byBkZXN0cnVjdHVyZSBub24taXRlcmFibGUgaW5zdGFuY2UuXFxuSW4gb3JkZXIgdG8gYmUgaXRlcmFibGUsIG5vbi1hcnJheSBvYmplY3RzIG11c3QgaGF2ZSBhIFtTeW1ib2wuaXRlcmF0b3JdKCkgbWV0aG9kLlwiKTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBfbm9uSXRlcmFibGVSZXN0OyIsImZ1bmN0aW9uIF9ub25JdGVyYWJsZVNwcmVhZCgpIHtcbiAgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkludmFsaWQgYXR0ZW1wdCB0byBzcHJlYWQgbm9uLWl0ZXJhYmxlIGluc3RhbmNlLlxcbkluIG9yZGVyIHRvIGJlIGl0ZXJhYmxlLCBub24tYXJyYXkgb2JqZWN0cyBtdXN0IGhhdmUgYSBbU3ltYm9sLml0ZXJhdG9yXSgpIG1ldGhvZC5cIik7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gX25vbkl0ZXJhYmxlU3ByZWFkOyIsInZhciBfdHlwZW9mID0gcmVxdWlyZShcIi4uL2hlbHBlcnMvdHlwZW9mXCIpO1xuXG52YXIgYXNzZXJ0VGhpc0luaXRpYWxpemVkID0gcmVxdWlyZShcIi4vYXNzZXJ0VGhpc0luaXRpYWxpemVkXCIpO1xuXG5mdW5jdGlvbiBfcG9zc2libGVDb25zdHJ1Y3RvclJldHVybihzZWxmLCBjYWxsKSB7XG4gIGlmIChjYWxsICYmIChfdHlwZW9mKGNhbGwpID09PSBcIm9iamVjdFwiIHx8IHR5cGVvZiBjYWxsID09PSBcImZ1bmN0aW9uXCIpKSB7XG4gICAgcmV0dXJuIGNhbGw7XG4gIH1cblxuICByZXR1cm4gYXNzZXJ0VGhpc0luaXRpYWxpemVkKHNlbGYpO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IF9wb3NzaWJsZUNvbnN0cnVjdG9yUmV0dXJuOyIsImZ1bmN0aW9uIF9zZXRQcm90b3R5cGVPZihvLCBwKSB7XG4gIG1vZHVsZS5leHBvcnRzID0gX3NldFByb3RvdHlwZU9mID0gT2JqZWN0LnNldFByb3RvdHlwZU9mIHx8IGZ1bmN0aW9uIF9zZXRQcm90b3R5cGVPZihvLCBwKSB7XG4gICAgby5fX3Byb3RvX18gPSBwO1xuICAgIHJldHVybiBvO1xuICB9O1xuXG4gIHJldHVybiBfc2V0UHJvdG90eXBlT2YobywgcCk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gX3NldFByb3RvdHlwZU9mOyIsInZhciBhcnJheVdpdGhIb2xlcyA9IHJlcXVpcmUoXCIuL2FycmF5V2l0aEhvbGVzXCIpO1xuXG52YXIgaXRlcmFibGVUb0FycmF5TGltaXQgPSByZXF1aXJlKFwiLi9pdGVyYWJsZVRvQXJyYXlMaW1pdFwiKTtcblxudmFyIHVuc3VwcG9ydGVkSXRlcmFibGVUb0FycmF5ID0gcmVxdWlyZShcIi4vdW5zdXBwb3J0ZWRJdGVyYWJsZVRvQXJyYXlcIik7XG5cbnZhciBub25JdGVyYWJsZVJlc3QgPSByZXF1aXJlKFwiLi9ub25JdGVyYWJsZVJlc3RcIik7XG5cbmZ1bmN0aW9uIF9zbGljZWRUb0FycmF5KGFyciwgaSkge1xuICByZXR1cm4gYXJyYXlXaXRoSG9sZXMoYXJyKSB8fCBpdGVyYWJsZVRvQXJyYXlMaW1pdChhcnIsIGkpIHx8IHVuc3VwcG9ydGVkSXRlcmFibGVUb0FycmF5KGFyciwgaSkgfHwgbm9uSXRlcmFibGVSZXN0KCk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gX3NsaWNlZFRvQXJyYXk7IiwidmFyIGFycmF5V2l0aG91dEhvbGVzID0gcmVxdWlyZShcIi4vYXJyYXlXaXRob3V0SG9sZXNcIik7XG5cbnZhciBpdGVyYWJsZVRvQXJyYXkgPSByZXF1aXJlKFwiLi9pdGVyYWJsZVRvQXJyYXlcIik7XG5cbnZhciB1bnN1cHBvcnRlZEl0ZXJhYmxlVG9BcnJheSA9IHJlcXVpcmUoXCIuL3Vuc3VwcG9ydGVkSXRlcmFibGVUb0FycmF5XCIpO1xuXG52YXIgbm9uSXRlcmFibGVTcHJlYWQgPSByZXF1aXJlKFwiLi9ub25JdGVyYWJsZVNwcmVhZFwiKTtcblxuZnVuY3Rpb24gX3RvQ29uc3VtYWJsZUFycmF5KGFycikge1xuICByZXR1cm4gYXJyYXlXaXRob3V0SG9sZXMoYXJyKSB8fCBpdGVyYWJsZVRvQXJyYXkoYXJyKSB8fCB1bnN1cHBvcnRlZEl0ZXJhYmxlVG9BcnJheShhcnIpIHx8IG5vbkl0ZXJhYmxlU3ByZWFkKCk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gX3RvQ29uc3VtYWJsZUFycmF5OyIsImZ1bmN0aW9uIF90eXBlb2Yob2JqKSB7XG4gIFwiQGJhYmVsL2hlbHBlcnMgLSB0eXBlb2ZcIjtcblxuICBpZiAodHlwZW9mIFN5bWJvbCA9PT0gXCJmdW5jdGlvblwiICYmIHR5cGVvZiBTeW1ib2wuaXRlcmF0b3IgPT09IFwic3ltYm9sXCIpIHtcbiAgICBtb2R1bGUuZXhwb3J0cyA9IF90eXBlb2YgPSBmdW5jdGlvbiBfdHlwZW9mKG9iaikge1xuICAgICAgcmV0dXJuIHR5cGVvZiBvYmo7XG4gICAgfTtcbiAgfSBlbHNlIHtcbiAgICBtb2R1bGUuZXhwb3J0cyA9IF90eXBlb2YgPSBmdW5jdGlvbiBfdHlwZW9mKG9iaikge1xuICAgICAgcmV0dXJuIG9iaiAmJiB0eXBlb2YgU3ltYm9sID09PSBcImZ1bmN0aW9uXCIgJiYgb2JqLmNvbnN0cnVjdG9yID09PSBTeW1ib2wgJiYgb2JqICE9PSBTeW1ib2wucHJvdG90eXBlID8gXCJzeW1ib2xcIiA6IHR5cGVvZiBvYmo7XG4gICAgfTtcbiAgfVxuXG4gIHJldHVybiBfdHlwZW9mKG9iaik7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gX3R5cGVvZjsiLCJ2YXIgYXJyYXlMaWtlVG9BcnJheSA9IHJlcXVpcmUoXCIuL2FycmF5TGlrZVRvQXJyYXlcIik7XG5cbmZ1bmN0aW9uIF91bnN1cHBvcnRlZEl0ZXJhYmxlVG9BcnJheShvLCBtaW5MZW4pIHtcbiAgaWYgKCFvKSByZXR1cm47XG4gIGlmICh0eXBlb2YgbyA9PT0gXCJzdHJpbmdcIikgcmV0dXJuIGFycmF5TGlrZVRvQXJyYXkobywgbWluTGVuKTtcbiAgdmFyIG4gPSBPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nLmNhbGwobykuc2xpY2UoOCwgLTEpO1xuICBpZiAobiA9PT0gXCJPYmplY3RcIiAmJiBvLmNvbnN0cnVjdG9yKSBuID0gby5jb25zdHJ1Y3Rvci5uYW1lO1xuICBpZiAobiA9PT0gXCJNYXBcIiB8fCBuID09PSBcIlNldFwiKSByZXR1cm4gQXJyYXkuZnJvbShvKTtcbiAgaWYgKG4gPT09IFwiQXJndW1lbnRzXCIgfHwgL14oPzpVaXxJKW50KD86OHwxNnwzMikoPzpDbGFtcGVkKT9BcnJheSQvLnRlc3QobikpIHJldHVybiBhcnJheUxpa2VUb0FycmF5KG8sIG1pbkxlbik7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gX3Vuc3VwcG9ydGVkSXRlcmFibGVUb0FycmF5OyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlZ2VuZXJhdG9yLXJ1bnRpbWVcIik7XG4iLCJpbXBvcnQgKiBhcyB0c2xpYl8xIGZyb20gXCJ0c2xpYlwiO1xuaW1wb3J0IHsgQmFzZUJhY2tlbmQgfSBmcm9tICdAc2VudHJ5L2NvcmUnO1xuaW1wb3J0IHsgU2V2ZXJpdHkgfSBmcm9tICdAc2VudHJ5L3R5cGVzJztcbmltcG9ydCB7IGFkZEV4Y2VwdGlvbk1lY2hhbmlzbSwgc3VwcG9ydHNGZXRjaCwgU3luY1Byb21pc2UgfSBmcm9tICdAc2VudHJ5L3V0aWxzJztcbmltcG9ydCB7IGV2ZW50RnJvbVN0cmluZywgZXZlbnRGcm9tVW5rbm93bklucHV0IH0gZnJvbSAnLi9ldmVudGJ1aWxkZXInO1xuaW1wb3J0IHsgRmV0Y2hUcmFuc3BvcnQsIFhIUlRyYW5zcG9ydCB9IGZyb20gJy4vdHJhbnNwb3J0cyc7XG4vKipcbiAqIFRoZSBTZW50cnkgQnJvd3NlciBTREsgQmFja2VuZC5cbiAqIEBoaWRkZW5cbiAqL1xudmFyIEJyb3dzZXJCYWNrZW5kID0gLyoqIEBjbGFzcyAqLyAoZnVuY3Rpb24gKF9zdXBlcikge1xuICAgIHRzbGliXzEuX19leHRlbmRzKEJyb3dzZXJCYWNrZW5kLCBfc3VwZXIpO1xuICAgIGZ1bmN0aW9uIEJyb3dzZXJCYWNrZW5kKCkge1xuICAgICAgICByZXR1cm4gX3N1cGVyICE9PSBudWxsICYmIF9zdXBlci5hcHBseSh0aGlzLCBhcmd1bWVudHMpIHx8IHRoaXM7XG4gICAgfVxuICAgIC8qKlxuICAgICAqIEBpbmhlcml0RG9jXG4gICAgICovXG4gICAgQnJvd3NlckJhY2tlbmQucHJvdG90eXBlLl9zZXR1cFRyYW5zcG9ydCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgaWYgKCF0aGlzLl9vcHRpb25zLmRzbikge1xuICAgICAgICAgICAgLy8gV2UgcmV0dXJuIHRoZSBub29wIHRyYW5zcG9ydCBoZXJlIGluIGNhc2UgdGhlcmUgaXMgbm8gRHNuLlxuICAgICAgICAgICAgcmV0dXJuIF9zdXBlci5wcm90b3R5cGUuX3NldHVwVHJhbnNwb3J0LmNhbGwodGhpcyk7XG4gICAgICAgIH1cbiAgICAgICAgdmFyIHRyYW5zcG9ydE9wdGlvbnMgPSB0c2xpYl8xLl9fYXNzaWduKHt9LCB0aGlzLl9vcHRpb25zLnRyYW5zcG9ydE9wdGlvbnMsIHsgZHNuOiB0aGlzLl9vcHRpb25zLmRzbiB9KTtcbiAgICAgICAgaWYgKHRoaXMuX29wdGlvbnMudHJhbnNwb3J0KSB7XG4gICAgICAgICAgICByZXR1cm4gbmV3IHRoaXMuX29wdGlvbnMudHJhbnNwb3J0KHRyYW5zcG9ydE9wdGlvbnMpO1xuICAgICAgICB9XG4gICAgICAgIGlmIChzdXBwb3J0c0ZldGNoKCkpIHtcbiAgICAgICAgICAgIHJldHVybiBuZXcgRmV0Y2hUcmFuc3BvcnQodHJhbnNwb3J0T3B0aW9ucyk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIG5ldyBYSFJUcmFuc3BvcnQodHJhbnNwb3J0T3B0aW9ucyk7XG4gICAgfTtcbiAgICAvKipcbiAgICAgKiBAaW5oZXJpdERvY1xuICAgICAqL1xuICAgIEJyb3dzZXJCYWNrZW5kLnByb3RvdHlwZS5ldmVudEZyb21FeGNlcHRpb24gPSBmdW5jdGlvbiAoZXhjZXB0aW9uLCBoaW50KSB7XG4gICAgICAgIHZhciBzeW50aGV0aWNFeGNlcHRpb24gPSAoaGludCAmJiBoaW50LnN5bnRoZXRpY0V4Y2VwdGlvbikgfHwgdW5kZWZpbmVkO1xuICAgICAgICB2YXIgZXZlbnQgPSBldmVudEZyb21Vbmtub3duSW5wdXQoZXhjZXB0aW9uLCBzeW50aGV0aWNFeGNlcHRpb24sIHtcbiAgICAgICAgICAgIGF0dGFjaFN0YWNrdHJhY2U6IHRoaXMuX29wdGlvbnMuYXR0YWNoU3RhY2t0cmFjZSxcbiAgICAgICAgfSk7XG4gICAgICAgIGFkZEV4Y2VwdGlvbk1lY2hhbmlzbShldmVudCwge1xuICAgICAgICAgICAgaGFuZGxlZDogdHJ1ZSxcbiAgICAgICAgICAgIHR5cGU6ICdnZW5lcmljJyxcbiAgICAgICAgfSk7XG4gICAgICAgIGV2ZW50LmxldmVsID0gU2V2ZXJpdHkuRXJyb3I7XG4gICAgICAgIGlmIChoaW50ICYmIGhpbnQuZXZlbnRfaWQpIHtcbiAgICAgICAgICAgIGV2ZW50LmV2ZW50X2lkID0gaGludC5ldmVudF9pZDtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gU3luY1Byb21pc2UucmVzb2x2ZShldmVudCk7XG4gICAgfTtcbiAgICAvKipcbiAgICAgKiBAaW5oZXJpdERvY1xuICAgICAqL1xuICAgIEJyb3dzZXJCYWNrZW5kLnByb3RvdHlwZS5ldmVudEZyb21NZXNzYWdlID0gZnVuY3Rpb24gKG1lc3NhZ2UsIGxldmVsLCBoaW50KSB7XG4gICAgICAgIGlmIChsZXZlbCA9PT0gdm9pZCAwKSB7IGxldmVsID0gU2V2ZXJpdHkuSW5mbzsgfVxuICAgICAgICB2YXIgc3ludGhldGljRXhjZXB0aW9uID0gKGhpbnQgJiYgaGludC5zeW50aGV0aWNFeGNlcHRpb24pIHx8IHVuZGVmaW5lZDtcbiAgICAgICAgdmFyIGV2ZW50ID0gZXZlbnRGcm9tU3RyaW5nKG1lc3NhZ2UsIHN5bnRoZXRpY0V4Y2VwdGlvbiwge1xuICAgICAgICAgICAgYXR0YWNoU3RhY2t0cmFjZTogdGhpcy5fb3B0aW9ucy5hdHRhY2hTdGFja3RyYWNlLFxuICAgICAgICB9KTtcbiAgICAgICAgZXZlbnQubGV2ZWwgPSBsZXZlbDtcbiAgICAgICAgaWYgKGhpbnQgJiYgaGludC5ldmVudF9pZCkge1xuICAgICAgICAgICAgZXZlbnQuZXZlbnRfaWQgPSBoaW50LmV2ZW50X2lkO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBTeW5jUHJvbWlzZS5yZXNvbHZlKGV2ZW50KTtcbiAgICB9O1xuICAgIHJldHVybiBCcm93c2VyQmFja2VuZDtcbn0oQmFzZUJhY2tlbmQpKTtcbmV4cG9ydCB7IEJyb3dzZXJCYWNrZW5kIH07XG4vLyMgc291cmNlTWFwcGluZ1VSTD1iYWNrZW5kLmpzLm1hcCIsImltcG9ydCAqIGFzIHRzbGliXzEgZnJvbSBcInRzbGliXCI7XG5pbXBvcnQgeyBBUEksIEJhc2VDbGllbnQgfSBmcm9tICdAc2VudHJ5L2NvcmUnO1xuaW1wb3J0IHsgZ2V0R2xvYmFsT2JqZWN0LCBsb2dnZXIgfSBmcm9tICdAc2VudHJ5L3V0aWxzJztcbmltcG9ydCB7IEJyb3dzZXJCYWNrZW5kIH0gZnJvbSAnLi9iYWNrZW5kJztcbmltcG9ydCB7IEJyZWFkY3J1bWJzIH0gZnJvbSAnLi9pbnRlZ3JhdGlvbnMnO1xuaW1wb3J0IHsgU0RLX05BTUUsIFNES19WRVJTSU9OIH0gZnJvbSAnLi92ZXJzaW9uJztcbi8qKlxuICogVGhlIFNlbnRyeSBCcm93c2VyIFNESyBDbGllbnQuXG4gKlxuICogQHNlZSBCcm93c2VyT3B0aW9ucyBmb3IgZG9jdW1lbnRhdGlvbiBvbiBjb25maWd1cmF0aW9uIG9wdGlvbnMuXG4gKiBAc2VlIFNlbnRyeUNsaWVudCBmb3IgdXNhZ2UgZG9jdW1lbnRhdGlvbi5cbiAqL1xudmFyIEJyb3dzZXJDbGllbnQgPSAvKiogQGNsYXNzICovIChmdW5jdGlvbiAoX3N1cGVyKSB7XG4gICAgdHNsaWJfMS5fX2V4dGVuZHMoQnJvd3NlckNsaWVudCwgX3N1cGVyKTtcbiAgICAvKipcbiAgICAgKiBDcmVhdGVzIGEgbmV3IEJyb3dzZXIgU0RLIGluc3RhbmNlLlxuICAgICAqXG4gICAgICogQHBhcmFtIG9wdGlvbnMgQ29uZmlndXJhdGlvbiBvcHRpb25zIGZvciB0aGlzIFNESy5cbiAgICAgKi9cbiAgICBmdW5jdGlvbiBCcm93c2VyQ2xpZW50KG9wdGlvbnMpIHtcbiAgICAgICAgaWYgKG9wdGlvbnMgPT09IHZvaWQgMCkgeyBvcHRpb25zID0ge307IH1cbiAgICAgICAgcmV0dXJuIF9zdXBlci5jYWxsKHRoaXMsIEJyb3dzZXJCYWNrZW5kLCBvcHRpb25zKSB8fCB0aGlzO1xuICAgIH1cbiAgICAvKipcbiAgICAgKiBAaW5oZXJpdERvY1xuICAgICAqL1xuICAgIEJyb3dzZXJDbGllbnQucHJvdG90eXBlLl9wcmVwYXJlRXZlbnQgPSBmdW5jdGlvbiAoZXZlbnQsIHNjb3BlLCBoaW50KSB7XG4gICAgICAgIGV2ZW50LnBsYXRmb3JtID0gZXZlbnQucGxhdGZvcm0gfHwgJ2phdmFzY3JpcHQnO1xuICAgICAgICBldmVudC5zZGsgPSB0c2xpYl8xLl9fYXNzaWduKHt9LCBldmVudC5zZGssIHsgbmFtZTogU0RLX05BTUUsIHBhY2thZ2VzOiB0c2xpYl8xLl9fc3ByZWFkKCgoZXZlbnQuc2RrICYmIGV2ZW50LnNkay5wYWNrYWdlcykgfHwgW10pLCBbXG4gICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICBuYW1lOiAnbnBtOkBzZW50cnkvYnJvd3NlcicsXG4gICAgICAgICAgICAgICAgICAgIHZlcnNpb246IFNES19WRVJTSU9OLFxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBdKSwgdmVyc2lvbjogU0RLX1ZFUlNJT04gfSk7XG4gICAgICAgIHJldHVybiBfc3VwZXIucHJvdG90eXBlLl9wcmVwYXJlRXZlbnQuY2FsbCh0aGlzLCBldmVudCwgc2NvcGUsIGhpbnQpO1xuICAgIH07XG4gICAgLyoqXG4gICAgICogQGluaGVyaXREb2NcbiAgICAgKi9cbiAgICBCcm93c2VyQ2xpZW50LnByb3RvdHlwZS5fc2VuZEV2ZW50ID0gZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICAgIHZhciBpbnRlZ3JhdGlvbiA9IHRoaXMuZ2V0SW50ZWdyYXRpb24oQnJlYWRjcnVtYnMpO1xuICAgICAgICBpZiAoaW50ZWdyYXRpb24pIHtcbiAgICAgICAgICAgIGludGVncmF0aW9uLmFkZFNlbnRyeUJyZWFkY3J1bWIoZXZlbnQpO1xuICAgICAgICB9XG4gICAgICAgIF9zdXBlci5wcm90b3R5cGUuX3NlbmRFdmVudC5jYWxsKHRoaXMsIGV2ZW50KTtcbiAgICB9O1xuICAgIC8qKlxuICAgICAqIFNob3cgYSByZXBvcnQgZGlhbG9nIHRvIHRoZSB1c2VyIHRvIHNlbmQgZmVlZGJhY2sgdG8gYSBzcGVjaWZpYyBldmVudC5cbiAgICAgKlxuICAgICAqIEBwYXJhbSBvcHRpb25zIFNldCBpbmRpdmlkdWFsIG9wdGlvbnMgZm9yIHRoZSBkaWFsb2dcbiAgICAgKi9cbiAgICBCcm93c2VyQ2xpZW50LnByb3RvdHlwZS5zaG93UmVwb3J0RGlhbG9nID0gZnVuY3Rpb24gKG9wdGlvbnMpIHtcbiAgICAgICAgaWYgKG9wdGlvbnMgPT09IHZvaWQgMCkgeyBvcHRpb25zID0ge307IH1cbiAgICAgICAgLy8gZG9lc24ndCB3b3JrIHdpdGhvdXQgYSBkb2N1bWVudCAoUmVhY3QgTmF0aXZlKVxuICAgICAgICB2YXIgZG9jdW1lbnQgPSBnZXRHbG9iYWxPYmplY3QoKS5kb2N1bWVudDtcbiAgICAgICAgaWYgKCFkb2N1bWVudCkge1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIGlmICghdGhpcy5faXNFbmFibGVkKCkpIHtcbiAgICAgICAgICAgIGxvZ2dlci5lcnJvcignVHJ5aW5nIHRvIGNhbGwgc2hvd1JlcG9ydERpYWxvZyB3aXRoIFNlbnRyeSBDbGllbnQgaXMgZGlzYWJsZWQnKTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuICAgICAgICB2YXIgZHNuID0gb3B0aW9ucy5kc24gfHwgdGhpcy5nZXREc24oKTtcbiAgICAgICAgaWYgKCFvcHRpb25zLmV2ZW50SWQpIHtcbiAgICAgICAgICAgIGxvZ2dlci5lcnJvcignTWlzc2luZyBgZXZlbnRJZGAgb3B0aW9uIGluIHNob3dSZXBvcnREaWFsb2cgY2FsbCcpO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIGlmICghZHNuKSB7XG4gICAgICAgICAgICBsb2dnZXIuZXJyb3IoJ01pc3NpbmcgYERzbmAgb3B0aW9uIGluIHNob3dSZXBvcnREaWFsb2cgY2FsbCcpO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIHZhciBzY3JpcHQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdzY3JpcHQnKTtcbiAgICAgICAgc2NyaXB0LmFzeW5jID0gdHJ1ZTtcbiAgICAgICAgc2NyaXB0LnNyYyA9IG5ldyBBUEkoZHNuKS5nZXRSZXBvcnREaWFsb2dFbmRwb2ludChvcHRpb25zKTtcbiAgICAgICAgaWYgKG9wdGlvbnMub25Mb2FkKSB7XG4gICAgICAgICAgICBzY3JpcHQub25sb2FkID0gb3B0aW9ucy5vbkxvYWQ7XG4gICAgICAgIH1cbiAgICAgICAgKGRvY3VtZW50LmhlYWQgfHwgZG9jdW1lbnQuYm9keSkuYXBwZW5kQ2hpbGQoc2NyaXB0KTtcbiAgICB9O1xuICAgIHJldHVybiBCcm93c2VyQ2xpZW50O1xufShCYXNlQ2xpZW50KSk7XG5leHBvcnQgeyBCcm93c2VyQ2xpZW50IH07XG4vLyMgc291cmNlTWFwcGluZ1VSTD1jbGllbnQuanMubWFwIiwiaW1wb3J0IHsgYWRkRXhjZXB0aW9uTWVjaGFuaXNtLCBhZGRFeGNlcHRpb25UeXBlVmFsdWUsIGlzRE9NRXJyb3IsIGlzRE9NRXhjZXB0aW9uLCBpc0Vycm9yLCBpc0Vycm9yRXZlbnQsIGlzRXZlbnQsIGlzUGxhaW5PYmplY3QsIH0gZnJvbSAnQHNlbnRyeS91dGlscyc7XG5pbXBvcnQgeyBldmVudEZyb21QbGFpbk9iamVjdCwgZXZlbnRGcm9tU3RhY2t0cmFjZSwgcHJlcGFyZUZyYW1lc0ZvckV2ZW50IH0gZnJvbSAnLi9wYXJzZXJzJztcbmltcG9ydCB7IGNvbXB1dGVTdGFja1RyYWNlIH0gZnJvbSAnLi90cmFjZWtpdCc7XG4vKiogSlNEb2MgKi9cbmV4cG9ydCBmdW5jdGlvbiBldmVudEZyb21Vbmtub3duSW5wdXQoZXhjZXB0aW9uLCBzeW50aGV0aWNFeGNlcHRpb24sIG9wdGlvbnMpIHtcbiAgICBpZiAob3B0aW9ucyA9PT0gdm9pZCAwKSB7IG9wdGlvbnMgPSB7fTsgfVxuICAgIHZhciBldmVudDtcbiAgICBpZiAoaXNFcnJvckV2ZW50KGV4Y2VwdGlvbikgJiYgZXhjZXB0aW9uLmVycm9yKSB7XG4gICAgICAgIC8vIElmIGl0IGlzIGFuIEVycm9yRXZlbnQgd2l0aCBgZXJyb3JgIHByb3BlcnR5LCBleHRyYWN0IGl0IHRvIGdldCBhY3R1YWwgRXJyb3JcbiAgICAgICAgdmFyIGVycm9yRXZlbnQgPSBleGNlcHRpb247XG4gICAgICAgIGV4Y2VwdGlvbiA9IGVycm9yRXZlbnQuZXJyb3I7IC8vIHRzbGludDpkaXNhYmxlLWxpbmU6bm8tcGFyYW1ldGVyLXJlYXNzaWdubWVudFxuICAgICAgICBldmVudCA9IGV2ZW50RnJvbVN0YWNrdHJhY2UoY29tcHV0ZVN0YWNrVHJhY2UoZXhjZXB0aW9uKSk7XG4gICAgICAgIHJldHVybiBldmVudDtcbiAgICB9XG4gICAgaWYgKGlzRE9NRXJyb3IoZXhjZXB0aW9uKSB8fCBpc0RPTUV4Y2VwdGlvbihleGNlcHRpb24pKSB7XG4gICAgICAgIC8vIElmIGl0IGlzIGEgRE9NRXJyb3Igb3IgRE9NRXhjZXB0aW9uICh3aGljaCBhcmUgbGVnYWN5IEFQSXMsIGJ1dCBzdGlsbCBzdXBwb3J0ZWQgaW4gc29tZSBicm93c2VycylcbiAgICAgICAgLy8gdGhlbiB3ZSBqdXN0IGV4dHJhY3QgdGhlIG5hbWUgYW5kIG1lc3NhZ2UsIGFzIHRoZXkgZG9uJ3QgcHJvdmlkZSBhbnl0aGluZyBlbHNlXG4gICAgICAgIC8vIGh0dHBzOi8vZGV2ZWxvcGVyLm1vemlsbGEub3JnL2VuLVVTL2RvY3MvV2ViL0FQSS9ET01FcnJvclxuICAgICAgICAvLyBodHRwczovL2RldmVsb3Blci5tb3ppbGxhLm9yZy9lbi1VUy9kb2NzL1dlYi9BUEkvRE9NRXhjZXB0aW9uXG4gICAgICAgIHZhciBkb21FeGNlcHRpb24gPSBleGNlcHRpb247XG4gICAgICAgIHZhciBuYW1lXzEgPSBkb21FeGNlcHRpb24ubmFtZSB8fCAoaXNET01FcnJvcihkb21FeGNlcHRpb24pID8gJ0RPTUVycm9yJyA6ICdET01FeGNlcHRpb24nKTtcbiAgICAgICAgdmFyIG1lc3NhZ2UgPSBkb21FeGNlcHRpb24ubWVzc2FnZSA/IG5hbWVfMSArIFwiOiBcIiArIGRvbUV4Y2VwdGlvbi5tZXNzYWdlIDogbmFtZV8xO1xuICAgICAgICBldmVudCA9IGV2ZW50RnJvbVN0cmluZyhtZXNzYWdlLCBzeW50aGV0aWNFeGNlcHRpb24sIG9wdGlvbnMpO1xuICAgICAgICBhZGRFeGNlcHRpb25UeXBlVmFsdWUoZXZlbnQsIG1lc3NhZ2UpO1xuICAgICAgICByZXR1cm4gZXZlbnQ7XG4gICAgfVxuICAgIGlmIChpc0Vycm9yKGV4Y2VwdGlvbikpIHtcbiAgICAgICAgLy8gd2UgaGF2ZSBhIHJlYWwgRXJyb3Igb2JqZWN0LCBkbyBub3RoaW5nXG4gICAgICAgIGV2ZW50ID0gZXZlbnRGcm9tU3RhY2t0cmFjZShjb21wdXRlU3RhY2tUcmFjZShleGNlcHRpb24pKTtcbiAgICAgICAgcmV0dXJuIGV2ZW50O1xuICAgIH1cbiAgICBpZiAoaXNQbGFpbk9iamVjdChleGNlcHRpb24pIHx8IGlzRXZlbnQoZXhjZXB0aW9uKSkge1xuICAgICAgICAvLyBJZiBpdCBpcyBwbGFpbiBPYmplY3Qgb3IgRXZlbnQsIHNlcmlhbGl6ZSBpdCBtYW51YWxseSBhbmQgZXh0cmFjdCBvcHRpb25zXG4gICAgICAgIC8vIFRoaXMgd2lsbCBhbGxvdyB1cyB0byBncm91cCBldmVudHMgYmFzZWQgb24gdG9wLWxldmVsIGtleXNcbiAgICAgICAgLy8gd2hpY2ggaXMgbXVjaCBiZXR0ZXIgdGhhbiBjcmVhdGluZyBuZXcgZ3JvdXAgd2hlbiBhbnkga2V5L3ZhbHVlIGNoYW5nZVxuICAgICAgICB2YXIgb2JqZWN0RXhjZXB0aW9uID0gZXhjZXB0aW9uO1xuICAgICAgICBldmVudCA9IGV2ZW50RnJvbVBsYWluT2JqZWN0KG9iamVjdEV4Y2VwdGlvbiwgc3ludGhldGljRXhjZXB0aW9uLCBvcHRpb25zLnJlamVjdGlvbik7XG4gICAgICAgIGFkZEV4Y2VwdGlvbk1lY2hhbmlzbShldmVudCwge1xuICAgICAgICAgICAgc3ludGhldGljOiB0cnVlLFxuICAgICAgICB9KTtcbiAgICAgICAgcmV0dXJuIGV2ZW50O1xuICAgIH1cbiAgICAvLyBJZiBub25lIG9mIHByZXZpb3VzIGNoZWNrcyB3ZXJlIHZhbGlkLCB0aGVuIGl0IG1lYW5zIHRoYXQgaXQncyBub3Q6XG4gICAgLy8gLSBhbiBpbnN0YW5jZSBvZiBET01FcnJvclxuICAgIC8vIC0gYW4gaW5zdGFuY2Ugb2YgRE9NRXhjZXB0aW9uXG4gICAgLy8gLSBhbiBpbnN0YW5jZSBvZiBFdmVudFxuICAgIC8vIC0gYW4gaW5zdGFuY2Ugb2YgRXJyb3JcbiAgICAvLyAtIGEgdmFsaWQgRXJyb3JFdmVudCAob25lIHdpdGggYW4gZXJyb3IgcHJvcGVydHkpXG4gICAgLy8gLSBhIHBsYWluIE9iamVjdFxuICAgIC8vXG4gICAgLy8gU28gYmFpbCBvdXQgYW5kIGNhcHR1cmUgaXQgYXMgYSBzaW1wbGUgbWVzc2FnZTpcbiAgICBldmVudCA9IGV2ZW50RnJvbVN0cmluZyhleGNlcHRpb24sIHN5bnRoZXRpY0V4Y2VwdGlvbiwgb3B0aW9ucyk7XG4gICAgYWRkRXhjZXB0aW9uVHlwZVZhbHVlKGV2ZW50LCBcIlwiICsgZXhjZXB0aW9uLCB1bmRlZmluZWQpO1xuICAgIGFkZEV4Y2VwdGlvbk1lY2hhbmlzbShldmVudCwge1xuICAgICAgICBzeW50aGV0aWM6IHRydWUsXG4gICAgfSk7XG4gICAgcmV0dXJuIGV2ZW50O1xufVxuLy8gdGhpcy5fb3B0aW9ucy5hdHRhY2hTdGFja3RyYWNlXG4vKiogSlNEb2MgKi9cbmV4cG9ydCBmdW5jdGlvbiBldmVudEZyb21TdHJpbmcoaW5wdXQsIHN5bnRoZXRpY0V4Y2VwdGlvbiwgb3B0aW9ucykge1xuICAgIGlmIChvcHRpb25zID09PSB2b2lkIDApIHsgb3B0aW9ucyA9IHt9OyB9XG4gICAgdmFyIGV2ZW50ID0ge1xuICAgICAgICBtZXNzYWdlOiBpbnB1dCxcbiAgICB9O1xuICAgIGlmIChvcHRpb25zLmF0dGFjaFN0YWNrdHJhY2UgJiYgc3ludGhldGljRXhjZXB0aW9uKSB7XG4gICAgICAgIHZhciBzdGFja3RyYWNlID0gY29tcHV0ZVN0YWNrVHJhY2Uoc3ludGhldGljRXhjZXB0aW9uKTtcbiAgICAgICAgdmFyIGZyYW1lc18xID0gcHJlcGFyZUZyYW1lc0ZvckV2ZW50KHN0YWNrdHJhY2Uuc3RhY2spO1xuICAgICAgICBldmVudC5zdGFja3RyYWNlID0ge1xuICAgICAgICAgICAgZnJhbWVzOiBmcmFtZXNfMSxcbiAgICAgICAgfTtcbiAgICB9XG4gICAgcmV0dXJuIGV2ZW50O1xufVxuLy8jIHNvdXJjZU1hcHBpbmdVUkw9ZXZlbnRidWlsZGVyLmpzLm1hcCIsImV4cG9ydCB7IFNldmVyaXR5LCBTdGF0dXMsIH0gZnJvbSAnQHNlbnRyeS90eXBlcyc7XG5leHBvcnQgeyBhZGRHbG9iYWxFdmVudFByb2Nlc3NvciwgYWRkQnJlYWRjcnVtYiwgY2FwdHVyZUV4Y2VwdGlvbiwgY2FwdHVyZUV2ZW50LCBjYXB0dXJlTWVzc2FnZSwgY29uZmlndXJlU2NvcGUsIGdldEh1YkZyb21DYXJyaWVyLCBnZXRDdXJyZW50SHViLCBIdWIsIG1ha2VNYWluLCBTY29wZSwgc3RhcnRUcmFuc2FjdGlvbiwgc2V0Q29udGV4dCwgc2V0RXh0cmEsIHNldEV4dHJhcywgc2V0VGFnLCBzZXRUYWdzLCBzZXRVc2VyLCB3aXRoU2NvcGUsIH0gZnJvbSAnQHNlbnRyeS9jb3JlJztcbmV4cG9ydCB7IEJyb3dzZXJDbGllbnQgfSBmcm9tICcuL2NsaWVudCc7XG5leHBvcnQgeyBkZWZhdWx0SW50ZWdyYXRpb25zLCBmb3JjZUxvYWQsIGluaXQsIGxhc3RFdmVudElkLCBvbkxvYWQsIHNob3dSZXBvcnREaWFsb2csIGZsdXNoLCBjbG9zZSwgd3JhcCB9IGZyb20gJy4vc2RrJztcbmV4cG9ydCB7IFNES19OQU1FLCBTREtfVkVSU0lPTiB9IGZyb20gJy4vdmVyc2lvbic7XG4vLyMgc291cmNlTWFwcGluZ1VSTD1leHBvcnRzLmpzLm1hcCIsImltcG9ydCAqIGFzIHRzbGliXzEgZnJvbSBcInRzbGliXCI7XG5pbXBvcnQgeyBjYXB0dXJlRXhjZXB0aW9uLCB3aXRoU2NvcGUgfSBmcm9tICdAc2VudHJ5L2NvcmUnO1xuaW1wb3J0IHsgYWRkRXhjZXB0aW9uTWVjaGFuaXNtLCBhZGRFeGNlcHRpb25UeXBlVmFsdWUgfSBmcm9tICdAc2VudHJ5L3V0aWxzJztcbnZhciBpZ25vcmVPbkVycm9yID0gMDtcbi8qKlxuICogQGhpZGRlblxuICovXG5leHBvcnQgZnVuY3Rpb24gc2hvdWxkSWdub3JlT25FcnJvcigpIHtcbiAgICByZXR1cm4gaWdub3JlT25FcnJvciA+IDA7XG59XG4vKipcbiAqIEBoaWRkZW5cbiAqL1xuZXhwb3J0IGZ1bmN0aW9uIGlnbm9yZU5leHRPbkVycm9yKCkge1xuICAgIC8vIG9uZXJyb3Igc2hvdWxkIHRyaWdnZXIgYmVmb3JlIHNldFRpbWVvdXRcbiAgICBpZ25vcmVPbkVycm9yICs9IDE7XG4gICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XG4gICAgICAgIGlnbm9yZU9uRXJyb3IgLT0gMTtcbiAgICB9KTtcbn1cbi8qKlxuICogSW5zdHJ1bWVudHMgdGhlIGdpdmVuIGZ1bmN0aW9uIGFuZCBzZW5kcyBhbiBldmVudCB0byBTZW50cnkgZXZlcnkgdGltZSB0aGVcbiAqIGZ1bmN0aW9uIHRocm93cyBhbiBleGNlcHRpb24uXG4gKlxuICogQHBhcmFtIGZuIEEgZnVuY3Rpb24gdG8gd3JhcC5cbiAqIEByZXR1cm5zIFRoZSB3cmFwcGVkIGZ1bmN0aW9uLlxuICogQGhpZGRlblxuICovXG5leHBvcnQgZnVuY3Rpb24gd3JhcChmbiwgb3B0aW9ucywgYmVmb3JlKSB7XG4gICAgaWYgKG9wdGlvbnMgPT09IHZvaWQgMCkgeyBvcHRpb25zID0ge307IH1cbiAgICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6c3RyaWN0LXR5cGUtcHJlZGljYXRlc1xuICAgIGlmICh0eXBlb2YgZm4gIT09ICdmdW5jdGlvbicpIHtcbiAgICAgICAgcmV0dXJuIGZuO1xuICAgIH1cbiAgICB0cnkge1xuICAgICAgICAvLyBXZSBkb24ndCB3YW5uYSB3cmFwIGl0IHR3aWNlXG4gICAgICAgIGlmIChmbi5fX3NlbnRyeV9fKSB7XG4gICAgICAgICAgICByZXR1cm4gZm47XG4gICAgICAgIH1cbiAgICAgICAgLy8gSWYgdGhpcyBoYXMgYWxyZWFkeSBiZWVuIHdyYXBwZWQgaW4gdGhlIHBhc3QsIHJldHVybiB0aGF0IHdyYXBwZWQgZnVuY3Rpb25cbiAgICAgICAgaWYgKGZuLl9fc2VudHJ5X3dyYXBwZWRfXykge1xuICAgICAgICAgICAgcmV0dXJuIGZuLl9fc2VudHJ5X3dyYXBwZWRfXztcbiAgICAgICAgfVxuICAgIH1cbiAgICBjYXRjaCAoZSkge1xuICAgICAgICAvLyBKdXN0IGFjY2Vzc2luZyBjdXN0b20gcHJvcHMgaW4gc29tZSBTZWxlbml1bSBlbnZpcm9ubWVudHNcbiAgICAgICAgLy8gY2FuIGNhdXNlIGEgXCJQZXJtaXNzaW9uIGRlbmllZFwiIGV4Y2VwdGlvbiAoc2VlIHJhdmVuLWpzIzQ5NSkuXG4gICAgICAgIC8vIEJhaWwgb24gd3JhcHBpbmcgYW5kIHJldHVybiB0aGUgZnVuY3Rpb24gYXMtaXMgKGRlZmVycyB0byB3aW5kb3cub25lcnJvcikuXG4gICAgICAgIHJldHVybiBmbjtcbiAgICB9XG4gICAgdmFyIHNlbnRyeVdyYXBwZWQgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciBhcmdzID0gQXJyYXkucHJvdG90eXBlLnNsaWNlLmNhbGwoYXJndW1lbnRzKTtcbiAgICAgICAgLy8gdHNsaW50OmRpc2FibGU6bm8tdW5zYWZlLWFueVxuICAgICAgICB0cnkge1xuICAgICAgICAgICAgLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOnN0cmljdC10eXBlLXByZWRpY2F0ZXNcbiAgICAgICAgICAgIGlmIChiZWZvcmUgJiYgdHlwZW9mIGJlZm9yZSA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICAgICAgICAgIGJlZm9yZS5hcHBseSh0aGlzLCBhcmd1bWVudHMpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdmFyIHdyYXBwZWRBcmd1bWVudHMgPSBhcmdzLm1hcChmdW5jdGlvbiAoYXJnKSB7IHJldHVybiB3cmFwKGFyZywgb3B0aW9ucyk7IH0pO1xuICAgICAgICAgICAgaWYgKGZuLmhhbmRsZUV2ZW50KSB7XG4gICAgICAgICAgICAgICAgLy8gQXR0ZW1wdCB0byBpbnZva2UgdXNlci1sYW5kIGZ1bmN0aW9uXG4gICAgICAgICAgICAgICAgLy8gTk9URTogSWYgeW91IGFyZSBhIFNlbnRyeSB1c2VyLCBhbmQgeW91IGFyZSBzZWVpbmcgdGhpcyBzdGFjayBmcmFtZSwgaXRcbiAgICAgICAgICAgICAgICAvLyAgICAgICBtZWFucyB0aGUgc2VudHJ5LmphdmFzY3JpcHQgU0RLIGNhdWdodCBhbiBlcnJvciBpbnZva2luZyB5b3VyIGFwcGxpY2F0aW9uIGNvZGUuIFRoaXNcbiAgICAgICAgICAgICAgICAvLyAgICAgICBpcyBleHBlY3RlZCBiZWhhdmlvciBhbmQgTk9UIGluZGljYXRpdmUgb2YgYSBidWcgd2l0aCBzZW50cnkuamF2YXNjcmlwdC5cbiAgICAgICAgICAgICAgICByZXR1cm4gZm4uaGFuZGxlRXZlbnQuYXBwbHkodGhpcywgd3JhcHBlZEFyZ3VtZW50cyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAvLyBBdHRlbXB0IHRvIGludm9rZSB1c2VyLWxhbmQgZnVuY3Rpb25cbiAgICAgICAgICAgIC8vIE5PVEU6IElmIHlvdSBhcmUgYSBTZW50cnkgdXNlciwgYW5kIHlvdSBhcmUgc2VlaW5nIHRoaXMgc3RhY2sgZnJhbWUsIGl0XG4gICAgICAgICAgICAvLyAgICAgICBtZWFucyB0aGUgc2VudHJ5LmphdmFzY3JpcHQgU0RLIGNhdWdodCBhbiBlcnJvciBpbnZva2luZyB5b3VyIGFwcGxpY2F0aW9uIGNvZGUuIFRoaXNcbiAgICAgICAgICAgIC8vICAgICAgIGlzIGV4cGVjdGVkIGJlaGF2aW9yIGFuZCBOT1QgaW5kaWNhdGl2ZSBvZiBhIGJ1ZyB3aXRoIHNlbnRyeS5qYXZhc2NyaXB0LlxuICAgICAgICAgICAgcmV0dXJuIGZuLmFwcGx5KHRoaXMsIHdyYXBwZWRBcmd1bWVudHMpO1xuICAgICAgICAgICAgLy8gdHNsaW50OmVuYWJsZTpuby11bnNhZmUtYW55XG4gICAgICAgIH1cbiAgICAgICAgY2F0Y2ggKGV4KSB7XG4gICAgICAgICAgICBpZ25vcmVOZXh0T25FcnJvcigpO1xuICAgICAgICAgICAgd2l0aFNjb3BlKGZ1bmN0aW9uIChzY29wZSkge1xuICAgICAgICAgICAgICAgIHNjb3BlLmFkZEV2ZW50UHJvY2Vzc29yKGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgICAgICAgICAgICAgICB2YXIgcHJvY2Vzc2VkRXZlbnQgPSB0c2xpYl8xLl9fYXNzaWduKHt9LCBldmVudCk7XG4gICAgICAgICAgICAgICAgICAgIGlmIChvcHRpb25zLm1lY2hhbmlzbSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgYWRkRXhjZXB0aW9uVHlwZVZhbHVlKHByb2Nlc3NlZEV2ZW50LCB1bmRlZmluZWQsIHVuZGVmaW5lZCk7XG4gICAgICAgICAgICAgICAgICAgICAgICBhZGRFeGNlcHRpb25NZWNoYW5pc20ocHJvY2Vzc2VkRXZlbnQsIG9wdGlvbnMubWVjaGFuaXNtKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBwcm9jZXNzZWRFdmVudC5leHRyYSA9IHRzbGliXzEuX19hc3NpZ24oe30sIHByb2Nlc3NlZEV2ZW50LmV4dHJhLCB7IGFyZ3VtZW50czogYXJncyB9KTtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHByb2Nlc3NlZEV2ZW50O1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIGNhcHR1cmVFeGNlcHRpb24oZXgpO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB0aHJvdyBleDtcbiAgICAgICAgfVxuICAgIH07XG4gICAgLy8gQWNjZXNzaW5nIHNvbWUgb2JqZWN0cyBtYXkgdGhyb3dcbiAgICAvLyByZWY6IGh0dHBzOi8vZ2l0aHViLmNvbS9nZXRzZW50cnkvc2VudHJ5LWphdmFzY3JpcHQvaXNzdWVzLzExNjhcbiAgICB0cnkge1xuICAgICAgICBmb3IgKHZhciBwcm9wZXJ0eSBpbiBmbikge1xuICAgICAgICAgICAgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChmbiwgcHJvcGVydHkpKSB7XG4gICAgICAgICAgICAgICAgc2VudHJ5V3JhcHBlZFtwcm9wZXJ0eV0gPSBmbltwcm9wZXJ0eV07XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG4gICAgY2F0Y2ggKF9vTykgeyB9IC8vIHRzbGludDpkaXNhYmxlLWxpbmU6bm8tZW1wdHlcbiAgICBmbi5wcm90b3R5cGUgPSBmbi5wcm90b3R5cGUgfHwge307XG4gICAgc2VudHJ5V3JhcHBlZC5wcm90b3R5cGUgPSBmbi5wcm90b3R5cGU7XG4gICAgT2JqZWN0LmRlZmluZVByb3BlcnR5KGZuLCAnX19zZW50cnlfd3JhcHBlZF9fJywge1xuICAgICAgICBlbnVtZXJhYmxlOiBmYWxzZSxcbiAgICAgICAgdmFsdWU6IHNlbnRyeVdyYXBwZWQsXG4gICAgfSk7XG4gICAgLy8gU2lnbmFsIHRoYXQgdGhpcyBmdW5jdGlvbiBoYXMgYmVlbiB3cmFwcGVkL2ZpbGxlZCBhbHJlYWR5XG4gICAgLy8gZm9yIGJvdGggZGVidWdnaW5nIGFuZCB0byBwcmV2ZW50IGl0IHRvIGJlaW5nIHdyYXBwZWQvZmlsbGVkIHR3aWNlXG4gICAgT2JqZWN0LmRlZmluZVByb3BlcnRpZXMoc2VudHJ5V3JhcHBlZCwge1xuICAgICAgICBfX3NlbnRyeV9fOiB7XG4gICAgICAgICAgICBlbnVtZXJhYmxlOiBmYWxzZSxcbiAgICAgICAgICAgIHZhbHVlOiB0cnVlLFxuICAgICAgICB9LFxuICAgICAgICBfX3NlbnRyeV9vcmlnaW5hbF9fOiB7XG4gICAgICAgICAgICBlbnVtZXJhYmxlOiBmYWxzZSxcbiAgICAgICAgICAgIHZhbHVlOiBmbixcbiAgICAgICAgfSxcbiAgICB9KTtcbiAgICAvLyBSZXN0b3JlIG9yaWdpbmFsIGZ1bmN0aW9uIG5hbWUgKG5vdCBhbGwgYnJvd3NlcnMgYWxsb3cgdGhhdClcbiAgICB0cnkge1xuICAgICAgICB2YXIgZGVzY3JpcHRvciA9IE9iamVjdC5nZXRPd25Qcm9wZXJ0eURlc2NyaXB0b3Ioc2VudHJ5V3JhcHBlZCwgJ25hbWUnKTtcbiAgICAgICAgaWYgKGRlc2NyaXB0b3IuY29uZmlndXJhYmxlKSB7XG4gICAgICAgICAgICBPYmplY3QuZGVmaW5lUHJvcGVydHkoc2VudHJ5V3JhcHBlZCwgJ25hbWUnLCB7XG4gICAgICAgICAgICAgICAgZ2V0OiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBmbi5uYW1lO1xuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgIH1cbiAgICBjYXRjaCAoX29PKSB7XG4gICAgICAgIC8qbm8tZW1wdHkqL1xuICAgIH1cbiAgICByZXR1cm4gc2VudHJ5V3JhcHBlZDtcbn1cbi8vIyBzb3VyY2VNYXBwaW5nVVJMPWhlbHBlcnMuanMubWFwIiwiaW1wb3J0ICogYXMgdHNsaWJfMSBmcm9tIFwidHNsaWJcIjtcbmV4cG9ydCAqIGZyb20gJy4vZXhwb3J0cyc7XG5pbXBvcnQgeyBJbnRlZ3JhdGlvbnMgYXMgQ29yZUludGVncmF0aW9ucyB9IGZyb20gJ0BzZW50cnkvY29yZSc7XG5pbXBvcnQgeyBnZXRHbG9iYWxPYmplY3QgfSBmcm9tICdAc2VudHJ5L3V0aWxzJztcbmltcG9ydCAqIGFzIEJyb3dzZXJJbnRlZ3JhdGlvbnMgZnJvbSAnLi9pbnRlZ3JhdGlvbnMnO1xuaW1wb3J0ICogYXMgVHJhbnNwb3J0cyBmcm9tICcuL3RyYW5zcG9ydHMnO1xudmFyIHdpbmRvd0ludGVncmF0aW9ucyA9IHt9O1xuLy8gVGhpcyBibG9jayBpcyBuZWVkZWQgdG8gYWRkIGNvbXBhdGliaWxpdHkgd2l0aCB0aGUgaW50ZWdyYXRpb25zIHBhY2thZ2VzIHdoZW4gdXNlZCB3aXRoIGEgQ0ROXG4vLyB0c2xpbnQ6ZGlzYWJsZTogbm8tdW5zYWZlLWFueVxudmFyIF93aW5kb3cgPSBnZXRHbG9iYWxPYmplY3QoKTtcbmlmIChfd2luZG93LlNlbnRyeSAmJiBfd2luZG93LlNlbnRyeS5JbnRlZ3JhdGlvbnMpIHtcbiAgICB3aW5kb3dJbnRlZ3JhdGlvbnMgPSBfd2luZG93LlNlbnRyeS5JbnRlZ3JhdGlvbnM7XG59XG4vLyB0c2xpbnQ6ZW5hYmxlOiBuby11bnNhZmUtYW55XG52YXIgSU5URUdSQVRJT05TID0gdHNsaWJfMS5fX2Fzc2lnbih7fSwgd2luZG93SW50ZWdyYXRpb25zLCBDb3JlSW50ZWdyYXRpb25zLCBCcm93c2VySW50ZWdyYXRpb25zKTtcbmV4cG9ydCB7IElOVEVHUkFUSU9OUyBhcyBJbnRlZ3JhdGlvbnMsIFRyYW5zcG9ydHMgfTtcbi8vIyBzb3VyY2VNYXBwaW5nVVJMPWluZGV4LmpzLm1hcCIsImltcG9ydCAqIGFzIHRzbGliXzEgZnJvbSBcInRzbGliXCI7XG5pbXBvcnQgeyBnZXRDdXJyZW50SHViIH0gZnJvbSAnQHNlbnRyeS9jb3JlJztcbmltcG9ydCB7IFNldmVyaXR5IH0gZnJvbSAnQHNlbnRyeS90eXBlcyc7XG5pbXBvcnQgeyBhZGRJbnN0cnVtZW50YXRpb25IYW5kbGVyLCBnZXRFdmVudERlc2NyaXB0aW9uLCBnZXRHbG9iYWxPYmplY3QsIGh0bWxUcmVlQXNTdHJpbmcsIHBhcnNlVXJsLCBzYWZlSm9pbiwgfSBmcm9tICdAc2VudHJ5L3V0aWxzJztcbi8qKlxuICogRGVmYXVsdCBCcmVhZGNydW1icyBpbnN0cnVtZW50YXRpb25zXG4gKiBUT0RPOiBEZXByZWNhdGVkIC0gd2l0aCB2NiwgdGhpcyB3aWxsIGJlIHJlbmFtZWQgdG8gYEluc3RydW1lbnRgXG4gKi9cbnZhciBCcmVhZGNydW1icyA9IC8qKiBAY2xhc3MgKi8gKGZ1bmN0aW9uICgpIHtcbiAgICAvKipcbiAgICAgKiBAaW5oZXJpdERvY1xuICAgICAqL1xuICAgIGZ1bmN0aW9uIEJyZWFkY3J1bWJzKG9wdGlvbnMpIHtcbiAgICAgICAgLyoqXG4gICAgICAgICAqIEBpbmhlcml0RG9jXG4gICAgICAgICAqL1xuICAgICAgICB0aGlzLm5hbWUgPSBCcmVhZGNydW1icy5pZDtcbiAgICAgICAgdGhpcy5fb3B0aW9ucyA9IHRzbGliXzEuX19hc3NpZ24oeyBjb25zb2xlOiB0cnVlLCBkb206IHRydWUsIGZldGNoOiB0cnVlLCBoaXN0b3J5OiB0cnVlLCBzZW50cnk6IHRydWUsIHhocjogdHJ1ZSB9LCBvcHRpb25zKTtcbiAgICB9XG4gICAgLyoqXG4gICAgICogQ3JlYXRlIGEgYnJlYWRjcnVtYiBvZiBgc2VudHJ5YCBmcm9tIHRoZSBldmVudHMgdGhlbXNlbHZlc1xuICAgICAqL1xuICAgIEJyZWFkY3J1bWJzLnByb3RvdHlwZS5hZGRTZW50cnlCcmVhZGNydW1iID0gZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICAgIGlmICghdGhpcy5fb3B0aW9ucy5zZW50cnkpIHtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuICAgICAgICBnZXRDdXJyZW50SHViKCkuYWRkQnJlYWRjcnVtYih7XG4gICAgICAgICAgICBjYXRlZ29yeTogXCJzZW50cnkuXCIgKyAoZXZlbnQudHlwZSA9PT0gJ3RyYW5zYWN0aW9uJyA/ICd0cmFuc2FjdGlvbicgOiAnZXZlbnQnKSxcbiAgICAgICAgICAgIGV2ZW50X2lkOiBldmVudC5ldmVudF9pZCxcbiAgICAgICAgICAgIGxldmVsOiBldmVudC5sZXZlbCxcbiAgICAgICAgICAgIG1lc3NhZ2U6IGdldEV2ZW50RGVzY3JpcHRpb24oZXZlbnQpLFxuICAgICAgICB9LCB7XG4gICAgICAgICAgICBldmVudDogZXZlbnQsXG4gICAgICAgIH0pO1xuICAgIH07XG4gICAgLyoqXG4gICAgICogQ3JlYXRlcyBicmVhZGNydW1icyBmcm9tIGNvbnNvbGUgQVBJIGNhbGxzXG4gICAgICovXG4gICAgQnJlYWRjcnVtYnMucHJvdG90eXBlLl9jb25zb2xlQnJlYWRjcnVtYiA9IGZ1bmN0aW9uIChoYW5kbGVyRGF0YSkge1xuICAgICAgICB2YXIgYnJlYWRjcnVtYiA9IHtcbiAgICAgICAgICAgIGNhdGVnb3J5OiAnY29uc29sZScsXG4gICAgICAgICAgICBkYXRhOiB7XG4gICAgICAgICAgICAgICAgYXJndW1lbnRzOiBoYW5kbGVyRGF0YS5hcmdzLFxuICAgICAgICAgICAgICAgIGxvZ2dlcjogJ2NvbnNvbGUnLFxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGxldmVsOiBTZXZlcml0eS5mcm9tU3RyaW5nKGhhbmRsZXJEYXRhLmxldmVsKSxcbiAgICAgICAgICAgIG1lc3NhZ2U6IHNhZmVKb2luKGhhbmRsZXJEYXRhLmFyZ3MsICcgJyksXG4gICAgICAgIH07XG4gICAgICAgIGlmIChoYW5kbGVyRGF0YS5sZXZlbCA9PT0gJ2Fzc2VydCcpIHtcbiAgICAgICAgICAgIGlmIChoYW5kbGVyRGF0YS5hcmdzWzBdID09PSBmYWxzZSkge1xuICAgICAgICAgICAgICAgIGJyZWFkY3J1bWIubWVzc2FnZSA9IFwiQXNzZXJ0aW9uIGZhaWxlZDogXCIgKyAoc2FmZUpvaW4oaGFuZGxlckRhdGEuYXJncy5zbGljZSgxKSwgJyAnKSB8fCAnY29uc29sZS5hc3NlcnQnKTtcbiAgICAgICAgICAgICAgICBicmVhZGNydW1iLmRhdGEuYXJndW1lbnRzID0gaGFuZGxlckRhdGEuYXJncy5zbGljZSgxKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIC8vIERvbid0IGNhcHR1cmUgYSBicmVhZGNydW1iIGZvciBwYXNzZWQgYXNzZXJ0aW9uc1xuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICBnZXRDdXJyZW50SHViKCkuYWRkQnJlYWRjcnVtYihicmVhZGNydW1iLCB7XG4gICAgICAgICAgICBpbnB1dDogaGFuZGxlckRhdGEuYXJncyxcbiAgICAgICAgICAgIGxldmVsOiBoYW5kbGVyRGF0YS5sZXZlbCxcbiAgICAgICAgfSk7XG4gICAgfTtcbiAgICAvKipcbiAgICAgKiBDcmVhdGVzIGJyZWFkY3J1bWJzIGZyb20gRE9NIEFQSSBjYWxsc1xuICAgICAqL1xuICAgIEJyZWFkY3J1bWJzLnByb3RvdHlwZS5fZG9tQnJlYWRjcnVtYiA9IGZ1bmN0aW9uIChoYW5kbGVyRGF0YSkge1xuICAgICAgICB2YXIgdGFyZ2V0O1xuICAgICAgICAvLyBBY2Nlc3NpbmcgZXZlbnQudGFyZ2V0IGNhbiB0aHJvdyAoc2VlIGdldHNlbnRyeS9yYXZlbi1qcyM4MzgsICM3NjgpXG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICB0YXJnZXQgPSBoYW5kbGVyRGF0YS5ldmVudC50YXJnZXRcbiAgICAgICAgICAgICAgICA/IGh0bWxUcmVlQXNTdHJpbmcoaGFuZGxlckRhdGEuZXZlbnQudGFyZ2V0KVxuICAgICAgICAgICAgICAgIDogaHRtbFRyZWVBc1N0cmluZyhoYW5kbGVyRGF0YS5ldmVudCk7XG4gICAgICAgIH1cbiAgICAgICAgY2F0Y2ggKGUpIHtcbiAgICAgICAgICAgIHRhcmdldCA9ICc8dW5rbm93bj4nO1xuICAgICAgICB9XG4gICAgICAgIGlmICh0YXJnZXQubGVuZ3RoID09PSAwKSB7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICAgICAgZ2V0Q3VycmVudEh1YigpLmFkZEJyZWFkY3J1bWIoe1xuICAgICAgICAgICAgY2F0ZWdvcnk6IFwidWkuXCIgKyBoYW5kbGVyRGF0YS5uYW1lLFxuICAgICAgICAgICAgbWVzc2FnZTogdGFyZ2V0LFxuICAgICAgICB9LCB7XG4gICAgICAgICAgICBldmVudDogaGFuZGxlckRhdGEuZXZlbnQsXG4gICAgICAgICAgICBuYW1lOiBoYW5kbGVyRGF0YS5uYW1lLFxuICAgICAgICB9KTtcbiAgICB9O1xuICAgIC8qKlxuICAgICAqIENyZWF0ZXMgYnJlYWRjcnVtYnMgZnJvbSBYSFIgQVBJIGNhbGxzXG4gICAgICovXG4gICAgQnJlYWRjcnVtYnMucHJvdG90eXBlLl94aHJCcmVhZGNydW1iID0gZnVuY3Rpb24gKGhhbmRsZXJEYXRhKSB7XG4gICAgICAgIGlmIChoYW5kbGVyRGF0YS5lbmRUaW1lc3RhbXApIHtcbiAgICAgICAgICAgIC8vIFdlIG9ubHkgY2FwdHVyZSBjb21wbGV0ZSwgbm9uLXNlbnRyeSByZXF1ZXN0c1xuICAgICAgICAgICAgaWYgKGhhbmRsZXJEYXRhLnhoci5fX3NlbnRyeV9vd25fcmVxdWVzdF9fKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZ2V0Q3VycmVudEh1YigpLmFkZEJyZWFkY3J1bWIoe1xuICAgICAgICAgICAgICAgIGNhdGVnb3J5OiAneGhyJyxcbiAgICAgICAgICAgICAgICBkYXRhOiBoYW5kbGVyRGF0YS54aHIuX19zZW50cnlfeGhyX18sXG4gICAgICAgICAgICAgICAgdHlwZTogJ2h0dHAnLFxuICAgICAgICAgICAgfSwge1xuICAgICAgICAgICAgICAgIHhocjogaGFuZGxlckRhdGEueGhyLFxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICB9O1xuICAgIC8qKlxuICAgICAqIENyZWF0ZXMgYnJlYWRjcnVtYnMgZnJvbSBmZXRjaCBBUEkgY2FsbHNcbiAgICAgKi9cbiAgICBCcmVhZGNydW1icy5wcm90b3R5cGUuX2ZldGNoQnJlYWRjcnVtYiA9IGZ1bmN0aW9uIChoYW5kbGVyRGF0YSkge1xuICAgICAgICAvLyBXZSBvbmx5IGNhcHR1cmUgY29tcGxldGUgZmV0Y2ggcmVxdWVzdHNcbiAgICAgICAgaWYgKCFoYW5kbGVyRGF0YS5lbmRUaW1lc3RhbXApIHtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuICAgICAgICBpZiAoaGFuZGxlckRhdGEuZmV0Y2hEYXRhLnVybC5tYXRjaCgvc2VudHJ5X2tleS8pICYmIGhhbmRsZXJEYXRhLmZldGNoRGF0YS5tZXRob2QgPT09ICdQT1NUJykge1xuICAgICAgICAgICAgLy8gV2Ugd2lsbCBub3QgY3JlYXRlIGJyZWFkY3J1bWJzIGZvciBmZXRjaCByZXF1ZXN0cyB0aGF0IGNvbnRhaW4gYHNlbnRyeV9rZXlgIChpbnRlcm5hbCBzZW50cnkgcmVxdWVzdHMpXG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICAgICAgaWYgKGhhbmRsZXJEYXRhLmVycm9yKSB7XG4gICAgICAgICAgICBnZXRDdXJyZW50SHViKCkuYWRkQnJlYWRjcnVtYih7XG4gICAgICAgICAgICAgICAgY2F0ZWdvcnk6ICdmZXRjaCcsXG4gICAgICAgICAgICAgICAgZGF0YTogaGFuZGxlckRhdGEuZmV0Y2hEYXRhLFxuICAgICAgICAgICAgICAgIGxldmVsOiBTZXZlcml0eS5FcnJvcixcbiAgICAgICAgICAgICAgICB0eXBlOiAnaHR0cCcsXG4gICAgICAgICAgICB9LCB7XG4gICAgICAgICAgICAgICAgZGF0YTogaGFuZGxlckRhdGEuZXJyb3IsXG4gICAgICAgICAgICAgICAgaW5wdXQ6IGhhbmRsZXJEYXRhLmFyZ3MsXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIGdldEN1cnJlbnRIdWIoKS5hZGRCcmVhZGNydW1iKHtcbiAgICAgICAgICAgICAgICBjYXRlZ29yeTogJ2ZldGNoJyxcbiAgICAgICAgICAgICAgICBkYXRhOiB0c2xpYl8xLl9fYXNzaWduKHt9LCBoYW5kbGVyRGF0YS5mZXRjaERhdGEsIHsgc3RhdHVzX2NvZGU6IGhhbmRsZXJEYXRhLnJlc3BvbnNlLnN0YXR1cyB9KSxcbiAgICAgICAgICAgICAgICB0eXBlOiAnaHR0cCcsXG4gICAgICAgICAgICB9LCB7XG4gICAgICAgICAgICAgICAgaW5wdXQ6IGhhbmRsZXJEYXRhLmFyZ3MsXG4gICAgICAgICAgICAgICAgcmVzcG9uc2U6IGhhbmRsZXJEYXRhLnJlc3BvbnNlLFxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICB9O1xuICAgIC8qKlxuICAgICAqIENyZWF0ZXMgYnJlYWRjcnVtYnMgZnJvbSBoaXN0b3J5IEFQSSBjYWxsc1xuICAgICAqL1xuICAgIEJyZWFkY3J1bWJzLnByb3RvdHlwZS5faGlzdG9yeUJyZWFkY3J1bWIgPSBmdW5jdGlvbiAoaGFuZGxlckRhdGEpIHtcbiAgICAgICAgdmFyIGdsb2JhbCA9IGdldEdsb2JhbE9iamVjdCgpO1xuICAgICAgICB2YXIgZnJvbSA9IGhhbmRsZXJEYXRhLmZyb207XG4gICAgICAgIHZhciB0byA9IGhhbmRsZXJEYXRhLnRvO1xuICAgICAgICB2YXIgcGFyc2VkTG9jID0gcGFyc2VVcmwoZ2xvYmFsLmxvY2F0aW9uLmhyZWYpO1xuICAgICAgICB2YXIgcGFyc2VkRnJvbSA9IHBhcnNlVXJsKGZyb20pO1xuICAgICAgICB2YXIgcGFyc2VkVG8gPSBwYXJzZVVybCh0byk7XG4gICAgICAgIC8vIEluaXRpYWwgcHVzaFN0YXRlIGRvZXNuJ3QgcHJvdmlkZSBgZnJvbWAgaW5mb3JtYXRpb25cbiAgICAgICAgaWYgKCFwYXJzZWRGcm9tLnBhdGgpIHtcbiAgICAgICAgICAgIHBhcnNlZEZyb20gPSBwYXJzZWRMb2M7XG4gICAgICAgIH1cbiAgICAgICAgLy8gVXNlIG9ubHkgdGhlIHBhdGggY29tcG9uZW50IG9mIHRoZSBVUkwgaWYgdGhlIFVSTCBtYXRjaGVzIHRoZSBjdXJyZW50XG4gICAgICAgIC8vIGRvY3VtZW50IChhbG1vc3QgYWxsIHRoZSB0aW1lIHdoZW4gdXNpbmcgcHVzaFN0YXRlKVxuICAgICAgICBpZiAocGFyc2VkTG9jLnByb3RvY29sID09PSBwYXJzZWRUby5wcm90b2NvbCAmJiBwYXJzZWRMb2MuaG9zdCA9PT0gcGFyc2VkVG8uaG9zdCkge1xuICAgICAgICAgICAgLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOm5vLXBhcmFtZXRlci1yZWFzc2lnbm1lbnRcbiAgICAgICAgICAgIHRvID0gcGFyc2VkVG8ucmVsYXRpdmU7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHBhcnNlZExvYy5wcm90b2NvbCA9PT0gcGFyc2VkRnJvbS5wcm90b2NvbCAmJiBwYXJzZWRMb2MuaG9zdCA9PT0gcGFyc2VkRnJvbS5ob3N0KSB7XG4gICAgICAgICAgICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6bm8tcGFyYW1ldGVyLXJlYXNzaWdubWVudFxuICAgICAgICAgICAgZnJvbSA9IHBhcnNlZEZyb20ucmVsYXRpdmU7XG4gICAgICAgIH1cbiAgICAgICAgZ2V0Q3VycmVudEh1YigpLmFkZEJyZWFkY3J1bWIoe1xuICAgICAgICAgICAgY2F0ZWdvcnk6ICduYXZpZ2F0aW9uJyxcbiAgICAgICAgICAgIGRhdGE6IHtcbiAgICAgICAgICAgICAgICBmcm9tOiBmcm9tLFxuICAgICAgICAgICAgICAgIHRvOiB0byxcbiAgICAgICAgICAgIH0sXG4gICAgICAgIH0pO1xuICAgIH07XG4gICAgLyoqXG4gICAgICogSW5zdHJ1bWVudCBicm93c2VyIGJ1aWx0LWlucyB3LyBicmVhZGNydW1iIGNhcHR1cmluZ1xuICAgICAqICAtIENvbnNvbGUgQVBJXG4gICAgICogIC0gRE9NIEFQSSAoY2xpY2svdHlwaW5nKVxuICAgICAqICAtIFhNTEh0dHBSZXF1ZXN0IEFQSVxuICAgICAqICAtIEZldGNoIEFQSVxuICAgICAqICAtIEhpc3RvcnkgQVBJXG4gICAgICovXG4gICAgQnJlYWRjcnVtYnMucHJvdG90eXBlLnNldHVwT25jZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdmFyIF90aGlzID0gdGhpcztcbiAgICAgICAgaWYgKHRoaXMuX29wdGlvbnMuY29uc29sZSkge1xuICAgICAgICAgICAgYWRkSW5zdHJ1bWVudGF0aW9uSGFuZGxlcih7XG4gICAgICAgICAgICAgICAgY2FsbGJhY2s6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgdmFyIGFyZ3MgPSBbXTtcbiAgICAgICAgICAgICAgICAgICAgZm9yICh2YXIgX2kgPSAwOyBfaSA8IGFyZ3VtZW50cy5sZW5ndGg7IF9pKyspIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGFyZ3NbX2ldID0gYXJndW1lbnRzW19pXTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBfdGhpcy5fY29uc29sZUJyZWFkY3J1bWIuYXBwbHkoX3RoaXMsIHRzbGliXzEuX19zcHJlYWQoYXJncykpO1xuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgdHlwZTogJ2NvbnNvbGUnLFxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHRoaXMuX29wdGlvbnMuZG9tKSB7XG4gICAgICAgICAgICBhZGRJbnN0cnVtZW50YXRpb25IYW5kbGVyKHtcbiAgICAgICAgICAgICAgICBjYWxsYmFjazogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICB2YXIgYXJncyA9IFtdO1xuICAgICAgICAgICAgICAgICAgICBmb3IgKHZhciBfaSA9IDA7IF9pIDwgYXJndW1lbnRzLmxlbmd0aDsgX2krKykge1xuICAgICAgICAgICAgICAgICAgICAgICAgYXJnc1tfaV0gPSBhcmd1bWVudHNbX2ldO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIF90aGlzLl9kb21CcmVhZGNydW1iLmFwcGx5KF90aGlzLCB0c2xpYl8xLl9fc3ByZWFkKGFyZ3MpKTtcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHR5cGU6ICdkb20nLFxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHRoaXMuX29wdGlvbnMueGhyKSB7XG4gICAgICAgICAgICBhZGRJbnN0cnVtZW50YXRpb25IYW5kbGVyKHtcbiAgICAgICAgICAgICAgICBjYWxsYmFjazogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgICAgICB2YXIgYXJncyA9IFtdO1xuICAgICAgICAgICAgICAgICAgICBmb3IgKHZhciBfaSA9IDA7IF9pIDwgYXJndW1lbnRzLmxlbmd0aDsgX2krKykge1xuICAgICAgICAgICAgICAgICAgICAgICAgYXJnc1tfaV0gPSBhcmd1bWVudHNbX2ldO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIF90aGlzLl94aHJCcmVhZGNydW1iLmFwcGx5KF90aGlzLCB0c2xpYl8xLl9fc3ByZWFkKGFyZ3MpKTtcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIHR5cGU6ICd4aHInLFxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHRoaXMuX29wdGlvbnMuZmV0Y2gpIHtcbiAgICAgICAgICAgIGFkZEluc3RydW1lbnRhdGlvbkhhbmRsZXIoe1xuICAgICAgICAgICAgICAgIGNhbGxiYWNrOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgICAgIHZhciBhcmdzID0gW107XG4gICAgICAgICAgICAgICAgICAgIGZvciAodmFyIF9pID0gMDsgX2kgPCBhcmd1bWVudHMubGVuZ3RoOyBfaSsrKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBhcmdzW19pXSA9IGFyZ3VtZW50c1tfaV07XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgX3RoaXMuX2ZldGNoQnJlYWRjcnVtYi5hcHBseShfdGhpcywgdHNsaWJfMS5fX3NwcmVhZChhcmdzKSk7XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICB0eXBlOiAnZmV0Y2gnLFxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHRoaXMuX29wdGlvbnMuaGlzdG9yeSkge1xuICAgICAgICAgICAgYWRkSW5zdHJ1bWVudGF0aW9uSGFuZGxlcih7XG4gICAgICAgICAgICAgICAgY2FsbGJhY2s6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgdmFyIGFyZ3MgPSBbXTtcbiAgICAgICAgICAgICAgICAgICAgZm9yICh2YXIgX2kgPSAwOyBfaSA8IGFyZ3VtZW50cy5sZW5ndGg7IF9pKyspIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGFyZ3NbX2ldID0gYXJndW1lbnRzW19pXTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBfdGhpcy5faGlzdG9yeUJyZWFkY3J1bWIuYXBwbHkoX3RoaXMsIHRzbGliXzEuX19zcHJlYWQoYXJncykpO1xuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgdHlwZTogJ2hpc3RvcnknLFxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICB9O1xuICAgIC8qKlxuICAgICAqIEBpbmhlcml0RG9jXG4gICAgICovXG4gICAgQnJlYWRjcnVtYnMuaWQgPSAnQnJlYWRjcnVtYnMnO1xuICAgIHJldHVybiBCcmVhZGNydW1icztcbn0oKSk7XG5leHBvcnQgeyBCcmVhZGNydW1icyB9O1xuLy8jIHNvdXJjZU1hcHBpbmdVUkw9YnJlYWRjcnVtYnMuanMubWFwIiwiaW1wb3J0ICogYXMgdHNsaWJfMSBmcm9tIFwidHNsaWJcIjtcbmltcG9ydCB7IGdldEN1cnJlbnRIdWIgfSBmcm9tICdAc2VudHJ5L2NvcmUnO1xuaW1wb3J0IHsgU2V2ZXJpdHkgfSBmcm9tICdAc2VudHJ5L3R5cGVzJztcbmltcG9ydCB7IGFkZEV4Y2VwdGlvbk1lY2hhbmlzbSwgYWRkSW5zdHJ1bWVudGF0aW9uSGFuZGxlciwgZ2V0TG9jYXRpb25IcmVmLCBpc0Vycm9yRXZlbnQsIGlzUHJpbWl0aXZlLCBpc1N0cmluZywgbG9nZ2VyLCB9IGZyb20gJ0BzZW50cnkvdXRpbHMnO1xuaW1wb3J0IHsgZXZlbnRGcm9tVW5rbm93bklucHV0IH0gZnJvbSAnLi4vZXZlbnRidWlsZGVyJztcbmltcG9ydCB7IHNob3VsZElnbm9yZU9uRXJyb3IgfSBmcm9tICcuLi9oZWxwZXJzJztcbi8qKiBHbG9iYWwgaGFuZGxlcnMgKi9cbnZhciBHbG9iYWxIYW5kbGVycyA9IC8qKiBAY2xhc3MgKi8gKGZ1bmN0aW9uICgpIHtcbiAgICAvKiogSlNEb2MgKi9cbiAgICBmdW5jdGlvbiBHbG9iYWxIYW5kbGVycyhvcHRpb25zKSB7XG4gICAgICAgIC8qKlxuICAgICAgICAgKiBAaW5oZXJpdERvY1xuICAgICAgICAgKi9cbiAgICAgICAgdGhpcy5uYW1lID0gR2xvYmFsSGFuZGxlcnMuaWQ7XG4gICAgICAgIC8qKiBKU0RvYyAqL1xuICAgICAgICB0aGlzLl9vbkVycm9ySGFuZGxlckluc3RhbGxlZCA9IGZhbHNlO1xuICAgICAgICAvKiogSlNEb2MgKi9cbiAgICAgICAgdGhpcy5fb25VbmhhbmRsZWRSZWplY3Rpb25IYW5kbGVySW5zdGFsbGVkID0gZmFsc2U7XG4gICAgICAgIHRoaXMuX29wdGlvbnMgPSB0c2xpYl8xLl9fYXNzaWduKHsgb25lcnJvcjogdHJ1ZSwgb251bmhhbmRsZWRyZWplY3Rpb246IHRydWUgfSwgb3B0aW9ucyk7XG4gICAgfVxuICAgIC8qKlxuICAgICAqIEBpbmhlcml0RG9jXG4gICAgICovXG4gICAgR2xvYmFsSGFuZGxlcnMucHJvdG90eXBlLnNldHVwT25jZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgRXJyb3Iuc3RhY2tUcmFjZUxpbWl0ID0gNTA7XG4gICAgICAgIGlmICh0aGlzLl9vcHRpb25zLm9uZXJyb3IpIHtcbiAgICAgICAgICAgIGxvZ2dlci5sb2coJ0dsb2JhbCBIYW5kbGVyIGF0dGFjaGVkOiBvbmVycm9yJyk7XG4gICAgICAgICAgICB0aGlzLl9pbnN0YWxsR2xvYmFsT25FcnJvckhhbmRsZXIoKTtcbiAgICAgICAgfVxuICAgICAgICBpZiAodGhpcy5fb3B0aW9ucy5vbnVuaGFuZGxlZHJlamVjdGlvbikge1xuICAgICAgICAgICAgbG9nZ2VyLmxvZygnR2xvYmFsIEhhbmRsZXIgYXR0YWNoZWQ6IG9udW5oYW5kbGVkcmVqZWN0aW9uJyk7XG4gICAgICAgICAgICB0aGlzLl9pbnN0YWxsR2xvYmFsT25VbmhhbmRsZWRSZWplY3Rpb25IYW5kbGVyKCk7XG4gICAgICAgIH1cbiAgICB9O1xuICAgIC8qKiBKU0RvYyAqL1xuICAgIEdsb2JhbEhhbmRsZXJzLnByb3RvdHlwZS5faW5zdGFsbEdsb2JhbE9uRXJyb3JIYW5kbGVyID0gZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXIgX3RoaXMgPSB0aGlzO1xuICAgICAgICBpZiAodGhpcy5fb25FcnJvckhhbmRsZXJJbnN0YWxsZWQpIHtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuICAgICAgICBhZGRJbnN0cnVtZW50YXRpb25IYW5kbGVyKHtcbiAgICAgICAgICAgIGNhbGxiYWNrOiBmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICAgICAgICAgIHZhciBlcnJvciA9IGRhdGEuZXJyb3I7XG4gICAgICAgICAgICAgICAgdmFyIGN1cnJlbnRIdWIgPSBnZXRDdXJyZW50SHViKCk7XG4gICAgICAgICAgICAgICAgdmFyIGhhc0ludGVncmF0aW9uID0gY3VycmVudEh1Yi5nZXRJbnRlZ3JhdGlvbihHbG9iYWxIYW5kbGVycyk7XG4gICAgICAgICAgICAgICAgdmFyIGlzRmFpbGVkT3duRGVsaXZlcnkgPSBlcnJvciAmJiBlcnJvci5fX3NlbnRyeV9vd25fcmVxdWVzdF9fID09PSB0cnVlO1xuICAgICAgICAgICAgICAgIGlmICghaGFzSW50ZWdyYXRpb24gfHwgc2hvdWxkSWdub3JlT25FcnJvcigpIHx8IGlzRmFpbGVkT3duRGVsaXZlcnkpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB2YXIgY2xpZW50ID0gY3VycmVudEh1Yi5nZXRDbGllbnQoKTtcbiAgICAgICAgICAgICAgICB2YXIgZXZlbnQgPSBpc1ByaW1pdGl2ZShlcnJvcilcbiAgICAgICAgICAgICAgICAgICAgPyBfdGhpcy5fZXZlbnRGcm9tSW5jb21wbGV0ZU9uRXJyb3IoZGF0YS5tc2csIGRhdGEudXJsLCBkYXRhLmxpbmUsIGRhdGEuY29sdW1uKVxuICAgICAgICAgICAgICAgICAgICA6IF90aGlzLl9lbmhhbmNlRXZlbnRXaXRoSW5pdGlhbEZyYW1lKGV2ZW50RnJvbVVua25vd25JbnB1dChlcnJvciwgdW5kZWZpbmVkLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICBhdHRhY2hTdGFja3RyYWNlOiBjbGllbnQgJiYgY2xpZW50LmdldE9wdGlvbnMoKS5hdHRhY2hTdGFja3RyYWNlLFxuICAgICAgICAgICAgICAgICAgICAgICAgcmVqZWN0aW9uOiBmYWxzZSxcbiAgICAgICAgICAgICAgICAgICAgfSksIGRhdGEudXJsLCBkYXRhLmxpbmUsIGRhdGEuY29sdW1uKTtcbiAgICAgICAgICAgICAgICBhZGRFeGNlcHRpb25NZWNoYW5pc20oZXZlbnQsIHtcbiAgICAgICAgICAgICAgICAgICAgaGFuZGxlZDogZmFsc2UsXG4gICAgICAgICAgICAgICAgICAgIHR5cGU6ICdvbmVycm9yJyxcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICBjdXJyZW50SHViLmNhcHR1cmVFdmVudChldmVudCwge1xuICAgICAgICAgICAgICAgICAgICBvcmlnaW5hbEV4Y2VwdGlvbjogZXJyb3IsXG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgdHlwZTogJ2Vycm9yJyxcbiAgICAgICAgfSk7XG4gICAgICAgIHRoaXMuX29uRXJyb3JIYW5kbGVySW5zdGFsbGVkID0gdHJ1ZTtcbiAgICB9O1xuICAgIC8qKiBKU0RvYyAqL1xuICAgIEdsb2JhbEhhbmRsZXJzLnByb3RvdHlwZS5faW5zdGFsbEdsb2JhbE9uVW5oYW5kbGVkUmVqZWN0aW9uSGFuZGxlciA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdmFyIF90aGlzID0gdGhpcztcbiAgICAgICAgaWYgKHRoaXMuX29uVW5oYW5kbGVkUmVqZWN0aW9uSGFuZGxlckluc3RhbGxlZCkge1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIGFkZEluc3RydW1lbnRhdGlvbkhhbmRsZXIoe1xuICAgICAgICAgICAgY2FsbGJhY2s6IGZ1bmN0aW9uIChlKSB7XG4gICAgICAgICAgICAgICAgdmFyIGVycm9yID0gZTtcbiAgICAgICAgICAgICAgICAvLyBkaWcgdGhlIG9iamVjdCBvZiB0aGUgcmVqZWN0aW9uIG91dCBvZiBrbm93biBldmVudCB0eXBlc1xuICAgICAgICAgICAgICAgIHRyeSB7XG4gICAgICAgICAgICAgICAgICAgIC8vIFByb21pc2VSZWplY3Rpb25FdmVudHMgc3RvcmUgdGhlIG9iamVjdCBvZiB0aGUgcmVqZWN0aW9uIHVuZGVyICdyZWFzb24nXG4gICAgICAgICAgICAgICAgICAgIC8vIHNlZSBodHRwczovL2RldmVsb3Blci5tb3ppbGxhLm9yZy9lbi1VUy9kb2NzL1dlYi9BUEkvUHJvbWlzZVJlamVjdGlvbkV2ZW50XG4gICAgICAgICAgICAgICAgICAgIGlmICgncmVhc29uJyBpbiBlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBlcnJvciA9IGUucmVhc29uO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIC8vIHNvbWV0aGluZywgc29tZXdoZXJlLCAobGlrZWx5IGEgYnJvd3NlciBleHRlbnNpb24pIGVmZmVjdGl2ZWx5IGNhc3RzIFByb21pc2VSZWplY3Rpb25FdmVudHNcbiAgICAgICAgICAgICAgICAgICAgLy8gdG8gQ3VzdG9tRXZlbnRzLCBtb3ZpbmcgdGhlIGBwcm9taXNlYCBhbmQgYHJlYXNvbmAgYXR0cmlidXRlcyBvZiB0aGUgUFJFIGludG9cbiAgICAgICAgICAgICAgICAgICAgLy8gdGhlIEN1c3RvbUV2ZW50J3MgYGRldGFpbGAgYXR0cmlidXRlLCBzaW5jZSB0aGV5J3JlIG5vdCBwYXJ0IG9mIEN1c3RvbUV2ZW50J3Mgc3BlY1xuICAgICAgICAgICAgICAgICAgICAvLyBzZWUgaHR0cHM6Ly9kZXZlbG9wZXIubW96aWxsYS5vcmcvZW4tVVMvZG9jcy9XZWIvQVBJL0N1c3RvbUV2ZW50IGFuZFxuICAgICAgICAgICAgICAgICAgICAvLyBodHRwczovL2dpdGh1Yi5jb20vZ2V0c2VudHJ5L3NlbnRyeS1qYXZhc2NyaXB0L2lzc3Vlcy8yMzgwXG4gICAgICAgICAgICAgICAgICAgIGVsc2UgaWYgKCdkZXRhaWwnIGluIGUgJiYgJ3JlYXNvbicgaW4gZS5kZXRhaWwpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGVycm9yID0gZS5kZXRhaWwucmVhc29uO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGNhdGNoIChfb08pIHtcbiAgICAgICAgICAgICAgICAgICAgLy8gbm8tZW1wdHlcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgdmFyIGN1cnJlbnRIdWIgPSBnZXRDdXJyZW50SHViKCk7XG4gICAgICAgICAgICAgICAgdmFyIGhhc0ludGVncmF0aW9uID0gY3VycmVudEh1Yi5nZXRJbnRlZ3JhdGlvbihHbG9iYWxIYW5kbGVycyk7XG4gICAgICAgICAgICAgICAgdmFyIGlzRmFpbGVkT3duRGVsaXZlcnkgPSBlcnJvciAmJiBlcnJvci5fX3NlbnRyeV9vd25fcmVxdWVzdF9fID09PSB0cnVlO1xuICAgICAgICAgICAgICAgIGlmICghaGFzSW50ZWdyYXRpb24gfHwgc2hvdWxkSWdub3JlT25FcnJvcigpIHx8IGlzRmFpbGVkT3duRGVsaXZlcnkpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIHZhciBjbGllbnQgPSBjdXJyZW50SHViLmdldENsaWVudCgpO1xuICAgICAgICAgICAgICAgIHZhciBldmVudCA9IGlzUHJpbWl0aXZlKGVycm9yKVxuICAgICAgICAgICAgICAgICAgICA/IF90aGlzLl9ldmVudEZyb21JbmNvbXBsZXRlUmVqZWN0aW9uKGVycm9yKVxuICAgICAgICAgICAgICAgICAgICA6IGV2ZW50RnJvbVVua25vd25JbnB1dChlcnJvciwgdW5kZWZpbmVkLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICBhdHRhY2hTdGFja3RyYWNlOiBjbGllbnQgJiYgY2xpZW50LmdldE9wdGlvbnMoKS5hdHRhY2hTdGFja3RyYWNlLFxuICAgICAgICAgICAgICAgICAgICAgICAgcmVqZWN0aW9uOiB0cnVlLFxuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICBldmVudC5sZXZlbCA9IFNldmVyaXR5LkVycm9yO1xuICAgICAgICAgICAgICAgIGFkZEV4Y2VwdGlvbk1lY2hhbmlzbShldmVudCwge1xuICAgICAgICAgICAgICAgICAgICBoYW5kbGVkOiBmYWxzZSxcbiAgICAgICAgICAgICAgICAgICAgdHlwZTogJ29udW5oYW5kbGVkcmVqZWN0aW9uJyxcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICBjdXJyZW50SHViLmNhcHR1cmVFdmVudChldmVudCwge1xuICAgICAgICAgICAgICAgICAgICBvcmlnaW5hbEV4Y2VwdGlvbjogZXJyb3IsXG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHR5cGU6ICd1bmhhbmRsZWRyZWplY3Rpb24nLFxuICAgICAgICB9KTtcbiAgICAgICAgdGhpcy5fb25VbmhhbmRsZWRSZWplY3Rpb25IYW5kbGVySW5zdGFsbGVkID0gdHJ1ZTtcbiAgICB9O1xuICAgIC8qKlxuICAgICAqIFRoaXMgZnVuY3Rpb24gY3JlYXRlcyBhIHN0YWNrIGZyb20gYW4gb2xkLCBlcnJvci1sZXNzIG9uZXJyb3IgaGFuZGxlci5cbiAgICAgKi9cbiAgICBHbG9iYWxIYW5kbGVycy5wcm90b3R5cGUuX2V2ZW50RnJvbUluY29tcGxldGVPbkVycm9yID0gZnVuY3Rpb24gKG1zZywgdXJsLCBsaW5lLCBjb2x1bW4pIHtcbiAgICAgICAgdmFyIEVSUk9SX1RZUEVTX1JFID0gL14oPzpbVXVdbmNhdWdodCAoPzpleGNlcHRpb246ICk/KT8oPzooKD86RXZhbHxJbnRlcm5hbHxSYW5nZXxSZWZlcmVuY2V8U3ludGF4fFR5cGV8VVJJfClFcnJvcik6ICk/KC4qKSQvaTtcbiAgICAgICAgLy8gSWYgJ21lc3NhZ2UnIGlzIEVycm9yRXZlbnQsIGdldCByZWFsIG1lc3NhZ2UgZnJvbSBpbnNpZGVcbiAgICAgICAgdmFyIG1lc3NhZ2UgPSBpc0Vycm9yRXZlbnQobXNnKSA/IG1zZy5tZXNzYWdlIDogbXNnO1xuICAgICAgICB2YXIgbmFtZTtcbiAgICAgICAgaWYgKGlzU3RyaW5nKG1lc3NhZ2UpKSB7XG4gICAgICAgICAgICB2YXIgZ3JvdXBzID0gbWVzc2FnZS5tYXRjaChFUlJPUl9UWVBFU19SRSk7XG4gICAgICAgICAgICBpZiAoZ3JvdXBzKSB7XG4gICAgICAgICAgICAgICAgbmFtZSA9IGdyb3Vwc1sxXTtcbiAgICAgICAgICAgICAgICBtZXNzYWdlID0gZ3JvdXBzWzJdO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHZhciBldmVudCA9IHtcbiAgICAgICAgICAgIGV4Y2VwdGlvbjoge1xuICAgICAgICAgICAgICAgIHZhbHVlczogW1xuICAgICAgICAgICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiBuYW1lIHx8ICdFcnJvcicsXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogbWVzc2FnZSxcbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBdLFxuICAgICAgICAgICAgfSxcbiAgICAgICAgfTtcbiAgICAgICAgcmV0dXJuIHRoaXMuX2VuaGFuY2VFdmVudFdpdGhJbml0aWFsRnJhbWUoZXZlbnQsIHVybCwgbGluZSwgY29sdW1uKTtcbiAgICB9O1xuICAgIC8qKlxuICAgICAqIFRoaXMgZnVuY3Rpb24gY3JlYXRlcyBhbiBFdmVudCBmcm9tIGFuIFRyYWNlS2l0U3RhY2tUcmFjZSB0aGF0IGhhcyBwYXJ0IG9mIGl0IG1pc3NpbmcuXG4gICAgICovXG4gICAgR2xvYmFsSGFuZGxlcnMucHJvdG90eXBlLl9ldmVudEZyb21JbmNvbXBsZXRlUmVqZWN0aW9uID0gZnVuY3Rpb24gKGVycm9yKSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICBleGNlcHRpb246IHtcbiAgICAgICAgICAgICAgICB2YWx1ZXM6IFtcbiAgICAgICAgICAgICAgICAgICAge1xuICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogJ1VuaGFuZGxlZFJlamVjdGlvbicsXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogXCJOb24tRXJyb3IgcHJvbWlzZSByZWplY3Rpb24gY2FwdHVyZWQgd2l0aCB2YWx1ZTogXCIgKyBlcnJvcixcbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBdLFxuICAgICAgICAgICAgfSxcbiAgICAgICAgfTtcbiAgICB9O1xuICAgIC8qKiBKU0RvYyAqL1xuICAgIEdsb2JhbEhhbmRsZXJzLnByb3RvdHlwZS5fZW5oYW5jZUV2ZW50V2l0aEluaXRpYWxGcmFtZSA9IGZ1bmN0aW9uIChldmVudCwgdXJsLCBsaW5lLCBjb2x1bW4pIHtcbiAgICAgICAgZXZlbnQuZXhjZXB0aW9uID0gZXZlbnQuZXhjZXB0aW9uIHx8IHt9O1xuICAgICAgICBldmVudC5leGNlcHRpb24udmFsdWVzID0gZXZlbnQuZXhjZXB0aW9uLnZhbHVlcyB8fCBbXTtcbiAgICAgICAgZXZlbnQuZXhjZXB0aW9uLnZhbHVlc1swXSA9IGV2ZW50LmV4Y2VwdGlvbi52YWx1ZXNbMF0gfHwge307XG4gICAgICAgIGV2ZW50LmV4Y2VwdGlvbi52YWx1ZXNbMF0uc3RhY2t0cmFjZSA9IGV2ZW50LmV4Y2VwdGlvbi52YWx1ZXNbMF0uc3RhY2t0cmFjZSB8fCB7fTtcbiAgICAgICAgZXZlbnQuZXhjZXB0aW9uLnZhbHVlc1swXS5zdGFja3RyYWNlLmZyYW1lcyA9IGV2ZW50LmV4Y2VwdGlvbi52YWx1ZXNbMF0uc3RhY2t0cmFjZS5mcmFtZXMgfHwgW107XG4gICAgICAgIHZhciBjb2xubyA9IGlzTmFOKHBhcnNlSW50KGNvbHVtbiwgMTApKSA/IHVuZGVmaW5lZCA6IGNvbHVtbjtcbiAgICAgICAgdmFyIGxpbmVubyA9IGlzTmFOKHBhcnNlSW50KGxpbmUsIDEwKSkgPyB1bmRlZmluZWQgOiBsaW5lO1xuICAgICAgICB2YXIgZmlsZW5hbWUgPSBpc1N0cmluZyh1cmwpICYmIHVybC5sZW5ndGggPiAwID8gdXJsIDogZ2V0TG9jYXRpb25IcmVmKCk7XG4gICAgICAgIGlmIChldmVudC5leGNlcHRpb24udmFsdWVzWzBdLnN0YWNrdHJhY2UuZnJhbWVzLmxlbmd0aCA9PT0gMCkge1xuICAgICAgICAgICAgZXZlbnQuZXhjZXB0aW9uLnZhbHVlc1swXS5zdGFja3RyYWNlLmZyYW1lcy5wdXNoKHtcbiAgICAgICAgICAgICAgICBjb2xubzogY29sbm8sXG4gICAgICAgICAgICAgICAgZmlsZW5hbWU6IGZpbGVuYW1lLFxuICAgICAgICAgICAgICAgIGZ1bmN0aW9uOiAnPycsXG4gICAgICAgICAgICAgICAgaW5fYXBwOiB0cnVlLFxuICAgICAgICAgICAgICAgIGxpbmVubzogbGluZW5vLFxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGV2ZW50O1xuICAgIH07XG4gICAgLyoqXG4gICAgICogQGluaGVyaXREb2NcbiAgICAgKi9cbiAgICBHbG9iYWxIYW5kbGVycy5pZCA9ICdHbG9iYWxIYW5kbGVycyc7XG4gICAgcmV0dXJuIEdsb2JhbEhhbmRsZXJzO1xufSgpKTtcbmV4cG9ydCB7IEdsb2JhbEhhbmRsZXJzIH07XG4vLyMgc291cmNlTWFwcGluZ1VSTD1nbG9iYWxoYW5kbGVycy5qcy5tYXAiLCJleHBvcnQgeyBHbG9iYWxIYW5kbGVycyB9IGZyb20gJy4vZ2xvYmFsaGFuZGxlcnMnO1xuZXhwb3J0IHsgVHJ5Q2F0Y2ggfSBmcm9tICcuL3RyeWNhdGNoJztcbmV4cG9ydCB7IEJyZWFkY3J1bWJzIH0gZnJvbSAnLi9icmVhZGNydW1icyc7XG5leHBvcnQgeyBMaW5rZWRFcnJvcnMgfSBmcm9tICcuL2xpbmtlZGVycm9ycyc7XG5leHBvcnQgeyBVc2VyQWdlbnQgfSBmcm9tICcuL3VzZXJhZ2VudCc7XG4vLyMgc291cmNlTWFwcGluZ1VSTD1pbmRleC5qcy5tYXAiLCJpbXBvcnQgKiBhcyB0c2xpYl8xIGZyb20gXCJ0c2xpYlwiO1xuaW1wb3J0IHsgYWRkR2xvYmFsRXZlbnRQcm9jZXNzb3IsIGdldEN1cnJlbnRIdWIgfSBmcm9tICdAc2VudHJ5L2NvcmUnO1xuaW1wb3J0IHsgaXNJbnN0YW5jZU9mIH0gZnJvbSAnQHNlbnRyeS91dGlscyc7XG5pbXBvcnQgeyBleGNlcHRpb25Gcm9tU3RhY2t0cmFjZSB9IGZyb20gJy4uL3BhcnNlcnMnO1xuaW1wb3J0IHsgY29tcHV0ZVN0YWNrVHJhY2UgfSBmcm9tICcuLi90cmFjZWtpdCc7XG52YXIgREVGQVVMVF9LRVkgPSAnY2F1c2UnO1xudmFyIERFRkFVTFRfTElNSVQgPSA1O1xuLyoqIEFkZHMgU0RLIGluZm8gdG8gYW4gZXZlbnQuICovXG52YXIgTGlua2VkRXJyb3JzID0gLyoqIEBjbGFzcyAqLyAoZnVuY3Rpb24gKCkge1xuICAgIC8qKlxuICAgICAqIEBpbmhlcml0RG9jXG4gICAgICovXG4gICAgZnVuY3Rpb24gTGlua2VkRXJyb3JzKG9wdGlvbnMpIHtcbiAgICAgICAgaWYgKG9wdGlvbnMgPT09IHZvaWQgMCkgeyBvcHRpb25zID0ge307IH1cbiAgICAgICAgLyoqXG4gICAgICAgICAqIEBpbmhlcml0RG9jXG4gICAgICAgICAqL1xuICAgICAgICB0aGlzLm5hbWUgPSBMaW5rZWRFcnJvcnMuaWQ7XG4gICAgICAgIHRoaXMuX2tleSA9IG9wdGlvbnMua2V5IHx8IERFRkFVTFRfS0VZO1xuICAgICAgICB0aGlzLl9saW1pdCA9IG9wdGlvbnMubGltaXQgfHwgREVGQVVMVF9MSU1JVDtcbiAgICB9XG4gICAgLyoqXG4gICAgICogQGluaGVyaXREb2NcbiAgICAgKi9cbiAgICBMaW5rZWRFcnJvcnMucHJvdG90eXBlLnNldHVwT25jZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgYWRkR2xvYmFsRXZlbnRQcm9jZXNzb3IoZnVuY3Rpb24gKGV2ZW50LCBoaW50KSB7XG4gICAgICAgICAgICB2YXIgc2VsZiA9IGdldEN1cnJlbnRIdWIoKS5nZXRJbnRlZ3JhdGlvbihMaW5rZWRFcnJvcnMpO1xuICAgICAgICAgICAgaWYgKHNlbGYpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gc2VsZi5faGFuZGxlcihldmVudCwgaGludCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gZXZlbnQ7XG4gICAgICAgIH0pO1xuICAgIH07XG4gICAgLyoqXG4gICAgICogQGluaGVyaXREb2NcbiAgICAgKi9cbiAgICBMaW5rZWRFcnJvcnMucHJvdG90eXBlLl9oYW5kbGVyID0gZnVuY3Rpb24gKGV2ZW50LCBoaW50KSB7XG4gICAgICAgIGlmICghZXZlbnQuZXhjZXB0aW9uIHx8ICFldmVudC5leGNlcHRpb24udmFsdWVzIHx8ICFoaW50IHx8ICFpc0luc3RhbmNlT2YoaGludC5vcmlnaW5hbEV4Y2VwdGlvbiwgRXJyb3IpKSB7XG4gICAgICAgICAgICByZXR1cm4gZXZlbnQ7XG4gICAgICAgIH1cbiAgICAgICAgdmFyIGxpbmtlZEVycm9ycyA9IHRoaXMuX3dhbGtFcnJvclRyZWUoaGludC5vcmlnaW5hbEV4Y2VwdGlvbiwgdGhpcy5fa2V5KTtcbiAgICAgICAgZXZlbnQuZXhjZXB0aW9uLnZhbHVlcyA9IHRzbGliXzEuX19zcHJlYWQobGlua2VkRXJyb3JzLCBldmVudC5leGNlcHRpb24udmFsdWVzKTtcbiAgICAgICAgcmV0dXJuIGV2ZW50O1xuICAgIH07XG4gICAgLyoqXG4gICAgICogQGluaGVyaXREb2NcbiAgICAgKi9cbiAgICBMaW5rZWRFcnJvcnMucHJvdG90eXBlLl93YWxrRXJyb3JUcmVlID0gZnVuY3Rpb24gKGVycm9yLCBrZXksIHN0YWNrKSB7XG4gICAgICAgIGlmIChzdGFjayA9PT0gdm9pZCAwKSB7IHN0YWNrID0gW107IH1cbiAgICAgICAgaWYgKCFpc0luc3RhbmNlT2YoZXJyb3Jba2V5XSwgRXJyb3IpIHx8IHN0YWNrLmxlbmd0aCArIDEgPj0gdGhpcy5fbGltaXQpIHtcbiAgICAgICAgICAgIHJldHVybiBzdGFjaztcbiAgICAgICAgfVxuICAgICAgICB2YXIgc3RhY2t0cmFjZSA9IGNvbXB1dGVTdGFja1RyYWNlKGVycm9yW2tleV0pO1xuICAgICAgICB2YXIgZXhjZXB0aW9uID0gZXhjZXB0aW9uRnJvbVN0YWNrdHJhY2Uoc3RhY2t0cmFjZSk7XG4gICAgICAgIHJldHVybiB0aGlzLl93YWxrRXJyb3JUcmVlKGVycm9yW2tleV0sIGtleSwgdHNsaWJfMS5fX3NwcmVhZChbZXhjZXB0aW9uXSwgc3RhY2spKTtcbiAgICB9O1xuICAgIC8qKlxuICAgICAqIEBpbmhlcml0RG9jXG4gICAgICovXG4gICAgTGlua2VkRXJyb3JzLmlkID0gJ0xpbmtlZEVycm9ycyc7XG4gICAgcmV0dXJuIExpbmtlZEVycm9ycztcbn0oKSk7XG5leHBvcnQgeyBMaW5rZWRFcnJvcnMgfTtcbi8vIyBzb3VyY2VNYXBwaW5nVVJMPWxpbmtlZGVycm9ycy5qcy5tYXAiLCJpbXBvcnQgKiBhcyB0c2xpYl8xIGZyb20gXCJ0c2xpYlwiO1xuaW1wb3J0IHsgZmlsbCwgZ2V0RnVuY3Rpb25OYW1lLCBnZXRHbG9iYWxPYmplY3QgfSBmcm9tICdAc2VudHJ5L3V0aWxzJztcbmltcG9ydCB7IHdyYXAgfSBmcm9tICcuLi9oZWxwZXJzJztcbnZhciBERUZBVUxUX0VWRU5UX1RBUkdFVCA9IFtcbiAgICAnRXZlbnRUYXJnZXQnLFxuICAgICdXaW5kb3cnLFxuICAgICdOb2RlJyxcbiAgICAnQXBwbGljYXRpb25DYWNoZScsXG4gICAgJ0F1ZGlvVHJhY2tMaXN0JyxcbiAgICAnQ2hhbm5lbE1lcmdlck5vZGUnLFxuICAgICdDcnlwdG9PcGVyYXRpb24nLFxuICAgICdFdmVudFNvdXJjZScsXG4gICAgJ0ZpbGVSZWFkZXInLFxuICAgICdIVE1MVW5rbm93bkVsZW1lbnQnLFxuICAgICdJREJEYXRhYmFzZScsXG4gICAgJ0lEQlJlcXVlc3QnLFxuICAgICdJREJUcmFuc2FjdGlvbicsXG4gICAgJ0tleU9wZXJhdGlvbicsXG4gICAgJ01lZGlhQ29udHJvbGxlcicsXG4gICAgJ01lc3NhZ2VQb3J0JyxcbiAgICAnTW9kYWxXaW5kb3cnLFxuICAgICdOb3RpZmljYXRpb24nLFxuICAgICdTVkdFbGVtZW50SW5zdGFuY2UnLFxuICAgICdTY3JlZW4nLFxuICAgICdUZXh0VHJhY2snLFxuICAgICdUZXh0VHJhY2tDdWUnLFxuICAgICdUZXh0VHJhY2tMaXN0JyxcbiAgICAnV2ViU29ja2V0JyxcbiAgICAnV2ViU29ja2V0V29ya2VyJyxcbiAgICAnV29ya2VyJyxcbiAgICAnWE1MSHR0cFJlcXVlc3QnLFxuICAgICdYTUxIdHRwUmVxdWVzdEV2ZW50VGFyZ2V0JyxcbiAgICAnWE1MSHR0cFJlcXVlc3RVcGxvYWQnLFxuXTtcbi8qKiBXcmFwIHRpbWVyIGZ1bmN0aW9ucyBhbmQgZXZlbnQgdGFyZ2V0cyB0byBjYXRjaCBlcnJvcnMgYW5kIHByb3ZpZGUgYmV0dGVyIG1ldGEgZGF0YSAqL1xudmFyIFRyeUNhdGNoID0gLyoqIEBjbGFzcyAqLyAoZnVuY3Rpb24gKCkge1xuICAgIC8qKlxuICAgICAqIEBpbmhlcml0RG9jXG4gICAgICovXG4gICAgZnVuY3Rpb24gVHJ5Q2F0Y2gob3B0aW9ucykge1xuICAgICAgICAvKipcbiAgICAgICAgICogQGluaGVyaXREb2NcbiAgICAgICAgICovXG4gICAgICAgIHRoaXMubmFtZSA9IFRyeUNhdGNoLmlkO1xuICAgICAgICB0aGlzLl9vcHRpb25zID0gdHNsaWJfMS5fX2Fzc2lnbih7IFhNTEh0dHBSZXF1ZXN0OiB0cnVlLCBldmVudFRhcmdldDogdHJ1ZSwgcmVxdWVzdEFuaW1hdGlvbkZyYW1lOiB0cnVlLCBzZXRJbnRlcnZhbDogdHJ1ZSwgc2V0VGltZW91dDogdHJ1ZSB9LCBvcHRpb25zKTtcbiAgICB9XG4gICAgLyoqIEpTRG9jICovXG4gICAgVHJ5Q2F0Y2gucHJvdG90eXBlLl93cmFwVGltZUZ1bmN0aW9uID0gZnVuY3Rpb24gKG9yaWdpbmFsKSB7XG4gICAgICAgIHJldHVybiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICB2YXIgYXJncyA9IFtdO1xuICAgICAgICAgICAgZm9yICh2YXIgX2kgPSAwOyBfaSA8IGFyZ3VtZW50cy5sZW5ndGg7IF9pKyspIHtcbiAgICAgICAgICAgICAgICBhcmdzW19pXSA9IGFyZ3VtZW50c1tfaV07XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICB2YXIgb3JpZ2luYWxDYWxsYmFjayA9IGFyZ3NbMF07XG4gICAgICAgICAgICBhcmdzWzBdID0gd3JhcChvcmlnaW5hbENhbGxiYWNrLCB7XG4gICAgICAgICAgICAgICAgbWVjaGFuaXNtOiB7XG4gICAgICAgICAgICAgICAgICAgIGRhdGE6IHsgZnVuY3Rpb246IGdldEZ1bmN0aW9uTmFtZShvcmlnaW5hbCkgfSxcbiAgICAgICAgICAgICAgICAgICAgaGFuZGxlZDogdHJ1ZSxcbiAgICAgICAgICAgICAgICAgICAgdHlwZTogJ2luc3RydW1lbnQnLFxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIHJldHVybiBvcmlnaW5hbC5hcHBseSh0aGlzLCBhcmdzKTtcbiAgICAgICAgfTtcbiAgICB9O1xuICAgIC8qKiBKU0RvYyAqL1xuICAgIFRyeUNhdGNoLnByb3RvdHlwZS5fd3JhcFJBRiA9IGZ1bmN0aW9uIChvcmlnaW5hbCkge1xuICAgICAgICByZXR1cm4gZnVuY3Rpb24gKGNhbGxiYWNrKSB7XG4gICAgICAgICAgICByZXR1cm4gb3JpZ2luYWwuY2FsbCh0aGlzLCB3cmFwKGNhbGxiYWNrLCB7XG4gICAgICAgICAgICAgICAgbWVjaGFuaXNtOiB7XG4gICAgICAgICAgICAgICAgICAgIGRhdGE6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGZ1bmN0aW9uOiAncmVxdWVzdEFuaW1hdGlvbkZyYW1lJyxcbiAgICAgICAgICAgICAgICAgICAgICAgIGhhbmRsZXI6IGdldEZ1bmN0aW9uTmFtZShvcmlnaW5hbCksXG4gICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgIGhhbmRsZWQ6IHRydWUsXG4gICAgICAgICAgICAgICAgICAgIHR5cGU6ICdpbnN0cnVtZW50JyxcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgfSkpO1xuICAgICAgICB9O1xuICAgIH07XG4gICAgLyoqIEpTRG9jICovXG4gICAgVHJ5Q2F0Y2gucHJvdG90eXBlLl93cmFwRXZlbnRUYXJnZXQgPSBmdW5jdGlvbiAodGFyZ2V0KSB7XG4gICAgICAgIHZhciBnbG9iYWwgPSBnZXRHbG9iYWxPYmplY3QoKTtcbiAgICAgICAgdmFyIHByb3RvID0gZ2xvYmFsW3RhcmdldF0gJiYgZ2xvYmFsW3RhcmdldF0ucHJvdG90eXBlO1xuICAgICAgICBpZiAoIXByb3RvIHx8ICFwcm90by5oYXNPd25Qcm9wZXJ0eSB8fCAhcHJvdG8uaGFzT3duUHJvcGVydHkoJ2FkZEV2ZW50TGlzdGVuZXInKSkge1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIGZpbGwocHJvdG8sICdhZGRFdmVudExpc3RlbmVyJywgZnVuY3Rpb24gKG9yaWdpbmFsKSB7XG4gICAgICAgICAgICByZXR1cm4gZnVuY3Rpb24gKGV2ZW50TmFtZSwgZm4sIG9wdGlvbnMpIHtcbiAgICAgICAgICAgICAgICB0cnkge1xuICAgICAgICAgICAgICAgICAgICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6bm8tdW5ib3VuZC1tZXRob2Qgc3RyaWN0LXR5cGUtcHJlZGljYXRlc1xuICAgICAgICAgICAgICAgICAgICBpZiAodHlwZW9mIGZuLmhhbmRsZUV2ZW50ID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBmbi5oYW5kbGVFdmVudCA9IHdyYXAoZm4uaGFuZGxlRXZlbnQuYmluZChmbiksIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBtZWNoYW5pc206IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGF0YToge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZnVuY3Rpb246ICdoYW5kbGVFdmVudCcsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGVyOiBnZXRGdW5jdGlvbk5hbWUoZm4pLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGFyZ2V0OiB0YXJnZXQsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhhbmRsZWQ6IHRydWUsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6ICdpbnN0cnVtZW50JyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgY2F0Y2ggKGVycikge1xuICAgICAgICAgICAgICAgICAgICAvLyBjYW4gc29tZXRpbWVzIGdldCAnUGVybWlzc2lvbiBkZW5pZWQgdG8gYWNjZXNzIHByb3BlcnR5IFwiaGFuZGxlIEV2ZW50J1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICByZXR1cm4gb3JpZ2luYWwuY2FsbCh0aGlzLCBldmVudE5hbWUsIHdyYXAoZm4sIHtcbiAgICAgICAgICAgICAgICAgICAgbWVjaGFuaXNtOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBkYXRhOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZnVuY3Rpb246ICdhZGRFdmVudExpc3RlbmVyJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGVyOiBnZXRGdW5jdGlvbk5hbWUoZm4pLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRhcmdldDogdGFyZ2V0LFxuICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgIGhhbmRsZWQ6IHRydWUsXG4gICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiAnaW5zdHJ1bWVudCcsXG4gICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgfSksIG9wdGlvbnMpO1xuICAgICAgICAgICAgfTtcbiAgICAgICAgfSk7XG4gICAgICAgIGZpbGwocHJvdG8sICdyZW1vdmVFdmVudExpc3RlbmVyJywgZnVuY3Rpb24gKG9yaWdpbmFsKSB7XG4gICAgICAgICAgICByZXR1cm4gZnVuY3Rpb24gKGV2ZW50TmFtZSwgZm4sIG9wdGlvbnMpIHtcbiAgICAgICAgICAgICAgICB2YXIgY2FsbGJhY2sgPSBmbjtcbiAgICAgICAgICAgICAgICB0cnkge1xuICAgICAgICAgICAgICAgICAgICBjYWxsYmFjayA9IGNhbGxiYWNrICYmIChjYWxsYmFjay5fX3NlbnRyeV93cmFwcGVkX18gfHwgY2FsbGJhY2spO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBjYXRjaCAoZSkge1xuICAgICAgICAgICAgICAgICAgICAvLyBpZ25vcmUsIGFjY2Vzc2luZyBfX3NlbnRyeV93cmFwcGVkX18gd2lsbCB0aHJvdyBpbiBzb21lIFNlbGVuaXVtIGVudmlyb25tZW50c1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICByZXR1cm4gb3JpZ2luYWwuY2FsbCh0aGlzLCBldmVudE5hbWUsIGNhbGxiYWNrLCBvcHRpb25zKTtcbiAgICAgICAgICAgIH07XG4gICAgICAgIH0pO1xuICAgIH07XG4gICAgLyoqIEpTRG9jICovXG4gICAgVHJ5Q2F0Y2gucHJvdG90eXBlLl93cmFwWEhSID0gZnVuY3Rpb24gKG9yaWdpbmFsU2VuZCkge1xuICAgICAgICByZXR1cm4gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgdmFyIGFyZ3MgPSBbXTtcbiAgICAgICAgICAgIGZvciAodmFyIF9pID0gMDsgX2kgPCBhcmd1bWVudHMubGVuZ3RoOyBfaSsrKSB7XG4gICAgICAgICAgICAgICAgYXJnc1tfaV0gPSBhcmd1bWVudHNbX2ldO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdmFyIHhociA9IHRoaXM7IC8vIHRzbGludDpkaXNhYmxlLWxpbmU6bm8tdGhpcy1hc3NpZ25tZW50XG4gICAgICAgICAgICB2YXIgeG1sSHR0cFJlcXVlc3RQcm9wcyA9IFsnb25sb2FkJywgJ29uZXJyb3InLCAnb25wcm9ncmVzcycsICdvbnJlYWR5c3RhdGVjaGFuZ2UnXTtcbiAgICAgICAgICAgIHhtbEh0dHBSZXF1ZXN0UHJvcHMuZm9yRWFjaChmdW5jdGlvbiAocHJvcCkge1xuICAgICAgICAgICAgICAgIGlmIChwcm9wIGluIHhociAmJiB0eXBlb2YgeGhyW3Byb3BdID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgICAgICAgICAgICAgIGZpbGwoeGhyLCBwcm9wLCBmdW5jdGlvbiAob3JpZ2luYWwpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciB3cmFwT3B0aW9ucyA9IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBtZWNoYW5pc206IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZGF0YToge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZnVuY3Rpb246IHByb3AsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGVyOiBnZXRGdW5jdGlvbk5hbWUob3JpZ2luYWwpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGVkOiB0cnVlLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiAnaW5zdHJ1bWVudCcsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBJZiBJbnN0cnVtZW50IGludGVncmF0aW9uIGhhcyBiZWVuIGNhbGxlZCBiZWZvcmUgVHJ5Q2F0Y2gsIGdldCB0aGUgbmFtZSBvZiBvcmlnaW5hbCBmdW5jdGlvblxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKG9yaWdpbmFsLl9fc2VudHJ5X29yaWdpbmFsX18pIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB3cmFwT3B0aW9ucy5tZWNoYW5pc20uZGF0YS5oYW5kbGVyID0gZ2V0RnVuY3Rpb25OYW1lKG9yaWdpbmFsLl9fc2VudHJ5X29yaWdpbmFsX18pO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gT3RoZXJ3aXNlIHdyYXAgZGlyZWN0bHlcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB3cmFwKG9yaWdpbmFsLCB3cmFwT3B0aW9ucyk7XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgcmV0dXJuIG9yaWdpbmFsU2VuZC5hcHBseSh0aGlzLCBhcmdzKTtcbiAgICAgICAgfTtcbiAgICB9O1xuICAgIC8qKlxuICAgICAqIFdyYXAgdGltZXIgZnVuY3Rpb25zIGFuZCBldmVudCB0YXJnZXRzIHRvIGNhdGNoIGVycm9yc1xuICAgICAqIGFuZCBwcm92aWRlIGJldHRlciBtZXRhZGF0YS5cbiAgICAgKi9cbiAgICBUcnlDYXRjaC5wcm90b3R5cGUuc2V0dXBPbmNlID0gZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXIgZ2xvYmFsID0gZ2V0R2xvYmFsT2JqZWN0KCk7XG4gICAgICAgIGlmICh0aGlzLl9vcHRpb25zLnNldFRpbWVvdXQpIHtcbiAgICAgICAgICAgIGZpbGwoZ2xvYmFsLCAnc2V0VGltZW91dCcsIHRoaXMuX3dyYXBUaW1lRnVuY3Rpb24uYmluZCh0aGlzKSk7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHRoaXMuX29wdGlvbnMuc2V0SW50ZXJ2YWwpIHtcbiAgICAgICAgICAgIGZpbGwoZ2xvYmFsLCAnc2V0SW50ZXJ2YWwnLCB0aGlzLl93cmFwVGltZUZ1bmN0aW9uLmJpbmQodGhpcykpO1xuICAgICAgICB9XG4gICAgICAgIGlmICh0aGlzLl9vcHRpb25zLnJlcXVlc3RBbmltYXRpb25GcmFtZSkge1xuICAgICAgICAgICAgZmlsbChnbG9iYWwsICdyZXF1ZXN0QW5pbWF0aW9uRnJhbWUnLCB0aGlzLl93cmFwUkFGLmJpbmQodGhpcykpO1xuICAgICAgICB9XG4gICAgICAgIGlmICh0aGlzLl9vcHRpb25zLlhNTEh0dHBSZXF1ZXN0ICYmICdYTUxIdHRwUmVxdWVzdCcgaW4gZ2xvYmFsKSB7XG4gICAgICAgICAgICBmaWxsKFhNTEh0dHBSZXF1ZXN0LnByb3RvdHlwZSwgJ3NlbmQnLCB0aGlzLl93cmFwWEhSLmJpbmQodGhpcykpO1xuICAgICAgICB9XG4gICAgICAgIGlmICh0aGlzLl9vcHRpb25zLmV2ZW50VGFyZ2V0KSB7XG4gICAgICAgICAgICB2YXIgZXZlbnRUYXJnZXQgPSBBcnJheS5pc0FycmF5KHRoaXMuX29wdGlvbnMuZXZlbnRUYXJnZXQpID8gdGhpcy5fb3B0aW9ucy5ldmVudFRhcmdldCA6IERFRkFVTFRfRVZFTlRfVEFSR0VUO1xuICAgICAgICAgICAgZXZlbnRUYXJnZXQuZm9yRWFjaCh0aGlzLl93cmFwRXZlbnRUYXJnZXQuYmluZCh0aGlzKSk7XG4gICAgICAgIH1cbiAgICB9O1xuICAgIC8qKlxuICAgICAqIEBpbmhlcml0RG9jXG4gICAgICovXG4gICAgVHJ5Q2F0Y2guaWQgPSAnVHJ5Q2F0Y2gnO1xuICAgIHJldHVybiBUcnlDYXRjaDtcbn0oKSk7XG5leHBvcnQgeyBUcnlDYXRjaCB9O1xuLy8jIHNvdXJjZU1hcHBpbmdVUkw9dHJ5Y2F0Y2guanMubWFwIiwiaW1wb3J0ICogYXMgdHNsaWJfMSBmcm9tIFwidHNsaWJcIjtcbmltcG9ydCB7IGFkZEdsb2JhbEV2ZW50UHJvY2Vzc29yLCBnZXRDdXJyZW50SHViIH0gZnJvbSAnQHNlbnRyeS9jb3JlJztcbmltcG9ydCB7IGdldEdsb2JhbE9iamVjdCB9IGZyb20gJ0BzZW50cnkvdXRpbHMnO1xudmFyIGdsb2JhbCA9IGdldEdsb2JhbE9iamVjdCgpO1xuLyoqIFVzZXJBZ2VudCAqL1xudmFyIFVzZXJBZ2VudCA9IC8qKiBAY2xhc3MgKi8gKGZ1bmN0aW9uICgpIHtcbiAgICBmdW5jdGlvbiBVc2VyQWdlbnQoKSB7XG4gICAgICAgIC8qKlxuICAgICAgICAgKiBAaW5oZXJpdERvY1xuICAgICAgICAgKi9cbiAgICAgICAgdGhpcy5uYW1lID0gVXNlckFnZW50LmlkO1xuICAgIH1cbiAgICAvKipcbiAgICAgKiBAaW5oZXJpdERvY1xuICAgICAqL1xuICAgIFVzZXJBZ2VudC5wcm90b3R5cGUuc2V0dXBPbmNlID0gZnVuY3Rpb24gKCkge1xuICAgICAgICBhZGRHbG9iYWxFdmVudFByb2Nlc3NvcihmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgICAgICAgIGlmIChnZXRDdXJyZW50SHViKCkuZ2V0SW50ZWdyYXRpb24oVXNlckFnZW50KSkge1xuICAgICAgICAgICAgICAgIGlmICghZ2xvYmFsLm5hdmlnYXRvciB8fCAhZ2xvYmFsLmxvY2F0aW9uKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBldmVudDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgdmFyIHJlcXVlc3QgPSBldmVudC5yZXF1ZXN0IHx8IHt9O1xuICAgICAgICAgICAgICAgIHJlcXVlc3QudXJsID0gcmVxdWVzdC51cmwgfHwgZ2xvYmFsLmxvY2F0aW9uLmhyZWY7XG4gICAgICAgICAgICAgICAgcmVxdWVzdC5oZWFkZXJzID0gcmVxdWVzdC5oZWFkZXJzIHx8IHt9O1xuICAgICAgICAgICAgICAgIHJlcXVlc3QuaGVhZGVyc1snVXNlci1BZ2VudCddID0gZ2xvYmFsLm5hdmlnYXRvci51c2VyQWdlbnQ7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHRzbGliXzEuX19hc3NpZ24oe30sIGV2ZW50LCB7IHJlcXVlc3Q6IHJlcXVlc3QgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gZXZlbnQ7XG4gICAgICAgIH0pO1xuICAgIH07XG4gICAgLyoqXG4gICAgICogQGluaGVyaXREb2NcbiAgICAgKi9cbiAgICBVc2VyQWdlbnQuaWQgPSAnVXNlckFnZW50JztcbiAgICByZXR1cm4gVXNlckFnZW50O1xufSgpKTtcbmV4cG9ydCB7IFVzZXJBZ2VudCB9O1xuLy8jIHNvdXJjZU1hcHBpbmdVUkw9dXNlcmFnZW50LmpzLm1hcCIsImltcG9ydCB7IGV4dHJhY3RFeGNlcHRpb25LZXlzRm9yTWVzc2FnZSwgaXNFdmVudCwgbm9ybWFsaXplVG9TaXplIH0gZnJvbSAnQHNlbnRyeS91dGlscyc7XG5pbXBvcnQgeyBjb21wdXRlU3RhY2tUcmFjZSB9IGZyb20gJy4vdHJhY2VraXQnO1xudmFyIFNUQUNLVFJBQ0VfTElNSVQgPSA1MDtcbi8qKlxuICogVGhpcyBmdW5jdGlvbiBjcmVhdGVzIGFuIGV4Y2VwdGlvbiBmcm9tIGFuIFRyYWNlS2l0U3RhY2tUcmFjZVxuICogQHBhcmFtIHN0YWNrdHJhY2UgVHJhY2VLaXRTdGFja1RyYWNlIHRoYXQgd2lsbCBiZSBjb252ZXJ0ZWQgdG8gYW4gZXhjZXB0aW9uXG4gKiBAaGlkZGVuXG4gKi9cbmV4cG9ydCBmdW5jdGlvbiBleGNlcHRpb25Gcm9tU3RhY2t0cmFjZShzdGFja3RyYWNlKSB7XG4gICAgdmFyIGZyYW1lcyA9IHByZXBhcmVGcmFtZXNGb3JFdmVudChzdGFja3RyYWNlLnN0YWNrKTtcbiAgICB2YXIgZXhjZXB0aW9uID0ge1xuICAgICAgICB0eXBlOiBzdGFja3RyYWNlLm5hbWUsXG4gICAgICAgIHZhbHVlOiBzdGFja3RyYWNlLm1lc3NhZ2UsXG4gICAgfTtcbiAgICBpZiAoZnJhbWVzICYmIGZyYW1lcy5sZW5ndGgpIHtcbiAgICAgICAgZXhjZXB0aW9uLnN0YWNrdHJhY2UgPSB7IGZyYW1lczogZnJhbWVzIH07XG4gICAgfVxuICAgIC8vIHRzbGludDpkaXNhYmxlLW5leHQtbGluZTpzdHJpY3QtdHlwZS1wcmVkaWNhdGVzXG4gICAgaWYgKGV4Y2VwdGlvbi50eXBlID09PSB1bmRlZmluZWQgJiYgZXhjZXB0aW9uLnZhbHVlID09PSAnJykge1xuICAgICAgICBleGNlcHRpb24udmFsdWUgPSAnVW5yZWNvdmVyYWJsZSBlcnJvciBjYXVnaHQnO1xuICAgIH1cbiAgICByZXR1cm4gZXhjZXB0aW9uO1xufVxuLyoqXG4gKiBAaGlkZGVuXG4gKi9cbmV4cG9ydCBmdW5jdGlvbiBldmVudEZyb21QbGFpbk9iamVjdChleGNlcHRpb24sIHN5bnRoZXRpY0V4Y2VwdGlvbiwgcmVqZWN0aW9uKSB7XG4gICAgdmFyIGV2ZW50ID0ge1xuICAgICAgICBleGNlcHRpb246IHtcbiAgICAgICAgICAgIHZhbHVlczogW1xuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgdHlwZTogaXNFdmVudChleGNlcHRpb24pID8gZXhjZXB0aW9uLmNvbnN0cnVjdG9yLm5hbWUgOiByZWplY3Rpb24gPyAnVW5oYW5kbGVkUmVqZWN0aW9uJyA6ICdFcnJvcicsXG4gICAgICAgICAgICAgICAgICAgIHZhbHVlOiBcIk5vbi1FcnJvciBcIiArIChyZWplY3Rpb24gPyAncHJvbWlzZSByZWplY3Rpb24nIDogJ2V4Y2VwdGlvbicpICsgXCIgY2FwdHVyZWQgd2l0aCBrZXlzOiBcIiArIGV4dHJhY3RFeGNlcHRpb25LZXlzRm9yTWVzc2FnZShleGNlcHRpb24pLFxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBdLFxuICAgICAgICB9LFxuICAgICAgICBleHRyYToge1xuICAgICAgICAgICAgX19zZXJpYWxpemVkX186IG5vcm1hbGl6ZVRvU2l6ZShleGNlcHRpb24pLFxuICAgICAgICB9LFxuICAgIH07XG4gICAgaWYgKHN5bnRoZXRpY0V4Y2VwdGlvbikge1xuICAgICAgICB2YXIgc3RhY2t0cmFjZSA9IGNvbXB1dGVTdGFja1RyYWNlKHN5bnRoZXRpY0V4Y2VwdGlvbik7XG4gICAgICAgIHZhciBmcmFtZXNfMSA9IHByZXBhcmVGcmFtZXNGb3JFdmVudChzdGFja3RyYWNlLnN0YWNrKTtcbiAgICAgICAgZXZlbnQuc3RhY2t0cmFjZSA9IHtcbiAgICAgICAgICAgIGZyYW1lczogZnJhbWVzXzEsXG4gICAgICAgIH07XG4gICAgfVxuICAgIHJldHVybiBldmVudDtcbn1cbi8qKlxuICogQGhpZGRlblxuICovXG5leHBvcnQgZnVuY3Rpb24gZXZlbnRGcm9tU3RhY2t0cmFjZShzdGFja3RyYWNlKSB7XG4gICAgdmFyIGV4Y2VwdGlvbiA9IGV4Y2VwdGlvbkZyb21TdGFja3RyYWNlKHN0YWNrdHJhY2UpO1xuICAgIHJldHVybiB7XG4gICAgICAgIGV4Y2VwdGlvbjoge1xuICAgICAgICAgICAgdmFsdWVzOiBbZXhjZXB0aW9uXSxcbiAgICAgICAgfSxcbiAgICB9O1xufVxuLyoqXG4gKiBAaGlkZGVuXG4gKi9cbmV4cG9ydCBmdW5jdGlvbiBwcmVwYXJlRnJhbWVzRm9yRXZlbnQoc3RhY2spIHtcbiAgICBpZiAoIXN0YWNrIHx8ICFzdGFjay5sZW5ndGgpIHtcbiAgICAgICAgcmV0dXJuIFtdO1xuICAgIH1cbiAgICB2YXIgbG9jYWxTdGFjayA9IHN0YWNrO1xuICAgIHZhciBmaXJzdEZyYW1lRnVuY3Rpb24gPSBsb2NhbFN0YWNrWzBdLmZ1bmMgfHwgJyc7XG4gICAgdmFyIGxhc3RGcmFtZUZ1bmN0aW9uID0gbG9jYWxTdGFja1tsb2NhbFN0YWNrLmxlbmd0aCAtIDFdLmZ1bmMgfHwgJyc7XG4gICAgLy8gSWYgc3RhY2sgc3RhcnRzIHdpdGggb25lIG9mIG91ciBBUEkgY2FsbHMsIHJlbW92ZSBpdCAoc3RhcnRzLCBtZWFuaW5nIGl0J3MgdGhlIHRvcCBvZiB0aGUgc3RhY2sgLSBha2EgbGFzdCBjYWxsKVxuICAgIGlmIChmaXJzdEZyYW1lRnVuY3Rpb24uaW5kZXhPZignY2FwdHVyZU1lc3NhZ2UnKSAhPT0gLTEgfHwgZmlyc3RGcmFtZUZ1bmN0aW9uLmluZGV4T2YoJ2NhcHR1cmVFeGNlcHRpb24nKSAhPT0gLTEpIHtcbiAgICAgICAgbG9jYWxTdGFjayA9IGxvY2FsU3RhY2suc2xpY2UoMSk7XG4gICAgfVxuICAgIC8vIElmIHN0YWNrIGVuZHMgd2l0aCBvbmUgb2Ygb3VyIGludGVybmFsIEFQSSBjYWxscywgcmVtb3ZlIGl0IChlbmRzLCBtZWFuaW5nIGl0J3MgdGhlIGJvdHRvbSBvZiB0aGUgc3RhY2sgLSBha2EgdG9wLW1vc3QgY2FsbClcbiAgICBpZiAobGFzdEZyYW1lRnVuY3Rpb24uaW5kZXhPZignc2VudHJ5V3JhcHBlZCcpICE9PSAtMSkge1xuICAgICAgICBsb2NhbFN0YWNrID0gbG9jYWxTdGFjay5zbGljZSgwLCAtMSk7XG4gICAgfVxuICAgIC8vIFRoZSBmcmFtZSB3aGVyZSB0aGUgY3Jhc2ggaGFwcGVuZWQsIHNob3VsZCBiZSB0aGUgbGFzdCBlbnRyeSBpbiB0aGUgYXJyYXlcbiAgICByZXR1cm4gbG9jYWxTdGFja1xuICAgICAgICAuc2xpY2UoMCwgU1RBQ0tUUkFDRV9MSU1JVClcbiAgICAgICAgLm1hcChmdW5jdGlvbiAoZnJhbWUpIHsgcmV0dXJuICh7XG4gICAgICAgIGNvbG5vOiBmcmFtZS5jb2x1bW4gPT09IG51bGwgPyB1bmRlZmluZWQgOiBmcmFtZS5jb2x1bW4sXG4gICAgICAgIGZpbGVuYW1lOiBmcmFtZS51cmwgfHwgbG9jYWxTdGFja1swXS51cmwsXG4gICAgICAgIGZ1bmN0aW9uOiBmcmFtZS5mdW5jIHx8ICc/JyxcbiAgICAgICAgaW5fYXBwOiB0cnVlLFxuICAgICAgICBsaW5lbm86IGZyYW1lLmxpbmUgPT09IG51bGwgPyB1bmRlZmluZWQgOiBmcmFtZS5saW5lLFxuICAgIH0pOyB9KVxuICAgICAgICAucmV2ZXJzZSgpO1xufVxuLy8jIHNvdXJjZU1hcHBpbmdVUkw9cGFyc2Vycy5qcy5tYXAiLCJpbXBvcnQgeyBnZXRDdXJyZW50SHViLCBpbml0QW5kQmluZCwgSW50ZWdyYXRpb25zIGFzIENvcmVJbnRlZ3JhdGlvbnMgfSBmcm9tICdAc2VudHJ5L2NvcmUnO1xuaW1wb3J0IHsgZ2V0R2xvYmFsT2JqZWN0LCBTeW5jUHJvbWlzZSB9IGZyb20gJ0BzZW50cnkvdXRpbHMnO1xuaW1wb3J0IHsgQnJvd3NlckNsaWVudCB9IGZyb20gJy4vY2xpZW50JztcbmltcG9ydCB7IHdyYXAgYXMgaW50ZXJuYWxXcmFwIH0gZnJvbSAnLi9oZWxwZXJzJztcbmltcG9ydCB7IEJyZWFkY3J1bWJzLCBHbG9iYWxIYW5kbGVycywgTGlua2VkRXJyb3JzLCBUcnlDYXRjaCwgVXNlckFnZW50IH0gZnJvbSAnLi9pbnRlZ3JhdGlvbnMnO1xuZXhwb3J0IHZhciBkZWZhdWx0SW50ZWdyYXRpb25zID0gW1xuICAgIG5ldyBDb3JlSW50ZWdyYXRpb25zLkluYm91bmRGaWx0ZXJzKCksXG4gICAgbmV3IENvcmVJbnRlZ3JhdGlvbnMuRnVuY3Rpb25Ub1N0cmluZygpLFxuICAgIG5ldyBUcnlDYXRjaCgpLFxuICAgIG5ldyBCcmVhZGNydW1icygpLFxuICAgIG5ldyBHbG9iYWxIYW5kbGVycygpLFxuICAgIG5ldyBMaW5rZWRFcnJvcnMoKSxcbiAgICBuZXcgVXNlckFnZW50KCksXG5dO1xuLyoqXG4gKiBUaGUgU2VudHJ5IEJyb3dzZXIgU0RLIENsaWVudC5cbiAqXG4gKiBUbyB1c2UgdGhpcyBTREssIGNhbGwgdGhlIHtAbGluayBpbml0fSBmdW5jdGlvbiBhcyBlYXJseSBhcyBwb3NzaWJsZSB3aGVuXG4gKiBsb2FkaW5nIHRoZSB3ZWIgcGFnZS4gVG8gc2V0IGNvbnRleHQgaW5mb3JtYXRpb24gb3Igc2VuZCBtYW51YWwgZXZlbnRzLCB1c2VcbiAqIHRoZSBwcm92aWRlZCBtZXRob2RzLlxuICpcbiAqIEBleGFtcGxlXG4gKlxuICogYGBgXG4gKlxuICogaW1wb3J0IHsgaW5pdCB9IGZyb20gJ0BzZW50cnkvYnJvd3Nlcic7XG4gKlxuICogaW5pdCh7XG4gKiAgIGRzbjogJ19fRFNOX18nLFxuICogICAvLyAuLi5cbiAqIH0pO1xuICogYGBgXG4gKlxuICogQGV4YW1wbGVcbiAqIGBgYFxuICpcbiAqIGltcG9ydCB7IGNvbmZpZ3VyZVNjb3BlIH0gZnJvbSAnQHNlbnRyeS9icm93c2VyJztcbiAqIGNvbmZpZ3VyZVNjb3BlKChzY29wZTogU2NvcGUpID0+IHtcbiAqICAgc2NvcGUuc2V0RXh0cmEoeyBiYXR0ZXJ5OiAwLjcgfSk7XG4gKiAgIHNjb3BlLnNldFRhZyh7IHVzZXJfbW9kZTogJ2FkbWluJyB9KTtcbiAqICAgc2NvcGUuc2V0VXNlcih7IGlkOiAnNDcxMScgfSk7XG4gKiB9KTtcbiAqIGBgYFxuICpcbiAqIEBleGFtcGxlXG4gKiBgYGBcbiAqXG4gKiBpbXBvcnQgeyBhZGRCcmVhZGNydW1iIH0gZnJvbSAnQHNlbnRyeS9icm93c2VyJztcbiAqIGFkZEJyZWFkY3J1bWIoe1xuICogICBtZXNzYWdlOiAnTXkgQnJlYWRjcnVtYicsXG4gKiAgIC8vIC4uLlxuICogfSk7XG4gKiBgYGBcbiAqXG4gKiBAZXhhbXBsZVxuICpcbiAqIGBgYFxuICpcbiAqIGltcG9ydCAqIGFzIFNlbnRyeSBmcm9tICdAc2VudHJ5L2Jyb3dzZXInO1xuICogU2VudHJ5LmNhcHR1cmVNZXNzYWdlKCdIZWxsbywgd29ybGQhJyk7XG4gKiBTZW50cnkuY2FwdHVyZUV4Y2VwdGlvbihuZXcgRXJyb3IoJ0dvb2QgYnllJykpO1xuICogU2VudHJ5LmNhcHR1cmVFdmVudCh7XG4gKiAgIG1lc3NhZ2U6ICdNYW51YWwnLFxuICogICBzdGFja3RyYWNlOiBbXG4gKiAgICAgLy8gLi4uXG4gKiAgIF0sXG4gKiB9KTtcbiAqIGBgYFxuICpcbiAqIEBzZWUge0BsaW5rIEJyb3dzZXJPcHRpb25zfSBmb3IgZG9jdW1lbnRhdGlvbiBvbiBjb25maWd1cmF0aW9uIG9wdGlvbnMuXG4gKi9cbmV4cG9ydCBmdW5jdGlvbiBpbml0KG9wdGlvbnMpIHtcbiAgICBpZiAob3B0aW9ucyA9PT0gdm9pZCAwKSB7IG9wdGlvbnMgPSB7fTsgfVxuICAgIGlmIChvcHRpb25zLmRlZmF1bHRJbnRlZ3JhdGlvbnMgPT09IHVuZGVmaW5lZCkge1xuICAgICAgICBvcHRpb25zLmRlZmF1bHRJbnRlZ3JhdGlvbnMgPSBkZWZhdWx0SW50ZWdyYXRpb25zO1xuICAgIH1cbiAgICBpZiAob3B0aW9ucy5yZWxlYXNlID09PSB1bmRlZmluZWQpIHtcbiAgICAgICAgdmFyIHdpbmRvd18xID0gZ2V0R2xvYmFsT2JqZWN0KCk7XG4gICAgICAgIC8vIFRoaXMgc3VwcG9ydHMgdGhlIHZhcmlhYmxlIHRoYXQgc2VudHJ5LXdlYnBhY2stcGx1Z2luIGluamVjdHNcbiAgICAgICAgaWYgKHdpbmRvd18xLlNFTlRSWV9SRUxFQVNFICYmIHdpbmRvd18xLlNFTlRSWV9SRUxFQVNFLmlkKSB7XG4gICAgICAgICAgICBvcHRpb25zLnJlbGVhc2UgPSB3aW5kb3dfMS5TRU5UUllfUkVMRUFTRS5pZDtcbiAgICAgICAgfVxuICAgIH1cbiAgICBpbml0QW5kQmluZChCcm93c2VyQ2xpZW50LCBvcHRpb25zKTtcbn1cbi8qKlxuICogUHJlc2VudCB0aGUgdXNlciB3aXRoIGEgcmVwb3J0IGRpYWxvZy5cbiAqXG4gKiBAcGFyYW0gb3B0aW9ucyBFdmVyeXRoaW5nIGlzIG9wdGlvbmFsLCB3ZSB0cnkgdG8gZmV0Y2ggYWxsIGluZm8gbmVlZCBmcm9tIHRoZSBnbG9iYWwgc2NvcGUuXG4gKi9cbmV4cG9ydCBmdW5jdGlvbiBzaG93UmVwb3J0RGlhbG9nKG9wdGlvbnMpIHtcbiAgICBpZiAob3B0aW9ucyA9PT0gdm9pZCAwKSB7IG9wdGlvbnMgPSB7fTsgfVxuICAgIGlmICghb3B0aW9ucy5ldmVudElkKSB7XG4gICAgICAgIG9wdGlvbnMuZXZlbnRJZCA9IGdldEN1cnJlbnRIdWIoKS5sYXN0RXZlbnRJZCgpO1xuICAgIH1cbiAgICB2YXIgY2xpZW50ID0gZ2V0Q3VycmVudEh1YigpLmdldENsaWVudCgpO1xuICAgIGlmIChjbGllbnQpIHtcbiAgICAgICAgY2xpZW50LnNob3dSZXBvcnREaWFsb2cob3B0aW9ucyk7XG4gICAgfVxufVxuLyoqXG4gKiBUaGlzIGlzIHRoZSBnZXR0ZXIgZm9yIGxhc3RFdmVudElkLlxuICpcbiAqIEByZXR1cm5zIFRoZSBsYXN0IGV2ZW50IGlkIG9mIGEgY2FwdHVyZWQgZXZlbnQuXG4gKi9cbmV4cG9ydCBmdW5jdGlvbiBsYXN0RXZlbnRJZCgpIHtcbiAgICByZXR1cm4gZ2V0Q3VycmVudEh1YigpLmxhc3RFdmVudElkKCk7XG59XG4vKipcbiAqIFRoaXMgZnVuY3Rpb24gaXMgaGVyZSB0byBiZSBBUEkgY29tcGF0aWJsZSB3aXRoIHRoZSBsb2FkZXIuXG4gKiBAaGlkZGVuXG4gKi9cbmV4cG9ydCBmdW5jdGlvbiBmb3JjZUxvYWQoKSB7XG4gICAgLy8gTm9vcFxufVxuLyoqXG4gKiBUaGlzIGZ1bmN0aW9uIGlzIGhlcmUgdG8gYmUgQVBJIGNvbXBhdGlibGUgd2l0aCB0aGUgbG9hZGVyLlxuICogQGhpZGRlblxuICovXG5leHBvcnQgZnVuY3Rpb24gb25Mb2FkKGNhbGxiYWNrKSB7XG4gICAgY2FsbGJhY2soKTtcbn1cbi8qKlxuICogQSBwcm9taXNlIHRoYXQgcmVzb2x2ZXMgd2hlbiBhbGwgY3VycmVudCBldmVudHMgaGF2ZSBiZWVuIHNlbnQuXG4gKiBJZiB5b3UgcHJvdmlkZSBhIHRpbWVvdXQgYW5kIHRoZSBxdWV1ZSB0YWtlcyBsb25nZXIgdG8gZHJhaW4gdGhlIHByb21pc2UgcmV0dXJucyBmYWxzZS5cbiAqXG4gKiBAcGFyYW0gdGltZW91dCBNYXhpbXVtIHRpbWUgaW4gbXMgdGhlIGNsaWVudCBzaG91bGQgd2FpdC5cbiAqL1xuZXhwb3J0IGZ1bmN0aW9uIGZsdXNoKHRpbWVvdXQpIHtcbiAgICB2YXIgY2xpZW50ID0gZ2V0Q3VycmVudEh1YigpLmdldENsaWVudCgpO1xuICAgIGlmIChjbGllbnQpIHtcbiAgICAgICAgcmV0dXJuIGNsaWVudC5mbHVzaCh0aW1lb3V0KTtcbiAgICB9XG4gICAgcmV0dXJuIFN5bmNQcm9taXNlLnJlamVjdChmYWxzZSk7XG59XG4vKipcbiAqIEEgcHJvbWlzZSB0aGF0IHJlc29sdmVzIHdoZW4gYWxsIGN1cnJlbnQgZXZlbnRzIGhhdmUgYmVlbiBzZW50LlxuICogSWYgeW91IHByb3ZpZGUgYSB0aW1lb3V0IGFuZCB0aGUgcXVldWUgdGFrZXMgbG9uZ2VyIHRvIGRyYWluIHRoZSBwcm9taXNlIHJldHVybnMgZmFsc2UuXG4gKlxuICogQHBhcmFtIHRpbWVvdXQgTWF4aW11bSB0aW1lIGluIG1zIHRoZSBjbGllbnQgc2hvdWxkIHdhaXQuXG4gKi9cbmV4cG9ydCBmdW5jdGlvbiBjbG9zZSh0aW1lb3V0KSB7XG4gICAgdmFyIGNsaWVudCA9IGdldEN1cnJlbnRIdWIoKS5nZXRDbGllbnQoKTtcbiAgICBpZiAoY2xpZW50KSB7XG4gICAgICAgIHJldHVybiBjbGllbnQuY2xvc2UodGltZW91dCk7XG4gICAgfVxuICAgIHJldHVybiBTeW5jUHJvbWlzZS5yZWplY3QoZmFsc2UpO1xufVxuLyoqXG4gKiBXcmFwIGNvZGUgd2l0aGluIGEgdHJ5L2NhdGNoIGJsb2NrIHNvIHRoZSBTREsgaXMgYWJsZSB0byBjYXB0dXJlIGVycm9ycy5cbiAqXG4gKiBAcGFyYW0gZm4gQSBmdW5jdGlvbiB0byB3cmFwLlxuICpcbiAqIEByZXR1cm5zIFRoZSByZXN1bHQgb2Ygd3JhcHBlZCBmdW5jdGlvbiBjYWxsLlxuICovXG5leHBvcnQgZnVuY3Rpb24gd3JhcChmbikge1xuICAgIHJldHVybiBpbnRlcm5hbFdyYXAoZm4pKCk7IC8vIHRzbGludDpkaXNhYmxlLWxpbmU6bm8tdW5zYWZlLWFueVxufVxuLy8jIHNvdXJjZU1hcHBpbmdVUkw9c2RrLmpzLm1hcCIsIi8vIHRzbGludDpkaXNhYmxlOm9iamVjdC1saXRlcmFsLXNvcnQta2V5c1xuaW1wb3J0ICogYXMgdHNsaWJfMSBmcm9tIFwidHNsaWJcIjtcbi8vIGdsb2JhbCByZWZlcmVuY2UgdG8gc2xpY2VcbnZhciBVTktOT1dOX0ZVTkNUSU9OID0gJz8nO1xuLy8gQ2hyb21pdW0gYmFzZWQgYnJvd3NlcnM6IENocm9tZSwgQnJhdmUsIG5ldyBPcGVyYSwgbmV3IEVkZ2VcbnZhciBjaHJvbWUgPSAvXlxccyphdCAoPzooLio/KSA/XFwoKT8oKD86ZmlsZXxodHRwcz98YmxvYnxjaHJvbWUtZXh0ZW5zaW9ufGFkZHJlc3N8bmF0aXZlfGV2YWx8d2VicGFja3w8YW5vbnltb3VzPnxbLWEtel0rOnwuKmJ1bmRsZXxcXC8pLio/KSg/OjooXFxkKykpPyg/OjooXFxkKykpP1xcKT9cXHMqJC9pO1xuLy8gZ2Vja28gcmVnZXg6IGAoPzpidW5kbGV8XFxkK1xcLmpzKWA6IGBidW5kbGVgIGlzIGZvciByZWFjdCBuYXRpdmUsIGBcXGQrXFwuanNgIGFsc28gYnV0IHNwZWNpZmljYWxseSBmb3IgcmFtIGJ1bmRsZXMgYmVjYXVzZSBpdFxuLy8gZ2VuZXJhdGVzIGZpbGVuYW1lcyB3aXRob3V0IGEgcHJlZml4IGxpa2UgYGZpbGU6Ly9gIHRoZSBmaWxlbmFtZXMgaW4gdGhlIHN0YWNrdHJhY2UgYXJlIGp1c3QgNDIuanNcbi8vIFdlIG5lZWQgdGhpcyBzcGVjaWZpYyBjYXNlIGZvciBub3cgYmVjYXVzZSB3ZSB3YW50IG5vIG90aGVyIHJlZ2V4IHRvIG1hdGNoLlxudmFyIGdlY2tvID0gL15cXHMqKC4qPykoPzpcXCgoLio/KVxcKSk/KD86XnxAKT8oKD86ZmlsZXxodHRwcz98YmxvYnxjaHJvbWV8d2VicGFja3xyZXNvdXJjZXxtb3otZXh0ZW5zaW9uKS4qPzpcXC8uKj98XFxbbmF0aXZlIGNvZGVcXF18W15AXSooPzpidW5kbGV8XFxkK1xcLmpzKSkoPzo6KFxcZCspKT8oPzo6KFxcZCspKT9cXHMqJC9pO1xudmFyIHdpbmpzID0gL15cXHMqYXQgKD86KCg/OlxcW29iamVjdCBvYmplY3RcXF0pPy4rKSApP1xcKD8oKD86ZmlsZXxtcy1hcHB4fGh0dHBzP3x3ZWJwYWNrfGJsb2IpOi4qPyk6KFxcZCspKD86OihcXGQrKSk/XFwpP1xccyokL2k7XG52YXIgZ2Vja29FdmFsID0gLyhcXFMrKSBsaW5lIChcXGQrKSg/OiA+IGV2YWwgbGluZSBcXGQrKSogPiBldmFsL2k7XG52YXIgY2hyb21lRXZhbCA9IC9cXCgoXFxTKikoPzo6KFxcZCspKSg/OjooXFxkKykpXFwpLztcbi8qKiBKU0RvYyAqL1xuZXhwb3J0IGZ1bmN0aW9uIGNvbXB1dGVTdGFja1RyYWNlKGV4KSB7XG4gICAgLy8gdHNsaW50OmRpc2FibGU6bm8tdW5zYWZlLWFueVxuICAgIHZhciBzdGFjayA9IG51bGw7XG4gICAgdmFyIHBvcFNpemUgPSBleCAmJiBleC5mcmFtZXNUb1BvcDtcbiAgICB0cnkge1xuICAgICAgICAvLyBUaGlzIG11c3QgYmUgdHJpZWQgZmlyc3QgYmVjYXVzZSBPcGVyYSAxMCAqZGVzdHJveXMqXG4gICAgICAgIC8vIGl0cyBzdGFja3RyYWNlIHByb3BlcnR5IGlmIHlvdSB0cnkgdG8gYWNjZXNzIHRoZSBzdGFja1xuICAgICAgICAvLyBwcm9wZXJ0eSBmaXJzdCEhXG4gICAgICAgIHN0YWNrID0gY29tcHV0ZVN0YWNrVHJhY2VGcm9tU3RhY2t0cmFjZVByb3AoZXgpO1xuICAgICAgICBpZiAoc3RhY2spIHtcbiAgICAgICAgICAgIHJldHVybiBwb3BGcmFtZXMoc3RhY2ssIHBvcFNpemUpO1xuICAgICAgICB9XG4gICAgfVxuICAgIGNhdGNoIChlKSB7XG4gICAgICAgIC8vIG5vLWVtcHR5XG4gICAgfVxuICAgIHRyeSB7XG4gICAgICAgIHN0YWNrID0gY29tcHV0ZVN0YWNrVHJhY2VGcm9tU3RhY2tQcm9wKGV4KTtcbiAgICAgICAgaWYgKHN0YWNrKSB7XG4gICAgICAgICAgICByZXR1cm4gcG9wRnJhbWVzKHN0YWNrLCBwb3BTaXplKTtcbiAgICAgICAgfVxuICAgIH1cbiAgICBjYXRjaCAoZSkge1xuICAgICAgICAvLyBuby1lbXB0eVxuICAgIH1cbiAgICByZXR1cm4ge1xuICAgICAgICBtZXNzYWdlOiBleHRyYWN0TWVzc2FnZShleCksXG4gICAgICAgIG5hbWU6IGV4ICYmIGV4Lm5hbWUsXG4gICAgICAgIHN0YWNrOiBbXSxcbiAgICAgICAgZmFpbGVkOiB0cnVlLFxuICAgIH07XG59XG4vKiogSlNEb2MgKi9cbi8vIHRzbGludDpkaXNhYmxlLW5leHQtbGluZTpjeWNsb21hdGljLWNvbXBsZXhpdHlcbmZ1bmN0aW9uIGNvbXB1dGVTdGFja1RyYWNlRnJvbVN0YWNrUHJvcChleCkge1xuICAgIC8vIHRzbGludDpkaXNhYmxlOm5vLWNvbmRpdGlvbmFsLWFzc2lnbm1lbnRcbiAgICBpZiAoIWV4IHx8ICFleC5zdGFjaykge1xuICAgICAgICByZXR1cm4gbnVsbDtcbiAgICB9XG4gICAgdmFyIHN0YWNrID0gW107XG4gICAgdmFyIGxpbmVzID0gZXguc3RhY2suc3BsaXQoJ1xcbicpO1xuICAgIHZhciBpc0V2YWw7XG4gICAgdmFyIHN1Ym1hdGNoO1xuICAgIHZhciBwYXJ0cztcbiAgICB2YXIgZWxlbWVudDtcbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IGxpbmVzLmxlbmd0aDsgKytpKSB7XG4gICAgICAgIGlmICgocGFydHMgPSBjaHJvbWUuZXhlYyhsaW5lc1tpXSkpKSB7XG4gICAgICAgICAgICB2YXIgaXNOYXRpdmUgPSBwYXJ0c1syXSAmJiBwYXJ0c1syXS5pbmRleE9mKCduYXRpdmUnKSA9PT0gMDsgLy8gc3RhcnQgb2YgbGluZVxuICAgICAgICAgICAgaXNFdmFsID0gcGFydHNbMl0gJiYgcGFydHNbMl0uaW5kZXhPZignZXZhbCcpID09PSAwOyAvLyBzdGFydCBvZiBsaW5lXG4gICAgICAgICAgICBpZiAoaXNFdmFsICYmIChzdWJtYXRjaCA9IGNocm9tZUV2YWwuZXhlYyhwYXJ0c1syXSkpKSB7XG4gICAgICAgICAgICAgICAgLy8gdGhyb3cgb3V0IGV2YWwgbGluZS9jb2x1bW4gYW5kIHVzZSB0b3AtbW9zdCBsaW5lL2NvbHVtbiBudW1iZXJcbiAgICAgICAgICAgICAgICBwYXJ0c1syXSA9IHN1Ym1hdGNoWzFdOyAvLyB1cmxcbiAgICAgICAgICAgICAgICBwYXJ0c1szXSA9IHN1Ym1hdGNoWzJdOyAvLyBsaW5lXG4gICAgICAgICAgICAgICAgcGFydHNbNF0gPSBzdWJtYXRjaFszXTsgLy8gY29sdW1uXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbGVtZW50ID0ge1xuICAgICAgICAgICAgICAgIC8vIHdvcmtpbmcgd2l0aCB0aGUgcmVnZXhwIGFib3ZlIGlzIHN1cGVyIHBhaW5mdWwuIGl0IGlzIHF1aXRlIGEgaGFjaywgYnV0IGp1c3Qgc3RyaXBwaW5nIHRoZSBgYWRkcmVzcyBhdCBgXG4gICAgICAgICAgICAgICAgLy8gcHJlZml4IGhlcmUgc2VlbXMgbGlrZSB0aGUgcXVpY2tlc3Qgc29sdXRpb24gZm9yIG5vdy5cbiAgICAgICAgICAgICAgICB1cmw6IHBhcnRzWzJdICYmIHBhcnRzWzJdLmluZGV4T2YoJ2FkZHJlc3MgYXQgJykgPT09IDAgPyBwYXJ0c1syXS5zdWJzdHIoJ2FkZHJlc3MgYXQgJy5sZW5ndGgpIDogcGFydHNbMl0sXG4gICAgICAgICAgICAgICAgZnVuYzogcGFydHNbMV0gfHwgVU5LTk9XTl9GVU5DVElPTixcbiAgICAgICAgICAgICAgICBhcmdzOiBpc05hdGl2ZSA/IFtwYXJ0c1syXV0gOiBbXSxcbiAgICAgICAgICAgICAgICBsaW5lOiBwYXJ0c1szXSA/ICtwYXJ0c1szXSA6IG51bGwsXG4gICAgICAgICAgICAgICAgY29sdW1uOiBwYXJ0c1s0XSA/ICtwYXJ0c1s0XSA6IG51bGwsXG4gICAgICAgICAgICB9O1xuICAgICAgICB9XG4gICAgICAgIGVsc2UgaWYgKChwYXJ0cyA9IHdpbmpzLmV4ZWMobGluZXNbaV0pKSkge1xuICAgICAgICAgICAgZWxlbWVudCA9IHtcbiAgICAgICAgICAgICAgICB1cmw6IHBhcnRzWzJdLFxuICAgICAgICAgICAgICAgIGZ1bmM6IHBhcnRzWzFdIHx8IFVOS05PV05fRlVOQ1RJT04sXG4gICAgICAgICAgICAgICAgYXJnczogW10sXG4gICAgICAgICAgICAgICAgbGluZTogK3BhcnRzWzNdLFxuICAgICAgICAgICAgICAgIGNvbHVtbjogcGFydHNbNF0gPyArcGFydHNbNF0gOiBudWxsLFxuICAgICAgICAgICAgfTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIGlmICgocGFydHMgPSBnZWNrby5leGVjKGxpbmVzW2ldKSkpIHtcbiAgICAgICAgICAgIGlzRXZhbCA9IHBhcnRzWzNdICYmIHBhcnRzWzNdLmluZGV4T2YoJyA+IGV2YWwnKSA+IC0xO1xuICAgICAgICAgICAgaWYgKGlzRXZhbCAmJiAoc3VibWF0Y2ggPSBnZWNrb0V2YWwuZXhlYyhwYXJ0c1szXSkpKSB7XG4gICAgICAgICAgICAgICAgLy8gdGhyb3cgb3V0IGV2YWwgbGluZS9jb2x1bW4gYW5kIHVzZSB0b3AtbW9zdCBsaW5lIG51bWJlclxuICAgICAgICAgICAgICAgIHBhcnRzWzFdID0gcGFydHNbMV0gfHwgXCJldmFsXCI7XG4gICAgICAgICAgICAgICAgcGFydHNbM10gPSBzdWJtYXRjaFsxXTtcbiAgICAgICAgICAgICAgICBwYXJ0c1s0XSA9IHN1Ym1hdGNoWzJdO1xuICAgICAgICAgICAgICAgIHBhcnRzWzVdID0gJyc7IC8vIG5vIGNvbHVtbiB3aGVuIGV2YWxcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2UgaWYgKGkgPT09IDAgJiYgIXBhcnRzWzVdICYmIGV4LmNvbHVtbk51bWJlciAhPT0gdm9pZCAwKSB7XG4gICAgICAgICAgICAgICAgLy8gRmlyZUZveCB1c2VzIHRoaXMgYXdlc29tZSBjb2x1bW5OdW1iZXIgcHJvcGVydHkgZm9yIGl0cyB0b3AgZnJhbWVcbiAgICAgICAgICAgICAgICAvLyBBbHNvIG5vdGUsIEZpcmVmb3gncyBjb2x1bW4gbnVtYmVyIGlzIDAtYmFzZWQgYW5kIGV2ZXJ5dGhpbmcgZWxzZSBleHBlY3RzIDEtYmFzZWQsXG4gICAgICAgICAgICAgICAgLy8gc28gYWRkaW5nIDFcbiAgICAgICAgICAgICAgICAvLyBOT1RFOiB0aGlzIGhhY2sgZG9lc24ndCB3b3JrIGlmIHRvcC1tb3N0IGZyYW1lIGlzIGV2YWxcbiAgICAgICAgICAgICAgICBzdGFja1swXS5jb2x1bW4gPSBleC5jb2x1bW5OdW1iZXIgKyAxO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxlbWVudCA9IHtcbiAgICAgICAgICAgICAgICB1cmw6IHBhcnRzWzNdLFxuICAgICAgICAgICAgICAgIGZ1bmM6IHBhcnRzWzFdIHx8IFVOS05PV05fRlVOQ1RJT04sXG4gICAgICAgICAgICAgICAgYXJnczogcGFydHNbMl0gPyBwYXJ0c1syXS5zcGxpdCgnLCcpIDogW10sXG4gICAgICAgICAgICAgICAgbGluZTogcGFydHNbNF0gPyArcGFydHNbNF0gOiBudWxsLFxuICAgICAgICAgICAgICAgIGNvbHVtbjogcGFydHNbNV0gPyArcGFydHNbNV0gOiBudWxsLFxuICAgICAgICAgICAgfTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIGNvbnRpbnVlO1xuICAgICAgICB9XG4gICAgICAgIGlmICghZWxlbWVudC5mdW5jICYmIGVsZW1lbnQubGluZSkge1xuICAgICAgICAgICAgZWxlbWVudC5mdW5jID0gVU5LTk9XTl9GVU5DVElPTjtcbiAgICAgICAgfVxuICAgICAgICBzdGFjay5wdXNoKGVsZW1lbnQpO1xuICAgIH1cbiAgICBpZiAoIXN0YWNrLmxlbmd0aCkge1xuICAgICAgICByZXR1cm4gbnVsbDtcbiAgICB9XG4gICAgcmV0dXJuIHtcbiAgICAgICAgbWVzc2FnZTogZXh0cmFjdE1lc3NhZ2UoZXgpLFxuICAgICAgICBuYW1lOiBleC5uYW1lLFxuICAgICAgICBzdGFjazogc3RhY2ssXG4gICAgfTtcbn1cbi8qKiBKU0RvYyAqL1xuZnVuY3Rpb24gY29tcHV0ZVN0YWNrVHJhY2VGcm9tU3RhY2t0cmFjZVByb3AoZXgpIHtcbiAgICBpZiAoIWV4IHx8ICFleC5zdGFja3RyYWNlKSB7XG4gICAgICAgIHJldHVybiBudWxsO1xuICAgIH1cbiAgICAvLyBBY2Nlc3MgYW5kIHN0b3JlIHRoZSBzdGFja3RyYWNlIHByb3BlcnR5IGJlZm9yZSBkb2luZyBBTllUSElOR1xuICAgIC8vIGVsc2UgdG8gaXQgYmVjYXVzZSBPcGVyYSBpcyBub3QgdmVyeSBnb29kIGF0IHByb3ZpZGluZyBpdFxuICAgIC8vIHJlbGlhYmx5IGluIG90aGVyIGNpcmN1bXN0YW5jZXMuXG4gICAgdmFyIHN0YWNrdHJhY2UgPSBleC5zdGFja3RyYWNlO1xuICAgIHZhciBvcGVyYTEwUmVnZXggPSAvIGxpbmUgKFxcZCspLipzY3JpcHQgKD86aW4gKT8oXFxTKykoPzo6IGluIGZ1bmN0aW9uIChcXFMrKSk/JC9pO1xuICAgIHZhciBvcGVyYTExUmVnZXggPSAvIGxpbmUgKFxcZCspLCBjb2x1bW4gKFxcZCspXFxzKig/OmluICg/Ojxhbm9ueW1vdXMgZnVuY3Rpb246IChbXj5dKyk+fChbXlxcKV0rKSlcXCgoLiopXFwpKT8gaW4gKC4qKTpcXHMqJC9pO1xuICAgIHZhciBsaW5lcyA9IHN0YWNrdHJhY2Uuc3BsaXQoJ1xcbicpO1xuICAgIHZhciBzdGFjayA9IFtdO1xuICAgIHZhciBwYXJ0cztcbiAgICBmb3IgKHZhciBsaW5lID0gMDsgbGluZSA8IGxpbmVzLmxlbmd0aDsgbGluZSArPSAyKSB7XG4gICAgICAgIC8vIHRzbGludDpkaXNhYmxlOm5vLWNvbmRpdGlvbmFsLWFzc2lnbm1lbnRcbiAgICAgICAgdmFyIGVsZW1lbnQgPSBudWxsO1xuICAgICAgICBpZiAoKHBhcnRzID0gb3BlcmExMFJlZ2V4LmV4ZWMobGluZXNbbGluZV0pKSkge1xuICAgICAgICAgICAgZWxlbWVudCA9IHtcbiAgICAgICAgICAgICAgICB1cmw6IHBhcnRzWzJdLFxuICAgICAgICAgICAgICAgIGZ1bmM6IHBhcnRzWzNdLFxuICAgICAgICAgICAgICAgIGFyZ3M6IFtdLFxuICAgICAgICAgICAgICAgIGxpbmU6ICtwYXJ0c1sxXSxcbiAgICAgICAgICAgICAgICBjb2x1bW46IG51bGwsXG4gICAgICAgICAgICB9O1xuICAgICAgICB9XG4gICAgICAgIGVsc2UgaWYgKChwYXJ0cyA9IG9wZXJhMTFSZWdleC5leGVjKGxpbmVzW2xpbmVdKSkpIHtcbiAgICAgICAgICAgIGVsZW1lbnQgPSB7XG4gICAgICAgICAgICAgICAgdXJsOiBwYXJ0c1s2XSxcbiAgICAgICAgICAgICAgICBmdW5jOiBwYXJ0c1szXSB8fCBwYXJ0c1s0XSxcbiAgICAgICAgICAgICAgICBhcmdzOiBwYXJ0c1s1XSA/IHBhcnRzWzVdLnNwbGl0KCcsJykgOiBbXSxcbiAgICAgICAgICAgICAgICBsaW5lOiArcGFydHNbMV0sXG4gICAgICAgICAgICAgICAgY29sdW1uOiArcGFydHNbMl0sXG4gICAgICAgICAgICB9O1xuICAgICAgICB9XG4gICAgICAgIGlmIChlbGVtZW50KSB7XG4gICAgICAgICAgICBpZiAoIWVsZW1lbnQuZnVuYyAmJiBlbGVtZW50LmxpbmUpIHtcbiAgICAgICAgICAgICAgICBlbGVtZW50LmZ1bmMgPSBVTktOT1dOX0ZVTkNUSU9OO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgc3RhY2sucHVzaChlbGVtZW50KTtcbiAgICAgICAgfVxuICAgIH1cbiAgICBpZiAoIXN0YWNrLmxlbmd0aCkge1xuICAgICAgICByZXR1cm4gbnVsbDtcbiAgICB9XG4gICAgcmV0dXJuIHtcbiAgICAgICAgbWVzc2FnZTogZXh0cmFjdE1lc3NhZ2UoZXgpLFxuICAgICAgICBuYW1lOiBleC5uYW1lLFxuICAgICAgICBzdGFjazogc3RhY2ssXG4gICAgfTtcbn1cbi8qKiBSZW1vdmUgTiBudW1iZXIgb2YgZnJhbWVzIGZyb20gdGhlIHN0YWNrICovXG5mdW5jdGlvbiBwb3BGcmFtZXMoc3RhY2t0cmFjZSwgcG9wU2l6ZSkge1xuICAgIHRyeSB7XG4gICAgICAgIHJldHVybiB0c2xpYl8xLl9fYXNzaWduKHt9LCBzdGFja3RyYWNlLCB7IHN0YWNrOiBzdGFja3RyYWNlLnN0YWNrLnNsaWNlKHBvcFNpemUpIH0pO1xuICAgIH1cbiAgICBjYXRjaCAoZSkge1xuICAgICAgICByZXR1cm4gc3RhY2t0cmFjZTtcbiAgICB9XG59XG4vKipcbiAqIFRoZXJlIGFyZSBjYXNlcyB3aGVyZSBzdGFja3RyYWNlLm1lc3NhZ2UgaXMgYW4gRXZlbnQgb2JqZWN0XG4gKiBodHRwczovL2dpdGh1Yi5jb20vZ2V0c2VudHJ5L3NlbnRyeS1qYXZhc2NyaXB0L2lzc3Vlcy8xOTQ5XG4gKiBJbiB0aGlzIHNwZWNpZmljIGNhc2Ugd2UgdHJ5IHRvIGV4dHJhY3Qgc3RhY2t0cmFjZS5tZXNzYWdlLmVycm9yLm1lc3NhZ2VcbiAqL1xuZnVuY3Rpb24gZXh0cmFjdE1lc3NhZ2UoZXgpIHtcbiAgICB2YXIgbWVzc2FnZSA9IGV4ICYmIGV4Lm1lc3NhZ2U7XG4gICAgaWYgKCFtZXNzYWdlKSB7XG4gICAgICAgIHJldHVybiAnTm8gZXJyb3IgbWVzc2FnZSc7XG4gICAgfVxuICAgIGlmIChtZXNzYWdlLmVycm9yICYmIHR5cGVvZiBtZXNzYWdlLmVycm9yLm1lc3NhZ2UgPT09ICdzdHJpbmcnKSB7XG4gICAgICAgIHJldHVybiBtZXNzYWdlLmVycm9yLm1lc3NhZ2U7XG4gICAgfVxuICAgIHJldHVybiBtZXNzYWdlO1xufVxuLy8jIHNvdXJjZU1hcHBpbmdVUkw9dHJhY2VraXQuanMubWFwIiwiaW1wb3J0IHsgQVBJIH0gZnJvbSAnQHNlbnRyeS9jb3JlJztcbmltcG9ydCB7IFByb21pc2VCdWZmZXIsIFNlbnRyeUVycm9yIH0gZnJvbSAnQHNlbnRyeS91dGlscyc7XG4vKiogQmFzZSBUcmFuc3BvcnQgY2xhc3MgaW1wbGVtZW50YXRpb24gKi9cbnZhciBCYXNlVHJhbnNwb3J0ID0gLyoqIEBjbGFzcyAqLyAoZnVuY3Rpb24gKCkge1xuICAgIGZ1bmN0aW9uIEJhc2VUcmFuc3BvcnQob3B0aW9ucykge1xuICAgICAgICB0aGlzLm9wdGlvbnMgPSBvcHRpb25zO1xuICAgICAgICAvKiogQSBzaW1wbGUgYnVmZmVyIGhvbGRpbmcgYWxsIHJlcXVlc3RzLiAqL1xuICAgICAgICB0aGlzLl9idWZmZXIgPSBuZXcgUHJvbWlzZUJ1ZmZlcigzMCk7XG4gICAgICAgIHRoaXMuX2FwaSA9IG5ldyBBUEkodGhpcy5vcHRpb25zLmRzbik7XG4gICAgICAgIC8vIHRzbGludDpkaXNhYmxlLW5leHQtbGluZTpkZXByZWNhdGlvblxuICAgICAgICB0aGlzLnVybCA9IHRoaXMuX2FwaS5nZXRTdG9yZUVuZHBvaW50V2l0aFVybEVuY29kZWRBdXRoKCk7XG4gICAgfVxuICAgIC8qKlxuICAgICAqIEBpbmhlcml0RG9jXG4gICAgICovXG4gICAgQmFzZVRyYW5zcG9ydC5wcm90b3R5cGUuc2VuZEV2ZW50ID0gZnVuY3Rpb24gKF8pIHtcbiAgICAgICAgdGhyb3cgbmV3IFNlbnRyeUVycm9yKCdUcmFuc3BvcnQgQ2xhc3MgaGFzIHRvIGltcGxlbWVudCBgc2VuZEV2ZW50YCBtZXRob2QnKTtcbiAgICB9O1xuICAgIC8qKlxuICAgICAqIEBpbmhlcml0RG9jXG4gICAgICovXG4gICAgQmFzZVRyYW5zcG9ydC5wcm90b3R5cGUuY2xvc2UgPSBmdW5jdGlvbiAodGltZW91dCkge1xuICAgICAgICByZXR1cm4gdGhpcy5fYnVmZmVyLmRyYWluKHRpbWVvdXQpO1xuICAgIH07XG4gICAgcmV0dXJuIEJhc2VUcmFuc3BvcnQ7XG59KCkpO1xuZXhwb3J0IHsgQmFzZVRyYW5zcG9ydCB9O1xuLy8jIHNvdXJjZU1hcHBpbmdVUkw9YmFzZS5qcy5tYXAiLCJpbXBvcnQgKiBhcyB0c2xpYl8xIGZyb20gXCJ0c2xpYlwiO1xuaW1wb3J0IHsgZXZlbnRUb1NlbnRyeVJlcXVlc3QgfSBmcm9tICdAc2VudHJ5L2NvcmUnO1xuaW1wb3J0IHsgU3RhdHVzIH0gZnJvbSAnQHNlbnRyeS90eXBlcyc7XG5pbXBvcnQgeyBnZXRHbG9iYWxPYmplY3QsIGxvZ2dlciwgcGFyc2VSZXRyeUFmdGVySGVhZGVyLCBzdXBwb3J0c1JlZmVycmVyUG9saWN5LCBTeW5jUHJvbWlzZSB9IGZyb20gJ0BzZW50cnkvdXRpbHMnO1xuaW1wb3J0IHsgQmFzZVRyYW5zcG9ydCB9IGZyb20gJy4vYmFzZSc7XG52YXIgZ2xvYmFsID0gZ2V0R2xvYmFsT2JqZWN0KCk7XG4vKiogYGZldGNoYCBiYXNlZCB0cmFuc3BvcnQgKi9cbnZhciBGZXRjaFRyYW5zcG9ydCA9IC8qKiBAY2xhc3MgKi8gKGZ1bmN0aW9uIChfc3VwZXIpIHtcbiAgICB0c2xpYl8xLl9fZXh0ZW5kcyhGZXRjaFRyYW5zcG9ydCwgX3N1cGVyKTtcbiAgICBmdW5jdGlvbiBGZXRjaFRyYW5zcG9ydCgpIHtcbiAgICAgICAgdmFyIF90aGlzID0gX3N1cGVyICE9PSBudWxsICYmIF9zdXBlci5hcHBseSh0aGlzLCBhcmd1bWVudHMpIHx8IHRoaXM7XG4gICAgICAgIC8qKiBMb2NrcyB0cmFuc3BvcnQgYWZ0ZXIgcmVjZWl2aW5nIDQyOSByZXNwb25zZSAqL1xuICAgICAgICBfdGhpcy5fZGlzYWJsZWRVbnRpbCA9IG5ldyBEYXRlKERhdGUubm93KCkpO1xuICAgICAgICByZXR1cm4gX3RoaXM7XG4gICAgfVxuICAgIC8qKlxuICAgICAqIEBpbmhlcml0RG9jXG4gICAgICovXG4gICAgRmV0Y2hUcmFuc3BvcnQucHJvdG90eXBlLnNlbmRFdmVudCA9IGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgICB2YXIgX3RoaXMgPSB0aGlzO1xuICAgICAgICBpZiAobmV3IERhdGUoRGF0ZS5ub3coKSkgPCB0aGlzLl9kaXNhYmxlZFVudGlsKSB7XG4gICAgICAgICAgICByZXR1cm4gUHJvbWlzZS5yZWplY3Qoe1xuICAgICAgICAgICAgICAgIGV2ZW50OiBldmVudCxcbiAgICAgICAgICAgICAgICByZWFzb246IFwiVHJhbnNwb3J0IGxvY2tlZCB0aWxsIFwiICsgdGhpcy5fZGlzYWJsZWRVbnRpbCArIFwiIGR1ZSB0byB0b28gbWFueSByZXF1ZXN0cy5cIixcbiAgICAgICAgICAgICAgICBzdGF0dXM6IDQyOSxcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgICAgIHZhciBzZW50cnlSZXEgPSBldmVudFRvU2VudHJ5UmVxdWVzdChldmVudCwgdGhpcy5fYXBpKTtcbiAgICAgICAgdmFyIG9wdGlvbnMgPSB7XG4gICAgICAgICAgICBib2R5OiBzZW50cnlSZXEuYm9keSxcbiAgICAgICAgICAgIG1ldGhvZDogJ1BPU1QnLFxuICAgICAgICAgICAgLy8gRGVzcGl0ZSBhbGwgc3RhcnMgaW4gdGhlIHNreSBzYXlpbmcgdGhhdCBFZGdlIHN1cHBvcnRzIG9sZCBkcmFmdCBzeW50YXgsIGFrYSAnbmV2ZXInLCAnYWx3YXlzJywgJ29yaWdpbicgYW5kICdkZWZhdWx0XG4gICAgICAgICAgICAvLyBodHRwczovL2Nhbml1c2UuY29tLyNmZWF0PXJlZmVycmVyLXBvbGljeVxuICAgICAgICAgICAgLy8gSXQgZG9lc24ndC4gQW5kIGl0IHRocm93IGV4Y2VwdGlvbiBpbnN0ZWFkIG9mIGlnbm9yaW5nIHRoaXMgcGFyYW1ldGVyLi4uXG4gICAgICAgICAgICAvLyBSRUY6IGh0dHBzOi8vZ2l0aHViLmNvbS9nZXRzZW50cnkvcmF2ZW4tanMvaXNzdWVzLzEyMzNcbiAgICAgICAgICAgIHJlZmVycmVyUG9saWN5OiAoc3VwcG9ydHNSZWZlcnJlclBvbGljeSgpID8gJ29yaWdpbicgOiAnJyksXG4gICAgICAgIH07XG4gICAgICAgIGlmICh0aGlzLm9wdGlvbnMuZmV0Y2hQYXJhbWV0ZXJzICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIE9iamVjdC5hc3NpZ24ob3B0aW9ucywgdGhpcy5vcHRpb25zLmZldGNoUGFyYW1ldGVycyk7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHRoaXMub3B0aW9ucy5oZWFkZXJzICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIG9wdGlvbnMuaGVhZGVycyA9IHRoaXMub3B0aW9ucy5oZWFkZXJzO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiB0aGlzLl9idWZmZXIuYWRkKG5ldyBTeW5jUHJvbWlzZShmdW5jdGlvbiAocmVzb2x2ZSwgcmVqZWN0KSB7XG4gICAgICAgICAgICBnbG9iYWxcbiAgICAgICAgICAgICAgICAuZmV0Y2goc2VudHJ5UmVxLnVybCwgb3B0aW9ucylcbiAgICAgICAgICAgICAgICAudGhlbihmdW5jdGlvbiAocmVzcG9uc2UpIHtcbiAgICAgICAgICAgICAgICB2YXIgc3RhdHVzID0gU3RhdHVzLmZyb21IdHRwQ29kZShyZXNwb25zZS5zdGF0dXMpO1xuICAgICAgICAgICAgICAgIGlmIChzdGF0dXMgPT09IFN0YXR1cy5TdWNjZXNzKSB7XG4gICAgICAgICAgICAgICAgICAgIHJlc29sdmUoeyBzdGF0dXM6IHN0YXR1cyB9KTtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBpZiAoc3RhdHVzID09PSBTdGF0dXMuUmF0ZUxpbWl0KSB7XG4gICAgICAgICAgICAgICAgICAgIHZhciBub3cgPSBEYXRlLm5vdygpO1xuICAgICAgICAgICAgICAgICAgICBfdGhpcy5fZGlzYWJsZWRVbnRpbCA9IG5ldyBEYXRlKG5vdyArIHBhcnNlUmV0cnlBZnRlckhlYWRlcihub3csIHJlc3BvbnNlLmhlYWRlcnMuZ2V0KCdSZXRyeS1BZnRlcicpKSk7XG4gICAgICAgICAgICAgICAgICAgIGxvZ2dlci53YXJuKFwiVG9vIG1hbnkgcmVxdWVzdHMsIGJhY2tpbmcgb2ZmIHRpbGw6IFwiICsgX3RoaXMuX2Rpc2FibGVkVW50aWwpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICByZWplY3QocmVzcG9uc2UpO1xuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAuY2F0Y2gocmVqZWN0KTtcbiAgICAgICAgfSkpO1xuICAgIH07XG4gICAgcmV0dXJuIEZldGNoVHJhbnNwb3J0O1xufShCYXNlVHJhbnNwb3J0KSk7XG5leHBvcnQgeyBGZXRjaFRyYW5zcG9ydCB9O1xuLy8jIHNvdXJjZU1hcHBpbmdVUkw9ZmV0Y2guanMubWFwIiwiZXhwb3J0IHsgQmFzZVRyYW5zcG9ydCB9IGZyb20gJy4vYmFzZSc7XG5leHBvcnQgeyBGZXRjaFRyYW5zcG9ydCB9IGZyb20gJy4vZmV0Y2gnO1xuZXhwb3J0IHsgWEhSVHJhbnNwb3J0IH0gZnJvbSAnLi94aHInO1xuLy8jIHNvdXJjZU1hcHBpbmdVUkw9aW5kZXguanMubWFwIiwiaW1wb3J0ICogYXMgdHNsaWJfMSBmcm9tIFwidHNsaWJcIjtcbmltcG9ydCB7IGV2ZW50VG9TZW50cnlSZXF1ZXN0IH0gZnJvbSAnQHNlbnRyeS9jb3JlJztcbmltcG9ydCB7IFN0YXR1cyB9IGZyb20gJ0BzZW50cnkvdHlwZXMnO1xuaW1wb3J0IHsgbG9nZ2VyLCBwYXJzZVJldHJ5QWZ0ZXJIZWFkZXIsIFN5bmNQcm9taXNlIH0gZnJvbSAnQHNlbnRyeS91dGlscyc7XG5pbXBvcnQgeyBCYXNlVHJhbnNwb3J0IH0gZnJvbSAnLi9iYXNlJztcbi8qKiBgWEhSYCBiYXNlZCB0cmFuc3BvcnQgKi9cbnZhciBYSFJUcmFuc3BvcnQgPSAvKiogQGNsYXNzICovIChmdW5jdGlvbiAoX3N1cGVyKSB7XG4gICAgdHNsaWJfMS5fX2V4dGVuZHMoWEhSVHJhbnNwb3J0LCBfc3VwZXIpO1xuICAgIGZ1bmN0aW9uIFhIUlRyYW5zcG9ydCgpIHtcbiAgICAgICAgdmFyIF90aGlzID0gX3N1cGVyICE9PSBudWxsICYmIF9zdXBlci5hcHBseSh0aGlzLCBhcmd1bWVudHMpIHx8IHRoaXM7XG4gICAgICAgIC8qKiBMb2NrcyB0cmFuc3BvcnQgYWZ0ZXIgcmVjZWl2aW5nIDQyOSByZXNwb25zZSAqL1xuICAgICAgICBfdGhpcy5fZGlzYWJsZWRVbnRpbCA9IG5ldyBEYXRlKERhdGUubm93KCkpO1xuICAgICAgICByZXR1cm4gX3RoaXM7XG4gICAgfVxuICAgIC8qKlxuICAgICAqIEBpbmhlcml0RG9jXG4gICAgICovXG4gICAgWEhSVHJhbnNwb3J0LnByb3RvdHlwZS5zZW5kRXZlbnQgPSBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgICAgdmFyIF90aGlzID0gdGhpcztcbiAgICAgICAgaWYgKG5ldyBEYXRlKERhdGUubm93KCkpIDwgdGhpcy5fZGlzYWJsZWRVbnRpbCkge1xuICAgICAgICAgICAgcmV0dXJuIFByb21pc2UucmVqZWN0KHtcbiAgICAgICAgICAgICAgICBldmVudDogZXZlbnQsXG4gICAgICAgICAgICAgICAgcmVhc29uOiBcIlRyYW5zcG9ydCBsb2NrZWQgdGlsbCBcIiArIHRoaXMuX2Rpc2FibGVkVW50aWwgKyBcIiBkdWUgdG8gdG9vIG1hbnkgcmVxdWVzdHMuXCIsXG4gICAgICAgICAgICAgICAgc3RhdHVzOiA0MjksXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgICAgICB2YXIgc2VudHJ5UmVxID0gZXZlbnRUb1NlbnRyeVJlcXVlc3QoZXZlbnQsIHRoaXMuX2FwaSk7XG4gICAgICAgIHJldHVybiB0aGlzLl9idWZmZXIuYWRkKG5ldyBTeW5jUHJvbWlzZShmdW5jdGlvbiAocmVzb2x2ZSwgcmVqZWN0KSB7XG4gICAgICAgICAgICB2YXIgcmVxdWVzdCA9IG5ldyBYTUxIdHRwUmVxdWVzdCgpO1xuICAgICAgICAgICAgcmVxdWVzdC5vbnJlYWR5c3RhdGVjaGFuZ2UgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgaWYgKHJlcXVlc3QucmVhZHlTdGF0ZSAhPT0gNCkge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIHZhciBzdGF0dXMgPSBTdGF0dXMuZnJvbUh0dHBDb2RlKHJlcXVlc3Quc3RhdHVzKTtcbiAgICAgICAgICAgICAgICBpZiAoc3RhdHVzID09PSBTdGF0dXMuU3VjY2Vzcykge1xuICAgICAgICAgICAgICAgICAgICByZXNvbHZlKHsgc3RhdHVzOiBzdGF0dXMgfSk7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgaWYgKHN0YXR1cyA9PT0gU3RhdHVzLlJhdGVMaW1pdCkge1xuICAgICAgICAgICAgICAgICAgICB2YXIgbm93ID0gRGF0ZS5ub3coKTtcbiAgICAgICAgICAgICAgICAgICAgX3RoaXMuX2Rpc2FibGVkVW50aWwgPSBuZXcgRGF0ZShub3cgKyBwYXJzZVJldHJ5QWZ0ZXJIZWFkZXIobm93LCByZXF1ZXN0LmdldFJlc3BvbnNlSGVhZGVyKCdSZXRyeS1BZnRlcicpKSk7XG4gICAgICAgICAgICAgICAgICAgIGxvZ2dlci53YXJuKFwiVG9vIG1hbnkgcmVxdWVzdHMsIGJhY2tpbmcgb2ZmIHRpbGw6IFwiICsgX3RoaXMuX2Rpc2FibGVkVW50aWwpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICByZWplY3QocmVxdWVzdCk7XG4gICAgICAgICAgICB9O1xuICAgICAgICAgICAgcmVxdWVzdC5vcGVuKCdQT1NUJywgc2VudHJ5UmVxLnVybCk7XG4gICAgICAgICAgICBmb3IgKHZhciBoZWFkZXIgaW4gX3RoaXMub3B0aW9ucy5oZWFkZXJzKSB7XG4gICAgICAgICAgICAgICAgaWYgKF90aGlzLm9wdGlvbnMuaGVhZGVycy5oYXNPd25Qcm9wZXJ0eShoZWFkZXIpKSB7XG4gICAgICAgICAgICAgICAgICAgIHJlcXVlc3Quc2V0UmVxdWVzdEhlYWRlcihoZWFkZXIsIF90aGlzLm9wdGlvbnMuaGVhZGVyc1toZWFkZXJdKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXF1ZXN0LnNlbmQoc2VudHJ5UmVxLmJvZHkpO1xuICAgICAgICB9KSk7XG4gICAgfTtcbiAgICByZXR1cm4gWEhSVHJhbnNwb3J0O1xufShCYXNlVHJhbnNwb3J0KSk7XG5leHBvcnQgeyBYSFJUcmFuc3BvcnQgfTtcbi8vIyBzb3VyY2VNYXBwaW5nVVJMPXhoci5qcy5tYXAiLCJleHBvcnQgdmFyIFNES19OQU1FID0gJ3NlbnRyeS5qYXZhc2NyaXB0LmJyb3dzZXInO1xuZXhwb3J0IHZhciBTREtfVkVSU0lPTiA9ICc1LjE4LjAnO1xuLy8jIHNvdXJjZU1hcHBpbmdVUkw9dmVyc2lvbi5qcy5tYXAiLCJpbXBvcnQgeyBEc24sIHVybEVuY29kZSB9IGZyb20gJ0BzZW50cnkvdXRpbHMnO1xudmFyIFNFTlRSWV9BUElfVkVSU0lPTiA9ICc3Jztcbi8qKiBIZWxwZXIgY2xhc3MgdG8gcHJvdmlkZSB1cmxzIHRvIGRpZmZlcmVudCBTZW50cnkgZW5kcG9pbnRzLiAqL1xudmFyIEFQSSA9IC8qKiBAY2xhc3MgKi8gKGZ1bmN0aW9uICgpIHtcbiAgICAvKiogQ3JlYXRlIGEgbmV3IGluc3RhbmNlIG9mIEFQSSAqL1xuICAgIGZ1bmN0aW9uIEFQSShkc24pIHtcbiAgICAgICAgdGhpcy5kc24gPSBkc247XG4gICAgICAgIHRoaXMuX2Rzbk9iamVjdCA9IG5ldyBEc24oZHNuKTtcbiAgICB9XG4gICAgLyoqIFJldHVybnMgdGhlIERzbiBvYmplY3QuICovXG4gICAgQVBJLnByb3RvdHlwZS5nZXREc24gPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLl9kc25PYmplY3Q7XG4gICAgfTtcbiAgICAvKiogUmV0dXJucyB0aGUgcHJlZml4IHRvIGNvbnN0cnVjdCBTZW50cnkgaW5nZXN0aW9uIEFQSSBlbmRwb2ludHMuICovXG4gICAgQVBJLnByb3RvdHlwZS5nZXRCYXNlQXBpRW5kcG9pbnQgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciBkc24gPSB0aGlzLl9kc25PYmplY3Q7XG4gICAgICAgIHZhciBwcm90b2NvbCA9IGRzbi5wcm90b2NvbCA/IGRzbi5wcm90b2NvbCArIFwiOlwiIDogJyc7XG4gICAgICAgIHZhciBwb3J0ID0gZHNuLnBvcnQgPyBcIjpcIiArIGRzbi5wb3J0IDogJyc7XG4gICAgICAgIHJldHVybiBwcm90b2NvbCArIFwiLy9cIiArIGRzbi5ob3N0ICsgcG9ydCArIChkc24ucGF0aCA/IFwiL1wiICsgZHNuLnBhdGggOiAnJykgKyBcIi9hcGkvXCI7XG4gICAgfTtcbiAgICAvKiogUmV0dXJucyB0aGUgc3RvcmUgZW5kcG9pbnQgVVJMLiAqL1xuICAgIEFQSS5wcm90b3R5cGUuZ2V0U3RvcmVFbmRwb2ludCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX2dldEluZ2VzdEVuZHBvaW50KCdzdG9yZScpO1xuICAgIH07XG4gICAgLyoqIFJldHVybnMgdGhlIGVudmVsb3BlIGVuZHBvaW50IFVSTC4gKi9cbiAgICBBUEkucHJvdG90eXBlLl9nZXRFbnZlbG9wZUVuZHBvaW50ID0gZnVuY3Rpb24gKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5fZ2V0SW5nZXN0RW5kcG9pbnQoJ2VudmVsb3BlJyk7XG4gICAgfTtcbiAgICAvKiogUmV0dXJucyB0aGUgaW5nZXN0IEFQSSBlbmRwb2ludCBmb3IgdGFyZ2V0LiAqL1xuICAgIEFQSS5wcm90b3R5cGUuX2dldEluZ2VzdEVuZHBvaW50ID0gZnVuY3Rpb24gKHRhcmdldCkge1xuICAgICAgICB2YXIgYmFzZSA9IHRoaXMuZ2V0QmFzZUFwaUVuZHBvaW50KCk7XG4gICAgICAgIHZhciBkc24gPSB0aGlzLl9kc25PYmplY3Q7XG4gICAgICAgIHJldHVybiBcIlwiICsgYmFzZSArIGRzbi5wcm9qZWN0SWQgKyBcIi9cIiArIHRhcmdldCArIFwiL1wiO1xuICAgIH07XG4gICAgLyoqXG4gICAgICogUmV0dXJucyB0aGUgc3RvcmUgZW5kcG9pbnQgVVJMIHdpdGggYXV0aCBpbiB0aGUgcXVlcnkgc3RyaW5nLlxuICAgICAqXG4gICAgICogU2VuZGluZyBhdXRoIGFzIHBhcnQgb2YgdGhlIHF1ZXJ5IHN0cmluZyBhbmQgbm90IGFzIGN1c3RvbSBIVFRQIGhlYWRlcnMgYXZvaWRzIENPUlMgcHJlZmxpZ2h0IHJlcXVlc3RzLlxuICAgICAqL1xuICAgIEFQSS5wcm90b3R5cGUuZ2V0U3RvcmVFbmRwb2ludFdpdGhVcmxFbmNvZGVkQXV0aCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuZ2V0U3RvcmVFbmRwb2ludCgpICsgXCI/XCIgKyB0aGlzLl9lbmNvZGVkQXV0aCgpO1xuICAgIH07XG4gICAgLyoqXG4gICAgICogUmV0dXJucyB0aGUgZW52ZWxvcGUgZW5kcG9pbnQgVVJMIHdpdGggYXV0aCBpbiB0aGUgcXVlcnkgc3RyaW5nLlxuICAgICAqXG4gICAgICogU2VuZGluZyBhdXRoIGFzIHBhcnQgb2YgdGhlIHF1ZXJ5IHN0cmluZyBhbmQgbm90IGFzIGN1c3RvbSBIVFRQIGhlYWRlcnMgYXZvaWRzIENPUlMgcHJlZmxpZ2h0IHJlcXVlc3RzLlxuICAgICAqL1xuICAgIEFQSS5wcm90b3R5cGUuZ2V0RW52ZWxvcGVFbmRwb2ludFdpdGhVcmxFbmNvZGVkQXV0aCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX2dldEVudmVsb3BlRW5kcG9pbnQoKSArIFwiP1wiICsgdGhpcy5fZW5jb2RlZEF1dGgoKTtcbiAgICB9O1xuICAgIC8qKiBSZXR1cm5zIGEgVVJMLWVuY29kZWQgc3RyaW5nIHdpdGggYXV0aCBjb25maWcgc3VpdGFibGUgZm9yIGEgcXVlcnkgc3RyaW5nLiAqL1xuICAgIEFQSS5wcm90b3R5cGUuX2VuY29kZWRBdXRoID0gZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXIgZHNuID0gdGhpcy5fZHNuT2JqZWN0O1xuICAgICAgICB2YXIgYXV0aCA9IHtcbiAgICAgICAgICAgIC8vIFdlIHNlbmQgb25seSB0aGUgbWluaW11bSBzZXQgb2YgcmVxdWlyZWQgaW5mb3JtYXRpb24uIFNlZVxuICAgICAgICAgICAgLy8gaHR0cHM6Ly9naXRodWIuY29tL2dldHNlbnRyeS9zZW50cnktamF2YXNjcmlwdC9pc3N1ZXMvMjU3Mi5cbiAgICAgICAgICAgIHNlbnRyeV9rZXk6IGRzbi51c2VyLFxuICAgICAgICAgICAgc2VudHJ5X3ZlcnNpb246IFNFTlRSWV9BUElfVkVSU0lPTixcbiAgICAgICAgfTtcbiAgICAgICAgcmV0dXJuIHVybEVuY29kZShhdXRoKTtcbiAgICB9O1xuICAgIC8qKiBSZXR1cm5zIG9ubHkgdGhlIHBhdGggY29tcG9uZW50IGZvciB0aGUgc3RvcmUgZW5kcG9pbnQuICovXG4gICAgQVBJLnByb3RvdHlwZS5nZXRTdG9yZUVuZHBvaW50UGF0aCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdmFyIGRzbiA9IHRoaXMuX2Rzbk9iamVjdDtcbiAgICAgICAgcmV0dXJuIChkc24ucGF0aCA/IFwiL1wiICsgZHNuLnBhdGggOiAnJykgKyBcIi9hcGkvXCIgKyBkc24ucHJvamVjdElkICsgXCIvc3RvcmUvXCI7XG4gICAgfTtcbiAgICAvKipcbiAgICAgKiBSZXR1cm5zIGFuIG9iamVjdCB0aGF0IGNhbiBiZSB1c2VkIGluIHJlcXVlc3QgaGVhZGVycy5cbiAgICAgKiBUaGlzIGlzIG5lZWRlZCBmb3Igbm9kZSBhbmQgdGhlIG9sZCAvc3RvcmUgZW5kcG9pbnQgaW4gc2VudHJ5XG4gICAgICovXG4gICAgQVBJLnByb3RvdHlwZS5nZXRSZXF1ZXN0SGVhZGVycyA9IGZ1bmN0aW9uIChjbGllbnROYW1lLCBjbGllbnRWZXJzaW9uKSB7XG4gICAgICAgIHZhciBkc24gPSB0aGlzLl9kc25PYmplY3Q7XG4gICAgICAgIHZhciBoZWFkZXIgPSBbXCJTZW50cnkgc2VudHJ5X3ZlcnNpb249XCIgKyBTRU5UUllfQVBJX1ZFUlNJT05dO1xuICAgICAgICBoZWFkZXIucHVzaChcInNlbnRyeV9jbGllbnQ9XCIgKyBjbGllbnROYW1lICsgXCIvXCIgKyBjbGllbnRWZXJzaW9uKTtcbiAgICAgICAgaGVhZGVyLnB1c2goXCJzZW50cnlfa2V5PVwiICsgZHNuLnVzZXIpO1xuICAgICAgICBpZiAoZHNuLnBhc3MpIHtcbiAgICAgICAgICAgIGhlYWRlci5wdXNoKFwic2VudHJ5X3NlY3JldD1cIiArIGRzbi5wYXNzKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgJ0NvbnRlbnQtVHlwZSc6ICdhcHBsaWNhdGlvbi9qc29uJyxcbiAgICAgICAgICAgICdYLVNlbnRyeS1BdXRoJzogaGVhZGVyLmpvaW4oJywgJyksXG4gICAgICAgIH07XG4gICAgfTtcbiAgICAvKiogUmV0dXJucyB0aGUgdXJsIHRvIHRoZSByZXBvcnQgZGlhbG9nIGVuZHBvaW50LiAqL1xuICAgIEFQSS5wcm90b3R5cGUuZ2V0UmVwb3J0RGlhbG9nRW5kcG9pbnQgPSBmdW5jdGlvbiAoZGlhbG9nT3B0aW9ucykge1xuICAgICAgICBpZiAoZGlhbG9nT3B0aW9ucyA9PT0gdm9pZCAwKSB7IGRpYWxvZ09wdGlvbnMgPSB7fTsgfVxuICAgICAgICB2YXIgZHNuID0gdGhpcy5fZHNuT2JqZWN0O1xuICAgICAgICB2YXIgZW5kcG9pbnQgPSB0aGlzLmdldEJhc2VBcGlFbmRwb2ludCgpICsgXCJlbWJlZC9lcnJvci1wYWdlL1wiO1xuICAgICAgICB2YXIgZW5jb2RlZE9wdGlvbnMgPSBbXTtcbiAgICAgICAgZW5jb2RlZE9wdGlvbnMucHVzaChcImRzbj1cIiArIGRzbi50b1N0cmluZygpKTtcbiAgICAgICAgZm9yICh2YXIga2V5IGluIGRpYWxvZ09wdGlvbnMpIHtcbiAgICAgICAgICAgIGlmIChrZXkgPT09ICd1c2VyJykge1xuICAgICAgICAgICAgICAgIGlmICghZGlhbG9nT3B0aW9ucy51c2VyKSB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnRpbnVlO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBpZiAoZGlhbG9nT3B0aW9ucy51c2VyLm5hbWUpIHtcbiAgICAgICAgICAgICAgICAgICAgZW5jb2RlZE9wdGlvbnMucHVzaChcIm5hbWU9XCIgKyBlbmNvZGVVUklDb21wb25lbnQoZGlhbG9nT3B0aW9ucy51c2VyLm5hbWUpKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgaWYgKGRpYWxvZ09wdGlvbnMudXNlci5lbWFpbCkge1xuICAgICAgICAgICAgICAgICAgICBlbmNvZGVkT3B0aW9ucy5wdXNoKFwiZW1haWw9XCIgKyBlbmNvZGVVUklDb21wb25lbnQoZGlhbG9nT3B0aW9ucy51c2VyLmVtYWlsKSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgZW5jb2RlZE9wdGlvbnMucHVzaChlbmNvZGVVUklDb21wb25lbnQoa2V5KSArIFwiPVwiICsgZW5jb2RlVVJJQ29tcG9uZW50KGRpYWxvZ09wdGlvbnNba2V5XSkpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIGlmIChlbmNvZGVkT3B0aW9ucy5sZW5ndGgpIHtcbiAgICAgICAgICAgIHJldHVybiBlbmRwb2ludCArIFwiP1wiICsgZW5jb2RlZE9wdGlvbnMuam9pbignJicpO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBlbmRwb2ludDtcbiAgICB9O1xuICAgIHJldHVybiBBUEk7XG59KCkpO1xuZXhwb3J0IHsgQVBJIH07XG4vLyMgc291cmNlTWFwcGluZ1VSTD1hcGkuanMubWFwIiwiaW1wb3J0IHsgbG9nZ2VyLCBTZW50cnlFcnJvciB9IGZyb20gJ0BzZW50cnkvdXRpbHMnO1xuaW1wb3J0IHsgTm9vcFRyYW5zcG9ydCB9IGZyb20gJy4vdHJhbnNwb3J0cy9ub29wJztcbi8qKlxuICogVGhpcyBpcyB0aGUgYmFzZSBpbXBsZW1lbnRpb24gb2YgYSBCYWNrZW5kLlxuICogQGhpZGRlblxuICovXG52YXIgQmFzZUJhY2tlbmQgPSAvKiogQGNsYXNzICovIChmdW5jdGlvbiAoKSB7XG4gICAgLyoqIENyZWF0ZXMgYSBuZXcgYmFja2VuZCBpbnN0YW5jZS4gKi9cbiAgICBmdW5jdGlvbiBCYXNlQmFja2VuZChvcHRpb25zKSB7XG4gICAgICAgIHRoaXMuX29wdGlvbnMgPSBvcHRpb25zO1xuICAgICAgICBpZiAoIXRoaXMuX29wdGlvbnMuZHNuKSB7XG4gICAgICAgICAgICBsb2dnZXIud2FybignTm8gRFNOIHByb3ZpZGVkLCBiYWNrZW5kIHdpbGwgbm90IGRvIGFueXRoaW5nLicpO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMuX3RyYW5zcG9ydCA9IHRoaXMuX3NldHVwVHJhbnNwb3J0KCk7XG4gICAgfVxuICAgIC8qKlxuICAgICAqIFNldHMgdXAgdGhlIHRyYW5zcG9ydCBzbyBpdCBjYW4gYmUgdXNlZCBsYXRlciB0byBzZW5kIHJlcXVlc3RzLlxuICAgICAqL1xuICAgIEJhc2VCYWNrZW5kLnByb3RvdHlwZS5fc2V0dXBUcmFuc3BvcnQgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHJldHVybiBuZXcgTm9vcFRyYW5zcG9ydCgpO1xuICAgIH07XG4gICAgLyoqXG4gICAgICogQGluaGVyaXREb2NcbiAgICAgKi9cbiAgICBCYXNlQmFja2VuZC5wcm90b3R5cGUuZXZlbnRGcm9tRXhjZXB0aW9uID0gZnVuY3Rpb24gKF9leGNlcHRpb24sIF9oaW50KSB7XG4gICAgICAgIHRocm93IG5ldyBTZW50cnlFcnJvcignQmFja2VuZCBoYXMgdG8gaW1wbGVtZW50IGBldmVudEZyb21FeGNlcHRpb25gIG1ldGhvZCcpO1xuICAgIH07XG4gICAgLyoqXG4gICAgICogQGluaGVyaXREb2NcbiAgICAgKi9cbiAgICBCYXNlQmFja2VuZC5wcm90b3R5cGUuZXZlbnRGcm9tTWVzc2FnZSA9IGZ1bmN0aW9uIChfbWVzc2FnZSwgX2xldmVsLCBfaGludCkge1xuICAgICAgICB0aHJvdyBuZXcgU2VudHJ5RXJyb3IoJ0JhY2tlbmQgaGFzIHRvIGltcGxlbWVudCBgZXZlbnRGcm9tTWVzc2FnZWAgbWV0aG9kJyk7XG4gICAgfTtcbiAgICAvKipcbiAgICAgKiBAaW5oZXJpdERvY1xuICAgICAqL1xuICAgIEJhc2VCYWNrZW5kLnByb3RvdHlwZS5zZW5kRXZlbnQgPSBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgICAgdGhpcy5fdHJhbnNwb3J0LnNlbmRFdmVudChldmVudCkudGhlbihudWxsLCBmdW5jdGlvbiAocmVhc29uKSB7XG4gICAgICAgICAgICBsb2dnZXIuZXJyb3IoXCJFcnJvciB3aGlsZSBzZW5kaW5nIGV2ZW50OiBcIiArIHJlYXNvbik7XG4gICAgICAgIH0pO1xuICAgIH07XG4gICAgLyoqXG4gICAgICogQGluaGVyaXREb2NcbiAgICAgKi9cbiAgICBCYXNlQmFja2VuZC5wcm90b3R5cGUuZ2V0VHJhbnNwb3J0ID0gZnVuY3Rpb24gKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5fdHJhbnNwb3J0O1xuICAgIH07XG4gICAgcmV0dXJuIEJhc2VCYWNrZW5kO1xufSgpKTtcbmV4cG9ydCB7IEJhc2VCYWNrZW5kIH07XG4vLyMgc291cmNlTWFwcGluZ1VSTD1iYXNlYmFja2VuZC5qcy5tYXAiLCJpbXBvcnQgKiBhcyB0c2xpYl8xIGZyb20gXCJ0c2xpYlwiO1xuaW1wb3J0IHsgU2NvcGUgfSBmcm9tICdAc2VudHJ5L2h1Yic7XG5pbXBvcnQgeyBEc24sIGlzUHJpbWl0aXZlLCBpc1RoZW5hYmxlLCBsb2dnZXIsIG5vcm1hbGl6ZSwgU3luY1Byb21pc2UsIHRpbWVzdGFtcFdpdGhNcywgdHJ1bmNhdGUsIHV1aWQ0LCB9IGZyb20gJ0BzZW50cnkvdXRpbHMnO1xuaW1wb3J0IHsgc2V0dXBJbnRlZ3JhdGlvbnMgfSBmcm9tICcuL2ludGVncmF0aW9uJztcbi8qKlxuICogQmFzZSBpbXBsZW1lbnRhdGlvbiBmb3IgYWxsIEphdmFTY3JpcHQgU0RLIGNsaWVudHMuXG4gKlxuICogQ2FsbCB0aGUgY29uc3RydWN0b3Igd2l0aCB0aGUgY29ycmVzcG9uZGluZyBiYWNrZW5kIGNvbnN0cnVjdG9yIGFuZCBvcHRpb25zXG4gKiBzcGVjaWZpYyB0byB0aGUgY2xpZW50IHN1YmNsYXNzLiBUbyBhY2Nlc3MgdGhlc2Ugb3B0aW9ucyBsYXRlciwgdXNlXG4gKiB7QGxpbmsgQ2xpZW50LmdldE9wdGlvbnN9LiBBbHNvLCB0aGUgQmFja2VuZCBpbnN0YW5jZSBpcyBhdmFpbGFibGUgdmlhXG4gKiB7QGxpbmsgQ2xpZW50LmdldEJhY2tlbmR9LlxuICpcbiAqIElmIGEgRHNuIGlzIHNwZWNpZmllZCBpbiB0aGUgb3B0aW9ucywgaXQgd2lsbCBiZSBwYXJzZWQgYW5kIHN0b3JlZC4gVXNlXG4gKiB7QGxpbmsgQ2xpZW50LmdldERzbn0gdG8gcmV0cmlldmUgdGhlIERzbiBhdCBhbnkgbW9tZW50LiBJbiBjYXNlIHRoZSBEc24gaXNcbiAqIGludmFsaWQsIHRoZSBjb25zdHJ1Y3RvciB3aWxsIHRocm93IGEge0BsaW5rIFNlbnRyeUV4Y2VwdGlvbn0uIE5vdGUgdGhhdFxuICogd2l0aG91dCBhIHZhbGlkIERzbiwgdGhlIFNESyB3aWxsIG5vdCBzZW5kIGFueSBldmVudHMgdG8gU2VudHJ5LlxuICpcbiAqIEJlZm9yZSBzZW5kaW5nIGFuIGV2ZW50IHZpYSB0aGUgYmFja2VuZCwgaXQgaXMgcGFzc2VkIHRocm91Z2hcbiAqIHtAbGluayBCYXNlQ2xpZW50LnByZXBhcmVFdmVudH0gdG8gYWRkIFNESyBpbmZvcm1hdGlvbiBhbmQgc2NvcGUgZGF0YVxuICogKGJyZWFkY3J1bWJzIGFuZCBjb250ZXh0KS4gVG8gYWRkIG1vcmUgY3VzdG9tIGluZm9ybWF0aW9uLCBvdmVycmlkZSB0aGlzXG4gKiBtZXRob2QgYW5kIGV4dGVuZCB0aGUgcmVzdWx0aW5nIHByZXBhcmVkIGV2ZW50LlxuICpcbiAqIFRvIGlzc3VlIGF1dG9tYXRpY2FsbHkgY3JlYXRlZCBldmVudHMgKGUuZy4gdmlhIGluc3RydW1lbnRhdGlvbiksIHVzZVxuICoge0BsaW5rIENsaWVudC5jYXB0dXJlRXZlbnR9LiBJdCB3aWxsIHByZXBhcmUgdGhlIGV2ZW50IGFuZCBwYXNzIGl0IHRocm91Z2hcbiAqIHRoZSBjYWxsYmFjayBsaWZlY3ljbGUuIFRvIGlzc3VlIGF1dG8tYnJlYWRjcnVtYnMsIHVzZVxuICoge0BsaW5rIENsaWVudC5hZGRCcmVhZGNydW1ifS5cbiAqXG4gKiBAZXhhbXBsZVxuICogY2xhc3MgTm9kZUNsaWVudCBleHRlbmRzIEJhc2VDbGllbnQ8Tm9kZUJhY2tlbmQsIE5vZGVPcHRpb25zPiB7XG4gKiAgIHB1YmxpYyBjb25zdHJ1Y3RvcihvcHRpb25zOiBOb2RlT3B0aW9ucykge1xuICogICAgIHN1cGVyKE5vZGVCYWNrZW5kLCBvcHRpb25zKTtcbiAqICAgfVxuICpcbiAqICAgLy8gLi4uXG4gKiB9XG4gKi9cbnZhciBCYXNlQ2xpZW50ID0gLyoqIEBjbGFzcyAqLyAoZnVuY3Rpb24gKCkge1xuICAgIC8qKlxuICAgICAqIEluaXRpYWxpemVzIHRoaXMgY2xpZW50IGluc3RhbmNlLlxuICAgICAqXG4gICAgICogQHBhcmFtIGJhY2tlbmRDbGFzcyBBIGNvbnN0cnVjdG9yIGZ1bmN0aW9uIHRvIGNyZWF0ZSB0aGUgYmFja2VuZC5cbiAgICAgKiBAcGFyYW0gb3B0aW9ucyBPcHRpb25zIGZvciB0aGUgY2xpZW50LlxuICAgICAqL1xuICAgIGZ1bmN0aW9uIEJhc2VDbGllbnQoYmFja2VuZENsYXNzLCBvcHRpb25zKSB7XG4gICAgICAgIC8qKiBBcnJheSBvZiB1c2VkIGludGVncmF0aW9ucy4gKi9cbiAgICAgICAgdGhpcy5faW50ZWdyYXRpb25zID0ge307XG4gICAgICAgIC8qKiBJcyB0aGUgY2xpZW50IHN0aWxsIHByb2Nlc3NpbmcgYSBjYWxsPyAqL1xuICAgICAgICB0aGlzLl9wcm9jZXNzaW5nID0gZmFsc2U7XG4gICAgICAgIHRoaXMuX2JhY2tlbmQgPSBuZXcgYmFja2VuZENsYXNzKG9wdGlvbnMpO1xuICAgICAgICB0aGlzLl9vcHRpb25zID0gb3B0aW9ucztcbiAgICAgICAgaWYgKG9wdGlvbnMuZHNuKSB7XG4gICAgICAgICAgICB0aGlzLl9kc24gPSBuZXcgRHNuKG9wdGlvbnMuZHNuKTtcbiAgICAgICAgfVxuICAgIH1cbiAgICAvKipcbiAgICAgKiBAaW5oZXJpdERvY1xuICAgICAqL1xuICAgIEJhc2VDbGllbnQucHJvdG90eXBlLmNhcHR1cmVFeGNlcHRpb24gPSBmdW5jdGlvbiAoZXhjZXB0aW9uLCBoaW50LCBzY29wZSkge1xuICAgICAgICB2YXIgX3RoaXMgPSB0aGlzO1xuICAgICAgICB2YXIgZXZlbnRJZCA9IGhpbnQgJiYgaGludC5ldmVudF9pZDtcbiAgICAgICAgdGhpcy5fcHJvY2Vzc2luZyA9IHRydWU7XG4gICAgICAgIHRoaXMuX2dldEJhY2tlbmQoKVxuICAgICAgICAgICAgLmV2ZW50RnJvbUV4Y2VwdGlvbihleGNlcHRpb24sIGhpbnQpXG4gICAgICAgICAgICAudGhlbihmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgICAgICAgIGV2ZW50SWQgPSBfdGhpcy5jYXB0dXJlRXZlbnQoZXZlbnQsIGhpbnQsIHNjb3BlKTtcbiAgICAgICAgfSk7XG4gICAgICAgIHJldHVybiBldmVudElkO1xuICAgIH07XG4gICAgLyoqXG4gICAgICogQGluaGVyaXREb2NcbiAgICAgKi9cbiAgICBCYXNlQ2xpZW50LnByb3RvdHlwZS5jYXB0dXJlTWVzc2FnZSA9IGZ1bmN0aW9uIChtZXNzYWdlLCBsZXZlbCwgaGludCwgc2NvcGUpIHtcbiAgICAgICAgdmFyIF90aGlzID0gdGhpcztcbiAgICAgICAgdmFyIGV2ZW50SWQgPSBoaW50ICYmIGhpbnQuZXZlbnRfaWQ7XG4gICAgICAgIHRoaXMuX3Byb2Nlc3NpbmcgPSB0cnVlO1xuICAgICAgICB2YXIgcHJvbWlzZWRFdmVudCA9IGlzUHJpbWl0aXZlKG1lc3NhZ2UpXG4gICAgICAgICAgICA/IHRoaXMuX2dldEJhY2tlbmQoKS5ldmVudEZyb21NZXNzYWdlKFwiXCIgKyBtZXNzYWdlLCBsZXZlbCwgaGludClcbiAgICAgICAgICAgIDogdGhpcy5fZ2V0QmFja2VuZCgpLmV2ZW50RnJvbUV4Y2VwdGlvbihtZXNzYWdlLCBoaW50KTtcbiAgICAgICAgcHJvbWlzZWRFdmVudC50aGVuKGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgICAgICAgZXZlbnRJZCA9IF90aGlzLmNhcHR1cmVFdmVudChldmVudCwgaGludCwgc2NvcGUpO1xuICAgICAgICB9KTtcbiAgICAgICAgcmV0dXJuIGV2ZW50SWQ7XG4gICAgfTtcbiAgICAvKipcbiAgICAgKiBAaW5oZXJpdERvY1xuICAgICAqL1xuICAgIEJhc2VDbGllbnQucHJvdG90eXBlLmNhcHR1cmVFdmVudCA9IGZ1bmN0aW9uIChldmVudCwgaGludCwgc2NvcGUpIHtcbiAgICAgICAgdmFyIF90aGlzID0gdGhpcztcbiAgICAgICAgdmFyIGV2ZW50SWQgPSBoaW50ICYmIGhpbnQuZXZlbnRfaWQ7XG4gICAgICAgIHRoaXMuX3Byb2Nlc3NpbmcgPSB0cnVlO1xuICAgICAgICB0aGlzLl9wcm9jZXNzRXZlbnQoZXZlbnQsIGhpbnQsIHNjb3BlKVxuICAgICAgICAgICAgLnRoZW4oZnVuY3Rpb24gKGZpbmFsRXZlbnQpIHtcbiAgICAgICAgICAgIC8vIFdlIG5lZWQgdG8gY2hlY2sgZm9yIGZpbmFsRXZlbnQgaW4gY2FzZSBiZWZvcmVTZW5kIHJldHVybmVkIG51bGxcbiAgICAgICAgICAgIGV2ZW50SWQgPSBmaW5hbEV2ZW50ICYmIGZpbmFsRXZlbnQuZXZlbnRfaWQ7XG4gICAgICAgICAgICBfdGhpcy5fcHJvY2Vzc2luZyA9IGZhbHNlO1xuICAgICAgICB9KVxuICAgICAgICAgICAgLnRoZW4obnVsbCwgZnVuY3Rpb24gKHJlYXNvbikge1xuICAgICAgICAgICAgbG9nZ2VyLmVycm9yKHJlYXNvbik7XG4gICAgICAgICAgICBfdGhpcy5fcHJvY2Vzc2luZyA9IGZhbHNlO1xuICAgICAgICB9KTtcbiAgICAgICAgcmV0dXJuIGV2ZW50SWQ7XG4gICAgfTtcbiAgICAvKipcbiAgICAgKiBAaW5oZXJpdERvY1xuICAgICAqL1xuICAgIEJhc2VDbGllbnQucHJvdG90eXBlLmdldERzbiA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX2RzbjtcbiAgICB9O1xuICAgIC8qKlxuICAgICAqIEBpbmhlcml0RG9jXG4gICAgICovXG4gICAgQmFzZUNsaWVudC5wcm90b3R5cGUuZ2V0T3B0aW9ucyA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX29wdGlvbnM7XG4gICAgfTtcbiAgICAvKipcbiAgICAgKiBAaW5oZXJpdERvY1xuICAgICAqL1xuICAgIEJhc2VDbGllbnQucHJvdG90eXBlLmZsdXNoID0gZnVuY3Rpb24gKHRpbWVvdXQpIHtcbiAgICAgICAgdmFyIF90aGlzID0gdGhpcztcbiAgICAgICAgcmV0dXJuIHRoaXMuX2lzQ2xpZW50UHJvY2Vzc2luZyh0aW1lb3V0KS50aGVuKGZ1bmN0aW9uIChzdGF0dXMpIHtcbiAgICAgICAgICAgIGNsZWFySW50ZXJ2YWwoc3RhdHVzLmludGVydmFsKTtcbiAgICAgICAgICAgIHJldHVybiBfdGhpcy5fZ2V0QmFja2VuZCgpXG4gICAgICAgICAgICAgICAgLmdldFRyYW5zcG9ydCgpXG4gICAgICAgICAgICAgICAgLmNsb3NlKHRpbWVvdXQpXG4gICAgICAgICAgICAgICAgLnRoZW4oZnVuY3Rpb24gKHRyYW5zcG9ydEZsdXNoZWQpIHsgcmV0dXJuIHN0YXR1cy5yZWFkeSAmJiB0cmFuc3BvcnRGbHVzaGVkOyB9KTtcbiAgICAgICAgfSk7XG4gICAgfTtcbiAgICAvKipcbiAgICAgKiBAaW5oZXJpdERvY1xuICAgICAqL1xuICAgIEJhc2VDbGllbnQucHJvdG90eXBlLmNsb3NlID0gZnVuY3Rpb24gKHRpbWVvdXQpIHtcbiAgICAgICAgdmFyIF90aGlzID0gdGhpcztcbiAgICAgICAgcmV0dXJuIHRoaXMuZmx1c2godGltZW91dCkudGhlbihmdW5jdGlvbiAocmVzdWx0KSB7XG4gICAgICAgICAgICBfdGhpcy5nZXRPcHRpb25zKCkuZW5hYmxlZCA9IGZhbHNlO1xuICAgICAgICAgICAgcmV0dXJuIHJlc3VsdDtcbiAgICAgICAgfSk7XG4gICAgfTtcbiAgICAvKipcbiAgICAgKiBTZXRzIHVwIHRoZSBpbnRlZ3JhdGlvbnNcbiAgICAgKi9cbiAgICBCYXNlQ2xpZW50LnByb3RvdHlwZS5zZXR1cEludGVncmF0aW9ucyA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgaWYgKHRoaXMuX2lzRW5hYmxlZCgpKSB7XG4gICAgICAgICAgICB0aGlzLl9pbnRlZ3JhdGlvbnMgPSBzZXR1cEludGVncmF0aW9ucyh0aGlzLl9vcHRpb25zKTtcbiAgICAgICAgfVxuICAgIH07XG4gICAgLyoqXG4gICAgICogQGluaGVyaXREb2NcbiAgICAgKi9cbiAgICBCYXNlQ2xpZW50LnByb3RvdHlwZS5nZXRJbnRlZ3JhdGlvbiA9IGZ1bmN0aW9uIChpbnRlZ3JhdGlvbikge1xuICAgICAgICB0cnkge1xuICAgICAgICAgICAgcmV0dXJuIHRoaXMuX2ludGVncmF0aW9uc1tpbnRlZ3JhdGlvbi5pZF0gfHwgbnVsbDtcbiAgICAgICAgfVxuICAgICAgICBjYXRjaCAoX29PKSB7XG4gICAgICAgICAgICBsb2dnZXIud2FybihcIkNhbm5vdCByZXRyaWV2ZSBpbnRlZ3JhdGlvbiBcIiArIGludGVncmF0aW9uLmlkICsgXCIgZnJvbSB0aGUgY3VycmVudCBDbGllbnRcIik7XG4gICAgICAgICAgICByZXR1cm4gbnVsbDtcbiAgICAgICAgfVxuICAgIH07XG4gICAgLyoqIFdhaXRzIGZvciB0aGUgY2xpZW50IHRvIGJlIGRvbmUgd2l0aCBwcm9jZXNzaW5nLiAqL1xuICAgIEJhc2VDbGllbnQucHJvdG90eXBlLl9pc0NsaWVudFByb2Nlc3NpbmcgPSBmdW5jdGlvbiAodGltZW91dCkge1xuICAgICAgICB2YXIgX3RoaXMgPSB0aGlzO1xuICAgICAgICByZXR1cm4gbmV3IFN5bmNQcm9taXNlKGZ1bmN0aW9uIChyZXNvbHZlKSB7XG4gICAgICAgICAgICB2YXIgdGlja2VkID0gMDtcbiAgICAgICAgICAgIHZhciB0aWNrID0gMTtcbiAgICAgICAgICAgIHZhciBpbnRlcnZhbCA9IDA7XG4gICAgICAgICAgICBjbGVhckludGVydmFsKGludGVydmFsKTtcbiAgICAgICAgICAgIGludGVydmFsID0gc2V0SW50ZXJ2YWwoZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIGlmICghX3RoaXMuX3Byb2Nlc3NpbmcpIHtcbiAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZSh7XG4gICAgICAgICAgICAgICAgICAgICAgICBpbnRlcnZhbDogaW50ZXJ2YWwsXG4gICAgICAgICAgICAgICAgICAgICAgICByZWFkeTogdHJ1ZSxcbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICB0aWNrZWQgKz0gdGljaztcbiAgICAgICAgICAgICAgICAgICAgaWYgKHRpbWVvdXQgJiYgdGlja2VkID49IHRpbWVvdXQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc29sdmUoe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGludGVydmFsOiBpbnRlcnZhbCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZWFkeTogZmFsc2UsXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sIHRpY2spO1xuICAgICAgICB9KTtcbiAgICB9O1xuICAgIC8qKiBSZXR1cm5zIHRoZSBjdXJyZW50IGJhY2tlbmQuICovXG4gICAgQmFzZUNsaWVudC5wcm90b3R5cGUuX2dldEJhY2tlbmQgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLl9iYWNrZW5kO1xuICAgIH07XG4gICAgLyoqIERldGVybWluZXMgd2hldGhlciB0aGlzIFNESyBpcyBlbmFibGVkIGFuZCBhIHZhbGlkIERzbiBpcyBwcmVzZW50LiAqL1xuICAgIEJhc2VDbGllbnQucHJvdG90eXBlLl9pc0VuYWJsZWQgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmdldE9wdGlvbnMoKS5lbmFibGVkICE9PSBmYWxzZSAmJiB0aGlzLl9kc24gIT09IHVuZGVmaW5lZDtcbiAgICB9O1xuICAgIC8qKlxuICAgICAqIEFkZHMgY29tbW9uIGluZm9ybWF0aW9uIHRvIGV2ZW50cy5cbiAgICAgKlxuICAgICAqIFRoZSBpbmZvcm1hdGlvbiBpbmNsdWRlcyByZWxlYXNlIGFuZCBlbnZpcm9ubWVudCBmcm9tIGBvcHRpb25zYCxcbiAgICAgKiBicmVhZGNydW1icyBhbmQgY29udGV4dCAoZXh0cmEsIHRhZ3MgYW5kIHVzZXIpIGZyb20gdGhlIHNjb3BlLlxuICAgICAqXG4gICAgICogSW5mb3JtYXRpb24gdGhhdCBpcyBhbHJlYWR5IHByZXNlbnQgaW4gdGhlIGV2ZW50IGlzIG5ldmVyIG92ZXJ3cml0dGVuLiBGb3JcbiAgICAgKiBuZXN0ZWQgb2JqZWN0cywgc3VjaCBhcyB0aGUgY29udGV4dCwga2V5cyBhcmUgbWVyZ2VkLlxuICAgICAqXG4gICAgICogQHBhcmFtIGV2ZW50IFRoZSBvcmlnaW5hbCBldmVudC5cbiAgICAgKiBAcGFyYW0gaGludCBNYXkgY29udGFpbiBhZGRpdGlvbmFsIGluZm9ybWF0aW9uIGFib3V0IHRoZSBvcmlnaW5hbCBleGNlcHRpb24uXG4gICAgICogQHBhcmFtIHNjb3BlIEEgc2NvcGUgY29udGFpbmluZyBldmVudCBtZXRhZGF0YS5cbiAgICAgKiBAcmV0dXJucyBBIG5ldyBldmVudCB3aXRoIG1vcmUgaW5mb3JtYXRpb24uXG4gICAgICovXG4gICAgQmFzZUNsaWVudC5wcm90b3R5cGUuX3ByZXBhcmVFdmVudCA9IGZ1bmN0aW9uIChldmVudCwgc2NvcGUsIGhpbnQpIHtcbiAgICAgICAgdmFyIF90aGlzID0gdGhpcztcbiAgICAgICAgdmFyIF9hID0gdGhpcy5nZXRPcHRpb25zKCkubm9ybWFsaXplRGVwdGgsIG5vcm1hbGl6ZURlcHRoID0gX2EgPT09IHZvaWQgMCA/IDMgOiBfYTtcbiAgICAgICAgdmFyIHByZXBhcmVkID0gdHNsaWJfMS5fX2Fzc2lnbih7fSwgZXZlbnQsIHsgZXZlbnRfaWQ6IGV2ZW50LmV2ZW50X2lkIHx8IChoaW50ICYmIGhpbnQuZXZlbnRfaWQgPyBoaW50LmV2ZW50X2lkIDogdXVpZDQoKSksIHRpbWVzdGFtcDogZXZlbnQudGltZXN0YW1wIHx8IHRpbWVzdGFtcFdpdGhNcygpIH0pO1xuICAgICAgICB0aGlzLl9hcHBseUNsaWVudE9wdGlvbnMocHJlcGFyZWQpO1xuICAgICAgICB0aGlzLl9hcHBseUludGVncmF0aW9uc01ldGFkYXRhKHByZXBhcmVkKTtcbiAgICAgICAgLy8gSWYgd2UgaGF2ZSBzY29wZSBnaXZlbiB0byB1cywgdXNlIGl0IGFzIHRoZSBiYXNlIGZvciBmdXJ0aGVyIG1vZGlmaWNhdGlvbnMuXG4gICAgICAgIC8vIFRoaXMgYWxsb3dzIHVzIHRvIHByZXZlbnQgdW5uZWNlc3NhcnkgY29weWluZyBvZiBkYXRhIGlmIGBjYXB0dXJlQ29udGV4dGAgaXMgbm90IHByb3ZpZGVkLlxuICAgICAgICB2YXIgZmluYWxTY29wZSA9IHNjb3BlO1xuICAgICAgICBpZiAoaGludCAmJiBoaW50LmNhcHR1cmVDb250ZXh0KSB7XG4gICAgICAgICAgICBmaW5hbFNjb3BlID0gU2NvcGUuY2xvbmUoZmluYWxTY29wZSkudXBkYXRlKGhpbnQuY2FwdHVyZUNvbnRleHQpO1xuICAgICAgICB9XG4gICAgICAgIC8vIFdlIHByZXBhcmUgdGhlIHJlc3VsdCBoZXJlIHdpdGggYSByZXNvbHZlZCBFdmVudC5cbiAgICAgICAgdmFyIHJlc3VsdCA9IFN5bmNQcm9taXNlLnJlc29sdmUocHJlcGFyZWQpO1xuICAgICAgICAvLyBUaGlzIHNob3VsZCBiZSB0aGUgbGFzdCB0aGluZyBjYWxsZWQsIHNpbmNlIHdlIHdhbnQgdGhhdFxuICAgICAgICAvLyB7QGxpbmsgSHViLmFkZEV2ZW50UHJvY2Vzc29yfSBnZXRzIHRoZSBmaW5pc2hlZCBwcmVwYXJlZCBldmVudC5cbiAgICAgICAgaWYgKGZpbmFsU2NvcGUpIHtcbiAgICAgICAgICAgIC8vIEluIGNhc2Ugd2UgaGF2ZSBhIGh1YiB3ZSByZWFzc2lnbiBpdC5cbiAgICAgICAgICAgIHJlc3VsdCA9IGZpbmFsU2NvcGUuYXBwbHlUb0V2ZW50KHByZXBhcmVkLCBoaW50KTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gcmVzdWx0LnRoZW4oZnVuY3Rpb24gKGV2dCkge1xuICAgICAgICAgICAgLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOnN0cmljdC10eXBlLXByZWRpY2F0ZXNcbiAgICAgICAgICAgIGlmICh0eXBlb2Ygbm9ybWFsaXplRGVwdGggPT09ICdudW1iZXInICYmIG5vcm1hbGl6ZURlcHRoID4gMCkge1xuICAgICAgICAgICAgICAgIHJldHVybiBfdGhpcy5fbm9ybWFsaXplRXZlbnQoZXZ0LCBub3JtYWxpemVEZXB0aCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm4gZXZ0O1xuICAgICAgICB9KTtcbiAgICB9O1xuICAgIC8qKlxuICAgICAqIEFwcGxpZXMgYG5vcm1hbGl6ZWAgZnVuY3Rpb24gb24gbmVjZXNzYXJ5IGBFdmVudGAgYXR0cmlidXRlcyB0byBtYWtlIHRoZW0gc2FmZSBmb3Igc2VyaWFsaXphdGlvbi5cbiAgICAgKiBOb3JtYWxpemVkIGtleXM6XG4gICAgICogLSBgYnJlYWRjcnVtYnMuZGF0YWBcbiAgICAgKiAtIGB1c2VyYFxuICAgICAqIC0gYGNvbnRleHRzYFxuICAgICAqIC0gYGV4dHJhYFxuICAgICAqIEBwYXJhbSBldmVudCBFdmVudFxuICAgICAqIEByZXR1cm5zIE5vcm1hbGl6ZWQgZXZlbnRcbiAgICAgKi9cbiAgICBCYXNlQ2xpZW50LnByb3RvdHlwZS5fbm9ybWFsaXplRXZlbnQgPSBmdW5jdGlvbiAoZXZlbnQsIGRlcHRoKSB7XG4gICAgICAgIGlmICghZXZlbnQpIHtcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xuICAgICAgICB9XG4gICAgICAgIC8vIHRzbGludDpkaXNhYmxlOm5vLXVuc2FmZS1hbnlcbiAgICAgICAgdmFyIG5vcm1hbGl6ZWQgPSB0c2xpYl8xLl9fYXNzaWduKHt9LCBldmVudCwgKGV2ZW50LmJyZWFkY3J1bWJzICYmIHtcbiAgICAgICAgICAgIGJyZWFkY3J1bWJzOiBldmVudC5icmVhZGNydW1icy5tYXAoZnVuY3Rpb24gKGIpIHsgcmV0dXJuICh0c2xpYl8xLl9fYXNzaWduKHt9LCBiLCAoYi5kYXRhICYmIHtcbiAgICAgICAgICAgICAgICBkYXRhOiBub3JtYWxpemUoYi5kYXRhLCBkZXB0aCksXG4gICAgICAgICAgICB9KSkpOyB9KSxcbiAgICAgICAgfSksIChldmVudC51c2VyICYmIHtcbiAgICAgICAgICAgIHVzZXI6IG5vcm1hbGl6ZShldmVudC51c2VyLCBkZXB0aCksXG4gICAgICAgIH0pLCAoZXZlbnQuY29udGV4dHMgJiYge1xuICAgICAgICAgICAgY29udGV4dHM6IG5vcm1hbGl6ZShldmVudC5jb250ZXh0cywgZGVwdGgpLFxuICAgICAgICB9KSwgKGV2ZW50LmV4dHJhICYmIHtcbiAgICAgICAgICAgIGV4dHJhOiBub3JtYWxpemUoZXZlbnQuZXh0cmEsIGRlcHRoKSxcbiAgICAgICAgfSkpO1xuICAgICAgICAvLyBldmVudC5jb250ZXh0cy50cmFjZSBzdG9yZXMgaW5mb3JtYXRpb24gYWJvdXQgYSBUcmFuc2FjdGlvbi4gU2ltaWxhcmx5LFxuICAgICAgICAvLyBldmVudC5zcGFuc1tdIHN0b3JlcyBpbmZvcm1hdGlvbiBhYm91dCBjaGlsZCBTcGFucy4gR2l2ZW4gdGhhdCBhXG4gICAgICAgIC8vIFRyYW5zYWN0aW9uIGlzIGNvbmNlcHR1YWxseSBhIFNwYW4sIG5vcm1hbGl6YXRpb24gc2hvdWxkIGFwcGx5IHRvIGJvdGhcbiAgICAgICAgLy8gVHJhbnNhY3Rpb25zIGFuZCBTcGFucyBjb25zaXN0ZW50bHkuXG4gICAgICAgIC8vIEZvciBub3cgdGhlIGRlY2lzaW9uIGlzIHRvIHNraXAgbm9ybWFsaXphdGlvbiBvZiBUcmFuc2FjdGlvbnMgYW5kIFNwYW5zLFxuICAgICAgICAvLyBzbyB0aGlzIGJsb2NrIG92ZXJ3cml0ZXMgdGhlIG5vcm1hbGl6ZWQgZXZlbnQgdG8gYWRkIGJhY2sgdGhlIG9yaWdpbmFsXG4gICAgICAgIC8vIFRyYW5zYWN0aW9uIGluZm9ybWF0aW9uIHByaW9yIHRvIG5vcm1hbGl6YXRpb24uXG4gICAgICAgIGlmIChldmVudC5jb250ZXh0cyAmJiBldmVudC5jb250ZXh0cy50cmFjZSkge1xuICAgICAgICAgICAgbm9ybWFsaXplZC5jb250ZXh0cy50cmFjZSA9IGV2ZW50LmNvbnRleHRzLnRyYWNlO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBub3JtYWxpemVkO1xuICAgIH07XG4gICAgLyoqXG4gICAgICogIEVuaGFuY2VzIGV2ZW50IHVzaW5nIHRoZSBjbGllbnQgY29uZmlndXJhdGlvbi5cbiAgICAgKiAgSXQgdGFrZXMgY2FyZSBvZiBhbGwgXCJzdGF0aWNcIiB2YWx1ZXMgbGlrZSBlbnZpcm9ubWVudCwgcmVsZWFzZSBhbmQgYGRpc3RgLFxuICAgICAqICBhcyB3ZWxsIGFzIHRydW5jYXRpbmcgb3Zlcmx5IGxvbmcgdmFsdWVzLlxuICAgICAqIEBwYXJhbSBldmVudCBldmVudCBpbnN0YW5jZSB0byBiZSBlbmhhbmNlZFxuICAgICAqL1xuICAgIEJhc2VDbGllbnQucHJvdG90eXBlLl9hcHBseUNsaWVudE9wdGlvbnMgPSBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgICAgdmFyIF9hID0gdGhpcy5nZXRPcHRpb25zKCksIGVudmlyb25tZW50ID0gX2EuZW52aXJvbm1lbnQsIHJlbGVhc2UgPSBfYS5yZWxlYXNlLCBkaXN0ID0gX2EuZGlzdCwgX2IgPSBfYS5tYXhWYWx1ZUxlbmd0aCwgbWF4VmFsdWVMZW5ndGggPSBfYiA9PT0gdm9pZCAwID8gMjUwIDogX2I7XG4gICAgICAgIGlmIChldmVudC5lbnZpcm9ubWVudCA9PT0gdW5kZWZpbmVkICYmIGVudmlyb25tZW50ICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIGV2ZW50LmVudmlyb25tZW50ID0gZW52aXJvbm1lbnQ7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKGV2ZW50LnJlbGVhc2UgPT09IHVuZGVmaW5lZCAmJiByZWxlYXNlICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIGV2ZW50LnJlbGVhc2UgPSByZWxlYXNlO1xuICAgICAgICB9XG4gICAgICAgIGlmIChldmVudC5kaXN0ID09PSB1bmRlZmluZWQgJiYgZGlzdCAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICBldmVudC5kaXN0ID0gZGlzdDtcbiAgICAgICAgfVxuICAgICAgICBpZiAoZXZlbnQubWVzc2FnZSkge1xuICAgICAgICAgICAgZXZlbnQubWVzc2FnZSA9IHRydW5jYXRlKGV2ZW50Lm1lc3NhZ2UsIG1heFZhbHVlTGVuZ3RoKTtcbiAgICAgICAgfVxuICAgICAgICB2YXIgZXhjZXB0aW9uID0gZXZlbnQuZXhjZXB0aW9uICYmIGV2ZW50LmV4Y2VwdGlvbi52YWx1ZXMgJiYgZXZlbnQuZXhjZXB0aW9uLnZhbHVlc1swXTtcbiAgICAgICAgaWYgKGV4Y2VwdGlvbiAmJiBleGNlcHRpb24udmFsdWUpIHtcbiAgICAgICAgICAgIGV4Y2VwdGlvbi52YWx1ZSA9IHRydW5jYXRlKGV4Y2VwdGlvbi52YWx1ZSwgbWF4VmFsdWVMZW5ndGgpO1xuICAgICAgICB9XG4gICAgICAgIHZhciByZXF1ZXN0ID0gZXZlbnQucmVxdWVzdDtcbiAgICAgICAgaWYgKHJlcXVlc3QgJiYgcmVxdWVzdC51cmwpIHtcbiAgICAgICAgICAgIHJlcXVlc3QudXJsID0gdHJ1bmNhdGUocmVxdWVzdC51cmwsIG1heFZhbHVlTGVuZ3RoKTtcbiAgICAgICAgfVxuICAgIH07XG4gICAgLyoqXG4gICAgICogVGhpcyBmdW5jdGlvbiBhZGRzIGFsbCB1c2VkIGludGVncmF0aW9ucyB0byB0aGUgU0RLIGluZm8gaW4gdGhlIGV2ZW50LlxuICAgICAqIEBwYXJhbSBzZGtJbmZvIFRoZSBzZGtJbmZvIG9mIHRoZSBldmVudCB0aGF0IHdpbGwgYmUgZmlsbGVkIHdpdGggYWxsIGludGVncmF0aW9ucy5cbiAgICAgKi9cbiAgICBCYXNlQ2xpZW50LnByb3RvdHlwZS5fYXBwbHlJbnRlZ3JhdGlvbnNNZXRhZGF0YSA9IGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgICB2YXIgc2RrSW5mbyA9IGV2ZW50LnNkaztcbiAgICAgICAgdmFyIGludGVncmF0aW9uc0FycmF5ID0gT2JqZWN0LmtleXModGhpcy5faW50ZWdyYXRpb25zKTtcbiAgICAgICAgaWYgKHNka0luZm8gJiYgaW50ZWdyYXRpb25zQXJyYXkubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgc2RrSW5mby5pbnRlZ3JhdGlvbnMgPSBpbnRlZ3JhdGlvbnNBcnJheTtcbiAgICAgICAgfVxuICAgIH07XG4gICAgLyoqXG4gICAgICogVGVsbHMgdGhlIGJhY2tlbmQgdG8gc2VuZCB0aGlzIGV2ZW50XG4gICAgICogQHBhcmFtIGV2ZW50IFRoZSBTZW50cnkgZXZlbnQgdG8gc2VuZFxuICAgICAqL1xuICAgIEJhc2VDbGllbnQucHJvdG90eXBlLl9zZW5kRXZlbnQgPSBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgICAgdGhpcy5fZ2V0QmFja2VuZCgpLnNlbmRFdmVudChldmVudCk7XG4gICAgfTtcbiAgICAvKipcbiAgICAgKiBQcm9jZXNzZXMgYW4gZXZlbnQgKGVpdGhlciBlcnJvciBvciBtZXNzYWdlKSBhbmQgc2VuZHMgaXQgdG8gU2VudHJ5LlxuICAgICAqXG4gICAgICogVGhpcyBhbHNvIGFkZHMgYnJlYWRjcnVtYnMgYW5kIGNvbnRleHQgaW5mb3JtYXRpb24gdG8gdGhlIGV2ZW50LiBIb3dldmVyLFxuICAgICAqIHBsYXRmb3JtIHNwZWNpZmljIG1ldGEgZGF0YSAoc3VjaCBhcyB0aGUgVXNlcidzIElQIGFkZHJlc3MpIG11c3QgYmUgYWRkZWRcbiAgICAgKiBieSB0aGUgU0RLIGltcGxlbWVudG9yLlxuICAgICAqXG4gICAgICpcbiAgICAgKiBAcGFyYW0gZXZlbnQgVGhlIGV2ZW50IHRvIHNlbmQgdG8gU2VudHJ5LlxuICAgICAqIEBwYXJhbSBoaW50IE1heSBjb250YWluIGFkZGl0aW9uYWwgaW5mb3JtYXRpb24gYWJvdXQgdGhlIG9yaWdpbmFsIGV4Y2VwdGlvbi5cbiAgICAgKiBAcGFyYW0gc2NvcGUgQSBzY29wZSBjb250YWluaW5nIGV2ZW50IG1ldGFkYXRhLlxuICAgICAqIEByZXR1cm5zIEEgU3luY1Byb21pc2UgdGhhdCByZXNvbHZlcyB3aXRoIHRoZSBldmVudCBvciByZWplY3RzIGluIGNhc2UgZXZlbnQgd2FzL3dpbGwgbm90IGJlIHNlbmQuXG4gICAgICovXG4gICAgQmFzZUNsaWVudC5wcm90b3R5cGUuX3Byb2Nlc3NFdmVudCA9IGZ1bmN0aW9uIChldmVudCwgaGludCwgc2NvcGUpIHtcbiAgICAgICAgdmFyIF90aGlzID0gdGhpcztcbiAgICAgICAgdmFyIF9hID0gdGhpcy5nZXRPcHRpb25zKCksIGJlZm9yZVNlbmQgPSBfYS5iZWZvcmVTZW5kLCBzYW1wbGVSYXRlID0gX2Euc2FtcGxlUmF0ZTtcbiAgICAgICAgaWYgKCF0aGlzLl9pc0VuYWJsZWQoKSkge1xuICAgICAgICAgICAgcmV0dXJuIFN5bmNQcm9taXNlLnJlamVjdCgnU0RLIG5vdCBlbmFibGVkLCB3aWxsIG5vdCBzZW5kIGV2ZW50LicpO1xuICAgICAgICB9XG4gICAgICAgIHZhciBpc1RyYW5zYWN0aW9uID0gZXZlbnQudHlwZSA9PT0gJ3RyYW5zYWN0aW9uJztcbiAgICAgICAgLy8gMS4wID09PSAxMDAlIGV2ZW50cyBhcmUgc2VudFxuICAgICAgICAvLyAwLjAgPT09IDAlIGV2ZW50cyBhcmUgc2VudFxuICAgICAgICAvLyBTYW1wbGluZyBmb3IgdHJhbnNhY3Rpb24gaGFwcGVucyBzb21ld2hlcmUgZWxzZVxuICAgICAgICBpZiAoIWlzVHJhbnNhY3Rpb24gJiYgdHlwZW9mIHNhbXBsZVJhdGUgPT09ICdudW1iZXInICYmIE1hdGgucmFuZG9tKCkgPiBzYW1wbGVSYXRlKSB7XG4gICAgICAgICAgICByZXR1cm4gU3luY1Byb21pc2UucmVqZWN0KCdUaGlzIGV2ZW50IGhhcyBiZWVuIHNhbXBsZWQsIHdpbGwgbm90IHNlbmQgZXZlbnQuJyk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIG5ldyBTeW5jUHJvbWlzZShmdW5jdGlvbiAocmVzb2x2ZSwgcmVqZWN0KSB7XG4gICAgICAgICAgICBfdGhpcy5fcHJlcGFyZUV2ZW50KGV2ZW50LCBzY29wZSwgaGludClcbiAgICAgICAgICAgICAgICAudGhlbihmdW5jdGlvbiAocHJlcGFyZWQpIHtcbiAgICAgICAgICAgICAgICBpZiAocHJlcGFyZWQgPT09IG51bGwpIHtcbiAgICAgICAgICAgICAgICAgICAgcmVqZWN0KCdBbiBldmVudCBwcm9jZXNzb3IgcmV0dXJuZWQgbnVsbCwgd2lsbCBub3Qgc2VuZCBldmVudC4nKTtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB2YXIgZmluYWxFdmVudCA9IHByZXBhcmVkO1xuICAgICAgICAgICAgICAgIHZhciBpc0ludGVybmFsRXhjZXB0aW9uID0gaGludCAmJiBoaW50LmRhdGEgJiYgaGludC5kYXRhLl9fc2VudHJ5X18gPT09IHRydWU7XG4gICAgICAgICAgICAgICAgLy8gV2Ugc2tpcCBiZWZvcmVTZW5kIGluIGNhc2Ugb2YgdHJhbnNhY3Rpb25zXG4gICAgICAgICAgICAgICAgaWYgKGlzSW50ZXJuYWxFeGNlcHRpb24gfHwgIWJlZm9yZVNlbmQgfHwgaXNUcmFuc2FjdGlvbikge1xuICAgICAgICAgICAgICAgICAgICBfdGhpcy5fc2VuZEV2ZW50KGZpbmFsRXZlbnQpO1xuICAgICAgICAgICAgICAgICAgICByZXNvbHZlKGZpbmFsRXZlbnQpO1xuICAgICAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIHZhciBiZWZvcmVTZW5kUmVzdWx0ID0gYmVmb3JlU2VuZChwcmVwYXJlZCwgaGludCk7XG4gICAgICAgICAgICAgICAgLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOnN0cmljdC10eXBlLXByZWRpY2F0ZXNcbiAgICAgICAgICAgICAgICBpZiAodHlwZW9mIGJlZm9yZVNlbmRSZXN1bHQgPT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgICAgICAgICAgICAgIGxvZ2dlci5lcnJvcignYGJlZm9yZVNlbmRgIG1ldGhvZCBoYXMgdG8gcmV0dXJuIGBudWxsYCBvciBhIHZhbGlkIGV2ZW50LicpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBlbHNlIGlmIChpc1RoZW5hYmxlKGJlZm9yZVNlbmRSZXN1bHQpKSB7XG4gICAgICAgICAgICAgICAgICAgIF90aGlzLl9oYW5kbGVBc3luY0JlZm9yZVNlbmQoYmVmb3JlU2VuZFJlc3VsdCwgcmVzb2x2ZSwgcmVqZWN0KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIGZpbmFsRXZlbnQgPSBiZWZvcmVTZW5kUmVzdWx0O1xuICAgICAgICAgICAgICAgICAgICBpZiAoZmluYWxFdmVudCA9PT0gbnVsbCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgbG9nZ2VyLmxvZygnYGJlZm9yZVNlbmRgIHJldHVybmVkIGBudWxsYCwgd2lsbCBub3Qgc2VuZCBldmVudC4nKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc29sdmUobnVsbCk7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgLy8gRnJvbSBoZXJlIG9uIHdlIGFyZSByZWFsbHkgYXN5bmNcbiAgICAgICAgICAgICAgICAgICAgX3RoaXMuX3NlbmRFdmVudChmaW5hbEV2ZW50KTtcbiAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZShmaW5hbEV2ZW50KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIC50aGVuKG51bGwsIGZ1bmN0aW9uIChyZWFzb24pIHtcbiAgICAgICAgICAgICAgICBfdGhpcy5jYXB0dXJlRXhjZXB0aW9uKHJlYXNvbiwge1xuICAgICAgICAgICAgICAgICAgICBkYXRhOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBfX3NlbnRyeV9fOiB0cnVlLFxuICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICBvcmlnaW5hbEV4Y2VwdGlvbjogcmVhc29uLFxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIHJlamVjdChcIkV2ZW50IHByb2Nlc3NpbmcgcGlwZWxpbmUgdGhyZXcgYW4gZXJyb3IsIG9yaWdpbmFsIGV2ZW50IHdpbGwgbm90IGJlIHNlbnQuIERldGFpbHMgaGF2ZSBiZWVuIHNlbnQgYXMgYSBuZXcgZXZlbnQuXFxuUmVhc29uOiBcIiArIHJlYXNvbik7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgfTtcbiAgICAvKipcbiAgICAgKiBSZXNvbHZlcyBiZWZvcmUgc2VuZCBQcm9taXNlIGFuZCBjYWxscyByZXNvbHZlL3JlamVjdCBvbiBwYXJlbnQgU3luY1Byb21pc2UuXG4gICAgICovXG4gICAgQmFzZUNsaWVudC5wcm90b3R5cGUuX2hhbmRsZUFzeW5jQmVmb3JlU2VuZCA9IGZ1bmN0aW9uIChiZWZvcmVTZW5kLCByZXNvbHZlLCByZWplY3QpIHtcbiAgICAgICAgdmFyIF90aGlzID0gdGhpcztcbiAgICAgICAgYmVmb3JlU2VuZFxuICAgICAgICAgICAgLnRoZW4oZnVuY3Rpb24gKHByb2Nlc3NlZEV2ZW50KSB7XG4gICAgICAgICAgICBpZiAocHJvY2Vzc2VkRXZlbnQgPT09IG51bGwpIHtcbiAgICAgICAgICAgICAgICByZWplY3QoJ2BiZWZvcmVTZW5kYCByZXR1cm5lZCBgbnVsbGAsIHdpbGwgbm90IHNlbmQgZXZlbnQuJyk7XG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLy8gRnJvbSBoZXJlIG9uIHdlIGFyZSByZWFsbHkgYXN5bmNcbiAgICAgICAgICAgIF90aGlzLl9zZW5kRXZlbnQocHJvY2Vzc2VkRXZlbnQpO1xuICAgICAgICAgICAgcmVzb2x2ZShwcm9jZXNzZWRFdmVudCk7XG4gICAgICAgIH0pXG4gICAgICAgICAgICAudGhlbihudWxsLCBmdW5jdGlvbiAoZSkge1xuICAgICAgICAgICAgcmVqZWN0KFwiYmVmb3JlU2VuZCByZWplY3RlZCB3aXRoIFwiICsgZSk7XG4gICAgICAgIH0pO1xuICAgIH07XG4gICAgcmV0dXJuIEJhc2VDbGllbnQ7XG59KCkpO1xuZXhwb3J0IHsgQmFzZUNsaWVudCB9O1xuLy8jIHNvdXJjZU1hcHBpbmdVUkw9YmFzZWNsaWVudC5qcy5tYXAiLCJleHBvcnQgeyBhZGRCcmVhZGNydW1iLCBjYXB0dXJlRXhjZXB0aW9uLCBjYXB0dXJlRXZlbnQsIGNhcHR1cmVNZXNzYWdlLCBjb25maWd1cmVTY29wZSwgc3RhcnRUcmFuc2FjdGlvbiwgc2V0Q29udGV4dCwgc2V0RXh0cmEsIHNldEV4dHJhcywgc2V0VGFnLCBzZXRUYWdzLCBzZXRVc2VyLCB3aXRoU2NvcGUsIH0gZnJvbSAnQHNlbnRyeS9taW5pbWFsJztcbmV4cG9ydCB7IGFkZEdsb2JhbEV2ZW50UHJvY2Vzc29yLCBnZXRDdXJyZW50SHViLCBnZXRIdWJGcm9tQ2FycmllciwgSHViLCBtYWtlTWFpbiwgU2NvcGUgfSBmcm9tICdAc2VudHJ5L2h1Yic7XG5leHBvcnQgeyBBUEkgfSBmcm9tICcuL2FwaSc7XG5leHBvcnQgeyBCYXNlQ2xpZW50IH0gZnJvbSAnLi9iYXNlY2xpZW50JztcbmV4cG9ydCB7IEJhc2VCYWNrZW5kIH0gZnJvbSAnLi9iYXNlYmFja2VuZCc7XG5leHBvcnQgeyBldmVudFRvU2VudHJ5UmVxdWVzdCB9IGZyb20gJy4vcmVxdWVzdCc7XG5leHBvcnQgeyBpbml0QW5kQmluZCB9IGZyb20gJy4vc2RrJztcbmV4cG9ydCB7IE5vb3BUcmFuc3BvcnQgfSBmcm9tICcuL3RyYW5zcG9ydHMvbm9vcCc7XG5pbXBvcnQgKiBhcyBJbnRlZ3JhdGlvbnMgZnJvbSAnLi9pbnRlZ3JhdGlvbnMnO1xuZXhwb3J0IHsgSW50ZWdyYXRpb25zIH07XG4vLyMgc291cmNlTWFwcGluZ1VSTD1pbmRleC5qcy5tYXAiLCJpbXBvcnQgKiBhcyB0c2xpYl8xIGZyb20gXCJ0c2xpYlwiO1xuaW1wb3J0IHsgYWRkR2xvYmFsRXZlbnRQcm9jZXNzb3IsIGdldEN1cnJlbnRIdWIgfSBmcm9tICdAc2VudHJ5L2h1Yic7XG5pbXBvcnQgeyBsb2dnZXIgfSBmcm9tICdAc2VudHJ5L3V0aWxzJztcbmV4cG9ydCB2YXIgaW5zdGFsbGVkSW50ZWdyYXRpb25zID0gW107XG4vKiogR2V0cyBpbnRlZ3JhdGlvbiB0byBpbnN0YWxsICovXG5leHBvcnQgZnVuY3Rpb24gZ2V0SW50ZWdyYXRpb25zVG9TZXR1cChvcHRpb25zKSB7XG4gICAgdmFyIGRlZmF1bHRJbnRlZ3JhdGlvbnMgPSAob3B0aW9ucy5kZWZhdWx0SW50ZWdyYXRpb25zICYmIHRzbGliXzEuX19zcHJlYWQob3B0aW9ucy5kZWZhdWx0SW50ZWdyYXRpb25zKSkgfHwgW107XG4gICAgdmFyIHVzZXJJbnRlZ3JhdGlvbnMgPSBvcHRpb25zLmludGVncmF0aW9ucztcbiAgICB2YXIgaW50ZWdyYXRpb25zID0gW107XG4gICAgaWYgKEFycmF5LmlzQXJyYXkodXNlckludGVncmF0aW9ucykpIHtcbiAgICAgICAgdmFyIHVzZXJJbnRlZ3JhdGlvbnNOYW1lc18xID0gdXNlckludGVncmF0aW9ucy5tYXAoZnVuY3Rpb24gKGkpIHsgcmV0dXJuIGkubmFtZTsgfSk7XG4gICAgICAgIHZhciBwaWNrZWRJbnRlZ3JhdGlvbnNOYW1lc18xID0gW107XG4gICAgICAgIC8vIExlYXZlIG9ubHkgdW5pcXVlIGRlZmF1bHQgaW50ZWdyYXRpb25zLCB0aGF0IHdlcmUgbm90IG92ZXJyaWRkZW4gd2l0aCBwcm92aWRlZCB1c2VyIGludGVncmF0aW9uc1xuICAgICAgICBkZWZhdWx0SW50ZWdyYXRpb25zLmZvckVhY2goZnVuY3Rpb24gKGRlZmF1bHRJbnRlZ3JhdGlvbikge1xuICAgICAgICAgICAgaWYgKHVzZXJJbnRlZ3JhdGlvbnNOYW1lc18xLmluZGV4T2YoZGVmYXVsdEludGVncmF0aW9uLm5hbWUpID09PSAtMSAmJlxuICAgICAgICAgICAgICAgIHBpY2tlZEludGVncmF0aW9uc05hbWVzXzEuaW5kZXhPZihkZWZhdWx0SW50ZWdyYXRpb24ubmFtZSkgPT09IC0xKSB7XG4gICAgICAgICAgICAgICAgaW50ZWdyYXRpb25zLnB1c2goZGVmYXVsdEludGVncmF0aW9uKTtcbiAgICAgICAgICAgICAgICBwaWNrZWRJbnRlZ3JhdGlvbnNOYW1lc18xLnB1c2goZGVmYXVsdEludGVncmF0aW9uLm5hbWUpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICAgICAgLy8gRG9uJ3QgYWRkIHNhbWUgdXNlciBpbnRlZ3JhdGlvbiB0d2ljZVxuICAgICAgICB1c2VySW50ZWdyYXRpb25zLmZvckVhY2goZnVuY3Rpb24gKHVzZXJJbnRlZ3JhdGlvbikge1xuICAgICAgICAgICAgaWYgKHBpY2tlZEludGVncmF0aW9uc05hbWVzXzEuaW5kZXhPZih1c2VySW50ZWdyYXRpb24ubmFtZSkgPT09IC0xKSB7XG4gICAgICAgICAgICAgICAgaW50ZWdyYXRpb25zLnB1c2godXNlckludGVncmF0aW9uKTtcbiAgICAgICAgICAgICAgICBwaWNrZWRJbnRlZ3JhdGlvbnNOYW1lc18xLnB1c2godXNlckludGVncmF0aW9uLm5hbWUpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9XG4gICAgZWxzZSBpZiAodHlwZW9mIHVzZXJJbnRlZ3JhdGlvbnMgPT09ICdmdW5jdGlvbicpIHtcbiAgICAgICAgaW50ZWdyYXRpb25zID0gdXNlckludGVncmF0aW9ucyhkZWZhdWx0SW50ZWdyYXRpb25zKTtcbiAgICAgICAgaW50ZWdyYXRpb25zID0gQXJyYXkuaXNBcnJheShpbnRlZ3JhdGlvbnMpID8gaW50ZWdyYXRpb25zIDogW2ludGVncmF0aW9uc107XG4gICAgfVxuICAgIGVsc2Uge1xuICAgICAgICBpbnRlZ3JhdGlvbnMgPSB0c2xpYl8xLl9fc3ByZWFkKGRlZmF1bHRJbnRlZ3JhdGlvbnMpO1xuICAgIH1cbiAgICAvLyBNYWtlIHN1cmUgdGhhdCBpZiBwcmVzZW50LCBgRGVidWdgIGludGVncmF0aW9uIHdpbGwgYWx3YXlzIHJ1biBsYXN0XG4gICAgdmFyIGludGVncmF0aW9uc05hbWVzID0gaW50ZWdyYXRpb25zLm1hcChmdW5jdGlvbiAoaSkgeyByZXR1cm4gaS5uYW1lOyB9KTtcbiAgICB2YXIgYWx3YXlzTGFzdFRvUnVuID0gJ0RlYnVnJztcbiAgICBpZiAoaW50ZWdyYXRpb25zTmFtZXMuaW5kZXhPZihhbHdheXNMYXN0VG9SdW4pICE9PSAtMSkge1xuICAgICAgICBpbnRlZ3JhdGlvbnMucHVzaC5hcHBseShpbnRlZ3JhdGlvbnMsIHRzbGliXzEuX19zcHJlYWQoaW50ZWdyYXRpb25zLnNwbGljZShpbnRlZ3JhdGlvbnNOYW1lcy5pbmRleE9mKGFsd2F5c0xhc3RUb1J1biksIDEpKSk7XG4gICAgfVxuICAgIHJldHVybiBpbnRlZ3JhdGlvbnM7XG59XG4vKiogU2V0dXAgZ2l2ZW4gaW50ZWdyYXRpb24gKi9cbmV4cG9ydCBmdW5jdGlvbiBzZXR1cEludGVncmF0aW9uKGludGVncmF0aW9uKSB7XG4gICAgaWYgKGluc3RhbGxlZEludGVncmF0aW9ucy5pbmRleE9mKGludGVncmF0aW9uLm5hbWUpICE9PSAtMSkge1xuICAgICAgICByZXR1cm47XG4gICAgfVxuICAgIGludGVncmF0aW9uLnNldHVwT25jZShhZGRHbG9iYWxFdmVudFByb2Nlc3NvciwgZ2V0Q3VycmVudEh1Yik7XG4gICAgaW5zdGFsbGVkSW50ZWdyYXRpb25zLnB1c2goaW50ZWdyYXRpb24ubmFtZSk7XG4gICAgbG9nZ2VyLmxvZyhcIkludGVncmF0aW9uIGluc3RhbGxlZDogXCIgKyBpbnRlZ3JhdGlvbi5uYW1lKTtcbn1cbi8qKlxuICogR2l2ZW4gYSBsaXN0IG9mIGludGVncmF0aW9uIGluc3RhbmNlcyB0aGlzIGluc3RhbGxzIHRoZW0gYWxsLiBXaGVuIGB3aXRoRGVmYXVsdHNgIGlzIHNldCB0byBgdHJ1ZWAgdGhlbiBhbGwgZGVmYXVsdFxuICogaW50ZWdyYXRpb25zIGFyZSBhZGRlZCB1bmxlc3MgdGhleSB3ZXJlIGFscmVhZHkgcHJvdmlkZWQgYmVmb3JlLlxuICogQHBhcmFtIGludGVncmF0aW9ucyBhcnJheSBvZiBpbnRlZ3JhdGlvbiBpbnN0YW5jZXNcbiAqIEBwYXJhbSB3aXRoRGVmYXVsdCBzaG91bGQgZW5hYmxlIGRlZmF1bHQgaW50ZWdyYXRpb25zXG4gKi9cbmV4cG9ydCBmdW5jdGlvbiBzZXR1cEludGVncmF0aW9ucyhvcHRpb25zKSB7XG4gICAgdmFyIGludGVncmF0aW9ucyA9IHt9O1xuICAgIGdldEludGVncmF0aW9uc1RvU2V0dXAob3B0aW9ucykuZm9yRWFjaChmdW5jdGlvbiAoaW50ZWdyYXRpb24pIHtcbiAgICAgICAgaW50ZWdyYXRpb25zW2ludGVncmF0aW9uLm5hbWVdID0gaW50ZWdyYXRpb247XG4gICAgICAgIHNldHVwSW50ZWdyYXRpb24oaW50ZWdyYXRpb24pO1xuICAgIH0pO1xuICAgIHJldHVybiBpbnRlZ3JhdGlvbnM7XG59XG4vLyMgc291cmNlTWFwcGluZ1VSTD1pbnRlZ3JhdGlvbi5qcy5tYXAiLCJ2YXIgb3JpZ2luYWxGdW5jdGlvblRvU3RyaW5nO1xuLyoqIFBhdGNoIHRvU3RyaW5nIGNhbGxzIHRvIHJldHVybiBwcm9wZXIgbmFtZSBmb3Igd3JhcHBlZCBmdW5jdGlvbnMgKi9cbnZhciBGdW5jdGlvblRvU3RyaW5nID0gLyoqIEBjbGFzcyAqLyAoZnVuY3Rpb24gKCkge1xuICAgIGZ1bmN0aW9uIEZ1bmN0aW9uVG9TdHJpbmcoKSB7XG4gICAgICAgIC8qKlxuICAgICAgICAgKiBAaW5oZXJpdERvY1xuICAgICAgICAgKi9cbiAgICAgICAgdGhpcy5uYW1lID0gRnVuY3Rpb25Ub1N0cmluZy5pZDtcbiAgICB9XG4gICAgLyoqXG4gICAgICogQGluaGVyaXREb2NcbiAgICAgKi9cbiAgICBGdW5jdGlvblRvU3RyaW5nLnByb3RvdHlwZS5zZXR1cE9uY2UgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIG9yaWdpbmFsRnVuY3Rpb25Ub1N0cmluZyA9IEZ1bmN0aW9uLnByb3RvdHlwZS50b1N0cmluZztcbiAgICAgICAgRnVuY3Rpb24ucHJvdG90eXBlLnRvU3RyaW5nID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgdmFyIGFyZ3MgPSBbXTtcbiAgICAgICAgICAgIGZvciAodmFyIF9pID0gMDsgX2kgPCBhcmd1bWVudHMubGVuZ3RoOyBfaSsrKSB7XG4gICAgICAgICAgICAgICAgYXJnc1tfaV0gPSBhcmd1bWVudHNbX2ldO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdmFyIGNvbnRleHQgPSB0aGlzLl9fc2VudHJ5X29yaWdpbmFsX18gfHwgdGhpcztcbiAgICAgICAgICAgIC8vIHRzbGludDpkaXNhYmxlLW5leHQtbGluZTpuby11bnNhZmUtYW55XG4gICAgICAgICAgICByZXR1cm4gb3JpZ2luYWxGdW5jdGlvblRvU3RyaW5nLmFwcGx5KGNvbnRleHQsIGFyZ3MpO1xuICAgICAgICB9O1xuICAgIH07XG4gICAgLyoqXG4gICAgICogQGluaGVyaXREb2NcbiAgICAgKi9cbiAgICBGdW5jdGlvblRvU3RyaW5nLmlkID0gJ0Z1bmN0aW9uVG9TdHJpbmcnO1xuICAgIHJldHVybiBGdW5jdGlvblRvU3RyaW5nO1xufSgpKTtcbmV4cG9ydCB7IEZ1bmN0aW9uVG9TdHJpbmcgfTtcbi8vIyBzb3VyY2VNYXBwaW5nVVJMPWZ1bmN0aW9udG9zdHJpbmcuanMubWFwIiwiaW1wb3J0ICogYXMgdHNsaWJfMSBmcm9tIFwidHNsaWJcIjtcbmltcG9ydCB7IGFkZEdsb2JhbEV2ZW50UHJvY2Vzc29yLCBnZXRDdXJyZW50SHViIH0gZnJvbSAnQHNlbnRyeS9odWInO1xuaW1wb3J0IHsgZ2V0RXZlbnREZXNjcmlwdGlvbiwgaXNNYXRjaGluZ1BhdHRlcm4sIGxvZ2dlciB9IGZyb20gJ0BzZW50cnkvdXRpbHMnO1xuLy8gXCJTY3JpcHQgZXJyb3IuXCIgaXMgaGFyZCBjb2RlZCBpbnRvIGJyb3dzZXJzIGZvciBlcnJvcnMgdGhhdCBpdCBjYW4ndCByZWFkLlxuLy8gdGhpcyBpcyB0aGUgcmVzdWx0IG9mIGEgc2NyaXB0IGJlaW5nIHB1bGxlZCBpbiBmcm9tIGFuIGV4dGVybmFsIGRvbWFpbiBhbmQgQ09SUy5cbnZhciBERUZBVUxUX0lHTk9SRV9FUlJPUlMgPSBbL15TY3JpcHQgZXJyb3JcXC4/JC8sIC9eSmF2YXNjcmlwdCBlcnJvcjogU2NyaXB0IGVycm9yXFwuPyBvbiBsaW5lIDAkL107XG4vKiogSW5ib3VuZCBmaWx0ZXJzIGNvbmZpZ3VyYWJsZSBieSB0aGUgdXNlciAqL1xudmFyIEluYm91bmRGaWx0ZXJzID0gLyoqIEBjbGFzcyAqLyAoZnVuY3Rpb24gKCkge1xuICAgIGZ1bmN0aW9uIEluYm91bmRGaWx0ZXJzKF9vcHRpb25zKSB7XG4gICAgICAgIGlmIChfb3B0aW9ucyA9PT0gdm9pZCAwKSB7IF9vcHRpb25zID0ge307IH1cbiAgICAgICAgdGhpcy5fb3B0aW9ucyA9IF9vcHRpb25zO1xuICAgICAgICAvKipcbiAgICAgICAgICogQGluaGVyaXREb2NcbiAgICAgICAgICovXG4gICAgICAgIHRoaXMubmFtZSA9IEluYm91bmRGaWx0ZXJzLmlkO1xuICAgIH1cbiAgICAvKipcbiAgICAgKiBAaW5oZXJpdERvY1xuICAgICAqL1xuICAgIEluYm91bmRGaWx0ZXJzLnByb3RvdHlwZS5zZXR1cE9uY2UgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGFkZEdsb2JhbEV2ZW50UHJvY2Vzc29yKGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgICAgICAgdmFyIGh1YiA9IGdldEN1cnJlbnRIdWIoKTtcbiAgICAgICAgICAgIGlmICghaHViKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGV2ZW50O1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdmFyIHNlbGYgPSBodWIuZ2V0SW50ZWdyYXRpb24oSW5ib3VuZEZpbHRlcnMpO1xuICAgICAgICAgICAgaWYgKHNlbGYpIHtcbiAgICAgICAgICAgICAgICB2YXIgY2xpZW50ID0gaHViLmdldENsaWVudCgpO1xuICAgICAgICAgICAgICAgIHZhciBjbGllbnRPcHRpb25zID0gY2xpZW50ID8gY2xpZW50LmdldE9wdGlvbnMoKSA6IHt9O1xuICAgICAgICAgICAgICAgIHZhciBvcHRpb25zID0gc2VsZi5fbWVyZ2VPcHRpb25zKGNsaWVudE9wdGlvbnMpO1xuICAgICAgICAgICAgICAgIGlmIChzZWxmLl9zaG91bGREcm9wRXZlbnQoZXZlbnQsIG9wdGlvbnMpKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBudWxsO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiBldmVudDtcbiAgICAgICAgfSk7XG4gICAgfTtcbiAgICAvKiogSlNEb2MgKi9cbiAgICBJbmJvdW5kRmlsdGVycy5wcm90b3R5cGUuX3Nob3VsZERyb3BFdmVudCA9IGZ1bmN0aW9uIChldmVudCwgb3B0aW9ucykge1xuICAgICAgICBpZiAodGhpcy5faXNTZW50cnlFcnJvcihldmVudCwgb3B0aW9ucykpIHtcbiAgICAgICAgICAgIGxvZ2dlci53YXJuKFwiRXZlbnQgZHJvcHBlZCBkdWUgdG8gYmVpbmcgaW50ZXJuYWwgU2VudHJ5IEVycm9yLlxcbkV2ZW50OiBcIiArIGdldEV2ZW50RGVzY3JpcHRpb24oZXZlbnQpKTtcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICB9XG4gICAgICAgIGlmICh0aGlzLl9pc0lnbm9yZWRFcnJvcihldmVudCwgb3B0aW9ucykpIHtcbiAgICAgICAgICAgIGxvZ2dlci53YXJuKFwiRXZlbnQgZHJvcHBlZCBkdWUgdG8gYmVpbmcgbWF0Y2hlZCBieSBgaWdub3JlRXJyb3JzYCBvcHRpb24uXFxuRXZlbnQ6IFwiICsgZ2V0RXZlbnREZXNjcmlwdGlvbihldmVudCkpO1xuICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHRoaXMuX2lzRGVuaWVkVXJsKGV2ZW50LCBvcHRpb25zKSkge1xuICAgICAgICAgICAgbG9nZ2VyLndhcm4oXCJFdmVudCBkcm9wcGVkIGR1ZSB0byBiZWluZyBtYXRjaGVkIGJ5IGBkZW55VXJsc2Agb3B0aW9uLlxcbkV2ZW50OiBcIiArIGdldEV2ZW50RGVzY3JpcHRpb24oZXZlbnQpICsgXCIuXFxuVXJsOiBcIiArIHRoaXMuX2dldEV2ZW50RmlsdGVyVXJsKGV2ZW50KSk7XG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgfVxuICAgICAgICBpZiAoIXRoaXMuX2lzQWxsb3dlZFVybChldmVudCwgb3B0aW9ucykpIHtcbiAgICAgICAgICAgIGxvZ2dlci53YXJuKFwiRXZlbnQgZHJvcHBlZCBkdWUgdG8gbm90IGJlaW5nIG1hdGNoZWQgYnkgYGFsbG93VXJsc2Agb3B0aW9uLlxcbkV2ZW50OiBcIiArIGdldEV2ZW50RGVzY3JpcHRpb24oZXZlbnQpICsgXCIuXFxuVXJsOiBcIiArIHRoaXMuX2dldEV2ZW50RmlsdGVyVXJsKGV2ZW50KSk7XG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgfTtcbiAgICAvKiogSlNEb2MgKi9cbiAgICBJbmJvdW5kRmlsdGVycy5wcm90b3R5cGUuX2lzU2VudHJ5RXJyb3IgPSBmdW5jdGlvbiAoZXZlbnQsIG9wdGlvbnMpIHtcbiAgICAgICAgaWYgKCFvcHRpb25zLmlnbm9yZUludGVybmFsKSB7XG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgIH1cbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIHJldHVybiAoKGV2ZW50ICYmXG4gICAgICAgICAgICAgICAgZXZlbnQuZXhjZXB0aW9uICYmXG4gICAgICAgICAgICAgICAgZXZlbnQuZXhjZXB0aW9uLnZhbHVlcyAmJlxuICAgICAgICAgICAgICAgIGV2ZW50LmV4Y2VwdGlvbi52YWx1ZXNbMF0gJiZcbiAgICAgICAgICAgICAgICBldmVudC5leGNlcHRpb24udmFsdWVzWzBdLnR5cGUgPT09ICdTZW50cnlFcnJvcicpIHx8XG4gICAgICAgICAgICAgICAgZmFsc2UpO1xuICAgICAgICB9XG4gICAgICAgIGNhdGNoIChfb08pIHtcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgfVxuICAgIH07XG4gICAgLyoqIEpTRG9jICovXG4gICAgSW5ib3VuZEZpbHRlcnMucHJvdG90eXBlLl9pc0lnbm9yZWRFcnJvciA9IGZ1bmN0aW9uIChldmVudCwgb3B0aW9ucykge1xuICAgICAgICBpZiAoIW9wdGlvbnMuaWdub3JlRXJyb3JzIHx8ICFvcHRpb25zLmlnbm9yZUVycm9ycy5sZW5ndGgpIHtcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdGhpcy5fZ2V0UG9zc2libGVFdmVudE1lc3NhZ2VzKGV2ZW50KS5zb21lKGZ1bmN0aW9uIChtZXNzYWdlKSB7XG4gICAgICAgICAgICAvLyBOb3Qgc3VyZSB3aHkgVHlwZVNjcmlwdCBjb21wbGFpbnMgaGVyZS4uLlxuICAgICAgICAgICAgcmV0dXJuIG9wdGlvbnMuaWdub3JlRXJyb3JzLnNvbWUoZnVuY3Rpb24gKHBhdHRlcm4pIHsgcmV0dXJuIGlzTWF0Y2hpbmdQYXR0ZXJuKG1lc3NhZ2UsIHBhdHRlcm4pOyB9KTtcbiAgICAgICAgfSk7XG4gICAgfTtcbiAgICAvKiogSlNEb2MgKi9cbiAgICBJbmJvdW5kRmlsdGVycy5wcm90b3R5cGUuX2lzRGVuaWVkVXJsID0gZnVuY3Rpb24gKGV2ZW50LCBvcHRpb25zKSB7XG4gICAgICAgIC8vIFRPRE86IFVzZSBHbG9iIGluc3RlYWQ/XG4gICAgICAgIGlmICghb3B0aW9ucy5kZW55VXJscyB8fCAhb3B0aW9ucy5kZW55VXJscy5sZW5ndGgpIHtcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgfVxuICAgICAgICB2YXIgdXJsID0gdGhpcy5fZ2V0RXZlbnRGaWx0ZXJVcmwoZXZlbnQpO1xuICAgICAgICByZXR1cm4gIXVybCA/IGZhbHNlIDogb3B0aW9ucy5kZW55VXJscy5zb21lKGZ1bmN0aW9uIChwYXR0ZXJuKSB7IHJldHVybiBpc01hdGNoaW5nUGF0dGVybih1cmwsIHBhdHRlcm4pOyB9KTtcbiAgICB9O1xuICAgIC8qKiBKU0RvYyAqL1xuICAgIEluYm91bmRGaWx0ZXJzLnByb3RvdHlwZS5faXNBbGxvd2VkVXJsID0gZnVuY3Rpb24gKGV2ZW50LCBvcHRpb25zKSB7XG4gICAgICAgIC8vIFRPRE86IFVzZSBHbG9iIGluc3RlYWQ/XG4gICAgICAgIGlmICghb3B0aW9ucy5hbGxvd1VybHMgfHwgIW9wdGlvbnMuYWxsb3dVcmxzLmxlbmd0aCkge1xuICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgIH1cbiAgICAgICAgdmFyIHVybCA9IHRoaXMuX2dldEV2ZW50RmlsdGVyVXJsKGV2ZW50KTtcbiAgICAgICAgcmV0dXJuICF1cmwgPyB0cnVlIDogb3B0aW9ucy5hbGxvd1VybHMuc29tZShmdW5jdGlvbiAocGF0dGVybikgeyByZXR1cm4gaXNNYXRjaGluZ1BhdHRlcm4odXJsLCBwYXR0ZXJuKTsgfSk7XG4gICAgfTtcbiAgICAvKiogSlNEb2MgKi9cbiAgICBJbmJvdW5kRmlsdGVycy5wcm90b3R5cGUuX21lcmdlT3B0aW9ucyA9IGZ1bmN0aW9uIChjbGllbnRPcHRpb25zKSB7XG4gICAgICAgIGlmIChjbGllbnRPcHRpb25zID09PSB2b2lkIDApIHsgY2xpZW50T3B0aW9ucyA9IHt9OyB9XG4gICAgICAgIC8vIHRzbGludDpkaXNhYmxlOmRlcHJlY2F0aW9uXG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICBhbGxvd1VybHM6IHRzbGliXzEuX19zcHJlYWQoKHRoaXMuX29wdGlvbnMud2hpdGVsaXN0VXJscyB8fCBbXSksICh0aGlzLl9vcHRpb25zLmFsbG93VXJscyB8fCBbXSksIChjbGllbnRPcHRpb25zLndoaXRlbGlzdFVybHMgfHwgW10pLCAoY2xpZW50T3B0aW9ucy5hbGxvd1VybHMgfHwgW10pKSxcbiAgICAgICAgICAgIGRlbnlVcmxzOiB0c2xpYl8xLl9fc3ByZWFkKCh0aGlzLl9vcHRpb25zLmJsYWNrbGlzdFVybHMgfHwgW10pLCAodGhpcy5fb3B0aW9ucy5kZW55VXJscyB8fCBbXSksIChjbGllbnRPcHRpb25zLmJsYWNrbGlzdFVybHMgfHwgW10pLCAoY2xpZW50T3B0aW9ucy5kZW55VXJscyB8fCBbXSkpLFxuICAgICAgICAgICAgaWdub3JlRXJyb3JzOiB0c2xpYl8xLl9fc3ByZWFkKCh0aGlzLl9vcHRpb25zLmlnbm9yZUVycm9ycyB8fCBbXSksIChjbGllbnRPcHRpb25zLmlnbm9yZUVycm9ycyB8fCBbXSksIERFRkFVTFRfSUdOT1JFX0VSUk9SUyksXG4gICAgICAgICAgICBpZ25vcmVJbnRlcm5hbDogdHlwZW9mIHRoaXMuX29wdGlvbnMuaWdub3JlSW50ZXJuYWwgIT09ICd1bmRlZmluZWQnID8gdGhpcy5fb3B0aW9ucy5pZ25vcmVJbnRlcm5hbCA6IHRydWUsXG4gICAgICAgIH07XG4gICAgfTtcbiAgICAvKiogSlNEb2MgKi9cbiAgICBJbmJvdW5kRmlsdGVycy5wcm90b3R5cGUuX2dldFBvc3NpYmxlRXZlbnRNZXNzYWdlcyA9IGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgICBpZiAoZXZlbnQubWVzc2FnZSkge1xuICAgICAgICAgICAgcmV0dXJuIFtldmVudC5tZXNzYWdlXTtcbiAgICAgICAgfVxuICAgICAgICBpZiAoZXZlbnQuZXhjZXB0aW9uKSB7XG4gICAgICAgICAgICB0cnkge1xuICAgICAgICAgICAgICAgIHZhciBfYSA9IChldmVudC5leGNlcHRpb24udmFsdWVzICYmIGV2ZW50LmV4Y2VwdGlvbi52YWx1ZXNbMF0pIHx8IHt9LCBfYiA9IF9hLnR5cGUsIHR5cGUgPSBfYiA9PT0gdm9pZCAwID8gJycgOiBfYiwgX2MgPSBfYS52YWx1ZSwgdmFsdWUgPSBfYyA9PT0gdm9pZCAwID8gJycgOiBfYztcbiAgICAgICAgICAgICAgICByZXR1cm4gW1wiXCIgKyB2YWx1ZSwgdHlwZSArIFwiOiBcIiArIHZhbHVlXTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGNhdGNoIChvTykge1xuICAgICAgICAgICAgICAgIGxvZ2dlci5lcnJvcihcIkNhbm5vdCBleHRyYWN0IG1lc3NhZ2UgZm9yIGV2ZW50IFwiICsgZ2V0RXZlbnREZXNjcmlwdGlvbihldmVudCkpO1xuICAgICAgICAgICAgICAgIHJldHVybiBbXTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gW107XG4gICAgfTtcbiAgICAvKiogSlNEb2MgKi9cbiAgICBJbmJvdW5kRmlsdGVycy5wcm90b3R5cGUuX2dldEV2ZW50RmlsdGVyVXJsID0gZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICBpZiAoZXZlbnQuc3RhY2t0cmFjZSkge1xuICAgICAgICAgICAgICAgIHZhciBmcmFtZXNfMSA9IGV2ZW50LnN0YWNrdHJhY2UuZnJhbWVzO1xuICAgICAgICAgICAgICAgIHJldHVybiAoZnJhbWVzXzEgJiYgZnJhbWVzXzFbZnJhbWVzXzEubGVuZ3RoIC0gMV0uZmlsZW5hbWUpIHx8IG51bGw7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAoZXZlbnQuZXhjZXB0aW9uKSB7XG4gICAgICAgICAgICAgICAgdmFyIGZyYW1lc18yID0gZXZlbnQuZXhjZXB0aW9uLnZhbHVlcyAmJiBldmVudC5leGNlcHRpb24udmFsdWVzWzBdLnN0YWNrdHJhY2UgJiYgZXZlbnQuZXhjZXB0aW9uLnZhbHVlc1swXS5zdGFja3RyYWNlLmZyYW1lcztcbiAgICAgICAgICAgICAgICByZXR1cm4gKGZyYW1lc18yICYmIGZyYW1lc18yW2ZyYW1lc18yLmxlbmd0aCAtIDFdLmZpbGVuYW1lKSB8fCBudWxsO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgICAgIH1cbiAgICAgICAgY2F0Y2ggKG9PKSB7XG4gICAgICAgICAgICBsb2dnZXIuZXJyb3IoXCJDYW5ub3QgZXh0cmFjdCB1cmwgZm9yIGV2ZW50IFwiICsgZ2V0RXZlbnREZXNjcmlwdGlvbihldmVudCkpO1xuICAgICAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgICAgIH1cbiAgICB9O1xuICAgIC8qKlxuICAgICAqIEBpbmhlcml0RG9jXG4gICAgICovXG4gICAgSW5ib3VuZEZpbHRlcnMuaWQgPSAnSW5ib3VuZEZpbHRlcnMnO1xuICAgIHJldHVybiBJbmJvdW5kRmlsdGVycztcbn0oKSk7XG5leHBvcnQgeyBJbmJvdW5kRmlsdGVycyB9O1xuLy8jIHNvdXJjZU1hcHBpbmdVUkw9aW5ib3VuZGZpbHRlcnMuanMubWFwIiwiZXhwb3J0IHsgRnVuY3Rpb25Ub1N0cmluZyB9IGZyb20gJy4vZnVuY3Rpb250b3N0cmluZyc7XG5leHBvcnQgeyBJbmJvdW5kRmlsdGVycyB9IGZyb20gJy4vaW5ib3VuZGZpbHRlcnMnO1xuLy8jIHNvdXJjZU1hcHBpbmdVUkw9aW5kZXguanMubWFwIiwiaW1wb3J0IHsgdGltZXN0YW1wV2l0aE1zIH0gZnJvbSAnQHNlbnRyeS91dGlscyc7XG4vKiogQ3JlYXRlcyBhIFNlbnRyeVJlcXVlc3QgZnJvbSBhbiBldmVudC4gKi9cbmV4cG9ydCBmdW5jdGlvbiBldmVudFRvU2VudHJ5UmVxdWVzdChldmVudCwgYXBpKSB7XG4gICAgdmFyIHVzZUVudmVsb3BlID0gZXZlbnQudHlwZSA9PT0gJ3RyYW5zYWN0aW9uJztcbiAgICB2YXIgcmVxID0ge1xuICAgICAgICBib2R5OiBKU09OLnN0cmluZ2lmeShldmVudCksXG4gICAgICAgIHVybDogdXNlRW52ZWxvcGUgPyBhcGkuZ2V0RW52ZWxvcGVFbmRwb2ludFdpdGhVcmxFbmNvZGVkQXV0aCgpIDogYXBpLmdldFN0b3JlRW5kcG9pbnRXaXRoVXJsRW5jb2RlZEF1dGgoKSxcbiAgICB9O1xuICAgIC8vIGh0dHBzOi8vZGV2ZWxvcC5zZW50cnkuZGV2L3Nkay9lbnZlbG9wZXMvXG4gICAgLy8gU2luY2Ugd2UgZG9uJ3QgbmVlZCB0byBtYW5pcHVsYXRlIGVudmVsb3BlcyBub3Igc3RvcmUgdGhlbSwgdGhlcmUgaXMgbm9cbiAgICAvLyBleHBvcnRlZCBjb25jZXB0IG9mIGFuIEVudmVsb3BlIHdpdGggb3BlcmF0aW9ucyBpbmNsdWRpbmcgc2VyaWFsaXphdGlvbiBhbmRcbiAgICAvLyBkZXNlcmlhbGl6YXRpb24uIEluc3RlYWQsIHdlIG9ubHkgaW1wbGVtZW50IGEgbWluaW1hbCBzdWJzZXQgb2YgdGhlIHNwZWMgdG9cbiAgICAvLyBzZXJpYWxpemUgZXZlbnRzIGlubGluZSBoZXJlLlxuICAgIGlmICh1c2VFbnZlbG9wZSkge1xuICAgICAgICB2YXIgZW52ZWxvcGVIZWFkZXJzID0gSlNPTi5zdHJpbmdpZnkoe1xuICAgICAgICAgICAgZXZlbnRfaWQ6IGV2ZW50LmV2ZW50X2lkLFxuICAgICAgICAgICAgLy8gV2UgbmVlZCB0byBhZGQgKiAxMDAwIHNpbmNlIHdlIGRpdmlkZSBpdCBieSAxMDAwIGJ5IGRlZmF1bHQgYnV0IEpTIHdvcmtzIHdpdGggbXMgcHJlY2lzaW9uXG4gICAgICAgICAgICAvLyBUaGUgcmVhc29uIHdlIHVzZSB0aW1lc3RhbXBXaXRoTXMgaGVyZSBpcyB0aGF0IGFsbCBjbG9ja3MgYWNyb3NzIHRoZSBTREsgdXNlIHRoZSBzYW1lIGNsb2NrXG4gICAgICAgICAgICBzZW50X2F0OiBuZXcgRGF0ZSh0aW1lc3RhbXBXaXRoTXMoKSAqIDEwMDApLnRvSVNPU3RyaW5nKCksXG4gICAgICAgIH0pO1xuICAgICAgICB2YXIgaXRlbUhlYWRlcnMgPSBKU09OLnN0cmluZ2lmeSh7XG4gICAgICAgICAgICB0eXBlOiBldmVudC50eXBlLFxuICAgICAgICB9KTtcbiAgICAgICAgLy8gVGhlIHRyYWlsaW5nIG5ld2xpbmUgaXMgb3B0aW9uYWwuIFdlIGludGVudGlvbmFsbHkgZG9uJ3Qgc2VuZCBpdCB0byBhdm9pZFxuICAgICAgICAvLyBzZW5kaW5nIHVubmVjZXNzYXJ5IGJ5dGVzLlxuICAgICAgICAvL1xuICAgICAgICAvLyBjb25zdCBlbnZlbG9wZSA9IGAke2VudmVsb3BlSGVhZGVyc31cXG4ke2l0ZW1IZWFkZXJzfVxcbiR7cmVxLmJvZHl9XFxuYDtcbiAgICAgICAgdmFyIGVudmVsb3BlID0gZW52ZWxvcGVIZWFkZXJzICsgXCJcXG5cIiArIGl0ZW1IZWFkZXJzICsgXCJcXG5cIiArIHJlcS5ib2R5O1xuICAgICAgICByZXEuYm9keSA9IGVudmVsb3BlO1xuICAgIH1cbiAgICByZXR1cm4gcmVxO1xufVxuLy8jIHNvdXJjZU1hcHBpbmdVUkw9cmVxdWVzdC5qcy5tYXAiLCJpbXBvcnQgeyBnZXRDdXJyZW50SHViIH0gZnJvbSAnQHNlbnRyeS9odWInO1xuaW1wb3J0IHsgbG9nZ2VyIH0gZnJvbSAnQHNlbnRyeS91dGlscyc7XG4vKipcbiAqIEludGVybmFsIGZ1bmN0aW9uIHRvIGNyZWF0ZSBhIG5ldyBTREsgY2xpZW50IGluc3RhbmNlLiBUaGUgY2xpZW50IGlzXG4gKiBpbnN0YWxsZWQgYW5kIHRoZW4gYm91bmQgdG8gdGhlIGN1cnJlbnQgc2NvcGUuXG4gKlxuICogQHBhcmFtIGNsaWVudENsYXNzIFRoZSBjbGllbnQgY2xhc3MgdG8gaW5zdGFuY2lhdGUuXG4gKiBAcGFyYW0gb3B0aW9ucyBPcHRpb25zIHRvIHBhc3MgdG8gdGhlIGNsaWVudC5cbiAqL1xuZXhwb3J0IGZ1bmN0aW9uIGluaXRBbmRCaW5kKGNsaWVudENsYXNzLCBvcHRpb25zKSB7XG4gICAgaWYgKG9wdGlvbnMuZGVidWcgPT09IHRydWUpIHtcbiAgICAgICAgbG9nZ2VyLmVuYWJsZSgpO1xuICAgIH1cbiAgICB2YXIgaHViID0gZ2V0Q3VycmVudEh1YigpO1xuICAgIHZhciBjbGllbnQgPSBuZXcgY2xpZW50Q2xhc3Mob3B0aW9ucyk7XG4gICAgaHViLmJpbmRDbGllbnQoY2xpZW50KTtcbn1cbi8vIyBzb3VyY2VNYXBwaW5nVVJMPXNkay5qcy5tYXAiLCJpbXBvcnQgeyBTdGF0dXMgfSBmcm9tICdAc2VudHJ5L3R5cGVzJztcbmltcG9ydCB7IFN5bmNQcm9taXNlIH0gZnJvbSAnQHNlbnRyeS91dGlscyc7XG4vKiogTm9vcCB0cmFuc3BvcnQgKi9cbnZhciBOb29wVHJhbnNwb3J0ID0gLyoqIEBjbGFzcyAqLyAoZnVuY3Rpb24gKCkge1xuICAgIGZ1bmN0aW9uIE5vb3BUcmFuc3BvcnQoKSB7XG4gICAgfVxuICAgIC8qKlxuICAgICAqIEBpbmhlcml0RG9jXG4gICAgICovXG4gICAgTm9vcFRyYW5zcG9ydC5wcm90b3R5cGUuc2VuZEV2ZW50ID0gZnVuY3Rpb24gKF8pIHtcbiAgICAgICAgcmV0dXJuIFN5bmNQcm9taXNlLnJlc29sdmUoe1xuICAgICAgICAgICAgcmVhc29uOiBcIk5vb3BUcmFuc3BvcnQ6IEV2ZW50IGhhcyBiZWVuIHNraXBwZWQgYmVjYXVzZSBubyBEc24gaXMgY29uZmlndXJlZC5cIixcbiAgICAgICAgICAgIHN0YXR1czogU3RhdHVzLlNraXBwZWQsXG4gICAgICAgIH0pO1xuICAgIH07XG4gICAgLyoqXG4gICAgICogQGluaGVyaXREb2NcbiAgICAgKi9cbiAgICBOb29wVHJhbnNwb3J0LnByb3RvdHlwZS5jbG9zZSA9IGZ1bmN0aW9uIChfKSB7XG4gICAgICAgIHJldHVybiBTeW5jUHJvbWlzZS5yZXNvbHZlKHRydWUpO1xuICAgIH07XG4gICAgcmV0dXJuIE5vb3BUcmFuc3BvcnQ7XG59KCkpO1xuZXhwb3J0IHsgTm9vcFRyYW5zcG9ydCB9O1xuLy8jIHNvdXJjZU1hcHBpbmdVUkw9bm9vcC5qcy5tYXAiLCJpbXBvcnQgKiBhcyB0c2xpYl8xIGZyb20gXCJ0c2xpYlwiO1xuaW1wb3J0IHsgY29uc29sZVNhbmRib3gsIGdldEdsb2JhbE9iamVjdCwgaXNOb2RlRW52LCBsb2dnZXIsIHRpbWVzdGFtcFdpdGhNcywgdXVpZDQgfSBmcm9tICdAc2VudHJ5L3V0aWxzJztcbmltcG9ydCB7IFNjb3BlIH0gZnJvbSAnLi9zY29wZSc7XG4vKipcbiAqIEFQSSBjb21wYXRpYmlsaXR5IHZlcnNpb24gb2YgdGhpcyBodWIuXG4gKlxuICogV0FSTklORzogVGhpcyBudW1iZXIgc2hvdWxkIG9ubHkgYmUgaW5jcmVzZWQgd2hlbiB0aGUgZ2xvYmFsIGludGVyZmFjZVxuICogY2hhbmdlcyBhIGFuZCBuZXcgbWV0aG9kcyBhcmUgaW50cm9kdWNlZC5cbiAqXG4gKiBAaGlkZGVuXG4gKi9cbmV4cG9ydCB2YXIgQVBJX1ZFUlNJT04gPSAzO1xuLyoqXG4gKiBEZWZhdWx0IG1heGltdW0gbnVtYmVyIG9mIGJyZWFkY3J1bWJzIGFkZGVkIHRvIGFuIGV2ZW50LiBDYW4gYmUgb3ZlcndyaXR0ZW5cbiAqIHdpdGgge0BsaW5rIE9wdGlvbnMubWF4QnJlYWRjcnVtYnN9LlxuICovXG52YXIgREVGQVVMVF9CUkVBRENSVU1CUyA9IDEwMDtcbi8qKlxuICogQWJzb2x1dGUgbWF4aW11bSBudW1iZXIgb2YgYnJlYWRjcnVtYnMgYWRkZWQgdG8gYW4gZXZlbnQuIFRoZVxuICogYG1heEJyZWFkY3J1bWJzYCBvcHRpb24gY2Fubm90IGJlIGhpZ2hlciB0aGFuIHRoaXMgdmFsdWUuXG4gKi9cbnZhciBNQVhfQlJFQURDUlVNQlMgPSAxMDA7XG4vKipcbiAqIEBpbmhlcml0RG9jXG4gKi9cbnZhciBIdWIgPSAvKiogQGNsYXNzICovIChmdW5jdGlvbiAoKSB7XG4gICAgLyoqXG4gICAgICogQ3JlYXRlcyBhIG5ldyBpbnN0YW5jZSBvZiB0aGUgaHViLCB3aWxsIHB1c2ggb25lIHtAbGluayBMYXllcn0gaW50byB0aGVcbiAgICAgKiBpbnRlcm5hbCBzdGFjayBvbiBjcmVhdGlvbi5cbiAgICAgKlxuICAgICAqIEBwYXJhbSBjbGllbnQgYm91bmQgdG8gdGhlIGh1Yi5cbiAgICAgKiBAcGFyYW0gc2NvcGUgYm91bmQgdG8gdGhlIGh1Yi5cbiAgICAgKiBAcGFyYW0gdmVyc2lvbiBudW1iZXIsIGhpZ2hlciBudW1iZXIgbWVhbnMgaGlnaGVyIHByaW9yaXR5LlxuICAgICAqL1xuICAgIGZ1bmN0aW9uIEh1YihjbGllbnQsIHNjb3BlLCBfdmVyc2lvbikge1xuICAgICAgICBpZiAoc2NvcGUgPT09IHZvaWQgMCkgeyBzY29wZSA9IG5ldyBTY29wZSgpOyB9XG4gICAgICAgIGlmIChfdmVyc2lvbiA9PT0gdm9pZCAwKSB7IF92ZXJzaW9uID0gQVBJX1ZFUlNJT047IH1cbiAgICAgICAgdGhpcy5fdmVyc2lvbiA9IF92ZXJzaW9uO1xuICAgICAgICAvKiogSXMgYSB7QGxpbmsgTGF5ZXJ9W10gY29udGFpbmluZyB0aGUgY2xpZW50IGFuZCBzY29wZSAqL1xuICAgICAgICB0aGlzLl9zdGFjayA9IFtdO1xuICAgICAgICB0aGlzLl9zdGFjay5wdXNoKHsgY2xpZW50OiBjbGllbnQsIHNjb3BlOiBzY29wZSB9KTtcbiAgICAgICAgdGhpcy5iaW5kQ2xpZW50KGNsaWVudCk7XG4gICAgfVxuICAgIC8qKlxuICAgICAqIEludGVybmFsIGhlbHBlciBmdW5jdGlvbiB0byBjYWxsIGEgbWV0aG9kIG9uIHRoZSB0b3AgY2xpZW50IGlmIGl0IGV4aXN0cy5cbiAgICAgKlxuICAgICAqIEBwYXJhbSBtZXRob2QgVGhlIG1ldGhvZCB0byBjYWxsIG9uIHRoZSBjbGllbnQuXG4gICAgICogQHBhcmFtIGFyZ3MgQXJndW1lbnRzIHRvIHBhc3MgdG8gdGhlIGNsaWVudCBmdW5jdGlvbi5cbiAgICAgKi9cbiAgICBIdWIucHJvdG90eXBlLl9pbnZva2VDbGllbnQgPSBmdW5jdGlvbiAobWV0aG9kKSB7XG4gICAgICAgIHZhciBfYTtcbiAgICAgICAgdmFyIGFyZ3MgPSBbXTtcbiAgICAgICAgZm9yICh2YXIgX2kgPSAxOyBfaSA8IGFyZ3VtZW50cy5sZW5ndGg7IF9pKyspIHtcbiAgICAgICAgICAgIGFyZ3NbX2kgLSAxXSA9IGFyZ3VtZW50c1tfaV07XG4gICAgICAgIH1cbiAgICAgICAgdmFyIHRvcCA9IHRoaXMuZ2V0U3RhY2tUb3AoKTtcbiAgICAgICAgaWYgKHRvcCAmJiB0b3AuY2xpZW50ICYmIHRvcC5jbGllbnRbbWV0aG9kXSkge1xuICAgICAgICAgICAgKF9hID0gdG9wLmNsaWVudClbbWV0aG9kXS5hcHBseShfYSwgdHNsaWJfMS5fX3NwcmVhZChhcmdzLCBbdG9wLnNjb3BlXSkpO1xuICAgICAgICB9XG4gICAgfTtcbiAgICAvKipcbiAgICAgKiBAaW5oZXJpdERvY1xuICAgICAqL1xuICAgIEh1Yi5wcm90b3R5cGUuaXNPbGRlclRoYW4gPSBmdW5jdGlvbiAodmVyc2lvbikge1xuICAgICAgICByZXR1cm4gdGhpcy5fdmVyc2lvbiA8IHZlcnNpb247XG4gICAgfTtcbiAgICAvKipcbiAgICAgKiBAaW5oZXJpdERvY1xuICAgICAqL1xuICAgIEh1Yi5wcm90b3R5cGUuYmluZENsaWVudCA9IGZ1bmN0aW9uIChjbGllbnQpIHtcbiAgICAgICAgdmFyIHRvcCA9IHRoaXMuZ2V0U3RhY2tUb3AoKTtcbiAgICAgICAgdG9wLmNsaWVudCA9IGNsaWVudDtcbiAgICAgICAgaWYgKGNsaWVudCAmJiBjbGllbnQuc2V0dXBJbnRlZ3JhdGlvbnMpIHtcbiAgICAgICAgICAgIGNsaWVudC5zZXR1cEludGVncmF0aW9ucygpO1xuICAgICAgICB9XG4gICAgfTtcbiAgICAvKipcbiAgICAgKiBAaW5oZXJpdERvY1xuICAgICAqL1xuICAgIEh1Yi5wcm90b3R5cGUucHVzaFNjb3BlID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAvLyBXZSB3YW50IHRvIGNsb25lIHRoZSBjb250ZW50IG9mIHByZXYgc2NvcGVcbiAgICAgICAgdmFyIHN0YWNrID0gdGhpcy5nZXRTdGFjaygpO1xuICAgICAgICB2YXIgcGFyZW50U2NvcGUgPSBzdGFjay5sZW5ndGggPiAwID8gc3RhY2tbc3RhY2subGVuZ3RoIC0gMV0uc2NvcGUgOiB1bmRlZmluZWQ7XG4gICAgICAgIHZhciBzY29wZSA9IFNjb3BlLmNsb25lKHBhcmVudFNjb3BlKTtcbiAgICAgICAgdGhpcy5nZXRTdGFjaygpLnB1c2goe1xuICAgICAgICAgICAgY2xpZW50OiB0aGlzLmdldENsaWVudCgpLFxuICAgICAgICAgICAgc2NvcGU6IHNjb3BlLFxuICAgICAgICB9KTtcbiAgICAgICAgcmV0dXJuIHNjb3BlO1xuICAgIH07XG4gICAgLyoqXG4gICAgICogQGluaGVyaXREb2NcbiAgICAgKi9cbiAgICBIdWIucHJvdG90eXBlLnBvcFNjb3BlID0gZnVuY3Rpb24gKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5nZXRTdGFjaygpLnBvcCgpICE9PSB1bmRlZmluZWQ7XG4gICAgfTtcbiAgICAvKipcbiAgICAgKiBAaW5oZXJpdERvY1xuICAgICAqL1xuICAgIEh1Yi5wcm90b3R5cGUud2l0aFNjb3BlID0gZnVuY3Rpb24gKGNhbGxiYWNrKSB7XG4gICAgICAgIHZhciBzY29wZSA9IHRoaXMucHVzaFNjb3BlKCk7XG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICBjYWxsYmFjayhzY29wZSk7XG4gICAgICAgIH1cbiAgICAgICAgZmluYWxseSB7XG4gICAgICAgICAgICB0aGlzLnBvcFNjb3BlKCk7XG4gICAgICAgIH1cbiAgICB9O1xuICAgIC8qKlxuICAgICAqIEBpbmhlcml0RG9jXG4gICAgICovXG4gICAgSHViLnByb3RvdHlwZS5nZXRDbGllbnQgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmdldFN0YWNrVG9wKCkuY2xpZW50O1xuICAgIH07XG4gICAgLyoqIFJldHVybnMgdGhlIHNjb3BlIG9mIHRoZSB0b3Agc3RhY2suICovXG4gICAgSHViLnByb3RvdHlwZS5nZXRTY29wZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuZ2V0U3RhY2tUb3AoKS5zY29wZTtcbiAgICB9O1xuICAgIC8qKiBSZXR1cm5zIHRoZSBzY29wZSBzdGFjayBmb3IgZG9tYWlucyBvciB0aGUgcHJvY2Vzcy4gKi9cbiAgICBIdWIucHJvdG90eXBlLmdldFN0YWNrID0gZnVuY3Rpb24gKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5fc3RhY2s7XG4gICAgfTtcbiAgICAvKiogUmV0dXJucyB0aGUgdG9wbW9zdCBzY29wZSBsYXllciBpbiB0aGUgb3JkZXIgZG9tYWluID4gbG9jYWwgPiBwcm9jZXNzLiAqL1xuICAgIEh1Yi5wcm90b3R5cGUuZ2V0U3RhY2tUb3AgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLl9zdGFja1t0aGlzLl9zdGFjay5sZW5ndGggLSAxXTtcbiAgICB9O1xuICAgIC8qKlxuICAgICAqIEBpbmhlcml0RG9jXG4gICAgICovXG4gICAgSHViLnByb3RvdHlwZS5jYXB0dXJlRXhjZXB0aW9uID0gZnVuY3Rpb24gKGV4Y2VwdGlvbiwgaGludCkge1xuICAgICAgICB2YXIgZXZlbnRJZCA9ICh0aGlzLl9sYXN0RXZlbnRJZCA9IHV1aWQ0KCkpO1xuICAgICAgICB2YXIgZmluYWxIaW50ID0gaGludDtcbiAgICAgICAgLy8gSWYgdGhlcmUncyBubyBleHBsaWNpdCBoaW50IHByb3ZpZGVkLCBtaW1pY2sgdGhlIHNhbWUgdGhpbmcgdGhhdCB3b3VsZCBoYXBwZW5cbiAgICAgICAgLy8gaW4gdGhlIG1pbmltYWwgaXRzZWxmIHRvIGNyZWF0ZSBhIGNvbnNpc3RlbnQgYmVoYXZpb3IuXG4gICAgICAgIC8vIFdlIGRvbid0IGRvIHRoaXMgaW4gdGhlIGNsaWVudCwgYXMgaXQncyB0aGUgbG93ZXN0IGxldmVsIEFQSSwgYW5kIGRvaW5nIHRoaXMsXG4gICAgICAgIC8vIHdvdWxkIHByZXZlbnQgdXNlciBmcm9tIGhhdmluZyBmdWxsIGNvbnRyb2wgb3ZlciBkaXJlY3QgY2FsbHMuXG4gICAgICAgIGlmICghaGludCkge1xuICAgICAgICAgICAgdmFyIHN5bnRoZXRpY0V4Y2VwdGlvbiA9IHZvaWQgMDtcbiAgICAgICAgICAgIHRyeSB7XG4gICAgICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdTZW50cnkgc3ludGhldGljRXhjZXB0aW9uJyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBjYXRjaCAoZXhjZXB0aW9uKSB7XG4gICAgICAgICAgICAgICAgc3ludGhldGljRXhjZXB0aW9uID0gZXhjZXB0aW9uO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZmluYWxIaW50ID0ge1xuICAgICAgICAgICAgICAgIG9yaWdpbmFsRXhjZXB0aW9uOiBleGNlcHRpb24sXG4gICAgICAgICAgICAgICAgc3ludGhldGljRXhjZXB0aW9uOiBzeW50aGV0aWNFeGNlcHRpb24sXG4gICAgICAgICAgICB9O1xuICAgICAgICB9XG4gICAgICAgIHRoaXMuX2ludm9rZUNsaWVudCgnY2FwdHVyZUV4Y2VwdGlvbicsIGV4Y2VwdGlvbiwgdHNsaWJfMS5fX2Fzc2lnbih7fSwgZmluYWxIaW50LCB7IGV2ZW50X2lkOiBldmVudElkIH0pKTtcbiAgICAgICAgcmV0dXJuIGV2ZW50SWQ7XG4gICAgfTtcbiAgICAvKipcbiAgICAgKiBAaW5oZXJpdERvY1xuICAgICAqL1xuICAgIEh1Yi5wcm90b3R5cGUuY2FwdHVyZU1lc3NhZ2UgPSBmdW5jdGlvbiAobWVzc2FnZSwgbGV2ZWwsIGhpbnQpIHtcbiAgICAgICAgdmFyIGV2ZW50SWQgPSAodGhpcy5fbGFzdEV2ZW50SWQgPSB1dWlkNCgpKTtcbiAgICAgICAgdmFyIGZpbmFsSGludCA9IGhpbnQ7XG4gICAgICAgIC8vIElmIHRoZXJlJ3Mgbm8gZXhwbGljaXQgaGludCBwcm92aWRlZCwgbWltaWNrIHRoZSBzYW1lIHRoaW5nIHRoYXQgd291bGQgaGFwcGVuXG4gICAgICAgIC8vIGluIHRoZSBtaW5pbWFsIGl0c2VsZiB0byBjcmVhdGUgYSBjb25zaXN0ZW50IGJlaGF2aW9yLlxuICAgICAgICAvLyBXZSBkb24ndCBkbyB0aGlzIGluIHRoZSBjbGllbnQsIGFzIGl0J3MgdGhlIGxvd2VzdCBsZXZlbCBBUEksIGFuZCBkb2luZyB0aGlzLFxuICAgICAgICAvLyB3b3VsZCBwcmV2ZW50IHVzZXIgZnJvbSBoYXZpbmcgZnVsbCBjb250cm9sIG92ZXIgZGlyZWN0IGNhbGxzLlxuICAgICAgICBpZiAoIWhpbnQpIHtcbiAgICAgICAgICAgIHZhciBzeW50aGV0aWNFeGNlcHRpb24gPSB2b2lkIDA7XG4gICAgICAgICAgICB0cnkge1xuICAgICAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihtZXNzYWdlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGNhdGNoIChleGNlcHRpb24pIHtcbiAgICAgICAgICAgICAgICBzeW50aGV0aWNFeGNlcHRpb24gPSBleGNlcHRpb247XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBmaW5hbEhpbnQgPSB7XG4gICAgICAgICAgICAgICAgb3JpZ2luYWxFeGNlcHRpb246IG1lc3NhZ2UsXG4gICAgICAgICAgICAgICAgc3ludGhldGljRXhjZXB0aW9uOiBzeW50aGV0aWNFeGNlcHRpb24sXG4gICAgICAgICAgICB9O1xuICAgICAgICB9XG4gICAgICAgIHRoaXMuX2ludm9rZUNsaWVudCgnY2FwdHVyZU1lc3NhZ2UnLCBtZXNzYWdlLCBsZXZlbCwgdHNsaWJfMS5fX2Fzc2lnbih7fSwgZmluYWxIaW50LCB7IGV2ZW50X2lkOiBldmVudElkIH0pKTtcbiAgICAgICAgcmV0dXJuIGV2ZW50SWQ7XG4gICAgfTtcbiAgICAvKipcbiAgICAgKiBAaW5oZXJpdERvY1xuICAgICAqL1xuICAgIEh1Yi5wcm90b3R5cGUuY2FwdHVyZUV2ZW50ID0gZnVuY3Rpb24gKGV2ZW50LCBoaW50KSB7XG4gICAgICAgIHZhciBldmVudElkID0gKHRoaXMuX2xhc3RFdmVudElkID0gdXVpZDQoKSk7XG4gICAgICAgIHRoaXMuX2ludm9rZUNsaWVudCgnY2FwdHVyZUV2ZW50JywgZXZlbnQsIHRzbGliXzEuX19hc3NpZ24oe30sIGhpbnQsIHsgZXZlbnRfaWQ6IGV2ZW50SWQgfSkpO1xuICAgICAgICByZXR1cm4gZXZlbnRJZDtcbiAgICB9O1xuICAgIC8qKlxuICAgICAqIEBpbmhlcml0RG9jXG4gICAgICovXG4gICAgSHViLnByb3RvdHlwZS5sYXN0RXZlbnRJZCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX2xhc3RFdmVudElkO1xuICAgIH07XG4gICAgLyoqXG4gICAgICogQGluaGVyaXREb2NcbiAgICAgKi9cbiAgICBIdWIucHJvdG90eXBlLmFkZEJyZWFkY3J1bWIgPSBmdW5jdGlvbiAoYnJlYWRjcnVtYiwgaGludCkge1xuICAgICAgICB2YXIgdG9wID0gdGhpcy5nZXRTdGFja1RvcCgpO1xuICAgICAgICBpZiAoIXRvcC5zY29wZSB8fCAhdG9wLmNsaWVudCkge1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIHZhciBfYSA9ICh0b3AuY2xpZW50LmdldE9wdGlvbnMgJiYgdG9wLmNsaWVudC5nZXRPcHRpb25zKCkpIHx8IHt9LCBfYiA9IF9hLmJlZm9yZUJyZWFkY3J1bWIsIGJlZm9yZUJyZWFkY3J1bWIgPSBfYiA9PT0gdm9pZCAwID8gbnVsbCA6IF9iLCBfYyA9IF9hLm1heEJyZWFkY3J1bWJzLCBtYXhCcmVhZGNydW1icyA9IF9jID09PSB2b2lkIDAgPyBERUZBVUxUX0JSRUFEQ1JVTUJTIDogX2M7XG4gICAgICAgIGlmIChtYXhCcmVhZGNydW1icyA8PSAwKSB7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICAgICAgdmFyIHRpbWVzdGFtcCA9IHRpbWVzdGFtcFdpdGhNcygpO1xuICAgICAgICB2YXIgbWVyZ2VkQnJlYWRjcnVtYiA9IHRzbGliXzEuX19hc3NpZ24oeyB0aW1lc3RhbXA6IHRpbWVzdGFtcCB9LCBicmVhZGNydW1iKTtcbiAgICAgICAgdmFyIGZpbmFsQnJlYWRjcnVtYiA9IGJlZm9yZUJyZWFkY3J1bWJcbiAgICAgICAgICAgID8gY29uc29sZVNhbmRib3goZnVuY3Rpb24gKCkgeyByZXR1cm4gYmVmb3JlQnJlYWRjcnVtYihtZXJnZWRCcmVhZGNydW1iLCBoaW50KTsgfSlcbiAgICAgICAgICAgIDogbWVyZ2VkQnJlYWRjcnVtYjtcbiAgICAgICAgaWYgKGZpbmFsQnJlYWRjcnVtYiA9PT0gbnVsbCkge1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIHRvcC5zY29wZS5hZGRCcmVhZGNydW1iKGZpbmFsQnJlYWRjcnVtYiwgTWF0aC5taW4obWF4QnJlYWRjcnVtYnMsIE1BWF9CUkVBRENSVU1CUykpO1xuICAgIH07XG4gICAgLyoqXG4gICAgICogQGluaGVyaXREb2NcbiAgICAgKi9cbiAgICBIdWIucHJvdG90eXBlLnNldFVzZXIgPSBmdW5jdGlvbiAodXNlcikge1xuICAgICAgICB2YXIgdG9wID0gdGhpcy5nZXRTdGFja1RvcCgpO1xuICAgICAgICBpZiAoIXRvcC5zY29wZSkge1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIHRvcC5zY29wZS5zZXRVc2VyKHVzZXIpO1xuICAgIH07XG4gICAgLyoqXG4gICAgICogQGluaGVyaXREb2NcbiAgICAgKi9cbiAgICBIdWIucHJvdG90eXBlLnNldFRhZ3MgPSBmdW5jdGlvbiAodGFncykge1xuICAgICAgICB2YXIgdG9wID0gdGhpcy5nZXRTdGFja1RvcCgpO1xuICAgICAgICBpZiAoIXRvcC5zY29wZSkge1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIHRvcC5zY29wZS5zZXRUYWdzKHRhZ3MpO1xuICAgIH07XG4gICAgLyoqXG4gICAgICogQGluaGVyaXREb2NcbiAgICAgKi9cbiAgICBIdWIucHJvdG90eXBlLnNldEV4dHJhcyA9IGZ1bmN0aW9uIChleHRyYXMpIHtcbiAgICAgICAgdmFyIHRvcCA9IHRoaXMuZ2V0U3RhY2tUb3AoKTtcbiAgICAgICAgaWYgKCF0b3Auc2NvcGUpIHtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuICAgICAgICB0b3Auc2NvcGUuc2V0RXh0cmFzKGV4dHJhcyk7XG4gICAgfTtcbiAgICAvKipcbiAgICAgKiBAaW5oZXJpdERvY1xuICAgICAqL1xuICAgIEh1Yi5wcm90b3R5cGUuc2V0VGFnID0gZnVuY3Rpb24gKGtleSwgdmFsdWUpIHtcbiAgICAgICAgdmFyIHRvcCA9IHRoaXMuZ2V0U3RhY2tUb3AoKTtcbiAgICAgICAgaWYgKCF0b3Auc2NvcGUpIHtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuICAgICAgICB0b3Auc2NvcGUuc2V0VGFnKGtleSwgdmFsdWUpO1xuICAgIH07XG4gICAgLyoqXG4gICAgICogQGluaGVyaXREb2NcbiAgICAgKi9cbiAgICBIdWIucHJvdG90eXBlLnNldEV4dHJhID0gZnVuY3Rpb24gKGtleSwgZXh0cmEpIHtcbiAgICAgICAgdmFyIHRvcCA9IHRoaXMuZ2V0U3RhY2tUb3AoKTtcbiAgICAgICAgaWYgKCF0b3Auc2NvcGUpIHtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuICAgICAgICB0b3Auc2NvcGUuc2V0RXh0cmEoa2V5LCBleHRyYSk7XG4gICAgfTtcbiAgICAvKipcbiAgICAgKiBAaW5oZXJpdERvY1xuICAgICAqL1xuICAgIEh1Yi5wcm90b3R5cGUuc2V0Q29udGV4dCA9IGZ1bmN0aW9uIChuYW1lLCBjb250ZXh0KSB7XG4gICAgICAgIHZhciB0b3AgPSB0aGlzLmdldFN0YWNrVG9wKCk7XG4gICAgICAgIGlmICghdG9wLnNjb3BlKSB7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICAgICAgdG9wLnNjb3BlLnNldENvbnRleHQobmFtZSwgY29udGV4dCk7XG4gICAgfTtcbiAgICAvKipcbiAgICAgKiBAaW5oZXJpdERvY1xuICAgICAqL1xuICAgIEh1Yi5wcm90b3R5cGUuY29uZmlndXJlU2NvcGUgPSBmdW5jdGlvbiAoY2FsbGJhY2spIHtcbiAgICAgICAgdmFyIHRvcCA9IHRoaXMuZ2V0U3RhY2tUb3AoKTtcbiAgICAgICAgaWYgKHRvcC5zY29wZSAmJiB0b3AuY2xpZW50KSB7XG4gICAgICAgICAgICBjYWxsYmFjayh0b3Auc2NvcGUpO1xuICAgICAgICB9XG4gICAgfTtcbiAgICAvKipcbiAgICAgKiBAaW5oZXJpdERvY1xuICAgICAqL1xuICAgIEh1Yi5wcm90b3R5cGUucnVuID0gZnVuY3Rpb24gKGNhbGxiYWNrKSB7XG4gICAgICAgIHZhciBvbGRIdWIgPSBtYWtlTWFpbih0aGlzKTtcbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIGNhbGxiYWNrKHRoaXMpO1xuICAgICAgICB9XG4gICAgICAgIGZpbmFsbHkge1xuICAgICAgICAgICAgbWFrZU1haW4ob2xkSHViKTtcbiAgICAgICAgfVxuICAgIH07XG4gICAgLyoqXG4gICAgICogQGluaGVyaXREb2NcbiAgICAgKi9cbiAgICBIdWIucHJvdG90eXBlLmdldEludGVncmF0aW9uID0gZnVuY3Rpb24gKGludGVncmF0aW9uKSB7XG4gICAgICAgIHZhciBjbGllbnQgPSB0aGlzLmdldENsaWVudCgpO1xuICAgICAgICBpZiAoIWNsaWVudCkge1xuICAgICAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgICAgIH1cbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIHJldHVybiBjbGllbnQuZ2V0SW50ZWdyYXRpb24oaW50ZWdyYXRpb24pO1xuICAgICAgICB9XG4gICAgICAgIGNhdGNoIChfb08pIHtcbiAgICAgICAgICAgIGxvZ2dlci53YXJuKFwiQ2Fubm90IHJldHJpZXZlIGludGVncmF0aW9uIFwiICsgaW50ZWdyYXRpb24uaWQgKyBcIiBmcm9tIHRoZSBjdXJyZW50IEh1YlwiKTtcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xuICAgICAgICB9XG4gICAgfTtcbiAgICAvKipcbiAgICAgKiBAaW5oZXJpdERvY1xuICAgICAqL1xuICAgIEh1Yi5wcm90b3R5cGUuc3RhcnRTcGFuID0gZnVuY3Rpb24gKGNvbnRleHQpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX2NhbGxFeHRlbnNpb25NZXRob2QoJ3N0YXJ0U3BhbicsIGNvbnRleHQpO1xuICAgIH07XG4gICAgLyoqXG4gICAgICogQGluaGVyaXREb2NcbiAgICAgKi9cbiAgICBIdWIucHJvdG90eXBlLnN0YXJ0VHJhbnNhY3Rpb24gPSBmdW5jdGlvbiAoY29udGV4dCkge1xuICAgICAgICByZXR1cm4gdGhpcy5fY2FsbEV4dGVuc2lvbk1ldGhvZCgnc3RhcnRUcmFuc2FjdGlvbicsIGNvbnRleHQpO1xuICAgIH07XG4gICAgLyoqXG4gICAgICogQGluaGVyaXREb2NcbiAgICAgKi9cbiAgICBIdWIucHJvdG90eXBlLnRyYWNlSGVhZGVycyA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX2NhbGxFeHRlbnNpb25NZXRob2QoJ3RyYWNlSGVhZGVycycpO1xuICAgIH07XG4gICAgLyoqXG4gICAgICogQ2FsbHMgZ2xvYmFsIGV4dGVuc2lvbiBtZXRob2QgYW5kIGJpbmRpbmcgY3VycmVudCBpbnN0YW5jZSB0byB0aGUgZnVuY3Rpb24gY2FsbFxuICAgICAqL1xuICAgIC8vIEB0cy1pZ25vcmVcbiAgICBIdWIucHJvdG90eXBlLl9jYWxsRXh0ZW5zaW9uTWV0aG9kID0gZnVuY3Rpb24gKG1ldGhvZCkge1xuICAgICAgICB2YXIgYXJncyA9IFtdO1xuICAgICAgICBmb3IgKHZhciBfaSA9IDE7IF9pIDwgYXJndW1lbnRzLmxlbmd0aDsgX2krKykge1xuICAgICAgICAgICAgYXJnc1tfaSAtIDFdID0gYXJndW1lbnRzW19pXTtcbiAgICAgICAgfVxuICAgICAgICB2YXIgY2FycmllciA9IGdldE1haW5DYXJyaWVyKCk7XG4gICAgICAgIHZhciBzZW50cnkgPSBjYXJyaWVyLl9fU0VOVFJZX187XG4gICAgICAgIC8vIHRzbGludDpkaXNhYmxlLW5leHQtbGluZTogc3RyaWN0LXR5cGUtcHJlZGljYXRlc1xuICAgICAgICBpZiAoc2VudHJ5ICYmIHNlbnRyeS5leHRlbnNpb25zICYmIHR5cGVvZiBzZW50cnkuZXh0ZW5zaW9uc1ttZXRob2RdID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgICAgICByZXR1cm4gc2VudHJ5LmV4dGVuc2lvbnNbbWV0aG9kXS5hcHBseSh0aGlzLCBhcmdzKTtcbiAgICAgICAgfVxuICAgICAgICBsb2dnZXIud2FybihcIkV4dGVuc2lvbiBtZXRob2QgXCIgKyBtZXRob2QgKyBcIiBjb3VsZG4ndCBiZSBmb3VuZCwgZG9pbmcgbm90aGluZy5cIik7XG4gICAgfTtcbiAgICByZXR1cm4gSHViO1xufSgpKTtcbmV4cG9ydCB7IEh1YiB9O1xuLyoqIFJldHVybnMgdGhlIGdsb2JhbCBzaGltIHJlZ2lzdHJ5LiAqL1xuZXhwb3J0IGZ1bmN0aW9uIGdldE1haW5DYXJyaWVyKCkge1xuICAgIHZhciBjYXJyaWVyID0gZ2V0R2xvYmFsT2JqZWN0KCk7XG4gICAgY2Fycmllci5fX1NFTlRSWV9fID0gY2Fycmllci5fX1NFTlRSWV9fIHx8IHtcbiAgICAgICAgZXh0ZW5zaW9uczoge30sXG4gICAgICAgIGh1YjogdW5kZWZpbmVkLFxuICAgIH07XG4gICAgcmV0dXJuIGNhcnJpZXI7XG59XG4vKipcbiAqIFJlcGxhY2VzIHRoZSBjdXJyZW50IG1haW4gaHViIHdpdGggdGhlIHBhc3NlZCBvbmUgb24gdGhlIGdsb2JhbCBvYmplY3RcbiAqXG4gKiBAcmV0dXJucyBUaGUgb2xkIHJlcGxhY2VkIGh1YlxuICovXG5leHBvcnQgZnVuY3Rpb24gbWFrZU1haW4oaHViKSB7XG4gICAgdmFyIHJlZ2lzdHJ5ID0gZ2V0TWFpbkNhcnJpZXIoKTtcbiAgICB2YXIgb2xkSHViID0gZ2V0SHViRnJvbUNhcnJpZXIocmVnaXN0cnkpO1xuICAgIHNldEh1Yk9uQ2FycmllcihyZWdpc3RyeSwgaHViKTtcbiAgICByZXR1cm4gb2xkSHViO1xufVxuLyoqXG4gKiBSZXR1cm5zIHRoZSBkZWZhdWx0IGh1YiBpbnN0YW5jZS5cbiAqXG4gKiBJZiBhIGh1YiBpcyBhbHJlYWR5IHJlZ2lzdGVyZWQgaW4gdGhlIGdsb2JhbCBjYXJyaWVyIGJ1dCB0aGlzIG1vZHVsZVxuICogY29udGFpbnMgYSBtb3JlIHJlY2VudCB2ZXJzaW9uLCBpdCByZXBsYWNlcyB0aGUgcmVnaXN0ZXJlZCB2ZXJzaW9uLlxuICogT3RoZXJ3aXNlLCB0aGUgY3VycmVudGx5IHJlZ2lzdGVyZWQgaHViIHdpbGwgYmUgcmV0dXJuZWQuXG4gKi9cbmV4cG9ydCBmdW5jdGlvbiBnZXRDdXJyZW50SHViKCkge1xuICAgIC8vIEdldCBtYWluIGNhcnJpZXIgKGdsb2JhbCBmb3IgZXZlcnkgZW52aXJvbm1lbnQpXG4gICAgdmFyIHJlZ2lzdHJ5ID0gZ2V0TWFpbkNhcnJpZXIoKTtcbiAgICAvLyBJZiB0aGVyZSdzIG5vIGh1Yiwgb3IgaXRzIGFuIG9sZCBBUEksIGFzc2lnbiBhIG5ldyBvbmVcbiAgICBpZiAoIWhhc0h1Yk9uQ2FycmllcihyZWdpc3RyeSkgfHwgZ2V0SHViRnJvbUNhcnJpZXIocmVnaXN0cnkpLmlzT2xkZXJUaGFuKEFQSV9WRVJTSU9OKSkge1xuICAgICAgICBzZXRIdWJPbkNhcnJpZXIocmVnaXN0cnksIG5ldyBIdWIoKSk7XG4gICAgfVxuICAgIC8vIFByZWZlciBkb21haW5zIG92ZXIgZ2xvYmFsIGlmIHRoZXkgYXJlIHRoZXJlIChhcHBsaWNhYmxlIG9ubHkgdG8gTm9kZSBlbnZpcm9ubWVudClcbiAgICBpZiAoaXNOb2RlRW52KCkpIHtcbiAgICAgICAgcmV0dXJuIGdldEh1YkZyb21BY3RpdmVEb21haW4ocmVnaXN0cnkpO1xuICAgIH1cbiAgICAvLyBSZXR1cm4gaHViIHRoYXQgbGl2ZXMgb24gYSBnbG9iYWwgb2JqZWN0XG4gICAgcmV0dXJuIGdldEh1YkZyb21DYXJyaWVyKHJlZ2lzdHJ5KTtcbn1cbi8qKlxuICogVHJ5IHRvIHJlYWQgdGhlIGh1YiBmcm9tIGFuIGFjdGl2ZSBkb21haW4sIGZhbGxiYWNrIHRvIHRoZSByZWdpc3RyeSBpZiBvbmUgZG9lc250IGV4aXN0XG4gKiBAcmV0dXJucyBkaXNjb3ZlcmVkIGh1YlxuICovXG5mdW5jdGlvbiBnZXRIdWJGcm9tQWN0aXZlRG9tYWluKHJlZ2lzdHJ5KSB7XG4gICAgdHJ5IHtcbiAgICAgICAgdmFyIHByb3BlcnR5ID0gJ2RvbWFpbic7XG4gICAgICAgIHZhciBjYXJyaWVyID0gZ2V0TWFpbkNhcnJpZXIoKTtcbiAgICAgICAgdmFyIHNlbnRyeSA9IGNhcnJpZXIuX19TRU5UUllfXztcbiAgICAgICAgLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOiBzdHJpY3QtdHlwZS1wcmVkaWNhdGVzXG4gICAgICAgIGlmICghc2VudHJ5IHx8ICFzZW50cnkuZXh0ZW5zaW9ucyB8fCAhc2VudHJ5LmV4dGVuc2lvbnNbcHJvcGVydHldKSB7XG4gICAgICAgICAgICByZXR1cm4gZ2V0SHViRnJvbUNhcnJpZXIocmVnaXN0cnkpO1xuICAgICAgICB9XG4gICAgICAgIHZhciBkb21haW4gPSBzZW50cnkuZXh0ZW5zaW9uc1twcm9wZXJ0eV07XG4gICAgICAgIHZhciBhY3RpdmVEb21haW4gPSBkb21haW4uYWN0aXZlO1xuICAgICAgICAvLyBJZiB0aGVyZSBubyBhY3RpdmUgZG9tYWluLCBqdXN0IHJldHVybiBnbG9iYWwgaHViXG4gICAgICAgIGlmICghYWN0aXZlRG9tYWluKSB7XG4gICAgICAgICAgICByZXR1cm4gZ2V0SHViRnJvbUNhcnJpZXIocmVnaXN0cnkpO1xuICAgICAgICB9XG4gICAgICAgIC8vIElmIHRoZXJlJ3Mgbm8gaHViIG9uIGN1cnJlbnQgZG9tYWluLCBvciBpdHMgYW4gb2xkIEFQSSwgYXNzaWduIGEgbmV3IG9uZVxuICAgICAgICBpZiAoIWhhc0h1Yk9uQ2FycmllcihhY3RpdmVEb21haW4pIHx8IGdldEh1YkZyb21DYXJyaWVyKGFjdGl2ZURvbWFpbikuaXNPbGRlclRoYW4oQVBJX1ZFUlNJT04pKSB7XG4gICAgICAgICAgICB2YXIgcmVnaXN0cnlIdWJUb3BTdGFjayA9IGdldEh1YkZyb21DYXJyaWVyKHJlZ2lzdHJ5KS5nZXRTdGFja1RvcCgpO1xuICAgICAgICAgICAgc2V0SHViT25DYXJyaWVyKGFjdGl2ZURvbWFpbiwgbmV3IEh1YihyZWdpc3RyeUh1YlRvcFN0YWNrLmNsaWVudCwgU2NvcGUuY2xvbmUocmVnaXN0cnlIdWJUb3BTdGFjay5zY29wZSkpKTtcbiAgICAgICAgfVxuICAgICAgICAvLyBSZXR1cm4gaHViIHRoYXQgbGl2ZXMgb24gYSBkb21haW5cbiAgICAgICAgcmV0dXJuIGdldEh1YkZyb21DYXJyaWVyKGFjdGl2ZURvbWFpbik7XG4gICAgfVxuICAgIGNhdGNoIChfT28pIHtcbiAgICAgICAgLy8gUmV0dXJuIGh1YiB0aGF0IGxpdmVzIG9uIGEgZ2xvYmFsIG9iamVjdFxuICAgICAgICByZXR1cm4gZ2V0SHViRnJvbUNhcnJpZXIocmVnaXN0cnkpO1xuICAgIH1cbn1cbi8qKlxuICogVGhpcyB3aWxsIHRlbGwgd2hldGhlciBhIGNhcnJpZXIgaGFzIGEgaHViIG9uIGl0IG9yIG5vdFxuICogQHBhcmFtIGNhcnJpZXIgb2JqZWN0XG4gKi9cbmZ1bmN0aW9uIGhhc0h1Yk9uQ2FycmllcihjYXJyaWVyKSB7XG4gICAgaWYgKGNhcnJpZXIgJiYgY2Fycmllci5fX1NFTlRSWV9fICYmIGNhcnJpZXIuX19TRU5UUllfXy5odWIpIHtcbiAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgfVxuICAgIHJldHVybiBmYWxzZTtcbn1cbi8qKlxuICogVGhpcyB3aWxsIGNyZWF0ZSBhIG5ldyB7QGxpbmsgSHVifSBhbmQgYWRkIHRvIHRoZSBwYXNzZWQgb2JqZWN0IG9uXG4gKiBfX1NFTlRSWV9fLmh1Yi5cbiAqIEBwYXJhbSBjYXJyaWVyIG9iamVjdFxuICogQGhpZGRlblxuICovXG5leHBvcnQgZnVuY3Rpb24gZ2V0SHViRnJvbUNhcnJpZXIoY2Fycmllcikge1xuICAgIGlmIChjYXJyaWVyICYmIGNhcnJpZXIuX19TRU5UUllfXyAmJiBjYXJyaWVyLl9fU0VOVFJZX18uaHViKSB7XG4gICAgICAgIHJldHVybiBjYXJyaWVyLl9fU0VOVFJZX18uaHViO1xuICAgIH1cbiAgICBjYXJyaWVyLl9fU0VOVFJZX18gPSBjYXJyaWVyLl9fU0VOVFJZX18gfHwge307XG4gICAgY2Fycmllci5fX1NFTlRSWV9fLmh1YiA9IG5ldyBIdWIoKTtcbiAgICByZXR1cm4gY2Fycmllci5fX1NFTlRSWV9fLmh1Yjtcbn1cbi8qKlxuICogVGhpcyB3aWxsIHNldCBwYXNzZWQge0BsaW5rIEh1Yn0gb24gdGhlIHBhc3NlZCBvYmplY3QncyBfX1NFTlRSWV9fLmh1YiBhdHRyaWJ1dGVcbiAqIEBwYXJhbSBjYXJyaWVyIG9iamVjdFxuICogQHBhcmFtIGh1YiBIdWJcbiAqL1xuZXhwb3J0IGZ1bmN0aW9uIHNldEh1Yk9uQ2FycmllcihjYXJyaWVyLCBodWIpIHtcbiAgICBpZiAoIWNhcnJpZXIpIHtcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cbiAgICBjYXJyaWVyLl9fU0VOVFJZX18gPSBjYXJyaWVyLl9fU0VOVFJZX18gfHwge307XG4gICAgY2Fycmllci5fX1NFTlRSWV9fLmh1YiA9IGh1YjtcbiAgICByZXR1cm4gdHJ1ZTtcbn1cbi8vIyBzb3VyY2VNYXBwaW5nVVJMPWh1Yi5qcy5tYXAiLCJleHBvcnQgeyBhZGRHbG9iYWxFdmVudFByb2Nlc3NvciwgU2NvcGUgfSBmcm9tICcuL3Njb3BlJztcbmV4cG9ydCB7IGdldEN1cnJlbnRIdWIsIGdldEh1YkZyb21DYXJyaWVyLCBnZXRNYWluQ2FycmllciwgSHViLCBtYWtlTWFpbiwgc2V0SHViT25DYXJyaWVyIH0gZnJvbSAnLi9odWInO1xuLy8jIHNvdXJjZU1hcHBpbmdVUkw9aW5kZXguanMubWFwIiwiaW1wb3J0ICogYXMgdHNsaWJfMSBmcm9tIFwidHNsaWJcIjtcbmltcG9ydCB7IGdldEdsb2JhbE9iamVjdCwgaXNQbGFpbk9iamVjdCwgaXNUaGVuYWJsZSwgU3luY1Byb21pc2UsIHRpbWVzdGFtcFdpdGhNcyB9IGZyb20gJ0BzZW50cnkvdXRpbHMnO1xuLyoqXG4gKiBIb2xkcyBhZGRpdGlvbmFsIGV2ZW50IGluZm9ybWF0aW9uLiB7QGxpbmsgU2NvcGUuYXBwbHlUb0V2ZW50fSB3aWxsIGJlXG4gKiBjYWxsZWQgYnkgdGhlIGNsaWVudCBiZWZvcmUgYW4gZXZlbnQgd2lsbCBiZSBzZW50LlxuICovXG52YXIgU2NvcGUgPSAvKiogQGNsYXNzICovIChmdW5jdGlvbiAoKSB7XG4gICAgZnVuY3Rpb24gU2NvcGUoKSB7XG4gICAgICAgIC8qKiBGbGFnIGlmIG5vdGlmaXlpbmcgaXMgaGFwcGVuaW5nLiAqL1xuICAgICAgICB0aGlzLl9ub3RpZnlpbmdMaXN0ZW5lcnMgPSBmYWxzZTtcbiAgICAgICAgLyoqIENhbGxiYWNrIGZvciBjbGllbnQgdG8gcmVjZWl2ZSBzY29wZSBjaGFuZ2VzLiAqL1xuICAgICAgICB0aGlzLl9zY29wZUxpc3RlbmVycyA9IFtdO1xuICAgICAgICAvKiogQ2FsbGJhY2sgbGlzdCB0aGF0IHdpbGwgYmUgY2FsbGVkIGFmdGVyIHtAbGluayBhcHBseVRvRXZlbnR9LiAqL1xuICAgICAgICB0aGlzLl9ldmVudFByb2Nlc3NvcnMgPSBbXTtcbiAgICAgICAgLyoqIEFycmF5IG9mIGJyZWFkY3J1bWJzLiAqL1xuICAgICAgICB0aGlzLl9icmVhZGNydW1icyA9IFtdO1xuICAgICAgICAvKiogVXNlciAqL1xuICAgICAgICB0aGlzLl91c2VyID0ge307XG4gICAgICAgIC8qKiBUYWdzICovXG4gICAgICAgIHRoaXMuX3RhZ3MgPSB7fTtcbiAgICAgICAgLyoqIEV4dHJhICovXG4gICAgICAgIHRoaXMuX2V4dHJhID0ge307XG4gICAgICAgIC8qKiBDb250ZXh0cyAqL1xuICAgICAgICB0aGlzLl9jb250ZXh0cyA9IHt9O1xuICAgIH1cbiAgICAvKipcbiAgICAgKiBBZGQgaW50ZXJuYWwgb24gY2hhbmdlIGxpc3RlbmVyLiBVc2VkIGZvciBzdWIgU0RLcyB0aGF0IG5lZWQgdG8gc3RvcmUgdGhlIHNjb3BlLlxuICAgICAqIEBoaWRkZW5cbiAgICAgKi9cbiAgICBTY29wZS5wcm90b3R5cGUuYWRkU2NvcGVMaXN0ZW5lciA9IGZ1bmN0aW9uIChjYWxsYmFjaykge1xuICAgICAgICB0aGlzLl9zY29wZUxpc3RlbmVycy5wdXNoKGNhbGxiYWNrKTtcbiAgICB9O1xuICAgIC8qKlxuICAgICAqIEBpbmhlcml0RG9jXG4gICAgICovXG4gICAgU2NvcGUucHJvdG90eXBlLmFkZEV2ZW50UHJvY2Vzc29yID0gZnVuY3Rpb24gKGNhbGxiYWNrKSB7XG4gICAgICAgIHRoaXMuX2V2ZW50UHJvY2Vzc29ycy5wdXNoKGNhbGxiYWNrKTtcbiAgICAgICAgcmV0dXJuIHRoaXM7XG4gICAgfTtcbiAgICAvKipcbiAgICAgKiBUaGlzIHdpbGwgYmUgY2FsbGVkIG9uIGV2ZXJ5IHNldCBjYWxsLlxuICAgICAqL1xuICAgIFNjb3BlLnByb3RvdHlwZS5fbm90aWZ5U2NvcGVMaXN0ZW5lcnMgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciBfdGhpcyA9IHRoaXM7XG4gICAgICAgIGlmICghdGhpcy5fbm90aWZ5aW5nTGlzdGVuZXJzKSB7XG4gICAgICAgICAgICB0aGlzLl9ub3RpZnlpbmdMaXN0ZW5lcnMgPSB0cnVlO1xuICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgX3RoaXMuX3Njb3BlTGlzdGVuZXJzLmZvckVhY2goZnVuY3Rpb24gKGNhbGxiYWNrKSB7XG4gICAgICAgICAgICAgICAgICAgIGNhbGxiYWNrKF90aGlzKTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICBfdGhpcy5fbm90aWZ5aW5nTGlzdGVuZXJzID0gZmFsc2U7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgIH07XG4gICAgLyoqXG4gICAgICogVGhpcyB3aWxsIGJlIGNhbGxlZCBhZnRlciB7QGxpbmsgYXBwbHlUb0V2ZW50fSBpcyBmaW5pc2hlZC5cbiAgICAgKi9cbiAgICBTY29wZS5wcm90b3R5cGUuX25vdGlmeUV2ZW50UHJvY2Vzc29ycyA9IGZ1bmN0aW9uIChwcm9jZXNzb3JzLCBldmVudCwgaGludCwgaW5kZXgpIHtcbiAgICAgICAgdmFyIF90aGlzID0gdGhpcztcbiAgICAgICAgaWYgKGluZGV4ID09PSB2b2lkIDApIHsgaW5kZXggPSAwOyB9XG4gICAgICAgIHJldHVybiBuZXcgU3luY1Byb21pc2UoZnVuY3Rpb24gKHJlc29sdmUsIHJlamVjdCkge1xuICAgICAgICAgICAgdmFyIHByb2Nlc3NvciA9IHByb2Nlc3NvcnNbaW5kZXhdO1xuICAgICAgICAgICAgLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOnN0cmljdC10eXBlLXByZWRpY2F0ZXNcbiAgICAgICAgICAgIGlmIChldmVudCA9PT0gbnVsbCB8fCB0eXBlb2YgcHJvY2Vzc29yICE9PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgICAgICAgICAgcmVzb2x2ZShldmVudCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICB2YXIgcmVzdWx0ID0gcHJvY2Vzc29yKHRzbGliXzEuX19hc3NpZ24oe30sIGV2ZW50KSwgaGludCk7XG4gICAgICAgICAgICAgICAgaWYgKGlzVGhlbmFibGUocmVzdWx0KSkge1xuICAgICAgICAgICAgICAgICAgICByZXN1bHRcbiAgICAgICAgICAgICAgICAgICAgICAgIC50aGVuKGZ1bmN0aW9uIChmaW5hbCkgeyByZXR1cm4gX3RoaXMuX25vdGlmeUV2ZW50UHJvY2Vzc29ycyhwcm9jZXNzb3JzLCBmaW5hbCwgaGludCwgaW5kZXggKyAxKS50aGVuKHJlc29sdmUpOyB9KVxuICAgICAgICAgICAgICAgICAgICAgICAgLnRoZW4obnVsbCwgcmVqZWN0KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIF90aGlzLl9ub3RpZnlFdmVudFByb2Nlc3NvcnMocHJvY2Vzc29ycywgcmVzdWx0LCBoaW50LCBpbmRleCArIDEpXG4gICAgICAgICAgICAgICAgICAgICAgICAudGhlbihyZXNvbHZlKVxuICAgICAgICAgICAgICAgICAgICAgICAgLnRoZW4obnVsbCwgcmVqZWN0KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgIH07XG4gICAgLyoqXG4gICAgICogQGluaGVyaXREb2NcbiAgICAgKi9cbiAgICBTY29wZS5wcm90b3R5cGUuc2V0VXNlciA9IGZ1bmN0aW9uICh1c2VyKSB7XG4gICAgICAgIHRoaXMuX3VzZXIgPSB1c2VyIHx8IHt9O1xuICAgICAgICB0aGlzLl9ub3RpZnlTY29wZUxpc3RlbmVycygpO1xuICAgICAgICByZXR1cm4gdGhpcztcbiAgICB9O1xuICAgIC8qKlxuICAgICAqIEBpbmhlcml0RG9jXG4gICAgICovXG4gICAgU2NvcGUucHJvdG90eXBlLnNldFRhZ3MgPSBmdW5jdGlvbiAodGFncykge1xuICAgICAgICB0aGlzLl90YWdzID0gdHNsaWJfMS5fX2Fzc2lnbih7fSwgdGhpcy5fdGFncywgdGFncyk7XG4gICAgICAgIHRoaXMuX25vdGlmeVNjb3BlTGlzdGVuZXJzKCk7XG4gICAgICAgIHJldHVybiB0aGlzO1xuICAgIH07XG4gICAgLyoqXG4gICAgICogQGluaGVyaXREb2NcbiAgICAgKi9cbiAgICBTY29wZS5wcm90b3R5cGUuc2V0VGFnID0gZnVuY3Rpb24gKGtleSwgdmFsdWUpIHtcbiAgICAgICAgdmFyIF9hO1xuICAgICAgICB0aGlzLl90YWdzID0gdHNsaWJfMS5fX2Fzc2lnbih7fSwgdGhpcy5fdGFncywgKF9hID0ge30sIF9hW2tleV0gPSB2YWx1ZSwgX2EpKTtcbiAgICAgICAgdGhpcy5fbm90aWZ5U2NvcGVMaXN0ZW5lcnMoKTtcbiAgICAgICAgcmV0dXJuIHRoaXM7XG4gICAgfTtcbiAgICAvKipcbiAgICAgKiBAaW5oZXJpdERvY1xuICAgICAqL1xuICAgIFNjb3BlLnByb3RvdHlwZS5zZXRFeHRyYXMgPSBmdW5jdGlvbiAoZXh0cmFzKSB7XG4gICAgICAgIHRoaXMuX2V4dHJhID0gdHNsaWJfMS5fX2Fzc2lnbih7fSwgdGhpcy5fZXh0cmEsIGV4dHJhcyk7XG4gICAgICAgIHRoaXMuX25vdGlmeVNjb3BlTGlzdGVuZXJzKCk7XG4gICAgICAgIHJldHVybiB0aGlzO1xuICAgIH07XG4gICAgLyoqXG4gICAgICogQGluaGVyaXREb2NcbiAgICAgKi9cbiAgICBTY29wZS5wcm90b3R5cGUuc2V0RXh0cmEgPSBmdW5jdGlvbiAoa2V5LCBleHRyYSkge1xuICAgICAgICB2YXIgX2E7XG4gICAgICAgIHRoaXMuX2V4dHJhID0gdHNsaWJfMS5fX2Fzc2lnbih7fSwgdGhpcy5fZXh0cmEsIChfYSA9IHt9LCBfYVtrZXldID0gZXh0cmEsIF9hKSk7XG4gICAgICAgIHRoaXMuX25vdGlmeVNjb3BlTGlzdGVuZXJzKCk7XG4gICAgICAgIHJldHVybiB0aGlzO1xuICAgIH07XG4gICAgLyoqXG4gICAgICogQGluaGVyaXREb2NcbiAgICAgKi9cbiAgICBTY29wZS5wcm90b3R5cGUuc2V0RmluZ2VycHJpbnQgPSBmdW5jdGlvbiAoZmluZ2VycHJpbnQpIHtcbiAgICAgICAgdGhpcy5fZmluZ2VycHJpbnQgPSBmaW5nZXJwcmludDtcbiAgICAgICAgdGhpcy5fbm90aWZ5U2NvcGVMaXN0ZW5lcnMoKTtcbiAgICAgICAgcmV0dXJuIHRoaXM7XG4gICAgfTtcbiAgICAvKipcbiAgICAgKiBAaW5oZXJpdERvY1xuICAgICAqL1xuICAgIFNjb3BlLnByb3RvdHlwZS5zZXRMZXZlbCA9IGZ1bmN0aW9uIChsZXZlbCkge1xuICAgICAgICB0aGlzLl9sZXZlbCA9IGxldmVsO1xuICAgICAgICB0aGlzLl9ub3RpZnlTY29wZUxpc3RlbmVycygpO1xuICAgICAgICByZXR1cm4gdGhpcztcbiAgICB9O1xuICAgIC8qKlxuICAgICAqIEBpbmhlcml0RG9jXG4gICAgICovXG4gICAgU2NvcGUucHJvdG90eXBlLnNldFRyYW5zYWN0aW9uTmFtZSA9IGZ1bmN0aW9uIChuYW1lKSB7XG4gICAgICAgIHRoaXMuX3RyYW5zYWN0aW9uTmFtZSA9IG5hbWU7XG4gICAgICAgIHRoaXMuX25vdGlmeVNjb3BlTGlzdGVuZXJzKCk7XG4gICAgICAgIHJldHVybiB0aGlzO1xuICAgIH07XG4gICAgLyoqXG4gICAgICogQ2FuIGJlIHJlbW92ZWQgaW4gbWFqb3IgdmVyc2lvbi5cbiAgICAgKiBAZGVwcmVjYXRlZCBpbiBmYXZvciBvZiB7QGxpbmsgdGhpcy5zZXRUcmFuc2FjdGlvbk5hbWV9XG4gICAgICovXG4gICAgU2NvcGUucHJvdG90eXBlLnNldFRyYW5zYWN0aW9uID0gZnVuY3Rpb24gKG5hbWUpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuc2V0VHJhbnNhY3Rpb25OYW1lKG5hbWUpO1xuICAgIH07XG4gICAgLyoqXG4gICAgICogQGluaGVyaXREb2NcbiAgICAgKi9cbiAgICBTY29wZS5wcm90b3R5cGUuc2V0Q29udGV4dCA9IGZ1bmN0aW9uIChrZXksIGNvbnRleHQpIHtcbiAgICAgICAgdmFyIF9hO1xuICAgICAgICB0aGlzLl9jb250ZXh0cyA9IHRzbGliXzEuX19hc3NpZ24oe30sIHRoaXMuX2NvbnRleHRzLCAoX2EgPSB7fSwgX2Fba2V5XSA9IGNvbnRleHQsIF9hKSk7XG4gICAgICAgIHRoaXMuX25vdGlmeVNjb3BlTGlzdGVuZXJzKCk7XG4gICAgICAgIHJldHVybiB0aGlzO1xuICAgIH07XG4gICAgLyoqXG4gICAgICogQGluaGVyaXREb2NcbiAgICAgKi9cbiAgICBTY29wZS5wcm90b3R5cGUuc2V0U3BhbiA9IGZ1bmN0aW9uIChzcGFuKSB7XG4gICAgICAgIHRoaXMuX3NwYW4gPSBzcGFuO1xuICAgICAgICB0aGlzLl9ub3RpZnlTY29wZUxpc3RlbmVycygpO1xuICAgICAgICByZXR1cm4gdGhpcztcbiAgICB9O1xuICAgIC8qKlxuICAgICAqIEBpbmhlcml0RG9jXG4gICAgICovXG4gICAgU2NvcGUucHJvdG90eXBlLmdldFNwYW4gPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLl9zcGFuO1xuICAgIH07XG4gICAgLyoqXG4gICAgICogQGluaGVyaXREb2NcbiAgICAgKi9cbiAgICBTY29wZS5wcm90b3R5cGUuZ2V0VHJhbnNhY3Rpb24gPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciBzcGFuID0gdGhpcy5nZXRTcGFuKCk7XG4gICAgICAgIGlmIChzcGFuICYmIHNwYW4uc3BhblJlY29yZGVyICYmIHNwYW4uc3BhblJlY29yZGVyLnNwYW5zWzBdKSB7XG4gICAgICAgICAgICByZXR1cm4gc3Bhbi5zcGFuUmVjb3JkZXIuc3BhbnNbMF07XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHVuZGVmaW5lZDtcbiAgICB9O1xuICAgIC8qKlxuICAgICAqIEluaGVyaXQgdmFsdWVzIGZyb20gdGhlIHBhcmVudCBzY29wZS5cbiAgICAgKiBAcGFyYW0gc2NvcGUgdG8gY2xvbmUuXG4gICAgICovXG4gICAgU2NvcGUuY2xvbmUgPSBmdW5jdGlvbiAoc2NvcGUpIHtcbiAgICAgICAgdmFyIG5ld1Njb3BlID0gbmV3IFNjb3BlKCk7XG4gICAgICAgIGlmIChzY29wZSkge1xuICAgICAgICAgICAgbmV3U2NvcGUuX2JyZWFkY3J1bWJzID0gdHNsaWJfMS5fX3NwcmVhZChzY29wZS5fYnJlYWRjcnVtYnMpO1xuICAgICAgICAgICAgbmV3U2NvcGUuX3RhZ3MgPSB0c2xpYl8xLl9fYXNzaWduKHt9LCBzY29wZS5fdGFncyk7XG4gICAgICAgICAgICBuZXdTY29wZS5fZXh0cmEgPSB0c2xpYl8xLl9fYXNzaWduKHt9LCBzY29wZS5fZXh0cmEpO1xuICAgICAgICAgICAgbmV3U2NvcGUuX2NvbnRleHRzID0gdHNsaWJfMS5fX2Fzc2lnbih7fSwgc2NvcGUuX2NvbnRleHRzKTtcbiAgICAgICAgICAgIG5ld1Njb3BlLl91c2VyID0gc2NvcGUuX3VzZXI7XG4gICAgICAgICAgICBuZXdTY29wZS5fbGV2ZWwgPSBzY29wZS5fbGV2ZWw7XG4gICAgICAgICAgICBuZXdTY29wZS5fc3BhbiA9IHNjb3BlLl9zcGFuO1xuICAgICAgICAgICAgbmV3U2NvcGUuX3RyYW5zYWN0aW9uTmFtZSA9IHNjb3BlLl90cmFuc2FjdGlvbk5hbWU7XG4gICAgICAgICAgICBuZXdTY29wZS5fZmluZ2VycHJpbnQgPSBzY29wZS5fZmluZ2VycHJpbnQ7XG4gICAgICAgICAgICBuZXdTY29wZS5fZXZlbnRQcm9jZXNzb3JzID0gdHNsaWJfMS5fX3NwcmVhZChzY29wZS5fZXZlbnRQcm9jZXNzb3JzKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gbmV3U2NvcGU7XG4gICAgfTtcbiAgICAvKipcbiAgICAgKiBAaW5oZXJpdERvY1xuICAgICAqL1xuICAgIFNjb3BlLnByb3RvdHlwZS51cGRhdGUgPSBmdW5jdGlvbiAoY2FwdHVyZUNvbnRleHQpIHtcbiAgICAgICAgaWYgKCFjYXB0dXJlQ29udGV4dCkge1xuICAgICAgICAgICAgcmV0dXJuIHRoaXM7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHR5cGVvZiBjYXB0dXJlQ29udGV4dCA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICAgICAgdmFyIHVwZGF0ZWRTY29wZSA9IGNhcHR1cmVDb250ZXh0KHRoaXMpO1xuICAgICAgICAgICAgcmV0dXJuIHVwZGF0ZWRTY29wZSBpbnN0YW5jZW9mIFNjb3BlID8gdXBkYXRlZFNjb3BlIDogdGhpcztcbiAgICAgICAgfVxuICAgICAgICBpZiAoY2FwdHVyZUNvbnRleHQgaW5zdGFuY2VvZiBTY29wZSkge1xuICAgICAgICAgICAgdGhpcy5fdGFncyA9IHRzbGliXzEuX19hc3NpZ24oe30sIHRoaXMuX3RhZ3MsIGNhcHR1cmVDb250ZXh0Ll90YWdzKTtcbiAgICAgICAgICAgIHRoaXMuX2V4dHJhID0gdHNsaWJfMS5fX2Fzc2lnbih7fSwgdGhpcy5fZXh0cmEsIGNhcHR1cmVDb250ZXh0Ll9leHRyYSk7XG4gICAgICAgICAgICB0aGlzLl9jb250ZXh0cyA9IHRzbGliXzEuX19hc3NpZ24oe30sIHRoaXMuX2NvbnRleHRzLCBjYXB0dXJlQ29udGV4dC5fY29udGV4dHMpO1xuICAgICAgICAgICAgaWYgKGNhcHR1cmVDb250ZXh0Ll91c2VyKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5fdXNlciA9IGNhcHR1cmVDb250ZXh0Ll91c2VyO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKGNhcHR1cmVDb250ZXh0Ll9sZXZlbCkge1xuICAgICAgICAgICAgICAgIHRoaXMuX2xldmVsID0gY2FwdHVyZUNvbnRleHQuX2xldmVsO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKGNhcHR1cmVDb250ZXh0Ll9maW5nZXJwcmludCkge1xuICAgICAgICAgICAgICAgIHRoaXMuX2ZpbmdlcnByaW50ID0gY2FwdHVyZUNvbnRleHQuX2ZpbmdlcnByaW50O1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIGVsc2UgaWYgKGlzUGxhaW5PYmplY3QoY2FwdHVyZUNvbnRleHQpKSB7XG4gICAgICAgICAgICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6bm8tcGFyYW1ldGVyLXJlYXNzaWdubWVudFxuICAgICAgICAgICAgY2FwdHVyZUNvbnRleHQgPSBjYXB0dXJlQ29udGV4dDtcbiAgICAgICAgICAgIHRoaXMuX3RhZ3MgPSB0c2xpYl8xLl9fYXNzaWduKHt9LCB0aGlzLl90YWdzLCBjYXB0dXJlQ29udGV4dC50YWdzKTtcbiAgICAgICAgICAgIHRoaXMuX2V4dHJhID0gdHNsaWJfMS5fX2Fzc2lnbih7fSwgdGhpcy5fZXh0cmEsIGNhcHR1cmVDb250ZXh0LmV4dHJhKTtcbiAgICAgICAgICAgIHRoaXMuX2NvbnRleHRzID0gdHNsaWJfMS5fX2Fzc2lnbih7fSwgdGhpcy5fY29udGV4dHMsIGNhcHR1cmVDb250ZXh0LmNvbnRleHRzKTtcbiAgICAgICAgICAgIGlmIChjYXB0dXJlQ29udGV4dC51c2VyKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5fdXNlciA9IGNhcHR1cmVDb250ZXh0LnVzZXI7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAoY2FwdHVyZUNvbnRleHQubGV2ZWwpIHtcbiAgICAgICAgICAgICAgICB0aGlzLl9sZXZlbCA9IGNhcHR1cmVDb250ZXh0LmxldmVsO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKGNhcHR1cmVDb250ZXh0LmZpbmdlcnByaW50KSB7XG4gICAgICAgICAgICAgICAgdGhpcy5fZmluZ2VycHJpbnQgPSBjYXB0dXJlQ29udGV4dC5maW5nZXJwcmludDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdGhpcztcbiAgICB9O1xuICAgIC8qKlxuICAgICAqIEBpbmhlcml0RG9jXG4gICAgICovXG4gICAgU2NvcGUucHJvdG90eXBlLmNsZWFyID0gZnVuY3Rpb24gKCkge1xuICAgICAgICB0aGlzLl9icmVhZGNydW1icyA9IFtdO1xuICAgICAgICB0aGlzLl90YWdzID0ge307XG4gICAgICAgIHRoaXMuX2V4dHJhID0ge307XG4gICAgICAgIHRoaXMuX3VzZXIgPSB7fTtcbiAgICAgICAgdGhpcy5fY29udGV4dHMgPSB7fTtcbiAgICAgICAgdGhpcy5fbGV2ZWwgPSB1bmRlZmluZWQ7XG4gICAgICAgIHRoaXMuX3RyYW5zYWN0aW9uTmFtZSA9IHVuZGVmaW5lZDtcbiAgICAgICAgdGhpcy5fZmluZ2VycHJpbnQgPSB1bmRlZmluZWQ7XG4gICAgICAgIHRoaXMuX3NwYW4gPSB1bmRlZmluZWQ7XG4gICAgICAgIHRoaXMuX25vdGlmeVNjb3BlTGlzdGVuZXJzKCk7XG4gICAgICAgIHJldHVybiB0aGlzO1xuICAgIH07XG4gICAgLyoqXG4gICAgICogQGluaGVyaXREb2NcbiAgICAgKi9cbiAgICBTY29wZS5wcm90b3R5cGUuYWRkQnJlYWRjcnVtYiA9IGZ1bmN0aW9uIChicmVhZGNydW1iLCBtYXhCcmVhZGNydW1icykge1xuICAgICAgICB2YXIgbWVyZ2VkQnJlYWRjcnVtYiA9IHRzbGliXzEuX19hc3NpZ24oeyB0aW1lc3RhbXA6IHRpbWVzdGFtcFdpdGhNcygpIH0sIGJyZWFkY3J1bWIpO1xuICAgICAgICB0aGlzLl9icmVhZGNydW1icyA9XG4gICAgICAgICAgICBtYXhCcmVhZGNydW1icyAhPT0gdW5kZWZpbmVkICYmIG1heEJyZWFkY3J1bWJzID49IDBcbiAgICAgICAgICAgICAgICA/IHRzbGliXzEuX19zcHJlYWQodGhpcy5fYnJlYWRjcnVtYnMsIFttZXJnZWRCcmVhZGNydW1iXSkuc2xpY2UoLW1heEJyZWFkY3J1bWJzKVxuICAgICAgICAgICAgICAgIDogdHNsaWJfMS5fX3NwcmVhZCh0aGlzLl9icmVhZGNydW1icywgW21lcmdlZEJyZWFkY3J1bWJdKTtcbiAgICAgICAgdGhpcy5fbm90aWZ5U2NvcGVMaXN0ZW5lcnMoKTtcbiAgICAgICAgcmV0dXJuIHRoaXM7XG4gICAgfTtcbiAgICAvKipcbiAgICAgKiBAaW5oZXJpdERvY1xuICAgICAqL1xuICAgIFNjb3BlLnByb3RvdHlwZS5jbGVhckJyZWFkY3J1bWJzID0gZnVuY3Rpb24gKCkge1xuICAgICAgICB0aGlzLl9icmVhZGNydW1icyA9IFtdO1xuICAgICAgICB0aGlzLl9ub3RpZnlTY29wZUxpc3RlbmVycygpO1xuICAgICAgICByZXR1cm4gdGhpcztcbiAgICB9O1xuICAgIC8qKlxuICAgICAqIEFwcGxpZXMgZmluZ2VycHJpbnQgZnJvbSB0aGUgc2NvcGUgdG8gdGhlIGV2ZW50IGlmIHRoZXJlJ3Mgb25lLFxuICAgICAqIHVzZXMgbWVzc2FnZSBpZiB0aGVyZSdzIG9uZSBpbnN0ZWFkIG9yIGdldCByaWQgb2YgZW1wdHkgZmluZ2VycHJpbnRcbiAgICAgKi9cbiAgICBTY29wZS5wcm90b3R5cGUuX2FwcGx5RmluZ2VycHJpbnQgPSBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgICAgLy8gTWFrZSBzdXJlIGl0J3MgYW4gYXJyYXkgZmlyc3QgYW5kIHdlIGFjdHVhbGx5IGhhdmUgc29tZXRoaW5nIGluIHBsYWNlXG4gICAgICAgIGV2ZW50LmZpbmdlcnByaW50ID0gZXZlbnQuZmluZ2VycHJpbnRcbiAgICAgICAgICAgID8gQXJyYXkuaXNBcnJheShldmVudC5maW5nZXJwcmludClcbiAgICAgICAgICAgICAgICA/IGV2ZW50LmZpbmdlcnByaW50XG4gICAgICAgICAgICAgICAgOiBbZXZlbnQuZmluZ2VycHJpbnRdXG4gICAgICAgICAgICA6IFtdO1xuICAgICAgICAvLyBJZiB3ZSBoYXZlIHNvbWV0aGluZyBvbiB0aGUgc2NvcGUsIHRoZW4gbWVyZ2UgaXQgd2l0aCBldmVudFxuICAgICAgICBpZiAodGhpcy5fZmluZ2VycHJpbnQpIHtcbiAgICAgICAgICAgIGV2ZW50LmZpbmdlcnByaW50ID0gZXZlbnQuZmluZ2VycHJpbnQuY29uY2F0KHRoaXMuX2ZpbmdlcnByaW50KTtcbiAgICAgICAgfVxuICAgICAgICAvLyBJZiB3ZSBoYXZlIG5vIGRhdGEgYXQgYWxsLCByZW1vdmUgZW1wdHkgYXJyYXkgZGVmYXVsdFxuICAgICAgICBpZiAoZXZlbnQuZmluZ2VycHJpbnQgJiYgIWV2ZW50LmZpbmdlcnByaW50Lmxlbmd0aCkge1xuICAgICAgICAgICAgZGVsZXRlIGV2ZW50LmZpbmdlcnByaW50O1xuICAgICAgICB9XG4gICAgfTtcbiAgICAvKipcbiAgICAgKiBBcHBsaWVzIHRoZSBjdXJyZW50IGNvbnRleHQgYW5kIGZpbmdlcnByaW50IHRvIHRoZSBldmVudC5cbiAgICAgKiBOb3RlIHRoYXQgYnJlYWRjcnVtYnMgd2lsbCBiZSBhZGRlZCBieSB0aGUgY2xpZW50LlxuICAgICAqIEFsc28gaWYgdGhlIGV2ZW50IGhhcyBhbHJlYWR5IGJyZWFkY3J1bWJzIG9uIGl0LCB3ZSBkbyBub3QgbWVyZ2UgdGhlbS5cbiAgICAgKiBAcGFyYW0gZXZlbnQgRXZlbnRcbiAgICAgKiBAcGFyYW0gaGludCBNYXkgY29udGFpbiBhZGRpdGlvbmFsIGluZm9ybWFydGlvbiBhYm91dCB0aGUgb3JpZ2luYWwgZXhjZXB0aW9uLlxuICAgICAqIEBoaWRkZW5cbiAgICAgKi9cbiAgICBTY29wZS5wcm90b3R5cGUuYXBwbHlUb0V2ZW50ID0gZnVuY3Rpb24gKGV2ZW50LCBoaW50KSB7XG4gICAgICAgIGlmICh0aGlzLl9leHRyYSAmJiBPYmplY3Qua2V5cyh0aGlzLl9leHRyYSkubGVuZ3RoKSB7XG4gICAgICAgICAgICBldmVudC5leHRyYSA9IHRzbGliXzEuX19hc3NpZ24oe30sIHRoaXMuX2V4dHJhLCBldmVudC5leHRyYSk7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHRoaXMuX3RhZ3MgJiYgT2JqZWN0LmtleXModGhpcy5fdGFncykubGVuZ3RoKSB7XG4gICAgICAgICAgICBldmVudC50YWdzID0gdHNsaWJfMS5fX2Fzc2lnbih7fSwgdGhpcy5fdGFncywgZXZlbnQudGFncyk7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHRoaXMuX3VzZXIgJiYgT2JqZWN0LmtleXModGhpcy5fdXNlcikubGVuZ3RoKSB7XG4gICAgICAgICAgICBldmVudC51c2VyID0gdHNsaWJfMS5fX2Fzc2lnbih7fSwgdGhpcy5fdXNlciwgZXZlbnQudXNlcik7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHRoaXMuX2NvbnRleHRzICYmIE9iamVjdC5rZXlzKHRoaXMuX2NvbnRleHRzKS5sZW5ndGgpIHtcbiAgICAgICAgICAgIGV2ZW50LmNvbnRleHRzID0gdHNsaWJfMS5fX2Fzc2lnbih7fSwgdGhpcy5fY29udGV4dHMsIGV2ZW50LmNvbnRleHRzKTtcbiAgICAgICAgfVxuICAgICAgICBpZiAodGhpcy5fbGV2ZWwpIHtcbiAgICAgICAgICAgIGV2ZW50LmxldmVsID0gdGhpcy5fbGV2ZWw7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHRoaXMuX3RyYW5zYWN0aW9uTmFtZSkge1xuICAgICAgICAgICAgZXZlbnQudHJhbnNhY3Rpb24gPSB0aGlzLl90cmFuc2FjdGlvbk5hbWU7XG4gICAgICAgIH1cbiAgICAgICAgLy8gV2Ugd2FudCB0byBzZXQgdGhlIHRyYWNlIGNvbnRleHQgZm9yIG5vcm1hbCBldmVudHMgb25seSBpZiB0aGVyZSBpc24ndCBhbHJlYWR5XG4gICAgICAgIC8vIGEgdHJhY2UgY29udGV4dCBvbiB0aGUgZXZlbnQuIFRoZXJlIGlzIGEgcHJvZHVjdCBmZWF0dXJlIGluIHBsYWNlIHdoZXJlIHdlIGxpbmtcbiAgICAgICAgLy8gZXJyb3JzIHdpdGggdHJhbnNhY3Rpb24gYW5kIGl0IHJlbHlzIG9uIHRoYXQuXG4gICAgICAgIGlmICh0aGlzLl9zcGFuKSB7XG4gICAgICAgICAgICBldmVudC5jb250ZXh0cyA9IHRzbGliXzEuX19hc3NpZ24oeyB0cmFjZTogdGhpcy5fc3Bhbi5nZXRUcmFjZUNvbnRleHQoKSB9LCBldmVudC5jb250ZXh0cyk7XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5fYXBwbHlGaW5nZXJwcmludChldmVudCk7XG4gICAgICAgIGV2ZW50LmJyZWFkY3J1bWJzID0gdHNsaWJfMS5fX3NwcmVhZCgoZXZlbnQuYnJlYWRjcnVtYnMgfHwgW10pLCB0aGlzLl9icmVhZGNydW1icyk7XG4gICAgICAgIGV2ZW50LmJyZWFkY3J1bWJzID0gZXZlbnQuYnJlYWRjcnVtYnMubGVuZ3RoID4gMCA/IGV2ZW50LmJyZWFkY3J1bWJzIDogdW5kZWZpbmVkO1xuICAgICAgICByZXR1cm4gdGhpcy5fbm90aWZ5RXZlbnRQcm9jZXNzb3JzKHRzbGliXzEuX19zcHJlYWQoZ2V0R2xvYmFsRXZlbnRQcm9jZXNzb3JzKCksIHRoaXMuX2V2ZW50UHJvY2Vzc29ycyksIGV2ZW50LCBoaW50KTtcbiAgICB9O1xuICAgIHJldHVybiBTY29wZTtcbn0oKSk7XG5leHBvcnQgeyBTY29wZSB9O1xuLyoqXG4gKiBSZXRydW5zIHRoZSBnbG9iYWwgZXZlbnQgcHJvY2Vzc29ycy5cbiAqL1xuZnVuY3Rpb24gZ2V0R2xvYmFsRXZlbnRQcm9jZXNzb3JzKCkge1xuICAgIHZhciBnbG9iYWwgPSBnZXRHbG9iYWxPYmplY3QoKTtcbiAgICBnbG9iYWwuX19TRU5UUllfXyA9IGdsb2JhbC5fX1NFTlRSWV9fIHx8IHt9O1xuICAgIGdsb2JhbC5fX1NFTlRSWV9fLmdsb2JhbEV2ZW50UHJvY2Vzc29ycyA9IGdsb2JhbC5fX1NFTlRSWV9fLmdsb2JhbEV2ZW50UHJvY2Vzc29ycyB8fCBbXTtcbiAgICByZXR1cm4gZ2xvYmFsLl9fU0VOVFJZX18uZ2xvYmFsRXZlbnRQcm9jZXNzb3JzO1xufVxuLyoqXG4gKiBBZGQgYSBFdmVudFByb2Nlc3NvciB0byBiZSBrZXB0IGdsb2JhbGx5LlxuICogQHBhcmFtIGNhbGxiYWNrIEV2ZW50UHJvY2Vzc29yIHRvIGFkZFxuICovXG5leHBvcnQgZnVuY3Rpb24gYWRkR2xvYmFsRXZlbnRQcm9jZXNzb3IoY2FsbGJhY2spIHtcbiAgICBnZXRHbG9iYWxFdmVudFByb2Nlc3NvcnMoKS5wdXNoKGNhbGxiYWNrKTtcbn1cbi8vIyBzb3VyY2VNYXBwaW5nVVJMPXNjb3BlLmpzLm1hcCIsImltcG9ydCAqIGFzIHRzbGliXzEgZnJvbSBcInRzbGliXCI7XG5pbXBvcnQgeyBnZXRDdXJyZW50SHViIH0gZnJvbSAnQHNlbnRyeS9odWInO1xuLyoqXG4gKiBUaGlzIGNhbGxzIGEgZnVuY3Rpb24gb24gdGhlIGN1cnJlbnQgaHViLlxuICogQHBhcmFtIG1ldGhvZCBmdW5jdGlvbiB0byBjYWxsIG9uIGh1Yi5cbiAqIEBwYXJhbSBhcmdzIHRvIHBhc3MgdG8gZnVuY3Rpb24uXG4gKi9cbmZ1bmN0aW9uIGNhbGxPbkh1YihtZXRob2QpIHtcbiAgICB2YXIgYXJncyA9IFtdO1xuICAgIGZvciAodmFyIF9pID0gMTsgX2kgPCBhcmd1bWVudHMubGVuZ3RoOyBfaSsrKSB7XG4gICAgICAgIGFyZ3NbX2kgLSAxXSA9IGFyZ3VtZW50c1tfaV07XG4gICAgfVxuICAgIHZhciBodWIgPSBnZXRDdXJyZW50SHViKCk7XG4gICAgaWYgKGh1YiAmJiBodWJbbWV0aG9kXSkge1xuICAgICAgICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6bm8tdW5zYWZlLWFueVxuICAgICAgICByZXR1cm4gaHViW21ldGhvZF0uYXBwbHkoaHViLCB0c2xpYl8xLl9fc3ByZWFkKGFyZ3MpKTtcbiAgICB9XG4gICAgdGhyb3cgbmV3IEVycm9yKFwiTm8gaHViIGRlZmluZWQgb3IgXCIgKyBtZXRob2QgKyBcIiB3YXMgbm90IGZvdW5kIG9uIHRoZSBodWIsIHBsZWFzZSBvcGVuIGEgYnVnIHJlcG9ydC5cIik7XG59XG4vKipcbiAqIENhcHR1cmVzIGFuIGV4Y2VwdGlvbiBldmVudCBhbmQgc2VuZHMgaXQgdG8gU2VudHJ5LlxuICpcbiAqIEBwYXJhbSBleGNlcHRpb24gQW4gZXhjZXB0aW9uLWxpa2Ugb2JqZWN0LlxuICogQHJldHVybnMgVGhlIGdlbmVyYXRlZCBldmVudElkLlxuICovXG5leHBvcnQgZnVuY3Rpb24gY2FwdHVyZUV4Y2VwdGlvbihleGNlcHRpb24sIGNhcHR1cmVDb250ZXh0KSB7XG4gICAgdmFyIHN5bnRoZXRpY0V4Y2VwdGlvbjtcbiAgICB0cnkge1xuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ1NlbnRyeSBzeW50aGV0aWNFeGNlcHRpb24nKTtcbiAgICB9XG4gICAgY2F0Y2ggKGV4Y2VwdGlvbikge1xuICAgICAgICBzeW50aGV0aWNFeGNlcHRpb24gPSBleGNlcHRpb247XG4gICAgfVxuICAgIHJldHVybiBjYWxsT25IdWIoJ2NhcHR1cmVFeGNlcHRpb24nLCBleGNlcHRpb24sIHtcbiAgICAgICAgY2FwdHVyZUNvbnRleHQ6IGNhcHR1cmVDb250ZXh0LFxuICAgICAgICBvcmlnaW5hbEV4Y2VwdGlvbjogZXhjZXB0aW9uLFxuICAgICAgICBzeW50aGV0aWNFeGNlcHRpb246IHN5bnRoZXRpY0V4Y2VwdGlvbixcbiAgICB9KTtcbn1cbi8qKlxuICogQ2FwdHVyZXMgYSBtZXNzYWdlIGV2ZW50IGFuZCBzZW5kcyBpdCB0byBTZW50cnkuXG4gKlxuICogQHBhcmFtIG1lc3NhZ2UgVGhlIG1lc3NhZ2UgdG8gc2VuZCB0byBTZW50cnkuXG4gKiBAcGFyYW0gbGV2ZWwgRGVmaW5lIHRoZSBsZXZlbCBvZiB0aGUgbWVzc2FnZS5cbiAqIEByZXR1cm5zIFRoZSBnZW5lcmF0ZWQgZXZlbnRJZC5cbiAqL1xuZXhwb3J0IGZ1bmN0aW9uIGNhcHR1cmVNZXNzYWdlKG1lc3NhZ2UsIGNhcHR1cmVDb250ZXh0KSB7XG4gICAgdmFyIHN5bnRoZXRpY0V4Y2VwdGlvbjtcbiAgICB0cnkge1xuICAgICAgICB0aHJvdyBuZXcgRXJyb3IobWVzc2FnZSk7XG4gICAgfVxuICAgIGNhdGNoIChleGNlcHRpb24pIHtcbiAgICAgICAgc3ludGhldGljRXhjZXB0aW9uID0gZXhjZXB0aW9uO1xuICAgIH1cbiAgICAvLyBUaGlzIGlzIG5lY2Vzc2FyeSB0byBwcm92aWRlIGV4cGxpY2l0IHNjb3BlcyB1cGdyYWRlLCB3aXRob3V0IGNoYW5naW5nIHRoZSBvcmlnaW5hbFxuICAgIC8vIGFycml0eSBvZiB0aGUgYGNhcHR1cmVNZXNzYWdlKG1lc3NhZ2UsIGxldmVsKWAgbWV0aG9kLlxuICAgIHZhciBsZXZlbCA9IHR5cGVvZiBjYXB0dXJlQ29udGV4dCA9PT0gJ3N0cmluZycgPyBjYXB0dXJlQ29udGV4dCA6IHVuZGVmaW5lZDtcbiAgICB2YXIgY29udGV4dCA9IHR5cGVvZiBjYXB0dXJlQ29udGV4dCAhPT0gJ3N0cmluZycgPyB7IGNhcHR1cmVDb250ZXh0OiBjYXB0dXJlQ29udGV4dCB9IDogdW5kZWZpbmVkO1xuICAgIHJldHVybiBjYWxsT25IdWIoJ2NhcHR1cmVNZXNzYWdlJywgbWVzc2FnZSwgbGV2ZWwsIHRzbGliXzEuX19hc3NpZ24oeyBvcmlnaW5hbEV4Y2VwdGlvbjogbWVzc2FnZSwgc3ludGhldGljRXhjZXB0aW9uOiBzeW50aGV0aWNFeGNlcHRpb24gfSwgY29udGV4dCkpO1xufVxuLyoqXG4gKiBDYXB0dXJlcyBhIG1hbnVhbGx5IGNyZWF0ZWQgZXZlbnQgYW5kIHNlbmRzIGl0IHRvIFNlbnRyeS5cbiAqXG4gKiBAcGFyYW0gZXZlbnQgVGhlIGV2ZW50IHRvIHNlbmQgdG8gU2VudHJ5LlxuICogQHJldHVybnMgVGhlIGdlbmVyYXRlZCBldmVudElkLlxuICovXG5leHBvcnQgZnVuY3Rpb24gY2FwdHVyZUV2ZW50KGV2ZW50KSB7XG4gICAgcmV0dXJuIGNhbGxPbkh1YignY2FwdHVyZUV2ZW50JywgZXZlbnQpO1xufVxuLyoqXG4gKiBDYWxsYmFjayB0byBzZXQgY29udGV4dCBpbmZvcm1hdGlvbiBvbnRvIHRoZSBzY29wZS5cbiAqIEBwYXJhbSBjYWxsYmFjayBDYWxsYmFjayBmdW5jdGlvbiB0aGF0IHJlY2VpdmVzIFNjb3BlLlxuICovXG5leHBvcnQgZnVuY3Rpb24gY29uZmlndXJlU2NvcGUoY2FsbGJhY2spIHtcbiAgICBjYWxsT25IdWIoJ2NvbmZpZ3VyZVNjb3BlJywgY2FsbGJhY2spO1xufVxuLyoqXG4gKiBSZWNvcmRzIGEgbmV3IGJyZWFkY3J1bWIgd2hpY2ggd2lsbCBiZSBhdHRhY2hlZCB0byBmdXR1cmUgZXZlbnRzLlxuICpcbiAqIEJyZWFkY3J1bWJzIHdpbGwgYmUgYWRkZWQgdG8gc3Vic2VxdWVudCBldmVudHMgdG8gcHJvdmlkZSBtb3JlIGNvbnRleHQgb25cbiAqIHVzZXIncyBhY3Rpb25zIHByaW9yIHRvIGFuIGVycm9yIG9yIGNyYXNoLlxuICpcbiAqIEBwYXJhbSBicmVhZGNydW1iIFRoZSBicmVhZGNydW1iIHRvIHJlY29yZC5cbiAqL1xuZXhwb3J0IGZ1bmN0aW9uIGFkZEJyZWFkY3J1bWIoYnJlYWRjcnVtYikge1xuICAgIGNhbGxPbkh1YignYWRkQnJlYWRjcnVtYicsIGJyZWFkY3J1bWIpO1xufVxuLyoqXG4gKiBTZXRzIGNvbnRleHQgZGF0YSB3aXRoIHRoZSBnaXZlbiBuYW1lLlxuICogQHBhcmFtIG5hbWUgb2YgdGhlIGNvbnRleHRcbiAqIEBwYXJhbSBjb250ZXh0IEFueSBraW5kIG9mIGRhdGEuIFRoaXMgZGF0YSB3aWxsIGJlIG5vcm1hbGl6ZWQuXG4gKi9cbmV4cG9ydCBmdW5jdGlvbiBzZXRDb250ZXh0KG5hbWUsIGNvbnRleHQpIHtcbiAgICBjYWxsT25IdWIoJ3NldENvbnRleHQnLCBuYW1lLCBjb250ZXh0KTtcbn1cbi8qKlxuICogU2V0IGFuIG9iamVjdCB0aGF0IHdpbGwgYmUgbWVyZ2VkIHNlbnQgYXMgZXh0cmEgZGF0YSB3aXRoIHRoZSBldmVudC5cbiAqIEBwYXJhbSBleHRyYXMgRXh0cmFzIG9iamVjdCB0byBtZXJnZSBpbnRvIGN1cnJlbnQgY29udGV4dC5cbiAqL1xuZXhwb3J0IGZ1bmN0aW9uIHNldEV4dHJhcyhleHRyYXMpIHtcbiAgICBjYWxsT25IdWIoJ3NldEV4dHJhcycsIGV4dHJhcyk7XG59XG4vKipcbiAqIFNldCBhbiBvYmplY3QgdGhhdCB3aWxsIGJlIG1lcmdlZCBzZW50IGFzIHRhZ3MgZGF0YSB3aXRoIHRoZSBldmVudC5cbiAqIEBwYXJhbSB0YWdzIFRhZ3MgY29udGV4dCBvYmplY3QgdG8gbWVyZ2UgaW50byBjdXJyZW50IGNvbnRleHQuXG4gKi9cbmV4cG9ydCBmdW5jdGlvbiBzZXRUYWdzKHRhZ3MpIHtcbiAgICBjYWxsT25IdWIoJ3NldFRhZ3MnLCB0YWdzKTtcbn1cbi8qKlxuICogU2V0IGtleTp2YWx1ZSB0aGF0IHdpbGwgYmUgc2VudCBhcyBleHRyYSBkYXRhIHdpdGggdGhlIGV2ZW50LlxuICogQHBhcmFtIGtleSBTdHJpbmcgb2YgZXh0cmFcbiAqIEBwYXJhbSBleHRyYSBBbnkga2luZCBvZiBkYXRhLiBUaGlzIGRhdGEgd2lsbCBiZSBub3JtYWxpemVkLlxuICovXG5leHBvcnQgZnVuY3Rpb24gc2V0RXh0cmEoa2V5LCBleHRyYSkge1xuICAgIGNhbGxPbkh1Yignc2V0RXh0cmEnLCBrZXksIGV4dHJhKTtcbn1cbi8qKlxuICogU2V0IGtleTp2YWx1ZSB0aGF0IHdpbGwgYmUgc2VudCBhcyB0YWdzIGRhdGEgd2l0aCB0aGUgZXZlbnQuXG4gKiBAcGFyYW0ga2V5IFN0cmluZyBrZXkgb2YgdGFnXG4gKiBAcGFyYW0gdmFsdWUgU3RyaW5nIHZhbHVlIG9mIHRhZ1xuICovXG5leHBvcnQgZnVuY3Rpb24gc2V0VGFnKGtleSwgdmFsdWUpIHtcbiAgICBjYWxsT25IdWIoJ3NldFRhZycsIGtleSwgdmFsdWUpO1xufVxuLyoqXG4gKiBVcGRhdGVzIHVzZXIgY29udGV4dCBpbmZvcm1hdGlvbiBmb3IgZnV0dXJlIGV2ZW50cy5cbiAqXG4gKiBAcGFyYW0gdXNlciBVc2VyIGNvbnRleHQgb2JqZWN0IHRvIGJlIHNldCBpbiB0aGUgY3VycmVudCBjb250ZXh0LiBQYXNzIGBudWxsYCB0byB1bnNldCB0aGUgdXNlci5cbiAqL1xuZXhwb3J0IGZ1bmN0aW9uIHNldFVzZXIodXNlcikge1xuICAgIGNhbGxPbkh1Yignc2V0VXNlcicsIHVzZXIpO1xufVxuLyoqXG4gKiBDcmVhdGVzIGEgbmV3IHNjb3BlIHdpdGggYW5kIGV4ZWN1dGVzIHRoZSBnaXZlbiBvcGVyYXRpb24gd2l0aGluLlxuICogVGhlIHNjb3BlIGlzIGF1dG9tYXRpY2FsbHkgcmVtb3ZlZCBvbmNlIHRoZSBvcGVyYXRpb25cbiAqIGZpbmlzaGVzIG9yIHRocm93cy5cbiAqXG4gKiBUaGlzIGlzIGVzc2VudGlhbGx5IGEgY29udmVuaWVuY2UgZnVuY3Rpb24gZm9yOlxuICpcbiAqICAgICBwdXNoU2NvcGUoKTtcbiAqICAgICBjYWxsYmFjaygpO1xuICogICAgIHBvcFNjb3BlKCk7XG4gKlxuICogQHBhcmFtIGNhbGxiYWNrIHRoYXQgd2lsbCBiZSBlbmNsb3NlZCBpbnRvIHB1c2gvcG9wU2NvcGUuXG4gKi9cbmV4cG9ydCBmdW5jdGlvbiB3aXRoU2NvcGUoY2FsbGJhY2spIHtcbiAgICBjYWxsT25IdWIoJ3dpdGhTY29wZScsIGNhbGxiYWNrKTtcbn1cbi8qKlxuICogQ2FsbHMgYSBmdW5jdGlvbiBvbiB0aGUgbGF0ZXN0IGNsaWVudC4gVXNlIHRoaXMgd2l0aCBjYXV0aW9uLCBpdCdzIG1lYW50IGFzXG4gKiBpbiBcImludGVybmFsXCIgaGVscGVyIHNvIHdlIGRvbid0IG5lZWQgdG8gZXhwb3NlIGV2ZXJ5IHBvc3NpYmxlIGZ1bmN0aW9uIGluXG4gKiB0aGUgc2hpbS4gSXQgaXMgbm90IGd1YXJhbnRlZWQgdGhhdCB0aGUgY2xpZW50IGFjdHVhbGx5IGltcGxlbWVudHMgdGhlXG4gKiBmdW5jdGlvbi5cbiAqXG4gKiBAcGFyYW0gbWV0aG9kIFRoZSBtZXRob2QgdG8gY2FsbCBvbiB0aGUgY2xpZW50L2NsaWVudC5cbiAqIEBwYXJhbSBhcmdzIEFyZ3VtZW50cyB0byBwYXNzIHRvIHRoZSBjbGllbnQvZm9udGVuZC5cbiAqIEBoaWRkZW5cbiAqL1xuZXhwb3J0IGZ1bmN0aW9uIF9jYWxsT25DbGllbnQobWV0aG9kKSB7XG4gICAgdmFyIGFyZ3MgPSBbXTtcbiAgICBmb3IgKHZhciBfaSA9IDE7IF9pIDwgYXJndW1lbnRzLmxlbmd0aDsgX2krKykge1xuICAgICAgICBhcmdzW19pIC0gMV0gPSBhcmd1bWVudHNbX2ldO1xuICAgIH1cbiAgICBjYWxsT25IdWIuYXBwbHkodm9pZCAwLCB0c2xpYl8xLl9fc3ByZWFkKFsnX2ludm9rZUNsaWVudCcsIG1ldGhvZF0sIGFyZ3MpKTtcbn1cbi8qKlxuICogU3RhcnRzIGEgbmV3IGBUcmFuc2FjdGlvbmAgYW5kIHJldHVybnMgaXQuIFRoaXMgaXMgdGhlIGVudHJ5IHBvaW50IHRvIG1hbnVhbFxuICogdHJhY2luZyBpbnN0cnVtZW50YXRpb24uXG4gKlxuICogQSB0cmVlIHN0cnVjdHVyZSBjYW4gYmUgYnVpbHQgYnkgYWRkaW5nIGNoaWxkIHNwYW5zIHRvIHRoZSB0cmFuc2FjdGlvbiwgYW5kXG4gKiBjaGlsZCBzcGFucyB0byBvdGhlciBzcGFucy4gVG8gc3RhcnQgYSBuZXcgY2hpbGQgc3BhbiB3aXRoaW4gdGhlIHRyYW5zYWN0aW9uXG4gKiBvciBhbnkgc3BhbiwgY2FsbCB0aGUgcmVzcGVjdGl2ZSBgLnN0YXJ0Q2hpbGQoKWAgbWV0aG9kLlxuICpcbiAqIEV2ZXJ5IGNoaWxkIHNwYW4gbXVzdCBiZSBmaW5pc2hlZCBiZWZvcmUgdGhlIHRyYW5zYWN0aW9uIGlzIGZpbmlzaGVkLFxuICogb3RoZXJ3aXNlIHRoZSB1bmZpbmlzaGVkIHNwYW5zIGFyZSBkaXNjYXJkZWQuXG4gKlxuICogVGhlIHRyYW5zYWN0aW9uIG11c3QgYmUgZmluaXNoZWQgd2l0aCBhIGNhbGwgdG8gaXRzIGAuZmluaXNoKClgIG1ldGhvZCwgYXRcbiAqIHdoaWNoIHBvaW50IHRoZSB0cmFuc2FjdGlvbiB3aXRoIGFsbCBpdHMgZmluaXNoZWQgY2hpbGQgc3BhbnMgd2lsbCBiZSBzZW50IHRvXG4gKiBTZW50cnkuXG4gKlxuICogQHBhcmFtIGNvbnRleHQgUHJvcGVydGllcyBvZiB0aGUgbmV3IGBUcmFuc2FjdGlvbmAuXG4gKi9cbmV4cG9ydCBmdW5jdGlvbiBzdGFydFRyYW5zYWN0aW9uKGNvbnRleHQpIHtcbiAgICByZXR1cm4gY2FsbE9uSHViKCdzdGFydFRyYW5zYWN0aW9uJywgdHNsaWJfMS5fX2Fzc2lnbih7fSwgY29udGV4dCkpO1xufVxuLy8jIHNvdXJjZU1hcHBpbmdVUkw9aW5kZXguanMubWFwIiwiZXhwb3J0IHsgTG9nTGV2ZWwgfSBmcm9tICcuL2xvZ2xldmVsJztcbmV4cG9ydCB7IFNldmVyaXR5IH0gZnJvbSAnLi9zZXZlcml0eSc7XG5leHBvcnQgeyBTdGF0dXMgfSBmcm9tICcuL3N0YXR1cyc7XG4vLyMgc291cmNlTWFwcGluZ1VSTD1pbmRleC5qcy5tYXAiLCIvKiogQ29uc29sZSBsb2dnaW5nIHZlcmJvc2l0eSBmb3IgdGhlIFNESy4gKi9cbmV4cG9ydCB2YXIgTG9nTGV2ZWw7XG4oZnVuY3Rpb24gKExvZ0xldmVsKSB7XG4gICAgLyoqIE5vIGxvZ3Mgd2lsbCBiZSBnZW5lcmF0ZWQuICovXG4gICAgTG9nTGV2ZWxbTG9nTGV2ZWxbXCJOb25lXCJdID0gMF0gPSBcIk5vbmVcIjtcbiAgICAvKiogT25seSBTREsgaW50ZXJuYWwgZXJyb3JzIHdpbGwgYmUgbG9nZ2VkLiAqL1xuICAgIExvZ0xldmVsW0xvZ0xldmVsW1wiRXJyb3JcIl0gPSAxXSA9IFwiRXJyb3JcIjtcbiAgICAvKiogSW5mb3JtYXRpb24gdXNlZnVsIGZvciBkZWJ1Z2dpbmcgdGhlIFNESyB3aWxsIGJlIGxvZ2dlZC4gKi9cbiAgICBMb2dMZXZlbFtMb2dMZXZlbFtcIkRlYnVnXCJdID0gMl0gPSBcIkRlYnVnXCI7XG4gICAgLyoqIEFsbCBTREsgYWN0aW9ucyB3aWxsIGJlIGxvZ2dlZC4gKi9cbiAgICBMb2dMZXZlbFtMb2dMZXZlbFtcIlZlcmJvc2VcIl0gPSAzXSA9IFwiVmVyYm9zZVwiO1xufSkoTG9nTGV2ZWwgfHwgKExvZ0xldmVsID0ge30pKTtcbi8vIyBzb3VyY2VNYXBwaW5nVVJMPWxvZ2xldmVsLmpzLm1hcCIsIi8qKiBKU0RvYyAqL1xuZXhwb3J0IHZhciBTZXZlcml0eTtcbihmdW5jdGlvbiAoU2V2ZXJpdHkpIHtcbiAgICAvKiogSlNEb2MgKi9cbiAgICBTZXZlcml0eVtcIkZhdGFsXCJdID0gXCJmYXRhbFwiO1xuICAgIC8qKiBKU0RvYyAqL1xuICAgIFNldmVyaXR5W1wiRXJyb3JcIl0gPSBcImVycm9yXCI7XG4gICAgLyoqIEpTRG9jICovXG4gICAgU2V2ZXJpdHlbXCJXYXJuaW5nXCJdID0gXCJ3YXJuaW5nXCI7XG4gICAgLyoqIEpTRG9jICovXG4gICAgU2V2ZXJpdHlbXCJMb2dcIl0gPSBcImxvZ1wiO1xuICAgIC8qKiBKU0RvYyAqL1xuICAgIFNldmVyaXR5W1wiSW5mb1wiXSA9IFwiaW5mb1wiO1xuICAgIC8qKiBKU0RvYyAqL1xuICAgIFNldmVyaXR5W1wiRGVidWdcIl0gPSBcImRlYnVnXCI7XG4gICAgLyoqIEpTRG9jICovXG4gICAgU2V2ZXJpdHlbXCJDcml0aWNhbFwiXSA9IFwiY3JpdGljYWxcIjtcbn0pKFNldmVyaXR5IHx8IChTZXZlcml0eSA9IHt9KSk7XG4vLyB0c2xpbnQ6ZGlzYWJsZTpjb21wbGV0ZWQtZG9jc1xuLy8gdHNsaW50OmRpc2FibGU6bm8tdW5uZWNlc3NhcnktcXVhbGlmaWVyIG5vLW5hbWVzcGFjZVxuKGZ1bmN0aW9uIChTZXZlcml0eSkge1xuICAgIC8qKlxuICAgICAqIENvbnZlcnRzIGEgc3RyaW5nLWJhc2VkIGxldmVsIGludG8gYSB7QGxpbmsgU2V2ZXJpdHl9LlxuICAgICAqXG4gICAgICogQHBhcmFtIGxldmVsIHN0cmluZyByZXByZXNlbnRhdGlvbiBvZiBTZXZlcml0eVxuICAgICAqIEByZXR1cm5zIFNldmVyaXR5XG4gICAgICovXG4gICAgZnVuY3Rpb24gZnJvbVN0cmluZyhsZXZlbCkge1xuICAgICAgICBzd2l0Y2ggKGxldmVsKSB7XG4gICAgICAgICAgICBjYXNlICdkZWJ1Zyc6XG4gICAgICAgICAgICAgICAgcmV0dXJuIFNldmVyaXR5LkRlYnVnO1xuICAgICAgICAgICAgY2FzZSAnaW5mbyc6XG4gICAgICAgICAgICAgICAgcmV0dXJuIFNldmVyaXR5LkluZm87XG4gICAgICAgICAgICBjYXNlICd3YXJuJzpcbiAgICAgICAgICAgIGNhc2UgJ3dhcm5pbmcnOlxuICAgICAgICAgICAgICAgIHJldHVybiBTZXZlcml0eS5XYXJuaW5nO1xuICAgICAgICAgICAgY2FzZSAnZXJyb3InOlxuICAgICAgICAgICAgICAgIHJldHVybiBTZXZlcml0eS5FcnJvcjtcbiAgICAgICAgICAgIGNhc2UgJ2ZhdGFsJzpcbiAgICAgICAgICAgICAgICByZXR1cm4gU2V2ZXJpdHkuRmF0YWw7XG4gICAgICAgICAgICBjYXNlICdjcml0aWNhbCc6XG4gICAgICAgICAgICAgICAgcmV0dXJuIFNldmVyaXR5LkNyaXRpY2FsO1xuICAgICAgICAgICAgY2FzZSAnbG9nJzpcbiAgICAgICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICAgICAgcmV0dXJuIFNldmVyaXR5LkxvZztcbiAgICAgICAgfVxuICAgIH1cbiAgICBTZXZlcml0eS5mcm9tU3RyaW5nID0gZnJvbVN0cmluZztcbn0pKFNldmVyaXR5IHx8IChTZXZlcml0eSA9IHt9KSk7XG4vLyMgc291cmNlTWFwcGluZ1VSTD1zZXZlcml0eS5qcy5tYXAiLCIvKiogVGhlIHN0YXR1cyBvZiBhbiBldmVudC4gKi9cbmV4cG9ydCB2YXIgU3RhdHVzO1xuKGZ1bmN0aW9uIChTdGF0dXMpIHtcbiAgICAvKiogVGhlIHN0YXR1cyBjb3VsZCBub3QgYmUgZGV0ZXJtaW5lZC4gKi9cbiAgICBTdGF0dXNbXCJVbmtub3duXCJdID0gXCJ1bmtub3duXCI7XG4gICAgLyoqIFRoZSBldmVudCB3YXMgc2tpcHBlZCBkdWUgdG8gY29uZmlndXJhdGlvbiBvciBjYWxsYmFja3MuICovXG4gICAgU3RhdHVzW1wiU2tpcHBlZFwiXSA9IFwic2tpcHBlZFwiO1xuICAgIC8qKiBUaGUgZXZlbnQgd2FzIHNlbnQgdG8gU2VudHJ5IHN1Y2Nlc3NmdWxseS4gKi9cbiAgICBTdGF0dXNbXCJTdWNjZXNzXCJdID0gXCJzdWNjZXNzXCI7XG4gICAgLyoqIFRoZSBjbGllbnQgaXMgY3VycmVudGx5IHJhdGUgbGltaXRlZCBhbmQgd2lsbCB0cnkgYWdhaW4gbGF0ZXIuICovXG4gICAgU3RhdHVzW1wiUmF0ZUxpbWl0XCJdID0gXCJyYXRlX2xpbWl0XCI7XG4gICAgLyoqIFRoZSBldmVudCBjb3VsZCBub3QgYmUgcHJvY2Vzc2VkLiAqL1xuICAgIFN0YXR1c1tcIkludmFsaWRcIl0gPSBcImludmFsaWRcIjtcbiAgICAvKiogQSBzZXJ2ZXItc2lkZSBlcnJvciBvY3VycmVkIGR1cmluZyBzdWJtaXNzaW9uLiAqL1xuICAgIFN0YXR1c1tcIkZhaWxlZFwiXSA9IFwiZmFpbGVkXCI7XG59KShTdGF0dXMgfHwgKFN0YXR1cyA9IHt9KSk7XG4vLyB0c2xpbnQ6ZGlzYWJsZTpjb21wbGV0ZWQtZG9jc1xuLy8gdHNsaW50OmRpc2FibGU6bm8tdW5uZWNlc3NhcnktcXVhbGlmaWVyIG5vLW5hbWVzcGFjZVxuKGZ1bmN0aW9uIChTdGF0dXMpIHtcbiAgICAvKipcbiAgICAgKiBDb252ZXJ0cyBhIEhUVFAgc3RhdHVzIGNvZGUgaW50byBhIHtAbGluayBTdGF0dXN9LlxuICAgICAqXG4gICAgICogQHBhcmFtIGNvZGUgVGhlIEhUVFAgcmVzcG9uc2Ugc3RhdHVzIGNvZGUuXG4gICAgICogQHJldHVybnMgVGhlIHNlbmQgc3RhdHVzIG9yIHtAbGluayBTdGF0dXMuVW5rbm93bn0uXG4gICAgICovXG4gICAgZnVuY3Rpb24gZnJvbUh0dHBDb2RlKGNvZGUpIHtcbiAgICAgICAgaWYgKGNvZGUgPj0gMjAwICYmIGNvZGUgPCAzMDApIHtcbiAgICAgICAgICAgIHJldHVybiBTdGF0dXMuU3VjY2VzcztcbiAgICAgICAgfVxuICAgICAgICBpZiAoY29kZSA9PT0gNDI5KSB7XG4gICAgICAgICAgICByZXR1cm4gU3RhdHVzLlJhdGVMaW1pdDtcbiAgICAgICAgfVxuICAgICAgICBpZiAoY29kZSA+PSA0MDAgJiYgY29kZSA8IDUwMCkge1xuICAgICAgICAgICAgcmV0dXJuIFN0YXR1cy5JbnZhbGlkO1xuICAgICAgICB9XG4gICAgICAgIGlmIChjb2RlID49IDUwMCkge1xuICAgICAgICAgICAgcmV0dXJuIFN0YXR1cy5GYWlsZWQ7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIFN0YXR1cy5Vbmtub3duO1xuICAgIH1cbiAgICBTdGF0dXMuZnJvbUh0dHBDb2RlID0gZnJvbUh0dHBDb2RlO1xufSkoU3RhdHVzIHx8IChTdGF0dXMgPSB7fSkpO1xuLy8jIHNvdXJjZU1hcHBpbmdVUkw9c3RhdHVzLmpzLm1hcCIsIi8qKlxuICogQ29uc3VtZXMgdGhlIHByb21pc2UgYW5kIGxvZ3MgdGhlIGVycm9yIHdoZW4gaXQgcmVqZWN0cy5cbiAqIEBwYXJhbSBwcm9taXNlIEEgcHJvbWlzZSB0byBmb3JnZXQuXG4gKi9cbmV4cG9ydCBmdW5jdGlvbiBmb3JnZXQocHJvbWlzZSkge1xuICAgIHByb21pc2UudGhlbihudWxsLCBmdW5jdGlvbiAoZSkge1xuICAgICAgICAvLyBUT0RPOiBVc2UgYSBiZXR0ZXIgbG9nZ2luZyBtZWNoYW5pc21cbiAgICAgICAgY29uc29sZS5lcnJvcihlKTtcbiAgICB9KTtcbn1cbi8vIyBzb3VyY2VNYXBwaW5nVVJMPWFzeW5jLmpzLm1hcCIsImltcG9ydCAqIGFzIHRzbGliXzEgZnJvbSBcInRzbGliXCI7XG5pbXBvcnQgeyBTZW50cnlFcnJvciB9IGZyb20gJy4vZXJyb3InO1xuLyoqIFJlZ3VsYXIgZXhwcmVzc2lvbiB1c2VkIHRvIHBhcnNlIGEgRHNuLiAqL1xudmFyIERTTl9SRUdFWCA9IC9eKD86KFxcdyspOilcXC9cXC8oPzooXFx3KykoPzo6KFxcdyspKT9AKShbXFx3XFwuLV0rKSg/OjooXFxkKykpP1xcLyguKykvO1xuLyoqIEVycm9yIG1lc3NhZ2UgKi9cbnZhciBFUlJPUl9NRVNTQUdFID0gJ0ludmFsaWQgRHNuJztcbi8qKiBUaGUgU2VudHJ5IERzbiwgaWRlbnRpZnlpbmcgYSBTZW50cnkgaW5zdGFuY2UgYW5kIHByb2plY3QuICovXG52YXIgRHNuID0gLyoqIEBjbGFzcyAqLyAoZnVuY3Rpb24gKCkge1xuICAgIC8qKiBDcmVhdGVzIGEgbmV3IERzbiBjb21wb25lbnQgKi9cbiAgICBmdW5jdGlvbiBEc24oZnJvbSkge1xuICAgICAgICBpZiAodHlwZW9mIGZyb20gPT09ICdzdHJpbmcnKSB7XG4gICAgICAgICAgICB0aGlzLl9mcm9tU3RyaW5nKGZyb20pO1xuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgdGhpcy5fZnJvbUNvbXBvbmVudHMoZnJvbSk7XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5fdmFsaWRhdGUoKTtcbiAgICB9XG4gICAgLyoqXG4gICAgICogUmVuZGVycyB0aGUgc3RyaW5nIHJlcHJlc2VudGF0aW9uIG9mIHRoaXMgRHNuLlxuICAgICAqXG4gICAgICogQnkgZGVmYXVsdCwgdGhpcyB3aWxsIHJlbmRlciB0aGUgcHVibGljIHJlcHJlc2VudGF0aW9uIHdpdGhvdXQgdGhlIHBhc3N3b3JkXG4gICAgICogY29tcG9uZW50LiBUbyBnZXQgdGhlIGRlcHJlY2F0ZWQgcHJpdmF0ZSByZXByZXNlbnRhdGlvbiwgc2V0IGB3aXRoUGFzc3dvcmRgXG4gICAgICogdG8gdHJ1ZS5cbiAgICAgKlxuICAgICAqIEBwYXJhbSB3aXRoUGFzc3dvcmQgV2hlbiBzZXQgdG8gdHJ1ZSwgdGhlIHBhc3N3b3JkIHdpbGwgYmUgaW5jbHVkZWQuXG4gICAgICovXG4gICAgRHNuLnByb3RvdHlwZS50b1N0cmluZyA9IGZ1bmN0aW9uICh3aXRoUGFzc3dvcmQpIHtcbiAgICAgICAgaWYgKHdpdGhQYXNzd29yZCA9PT0gdm9pZCAwKSB7IHdpdGhQYXNzd29yZCA9IGZhbHNlOyB9XG4gICAgICAgIC8vIHRzbGludDpkaXNhYmxlLW5leHQtbGluZTpuby10aGlzLWFzc2lnbm1lbnRcbiAgICAgICAgdmFyIF9hID0gdGhpcywgaG9zdCA9IF9hLmhvc3QsIHBhdGggPSBfYS5wYXRoLCBwYXNzID0gX2EucGFzcywgcG9ydCA9IF9hLnBvcnQsIHByb2plY3RJZCA9IF9hLnByb2plY3RJZCwgcHJvdG9jb2wgPSBfYS5wcm90b2NvbCwgdXNlciA9IF9hLnVzZXI7XG4gICAgICAgIHJldHVybiAocHJvdG9jb2wgKyBcIjovL1wiICsgdXNlciArICh3aXRoUGFzc3dvcmQgJiYgcGFzcyA/IFwiOlwiICsgcGFzcyA6ICcnKSArXG4gICAgICAgICAgICAoXCJAXCIgKyBob3N0ICsgKHBvcnQgPyBcIjpcIiArIHBvcnQgOiAnJykgKyBcIi9cIiArIChwYXRoID8gcGF0aCArIFwiL1wiIDogcGF0aCkgKyBwcm9qZWN0SWQpKTtcbiAgICB9O1xuICAgIC8qKiBQYXJzZXMgYSBzdHJpbmcgaW50byB0aGlzIERzbi4gKi9cbiAgICBEc24ucHJvdG90eXBlLl9mcm9tU3RyaW5nID0gZnVuY3Rpb24gKHN0cikge1xuICAgICAgICB2YXIgbWF0Y2ggPSBEU05fUkVHRVguZXhlYyhzdHIpO1xuICAgICAgICBpZiAoIW1hdGNoKSB7XG4gICAgICAgICAgICB0aHJvdyBuZXcgU2VudHJ5RXJyb3IoRVJST1JfTUVTU0FHRSk7XG4gICAgICAgIH1cbiAgICAgICAgdmFyIF9hID0gdHNsaWJfMS5fX3JlYWQobWF0Y2guc2xpY2UoMSksIDYpLCBwcm90b2NvbCA9IF9hWzBdLCB1c2VyID0gX2FbMV0sIF9iID0gX2FbMl0sIHBhc3MgPSBfYiA9PT0gdm9pZCAwID8gJycgOiBfYiwgaG9zdCA9IF9hWzNdLCBfYyA9IF9hWzRdLCBwb3J0ID0gX2MgPT09IHZvaWQgMCA/ICcnIDogX2MsIGxhc3RQYXRoID0gX2FbNV07XG4gICAgICAgIHZhciBwYXRoID0gJyc7XG4gICAgICAgIHZhciBwcm9qZWN0SWQgPSBsYXN0UGF0aDtcbiAgICAgICAgdmFyIHNwbGl0ID0gcHJvamVjdElkLnNwbGl0KCcvJyk7XG4gICAgICAgIGlmIChzcGxpdC5sZW5ndGggPiAxKSB7XG4gICAgICAgICAgICBwYXRoID0gc3BsaXQuc2xpY2UoMCwgLTEpLmpvaW4oJy8nKTtcbiAgICAgICAgICAgIHByb2plY3RJZCA9IHNwbGl0LnBvcCgpO1xuICAgICAgICB9XG4gICAgICAgIGlmIChwcm9qZWN0SWQpIHtcbiAgICAgICAgICAgIHZhciBwcm9qZWN0TWF0Y2ggPSBwcm9qZWN0SWQubWF0Y2goL15cXGQrLyk7XG4gICAgICAgICAgICBpZiAocHJvamVjdE1hdGNoKSB7XG4gICAgICAgICAgICAgICAgcHJvamVjdElkID0gcHJvamVjdE1hdGNoWzBdO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHRoaXMuX2Zyb21Db21wb25lbnRzKHsgaG9zdDogaG9zdCwgcGFzczogcGFzcywgcGF0aDogcGF0aCwgcHJvamVjdElkOiBwcm9qZWN0SWQsIHBvcnQ6IHBvcnQsIHByb3RvY29sOiBwcm90b2NvbCwgdXNlcjogdXNlciB9KTtcbiAgICB9O1xuICAgIC8qKiBNYXBzIERzbiBjb21wb25lbnRzIGludG8gdGhpcyBpbnN0YW5jZS4gKi9cbiAgICBEc24ucHJvdG90eXBlLl9mcm9tQ29tcG9uZW50cyA9IGZ1bmN0aW9uIChjb21wb25lbnRzKSB7XG4gICAgICAgIHRoaXMucHJvdG9jb2wgPSBjb21wb25lbnRzLnByb3RvY29sO1xuICAgICAgICB0aGlzLnVzZXIgPSBjb21wb25lbnRzLnVzZXI7XG4gICAgICAgIHRoaXMucGFzcyA9IGNvbXBvbmVudHMucGFzcyB8fCAnJztcbiAgICAgICAgdGhpcy5ob3N0ID0gY29tcG9uZW50cy5ob3N0O1xuICAgICAgICB0aGlzLnBvcnQgPSBjb21wb25lbnRzLnBvcnQgfHwgJyc7XG4gICAgICAgIHRoaXMucGF0aCA9IGNvbXBvbmVudHMucGF0aCB8fCAnJztcbiAgICAgICAgdGhpcy5wcm9qZWN0SWQgPSBjb21wb25lbnRzLnByb2plY3RJZDtcbiAgICB9O1xuICAgIC8qKiBWYWxpZGF0ZXMgdGhpcyBEc24gYW5kIHRocm93cyBvbiBlcnJvci4gKi9cbiAgICBEc24ucHJvdG90eXBlLl92YWxpZGF0ZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdmFyIF90aGlzID0gdGhpcztcbiAgICAgICAgWydwcm90b2NvbCcsICd1c2VyJywgJ2hvc3QnLCAncHJvamVjdElkJ10uZm9yRWFjaChmdW5jdGlvbiAoY29tcG9uZW50KSB7XG4gICAgICAgICAgICBpZiAoIV90aGlzW2NvbXBvbmVudF0pIHtcbiAgICAgICAgICAgICAgICB0aHJvdyBuZXcgU2VudHJ5RXJyb3IoRVJST1JfTUVTU0FHRSArIFwiOiBcIiArIGNvbXBvbmVudCArIFwiIG1pc3NpbmdcIik7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgICBpZiAoIXRoaXMucHJvamVjdElkLm1hdGNoKC9eXFxkKyQvKSkge1xuICAgICAgICAgICAgdGhyb3cgbmV3IFNlbnRyeUVycm9yKEVSUk9SX01FU1NBR0UgKyBcIjogSW52YWxpZCBwcm9qZWN0SWQgXCIgKyB0aGlzLnByb2plY3RJZCk7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHRoaXMucHJvdG9jb2wgIT09ICdodHRwJyAmJiB0aGlzLnByb3RvY29sICE9PSAnaHR0cHMnKSB7XG4gICAgICAgICAgICB0aHJvdyBuZXcgU2VudHJ5RXJyb3IoRVJST1JfTUVTU0FHRSArIFwiOiBJbnZhbGlkIHByb3RvY29sIFwiICsgdGhpcy5wcm90b2NvbCk7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHRoaXMucG9ydCAmJiBpc05hTihwYXJzZUludCh0aGlzLnBvcnQsIDEwKSkpIHtcbiAgICAgICAgICAgIHRocm93IG5ldyBTZW50cnlFcnJvcihFUlJPUl9NRVNTQUdFICsgXCI6IEludmFsaWQgcG9ydCBcIiArIHRoaXMucG9ydCk7XG4gICAgICAgIH1cbiAgICB9O1xuICAgIHJldHVybiBEc247XG59KCkpO1xuZXhwb3J0IHsgRHNuIH07XG4vLyMgc291cmNlTWFwcGluZ1VSTD1kc24uanMubWFwIiwiaW1wb3J0ICogYXMgdHNsaWJfMSBmcm9tIFwidHNsaWJcIjtcbmltcG9ydCB7IHNldFByb3RvdHlwZU9mIH0gZnJvbSAnLi9wb2x5ZmlsbCc7XG4vKiogQW4gZXJyb3IgZW1pdHRlZCBieSBTZW50cnkgU0RLcyBhbmQgcmVsYXRlZCB1dGlsaXRpZXMuICovXG52YXIgU2VudHJ5RXJyb3IgPSAvKiogQGNsYXNzICovIChmdW5jdGlvbiAoX3N1cGVyKSB7XG4gICAgdHNsaWJfMS5fX2V4dGVuZHMoU2VudHJ5RXJyb3IsIF9zdXBlcik7XG4gICAgZnVuY3Rpb24gU2VudHJ5RXJyb3IobWVzc2FnZSkge1xuICAgICAgICB2YXIgX25ld1RhcmdldCA9IHRoaXMuY29uc3RydWN0b3I7XG4gICAgICAgIHZhciBfdGhpcyA9IF9zdXBlci5jYWxsKHRoaXMsIG1lc3NhZ2UpIHx8IHRoaXM7XG4gICAgICAgIF90aGlzLm1lc3NhZ2UgPSBtZXNzYWdlO1xuICAgICAgICAvLyB0c2xpbnQ6ZGlzYWJsZTpuby11bnNhZmUtYW55XG4gICAgICAgIF90aGlzLm5hbWUgPSBfbmV3VGFyZ2V0LnByb3RvdHlwZS5jb25zdHJ1Y3Rvci5uYW1lO1xuICAgICAgICBzZXRQcm90b3R5cGVPZihfdGhpcywgX25ld1RhcmdldC5wcm90b3R5cGUpO1xuICAgICAgICByZXR1cm4gX3RoaXM7XG4gICAgfVxuICAgIHJldHVybiBTZW50cnlFcnJvcjtcbn0oRXJyb3IpKTtcbmV4cG9ydCB7IFNlbnRyeUVycm9yIH07XG4vLyMgc291cmNlTWFwcGluZ1VSTD1lcnJvci5qcy5tYXAiLCJleHBvcnQgKiBmcm9tICcuL2FzeW5jJztcbmV4cG9ydCAqIGZyb20gJy4vZXJyb3InO1xuZXhwb3J0ICogZnJvbSAnLi9pcyc7XG5leHBvcnQgKiBmcm9tICcuL2xvZ2dlcic7XG5leHBvcnQgKiBmcm9tICcuL21lbW8nO1xuZXhwb3J0ICogZnJvbSAnLi9taXNjJztcbmV4cG9ydCAqIGZyb20gJy4vb2JqZWN0JztcbmV4cG9ydCAqIGZyb20gJy4vcGF0aCc7XG5leHBvcnQgKiBmcm9tICcuL3Byb21pc2VidWZmZXInO1xuZXhwb3J0ICogZnJvbSAnLi9zdHJpbmcnO1xuZXhwb3J0ICogZnJvbSAnLi9zdXBwb3J0cyc7XG5leHBvcnQgKiBmcm9tICcuL3N5bmNwcm9taXNlJztcbmV4cG9ydCAqIGZyb20gJy4vaW5zdHJ1bWVudCc7XG5leHBvcnQgKiBmcm9tICcuL2Rzbic7XG4vLyMgc291cmNlTWFwcGluZ1VSTD1pbmRleC5qcy5tYXAiLCIvKiB0c2xpbnQ6ZGlzYWJsZTpvbmx5LWFycm93LWZ1bmN0aW9ucyBuby11bnNhZmUtYW55ICovXG5pbXBvcnQgKiBhcyB0c2xpYl8xIGZyb20gXCJ0c2xpYlwiO1xuaW1wb3J0IHsgaXNJbnN0YW5jZU9mLCBpc1N0cmluZyB9IGZyb20gJy4vaXMnO1xuaW1wb3J0IHsgbG9nZ2VyIH0gZnJvbSAnLi9sb2dnZXInO1xuaW1wb3J0IHsgZ2V0RnVuY3Rpb25OYW1lLCBnZXRHbG9iYWxPYmplY3QgfSBmcm9tICcuL21pc2MnO1xuaW1wb3J0IHsgZmlsbCB9IGZyb20gJy4vb2JqZWN0JztcbmltcG9ydCB7IHN1cHBvcnRzSGlzdG9yeSwgc3VwcG9ydHNOYXRpdmVGZXRjaCB9IGZyb20gJy4vc3VwcG9ydHMnO1xudmFyIGdsb2JhbCA9IGdldEdsb2JhbE9iamVjdCgpO1xuLyoqXG4gKiBJbnN0cnVtZW50IG5hdGl2ZSBBUElzIHRvIGNhbGwgaGFuZGxlcnMgdGhhdCBjYW4gYmUgdXNlZCB0byBjcmVhdGUgYnJlYWRjcnVtYnMsIEFQTSBzcGFucyBldGMuXG4gKiAgLSBDb25zb2xlIEFQSVxuICogIC0gRmV0Y2ggQVBJXG4gKiAgLSBYSFIgQVBJXG4gKiAgLSBIaXN0b3J5IEFQSVxuICogIC0gRE9NIEFQSSAoY2xpY2svdHlwaW5nKVxuICogIC0gRXJyb3IgQVBJXG4gKiAgLSBVbmhhbmRsZWRSZWplY3Rpb24gQVBJXG4gKi9cbnZhciBoYW5kbGVycyA9IHt9O1xudmFyIGluc3RydW1lbnRlZCA9IHt9O1xuLyoqIEluc3RydW1lbnRzIGdpdmVuIEFQSSAqL1xuZnVuY3Rpb24gaW5zdHJ1bWVudCh0eXBlKSB7XG4gICAgaWYgKGluc3RydW1lbnRlZFt0eXBlXSkge1xuICAgICAgICByZXR1cm47XG4gICAgfVxuICAgIGluc3RydW1lbnRlZFt0eXBlXSA9IHRydWU7XG4gICAgc3dpdGNoICh0eXBlKSB7XG4gICAgICAgIGNhc2UgJ2NvbnNvbGUnOlxuICAgICAgICAgICAgaW5zdHJ1bWVudENvbnNvbGUoKTtcbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICBjYXNlICdkb20nOlxuICAgICAgICAgICAgaW5zdHJ1bWVudERPTSgpO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgIGNhc2UgJ3hocic6XG4gICAgICAgICAgICBpbnN0cnVtZW50WEhSKCk7XG4gICAgICAgICAgICBicmVhaztcbiAgICAgICAgY2FzZSAnZmV0Y2gnOlxuICAgICAgICAgICAgaW5zdHJ1bWVudEZldGNoKCk7XG4gICAgICAgICAgICBicmVhaztcbiAgICAgICAgY2FzZSAnaGlzdG9yeSc6XG4gICAgICAgICAgICBpbnN0cnVtZW50SGlzdG9yeSgpO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgIGNhc2UgJ2Vycm9yJzpcbiAgICAgICAgICAgIGluc3RydW1lbnRFcnJvcigpO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgIGNhc2UgJ3VuaGFuZGxlZHJlamVjdGlvbic6XG4gICAgICAgICAgICBpbnN0cnVtZW50VW5oYW5kbGVkUmVqZWN0aW9uKCk7XG4gICAgICAgICAgICBicmVhaztcbiAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICAgIGxvZ2dlci53YXJuKCd1bmtub3duIGluc3RydW1lbnRhdGlvbiB0eXBlOicsIHR5cGUpO1xuICAgIH1cbn1cbi8qKlxuICogQWRkIGhhbmRsZXIgdGhhdCB3aWxsIGJlIGNhbGxlZCB3aGVuIGdpdmVuIHR5cGUgb2YgaW5zdHJ1bWVudGF0aW9uIHRyaWdnZXJzLlxuICogVXNlIGF0IHlvdXIgb3duIHJpc2ssIHRoaXMgbWlnaHQgYnJlYWsgd2l0aG91dCBjaGFuZ2Vsb2cgbm90aWNlLCBvbmx5IHVzZWQgaW50ZXJuYWxseS5cbiAqIEBoaWRkZW5cbiAqL1xuZXhwb3J0IGZ1bmN0aW9uIGFkZEluc3RydW1lbnRhdGlvbkhhbmRsZXIoaGFuZGxlcikge1xuICAgIC8vIHRzbGludDpkaXNhYmxlLW5leHQtbGluZTpzdHJpY3QtdHlwZS1wcmVkaWNhdGVzXG4gICAgaWYgKCFoYW5kbGVyIHx8IHR5cGVvZiBoYW5kbGVyLnR5cGUgIT09ICdzdHJpbmcnIHx8IHR5cGVvZiBoYW5kbGVyLmNhbGxiYWNrICE9PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgIHJldHVybjtcbiAgICB9XG4gICAgaGFuZGxlcnNbaGFuZGxlci50eXBlXSA9IGhhbmRsZXJzW2hhbmRsZXIudHlwZV0gfHwgW107XG4gICAgaGFuZGxlcnNbaGFuZGxlci50eXBlXS5wdXNoKGhhbmRsZXIuY2FsbGJhY2spO1xuICAgIGluc3RydW1lbnQoaGFuZGxlci50eXBlKTtcbn1cbi8qKiBKU0RvYyAqL1xuZnVuY3Rpb24gdHJpZ2dlckhhbmRsZXJzKHR5cGUsIGRhdGEpIHtcbiAgICB2YXIgZV8xLCBfYTtcbiAgICBpZiAoIXR5cGUgfHwgIWhhbmRsZXJzW3R5cGVdKSB7XG4gICAgICAgIHJldHVybjtcbiAgICB9XG4gICAgdHJ5IHtcbiAgICAgICAgZm9yICh2YXIgX2IgPSB0c2xpYl8xLl9fdmFsdWVzKGhhbmRsZXJzW3R5cGVdIHx8IFtdKSwgX2MgPSBfYi5uZXh0KCk7ICFfYy5kb25lOyBfYyA9IF9iLm5leHQoKSkge1xuICAgICAgICAgICAgdmFyIGhhbmRsZXIgPSBfYy52YWx1ZTtcbiAgICAgICAgICAgIHRyeSB7XG4gICAgICAgICAgICAgICAgaGFuZGxlcihkYXRhKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGNhdGNoIChlKSB7XG4gICAgICAgICAgICAgICAgbG9nZ2VyLmVycm9yKFwiRXJyb3Igd2hpbGUgdHJpZ2dlcmluZyBpbnN0cnVtZW50YXRpb24gaGFuZGxlci5cXG5UeXBlOiBcIiArIHR5cGUgKyBcIlxcbk5hbWU6IFwiICsgZ2V0RnVuY3Rpb25OYW1lKGhhbmRsZXIpICsgXCJcXG5FcnJvcjogXCIgKyBlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cbiAgICBjYXRjaCAoZV8xXzEpIHsgZV8xID0geyBlcnJvcjogZV8xXzEgfTsgfVxuICAgIGZpbmFsbHkge1xuICAgICAgICB0cnkge1xuICAgICAgICAgICAgaWYgKF9jICYmICFfYy5kb25lICYmIChfYSA9IF9iLnJldHVybikpIF9hLmNhbGwoX2IpO1xuICAgICAgICB9XG4gICAgICAgIGZpbmFsbHkgeyBpZiAoZV8xKSB0aHJvdyBlXzEuZXJyb3I7IH1cbiAgICB9XG59XG4vKiogSlNEb2MgKi9cbmZ1bmN0aW9uIGluc3RydW1lbnRDb25zb2xlKCkge1xuICAgIGlmICghKCdjb25zb2xlJyBpbiBnbG9iYWwpKSB7XG4gICAgICAgIHJldHVybjtcbiAgICB9XG4gICAgWydkZWJ1ZycsICdpbmZvJywgJ3dhcm4nLCAnZXJyb3InLCAnbG9nJywgJ2Fzc2VydCddLmZvckVhY2goZnVuY3Rpb24gKGxldmVsKSB7XG4gICAgICAgIGlmICghKGxldmVsIGluIGdsb2JhbC5jb25zb2xlKSkge1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIGZpbGwoZ2xvYmFsLmNvbnNvbGUsIGxldmVsLCBmdW5jdGlvbiAob3JpZ2luYWxDb25zb2xlTGV2ZWwpIHtcbiAgICAgICAgICAgIHJldHVybiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgdmFyIGFyZ3MgPSBbXTtcbiAgICAgICAgICAgICAgICBmb3IgKHZhciBfaSA9IDA7IF9pIDwgYXJndW1lbnRzLmxlbmd0aDsgX2krKykge1xuICAgICAgICAgICAgICAgICAgICBhcmdzW19pXSA9IGFyZ3VtZW50c1tfaV07XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIHRyaWdnZXJIYW5kbGVycygnY29uc29sZScsIHsgYXJnczogYXJncywgbGV2ZWw6IGxldmVsIH0pO1xuICAgICAgICAgICAgICAgIC8vIHRoaXMgZmFpbHMgZm9yIHNvbWUgYnJvd3NlcnMuIDooXG4gICAgICAgICAgICAgICAgaWYgKG9yaWdpbmFsQ29uc29sZUxldmVsKSB7XG4gICAgICAgICAgICAgICAgICAgIEZ1bmN0aW9uLnByb3RvdHlwZS5hcHBseS5jYWxsKG9yaWdpbmFsQ29uc29sZUxldmVsLCBnbG9iYWwuY29uc29sZSwgYXJncyk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfTtcbiAgICAgICAgfSk7XG4gICAgfSk7XG59XG4vKiogSlNEb2MgKi9cbmZ1bmN0aW9uIGluc3RydW1lbnRGZXRjaCgpIHtcbiAgICBpZiAoIXN1cHBvcnRzTmF0aXZlRmV0Y2goKSkge1xuICAgICAgICByZXR1cm47XG4gICAgfVxuICAgIGZpbGwoZ2xvYmFsLCAnZmV0Y2gnLCBmdW5jdGlvbiAob3JpZ2luYWxGZXRjaCkge1xuICAgICAgICByZXR1cm4gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgdmFyIGFyZ3MgPSBbXTtcbiAgICAgICAgICAgIGZvciAodmFyIF9pID0gMDsgX2kgPCBhcmd1bWVudHMubGVuZ3RoOyBfaSsrKSB7XG4gICAgICAgICAgICAgICAgYXJnc1tfaV0gPSBhcmd1bWVudHNbX2ldO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdmFyIGNvbW1vbkhhbmRsZXJEYXRhID0ge1xuICAgICAgICAgICAgICAgIGFyZ3M6IGFyZ3MsXG4gICAgICAgICAgICAgICAgZmV0Y2hEYXRhOiB7XG4gICAgICAgICAgICAgICAgICAgIG1ldGhvZDogZ2V0RmV0Y2hNZXRob2QoYXJncyksXG4gICAgICAgICAgICAgICAgICAgIHVybDogZ2V0RmV0Y2hVcmwoYXJncyksXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBzdGFydFRpbWVzdGFtcDogRGF0ZS5ub3coKSxcbiAgICAgICAgICAgIH07XG4gICAgICAgICAgICB0cmlnZ2VySGFuZGxlcnMoJ2ZldGNoJywgdHNsaWJfMS5fX2Fzc2lnbih7fSwgY29tbW9uSGFuZGxlckRhdGEpKTtcbiAgICAgICAgICAgIHJldHVybiBvcmlnaW5hbEZldGNoLmFwcGx5KGdsb2JhbCwgYXJncykudGhlbihmdW5jdGlvbiAocmVzcG9uc2UpIHtcbiAgICAgICAgICAgICAgICB0cmlnZ2VySGFuZGxlcnMoJ2ZldGNoJywgdHNsaWJfMS5fX2Fzc2lnbih7fSwgY29tbW9uSGFuZGxlckRhdGEsIHsgZW5kVGltZXN0YW1wOiBEYXRlLm5vdygpLCByZXNwb25zZTogcmVzcG9uc2UgfSkpO1xuICAgICAgICAgICAgICAgIHJldHVybiByZXNwb25zZTtcbiAgICAgICAgICAgIH0sIGZ1bmN0aW9uIChlcnJvcikge1xuICAgICAgICAgICAgICAgIHRyaWdnZXJIYW5kbGVycygnZmV0Y2gnLCB0c2xpYl8xLl9fYXNzaWduKHt9LCBjb21tb25IYW5kbGVyRGF0YSwgeyBlbmRUaW1lc3RhbXA6IERhdGUubm93KCksIGVycm9yOiBlcnJvciB9KSk7XG4gICAgICAgICAgICAgICAgdGhyb3cgZXJyb3I7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfTtcbiAgICB9KTtcbn1cbi8qKiBFeHRyYWN0IGBtZXRob2RgIGZyb20gZmV0Y2ggY2FsbCBhcmd1bWVudHMgKi9cbmZ1bmN0aW9uIGdldEZldGNoTWV0aG9kKGZldGNoQXJncykge1xuICAgIGlmIChmZXRjaEFyZ3MgPT09IHZvaWQgMCkgeyBmZXRjaEFyZ3MgPSBbXTsgfVxuICAgIGlmICgnUmVxdWVzdCcgaW4gZ2xvYmFsICYmIGlzSW5zdGFuY2VPZihmZXRjaEFyZ3NbMF0sIFJlcXVlc3QpICYmIGZldGNoQXJnc1swXS5tZXRob2QpIHtcbiAgICAgICAgcmV0dXJuIFN0cmluZyhmZXRjaEFyZ3NbMF0ubWV0aG9kKS50b1VwcGVyQ2FzZSgpO1xuICAgIH1cbiAgICBpZiAoZmV0Y2hBcmdzWzFdICYmIGZldGNoQXJnc1sxXS5tZXRob2QpIHtcbiAgICAgICAgcmV0dXJuIFN0cmluZyhmZXRjaEFyZ3NbMV0ubWV0aG9kKS50b1VwcGVyQ2FzZSgpO1xuICAgIH1cbiAgICByZXR1cm4gJ0dFVCc7XG59XG4vKiogRXh0cmFjdCBgdXJsYCBmcm9tIGZldGNoIGNhbGwgYXJndW1lbnRzICovXG5mdW5jdGlvbiBnZXRGZXRjaFVybChmZXRjaEFyZ3MpIHtcbiAgICBpZiAoZmV0Y2hBcmdzID09PSB2b2lkIDApIHsgZmV0Y2hBcmdzID0gW107IH1cbiAgICBpZiAodHlwZW9mIGZldGNoQXJnc1swXSA9PT0gJ3N0cmluZycpIHtcbiAgICAgICAgcmV0dXJuIGZldGNoQXJnc1swXTtcbiAgICB9XG4gICAgaWYgKCdSZXF1ZXN0JyBpbiBnbG9iYWwgJiYgaXNJbnN0YW5jZU9mKGZldGNoQXJnc1swXSwgUmVxdWVzdCkpIHtcbiAgICAgICAgcmV0dXJuIGZldGNoQXJnc1swXS51cmw7XG4gICAgfVxuICAgIHJldHVybiBTdHJpbmcoZmV0Y2hBcmdzWzBdKTtcbn1cbi8qKiBKU0RvYyAqL1xuZnVuY3Rpb24gaW5zdHJ1bWVudFhIUigpIHtcbiAgICBpZiAoISgnWE1MSHR0cFJlcXVlc3QnIGluIGdsb2JhbCkpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICB2YXIgeGhycHJvdG8gPSBYTUxIdHRwUmVxdWVzdC5wcm90b3R5cGU7XG4gICAgZmlsbCh4aHJwcm90bywgJ29wZW4nLCBmdW5jdGlvbiAob3JpZ2luYWxPcGVuKSB7XG4gICAgICAgIHJldHVybiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICB2YXIgYXJncyA9IFtdO1xuICAgICAgICAgICAgZm9yICh2YXIgX2kgPSAwOyBfaSA8IGFyZ3VtZW50cy5sZW5ndGg7IF9pKyspIHtcbiAgICAgICAgICAgICAgICBhcmdzW19pXSA9IGFyZ3VtZW50c1tfaV07XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICB2YXIgeGhyID0gdGhpczsgLy8gdHNsaW50OmRpc2FibGUtbGluZTpuby10aGlzLWFzc2lnbm1lbnRcbiAgICAgICAgICAgIHZhciB1cmwgPSBhcmdzWzFdO1xuICAgICAgICAgICAgeGhyLl9fc2VudHJ5X3hocl9fID0ge1xuICAgICAgICAgICAgICAgIG1ldGhvZDogaXNTdHJpbmcoYXJnc1swXSkgPyBhcmdzWzBdLnRvVXBwZXJDYXNlKCkgOiBhcmdzWzBdLFxuICAgICAgICAgICAgICAgIHVybDogYXJnc1sxXSxcbiAgICAgICAgICAgIH07XG4gICAgICAgICAgICAvLyBpZiBTZW50cnkga2V5IGFwcGVhcnMgaW4gVVJMLCBkb24ndCBjYXB0dXJlIGl0IGFzIGEgcmVxdWVzdFxuICAgICAgICAgICAgaWYgKGlzU3RyaW5nKHVybCkgJiYgeGhyLl9fc2VudHJ5X3hocl9fLm1ldGhvZCA9PT0gJ1BPU1QnICYmIHVybC5tYXRjaCgvc2VudHJ5X2tleS8pKSB7XG4gICAgICAgICAgICAgICAgeGhyLl9fc2VudHJ5X293bl9yZXF1ZXN0X18gPSB0cnVlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdmFyIG9ucmVhZHlzdGF0ZWNoYW5nZUhhbmRsZXIgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgaWYgKHhoci5yZWFkeVN0YXRlID09PSA0KSB7XG4gICAgICAgICAgICAgICAgICAgIHRyeSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAvLyB0b3VjaGluZyBzdGF0dXNDb2RlIGluIHNvbWUgcGxhdGZvcm1zIHRocm93c1xuICAgICAgICAgICAgICAgICAgICAgICAgLy8gYW4gZXhjZXB0aW9uXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoeGhyLl9fc2VudHJ5X3hocl9fKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgeGhyLl9fc2VudHJ5X3hocl9fLnN0YXR1c19jb2RlID0geGhyLnN0YXR1cztcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBjYXRjaCAoZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgLyogZG8gbm90aGluZyAqL1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIHRyaWdnZXJIYW5kbGVycygneGhyJywge1xuICAgICAgICAgICAgICAgICAgICAgICAgYXJnczogYXJncyxcbiAgICAgICAgICAgICAgICAgICAgICAgIGVuZFRpbWVzdGFtcDogRGF0ZS5ub3coKSxcbiAgICAgICAgICAgICAgICAgICAgICAgIHN0YXJ0VGltZXN0YW1wOiBEYXRlLm5vdygpLFxuICAgICAgICAgICAgICAgICAgICAgICAgeGhyOiB4aHIsXG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH07XG4gICAgICAgICAgICBpZiAoJ29ucmVhZHlzdGF0ZWNoYW5nZScgaW4geGhyICYmIHR5cGVvZiB4aHIub25yZWFkeXN0YXRlY2hhbmdlID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgICAgICAgICAgZmlsbCh4aHIsICdvbnJlYWR5c3RhdGVjaGFuZ2UnLCBmdW5jdGlvbiAob3JpZ2luYWwpIHtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciByZWFkeVN0YXRlQXJncyA9IFtdO1xuICAgICAgICAgICAgICAgICAgICAgICAgZm9yICh2YXIgX2kgPSAwOyBfaSA8IGFyZ3VtZW50cy5sZW5ndGg7IF9pKyspIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZWFkeVN0YXRlQXJnc1tfaV0gPSBhcmd1bWVudHNbX2ldO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgb25yZWFkeXN0YXRlY2hhbmdlSGFuZGxlcigpO1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIG9yaWdpbmFsLmFwcGx5KHhociwgcmVhZHlTdGF0ZUFyZ3MpO1xuICAgICAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgeGhyLmFkZEV2ZW50TGlzdGVuZXIoJ3JlYWR5c3RhdGVjaGFuZ2UnLCBvbnJlYWR5c3RhdGVjaGFuZ2VIYW5kbGVyKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiBvcmlnaW5hbE9wZW4uYXBwbHkoeGhyLCBhcmdzKTtcbiAgICAgICAgfTtcbiAgICB9KTtcbiAgICBmaWxsKHhocnByb3RvLCAnc2VuZCcsIGZ1bmN0aW9uIChvcmlnaW5hbFNlbmQpIHtcbiAgICAgICAgcmV0dXJuIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHZhciBhcmdzID0gW107XG4gICAgICAgICAgICBmb3IgKHZhciBfaSA9IDA7IF9pIDwgYXJndW1lbnRzLmxlbmd0aDsgX2krKykge1xuICAgICAgICAgICAgICAgIGFyZ3NbX2ldID0gYXJndW1lbnRzW19pXTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHRyaWdnZXJIYW5kbGVycygneGhyJywge1xuICAgICAgICAgICAgICAgIGFyZ3M6IGFyZ3MsXG4gICAgICAgICAgICAgICAgc3RhcnRUaW1lc3RhbXA6IERhdGUubm93KCksXG4gICAgICAgICAgICAgICAgeGhyOiB0aGlzLFxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICByZXR1cm4gb3JpZ2luYWxTZW5kLmFwcGx5KHRoaXMsIGFyZ3MpO1xuICAgICAgICB9O1xuICAgIH0pO1xufVxudmFyIGxhc3RIcmVmO1xuLyoqIEpTRG9jICovXG5mdW5jdGlvbiBpbnN0cnVtZW50SGlzdG9yeSgpIHtcbiAgICBpZiAoIXN1cHBvcnRzSGlzdG9yeSgpKSB7XG4gICAgICAgIHJldHVybjtcbiAgICB9XG4gICAgdmFyIG9sZE9uUG9wU3RhdGUgPSBnbG9iYWwub25wb3BzdGF0ZTtcbiAgICBnbG9iYWwub25wb3BzdGF0ZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdmFyIGFyZ3MgPSBbXTtcbiAgICAgICAgZm9yICh2YXIgX2kgPSAwOyBfaSA8IGFyZ3VtZW50cy5sZW5ndGg7IF9pKyspIHtcbiAgICAgICAgICAgIGFyZ3NbX2ldID0gYXJndW1lbnRzW19pXTtcbiAgICAgICAgfVxuICAgICAgICB2YXIgdG8gPSBnbG9iYWwubG9jYXRpb24uaHJlZjtcbiAgICAgICAgLy8ga2VlcCB0cmFjayBvZiB0aGUgY3VycmVudCBVUkwgc3RhdGUsIGFzIHdlIGFsd2F5cyByZWNlaXZlIG9ubHkgdGhlIHVwZGF0ZWQgc3RhdGVcbiAgICAgICAgdmFyIGZyb20gPSBsYXN0SHJlZjtcbiAgICAgICAgbGFzdEhyZWYgPSB0bztcbiAgICAgICAgdHJpZ2dlckhhbmRsZXJzKCdoaXN0b3J5Jywge1xuICAgICAgICAgICAgZnJvbTogZnJvbSxcbiAgICAgICAgICAgIHRvOiB0byxcbiAgICAgICAgfSk7XG4gICAgICAgIGlmIChvbGRPblBvcFN0YXRlKSB7XG4gICAgICAgICAgICByZXR1cm4gb2xkT25Qb3BTdGF0ZS5hcHBseSh0aGlzLCBhcmdzKTtcbiAgICAgICAgfVxuICAgIH07XG4gICAgLyoqIEBoaWRkZW4gKi9cbiAgICBmdW5jdGlvbiBoaXN0b3J5UmVwbGFjZW1lbnRGdW5jdGlvbihvcmlnaW5hbEhpc3RvcnlGdW5jdGlvbikge1xuICAgICAgICByZXR1cm4gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgdmFyIGFyZ3MgPSBbXTtcbiAgICAgICAgICAgIGZvciAodmFyIF9pID0gMDsgX2kgPCBhcmd1bWVudHMubGVuZ3RoOyBfaSsrKSB7XG4gICAgICAgICAgICAgICAgYXJnc1tfaV0gPSBhcmd1bWVudHNbX2ldO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdmFyIHVybCA9IGFyZ3MubGVuZ3RoID4gMiA/IGFyZ3NbMl0gOiB1bmRlZmluZWQ7XG4gICAgICAgICAgICBpZiAodXJsKSB7XG4gICAgICAgICAgICAgICAgLy8gY29lcmNlIHRvIHN0cmluZyAodGhpcyBpcyB3aGF0IHB1c2hTdGF0ZSBkb2VzKVxuICAgICAgICAgICAgICAgIHZhciBmcm9tID0gbGFzdEhyZWY7XG4gICAgICAgICAgICAgICAgdmFyIHRvID0gU3RyaW5nKHVybCk7XG4gICAgICAgICAgICAgICAgLy8ga2VlcCB0cmFjayBvZiB0aGUgY3VycmVudCBVUkwgc3RhdGUsIGFzIHdlIGFsd2F5cyByZWNlaXZlIG9ubHkgdGhlIHVwZGF0ZWQgc3RhdGVcbiAgICAgICAgICAgICAgICBsYXN0SHJlZiA9IHRvO1xuICAgICAgICAgICAgICAgIHRyaWdnZXJIYW5kbGVycygnaGlzdG9yeScsIHtcbiAgICAgICAgICAgICAgICAgICAgZnJvbTogZnJvbSxcbiAgICAgICAgICAgICAgICAgICAgdG86IHRvLFxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIG9yaWdpbmFsSGlzdG9yeUZ1bmN0aW9uLmFwcGx5KHRoaXMsIGFyZ3MpO1xuICAgICAgICB9O1xuICAgIH1cbiAgICBmaWxsKGdsb2JhbC5oaXN0b3J5LCAncHVzaFN0YXRlJywgaGlzdG9yeVJlcGxhY2VtZW50RnVuY3Rpb24pO1xuICAgIGZpbGwoZ2xvYmFsLmhpc3RvcnksICdyZXBsYWNlU3RhdGUnLCBoaXN0b3J5UmVwbGFjZW1lbnRGdW5jdGlvbik7XG59XG4vKiogSlNEb2MgKi9cbmZ1bmN0aW9uIGluc3RydW1lbnRET00oKSB7XG4gICAgaWYgKCEoJ2RvY3VtZW50JyBpbiBnbG9iYWwpKSB7XG4gICAgICAgIHJldHVybjtcbiAgICB9XG4gICAgLy8gQ2FwdHVyZSBicmVhZGNydW1icyBmcm9tIGFueSBjbGljayB0aGF0IGlzIHVuaGFuZGxlZCAvIGJ1YmJsZWQgdXAgYWxsIHRoZSB3YXlcbiAgICAvLyB0byB0aGUgZG9jdW1lbnQuIERvIHRoaXMgYmVmb3JlIHdlIGluc3RydW1lbnQgYWRkRXZlbnRMaXN0ZW5lci5cbiAgICBnbG9iYWwuZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBkb21FdmVudEhhbmRsZXIoJ2NsaWNrJywgdHJpZ2dlckhhbmRsZXJzLmJpbmQobnVsbCwgJ2RvbScpKSwgZmFsc2UpO1xuICAgIGdsb2JhbC5kb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKCdrZXlwcmVzcycsIGtleXByZXNzRXZlbnRIYW5kbGVyKHRyaWdnZXJIYW5kbGVycy5iaW5kKG51bGwsICdkb20nKSksIGZhbHNlKTtcbiAgICAvLyBBZnRlciBob29raW5nIGludG8gZG9jdW1lbnQgYnViYmxlZCB1cCBjbGljayBhbmQga2V5cHJlc3NlcyBldmVudHMsIHdlIGFsc28gaG9vayBpbnRvIHVzZXIgaGFuZGxlZCBjbGljayAmIGtleXByZXNzZXMuXG4gICAgWydFdmVudFRhcmdldCcsICdOb2RlJ10uZm9yRWFjaChmdW5jdGlvbiAodGFyZ2V0KSB7XG4gICAgICAgIHZhciBwcm90byA9IGdsb2JhbFt0YXJnZXRdICYmIGdsb2JhbFt0YXJnZXRdLnByb3RvdHlwZTtcbiAgICAgICAgaWYgKCFwcm90byB8fCAhcHJvdG8uaGFzT3duUHJvcGVydHkgfHwgIXByb3RvLmhhc093blByb3BlcnR5KCdhZGRFdmVudExpc3RlbmVyJykpIHtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuICAgICAgICBmaWxsKHByb3RvLCAnYWRkRXZlbnRMaXN0ZW5lcicsIGZ1bmN0aW9uIChvcmlnaW5hbCkge1xuICAgICAgICAgICAgcmV0dXJuIGZ1bmN0aW9uIChldmVudE5hbWUsIGZuLCBvcHRpb25zKSB7XG4gICAgICAgICAgICAgICAgaWYgKGZuICYmIGZuLmhhbmRsZUV2ZW50KSB7XG4gICAgICAgICAgICAgICAgICAgIGlmIChldmVudE5hbWUgPT09ICdjbGljaycpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGZpbGwoZm4sICdoYW5kbGVFdmVudCcsIGZ1bmN0aW9uIChpbm5lck9yaWdpbmFsKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGZ1bmN0aW9uIChldmVudCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBkb21FdmVudEhhbmRsZXIoJ2NsaWNrJywgdHJpZ2dlckhhbmRsZXJzLmJpbmQobnVsbCwgJ2RvbScpKShldmVudCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBpbm5lck9yaWdpbmFsLmNhbGwodGhpcywgZXZlbnQpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBpZiAoZXZlbnROYW1lID09PSAna2V5cHJlc3MnKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBmaWxsKGZuLCAnaGFuZGxlRXZlbnQnLCBmdW5jdGlvbiAoaW5uZXJPcmlnaW5hbCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBmdW5jdGlvbiAoZXZlbnQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAga2V5cHJlc3NFdmVudEhhbmRsZXIodHJpZ2dlckhhbmRsZXJzLmJpbmQobnVsbCwgJ2RvbScpKShldmVudCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBpbm5lck9yaWdpbmFsLmNhbGwodGhpcywgZXZlbnQpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKGV2ZW50TmFtZSA9PT0gJ2NsaWNrJykge1xuICAgICAgICAgICAgICAgICAgICAgICAgZG9tRXZlbnRIYW5kbGVyKCdjbGljaycsIHRyaWdnZXJIYW5kbGVycy5iaW5kKG51bGwsICdkb20nKSwgdHJ1ZSkodGhpcyk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgaWYgKGV2ZW50TmFtZSA9PT0gJ2tleXByZXNzJykge1xuICAgICAgICAgICAgICAgICAgICAgICAga2V5cHJlc3NFdmVudEhhbmRsZXIodHJpZ2dlckhhbmRsZXJzLmJpbmQobnVsbCwgJ2RvbScpKSh0aGlzKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICByZXR1cm4gb3JpZ2luYWwuY2FsbCh0aGlzLCBldmVudE5hbWUsIGZuLCBvcHRpb25zKTtcbiAgICAgICAgICAgIH07XG4gICAgICAgIH0pO1xuICAgICAgICBmaWxsKHByb3RvLCAncmVtb3ZlRXZlbnRMaXN0ZW5lcicsIGZ1bmN0aW9uIChvcmlnaW5hbCkge1xuICAgICAgICAgICAgcmV0dXJuIGZ1bmN0aW9uIChldmVudE5hbWUsIGZuLCBvcHRpb25zKSB7XG4gICAgICAgICAgICAgICAgdmFyIGNhbGxiYWNrID0gZm47XG4gICAgICAgICAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgICAgICAgICAgY2FsbGJhY2sgPSBjYWxsYmFjayAmJiAoY2FsbGJhY2suX19zZW50cnlfd3JhcHBlZF9fIHx8IGNhbGxiYWNrKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgY2F0Y2ggKGUpIHtcbiAgICAgICAgICAgICAgICAgICAgLy8gaWdub3JlLCBhY2Nlc3NpbmcgX19zZW50cnlfd3JhcHBlZF9fIHdpbGwgdGhyb3cgaW4gc29tZSBTZWxlbml1bSBlbnZpcm9ubWVudHNcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgcmV0dXJuIG9yaWdpbmFsLmNhbGwodGhpcywgZXZlbnROYW1lLCBjYWxsYmFjaywgb3B0aW9ucyk7XG4gICAgICAgICAgICB9O1xuICAgICAgICB9KTtcbiAgICB9KTtcbn1cbnZhciBkZWJvdW5jZUR1cmF0aW9uID0gMTAwMDtcbnZhciBkZWJvdW5jZVRpbWVyID0gMDtcbnZhciBrZXlwcmVzc1RpbWVvdXQ7XG52YXIgbGFzdENhcHR1cmVkRXZlbnQ7XG4vKipcbiAqIFdyYXBzIGFkZEV2ZW50TGlzdGVuZXIgdG8gY2FwdHVyZSBVSSBicmVhZGNydW1ic1xuICogQHBhcmFtIG5hbWUgdGhlIGV2ZW50IG5hbWUgKGUuZy4gXCJjbGlja1wiKVxuICogQHBhcmFtIGhhbmRsZXIgZnVuY3Rpb24gdGhhdCB3aWxsIGJlIHRyaWdnZXJlZFxuICogQHBhcmFtIGRlYm91bmNlIGRlY2lkZXMgd2hldGhlciBpdCBzaG91bGQgd2FpdCB0aWxsIGFub3RoZXIgZXZlbnQgbG9vcFxuICogQHJldHVybnMgd3JhcHBlZCBicmVhZGNydW1iIGV2ZW50cyBoYW5kbGVyXG4gKiBAaGlkZGVuXG4gKi9cbmZ1bmN0aW9uIGRvbUV2ZW50SGFuZGxlcihuYW1lLCBoYW5kbGVyLCBkZWJvdW5jZSkge1xuICAgIGlmIChkZWJvdW5jZSA9PT0gdm9pZCAwKSB7IGRlYm91bmNlID0gZmFsc2U7IH1cbiAgICByZXR1cm4gZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICAgIC8vIHJlc2V0IGtleXByZXNzIHRpbWVvdXQ7IGUuZy4gdHJpZ2dlcmluZyBhICdjbGljaycgYWZ0ZXJcbiAgICAgICAgLy8gYSAna2V5cHJlc3MnIHdpbGwgcmVzZXQgdGhlIGtleXByZXNzIGRlYm91bmNlIHNvIHRoYXQgYSBuZXdcbiAgICAgICAgLy8gc2V0IG9mIGtleXByZXNzZXMgY2FuIGJlIHJlY29yZGVkXG4gICAgICAgIGtleXByZXNzVGltZW91dCA9IHVuZGVmaW5lZDtcbiAgICAgICAgLy8gSXQncyBwb3NzaWJsZSB0aGlzIGhhbmRsZXIgbWlnaHQgdHJpZ2dlciBtdWx0aXBsZSB0aW1lcyBmb3IgdGhlIHNhbWVcbiAgICAgICAgLy8gZXZlbnQgKGUuZy4gZXZlbnQgcHJvcGFnYXRpb24gdGhyb3VnaCBub2RlIGFuY2VzdG9ycykuIElnbm9yZSBpZiB3ZSd2ZVxuICAgICAgICAvLyBhbHJlYWR5IGNhcHR1cmVkIHRoZSBldmVudC5cbiAgICAgICAgaWYgKCFldmVudCB8fCBsYXN0Q2FwdHVyZWRFdmVudCA9PT0gZXZlbnQpIHtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuICAgICAgICBsYXN0Q2FwdHVyZWRFdmVudCA9IGV2ZW50O1xuICAgICAgICBpZiAoZGVib3VuY2VUaW1lcikge1xuICAgICAgICAgICAgY2xlYXJUaW1lb3V0KGRlYm91bmNlVGltZXIpO1xuICAgICAgICB9XG4gICAgICAgIGlmIChkZWJvdW5jZSkge1xuICAgICAgICAgICAgZGVib3VuY2VUaW1lciA9IHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIGhhbmRsZXIoeyBldmVudDogZXZlbnQsIG5hbWU6IG5hbWUgfSk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIGhhbmRsZXIoeyBldmVudDogZXZlbnQsIG5hbWU6IG5hbWUgfSk7XG4gICAgICAgIH1cbiAgICB9O1xufVxuLyoqXG4gKiBXcmFwcyBhZGRFdmVudExpc3RlbmVyIHRvIGNhcHR1cmUga2V5cHJlc3MgVUkgZXZlbnRzXG4gKiBAcGFyYW0gaGFuZGxlciBmdW5jdGlvbiB0aGF0IHdpbGwgYmUgdHJpZ2dlcmVkXG4gKiBAcmV0dXJucyB3cmFwcGVkIGtleXByZXNzIGV2ZW50cyBoYW5kbGVyXG4gKiBAaGlkZGVuXG4gKi9cbmZ1bmN0aW9uIGtleXByZXNzRXZlbnRIYW5kbGVyKGhhbmRsZXIpIHtcbiAgICAvLyBUT0RPOiBpZiBzb21laG93IHVzZXIgc3dpdGNoZXMga2V5cHJlc3MgdGFyZ2V0IGJlZm9yZVxuICAgIC8vICAgICAgIGRlYm91bmNlIHRpbWVvdXQgaXMgdHJpZ2dlcmVkLCB3ZSB3aWxsIG9ubHkgY2FwdHVyZVxuICAgIC8vICAgICAgIGEgc2luZ2xlIGJyZWFkY3J1bWIgZnJvbSB0aGUgRklSU1QgdGFyZ2V0IChhY2NlcHRhYmxlPylcbiAgICByZXR1cm4gZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgICAgIHZhciB0YXJnZXQ7XG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICB0YXJnZXQgPSBldmVudC50YXJnZXQ7XG4gICAgICAgIH1cbiAgICAgICAgY2F0Y2ggKGUpIHtcbiAgICAgICAgICAgIC8vIGp1c3QgYWNjZXNzaW5nIGV2ZW50IHByb3BlcnRpZXMgY2FuIHRocm93IGFuIGV4Y2VwdGlvbiBpbiBzb21lIHJhcmUgY2lyY3Vtc3RhbmNlc1xuICAgICAgICAgICAgLy8gc2VlOiBodHRwczovL2dpdGh1Yi5jb20vZ2V0c2VudHJ5L3JhdmVuLWpzL2lzc3Vlcy84MzhcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuICAgICAgICB2YXIgdGFnTmFtZSA9IHRhcmdldCAmJiB0YXJnZXQudGFnTmFtZTtcbiAgICAgICAgLy8gb25seSBjb25zaWRlciBrZXlwcmVzcyBldmVudHMgb24gYWN0dWFsIGlucHV0IGVsZW1lbnRzXG4gICAgICAgIC8vIHRoaXMgd2lsbCBkaXNyZWdhcmQga2V5cHJlc3NlcyB0YXJnZXRpbmcgYm9keSAoZS5nLiB0YWJiaW5nXG4gICAgICAgIC8vIHRocm91Z2ggZWxlbWVudHMsIGhvdGtleXMsIGV0YylcbiAgICAgICAgaWYgKCF0YWdOYW1lIHx8ICh0YWdOYW1lICE9PSAnSU5QVVQnICYmIHRhZ05hbWUgIT09ICdURVhUQVJFQScgJiYgIXRhcmdldC5pc0NvbnRlbnRFZGl0YWJsZSkpIHtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuICAgICAgICAvLyByZWNvcmQgZmlyc3Qga2V5cHJlc3MgaW4gYSBzZXJpZXMsIGJ1dCBpZ25vcmUgc3Vic2VxdWVudFxuICAgICAgICAvLyBrZXlwcmVzc2VzIHVudGlsIGRlYm91bmNlIGNsZWFyc1xuICAgICAgICBpZiAoIWtleXByZXNzVGltZW91dCkge1xuICAgICAgICAgICAgZG9tRXZlbnRIYW5kbGVyKCdpbnB1dCcsIGhhbmRsZXIpKGV2ZW50KTtcbiAgICAgICAgfVxuICAgICAgICBjbGVhclRpbWVvdXQoa2V5cHJlc3NUaW1lb3V0KTtcbiAgICAgICAga2V5cHJlc3NUaW1lb3V0ID0gc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBrZXlwcmVzc1RpbWVvdXQgPSB1bmRlZmluZWQ7XG4gICAgICAgIH0sIGRlYm91bmNlRHVyYXRpb24pO1xuICAgIH07XG59XG52YXIgX29sZE9uRXJyb3JIYW5kbGVyID0gbnVsbDtcbi8qKiBKU0RvYyAqL1xuZnVuY3Rpb24gaW5zdHJ1bWVudEVycm9yKCkge1xuICAgIF9vbGRPbkVycm9ySGFuZGxlciA9IGdsb2JhbC5vbmVycm9yO1xuICAgIGdsb2JhbC5vbmVycm9yID0gZnVuY3Rpb24gKG1zZywgdXJsLCBsaW5lLCBjb2x1bW4sIGVycm9yKSB7XG4gICAgICAgIHRyaWdnZXJIYW5kbGVycygnZXJyb3InLCB7XG4gICAgICAgICAgICBjb2x1bW46IGNvbHVtbixcbiAgICAgICAgICAgIGVycm9yOiBlcnJvcixcbiAgICAgICAgICAgIGxpbmU6IGxpbmUsXG4gICAgICAgICAgICBtc2c6IG1zZyxcbiAgICAgICAgICAgIHVybDogdXJsLFxuICAgICAgICB9KTtcbiAgICAgICAgaWYgKF9vbGRPbkVycm9ySGFuZGxlcikge1xuICAgICAgICAgICAgcmV0dXJuIF9vbGRPbkVycm9ySGFuZGxlci5hcHBseSh0aGlzLCBhcmd1bWVudHMpO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9O1xufVxudmFyIF9vbGRPblVuaGFuZGxlZFJlamVjdGlvbkhhbmRsZXIgPSBudWxsO1xuLyoqIEpTRG9jICovXG5mdW5jdGlvbiBpbnN0cnVtZW50VW5oYW5kbGVkUmVqZWN0aW9uKCkge1xuICAgIF9vbGRPblVuaGFuZGxlZFJlamVjdGlvbkhhbmRsZXIgPSBnbG9iYWwub251bmhhbmRsZWRyZWplY3Rpb247XG4gICAgZ2xvYmFsLm9udW5oYW5kbGVkcmVqZWN0aW9uID0gZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgdHJpZ2dlckhhbmRsZXJzKCd1bmhhbmRsZWRyZWplY3Rpb24nLCBlKTtcbiAgICAgICAgaWYgKF9vbGRPblVuaGFuZGxlZFJlamVjdGlvbkhhbmRsZXIpIHtcbiAgICAgICAgICAgIHJldHVybiBfb2xkT25VbmhhbmRsZWRSZWplY3Rpb25IYW5kbGVyLmFwcGx5KHRoaXMsIGFyZ3VtZW50cyk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgfTtcbn1cbi8vIyBzb3VyY2VNYXBwaW5nVVJMPWluc3RydW1lbnQuanMubWFwIiwiLyoqXG4gKiBDaGVja3Mgd2hldGhlciBnaXZlbiB2YWx1ZSdzIHR5cGUgaXMgb25lIG9mIGEgZmV3IEVycm9yIG9yIEVycm9yLWxpa2VcbiAqIHtAbGluayBpc0Vycm9yfS5cbiAqXG4gKiBAcGFyYW0gd2F0IEEgdmFsdWUgdG8gYmUgY2hlY2tlZC5cbiAqIEByZXR1cm5zIEEgYm9vbGVhbiByZXByZXNlbnRpbmcgdGhlIHJlc3VsdC5cbiAqL1xuZXhwb3J0IGZ1bmN0aW9uIGlzRXJyb3Iod2F0KSB7XG4gICAgc3dpdGNoIChPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nLmNhbGwod2F0KSkge1xuICAgICAgICBjYXNlICdbb2JqZWN0IEVycm9yXSc6XG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgY2FzZSAnW29iamVjdCBFeGNlcHRpb25dJzpcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICBjYXNlICdbb2JqZWN0IERPTUV4Y2VwdGlvbl0nOlxuICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICByZXR1cm4gaXNJbnN0YW5jZU9mKHdhdCwgRXJyb3IpO1xuICAgIH1cbn1cbi8qKlxuICogQ2hlY2tzIHdoZXRoZXIgZ2l2ZW4gdmFsdWUncyB0eXBlIGlzIEVycm9yRXZlbnRcbiAqIHtAbGluayBpc0Vycm9yRXZlbnR9LlxuICpcbiAqIEBwYXJhbSB3YXQgQSB2YWx1ZSB0byBiZSBjaGVja2VkLlxuICogQHJldHVybnMgQSBib29sZWFuIHJlcHJlc2VudGluZyB0aGUgcmVzdWx0LlxuICovXG5leHBvcnQgZnVuY3Rpb24gaXNFcnJvckV2ZW50KHdhdCkge1xuICAgIHJldHVybiBPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nLmNhbGwod2F0KSA9PT0gJ1tvYmplY3QgRXJyb3JFdmVudF0nO1xufVxuLyoqXG4gKiBDaGVja3Mgd2hldGhlciBnaXZlbiB2YWx1ZSdzIHR5cGUgaXMgRE9NRXJyb3JcbiAqIHtAbGluayBpc0RPTUVycm9yfS5cbiAqXG4gKiBAcGFyYW0gd2F0IEEgdmFsdWUgdG8gYmUgY2hlY2tlZC5cbiAqIEByZXR1cm5zIEEgYm9vbGVhbiByZXByZXNlbnRpbmcgdGhlIHJlc3VsdC5cbiAqL1xuZXhwb3J0IGZ1bmN0aW9uIGlzRE9NRXJyb3Iod2F0KSB7XG4gICAgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUudG9TdHJpbmcuY2FsbCh3YXQpID09PSAnW29iamVjdCBET01FcnJvcl0nO1xufVxuLyoqXG4gKiBDaGVja3Mgd2hldGhlciBnaXZlbiB2YWx1ZSdzIHR5cGUgaXMgRE9NRXhjZXB0aW9uXG4gKiB7QGxpbmsgaXNET01FeGNlcHRpb259LlxuICpcbiAqIEBwYXJhbSB3YXQgQSB2YWx1ZSB0byBiZSBjaGVja2VkLlxuICogQHJldHVybnMgQSBib29sZWFuIHJlcHJlc2VudGluZyB0aGUgcmVzdWx0LlxuICovXG5leHBvcnQgZnVuY3Rpb24gaXNET01FeGNlcHRpb24od2F0KSB7XG4gICAgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUudG9TdHJpbmcuY2FsbCh3YXQpID09PSAnW29iamVjdCBET01FeGNlcHRpb25dJztcbn1cbi8qKlxuICogQ2hlY2tzIHdoZXRoZXIgZ2l2ZW4gdmFsdWUncyB0eXBlIGlzIGEgc3RyaW5nXG4gKiB7QGxpbmsgaXNTdHJpbmd9LlxuICpcbiAqIEBwYXJhbSB3YXQgQSB2YWx1ZSB0byBiZSBjaGVja2VkLlxuICogQHJldHVybnMgQSBib29sZWFuIHJlcHJlc2VudGluZyB0aGUgcmVzdWx0LlxuICovXG5leHBvcnQgZnVuY3Rpb24gaXNTdHJpbmcod2F0KSB7XG4gICAgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUudG9TdHJpbmcuY2FsbCh3YXQpID09PSAnW29iamVjdCBTdHJpbmddJztcbn1cbi8qKlxuICogQ2hlY2tzIHdoZXRoZXIgZ2l2ZW4gdmFsdWUncyBpcyBhIHByaW1pdGl2ZSAodW5kZWZpbmVkLCBudWxsLCBudW1iZXIsIGJvb2xlYW4sIHN0cmluZylcbiAqIHtAbGluayBpc1ByaW1pdGl2ZX0uXG4gKlxuICogQHBhcmFtIHdhdCBBIHZhbHVlIHRvIGJlIGNoZWNrZWQuXG4gKiBAcmV0dXJucyBBIGJvb2xlYW4gcmVwcmVzZW50aW5nIHRoZSByZXN1bHQuXG4gKi9cbmV4cG9ydCBmdW5jdGlvbiBpc1ByaW1pdGl2ZSh3YXQpIHtcbiAgICByZXR1cm4gd2F0ID09PSBudWxsIHx8ICh0eXBlb2Ygd2F0ICE9PSAnb2JqZWN0JyAmJiB0eXBlb2Ygd2F0ICE9PSAnZnVuY3Rpb24nKTtcbn1cbi8qKlxuICogQ2hlY2tzIHdoZXRoZXIgZ2l2ZW4gdmFsdWUncyB0eXBlIGlzIGFuIG9iamVjdCBsaXRlcmFsXG4gKiB7QGxpbmsgaXNQbGFpbk9iamVjdH0uXG4gKlxuICogQHBhcmFtIHdhdCBBIHZhbHVlIHRvIGJlIGNoZWNrZWQuXG4gKiBAcmV0dXJucyBBIGJvb2xlYW4gcmVwcmVzZW50aW5nIHRoZSByZXN1bHQuXG4gKi9cbmV4cG9ydCBmdW5jdGlvbiBpc1BsYWluT2JqZWN0KHdhdCkge1xuICAgIHJldHVybiBPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nLmNhbGwod2F0KSA9PT0gJ1tvYmplY3QgT2JqZWN0XSc7XG59XG4vKipcbiAqIENoZWNrcyB3aGV0aGVyIGdpdmVuIHZhbHVlJ3MgdHlwZSBpcyBhbiBFdmVudCBpbnN0YW5jZVxuICoge0BsaW5rIGlzRXZlbnR9LlxuICpcbiAqIEBwYXJhbSB3YXQgQSB2YWx1ZSB0byBiZSBjaGVja2VkLlxuICogQHJldHVybnMgQSBib29sZWFuIHJlcHJlc2VudGluZyB0aGUgcmVzdWx0LlxuICovXG5leHBvcnQgZnVuY3Rpb24gaXNFdmVudCh3YXQpIHtcbiAgICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6c3RyaWN0LXR5cGUtcHJlZGljYXRlc1xuICAgIHJldHVybiB0eXBlb2YgRXZlbnQgIT09ICd1bmRlZmluZWQnICYmIGlzSW5zdGFuY2VPZih3YXQsIEV2ZW50KTtcbn1cbi8qKlxuICogQ2hlY2tzIHdoZXRoZXIgZ2l2ZW4gdmFsdWUncyB0eXBlIGlzIGFuIEVsZW1lbnQgaW5zdGFuY2VcbiAqIHtAbGluayBpc0VsZW1lbnR9LlxuICpcbiAqIEBwYXJhbSB3YXQgQSB2YWx1ZSB0byBiZSBjaGVja2VkLlxuICogQHJldHVybnMgQSBib29sZWFuIHJlcHJlc2VudGluZyB0aGUgcmVzdWx0LlxuICovXG5leHBvcnQgZnVuY3Rpb24gaXNFbGVtZW50KHdhdCkge1xuICAgIC8vIHRzbGludDpkaXNhYmxlLW5leHQtbGluZTpzdHJpY3QtdHlwZS1wcmVkaWNhdGVzXG4gICAgcmV0dXJuIHR5cGVvZiBFbGVtZW50ICE9PSAndW5kZWZpbmVkJyAmJiBpc0luc3RhbmNlT2Yod2F0LCBFbGVtZW50KTtcbn1cbi8qKlxuICogQ2hlY2tzIHdoZXRoZXIgZ2l2ZW4gdmFsdWUncyB0eXBlIGlzIGFuIHJlZ2V4cFxuICoge0BsaW5rIGlzUmVnRXhwfS5cbiAqXG4gKiBAcGFyYW0gd2F0IEEgdmFsdWUgdG8gYmUgY2hlY2tlZC5cbiAqIEByZXR1cm5zIEEgYm9vbGVhbiByZXByZXNlbnRpbmcgdGhlIHJlc3VsdC5cbiAqL1xuZXhwb3J0IGZ1bmN0aW9uIGlzUmVnRXhwKHdhdCkge1xuICAgIHJldHVybiBPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nLmNhbGwod2F0KSA9PT0gJ1tvYmplY3QgUmVnRXhwXSc7XG59XG4vKipcbiAqIENoZWNrcyB3aGV0aGVyIGdpdmVuIHZhbHVlIGhhcyBhIHRoZW4gZnVuY3Rpb24uXG4gKiBAcGFyYW0gd2F0IEEgdmFsdWUgdG8gYmUgY2hlY2tlZC5cbiAqL1xuZXhwb3J0IGZ1bmN0aW9uIGlzVGhlbmFibGUod2F0KSB7XG4gICAgLy8gdHNsaW50OmRpc2FibGU6bm8tdW5zYWZlLWFueVxuICAgIHJldHVybiBCb29sZWFuKHdhdCAmJiB3YXQudGhlbiAmJiB0eXBlb2Ygd2F0LnRoZW4gPT09ICdmdW5jdGlvbicpO1xuICAgIC8vIHRzbGludDplbmFibGU6bm8tdW5zYWZlLWFueVxufVxuLyoqXG4gKiBDaGVja3Mgd2hldGhlciBnaXZlbiB2YWx1ZSdzIHR5cGUgaXMgYSBTeW50aGV0aWNFdmVudFxuICoge0BsaW5rIGlzU3ludGhldGljRXZlbnR9LlxuICpcbiAqIEBwYXJhbSB3YXQgQSB2YWx1ZSB0byBiZSBjaGVja2VkLlxuICogQHJldHVybnMgQSBib29sZWFuIHJlcHJlc2VudGluZyB0aGUgcmVzdWx0LlxuICovXG5leHBvcnQgZnVuY3Rpb24gaXNTeW50aGV0aWNFdmVudCh3YXQpIHtcbiAgICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6bm8tdW5zYWZlLWFueVxuICAgIHJldHVybiBpc1BsYWluT2JqZWN0KHdhdCkgJiYgJ25hdGl2ZUV2ZW50JyBpbiB3YXQgJiYgJ3ByZXZlbnREZWZhdWx0JyBpbiB3YXQgJiYgJ3N0b3BQcm9wYWdhdGlvbicgaW4gd2F0O1xufVxuLyoqXG4gKiBDaGVja3Mgd2hldGhlciBnaXZlbiB2YWx1ZSdzIHR5cGUgaXMgYW4gaW5zdGFuY2Ugb2YgcHJvdmlkZWQgY29uc3RydWN0b3IuXG4gKiB7QGxpbmsgaXNJbnN0YW5jZU9mfS5cbiAqXG4gKiBAcGFyYW0gd2F0IEEgdmFsdWUgdG8gYmUgY2hlY2tlZC5cbiAqIEBwYXJhbSBiYXNlIEEgY29uc3RydWN0b3IgdG8gYmUgdXNlZCBpbiBhIGNoZWNrLlxuICogQHJldHVybnMgQSBib29sZWFuIHJlcHJlc2VudGluZyB0aGUgcmVzdWx0LlxuICovXG5leHBvcnQgZnVuY3Rpb24gaXNJbnN0YW5jZU9mKHdhdCwgYmFzZSkge1xuICAgIHRyeSB7XG4gICAgICAgIC8vIHRzbGludDpkaXNhYmxlLW5leHQtbGluZTpuby11bnNhZmUtYW55XG4gICAgICAgIHJldHVybiB3YXQgaW5zdGFuY2VvZiBiYXNlO1xuICAgIH1cbiAgICBjYXRjaCAoX2UpIHtcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cbn1cbi8vIyBzb3VyY2VNYXBwaW5nVVJMPWlzLmpzLm1hcCIsImltcG9ydCB7IGNvbnNvbGVTYW5kYm94LCBnZXRHbG9iYWxPYmplY3QgfSBmcm9tICcuL21pc2MnO1xuLy8gVE9ETzogSW1wbGVtZW50IGRpZmZlcmVudCBsb2dnZXJzIGZvciBkaWZmZXJlbnQgZW52aXJvbm1lbnRzXG52YXIgZ2xvYmFsID0gZ2V0R2xvYmFsT2JqZWN0KCk7XG4vKiogUHJlZml4IGZvciBsb2dnaW5nIHN0cmluZ3MgKi9cbnZhciBQUkVGSVggPSAnU2VudHJ5IExvZ2dlciAnO1xuLyoqIEpTRG9jICovXG52YXIgTG9nZ2VyID0gLyoqIEBjbGFzcyAqLyAoZnVuY3Rpb24gKCkge1xuICAgIC8qKiBKU0RvYyAqL1xuICAgIGZ1bmN0aW9uIExvZ2dlcigpIHtcbiAgICAgICAgdGhpcy5fZW5hYmxlZCA9IGZhbHNlO1xuICAgIH1cbiAgICAvKiogSlNEb2MgKi9cbiAgICBMb2dnZXIucHJvdG90eXBlLmRpc2FibGUgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHRoaXMuX2VuYWJsZWQgPSBmYWxzZTtcbiAgICB9O1xuICAgIC8qKiBKU0RvYyAqL1xuICAgIExvZ2dlci5wcm90b3R5cGUuZW5hYmxlID0gZnVuY3Rpb24gKCkge1xuICAgICAgICB0aGlzLl9lbmFibGVkID0gdHJ1ZTtcbiAgICB9O1xuICAgIC8qKiBKU0RvYyAqL1xuICAgIExvZ2dlci5wcm90b3R5cGUubG9nID0gZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXIgYXJncyA9IFtdO1xuICAgICAgICBmb3IgKHZhciBfaSA9IDA7IF9pIDwgYXJndW1lbnRzLmxlbmd0aDsgX2krKykge1xuICAgICAgICAgICAgYXJnc1tfaV0gPSBhcmd1bWVudHNbX2ldO1xuICAgICAgICB9XG4gICAgICAgIGlmICghdGhpcy5fZW5hYmxlZCkge1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIGNvbnNvbGVTYW5kYm94KGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIGdsb2JhbC5jb25zb2xlLmxvZyhQUkVGSVggKyBcIltMb2ddOiBcIiArIGFyZ3Muam9pbignICcpKTsgLy8gdHNsaW50OmRpc2FibGUtbGluZTpuby1jb25zb2xlXG4gICAgICAgIH0pO1xuICAgIH07XG4gICAgLyoqIEpTRG9jICovXG4gICAgTG9nZ2VyLnByb3RvdHlwZS53YXJuID0gZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXIgYXJncyA9IFtdO1xuICAgICAgICBmb3IgKHZhciBfaSA9IDA7IF9pIDwgYXJndW1lbnRzLmxlbmd0aDsgX2krKykge1xuICAgICAgICAgICAgYXJnc1tfaV0gPSBhcmd1bWVudHNbX2ldO1xuICAgICAgICB9XG4gICAgICAgIGlmICghdGhpcy5fZW5hYmxlZCkge1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIGNvbnNvbGVTYW5kYm94KGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIGdsb2JhbC5jb25zb2xlLndhcm4oUFJFRklYICsgXCJbV2Fybl06IFwiICsgYXJncy5qb2luKCcgJykpOyAvLyB0c2xpbnQ6ZGlzYWJsZS1saW5lOm5vLWNvbnNvbGVcbiAgICAgICAgfSk7XG4gICAgfTtcbiAgICAvKiogSlNEb2MgKi9cbiAgICBMb2dnZXIucHJvdG90eXBlLmVycm9yID0gZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXIgYXJncyA9IFtdO1xuICAgICAgICBmb3IgKHZhciBfaSA9IDA7IF9pIDwgYXJndW1lbnRzLmxlbmd0aDsgX2krKykge1xuICAgICAgICAgICAgYXJnc1tfaV0gPSBhcmd1bWVudHNbX2ldO1xuICAgICAgICB9XG4gICAgICAgIGlmICghdGhpcy5fZW5hYmxlZCkge1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIGNvbnNvbGVTYW5kYm94KGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIGdsb2JhbC5jb25zb2xlLmVycm9yKFBSRUZJWCArIFwiW0Vycm9yXTogXCIgKyBhcmdzLmpvaW4oJyAnKSk7IC8vIHRzbGludDpkaXNhYmxlLWxpbmU6bm8tY29uc29sZVxuICAgICAgICB9KTtcbiAgICB9O1xuICAgIHJldHVybiBMb2dnZXI7XG59KCkpO1xuLy8gRW5zdXJlIHdlIG9ubHkgaGF2ZSBhIHNpbmdsZSBsb2dnZXIgaW5zdGFuY2UsIGV2ZW4gaWYgbXVsdGlwbGUgdmVyc2lvbnMgb2YgQHNlbnRyeS91dGlscyBhcmUgYmVpbmcgdXNlZFxuZ2xvYmFsLl9fU0VOVFJZX18gPSBnbG9iYWwuX19TRU5UUllfXyB8fCB7fTtcbnZhciBsb2dnZXIgPSBnbG9iYWwuX19TRU5UUllfXy5sb2dnZXIgfHwgKGdsb2JhbC5fX1NFTlRSWV9fLmxvZ2dlciA9IG5ldyBMb2dnZXIoKSk7XG5leHBvcnQgeyBsb2dnZXIgfTtcbi8vIyBzb3VyY2VNYXBwaW5nVVJMPWxvZ2dlci5qcy5tYXAiLCIvLyB0c2xpbnQ6ZGlzYWJsZTpuby11bnNhZmUtYW55XG4vKipcbiAqIE1lbW8gY2xhc3MgdXNlZCBmb3IgZGVjeWNsZSBqc29uIG9iamVjdHMuIFVzZXMgV2Vha1NldCBpZiBhdmFpbGFibGUgb3RoZXJ3aXNlIGFycmF5LlxuICovXG52YXIgTWVtbyA9IC8qKiBAY2xhc3MgKi8gKGZ1bmN0aW9uICgpIHtcbiAgICBmdW5jdGlvbiBNZW1vKCkge1xuICAgICAgICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmVcbiAgICAgICAgdGhpcy5faGFzV2Vha1NldCA9IHR5cGVvZiBXZWFrU2V0ID09PSAnZnVuY3Rpb24nO1xuICAgICAgICB0aGlzLl9pbm5lciA9IHRoaXMuX2hhc1dlYWtTZXQgPyBuZXcgV2Vha1NldCgpIDogW107XG4gICAgfVxuICAgIC8qKlxuICAgICAqIFNldHMgb2JqIHRvIHJlbWVtYmVyLlxuICAgICAqIEBwYXJhbSBvYmogT2JqZWN0IHRvIHJlbWVtYmVyXG4gICAgICovXG4gICAgTWVtby5wcm90b3R5cGUubWVtb2l6ZSA9IGZ1bmN0aW9uIChvYmopIHtcbiAgICAgICAgaWYgKHRoaXMuX2hhc1dlYWtTZXQpIHtcbiAgICAgICAgICAgIGlmICh0aGlzLl9pbm5lci5oYXMob2JqKSkge1xuICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdGhpcy5faW5uZXIuYWRkKG9iaik7XG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgIH1cbiAgICAgICAgLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOnByZWZlci1mb3Itb2ZcbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCB0aGlzLl9pbm5lci5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgdmFyIHZhbHVlID0gdGhpcy5faW5uZXJbaV07XG4gICAgICAgICAgICBpZiAodmFsdWUgPT09IG9iaikge1xuICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHRoaXMuX2lubmVyLnB1c2gob2JqKTtcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH07XG4gICAgLyoqXG4gICAgICogUmVtb3ZlcyBvYmplY3QgZnJvbSBpbnRlcm5hbCBzdG9yYWdlLlxuICAgICAqIEBwYXJhbSBvYmogT2JqZWN0IHRvIGZvcmdldFxuICAgICAqL1xuICAgIE1lbW8ucHJvdG90eXBlLnVubWVtb2l6ZSA9IGZ1bmN0aW9uIChvYmopIHtcbiAgICAgICAgaWYgKHRoaXMuX2hhc1dlYWtTZXQpIHtcbiAgICAgICAgICAgIHRoaXMuX2lubmVyLmRlbGV0ZShvYmopO1xuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCB0aGlzLl9pbm5lci5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgICAgIGlmICh0aGlzLl9pbm5lcltpXSA9PT0gb2JqKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX2lubmVyLnNwbGljZShpLCAxKTtcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfTtcbiAgICByZXR1cm4gTWVtbztcbn0oKSk7XG5leHBvcnQgeyBNZW1vIH07XG4vLyMgc291cmNlTWFwcGluZ1VSTD1tZW1vLmpzLm1hcCIsImltcG9ydCB7IGlzU3RyaW5nIH0gZnJvbSAnLi9pcyc7XG5pbXBvcnQgeyBzbmlwTGluZSB9IGZyb20gJy4vc3RyaW5nJztcbi8qKlxuICogUmVxdWlyZXMgYSBtb2R1bGUgd2hpY2ggaXMgcHJvdGVjdGVkIGFnYWluc3QgYnVuZGxlciBtaW5pZmljYXRpb24uXG4gKlxuICogQHBhcmFtIHJlcXVlc3QgVGhlIG1vZHVsZSBwYXRoIHRvIHJlc29sdmVcbiAqL1xuZXhwb3J0IGZ1bmN0aW9uIGR5bmFtaWNSZXF1aXJlKG1vZCwgcmVxdWVzdCkge1xuICAgIC8vIHRzbGludDpkaXNhYmxlLW5leHQtbGluZTogbm8tdW5zYWZlLWFueVxuICAgIHJldHVybiBtb2QucmVxdWlyZShyZXF1ZXN0KTtcbn1cbi8qKlxuICogQ2hlY2tzIHdoZXRoZXIgd2UncmUgaW4gdGhlIE5vZGUuanMgb3IgQnJvd3NlciBlbnZpcm9ubWVudFxuICpcbiAqIEByZXR1cm5zIEFuc3dlciB0byBnaXZlbiBxdWVzdGlvblxuICovXG5leHBvcnQgZnVuY3Rpb24gaXNOb2RlRW52KCkge1xuICAgIC8vIHRzbGludDpkaXNhYmxlOnN0cmljdC10eXBlLXByZWRpY2F0ZXNcbiAgICByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS50b1N0cmluZy5jYWxsKHR5cGVvZiBwcm9jZXNzICE9PSAndW5kZWZpbmVkJyA/IHByb2Nlc3MgOiAwKSA9PT0gJ1tvYmplY3QgcHJvY2Vzc10nO1xufVxudmFyIGZhbGxiYWNrR2xvYmFsT2JqZWN0ID0ge307XG4vKipcbiAqIFNhZmVseSBnZXQgZ2xvYmFsIHNjb3BlIG9iamVjdFxuICpcbiAqIEByZXR1cm5zIEdsb2JhbCBzY29wZSBvYmplY3RcbiAqL1xuZXhwb3J0IGZ1bmN0aW9uIGdldEdsb2JhbE9iamVjdCgpIHtcbiAgICByZXR1cm4gKGlzTm9kZUVudigpXG4gICAgICAgID8gZ2xvYmFsXG4gICAgICAgIDogdHlwZW9mIHdpbmRvdyAhPT0gJ3VuZGVmaW5lZCdcbiAgICAgICAgICAgID8gd2luZG93XG4gICAgICAgICAgICA6IHR5cGVvZiBzZWxmICE9PSAndW5kZWZpbmVkJ1xuICAgICAgICAgICAgICAgID8gc2VsZlxuICAgICAgICAgICAgICAgIDogZmFsbGJhY2tHbG9iYWxPYmplY3QpO1xufVxuLyoqXG4gKiBVVUlENCBnZW5lcmF0b3JcbiAqXG4gKiBAcmV0dXJucyBzdHJpbmcgR2VuZXJhdGVkIFVVSUQ0LlxuICovXG5leHBvcnQgZnVuY3Rpb24gdXVpZDQoKSB7XG4gICAgdmFyIGdsb2JhbCA9IGdldEdsb2JhbE9iamVjdCgpO1xuICAgIHZhciBjcnlwdG8gPSBnbG9iYWwuY3J5cHRvIHx8IGdsb2JhbC5tc0NyeXB0bztcbiAgICBpZiAoIShjcnlwdG8gPT09IHZvaWQgMCkgJiYgY3J5cHRvLmdldFJhbmRvbVZhbHVlcykge1xuICAgICAgICAvLyBVc2Ugd2luZG93LmNyeXB0byBBUEkgaWYgYXZhaWxhYmxlXG4gICAgICAgIHZhciBhcnIgPSBuZXcgVWludDE2QXJyYXkoOCk7XG4gICAgICAgIGNyeXB0by5nZXRSYW5kb21WYWx1ZXMoYXJyKTtcbiAgICAgICAgLy8gc2V0IDQgaW4gYnl0ZSA3XG4gICAgICAgIC8vIHRzbGludDpkaXNhYmxlLW5leHQtbGluZTpuby1iaXR3aXNlXG4gICAgICAgIGFyclszXSA9IChhcnJbM10gJiAweGZmZikgfCAweDQwMDA7XG4gICAgICAgIC8vIHNldCAyIG1vc3Qgc2lnbmlmaWNhbnQgYml0cyBvZiBieXRlIDkgdG8gJzEwJ1xuICAgICAgICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6bm8tYml0d2lzZVxuICAgICAgICBhcnJbNF0gPSAoYXJyWzRdICYgMHgzZmZmKSB8IDB4ODAwMDtcbiAgICAgICAgdmFyIHBhZCA9IGZ1bmN0aW9uIChudW0pIHtcbiAgICAgICAgICAgIHZhciB2ID0gbnVtLnRvU3RyaW5nKDE2KTtcbiAgICAgICAgICAgIHdoaWxlICh2Lmxlbmd0aCA8IDQpIHtcbiAgICAgICAgICAgICAgICB2ID0gXCIwXCIgKyB2O1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIHY7XG4gICAgICAgIH07XG4gICAgICAgIHJldHVybiAocGFkKGFyclswXSkgKyBwYWQoYXJyWzFdKSArIHBhZChhcnJbMl0pICsgcGFkKGFyclszXSkgKyBwYWQoYXJyWzRdKSArIHBhZChhcnJbNV0pICsgcGFkKGFycls2XSkgKyBwYWQoYXJyWzddKSk7XG4gICAgfVxuICAgIC8vIGh0dHA6Ly9zdGFja292ZXJmbG93LmNvbS9xdWVzdGlvbnMvMTA1MDM0L2hvdy10by1jcmVhdGUtYS1ndWlkLXV1aWQtaW4tamF2YXNjcmlwdC8yMTE3NTIzIzIxMTc1MjNcbiAgICByZXR1cm4gJ3h4eHh4eHh4eHh4eDR4eHh5eHh4eHh4eHh4eHh4eHh4Jy5yZXBsYWNlKC9beHldL2csIGZ1bmN0aW9uIChjKSB7XG4gICAgICAgIC8vIHRzbGludDpkaXNhYmxlLW5leHQtbGluZTpuby1iaXR3aXNlXG4gICAgICAgIHZhciByID0gKE1hdGgucmFuZG9tKCkgKiAxNikgfCAwO1xuICAgICAgICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6bm8tYml0d2lzZVxuICAgICAgICB2YXIgdiA9IGMgPT09ICd4JyA/IHIgOiAociAmIDB4MykgfCAweDg7XG4gICAgICAgIHJldHVybiB2LnRvU3RyaW5nKDE2KTtcbiAgICB9KTtcbn1cbi8qKlxuICogUGFyc2VzIHN0cmluZyBmb3JtIG9mIFVSTCBpbnRvIGFuIG9iamVjdFxuICogLy8gYm9ycm93ZWQgZnJvbSBodHRwczovL3Rvb2xzLmlldGYub3JnL2h0bWwvcmZjMzk4NiNhcHBlbmRpeC1CXG4gKiAvLyBpbnRlbnRpb25hbGx5IHVzaW5nIHJlZ2V4IGFuZCBub3QgPGEvPiBocmVmIHBhcnNpbmcgdHJpY2sgYmVjYXVzZSBSZWFjdCBOYXRpdmUgYW5kIG90aGVyXG4gKiAvLyBlbnZpcm9ubWVudHMgd2hlcmUgRE9NIG1pZ2h0IG5vdCBiZSBhdmFpbGFibGVcbiAqIEByZXR1cm5zIHBhcnNlZCBVUkwgb2JqZWN0XG4gKi9cbmV4cG9ydCBmdW5jdGlvbiBwYXJzZVVybCh1cmwpIHtcbiAgICBpZiAoIXVybCkge1xuICAgICAgICByZXR1cm4ge307XG4gICAgfVxuICAgIHZhciBtYXRjaCA9IHVybC5tYXRjaCgvXigoW146XFwvPyNdKyk6KT8oXFwvXFwvKFteXFwvPyNdKikpPyhbXj8jXSopKFxcPyhbXiNdKikpPygjKC4qKSk/JC8pO1xuICAgIGlmICghbWF0Y2gpIHtcbiAgICAgICAgcmV0dXJuIHt9O1xuICAgIH1cbiAgICAvLyBjb2VyY2UgdG8gdW5kZWZpbmVkIHZhbHVlcyB0byBlbXB0eSBzdHJpbmcgc28gd2UgZG9uJ3QgZ2V0ICd1bmRlZmluZWQnXG4gICAgdmFyIHF1ZXJ5ID0gbWF0Y2hbNl0gfHwgJyc7XG4gICAgdmFyIGZyYWdtZW50ID0gbWF0Y2hbOF0gfHwgJyc7XG4gICAgcmV0dXJuIHtcbiAgICAgICAgaG9zdDogbWF0Y2hbNF0sXG4gICAgICAgIHBhdGg6IG1hdGNoWzVdLFxuICAgICAgICBwcm90b2NvbDogbWF0Y2hbMl0sXG4gICAgICAgIHJlbGF0aXZlOiBtYXRjaFs1XSArIHF1ZXJ5ICsgZnJhZ21lbnQsXG4gICAgfTtcbn1cbi8qKlxuICogRXh0cmFjdHMgZWl0aGVyIG1lc3NhZ2Ugb3IgdHlwZSt2YWx1ZSBmcm9tIGFuIGV2ZW50IHRoYXQgY2FuIGJlIHVzZWQgZm9yIHVzZXItZmFjaW5nIGxvZ3NcbiAqIEByZXR1cm5zIGV2ZW50J3MgZGVzY3JpcHRpb25cbiAqL1xuZXhwb3J0IGZ1bmN0aW9uIGdldEV2ZW50RGVzY3JpcHRpb24oZXZlbnQpIHtcbiAgICBpZiAoZXZlbnQubWVzc2FnZSkge1xuICAgICAgICByZXR1cm4gZXZlbnQubWVzc2FnZTtcbiAgICB9XG4gICAgaWYgKGV2ZW50LmV4Y2VwdGlvbiAmJiBldmVudC5leGNlcHRpb24udmFsdWVzICYmIGV2ZW50LmV4Y2VwdGlvbi52YWx1ZXNbMF0pIHtcbiAgICAgICAgdmFyIGV4Y2VwdGlvbiA9IGV2ZW50LmV4Y2VwdGlvbi52YWx1ZXNbMF07XG4gICAgICAgIGlmIChleGNlcHRpb24udHlwZSAmJiBleGNlcHRpb24udmFsdWUpIHtcbiAgICAgICAgICAgIHJldHVybiBleGNlcHRpb24udHlwZSArIFwiOiBcIiArIGV4Y2VwdGlvbi52YWx1ZTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gZXhjZXB0aW9uLnR5cGUgfHwgZXhjZXB0aW9uLnZhbHVlIHx8IGV2ZW50LmV2ZW50X2lkIHx8ICc8dW5rbm93bj4nO1xuICAgIH1cbiAgICByZXR1cm4gZXZlbnQuZXZlbnRfaWQgfHwgJzx1bmtub3duPic7XG59XG4vKiogSlNEb2MgKi9cbmV4cG9ydCBmdW5jdGlvbiBjb25zb2xlU2FuZGJveChjYWxsYmFjaykge1xuICAgIHZhciBnbG9iYWwgPSBnZXRHbG9iYWxPYmplY3QoKTtcbiAgICB2YXIgbGV2ZWxzID0gWydkZWJ1ZycsICdpbmZvJywgJ3dhcm4nLCAnZXJyb3InLCAnbG9nJywgJ2Fzc2VydCddO1xuICAgIGlmICghKCdjb25zb2xlJyBpbiBnbG9iYWwpKSB7XG4gICAgICAgIHJldHVybiBjYWxsYmFjaygpO1xuICAgIH1cbiAgICB2YXIgb3JpZ2luYWxDb25zb2xlID0gZ2xvYmFsLmNvbnNvbGU7XG4gICAgdmFyIHdyYXBwZWRMZXZlbHMgPSB7fTtcbiAgICAvLyBSZXN0b3JlIGFsbCB3cmFwcGVkIGNvbnNvbGUgbWV0aG9kc1xuICAgIGxldmVscy5mb3JFYWNoKGZ1bmN0aW9uIChsZXZlbCkge1xuICAgICAgICBpZiAobGV2ZWwgaW4gZ2xvYmFsLmNvbnNvbGUgJiYgb3JpZ2luYWxDb25zb2xlW2xldmVsXS5fX3NlbnRyeV9vcmlnaW5hbF9fKSB7XG4gICAgICAgICAgICB3cmFwcGVkTGV2ZWxzW2xldmVsXSA9IG9yaWdpbmFsQ29uc29sZVtsZXZlbF07XG4gICAgICAgICAgICBvcmlnaW5hbENvbnNvbGVbbGV2ZWxdID0gb3JpZ2luYWxDb25zb2xlW2xldmVsXS5fX3NlbnRyeV9vcmlnaW5hbF9fO1xuICAgICAgICB9XG4gICAgfSk7XG4gICAgLy8gUGVyZm9ybSBjYWxsYmFjayBtYW5pcHVsYXRpb25zXG4gICAgdmFyIHJlc3VsdCA9IGNhbGxiYWNrKCk7XG4gICAgLy8gUmV2ZXJ0IHJlc3RvcmF0aW9uIHRvIHdyYXBwZWQgc3RhdGVcbiAgICBPYmplY3Qua2V5cyh3cmFwcGVkTGV2ZWxzKS5mb3JFYWNoKGZ1bmN0aW9uIChsZXZlbCkge1xuICAgICAgICBvcmlnaW5hbENvbnNvbGVbbGV2ZWxdID0gd3JhcHBlZExldmVsc1tsZXZlbF07XG4gICAgfSk7XG4gICAgcmV0dXJuIHJlc3VsdDtcbn1cbi8qKlxuICogQWRkcyBleGNlcHRpb24gdmFsdWVzLCB0eXBlIGFuZCB2YWx1ZSB0byBhbiBzeW50aGV0aWMgRXhjZXB0aW9uLlxuICogQHBhcmFtIGV2ZW50IFRoZSBldmVudCB0byBtb2RpZnkuXG4gKiBAcGFyYW0gdmFsdWUgVmFsdWUgb2YgdGhlIGV4Y2VwdGlvbi5cbiAqIEBwYXJhbSB0eXBlIFR5cGUgb2YgdGhlIGV4Y2VwdGlvbi5cbiAqIEBoaWRkZW5cbiAqL1xuZXhwb3J0IGZ1bmN0aW9uIGFkZEV4Y2VwdGlvblR5cGVWYWx1ZShldmVudCwgdmFsdWUsIHR5cGUpIHtcbiAgICBldmVudC5leGNlcHRpb24gPSBldmVudC5leGNlcHRpb24gfHwge307XG4gICAgZXZlbnQuZXhjZXB0aW9uLnZhbHVlcyA9IGV2ZW50LmV4Y2VwdGlvbi52YWx1ZXMgfHwgW107XG4gICAgZXZlbnQuZXhjZXB0aW9uLnZhbHVlc1swXSA9IGV2ZW50LmV4Y2VwdGlvbi52YWx1ZXNbMF0gfHwge307XG4gICAgZXZlbnQuZXhjZXB0aW9uLnZhbHVlc1swXS52YWx1ZSA9IGV2ZW50LmV4Y2VwdGlvbi52YWx1ZXNbMF0udmFsdWUgfHwgdmFsdWUgfHwgJyc7XG4gICAgZXZlbnQuZXhjZXB0aW9uLnZhbHVlc1swXS50eXBlID0gZXZlbnQuZXhjZXB0aW9uLnZhbHVlc1swXS50eXBlIHx8IHR5cGUgfHwgJ0Vycm9yJztcbn1cbi8qKlxuICogQWRkcyBleGNlcHRpb24gbWVjaGFuaXNtIHRvIGEgZ2l2ZW4gZXZlbnQuXG4gKiBAcGFyYW0gZXZlbnQgVGhlIGV2ZW50IHRvIG1vZGlmeS5cbiAqIEBwYXJhbSBtZWNoYW5pc20gTWVjaGFuaXNtIG9mIHRoZSBtZWNoYW5pc20uXG4gKiBAaGlkZGVuXG4gKi9cbmV4cG9ydCBmdW5jdGlvbiBhZGRFeGNlcHRpb25NZWNoYW5pc20oZXZlbnQsIG1lY2hhbmlzbSkge1xuICAgIGlmIChtZWNoYW5pc20gPT09IHZvaWQgMCkgeyBtZWNoYW5pc20gPSB7fTsgfVxuICAgIC8vIFRPRE86IFVzZSByZWFsIHR5cGUgd2l0aCBga2V5b2YgTWVjaGFuaXNtYCB0aGluZ3kgYW5kIG1heWJlIG1ha2UgaXQgYmV0dGVyP1xuICAgIHRyeSB7XG4gICAgICAgIC8vIEB0cy1pZ25vcmVcbiAgICAgICAgLy8gdHNsaW50OmRpc2FibGU6bm8tbm9uLW51bGwtYXNzZXJ0aW9uXG4gICAgICAgIGV2ZW50LmV4Y2VwdGlvbi52YWx1ZXNbMF0ubWVjaGFuaXNtID0gZXZlbnQuZXhjZXB0aW9uLnZhbHVlc1swXS5tZWNoYW5pc20gfHwge307XG4gICAgICAgIE9iamVjdC5rZXlzKG1lY2hhbmlzbSkuZm9yRWFjaChmdW5jdGlvbiAoa2V5KSB7XG4gICAgICAgICAgICAvLyBAdHMtaWdub3JlXG4gICAgICAgICAgICBldmVudC5leGNlcHRpb24udmFsdWVzWzBdLm1lY2hhbmlzbVtrZXldID0gbWVjaGFuaXNtW2tleV07XG4gICAgICAgIH0pO1xuICAgIH1cbiAgICBjYXRjaCAoX29PKSB7XG4gICAgICAgIC8vIG5vLWVtcHR5XG4gICAgfVxufVxuLyoqXG4gKiBBIHNhZmUgZm9ybSBvZiBsb2NhdGlvbi5ocmVmXG4gKi9cbmV4cG9ydCBmdW5jdGlvbiBnZXRMb2NhdGlvbkhyZWYoKSB7XG4gICAgdHJ5IHtcbiAgICAgICAgcmV0dXJuIGRvY3VtZW50LmxvY2F0aW9uLmhyZWY7XG4gICAgfVxuICAgIGNhdGNoIChvTykge1xuICAgICAgICByZXR1cm4gJyc7XG4gICAgfVxufVxuLyoqXG4gKiBHaXZlbiBhIGNoaWxkIERPTSBlbGVtZW50LCByZXR1cm5zIGEgcXVlcnktc2VsZWN0b3Igc3RhdGVtZW50IGRlc2NyaWJpbmcgdGhhdFxuICogYW5kIGl0cyBhbmNlc3RvcnNcbiAqIGUuZy4gW0hUTUxFbGVtZW50XSA9PiBib2R5ID4gZGl2ID4gaW5wdXQjZm9vLmJ0bltuYW1lPWJhel1cbiAqIEByZXR1cm5zIGdlbmVyYXRlZCBET00gcGF0aFxuICovXG5leHBvcnQgZnVuY3Rpb24gaHRtbFRyZWVBc1N0cmluZyhlbGVtKSB7XG4gICAgLy8gdHJ5L2NhdGNoIGJvdGg6XG4gICAgLy8gLSBhY2Nlc3NpbmcgZXZlbnQudGFyZ2V0IChzZWUgZ2V0c2VudHJ5L3JhdmVuLWpzIzgzOCwgIzc2OClcbiAgICAvLyAtIGBodG1sVHJlZUFzU3RyaW5nYCBiZWNhdXNlIGl0J3MgY29tcGxleCwgYW5kIGp1c3QgYWNjZXNzaW5nIHRoZSBET00gaW5jb3JyZWN0bHlcbiAgICAvLyAtIGNhbiB0aHJvdyBhbiBleGNlcHRpb24gaW4gc29tZSBjaXJjdW1zdGFuY2VzLlxuICAgIHRyeSB7XG4gICAgICAgIHZhciBjdXJyZW50RWxlbSA9IGVsZW07XG4gICAgICAgIHZhciBNQVhfVFJBVkVSU0VfSEVJR0hUID0gNTtcbiAgICAgICAgdmFyIE1BWF9PVVRQVVRfTEVOID0gODA7XG4gICAgICAgIHZhciBvdXQgPSBbXTtcbiAgICAgICAgdmFyIGhlaWdodCA9IDA7XG4gICAgICAgIHZhciBsZW4gPSAwO1xuICAgICAgICB2YXIgc2VwYXJhdG9yID0gJyA+ICc7XG4gICAgICAgIHZhciBzZXBMZW5ndGggPSBzZXBhcmF0b3IubGVuZ3RoO1xuICAgICAgICB2YXIgbmV4dFN0ciA9IHZvaWQgMDtcbiAgICAgICAgd2hpbGUgKGN1cnJlbnRFbGVtICYmIGhlaWdodCsrIDwgTUFYX1RSQVZFUlNFX0hFSUdIVCkge1xuICAgICAgICAgICAgbmV4dFN0ciA9IF9odG1sRWxlbWVudEFzU3RyaW5nKGN1cnJlbnRFbGVtKTtcbiAgICAgICAgICAgIC8vIGJhaWwgb3V0IGlmXG4gICAgICAgICAgICAvLyAtIG5leHRTdHIgaXMgdGhlICdodG1sJyBlbGVtZW50XG4gICAgICAgICAgICAvLyAtIHRoZSBsZW5ndGggb2YgdGhlIHN0cmluZyB0aGF0IHdvdWxkIGJlIGNyZWF0ZWQgZXhjZWVkcyBNQVhfT1VUUFVUX0xFTlxuICAgICAgICAgICAgLy8gICAoaWdub3JlIHRoaXMgbGltaXQgaWYgd2UgYXJlIG9uIHRoZSBmaXJzdCBpdGVyYXRpb24pXG4gICAgICAgICAgICBpZiAobmV4dFN0ciA9PT0gJ2h0bWwnIHx8IChoZWlnaHQgPiAxICYmIGxlbiArIG91dC5sZW5ndGggKiBzZXBMZW5ndGggKyBuZXh0U3RyLmxlbmd0aCA+PSBNQVhfT1VUUFVUX0xFTikpIHtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIG91dC5wdXNoKG5leHRTdHIpO1xuICAgICAgICAgICAgbGVuICs9IG5leHRTdHIubGVuZ3RoO1xuICAgICAgICAgICAgY3VycmVudEVsZW0gPSBjdXJyZW50RWxlbS5wYXJlbnROb2RlO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBvdXQucmV2ZXJzZSgpLmpvaW4oc2VwYXJhdG9yKTtcbiAgICB9XG4gICAgY2F0Y2ggKF9vTykge1xuICAgICAgICByZXR1cm4gJzx1bmtub3duPic7XG4gICAgfVxufVxuLyoqXG4gKiBSZXR1cm5zIGEgc2ltcGxlLCBxdWVyeS1zZWxlY3RvciByZXByZXNlbnRhdGlvbiBvZiBhIERPTSBlbGVtZW50XG4gKiBlLmcuIFtIVE1MRWxlbWVudF0gPT4gaW5wdXQjZm9vLmJ0bltuYW1lPWJhel1cbiAqIEByZXR1cm5zIGdlbmVyYXRlZCBET00gcGF0aFxuICovXG5mdW5jdGlvbiBfaHRtbEVsZW1lbnRBc1N0cmluZyhlbCkge1xuICAgIHZhciBlbGVtID0gZWw7XG4gICAgdmFyIG91dCA9IFtdO1xuICAgIHZhciBjbGFzc05hbWU7XG4gICAgdmFyIGNsYXNzZXM7XG4gICAgdmFyIGtleTtcbiAgICB2YXIgYXR0cjtcbiAgICB2YXIgaTtcbiAgICBpZiAoIWVsZW0gfHwgIWVsZW0udGFnTmFtZSkge1xuICAgICAgICByZXR1cm4gJyc7XG4gICAgfVxuICAgIG91dC5wdXNoKGVsZW0udGFnTmFtZS50b0xvd2VyQ2FzZSgpKTtcbiAgICBpZiAoZWxlbS5pZCkge1xuICAgICAgICBvdXQucHVzaChcIiNcIiArIGVsZW0uaWQpO1xuICAgIH1cbiAgICBjbGFzc05hbWUgPSBlbGVtLmNsYXNzTmFtZTtcbiAgICBpZiAoY2xhc3NOYW1lICYmIGlzU3RyaW5nKGNsYXNzTmFtZSkpIHtcbiAgICAgICAgY2xhc3NlcyA9IGNsYXNzTmFtZS5zcGxpdCgvXFxzKy8pO1xuICAgICAgICBmb3IgKGkgPSAwOyBpIDwgY2xhc3Nlcy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgb3V0LnB1c2goXCIuXCIgKyBjbGFzc2VzW2ldKTtcbiAgICAgICAgfVxuICAgIH1cbiAgICB2YXIgYWxsb3dlZEF0dHJzID0gWyd0eXBlJywgJ25hbWUnLCAndGl0bGUnLCAnYWx0J107XG4gICAgZm9yIChpID0gMDsgaSA8IGFsbG93ZWRBdHRycy5sZW5ndGg7IGkrKykge1xuICAgICAgICBrZXkgPSBhbGxvd2VkQXR0cnNbaV07XG4gICAgICAgIGF0dHIgPSBlbGVtLmdldEF0dHJpYnV0ZShrZXkpO1xuICAgICAgICBpZiAoYXR0cikge1xuICAgICAgICAgICAgb3V0LnB1c2goXCJbXCIgKyBrZXkgKyBcIj1cXFwiXCIgKyBhdHRyICsgXCJcXFwiXVwiKTtcbiAgICAgICAgfVxuICAgIH1cbiAgICByZXR1cm4gb3V0LmpvaW4oJycpO1xufVxudmFyIElOSVRJQUxfVElNRSA9IERhdGUubm93KCk7XG52YXIgcHJldk5vdyA9IDA7XG52YXIgcGVyZm9ybWFuY2VGYWxsYmFjayA9IHtcbiAgICBub3c6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdmFyIG5vdyA9IERhdGUubm93KCkgLSBJTklUSUFMX1RJTUU7XG4gICAgICAgIGlmIChub3cgPCBwcmV2Tm93KSB7XG4gICAgICAgICAgICBub3cgPSBwcmV2Tm93O1xuICAgICAgICB9XG4gICAgICAgIHByZXZOb3cgPSBub3c7XG4gICAgICAgIHJldHVybiBub3c7XG4gICAgfSxcbiAgICB0aW1lT3JpZ2luOiBJTklUSUFMX1RJTUUsXG59O1xuZXhwb3J0IHZhciBjcm9zc1BsYXRmb3JtUGVyZm9ybWFuY2UgPSAoZnVuY3Rpb24gKCkge1xuICAgIGlmIChpc05vZGVFbnYoKSkge1xuICAgICAgICB0cnkge1xuICAgICAgICAgICAgdmFyIHBlcmZIb29rcyA9IGR5bmFtaWNSZXF1aXJlKG1vZHVsZSwgJ3BlcmZfaG9va3MnKTtcbiAgICAgICAgICAgIHJldHVybiBwZXJmSG9va3MucGVyZm9ybWFuY2U7XG4gICAgICAgIH1cbiAgICAgICAgY2F0Y2ggKF8pIHtcbiAgICAgICAgICAgIHJldHVybiBwZXJmb3JtYW5jZUZhbGxiYWNrO1xuICAgICAgICB9XG4gICAgfVxuICAgIHZhciBwZXJmb3JtYW5jZSA9IGdldEdsb2JhbE9iamVjdCgpLnBlcmZvcm1hbmNlO1xuICAgIGlmICghcGVyZm9ybWFuY2UgfHwgIXBlcmZvcm1hbmNlLm5vdykge1xuICAgICAgICByZXR1cm4gcGVyZm9ybWFuY2VGYWxsYmFjaztcbiAgICB9XG4gICAgLy8gUG9seWZpbGwgZm9yIHBlcmZvcm1hbmNlLnRpbWVPcmlnaW4uXG4gICAgLy9cbiAgICAvLyBXaGlsZSBwZXJmb3JtYW5jZS50aW1pbmcubmF2aWdhdGlvblN0YXJ0IGlzIGRlcHJlY2F0ZWQgaW4gZmF2b3Igb2YgcGVyZm9ybWFuY2UudGltZU9yaWdpbiwgcGVyZm9ybWFuY2UudGltZU9yaWdpblxuICAgIC8vIGlzIG5vdCBhcyB3aWRlbHkgc3VwcG9ydGVkLiBOYW1lbHksIHBlcmZvcm1hbmNlLnRpbWVPcmlnaW4gaXMgdW5kZWZpbmVkIGluIFNhZmFyaSBhcyBvZiB3cml0aW5nLlxuICAgIC8vIHRzbGludDpkaXNhYmxlLW5leHQtbGluZTpzdHJpY3QtdHlwZS1wcmVkaWNhdGVzXG4gICAgaWYgKHBlcmZvcm1hbmNlLnRpbWVPcmlnaW4gPT09IHVuZGVmaW5lZCkge1xuICAgICAgICAvLyBBcyBvZiB3cml0aW5nLCBwZXJmb3JtYW5jZS50aW1pbmcgaXMgbm90IGF2YWlsYWJsZSBpbiBXZWIgV29ya2VycyBpbiBtYWluc3RyZWFtIGJyb3dzZXJzLCBzbyBpdCBpcyBub3QgYWx3YXlzIGFcbiAgICAgICAgLy8gdmFsaWQgZmFsbGJhY2suIEluIHRoZSBhYnNlbmNlIG9mIGEgaW5pdGlhbCB0aW1lIHByb3ZpZGVkIGJ5IHRoZSBicm93c2VyLCBmYWxsYmFjayB0byBJTklUSUFMX1RJTUUuXG4gICAgICAgIC8vIEB0cy1pZ25vcmVcbiAgICAgICAgLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOmRlcHJlY2F0aW9uXG4gICAgICAgIHBlcmZvcm1hbmNlLnRpbWVPcmlnaW4gPSAocGVyZm9ybWFuY2UudGltaW5nICYmIHBlcmZvcm1hbmNlLnRpbWluZy5uYXZpZ2F0aW9uU3RhcnQpIHx8IElOSVRJQUxfVElNRTtcbiAgICB9XG4gICAgcmV0dXJuIHBlcmZvcm1hbmNlO1xufSkoKTtcbi8qKlxuICogUmV0dXJucyBhIHRpbWVzdGFtcCBpbiBzZWNvbmRzIHdpdGggbWlsbGlzZWNvbmRzIHByZWNpc2lvbiBzaW5jZSB0aGUgVU5JWCBlcG9jaCBjYWxjdWxhdGVkIHdpdGggdGhlIG1vbm90b25pYyBjbG9jay5cbiAqL1xuZXhwb3J0IGZ1bmN0aW9uIHRpbWVzdGFtcFdpdGhNcygpIHtcbiAgICByZXR1cm4gKGNyb3NzUGxhdGZvcm1QZXJmb3JtYW5jZS50aW1lT3JpZ2luICsgY3Jvc3NQbGF0Zm9ybVBlcmZvcm1hbmNlLm5vdygpKSAvIDEwMDA7XG59XG4vLyBodHRwczovL3NlbXZlci5vcmcvI2lzLXRoZXJlLWEtc3VnZ2VzdGVkLXJlZ3VsYXItZXhwcmVzc2lvbi1yZWdleC10by1jaGVjay1hLXNlbXZlci1zdHJpbmdcbnZhciBTRU1WRVJfUkVHRVhQID0gL14oMHxbMS05XVxcZCopXFwuKDB8WzEtOV1cXGQqKVxcLigwfFsxLTldXFxkKikoPzotKCg/OjB8WzEtOV1cXGQqfFxcZCpbYS16QS1aLV1bMC05YS16QS1aLV0qKSg/OlxcLig/OjB8WzEtOV1cXGQqfFxcZCpbYS16QS1aLV1bMC05YS16QS1aLV0qKSkqKSk/KD86XFwrKFswLTlhLXpBLVotXSsoPzpcXC5bMC05YS16QS1aLV0rKSopKT8kLztcbi8qKlxuICogUGFyc2VzIGlucHV0IGludG8gYSBTZW1WZXIgaW50ZXJmYWNlXG4gKiBAcGFyYW0gaW5wdXQgc3RyaW5nIHJlcHJlc2VudGF0aW9uIG9mIGEgc2VtdmVyIHZlcnNpb25cbiAqL1xuZXhwb3J0IGZ1bmN0aW9uIHBhcnNlU2VtdmVyKGlucHV0KSB7XG4gICAgdmFyIG1hdGNoID0gaW5wdXQubWF0Y2goU0VNVkVSX1JFR0VYUCkgfHwgW107XG4gICAgdmFyIG1ham9yID0gcGFyc2VJbnQobWF0Y2hbMV0sIDEwKTtcbiAgICB2YXIgbWlub3IgPSBwYXJzZUludChtYXRjaFsyXSwgMTApO1xuICAgIHZhciBwYXRjaCA9IHBhcnNlSW50KG1hdGNoWzNdLCAxMCk7XG4gICAgcmV0dXJuIHtcbiAgICAgICAgYnVpbGRtZXRhZGF0YTogbWF0Y2hbNV0sXG4gICAgICAgIG1ham9yOiBpc05hTihtYWpvcikgPyB1bmRlZmluZWQgOiBtYWpvcixcbiAgICAgICAgbWlub3I6IGlzTmFOKG1pbm9yKSA/IHVuZGVmaW5lZCA6IG1pbm9yLFxuICAgICAgICBwYXRjaDogaXNOYU4ocGF0Y2gpID8gdW5kZWZpbmVkIDogcGF0Y2gsXG4gICAgICAgIHByZXJlbGVhc2U6IG1hdGNoWzRdLFxuICAgIH07XG59XG52YXIgZGVmYXVsdFJldHJ5QWZ0ZXIgPSA2MCAqIDEwMDA7IC8vIDYwIHNlY29uZHNcbi8qKlxuICogRXh0cmFjdHMgUmV0cnktQWZ0ZXIgdmFsdWUgZnJvbSB0aGUgcmVxdWVzdCBoZWFkZXIgb3IgcmV0dXJucyBkZWZhdWx0IHZhbHVlXG4gKiBAcGFyYW0gbm93IGN1cnJlbnQgdW5peCB0aW1lc3RhbXBcbiAqIEBwYXJhbSBoZWFkZXIgc3RyaW5nIHJlcHJlc2VudGF0aW9uIG9mICdSZXRyeS1BZnRlcicgaGVhZGVyXG4gKi9cbmV4cG9ydCBmdW5jdGlvbiBwYXJzZVJldHJ5QWZ0ZXJIZWFkZXIobm93LCBoZWFkZXIpIHtcbiAgICBpZiAoIWhlYWRlcikge1xuICAgICAgICByZXR1cm4gZGVmYXVsdFJldHJ5QWZ0ZXI7XG4gICAgfVxuICAgIHZhciBoZWFkZXJEZWxheSA9IHBhcnNlSW50KFwiXCIgKyBoZWFkZXIsIDEwKTtcbiAgICBpZiAoIWlzTmFOKGhlYWRlckRlbGF5KSkge1xuICAgICAgICByZXR1cm4gaGVhZGVyRGVsYXkgKiAxMDAwO1xuICAgIH1cbiAgICB2YXIgaGVhZGVyRGF0ZSA9IERhdGUucGFyc2UoXCJcIiArIGhlYWRlcik7XG4gICAgaWYgKCFpc05hTihoZWFkZXJEYXRlKSkge1xuICAgICAgICByZXR1cm4gaGVhZGVyRGF0ZSAtIG5vdztcbiAgICB9XG4gICAgcmV0dXJuIGRlZmF1bHRSZXRyeUFmdGVyO1xufVxudmFyIGRlZmF1bHRGdW5jdGlvbk5hbWUgPSAnPGFub255bW91cz4nO1xuLyoqXG4gKiBTYWZlbHkgZXh0cmFjdCBmdW5jdGlvbiBuYW1lIGZyb20gaXRzZWxmXG4gKi9cbmV4cG9ydCBmdW5jdGlvbiBnZXRGdW5jdGlvbk5hbWUoZm4pIHtcbiAgICB0cnkge1xuICAgICAgICBpZiAoIWZuIHx8IHR5cGVvZiBmbiAhPT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICAgICAgcmV0dXJuIGRlZmF1bHRGdW5jdGlvbk5hbWU7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGZuLm5hbWUgfHwgZGVmYXVsdEZ1bmN0aW9uTmFtZTtcbiAgICB9XG4gICAgY2F0Y2ggKGUpIHtcbiAgICAgICAgLy8gSnVzdCBhY2Nlc3NpbmcgY3VzdG9tIHByb3BzIGluIHNvbWUgU2VsZW5pdW0gZW52aXJvbm1lbnRzXG4gICAgICAgIC8vIGNhbiBjYXVzZSBhIFwiUGVybWlzc2lvbiBkZW5pZWRcIiBleGNlcHRpb24gKHNlZSByYXZlbi1qcyM0OTUpLlxuICAgICAgICByZXR1cm4gZGVmYXVsdEZ1bmN0aW9uTmFtZTtcbiAgICB9XG59XG4vKipcbiAqIFRoaXMgZnVuY3Rpb24gYWRkcyBjb250ZXh0IChwcmUvcG9zdC9saW5lKSBsaW5lcyB0byB0aGUgcHJvdmlkZWQgZnJhbWVcbiAqXG4gKiBAcGFyYW0gbGluZXMgc3RyaW5nW10gY29udGFpbmluZyBhbGwgbGluZXNcbiAqIEBwYXJhbSBmcmFtZSBTdGFja0ZyYW1lIHRoYXQgd2lsbCBiZSBtdXRhdGVkXG4gKiBAcGFyYW0gbGluZXNPZkNvbnRleHQgbnVtYmVyIG9mIGNvbnRleHQgbGluZXMgd2Ugd2FudCB0byBhZGQgcHJlL3Bvc3RcbiAqL1xuZXhwb3J0IGZ1bmN0aW9uIGFkZENvbnRleHRUb0ZyYW1lKGxpbmVzLCBmcmFtZSwgbGluZXNPZkNvbnRleHQpIHtcbiAgICBpZiAobGluZXNPZkNvbnRleHQgPT09IHZvaWQgMCkgeyBsaW5lc09mQ29udGV4dCA9IDU7IH1cbiAgICB2YXIgbGluZW5vID0gZnJhbWUubGluZW5vIHx8IDA7XG4gICAgdmFyIG1heExpbmVzID0gbGluZXMubGVuZ3RoO1xuICAgIHZhciBzb3VyY2VMaW5lID0gTWF0aC5tYXgoTWF0aC5taW4obWF4TGluZXMsIGxpbmVubyAtIDEpLCAwKTtcbiAgICBmcmFtZS5wcmVfY29udGV4dCA9IGxpbmVzXG4gICAgICAgIC5zbGljZShNYXRoLm1heCgwLCBzb3VyY2VMaW5lIC0gbGluZXNPZkNvbnRleHQpLCBzb3VyY2VMaW5lKVxuICAgICAgICAubWFwKGZ1bmN0aW9uIChsaW5lKSB7IHJldHVybiBzbmlwTGluZShsaW5lLCAwKTsgfSk7XG4gICAgZnJhbWUuY29udGV4dF9saW5lID0gc25pcExpbmUobGluZXNbTWF0aC5taW4obWF4TGluZXMgLSAxLCBzb3VyY2VMaW5lKV0sIGZyYW1lLmNvbG5vIHx8IDApO1xuICAgIGZyYW1lLnBvc3RfY29udGV4dCA9IGxpbmVzXG4gICAgICAgIC5zbGljZShNYXRoLm1pbihzb3VyY2VMaW5lICsgMSwgbWF4TGluZXMpLCBzb3VyY2VMaW5lICsgMSArIGxpbmVzT2ZDb250ZXh0KVxuICAgICAgICAubWFwKGZ1bmN0aW9uIChsaW5lKSB7IHJldHVybiBzbmlwTGluZShsaW5lLCAwKTsgfSk7XG59XG4vLyMgc291cmNlTWFwcGluZ1VSTD1taXNjLmpzLm1hcCIsImltcG9ydCAqIGFzIHRzbGliXzEgZnJvbSBcInRzbGliXCI7XG5pbXBvcnQgeyBpc0VsZW1lbnQsIGlzRXJyb3IsIGlzRXZlbnQsIGlzSW5zdGFuY2VPZiwgaXNQbGFpbk9iamVjdCwgaXNQcmltaXRpdmUsIGlzU3ludGhldGljRXZlbnQgfSBmcm9tICcuL2lzJztcbmltcG9ydCB7IE1lbW8gfSBmcm9tICcuL21lbW8nO1xuaW1wb3J0IHsgZ2V0RnVuY3Rpb25OYW1lLCBodG1sVHJlZUFzU3RyaW5nIH0gZnJvbSAnLi9taXNjJztcbmltcG9ydCB7IHRydW5jYXRlIH0gZnJvbSAnLi9zdHJpbmcnO1xuLyoqXG4gKiBXcmFwIGEgZ2l2ZW4gb2JqZWN0IG1ldGhvZCB3aXRoIGEgaGlnaGVyLW9yZGVyIGZ1bmN0aW9uXG4gKlxuICogQHBhcmFtIHNvdXJjZSBBbiBvYmplY3QgdGhhdCBjb250YWlucyBhIG1ldGhvZCB0byBiZSB3cmFwcGVkLlxuICogQHBhcmFtIG5hbWUgQSBuYW1lIG9mIG1ldGhvZCB0byBiZSB3cmFwcGVkLlxuICogQHBhcmFtIHJlcGxhY2VtZW50IEEgZnVuY3Rpb24gdGhhdCBzaG91bGQgYmUgdXNlZCB0byB3cmFwIGEgZ2l2ZW4gbWV0aG9kLlxuICogQHJldHVybnMgdm9pZFxuICovXG5leHBvcnQgZnVuY3Rpb24gZmlsbChzb3VyY2UsIG5hbWUsIHJlcGxhY2VtZW50KSB7XG4gICAgaWYgKCEobmFtZSBpbiBzb3VyY2UpKSB7XG4gICAgICAgIHJldHVybjtcbiAgICB9XG4gICAgdmFyIG9yaWdpbmFsID0gc291cmNlW25hbWVdO1xuICAgIHZhciB3cmFwcGVkID0gcmVwbGFjZW1lbnQob3JpZ2luYWwpO1xuICAgIC8vIE1ha2Ugc3VyZSBpdCdzIGEgZnVuY3Rpb24gZmlyc3QsIGFzIHdlIG5lZWQgdG8gYXR0YWNoIGFuIGVtcHR5IHByb3RvdHlwZSBmb3IgYGRlZmluZVByb3BlcnRpZXNgIHRvIHdvcmtcbiAgICAvLyBvdGhlcndpc2UgaXQnbGwgdGhyb3cgXCJUeXBlRXJyb3I6IE9iamVjdC5kZWZpbmVQcm9wZXJ0aWVzIGNhbGxlZCBvbiBub24tb2JqZWN0XCJcbiAgICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6c3RyaWN0LXR5cGUtcHJlZGljYXRlc1xuICAgIGlmICh0eXBlb2Ygd3JhcHBlZCA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICB0cnkge1xuICAgICAgICAgICAgd3JhcHBlZC5wcm90b3R5cGUgPSB3cmFwcGVkLnByb3RvdHlwZSB8fCB7fTtcbiAgICAgICAgICAgIE9iamVjdC5kZWZpbmVQcm9wZXJ0aWVzKHdyYXBwZWQsIHtcbiAgICAgICAgICAgICAgICBfX3NlbnRyeV9vcmlnaW5hbF9fOiB7XG4gICAgICAgICAgICAgICAgICAgIGVudW1lcmFibGU6IGZhbHNlLFxuICAgICAgICAgICAgICAgICAgICB2YWx1ZTogb3JpZ2luYWwsXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgICAgIGNhdGNoIChfT28pIHtcbiAgICAgICAgICAgIC8vIFRoaXMgY2FuIHRocm93IGlmIG11bHRpcGxlIGZpbGwgaGFwcGVucyBvbiBhIGdsb2JhbCBvYmplY3QgbGlrZSBYTUxIdHRwUmVxdWVzdFxuICAgICAgICAgICAgLy8gRml4ZXMgaHR0cHM6Ly9naXRodWIuY29tL2dldHNlbnRyeS9zZW50cnktamF2YXNjcmlwdC9pc3N1ZXMvMjA0M1xuICAgICAgICB9XG4gICAgfVxuICAgIHNvdXJjZVtuYW1lXSA9IHdyYXBwZWQ7XG59XG4vKipcbiAqIEVuY29kZXMgZ2l2ZW4gb2JqZWN0IGludG8gdXJsLWZyaWVuZGx5IGZvcm1hdFxuICpcbiAqIEBwYXJhbSBvYmplY3QgQW4gb2JqZWN0IHRoYXQgY29udGFpbnMgc2VyaWFsaXphYmxlIHZhbHVlc1xuICogQHJldHVybnMgc3RyaW5nIEVuY29kZWRcbiAqL1xuZXhwb3J0IGZ1bmN0aW9uIHVybEVuY29kZShvYmplY3QpIHtcbiAgICByZXR1cm4gT2JqZWN0LmtleXMob2JqZWN0KVxuICAgICAgICAubWFwKFxuICAgIC8vIHRzbGludDpkaXNhYmxlLW5leHQtbGluZTpuby11bnNhZmUtYW55XG4gICAgZnVuY3Rpb24gKGtleSkgeyByZXR1cm4gZW5jb2RlVVJJQ29tcG9uZW50KGtleSkgKyBcIj1cIiArIGVuY29kZVVSSUNvbXBvbmVudChvYmplY3Rba2V5XSk7IH0pXG4gICAgICAgIC5qb2luKCcmJyk7XG59XG4vKipcbiAqIFRyYW5zZm9ybXMgYW55IG9iamVjdCBpbnRvIGFuIG9iamVjdCBsaXRlcmFsIHdpdGggYWxsIGl0J3MgYXR0cmlidXRlc1xuICogYXR0YWNoZWQgdG8gaXQuXG4gKlxuICogQHBhcmFtIHZhbHVlIEluaXRpYWwgc291cmNlIHRoYXQgd2UgaGF2ZSB0byB0cmFuc2Zvcm0gaW4gb3JkZXIgdG8gYmUgdXNhYmxlIGJ5IHRoZSBzZXJpYWxpemVyXG4gKi9cbmZ1bmN0aW9uIGdldFdhbGtTb3VyY2UodmFsdWUpIHtcbiAgICBpZiAoaXNFcnJvcih2YWx1ZSkpIHtcbiAgICAgICAgdmFyIGVycm9yID0gdmFsdWU7XG4gICAgICAgIHZhciBlcnIgPSB7XG4gICAgICAgICAgICBtZXNzYWdlOiBlcnJvci5tZXNzYWdlLFxuICAgICAgICAgICAgbmFtZTogZXJyb3IubmFtZSxcbiAgICAgICAgICAgIHN0YWNrOiBlcnJvci5zdGFjayxcbiAgICAgICAgfTtcbiAgICAgICAgZm9yICh2YXIgaSBpbiBlcnJvcikge1xuICAgICAgICAgICAgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChlcnJvciwgaSkpIHtcbiAgICAgICAgICAgICAgICBlcnJbaV0gPSBlcnJvcltpXTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gZXJyO1xuICAgIH1cbiAgICBpZiAoaXNFdmVudCh2YWx1ZSkpIHtcbiAgICAgICAgdmFyIGV2ZW50XzEgPSB2YWx1ZTtcbiAgICAgICAgdmFyIHNvdXJjZSA9IHt9O1xuICAgICAgICBzb3VyY2UudHlwZSA9IGV2ZW50XzEudHlwZTtcbiAgICAgICAgLy8gQWNjZXNzaW5nIGV2ZW50LnRhcmdldCBjYW4gdGhyb3cgKHNlZSBnZXRzZW50cnkvcmF2ZW4tanMjODM4LCAjNzY4KVxuICAgICAgICB0cnkge1xuICAgICAgICAgICAgc291cmNlLnRhcmdldCA9IGlzRWxlbWVudChldmVudF8xLnRhcmdldClcbiAgICAgICAgICAgICAgICA/IGh0bWxUcmVlQXNTdHJpbmcoZXZlbnRfMS50YXJnZXQpXG4gICAgICAgICAgICAgICAgOiBPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nLmNhbGwoZXZlbnRfMS50YXJnZXQpO1xuICAgICAgICB9XG4gICAgICAgIGNhdGNoIChfb08pIHtcbiAgICAgICAgICAgIHNvdXJjZS50YXJnZXQgPSAnPHVua25vd24+JztcbiAgICAgICAgfVxuICAgICAgICB0cnkge1xuICAgICAgICAgICAgc291cmNlLmN1cnJlbnRUYXJnZXQgPSBpc0VsZW1lbnQoZXZlbnRfMS5jdXJyZW50VGFyZ2V0KVxuICAgICAgICAgICAgICAgID8gaHRtbFRyZWVBc1N0cmluZyhldmVudF8xLmN1cnJlbnRUYXJnZXQpXG4gICAgICAgICAgICAgICAgOiBPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nLmNhbGwoZXZlbnRfMS5jdXJyZW50VGFyZ2V0KTtcbiAgICAgICAgfVxuICAgICAgICBjYXRjaCAoX29PKSB7XG4gICAgICAgICAgICBzb3VyY2UuY3VycmVudFRhcmdldCA9ICc8dW5rbm93bj4nO1xuICAgICAgICB9XG4gICAgICAgIC8vIHRzbGludDpkaXNhYmxlLW5leHQtbGluZTpzdHJpY3QtdHlwZS1wcmVkaWNhdGVzXG4gICAgICAgIGlmICh0eXBlb2YgQ3VzdG9tRXZlbnQgIT09ICd1bmRlZmluZWQnICYmIGlzSW5zdGFuY2VPZih2YWx1ZSwgQ3VzdG9tRXZlbnQpKSB7XG4gICAgICAgICAgICBzb3VyY2UuZGV0YWlsID0gZXZlbnRfMS5kZXRhaWw7XG4gICAgICAgIH1cbiAgICAgICAgZm9yICh2YXIgaSBpbiBldmVudF8xKSB7XG4gICAgICAgICAgICBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKGV2ZW50XzEsIGkpKSB7XG4gICAgICAgICAgICAgICAgc291cmNlW2ldID0gZXZlbnRfMTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gc291cmNlO1xuICAgIH1cbiAgICByZXR1cm4gdmFsdWU7XG59XG4vKiogQ2FsY3VsYXRlcyBieXRlcyBzaXplIG9mIGlucHV0IHN0cmluZyAqL1xuZnVuY3Rpb24gdXRmOExlbmd0aCh2YWx1ZSkge1xuICAgIC8vIHRzbGludDpkaXNhYmxlLW5leHQtbGluZTpuby1iaXR3aXNlXG4gICAgcmV0dXJuIH4tZW5jb2RlVVJJKHZhbHVlKS5zcGxpdCgvJS4ufC4vKS5sZW5ndGg7XG59XG4vKiogQ2FsY3VsYXRlcyBieXRlcyBzaXplIG9mIGlucHV0IG9iamVjdCAqL1xuZnVuY3Rpb24ganNvblNpemUodmFsdWUpIHtcbiAgICByZXR1cm4gdXRmOExlbmd0aChKU09OLnN0cmluZ2lmeSh2YWx1ZSkpO1xufVxuLyoqIEpTRG9jICovXG5leHBvcnQgZnVuY3Rpb24gbm9ybWFsaXplVG9TaXplKG9iamVjdCwgXG4vLyBEZWZhdWx0IE5vZGUuanMgUkVQTCBkZXB0aFxuZGVwdGgsIFxuLy8gMTAwa0IsIGFzIDIwMGtCIGlzIG1heCBwYXlsb2FkIHNpemUsIHNvIGhhbGYgc291bmRzIHJlYXNvbmFibGVcbm1heFNpemUpIHtcbiAgICBpZiAoZGVwdGggPT09IHZvaWQgMCkgeyBkZXB0aCA9IDM7IH1cbiAgICBpZiAobWF4U2l6ZSA9PT0gdm9pZCAwKSB7IG1heFNpemUgPSAxMDAgKiAxMDI0OyB9XG4gICAgdmFyIHNlcmlhbGl6ZWQgPSBub3JtYWxpemUob2JqZWN0LCBkZXB0aCk7XG4gICAgaWYgKGpzb25TaXplKHNlcmlhbGl6ZWQpID4gbWF4U2l6ZSkge1xuICAgICAgICByZXR1cm4gbm9ybWFsaXplVG9TaXplKG9iamVjdCwgZGVwdGggLSAxLCBtYXhTaXplKTtcbiAgICB9XG4gICAgcmV0dXJuIHNlcmlhbGl6ZWQ7XG59XG4vKiogVHJhbnNmb3JtcyBhbnkgaW5wdXQgdmFsdWUgaW50byBhIHN0cmluZyBmb3JtLCBlaXRoZXIgcHJpbWl0aXZlIHZhbHVlIG9yIGEgdHlwZSBvZiB0aGUgaW5wdXQgKi9cbmZ1bmN0aW9uIHNlcmlhbGl6ZVZhbHVlKHZhbHVlKSB7XG4gICAgdmFyIHR5cGUgPSBPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nLmNhbGwodmFsdWUpO1xuICAgIC8vIE5vZGUuanMgUkVQTCBub3RhdGlvblxuICAgIGlmICh0eXBlb2YgdmFsdWUgPT09ICdzdHJpbmcnKSB7XG4gICAgICAgIHJldHVybiB2YWx1ZTtcbiAgICB9XG4gICAgaWYgKHR5cGUgPT09ICdbb2JqZWN0IE9iamVjdF0nKSB7XG4gICAgICAgIHJldHVybiAnW09iamVjdF0nO1xuICAgIH1cbiAgICBpZiAodHlwZSA9PT0gJ1tvYmplY3QgQXJyYXldJykge1xuICAgICAgICByZXR1cm4gJ1tBcnJheV0nO1xuICAgIH1cbiAgICB2YXIgbm9ybWFsaXplZCA9IG5vcm1hbGl6ZVZhbHVlKHZhbHVlKTtcbiAgICByZXR1cm4gaXNQcmltaXRpdmUobm9ybWFsaXplZCkgPyBub3JtYWxpemVkIDogdHlwZTtcbn1cbi8qKlxuICogbm9ybWFsaXplVmFsdWUoKVxuICpcbiAqIFRha2VzIHVuc2VyaWFsaXphYmxlIGlucHV0IGFuZCBtYWtlIGl0IHNlcmlhbGl6YWJsZSBmcmllbmRseVxuICpcbiAqIC0gdHJhbnNsYXRlcyB1bmRlZmluZWQvTmFOIHZhbHVlcyB0byBcIlt1bmRlZmluZWRdXCIvXCJbTmFOXVwiIHJlc3BlY3RpdmVseSxcbiAqIC0gc2VyaWFsaXplcyBFcnJvciBvYmplY3RzXG4gKiAtIGZpbHRlciBnbG9iYWwgb2JqZWN0c1xuICovXG4vLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6Y3ljbG9tYXRpYy1jb21wbGV4aXR5XG5mdW5jdGlvbiBub3JtYWxpemVWYWx1ZSh2YWx1ZSwga2V5KSB7XG4gICAgaWYgKGtleSA9PT0gJ2RvbWFpbicgJiYgdmFsdWUgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZS5fZXZlbnRzKSB7XG4gICAgICAgIHJldHVybiAnW0RvbWFpbl0nO1xuICAgIH1cbiAgICBpZiAoa2V5ID09PSAnZG9tYWluRW1pdHRlcicpIHtcbiAgICAgICAgcmV0dXJuICdbRG9tYWluRW1pdHRlcl0nO1xuICAgIH1cbiAgICBpZiAodHlwZW9mIGdsb2JhbCAhPT0gJ3VuZGVmaW5lZCcgJiYgdmFsdWUgPT09IGdsb2JhbCkge1xuICAgICAgICByZXR1cm4gJ1tHbG9iYWxdJztcbiAgICB9XG4gICAgaWYgKHR5cGVvZiB3aW5kb3cgIT09ICd1bmRlZmluZWQnICYmIHZhbHVlID09PSB3aW5kb3cpIHtcbiAgICAgICAgcmV0dXJuICdbV2luZG93XSc7XG4gICAgfVxuICAgIGlmICh0eXBlb2YgZG9jdW1lbnQgIT09ICd1bmRlZmluZWQnICYmIHZhbHVlID09PSBkb2N1bWVudCkge1xuICAgICAgICByZXR1cm4gJ1tEb2N1bWVudF0nO1xuICAgIH1cbiAgICAvLyBSZWFjdCdzIFN5bnRoZXRpY0V2ZW50IHRoaW5neVxuICAgIGlmIChpc1N5bnRoZXRpY0V2ZW50KHZhbHVlKSkge1xuICAgICAgICByZXR1cm4gJ1tTeW50aGV0aWNFdmVudF0nO1xuICAgIH1cbiAgICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6bm8tdGF1dG9sb2d5LWV4cHJlc3Npb25cbiAgICBpZiAodHlwZW9mIHZhbHVlID09PSAnbnVtYmVyJyAmJiB2YWx1ZSAhPT0gdmFsdWUpIHtcbiAgICAgICAgcmV0dXJuICdbTmFOXSc7XG4gICAgfVxuICAgIGlmICh2YWx1ZSA9PT0gdm9pZCAwKSB7XG4gICAgICAgIHJldHVybiAnW3VuZGVmaW5lZF0nO1xuICAgIH1cbiAgICBpZiAodHlwZW9mIHZhbHVlID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgIHJldHVybiBcIltGdW5jdGlvbjogXCIgKyBnZXRGdW5jdGlvbk5hbWUodmFsdWUpICsgXCJdXCI7XG4gICAgfVxuICAgIHJldHVybiB2YWx1ZTtcbn1cbi8qKlxuICogV2Fsa3MgYW4gb2JqZWN0IHRvIHBlcmZvcm0gYSBub3JtYWxpemF0aW9uIG9uIGl0XG4gKlxuICogQHBhcmFtIGtleSBvZiBvYmplY3QgdGhhdCdzIHdhbGtlZCBpbiBjdXJyZW50IGl0ZXJhdGlvblxuICogQHBhcmFtIHZhbHVlIG9iamVjdCB0byBiZSB3YWxrZWRcbiAqIEBwYXJhbSBkZXB0aCBPcHRpb25hbCBudW1iZXIgaW5kaWNhdGluZyBob3cgZGVlcCBzaG91bGQgd2Fsa2luZyBiZSBwZXJmb3JtZWRcbiAqIEBwYXJhbSBtZW1vIE9wdGlvbmFsIE1lbW8gY2xhc3MgaGFuZGxpbmcgZGVjeWNsaW5nXG4gKi9cbmV4cG9ydCBmdW5jdGlvbiB3YWxrKGtleSwgdmFsdWUsIGRlcHRoLCBtZW1vKSB7XG4gICAgaWYgKGRlcHRoID09PSB2b2lkIDApIHsgZGVwdGggPSArSW5maW5pdHk7IH1cbiAgICBpZiAobWVtbyA9PT0gdm9pZCAwKSB7IG1lbW8gPSBuZXcgTWVtbygpOyB9XG4gICAgLy8gSWYgd2UgcmVhY2ggdGhlIG1heGltdW0gZGVwdGgsIHNlcmlhbGl6ZSB3aGF0ZXZlciBoYXMgbGVmdFxuICAgIGlmIChkZXB0aCA9PT0gMCkge1xuICAgICAgICByZXR1cm4gc2VyaWFsaXplVmFsdWUodmFsdWUpO1xuICAgIH1cbiAgICAvLyBJZiB2YWx1ZSBpbXBsZW1lbnRzIGB0b0pTT05gIG1ldGhvZCwgY2FsbCBpdCBhbmQgcmV0dXJuIGVhcmx5XG4gICAgLy8gdHNsaW50OmRpc2FibGU6bm8tdW5zYWZlLWFueVxuICAgIGlmICh2YWx1ZSAhPT0gbnVsbCAmJiB2YWx1ZSAhPT0gdW5kZWZpbmVkICYmIHR5cGVvZiB2YWx1ZS50b0pTT04gPT09ICdmdW5jdGlvbicpIHtcbiAgICAgICAgcmV0dXJuIHZhbHVlLnRvSlNPTigpO1xuICAgIH1cbiAgICAvLyB0c2xpbnQ6ZW5hYmxlOm5vLXVuc2FmZS1hbnlcbiAgICAvLyBJZiBub3JtYWxpemVkIHZhbHVlIGlzIGEgcHJpbWl0aXZlLCB0aGVyZSBhcmUgbm8gYnJhbmNoZXMgbGVmdCB0byB3YWxrLCBzbyB3ZSBjYW4ganVzdCBiYWlsIG91dCwgYXMgdGhlcmVzIG5vIHBvaW50IGluIGdvaW5nIGRvd24gdGhhdCBicmFuY2ggYW55IGZ1cnRoZXJcbiAgICB2YXIgbm9ybWFsaXplZCA9IG5vcm1hbGl6ZVZhbHVlKHZhbHVlLCBrZXkpO1xuICAgIGlmIChpc1ByaW1pdGl2ZShub3JtYWxpemVkKSkge1xuICAgICAgICByZXR1cm4gbm9ybWFsaXplZDtcbiAgICB9XG4gICAgLy8gQ3JlYXRlIHNvdXJjZSB0aGF0IHdlIHdpbGwgdXNlIGZvciBuZXh0IGl0dGVyYXRpb25zLCBlaXRoZXIgb2JqZWN0aWZpZWQgZXJyb3Igb2JqZWN0IChFcnJvciB0eXBlIHdpdGggZXh0cmFjdGVkIGtleXM6dmFsdWUgcGFpcnMpIG9yIHRoZSBpbnB1dCBpdHNlbGZcbiAgICB2YXIgc291cmNlID0gZ2V0V2Fsa1NvdXJjZSh2YWx1ZSk7XG4gICAgLy8gQ3JlYXRlIGFuIGFjY3VtdWxhdG9yIHRoYXQgd2lsbCBhY3QgYXMgYSBwYXJlbnQgZm9yIGFsbCBmdXR1cmUgaXR0ZXJhdGlvbnMgb2YgdGhhdCBicmFuY2hcbiAgICB2YXIgYWNjID0gQXJyYXkuaXNBcnJheSh2YWx1ZSkgPyBbXSA6IHt9O1xuICAgIC8vIElmIHdlIGFscmVhZHkgd2Fsa2VkIHRoYXQgYnJhbmNoLCBiYWlsIG91dCwgYXMgaXQncyBjaXJjdWxhciByZWZlcmVuY2VcbiAgICBpZiAobWVtby5tZW1vaXplKHZhbHVlKSkge1xuICAgICAgICByZXR1cm4gJ1tDaXJjdWxhciB+XSc7XG4gICAgfVxuICAgIC8vIFdhbGsgYWxsIGtleXMgb2YgdGhlIHNvdXJjZVxuICAgIGZvciAodmFyIGlubmVyS2V5IGluIHNvdXJjZSkge1xuICAgICAgICAvLyBBdm9pZCBpdGVyYXRpbmcgb3ZlciBmaWVsZHMgaW4gdGhlIHByb3RvdHlwZSBpZiB0aGV5J3ZlIHNvbWVob3cgYmVlbiBleHBvc2VkIHRvIGVudW1lcmF0aW9uLlxuICAgICAgICBpZiAoIU9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChzb3VyY2UsIGlubmVyS2V5KSkge1xuICAgICAgICAgICAgY29udGludWU7XG4gICAgICAgIH1cbiAgICAgICAgLy8gUmVjdXJzaXZlbHkgd2FsayB0aHJvdWdoIGFsbCB0aGUgY2hpbGQgbm9kZXNcbiAgICAgICAgYWNjW2lubmVyS2V5XSA9IHdhbGsoaW5uZXJLZXksIHNvdXJjZVtpbm5lcktleV0sIGRlcHRoIC0gMSwgbWVtbyk7XG4gICAgfVxuICAgIC8vIE9uY2Ugd2Fsa2VkIHRocm91Z2ggYWxsIHRoZSBicmFuY2hlcywgcmVtb3ZlIHRoZSBwYXJlbnQgZnJvbSBtZW1vIHN0b3JhZ2VcbiAgICBtZW1vLnVubWVtb2l6ZSh2YWx1ZSk7XG4gICAgLy8gUmV0dXJuIGFjY3VtdWxhdGVkIHZhbHVlc1xuICAgIHJldHVybiBhY2M7XG59XG4vKipcbiAqIG5vcm1hbGl6ZSgpXG4gKlxuICogLSBDcmVhdGVzIGEgY29weSB0byBwcmV2ZW50IG9yaWdpbmFsIGlucHV0IG11dGF0aW9uXG4gKiAtIFNraXAgbm9uLWVudW1lcmFibGVyc1xuICogLSBDYWxscyBgdG9KU09OYCBpZiBpbXBsZW1lbnRlZFxuICogLSBSZW1vdmVzIGNpcmN1bGFyIHJlZmVyZW5jZXNcbiAqIC0gVHJhbnNsYXRlcyBub24tc2VyaWFsaXplYWJsZSB2YWx1ZXMgKHVuZGVmaW5lZC9OYU4vRnVuY3Rpb25zKSB0byBzZXJpYWxpemFibGUgZm9ybWF0XG4gKiAtIFRyYW5zbGF0ZXMga25vd24gZ2xvYmFsIG9iamVjdHMvQ2xhc3NlcyB0byBhIHN0cmluZyByZXByZXNlbnRhdGlvbnNcbiAqIC0gVGFrZXMgY2FyZSBvZiBFcnJvciBvYmplY3RzIHNlcmlhbGl6YXRpb25cbiAqIC0gT3B0aW9uYWxseSBsaW1pdCBkZXB0aCBvZiBmaW5hbCBvdXRwdXRcbiAqL1xuZXhwb3J0IGZ1bmN0aW9uIG5vcm1hbGl6ZShpbnB1dCwgZGVwdGgpIHtcbiAgICB0cnkge1xuICAgICAgICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6bm8tdW5zYWZlLWFueVxuICAgICAgICByZXR1cm4gSlNPTi5wYXJzZShKU09OLnN0cmluZ2lmeShpbnB1dCwgZnVuY3Rpb24gKGtleSwgdmFsdWUpIHsgcmV0dXJuIHdhbGsoa2V5LCB2YWx1ZSwgZGVwdGgpOyB9KSk7XG4gICAgfVxuICAgIGNhdGNoIChfb08pIHtcbiAgICAgICAgcmV0dXJuICcqKm5vbi1zZXJpYWxpemFibGUqKic7XG4gICAgfVxufVxuLyoqXG4gKiBHaXZlbiBhbnkgY2FwdHVyZWQgZXhjZXB0aW9uLCBleHRyYWN0IGl0cyBrZXlzIGFuZCBjcmVhdGUgYSBzb3J0ZWRcbiAqIGFuZCB0cnVuY2F0ZWQgbGlzdCB0aGF0IHdpbGwgYmUgdXNlZCBpbnNpZGUgdGhlIGV2ZW50IG1lc3NhZ2UuXG4gKiBlZy4gYE5vbi1lcnJvciBleGNlcHRpb24gY2FwdHVyZWQgd2l0aCBrZXlzOiBmb28sIGJhciwgYmF6YFxuICovXG5leHBvcnQgZnVuY3Rpb24gZXh0cmFjdEV4Y2VwdGlvbktleXNGb3JNZXNzYWdlKGV4Y2VwdGlvbiwgbWF4TGVuZ3RoKSB7XG4gICAgaWYgKG1heExlbmd0aCA9PT0gdm9pZCAwKSB7IG1heExlbmd0aCA9IDQwOyB9XG4gICAgLy8gdHNsaW50OmRpc2FibGU6c3RyaWN0LXR5cGUtcHJlZGljYXRlc1xuICAgIHZhciBrZXlzID0gT2JqZWN0LmtleXMoZ2V0V2Fsa1NvdXJjZShleGNlcHRpb24pKTtcbiAgICBrZXlzLnNvcnQoKTtcbiAgICBpZiAoIWtleXMubGVuZ3RoKSB7XG4gICAgICAgIHJldHVybiAnW29iamVjdCBoYXMgbm8ga2V5c10nO1xuICAgIH1cbiAgICBpZiAoa2V5c1swXS5sZW5ndGggPj0gbWF4TGVuZ3RoKSB7XG4gICAgICAgIHJldHVybiB0cnVuY2F0ZShrZXlzWzBdLCBtYXhMZW5ndGgpO1xuICAgIH1cbiAgICBmb3IgKHZhciBpbmNsdWRlZEtleXMgPSBrZXlzLmxlbmd0aDsgaW5jbHVkZWRLZXlzID4gMDsgaW5jbHVkZWRLZXlzLS0pIHtcbiAgICAgICAgdmFyIHNlcmlhbGl6ZWQgPSBrZXlzLnNsaWNlKDAsIGluY2x1ZGVkS2V5cykuam9pbignLCAnKTtcbiAgICAgICAgaWYgKHNlcmlhbGl6ZWQubGVuZ3RoID4gbWF4TGVuZ3RoKSB7XG4gICAgICAgICAgICBjb250aW51ZTtcbiAgICAgICAgfVxuICAgICAgICBpZiAoaW5jbHVkZWRLZXlzID09PSBrZXlzLmxlbmd0aCkge1xuICAgICAgICAgICAgcmV0dXJuIHNlcmlhbGl6ZWQ7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHRydW5jYXRlKHNlcmlhbGl6ZWQsIG1heExlbmd0aCk7XG4gICAgfVxuICAgIHJldHVybiAnJztcbn1cbi8qKlxuICogR2l2ZW4gYW55IG9iamVjdCwgcmV0dXJuIHRoZSBuZXcgb2JqZWN0IHdpdGggcmVtb3ZlZCBrZXlzIHRoYXQgdmFsdWUgd2FzIGB1bmRlZmluZWRgLlxuICogV29ya3MgcmVjdXJzaXZlbHkgb24gb2JqZWN0cyBhbmQgYXJyYXlzLlxuICovXG5leHBvcnQgZnVuY3Rpb24gZHJvcFVuZGVmaW5lZEtleXModmFsKSB7XG4gICAgdmFyIGVfMSwgX2E7XG4gICAgaWYgKGlzUGxhaW5PYmplY3QodmFsKSkge1xuICAgICAgICB2YXIgb2JqID0gdmFsO1xuICAgICAgICB2YXIgcnYgPSB7fTtcbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIGZvciAodmFyIF9iID0gdHNsaWJfMS5fX3ZhbHVlcyhPYmplY3Qua2V5cyhvYmopKSwgX2MgPSBfYi5uZXh0KCk7ICFfYy5kb25lOyBfYyA9IF9iLm5leHQoKSkge1xuICAgICAgICAgICAgICAgIHZhciBrZXkgPSBfYy52YWx1ZTtcbiAgICAgICAgICAgICAgICBpZiAodHlwZW9mIG9ialtrZXldICE9PSAndW5kZWZpbmVkJykge1xuICAgICAgICAgICAgICAgICAgICBydltrZXldID0gZHJvcFVuZGVmaW5lZEtleXMob2JqW2tleV0pO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICBjYXRjaCAoZV8xXzEpIHsgZV8xID0geyBlcnJvcjogZV8xXzEgfTsgfVxuICAgICAgICBmaW5hbGx5IHtcbiAgICAgICAgICAgIHRyeSB7XG4gICAgICAgICAgICAgICAgaWYgKF9jICYmICFfYy5kb25lICYmIChfYSA9IF9iLnJldHVybikpIF9hLmNhbGwoX2IpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZmluYWxseSB7IGlmIChlXzEpIHRocm93IGVfMS5lcnJvcjsgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiBydjtcbiAgICB9XG4gICAgaWYgKEFycmF5LmlzQXJyYXkodmFsKSkge1xuICAgICAgICByZXR1cm4gdmFsLm1hcChkcm9wVW5kZWZpbmVkS2V5cyk7XG4gICAgfVxuICAgIHJldHVybiB2YWw7XG59XG4vLyMgc291cmNlTWFwcGluZ1VSTD1vYmplY3QuanMubWFwIiwiLy8gU2xpZ2h0bHkgbW9kaWZpZWQgKG5vIElFOCBzdXBwb3J0LCBFUzYpIGFuZCB0cmFuc2NyaWJlZCB0byBUeXBlU2NyaXB0XG4vLyBodHRwczovL3Jhdy5naXRodWJ1c2VyY29udGVudC5jb20vY2FsdmlubWV0Y2FsZi9yb2xsdXAtcGx1Z2luLW5vZGUtYnVpbHRpbnMvbWFzdGVyL3NyYy9lczYvcGF0aC5qc1xuLyoqIEpTRG9jICovXG5mdW5jdGlvbiBub3JtYWxpemVBcnJheShwYXJ0cywgYWxsb3dBYm92ZVJvb3QpIHtcbiAgICAvLyBpZiB0aGUgcGF0aCB0cmllcyB0byBnbyBhYm92ZSB0aGUgcm9vdCwgYHVwYCBlbmRzIHVwID4gMFxuICAgIHZhciB1cCA9IDA7XG4gICAgZm9yICh2YXIgaSA9IHBhcnRzLmxlbmd0aCAtIDE7IGkgPj0gMDsgaS0tKSB7XG4gICAgICAgIHZhciBsYXN0ID0gcGFydHNbaV07XG4gICAgICAgIGlmIChsYXN0ID09PSAnLicpIHtcbiAgICAgICAgICAgIHBhcnRzLnNwbGljZShpLCAxKTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIGlmIChsYXN0ID09PSAnLi4nKSB7XG4gICAgICAgICAgICBwYXJ0cy5zcGxpY2UoaSwgMSk7XG4gICAgICAgICAgICB1cCsrO1xuICAgICAgICB9XG4gICAgICAgIGVsc2UgaWYgKHVwKSB7XG4gICAgICAgICAgICBwYXJ0cy5zcGxpY2UoaSwgMSk7XG4gICAgICAgICAgICB1cC0tO1xuICAgICAgICB9XG4gICAgfVxuICAgIC8vIGlmIHRoZSBwYXRoIGlzIGFsbG93ZWQgdG8gZ28gYWJvdmUgdGhlIHJvb3QsIHJlc3RvcmUgbGVhZGluZyAuLnNcbiAgICBpZiAoYWxsb3dBYm92ZVJvb3QpIHtcbiAgICAgICAgZm9yICg7IHVwLS07IHVwKSB7XG4gICAgICAgICAgICBwYXJ0cy51bnNoaWZ0KCcuLicpO1xuICAgICAgICB9XG4gICAgfVxuICAgIHJldHVybiBwYXJ0cztcbn1cbi8vIFNwbGl0IGEgZmlsZW5hbWUgaW50byBbcm9vdCwgZGlyLCBiYXNlbmFtZSwgZXh0XSwgdW5peCB2ZXJzaW9uXG4vLyAncm9vdCcgaXMganVzdCBhIHNsYXNoLCBvciBub3RoaW5nLlxudmFyIHNwbGl0UGF0aFJlID0gL14oXFwvP3wpKFtcXHNcXFNdKj8pKCg/OlxcLnsxLDJ9fFteXFwvXSs/fCkoXFwuW14uXFwvXSp8KSkoPzpbXFwvXSopJC87XG4vKiogSlNEb2MgKi9cbmZ1bmN0aW9uIHNwbGl0UGF0aChmaWxlbmFtZSkge1xuICAgIHZhciBwYXJ0cyA9IHNwbGl0UGF0aFJlLmV4ZWMoZmlsZW5hbWUpO1xuICAgIHJldHVybiBwYXJ0cyA/IHBhcnRzLnNsaWNlKDEpIDogW107XG59XG4vLyBwYXRoLnJlc29sdmUoW2Zyb20gLi4uXSwgdG8pXG4vLyBwb3NpeCB2ZXJzaW9uXG4vKiogSlNEb2MgKi9cbmV4cG9ydCBmdW5jdGlvbiByZXNvbHZlKCkge1xuICAgIHZhciBhcmdzID0gW107XG4gICAgZm9yICh2YXIgX2kgPSAwOyBfaSA8IGFyZ3VtZW50cy5sZW5ndGg7IF9pKyspIHtcbiAgICAgICAgYXJnc1tfaV0gPSBhcmd1bWVudHNbX2ldO1xuICAgIH1cbiAgICB2YXIgcmVzb2x2ZWRQYXRoID0gJyc7XG4gICAgdmFyIHJlc29sdmVkQWJzb2x1dGUgPSBmYWxzZTtcbiAgICBmb3IgKHZhciBpID0gYXJncy5sZW5ndGggLSAxOyBpID49IC0xICYmICFyZXNvbHZlZEFic29sdXRlOyBpLS0pIHtcbiAgICAgICAgdmFyIHBhdGggPSBpID49IDAgPyBhcmdzW2ldIDogJy8nO1xuICAgICAgICAvLyBTa2lwIGVtcHR5IGVudHJpZXNcbiAgICAgICAgaWYgKCFwYXRoKSB7XG4gICAgICAgICAgICBjb250aW51ZTtcbiAgICAgICAgfVxuICAgICAgICByZXNvbHZlZFBhdGggPSBwYXRoICsgXCIvXCIgKyByZXNvbHZlZFBhdGg7XG4gICAgICAgIHJlc29sdmVkQWJzb2x1dGUgPSBwYXRoLmNoYXJBdCgwKSA9PT0gJy8nO1xuICAgIH1cbiAgICAvLyBBdCB0aGlzIHBvaW50IHRoZSBwYXRoIHNob3VsZCBiZSByZXNvbHZlZCB0byBhIGZ1bGwgYWJzb2x1dGUgcGF0aCwgYnV0XG4gICAgLy8gaGFuZGxlIHJlbGF0aXZlIHBhdGhzIHRvIGJlIHNhZmUgKG1pZ2h0IGhhcHBlbiB3aGVuIHByb2Nlc3MuY3dkKCkgZmFpbHMpXG4gICAgLy8gTm9ybWFsaXplIHRoZSBwYXRoXG4gICAgcmVzb2x2ZWRQYXRoID0gbm9ybWFsaXplQXJyYXkocmVzb2x2ZWRQYXRoLnNwbGl0KCcvJykuZmlsdGVyKGZ1bmN0aW9uIChwKSB7IHJldHVybiAhIXA7IH0pLCAhcmVzb2x2ZWRBYnNvbHV0ZSkuam9pbignLycpO1xuICAgIHJldHVybiAocmVzb2x2ZWRBYnNvbHV0ZSA/ICcvJyA6ICcnKSArIHJlc29sdmVkUGF0aCB8fCAnLic7XG59XG4vKiogSlNEb2MgKi9cbmZ1bmN0aW9uIHRyaW0oYXJyKSB7XG4gICAgdmFyIHN0YXJ0ID0gMDtcbiAgICBmb3IgKDsgc3RhcnQgPCBhcnIubGVuZ3RoOyBzdGFydCsrKSB7XG4gICAgICAgIGlmIChhcnJbc3RhcnRdICE9PSAnJykge1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cbiAgICB9XG4gICAgdmFyIGVuZCA9IGFyci5sZW5ndGggLSAxO1xuICAgIGZvciAoOyBlbmQgPj0gMDsgZW5kLS0pIHtcbiAgICAgICAgaWYgKGFycltlbmRdICE9PSAnJykge1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cbiAgICB9XG4gICAgaWYgKHN0YXJ0ID4gZW5kKSB7XG4gICAgICAgIHJldHVybiBbXTtcbiAgICB9XG4gICAgcmV0dXJuIGFyci5zbGljZShzdGFydCwgZW5kIC0gc3RhcnQgKyAxKTtcbn1cbi8vIHBhdGgucmVsYXRpdmUoZnJvbSwgdG8pXG4vLyBwb3NpeCB2ZXJzaW9uXG4vKiogSlNEb2MgKi9cbmV4cG9ydCBmdW5jdGlvbiByZWxhdGl2ZShmcm9tLCB0bykge1xuICAgIC8vIHRzbGludDpkaXNhYmxlOm5vLXBhcmFtZXRlci1yZWFzc2lnbm1lbnRcbiAgICBmcm9tID0gcmVzb2x2ZShmcm9tKS5zdWJzdHIoMSk7XG4gICAgdG8gPSByZXNvbHZlKHRvKS5zdWJzdHIoMSk7XG4gICAgdmFyIGZyb21QYXJ0cyA9IHRyaW0oZnJvbS5zcGxpdCgnLycpKTtcbiAgICB2YXIgdG9QYXJ0cyA9IHRyaW0odG8uc3BsaXQoJy8nKSk7XG4gICAgdmFyIGxlbmd0aCA9IE1hdGgubWluKGZyb21QYXJ0cy5sZW5ndGgsIHRvUGFydHMubGVuZ3RoKTtcbiAgICB2YXIgc2FtZVBhcnRzTGVuZ3RoID0gbGVuZ3RoO1xuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgbGVuZ3RoOyBpKyspIHtcbiAgICAgICAgaWYgKGZyb21QYXJ0c1tpXSAhPT0gdG9QYXJ0c1tpXSkge1xuICAgICAgICAgICAgc2FtZVBhcnRzTGVuZ3RoID0gaTtcbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICB9XG4gICAgfVxuICAgIHZhciBvdXRwdXRQYXJ0cyA9IFtdO1xuICAgIGZvciAodmFyIGkgPSBzYW1lUGFydHNMZW5ndGg7IGkgPCBmcm9tUGFydHMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgb3V0cHV0UGFydHMucHVzaCgnLi4nKTtcbiAgICB9XG4gICAgb3V0cHV0UGFydHMgPSBvdXRwdXRQYXJ0cy5jb25jYXQodG9QYXJ0cy5zbGljZShzYW1lUGFydHNMZW5ndGgpKTtcbiAgICByZXR1cm4gb3V0cHV0UGFydHMuam9pbignLycpO1xufVxuLy8gcGF0aC5ub3JtYWxpemUocGF0aClcbi8vIHBvc2l4IHZlcnNpb25cbi8qKiBKU0RvYyAqL1xuZXhwb3J0IGZ1bmN0aW9uIG5vcm1hbGl6ZVBhdGgocGF0aCkge1xuICAgIHZhciBpc1BhdGhBYnNvbHV0ZSA9IGlzQWJzb2x1dGUocGF0aCk7XG4gICAgdmFyIHRyYWlsaW5nU2xhc2ggPSBwYXRoLnN1YnN0cigtMSkgPT09ICcvJztcbiAgICAvLyBOb3JtYWxpemUgdGhlIHBhdGhcbiAgICB2YXIgbm9ybWFsaXplZFBhdGggPSBub3JtYWxpemVBcnJheShwYXRoLnNwbGl0KCcvJykuZmlsdGVyKGZ1bmN0aW9uIChwKSB7IHJldHVybiAhIXA7IH0pLCAhaXNQYXRoQWJzb2x1dGUpLmpvaW4oJy8nKTtcbiAgICBpZiAoIW5vcm1hbGl6ZWRQYXRoICYmICFpc1BhdGhBYnNvbHV0ZSkge1xuICAgICAgICBub3JtYWxpemVkUGF0aCA9ICcuJztcbiAgICB9XG4gICAgaWYgKG5vcm1hbGl6ZWRQYXRoICYmIHRyYWlsaW5nU2xhc2gpIHtcbiAgICAgICAgbm9ybWFsaXplZFBhdGggKz0gJy8nO1xuICAgIH1cbiAgICByZXR1cm4gKGlzUGF0aEFic29sdXRlID8gJy8nIDogJycpICsgbm9ybWFsaXplZFBhdGg7XG59XG4vLyBwb3NpeCB2ZXJzaW9uXG4vKiogSlNEb2MgKi9cbmV4cG9ydCBmdW5jdGlvbiBpc0Fic29sdXRlKHBhdGgpIHtcbiAgICByZXR1cm4gcGF0aC5jaGFyQXQoMCkgPT09ICcvJztcbn1cbi8vIHBvc2l4IHZlcnNpb25cbi8qKiBKU0RvYyAqL1xuZXhwb3J0IGZ1bmN0aW9uIGpvaW4oKSB7XG4gICAgdmFyIGFyZ3MgPSBbXTtcbiAgICBmb3IgKHZhciBfaSA9IDA7IF9pIDwgYXJndW1lbnRzLmxlbmd0aDsgX2krKykge1xuICAgICAgICBhcmdzW19pXSA9IGFyZ3VtZW50c1tfaV07XG4gICAgfVxuICAgIHJldHVybiBub3JtYWxpemVQYXRoKGFyZ3Muam9pbignLycpKTtcbn1cbi8qKiBKU0RvYyAqL1xuZXhwb3J0IGZ1bmN0aW9uIGRpcm5hbWUocGF0aCkge1xuICAgIHZhciByZXN1bHQgPSBzcGxpdFBhdGgocGF0aCk7XG4gICAgdmFyIHJvb3QgPSByZXN1bHRbMF07XG4gICAgdmFyIGRpciA9IHJlc3VsdFsxXTtcbiAgICBpZiAoIXJvb3QgJiYgIWRpcikge1xuICAgICAgICAvLyBObyBkaXJuYW1lIHdoYXRzb2V2ZXJcbiAgICAgICAgcmV0dXJuICcuJztcbiAgICB9XG4gICAgaWYgKGRpcikge1xuICAgICAgICAvLyBJdCBoYXMgYSBkaXJuYW1lLCBzdHJpcCB0cmFpbGluZyBzbGFzaFxuICAgICAgICBkaXIgPSBkaXIuc3Vic3RyKDAsIGRpci5sZW5ndGggLSAxKTtcbiAgICB9XG4gICAgcmV0dXJuIHJvb3QgKyBkaXI7XG59XG4vKiogSlNEb2MgKi9cbmV4cG9ydCBmdW5jdGlvbiBiYXNlbmFtZShwYXRoLCBleHQpIHtcbiAgICB2YXIgZiA9IHNwbGl0UGF0aChwYXRoKVsyXTtcbiAgICBpZiAoZXh0ICYmIGYuc3Vic3RyKGV4dC5sZW5ndGggKiAtMSkgPT09IGV4dCkge1xuICAgICAgICBmID0gZi5zdWJzdHIoMCwgZi5sZW5ndGggLSBleHQubGVuZ3RoKTtcbiAgICB9XG4gICAgcmV0dXJuIGY7XG59XG4vLyMgc291cmNlTWFwcGluZ1VSTD1wYXRoLmpzLm1hcCIsImV4cG9ydCB2YXIgc2V0UHJvdG90eXBlT2YgPSBPYmplY3Quc2V0UHJvdG90eXBlT2YgfHwgKHsgX19wcm90b19fOiBbXSB9IGluc3RhbmNlb2YgQXJyYXkgPyBzZXRQcm90b09mIDogbWl4aW5Qcm9wZXJ0aWVzKTsgLy8gdHNsaW50OmRpc2FibGUtbGluZTpuby11bmJvdW5kLW1ldGhvZFxuLyoqXG4gKiBzZXRQcm90b3R5cGVPZiBwb2x5ZmlsbCB1c2luZyBfX3Byb3RvX19cbiAqL1xuZnVuY3Rpb24gc2V0UHJvdG9PZihvYmosIHByb3RvKSB7XG4gICAgLy8gQHRzLWlnbm9yZVxuICAgIG9iai5fX3Byb3RvX18gPSBwcm90bztcbiAgICByZXR1cm4gb2JqO1xufVxuLyoqXG4gKiBzZXRQcm90b3R5cGVPZiBwb2x5ZmlsbCB1c2luZyBtaXhpblxuICovXG5mdW5jdGlvbiBtaXhpblByb3BlcnRpZXMob2JqLCBwcm90bykge1xuICAgIGZvciAodmFyIHByb3AgaW4gcHJvdG8pIHtcbiAgICAgICAgaWYgKCFvYmouaGFzT3duUHJvcGVydHkocHJvcCkpIHtcbiAgICAgICAgICAgIC8vIEB0cy1pZ25vcmVcbiAgICAgICAgICAgIG9ialtwcm9wXSA9IHByb3RvW3Byb3BdO1xuICAgICAgICB9XG4gICAgfVxuICAgIHJldHVybiBvYmo7XG59XG4vLyMgc291cmNlTWFwcGluZ1VSTD1wb2x5ZmlsbC5qcy5tYXAiLCJpbXBvcnQgeyBTZW50cnlFcnJvciB9IGZyb20gJy4vZXJyb3InO1xuaW1wb3J0IHsgU3luY1Byb21pc2UgfSBmcm9tICcuL3N5bmNwcm9taXNlJztcbi8qKiBBIHNpbXBsZSBxdWV1ZSB0aGF0IGhvbGRzIHByb21pc2VzLiAqL1xudmFyIFByb21pc2VCdWZmZXIgPSAvKiogQGNsYXNzICovIChmdW5jdGlvbiAoKSB7XG4gICAgZnVuY3Rpb24gUHJvbWlzZUJ1ZmZlcihfbGltaXQpIHtcbiAgICAgICAgdGhpcy5fbGltaXQgPSBfbGltaXQ7XG4gICAgICAgIC8qKiBJbnRlcm5hbCBzZXQgb2YgcXVldWVkIFByb21pc2VzICovXG4gICAgICAgIHRoaXMuX2J1ZmZlciA9IFtdO1xuICAgIH1cbiAgICAvKipcbiAgICAgKiBTYXlzIGlmIHRoZSBidWZmZXIgaXMgcmVhZHkgdG8gdGFrZSBtb3JlIHJlcXVlc3RzXG4gICAgICovXG4gICAgUHJvbWlzZUJ1ZmZlci5wcm90b3R5cGUuaXNSZWFkeSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX2xpbWl0ID09PSB1bmRlZmluZWQgfHwgdGhpcy5sZW5ndGgoKSA8IHRoaXMuX2xpbWl0O1xuICAgIH07XG4gICAgLyoqXG4gICAgICogQWRkIGEgcHJvbWlzZSB0byB0aGUgcXVldWUuXG4gICAgICpcbiAgICAgKiBAcGFyYW0gdGFzayBDYW4gYmUgYW55IFByb21pc2VMaWtlPFQ+XG4gICAgICogQHJldHVybnMgVGhlIG9yaWdpbmFsIHByb21pc2UuXG4gICAgICovXG4gICAgUHJvbWlzZUJ1ZmZlci5wcm90b3R5cGUuYWRkID0gZnVuY3Rpb24gKHRhc2spIHtcbiAgICAgICAgdmFyIF90aGlzID0gdGhpcztcbiAgICAgICAgaWYgKCF0aGlzLmlzUmVhZHkoKSkge1xuICAgICAgICAgICAgcmV0dXJuIFN5bmNQcm9taXNlLnJlamVjdChuZXcgU2VudHJ5RXJyb3IoJ05vdCBhZGRpbmcgUHJvbWlzZSBkdWUgdG8gYnVmZmVyIGxpbWl0IHJlYWNoZWQuJykpO1xuICAgICAgICB9XG4gICAgICAgIGlmICh0aGlzLl9idWZmZXIuaW5kZXhPZih0YXNrKSA9PT0gLTEpIHtcbiAgICAgICAgICAgIHRoaXMuX2J1ZmZlci5wdXNoKHRhc2spO1xuICAgICAgICB9XG4gICAgICAgIHRhc2tcbiAgICAgICAgICAgIC50aGVuKGZ1bmN0aW9uICgpIHsgcmV0dXJuIF90aGlzLnJlbW92ZSh0YXNrKTsgfSlcbiAgICAgICAgICAgIC50aGVuKG51bGwsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHJldHVybiBfdGhpcy5yZW1vdmUodGFzaykudGhlbihudWxsLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgLy8gV2UgaGF2ZSB0byBhZGQgdGhpcyBjYXRjaCBoZXJlIG90aGVyd2lzZSB3ZSBoYXZlIGFuIHVuaGFuZGxlZFByb21pc2VSZWplY3Rpb25cbiAgICAgICAgICAgICAgICAvLyBiZWNhdXNlIGl0J3MgYSBuZXcgUHJvbWlzZSBjaGFpbi5cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICAgICAgcmV0dXJuIHRhc2s7XG4gICAgfTtcbiAgICAvKipcbiAgICAgKiBSZW1vdmUgYSBwcm9taXNlIHRvIHRoZSBxdWV1ZS5cbiAgICAgKlxuICAgICAqIEBwYXJhbSB0YXNrIENhbiBiZSBhbnkgUHJvbWlzZUxpa2U8VD5cbiAgICAgKiBAcmV0dXJucyBSZW1vdmVkIHByb21pc2UuXG4gICAgICovXG4gICAgUHJvbWlzZUJ1ZmZlci5wcm90b3R5cGUucmVtb3ZlID0gZnVuY3Rpb24gKHRhc2spIHtcbiAgICAgICAgdmFyIHJlbW92ZWRUYXNrID0gdGhpcy5fYnVmZmVyLnNwbGljZSh0aGlzLl9idWZmZXIuaW5kZXhPZih0YXNrKSwgMSlbMF07XG4gICAgICAgIHJldHVybiByZW1vdmVkVGFzaztcbiAgICB9O1xuICAgIC8qKlxuICAgICAqIFRoaXMgZnVuY3Rpb24gcmV0dXJucyB0aGUgbnVtYmVyIG9mIHVucmVzb2x2ZWQgcHJvbWlzZXMgaW4gdGhlIHF1ZXVlLlxuICAgICAqL1xuICAgIFByb21pc2VCdWZmZXIucHJvdG90eXBlLmxlbmd0aCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX2J1ZmZlci5sZW5ndGg7XG4gICAgfTtcbiAgICAvKipcbiAgICAgKiBUaGlzIHdpbGwgZHJhaW4gdGhlIHdob2xlIHF1ZXVlLCByZXR1cm5zIHRydWUgaWYgcXVldWUgaXMgZW1wdHkgb3IgZHJhaW5lZC5cbiAgICAgKiBJZiB0aW1lb3V0IGlzIHByb3ZpZGVkIGFuZCB0aGUgcXVldWUgdGFrZXMgbG9uZ2VyIHRvIGRyYWluLCB0aGUgcHJvbWlzZSBzdGlsbCByZXNvbHZlcyBidXQgd2l0aCBmYWxzZS5cbiAgICAgKlxuICAgICAqIEBwYXJhbSB0aW1lb3V0IE51bWJlciBpbiBtcyB0byB3YWl0IHVudGlsIGl0IHJlc29sdmVzIHdpdGggZmFsc2UuXG4gICAgICovXG4gICAgUHJvbWlzZUJ1ZmZlci5wcm90b3R5cGUuZHJhaW4gPSBmdW5jdGlvbiAodGltZW91dCkge1xuICAgICAgICB2YXIgX3RoaXMgPSB0aGlzO1xuICAgICAgICByZXR1cm4gbmV3IFN5bmNQcm9taXNlKGZ1bmN0aW9uIChyZXNvbHZlKSB7XG4gICAgICAgICAgICB2YXIgY2FwdHVyZWRTZXRUaW1lb3V0ID0gc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgaWYgKHRpbWVvdXQgJiYgdGltZW91dCA+IDApIHtcbiAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZShmYWxzZSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSwgdGltZW91dCk7XG4gICAgICAgICAgICBTeW5jUHJvbWlzZS5hbGwoX3RoaXMuX2J1ZmZlcilcbiAgICAgICAgICAgICAgICAudGhlbihmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgY2xlYXJUaW1lb3V0KGNhcHR1cmVkU2V0VGltZW91dCk7XG4gICAgICAgICAgICAgICAgcmVzb2x2ZSh0cnVlKTtcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgLnRoZW4obnVsbCwgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIHJlc29sdmUodHJ1ZSk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgfTtcbiAgICByZXR1cm4gUHJvbWlzZUJ1ZmZlcjtcbn0oKSk7XG5leHBvcnQgeyBQcm9taXNlQnVmZmVyIH07XG4vLyMgc291cmNlTWFwcGluZ1VSTD1wcm9taXNlYnVmZmVyLmpzLm1hcCIsImltcG9ydCB7IGlzUmVnRXhwLCBpc1N0cmluZyB9IGZyb20gJy4vaXMnO1xuLyoqXG4gKiBUcnVuY2F0ZXMgZ2l2ZW4gc3RyaW5nIHRvIHRoZSBtYXhpbXVtIGNoYXJhY3RlcnMgY291bnRcbiAqXG4gKiBAcGFyYW0gc3RyIEFuIG9iamVjdCB0aGF0IGNvbnRhaW5zIHNlcmlhbGl6YWJsZSB2YWx1ZXNcbiAqIEBwYXJhbSBtYXggTWF4aW11bSBudW1iZXIgb2YgY2hhcmFjdGVycyBpbiB0cnVuY2F0ZWQgc3RyaW5nXG4gKiBAcmV0dXJucyBzdHJpbmcgRW5jb2RlZFxuICovXG5leHBvcnQgZnVuY3Rpb24gdHJ1bmNhdGUoc3RyLCBtYXgpIHtcbiAgICBpZiAobWF4ID09PSB2b2lkIDApIHsgbWF4ID0gMDsgfVxuICAgIC8vIHRzbGludDpkaXNhYmxlLW5leHQtbGluZTpzdHJpY3QtdHlwZS1wcmVkaWNhdGVzXG4gICAgaWYgKHR5cGVvZiBzdHIgIT09ICdzdHJpbmcnIHx8IG1heCA9PT0gMCkge1xuICAgICAgICByZXR1cm4gc3RyO1xuICAgIH1cbiAgICByZXR1cm4gc3RyLmxlbmd0aCA8PSBtYXggPyBzdHIgOiBzdHIuc3Vic3RyKDAsIG1heCkgKyBcIi4uLlwiO1xufVxuLyoqXG4gKiBUaGlzIGlzIGJhc2ljYWxseSBqdXN0IGB0cmltX2xpbmVgIGZyb21cbiAqIGh0dHBzOi8vZ2l0aHViLmNvbS9nZXRzZW50cnkvc2VudHJ5L2Jsb2IvbWFzdGVyL3NyYy9zZW50cnkvbGFuZy9qYXZhc2NyaXB0L3Byb2Nlc3Nvci5weSNMNjdcbiAqXG4gKiBAcGFyYW0gc3RyIEFuIG9iamVjdCB0aGF0IGNvbnRhaW5zIHNlcmlhbGl6YWJsZSB2YWx1ZXNcbiAqIEBwYXJhbSBtYXggTWF4aW11bSBudW1iZXIgb2YgY2hhcmFjdGVycyBpbiB0cnVuY2F0ZWQgc3RyaW5nXG4gKiBAcmV0dXJucyBzdHJpbmcgRW5jb2RlZFxuICovXG5leHBvcnQgZnVuY3Rpb24gc25pcExpbmUobGluZSwgY29sbm8pIHtcbiAgICB2YXIgbmV3TGluZSA9IGxpbmU7XG4gICAgdmFyIGxsID0gbmV3TGluZS5sZW5ndGg7XG4gICAgaWYgKGxsIDw9IDE1MCkge1xuICAgICAgICByZXR1cm4gbmV3TGluZTtcbiAgICB9XG4gICAgaWYgKGNvbG5vID4gbGwpIHtcbiAgICAgICAgY29sbm8gPSBsbDsgLy8gdHNsaW50OmRpc2FibGUtbGluZTpuby1wYXJhbWV0ZXItcmVhc3NpZ25tZW50XG4gICAgfVxuICAgIHZhciBzdGFydCA9IE1hdGgubWF4KGNvbG5vIC0gNjAsIDApO1xuICAgIGlmIChzdGFydCA8IDUpIHtcbiAgICAgICAgc3RhcnQgPSAwO1xuICAgIH1cbiAgICB2YXIgZW5kID0gTWF0aC5taW4oc3RhcnQgKyAxNDAsIGxsKTtcbiAgICBpZiAoZW5kID4gbGwgLSA1KSB7XG4gICAgICAgIGVuZCA9IGxsO1xuICAgIH1cbiAgICBpZiAoZW5kID09PSBsbCkge1xuICAgICAgICBzdGFydCA9IE1hdGgubWF4KGVuZCAtIDE0MCwgMCk7XG4gICAgfVxuICAgIG5ld0xpbmUgPSBuZXdMaW5lLnNsaWNlKHN0YXJ0LCBlbmQpO1xuICAgIGlmIChzdGFydCA+IDApIHtcbiAgICAgICAgbmV3TGluZSA9IFwiJ3tzbmlwfSBcIiArIG5ld0xpbmU7XG4gICAgfVxuICAgIGlmIChlbmQgPCBsbCkge1xuICAgICAgICBuZXdMaW5lICs9ICcge3NuaXB9JztcbiAgICB9XG4gICAgcmV0dXJuIG5ld0xpbmU7XG59XG4vKipcbiAqIEpvaW4gdmFsdWVzIGluIGFycmF5XG4gKiBAcGFyYW0gaW5wdXQgYXJyYXkgb2YgdmFsdWVzIHRvIGJlIGpvaW5lZCB0b2dldGhlclxuICogQHBhcmFtIGRlbGltaXRlciBzdHJpbmcgdG8gYmUgcGxhY2VkIGluLWJldHdlZW4gdmFsdWVzXG4gKiBAcmV0dXJucyBKb2luZWQgdmFsdWVzXG4gKi9cbmV4cG9ydCBmdW5jdGlvbiBzYWZlSm9pbihpbnB1dCwgZGVsaW1pdGVyKSB7XG4gICAgaWYgKCFBcnJheS5pc0FycmF5KGlucHV0KSkge1xuICAgICAgICByZXR1cm4gJyc7XG4gICAgfVxuICAgIHZhciBvdXRwdXQgPSBbXTtcbiAgICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6cHJlZmVyLWZvci1vZlxuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgaW5wdXQubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgdmFyIHZhbHVlID0gaW5wdXRbaV07XG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICBvdXRwdXQucHVzaChTdHJpbmcodmFsdWUpKTtcbiAgICAgICAgfVxuICAgICAgICBjYXRjaCAoZSkge1xuICAgICAgICAgICAgb3V0cHV0LnB1c2goJ1t2YWx1ZSBjYW5ub3QgYmUgc2VyaWFsaXplZF0nKTtcbiAgICAgICAgfVxuICAgIH1cbiAgICByZXR1cm4gb3V0cHV0LmpvaW4oZGVsaW1pdGVyKTtcbn1cbi8qKlxuICogQ2hlY2tzIGlmIHRoZSB2YWx1ZSBtYXRjaGVzIGEgcmVnZXggb3IgaW5jbHVkZXMgdGhlIHN0cmluZ1xuICogQHBhcmFtIHZhbHVlIFRoZSBzdHJpbmcgdmFsdWUgdG8gYmUgY2hlY2tlZCBhZ2FpbnN0XG4gKiBAcGFyYW0gcGF0dGVybiBFaXRoZXIgYSByZWdleCBvciBhIHN0cmluZyB0aGF0IG11c3QgYmUgY29udGFpbmVkIGluIHZhbHVlXG4gKi9cbmV4cG9ydCBmdW5jdGlvbiBpc01hdGNoaW5nUGF0dGVybih2YWx1ZSwgcGF0dGVybikge1xuICAgIGlmICghaXNTdHJpbmcodmFsdWUpKSB7XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG4gICAgaWYgKGlzUmVnRXhwKHBhdHRlcm4pKSB7XG4gICAgICAgIHJldHVybiBwYXR0ZXJuLnRlc3QodmFsdWUpO1xuICAgIH1cbiAgICBpZiAodHlwZW9mIHBhdHRlcm4gPT09ICdzdHJpbmcnKSB7XG4gICAgICAgIHJldHVybiB2YWx1ZS5pbmRleE9mKHBhdHRlcm4pICE9PSAtMTtcbiAgICB9XG4gICAgcmV0dXJuIGZhbHNlO1xufVxuLy8jIHNvdXJjZU1hcHBpbmdVUkw9c3RyaW5nLmpzLm1hcCIsImltcG9ydCB7IGxvZ2dlciB9IGZyb20gJy4vbG9nZ2VyJztcbmltcG9ydCB7IGdldEdsb2JhbE9iamVjdCB9IGZyb20gJy4vbWlzYyc7XG4vKipcbiAqIFRlbGxzIHdoZXRoZXIgY3VycmVudCBlbnZpcm9ubWVudCBzdXBwb3J0cyBFcnJvckV2ZW50IG9iamVjdHNcbiAqIHtAbGluayBzdXBwb3J0c0Vycm9yRXZlbnR9LlxuICpcbiAqIEByZXR1cm5zIEFuc3dlciB0byB0aGUgZ2l2ZW4gcXVlc3Rpb24uXG4gKi9cbmV4cG9ydCBmdW5jdGlvbiBzdXBwb3J0c0Vycm9yRXZlbnQoKSB7XG4gICAgdHJ5IHtcbiAgICAgICAgLy8gdHNsaW50OmRpc2FibGU6bm8tdW51c2VkLWV4cHJlc3Npb25cbiAgICAgICAgbmV3IEVycm9yRXZlbnQoJycpO1xuICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICB9XG4gICAgY2F0Y2ggKGUpIHtcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cbn1cbi8qKlxuICogVGVsbHMgd2hldGhlciBjdXJyZW50IGVudmlyb25tZW50IHN1cHBvcnRzIERPTUVycm9yIG9iamVjdHNcbiAqIHtAbGluayBzdXBwb3J0c0RPTUVycm9yfS5cbiAqXG4gKiBAcmV0dXJucyBBbnN3ZXIgdG8gdGhlIGdpdmVuIHF1ZXN0aW9uLlxuICovXG5leHBvcnQgZnVuY3Rpb24gc3VwcG9ydHNET01FcnJvcigpIHtcbiAgICB0cnkge1xuICAgICAgICAvLyBJdCByZWFsbHkgbmVlZHMgMSBhcmd1bWVudCwgbm90IDAuXG4gICAgICAgIC8vIENocm9tZTogVk04OToxIFVuY2F1Z2h0IFR5cGVFcnJvcjogRmFpbGVkIHRvIGNvbnN0cnVjdCAnRE9NRXJyb3InOlxuICAgICAgICAvLyAxIGFyZ3VtZW50IHJlcXVpcmVkLCBidXQgb25seSAwIHByZXNlbnQuXG4gICAgICAgIC8vIEB0cy1pZ25vcmVcbiAgICAgICAgLy8gdHNsaW50OmRpc2FibGU6bm8tdW51c2VkLWV4cHJlc3Npb25cbiAgICAgICAgbmV3IERPTUVycm9yKCcnKTtcbiAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgfVxuICAgIGNhdGNoIChlKSB7XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG59XG4vKipcbiAqIFRlbGxzIHdoZXRoZXIgY3VycmVudCBlbnZpcm9ubWVudCBzdXBwb3J0cyBET01FeGNlcHRpb24gb2JqZWN0c1xuICoge0BsaW5rIHN1cHBvcnRzRE9NRXhjZXB0aW9ufS5cbiAqXG4gKiBAcmV0dXJucyBBbnN3ZXIgdG8gdGhlIGdpdmVuIHF1ZXN0aW9uLlxuICovXG5leHBvcnQgZnVuY3Rpb24gc3VwcG9ydHNET01FeGNlcHRpb24oKSB7XG4gICAgdHJ5IHtcbiAgICAgICAgLy8gdHNsaW50OmRpc2FibGU6bm8tdW51c2VkLWV4cHJlc3Npb25cbiAgICAgICAgbmV3IERPTUV4Y2VwdGlvbignJyk7XG4gICAgICAgIHJldHVybiB0cnVlO1xuICAgIH1cbiAgICBjYXRjaCAoZSkge1xuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxufVxuLyoqXG4gKiBUZWxscyB3aGV0aGVyIGN1cnJlbnQgZW52aXJvbm1lbnQgc3VwcG9ydHMgRmV0Y2ggQVBJXG4gKiB7QGxpbmsgc3VwcG9ydHNGZXRjaH0uXG4gKlxuICogQHJldHVybnMgQW5zd2VyIHRvIHRoZSBnaXZlbiBxdWVzdGlvbi5cbiAqL1xuZXhwb3J0IGZ1bmN0aW9uIHN1cHBvcnRzRmV0Y2goKSB7XG4gICAgaWYgKCEoJ2ZldGNoJyBpbiBnZXRHbG9iYWxPYmplY3QoKSkpIHtcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cbiAgICB0cnkge1xuICAgICAgICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6bm8tdW51c2VkLWV4cHJlc3Npb25cbiAgICAgICAgbmV3IEhlYWRlcnMoKTtcbiAgICAgICAgLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOm5vLXVudXNlZC1leHByZXNzaW9uXG4gICAgICAgIG5ldyBSZXF1ZXN0KCcnKTtcbiAgICAgICAgLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOm5vLXVudXNlZC1leHByZXNzaW9uXG4gICAgICAgIG5ldyBSZXNwb25zZSgpO1xuICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICB9XG4gICAgY2F0Y2ggKGUpIHtcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cbn1cbi8qKlxuICogaXNOYXRpdmVGZXRjaCBjaGVja3MgaWYgdGhlIGdpdmVuIGZ1bmN0aW9uIGlzIGEgbmF0aXZlIGltcGxlbWVudGF0aW9uIG9mIGZldGNoKClcbiAqL1xuZnVuY3Rpb24gaXNOYXRpdmVGZXRjaChmdW5jKSB7XG4gICAgcmV0dXJuIGZ1bmMgJiYgL15mdW5jdGlvbiBmZXRjaFxcKFxcKVxccytcXHtcXHMrXFxbbmF0aXZlIGNvZGVcXF1cXHMrXFx9JC8udGVzdChmdW5jLnRvU3RyaW5nKCkpO1xufVxuLyoqXG4gKiBUZWxscyB3aGV0aGVyIGN1cnJlbnQgZW52aXJvbm1lbnQgc3VwcG9ydHMgRmV0Y2ggQVBJIG5hdGl2ZWx5XG4gKiB7QGxpbmsgc3VwcG9ydHNOYXRpdmVGZXRjaH0uXG4gKlxuICogQHJldHVybnMgdHJ1ZSBpZiBgd2luZG93LmZldGNoYCBpcyBuYXRpdmVseSBpbXBsZW1lbnRlZCwgZmFsc2Ugb3RoZXJ3aXNlXG4gKi9cbmV4cG9ydCBmdW5jdGlvbiBzdXBwb3J0c05hdGl2ZUZldGNoKCkge1xuICAgIGlmICghc3VwcG9ydHNGZXRjaCgpKSB7XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG4gICAgdmFyIGdsb2JhbCA9IGdldEdsb2JhbE9iamVjdCgpO1xuICAgIC8vIEZhc3QgcGF0aCB0byBhdm9pZCBET00gSS9PXG4gICAgLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOm5vLXVuYm91bmQtbWV0aG9kXG4gICAgaWYgKGlzTmF0aXZlRmV0Y2goZ2xvYmFsLmZldGNoKSkge1xuICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICB9XG4gICAgLy8gd2luZG93LmZldGNoIGlzIGltcGxlbWVudGVkLCBidXQgaXMgcG9seWZpbGxlZCBvciBhbHJlYWR5IHdyYXBwZWQgKGUuZzogYnkgYSBjaHJvbWUgZXh0ZW5zaW9uKVxuICAgIC8vIHNvIGNyZWF0ZSBhIFwicHVyZVwiIGlmcmFtZSB0byBzZWUgaWYgdGhhdCBoYXMgbmF0aXZlIGZldGNoXG4gICAgdmFyIHJlc3VsdCA9IGZhbHNlO1xuICAgIHZhciBkb2MgPSBnbG9iYWwuZG9jdW1lbnQ7XG4gICAgLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOm5vLXVuYm91bmQtbWV0aG9kIGRlcHJlY2F0aW9uXG4gICAgaWYgKGRvYyAmJiB0eXBlb2YgZG9jLmNyZWF0ZUVsZW1lbnQgPT09IFwiZnVuY3Rpb25cIikge1xuICAgICAgICB0cnkge1xuICAgICAgICAgICAgdmFyIHNhbmRib3ggPSBkb2MuY3JlYXRlRWxlbWVudCgnaWZyYW1lJyk7XG4gICAgICAgICAgICBzYW5kYm94LmhpZGRlbiA9IHRydWU7XG4gICAgICAgICAgICBkb2MuaGVhZC5hcHBlbmRDaGlsZChzYW5kYm94KTtcbiAgICAgICAgICAgIGlmIChzYW5kYm94LmNvbnRlbnRXaW5kb3cgJiYgc2FuZGJveC5jb250ZW50V2luZG93LmZldGNoKSB7XG4gICAgICAgICAgICAgICAgLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOm5vLXVuYm91bmQtbWV0aG9kXG4gICAgICAgICAgICAgICAgcmVzdWx0ID0gaXNOYXRpdmVGZXRjaChzYW5kYm94LmNvbnRlbnRXaW5kb3cuZmV0Y2gpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZG9jLmhlYWQucmVtb3ZlQ2hpbGQoc2FuZGJveCk7XG4gICAgICAgIH1cbiAgICAgICAgY2F0Y2ggKGVycikge1xuICAgICAgICAgICAgbG9nZ2VyLndhcm4oJ0NvdWxkIG5vdCBjcmVhdGUgc2FuZGJveCBpZnJhbWUgZm9yIHB1cmUgZmV0Y2ggY2hlY2ssIGJhaWxpbmcgdG8gd2luZG93LmZldGNoOiAnLCBlcnIpO1xuICAgICAgICB9XG4gICAgfVxuICAgIHJldHVybiByZXN1bHQ7XG59XG4vKipcbiAqIFRlbGxzIHdoZXRoZXIgY3VycmVudCBlbnZpcm9ubWVudCBzdXBwb3J0cyBSZXBvcnRpbmdPYnNlcnZlciBBUElcbiAqIHtAbGluayBzdXBwb3J0c1JlcG9ydGluZ09ic2VydmVyfS5cbiAqXG4gKiBAcmV0dXJucyBBbnN3ZXIgdG8gdGhlIGdpdmVuIHF1ZXN0aW9uLlxuICovXG5leHBvcnQgZnVuY3Rpb24gc3VwcG9ydHNSZXBvcnRpbmdPYnNlcnZlcigpIHtcbiAgICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6IG5vLXVuc2FmZS1hbnlcbiAgICByZXR1cm4gJ1JlcG9ydGluZ09ic2VydmVyJyBpbiBnZXRHbG9iYWxPYmplY3QoKTtcbn1cbi8qKlxuICogVGVsbHMgd2hldGhlciBjdXJyZW50IGVudmlyb25tZW50IHN1cHBvcnRzIFJlZmVycmVyIFBvbGljeSBBUElcbiAqIHtAbGluayBzdXBwb3J0c1JlZmVycmVyUG9saWN5fS5cbiAqXG4gKiBAcmV0dXJucyBBbnN3ZXIgdG8gdGhlIGdpdmVuIHF1ZXN0aW9uLlxuICovXG5leHBvcnQgZnVuY3Rpb24gc3VwcG9ydHNSZWZlcnJlclBvbGljeSgpIHtcbiAgICAvLyBEZXNwaXRlIGFsbCBzdGFycyBpbiB0aGUgc2t5IHNheWluZyB0aGF0IEVkZ2Ugc3VwcG9ydHMgb2xkIGRyYWZ0IHN5bnRheCwgYWthICduZXZlcicsICdhbHdheXMnLCAnb3JpZ2luJyBhbmQgJ2RlZmF1bHRcbiAgICAvLyBodHRwczovL2Nhbml1c2UuY29tLyNmZWF0PXJlZmVycmVyLXBvbGljeVxuICAgIC8vIEl0IGRvZXNuJ3QuIEFuZCBpdCB0aHJvdyBleGNlcHRpb24gaW5zdGVhZCBvZiBpZ25vcmluZyB0aGlzIHBhcmFtZXRlci4uLlxuICAgIC8vIFJFRjogaHR0cHM6Ly9naXRodWIuY29tL2dldHNlbnRyeS9yYXZlbi1qcy9pc3N1ZXMvMTIzM1xuICAgIGlmICghc3VwcG9ydHNGZXRjaCgpKSB7XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG4gICAgdHJ5IHtcbiAgICAgICAgLy8gdHNsaW50OmRpc2FibGU6bm8tdW51c2VkLWV4cHJlc3Npb25cbiAgICAgICAgbmV3IFJlcXVlc3QoJ18nLCB7XG4gICAgICAgICAgICByZWZlcnJlclBvbGljeTogJ29yaWdpbicsXG4gICAgICAgIH0pO1xuICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICB9XG4gICAgY2F0Y2ggKGUpIHtcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cbn1cbi8qKlxuICogVGVsbHMgd2hldGhlciBjdXJyZW50IGVudmlyb25tZW50IHN1cHBvcnRzIEhpc3RvcnkgQVBJXG4gKiB7QGxpbmsgc3VwcG9ydHNIaXN0b3J5fS5cbiAqXG4gKiBAcmV0dXJucyBBbnN3ZXIgdG8gdGhlIGdpdmVuIHF1ZXN0aW9uLlxuICovXG5leHBvcnQgZnVuY3Rpb24gc3VwcG9ydHNIaXN0b3J5KCkge1xuICAgIC8vIE5PVEU6IGluIENocm9tZSBBcHAgZW52aXJvbm1lbnQsIHRvdWNoaW5nIGhpc3RvcnkucHVzaFN0YXRlLCAqZXZlbiBpbnNpZGVcbiAgICAvLyAgICAgICBhIHRyeS9jYXRjaCBibG9jayosIHdpbGwgY2F1c2UgQ2hyb21lIHRvIG91dHB1dCBhbiBlcnJvciB0byBjb25zb2xlLmVycm9yXG4gICAgLy8gYm9ycm93ZWQgZnJvbTogaHR0cHM6Ly9naXRodWIuY29tL2FuZ3VsYXIvYW5ndWxhci5qcy9wdWxsLzEzOTQ1L2ZpbGVzXG4gICAgdmFyIGdsb2JhbCA9IGdldEdsb2JhbE9iamVjdCgpO1xuICAgIHZhciBjaHJvbWUgPSBnbG9iYWwuY2hyb21lO1xuICAgIC8vIHRzbGludDpkaXNhYmxlLW5leHQtbGluZTpuby11bnNhZmUtYW55XG4gICAgdmFyIGlzQ2hyb21lUGFja2FnZWRBcHAgPSBjaHJvbWUgJiYgY2hyb21lLmFwcCAmJiBjaHJvbWUuYXBwLnJ1bnRpbWU7XG4gICAgdmFyIGhhc0hpc3RvcnlBcGkgPSAnaGlzdG9yeScgaW4gZ2xvYmFsICYmICEhZ2xvYmFsLmhpc3RvcnkucHVzaFN0YXRlICYmICEhZ2xvYmFsLmhpc3RvcnkucmVwbGFjZVN0YXRlO1xuICAgIHJldHVybiAhaXNDaHJvbWVQYWNrYWdlZEFwcCAmJiBoYXNIaXN0b3J5QXBpO1xufVxuLy8jIHNvdXJjZU1hcHBpbmdVUkw9c3VwcG9ydHMuanMubWFwIiwiaW1wb3J0IHsgaXNUaGVuYWJsZSB9IGZyb20gJy4vaXMnO1xuLyoqIFN5bmNQcm9taXNlIGludGVybmFsIHN0YXRlcyAqL1xudmFyIFN0YXRlcztcbihmdW5jdGlvbiAoU3RhdGVzKSB7XG4gICAgLyoqIFBlbmRpbmcgKi9cbiAgICBTdGF0ZXNbXCJQRU5ESU5HXCJdID0gXCJQRU5ESU5HXCI7XG4gICAgLyoqIFJlc29sdmVkIC8gT0sgKi9cbiAgICBTdGF0ZXNbXCJSRVNPTFZFRFwiXSA9IFwiUkVTT0xWRURcIjtcbiAgICAvKiogUmVqZWN0ZWQgLyBFcnJvciAqL1xuICAgIFN0YXRlc1tcIlJFSkVDVEVEXCJdID0gXCJSRUpFQ1RFRFwiO1xufSkoU3RhdGVzIHx8IChTdGF0ZXMgPSB7fSkpO1xuLyoqXG4gKiBUaGVuYWJsZSBjbGFzcyB0aGF0IGJlaGF2ZXMgbGlrZSBhIFByb21pc2UgYW5kIGZvbGxvd3MgaXQncyBpbnRlcmZhY2VcbiAqIGJ1dCBpcyBub3QgYXN5bmMgaW50ZXJuYWxseVxuICovXG52YXIgU3luY1Byb21pc2UgPSAvKiogQGNsYXNzICovIChmdW5jdGlvbiAoKSB7XG4gICAgZnVuY3Rpb24gU3luY1Byb21pc2UoZXhlY3V0b3IpIHtcbiAgICAgICAgdmFyIF90aGlzID0gdGhpcztcbiAgICAgICAgdGhpcy5fc3RhdGUgPSBTdGF0ZXMuUEVORElORztcbiAgICAgICAgdGhpcy5faGFuZGxlcnMgPSBbXTtcbiAgICAgICAgLyoqIEpTRG9jICovXG4gICAgICAgIHRoaXMuX3Jlc29sdmUgPSBmdW5jdGlvbiAodmFsdWUpIHtcbiAgICAgICAgICAgIF90aGlzLl9zZXRSZXN1bHQoU3RhdGVzLlJFU09MVkVELCB2YWx1ZSk7XG4gICAgICAgIH07XG4gICAgICAgIC8qKiBKU0RvYyAqL1xuICAgICAgICB0aGlzLl9yZWplY3QgPSBmdW5jdGlvbiAocmVhc29uKSB7XG4gICAgICAgICAgICBfdGhpcy5fc2V0UmVzdWx0KFN0YXRlcy5SRUpFQ1RFRCwgcmVhc29uKTtcbiAgICAgICAgfTtcbiAgICAgICAgLyoqIEpTRG9jICovXG4gICAgICAgIHRoaXMuX3NldFJlc3VsdCA9IGZ1bmN0aW9uIChzdGF0ZSwgdmFsdWUpIHtcbiAgICAgICAgICAgIGlmIChfdGhpcy5fc3RhdGUgIT09IFN0YXRlcy5QRU5ESU5HKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKGlzVGhlbmFibGUodmFsdWUpKSB7XG4gICAgICAgICAgICAgICAgdmFsdWUudGhlbihfdGhpcy5fcmVzb2x2ZSwgX3RoaXMuX3JlamVjdCk7XG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgX3RoaXMuX3N0YXRlID0gc3RhdGU7XG4gICAgICAgICAgICBfdGhpcy5fdmFsdWUgPSB2YWx1ZTtcbiAgICAgICAgICAgIF90aGlzLl9leGVjdXRlSGFuZGxlcnMoKTtcbiAgICAgICAgfTtcbiAgICAgICAgLy8gVE9ETzogRklYTUVcbiAgICAgICAgLyoqIEpTRG9jICovXG4gICAgICAgIHRoaXMuX2F0dGFjaEhhbmRsZXIgPSBmdW5jdGlvbiAoaGFuZGxlcikge1xuICAgICAgICAgICAgX3RoaXMuX2hhbmRsZXJzID0gX3RoaXMuX2hhbmRsZXJzLmNvbmNhdChoYW5kbGVyKTtcbiAgICAgICAgICAgIF90aGlzLl9leGVjdXRlSGFuZGxlcnMoKTtcbiAgICAgICAgfTtcbiAgICAgICAgLyoqIEpTRG9jICovXG4gICAgICAgIHRoaXMuX2V4ZWN1dGVIYW5kbGVycyA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIGlmIChfdGhpcy5fc3RhdGUgPT09IFN0YXRlcy5QRU5ESU5HKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdmFyIGNhY2hlZEhhbmRsZXJzID0gX3RoaXMuX2hhbmRsZXJzLnNsaWNlKCk7XG4gICAgICAgICAgICBfdGhpcy5faGFuZGxlcnMgPSBbXTtcbiAgICAgICAgICAgIGNhY2hlZEhhbmRsZXJzLmZvckVhY2goZnVuY3Rpb24gKGhhbmRsZXIpIHtcbiAgICAgICAgICAgICAgICBpZiAoaGFuZGxlci5kb25lKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgaWYgKF90aGlzLl9zdGF0ZSA9PT0gU3RhdGVzLlJFU09MVkVEKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmIChoYW5kbGVyLm9uZnVsZmlsbGVkKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBoYW5kbGVyLm9uZnVsZmlsbGVkKF90aGlzLl92YWx1ZSk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgaWYgKF90aGlzLl9zdGF0ZSA9PT0gU3RhdGVzLlJFSkVDVEVEKSB7XG4gICAgICAgICAgICAgICAgICAgIGlmIChoYW5kbGVyLm9ucmVqZWN0ZWQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGhhbmRsZXIub25yZWplY3RlZChfdGhpcy5fdmFsdWUpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGhhbmRsZXIuZG9uZSA9IHRydWU7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfTtcbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIGV4ZWN1dG9yKHRoaXMuX3Jlc29sdmUsIHRoaXMuX3JlamVjdCk7XG4gICAgICAgIH1cbiAgICAgICAgY2F0Y2ggKGUpIHtcbiAgICAgICAgICAgIHRoaXMuX3JlamVjdChlKTtcbiAgICAgICAgfVxuICAgIH1cbiAgICAvKiogSlNEb2MgKi9cbiAgICBTeW5jUHJvbWlzZS5wcm90b3R5cGUudG9TdHJpbmcgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHJldHVybiAnW29iamVjdCBTeW5jUHJvbWlzZV0nO1xuICAgIH07XG4gICAgLyoqIEpTRG9jICovXG4gICAgU3luY1Byb21pc2UucmVzb2x2ZSA9IGZ1bmN0aW9uICh2YWx1ZSkge1xuICAgICAgICByZXR1cm4gbmV3IFN5bmNQcm9taXNlKGZ1bmN0aW9uIChyZXNvbHZlKSB7XG4gICAgICAgICAgICByZXNvbHZlKHZhbHVlKTtcbiAgICAgICAgfSk7XG4gICAgfTtcbiAgICAvKiogSlNEb2MgKi9cbiAgICBTeW5jUHJvbWlzZS5yZWplY3QgPSBmdW5jdGlvbiAocmVhc29uKSB7XG4gICAgICAgIHJldHVybiBuZXcgU3luY1Byb21pc2UoZnVuY3Rpb24gKF8sIHJlamVjdCkge1xuICAgICAgICAgICAgcmVqZWN0KHJlYXNvbik7XG4gICAgICAgIH0pO1xuICAgIH07XG4gICAgLyoqIEpTRG9jICovXG4gICAgU3luY1Byb21pc2UuYWxsID0gZnVuY3Rpb24gKGNvbGxlY3Rpb24pIHtcbiAgICAgICAgcmV0dXJuIG5ldyBTeW5jUHJvbWlzZShmdW5jdGlvbiAocmVzb2x2ZSwgcmVqZWN0KSB7XG4gICAgICAgICAgICBpZiAoIUFycmF5LmlzQXJyYXkoY29sbGVjdGlvbikpIHtcbiAgICAgICAgICAgICAgICByZWplY3QobmV3IFR5cGVFcnJvcihcIlByb21pc2UuYWxsIHJlcXVpcmVzIGFuIGFycmF5IGFzIGlucHV0LlwiKSk7XG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKGNvbGxlY3Rpb24ubGVuZ3RoID09PSAwKSB7XG4gICAgICAgICAgICAgICAgcmVzb2x2ZShbXSk7XG4gICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdmFyIGNvdW50ZXIgPSBjb2xsZWN0aW9uLmxlbmd0aDtcbiAgICAgICAgICAgIHZhciByZXNvbHZlZENvbGxlY3Rpb24gPSBbXTtcbiAgICAgICAgICAgIGNvbGxlY3Rpb24uZm9yRWFjaChmdW5jdGlvbiAoaXRlbSwgaW5kZXgpIHtcbiAgICAgICAgICAgICAgICBTeW5jUHJvbWlzZS5yZXNvbHZlKGl0ZW0pXG4gICAgICAgICAgICAgICAgICAgIC50aGVuKGZ1bmN0aW9uICh2YWx1ZSkge1xuICAgICAgICAgICAgICAgICAgICByZXNvbHZlZENvbGxlY3Rpb25baW5kZXhdID0gdmFsdWU7XG4gICAgICAgICAgICAgICAgICAgIGNvdW50ZXIgLT0gMTtcbiAgICAgICAgICAgICAgICAgICAgaWYgKGNvdW50ZXIgIT09IDApIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICByZXNvbHZlKHJlc29sdmVkQ29sbGVjdGlvbik7XG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgICAgLnRoZW4obnVsbCwgcmVqZWN0KTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICB9O1xuICAgIC8qKiBKU0RvYyAqL1xuICAgIFN5bmNQcm9taXNlLnByb3RvdHlwZS50aGVuID0gZnVuY3Rpb24gKG9uZnVsZmlsbGVkLCBvbnJlamVjdGVkKSB7XG4gICAgICAgIHZhciBfdGhpcyA9IHRoaXM7XG4gICAgICAgIHJldHVybiBuZXcgU3luY1Byb21pc2UoZnVuY3Rpb24gKHJlc29sdmUsIHJlamVjdCkge1xuICAgICAgICAgICAgX3RoaXMuX2F0dGFjaEhhbmRsZXIoe1xuICAgICAgICAgICAgICAgIGRvbmU6IGZhbHNlLFxuICAgICAgICAgICAgICAgIG9uZnVsZmlsbGVkOiBmdW5jdGlvbiAocmVzdWx0KSB7XG4gICAgICAgICAgICAgICAgICAgIGlmICghb25mdWxmaWxsZWQpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIFRPRE86IMKvXFxfKOODhClfL8KvXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBUT0RPOiBGSVhNRVxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZShyZXN1bHQpO1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIHRyeSB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXNvbHZlKG9uZnVsZmlsbGVkKHJlc3VsdCkpO1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGNhdGNoIChlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZWplY3QoZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIG9ucmVqZWN0ZWQ6IGZ1bmN0aW9uIChyZWFzb24pIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKCFvbnJlamVjdGVkKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZWplY3QocmVhc29uKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB0cnkge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZShvbnJlamVjdGVkKHJlYXNvbikpO1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGNhdGNoIChlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZWplY3QoZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgIH07XG4gICAgLyoqIEpTRG9jICovXG4gICAgU3luY1Byb21pc2UucHJvdG90eXBlLmNhdGNoID0gZnVuY3Rpb24gKG9ucmVqZWN0ZWQpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMudGhlbihmdW5jdGlvbiAodmFsKSB7IHJldHVybiB2YWw7IH0sIG9ucmVqZWN0ZWQpO1xuICAgIH07XG4gICAgLyoqIEpTRG9jICovXG4gICAgU3luY1Byb21pc2UucHJvdG90eXBlLmZpbmFsbHkgPSBmdW5jdGlvbiAob25maW5hbGx5KSB7XG4gICAgICAgIHZhciBfdGhpcyA9IHRoaXM7XG4gICAgICAgIHJldHVybiBuZXcgU3luY1Byb21pc2UoZnVuY3Rpb24gKHJlc29sdmUsIHJlamVjdCkge1xuICAgICAgICAgICAgdmFyIHZhbDtcbiAgICAgICAgICAgIHZhciBpc1JlamVjdGVkO1xuICAgICAgICAgICAgcmV0dXJuIF90aGlzLnRoZW4oZnVuY3Rpb24gKHZhbHVlKSB7XG4gICAgICAgICAgICAgICAgaXNSZWplY3RlZCA9IGZhbHNlO1xuICAgICAgICAgICAgICAgIHZhbCA9IHZhbHVlO1xuICAgICAgICAgICAgICAgIGlmIChvbmZpbmFsbHkpIHtcbiAgICAgICAgICAgICAgICAgICAgb25maW5hbGx5KCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSwgZnVuY3Rpb24gKHJlYXNvbikge1xuICAgICAgICAgICAgICAgIGlzUmVqZWN0ZWQgPSB0cnVlO1xuICAgICAgICAgICAgICAgIHZhbCA9IHJlYXNvbjtcbiAgICAgICAgICAgICAgICBpZiAob25maW5hbGx5KSB7XG4gICAgICAgICAgICAgICAgICAgIG9uZmluYWxseSgpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pLnRoZW4oZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIGlmIChpc1JlamVjdGVkKSB7XG4gICAgICAgICAgICAgICAgICAgIHJlamVjdCh2YWwpO1xuICAgICAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIHJlc29sdmUodmFsKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICB9O1xuICAgIHJldHVybiBTeW5jUHJvbWlzZTtcbn0oKSk7XG5leHBvcnQgeyBTeW5jUHJvbWlzZSB9O1xuLy8jIHNvdXJjZU1hcHBpbmdVUkw9c3luY3Byb21pc2UuanMubWFwIiwiLy8gQ29weXJpZ2h0IEpveWVudCwgSW5jLiBhbmQgb3RoZXIgTm9kZSBjb250cmlidXRvcnMuXG4vL1xuLy8gUGVybWlzc2lvbiBpcyBoZXJlYnkgZ3JhbnRlZCwgZnJlZSBvZiBjaGFyZ2UsIHRvIGFueSBwZXJzb24gb2J0YWluaW5nIGFcbi8vIGNvcHkgb2YgdGhpcyBzb2Z0d2FyZSBhbmQgYXNzb2NpYXRlZCBkb2N1bWVudGF0aW9uIGZpbGVzICh0aGVcbi8vIFwiU29mdHdhcmVcIiksIHRvIGRlYWwgaW4gdGhlIFNvZnR3YXJlIHdpdGhvdXQgcmVzdHJpY3Rpb24sIGluY2x1ZGluZ1xuLy8gd2l0aG91dCBsaW1pdGF0aW9uIHRoZSByaWdodHMgdG8gdXNlLCBjb3B5LCBtb2RpZnksIG1lcmdlLCBwdWJsaXNoLFxuLy8gZGlzdHJpYnV0ZSwgc3VibGljZW5zZSwgYW5kL29yIHNlbGwgY29waWVzIG9mIHRoZSBTb2Z0d2FyZSwgYW5kIHRvIHBlcm1pdFxuLy8gcGVyc29ucyB0byB3aG9tIHRoZSBTb2Z0d2FyZSBpcyBmdXJuaXNoZWQgdG8gZG8gc28sIHN1YmplY3QgdG8gdGhlXG4vLyBmb2xsb3dpbmcgY29uZGl0aW9uczpcbi8vXG4vLyBUaGUgYWJvdmUgY29weXJpZ2h0IG5vdGljZSBhbmQgdGhpcyBwZXJtaXNzaW9uIG5vdGljZSBzaGFsbCBiZSBpbmNsdWRlZFxuLy8gaW4gYWxsIGNvcGllcyBvciBzdWJzdGFudGlhbCBwb3J0aW9ucyBvZiB0aGUgU29mdHdhcmUuXG4vL1xuLy8gVEhFIFNPRlRXQVJFIElTIFBST1ZJREVEIFwiQVMgSVNcIiwgV0lUSE9VVCBXQVJSQU5UWSBPRiBBTlkgS0lORCwgRVhQUkVTU1xuLy8gT1IgSU1QTElFRCwgSU5DTFVESU5HIEJVVCBOT1QgTElNSVRFRCBUTyBUSEUgV0FSUkFOVElFUyBPRlxuLy8gTUVSQ0hBTlRBQklMSVRZLCBGSVRORVNTIEZPUiBBIFBBUlRJQ1VMQVIgUFVSUE9TRSBBTkQgTk9OSU5GUklOR0VNRU5ULiBJTlxuLy8gTk8gRVZFTlQgU0hBTEwgVEhFIEFVVEhPUlMgT1IgQ09QWVJJR0hUIEhPTERFUlMgQkUgTElBQkxFIEZPUiBBTlkgQ0xBSU0sXG4vLyBEQU1BR0VTIE9SIE9USEVSIExJQUJJTElUWSwgV0hFVEhFUiBJTiBBTiBBQ1RJT04gT0YgQ09OVFJBQ1QsIFRPUlQgT1Jcbi8vIE9USEVSV0lTRSwgQVJJU0lORyBGUk9NLCBPVVQgT0YgT1IgSU4gQ09OTkVDVElPTiBXSVRIIFRIRSBTT0ZUV0FSRSBPUiBUSEVcbi8vIFVTRSBPUiBPVEhFUiBERUFMSU5HUyBJTiBUSEUgU09GVFdBUkUuXG5cbid1c2Ugc3RyaWN0JztcblxudmFyIFIgPSB0eXBlb2YgUmVmbGVjdCA9PT0gJ29iamVjdCcgPyBSZWZsZWN0IDogbnVsbFxudmFyIFJlZmxlY3RBcHBseSA9IFIgJiYgdHlwZW9mIFIuYXBwbHkgPT09ICdmdW5jdGlvbidcbiAgPyBSLmFwcGx5XG4gIDogZnVuY3Rpb24gUmVmbGVjdEFwcGx5KHRhcmdldCwgcmVjZWl2ZXIsIGFyZ3MpIHtcbiAgICByZXR1cm4gRnVuY3Rpb24ucHJvdG90eXBlLmFwcGx5LmNhbGwodGFyZ2V0LCByZWNlaXZlciwgYXJncyk7XG4gIH1cblxudmFyIFJlZmxlY3RPd25LZXlzXG5pZiAoUiAmJiB0eXBlb2YgUi5vd25LZXlzID09PSAnZnVuY3Rpb24nKSB7XG4gIFJlZmxlY3RPd25LZXlzID0gUi5vd25LZXlzXG59IGVsc2UgaWYgKE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMpIHtcbiAgUmVmbGVjdE93bktleXMgPSBmdW5jdGlvbiBSZWZsZWN0T3duS2V5cyh0YXJnZXQpIHtcbiAgICByZXR1cm4gT2JqZWN0LmdldE93blByb3BlcnR5TmFtZXModGFyZ2V0KVxuICAgICAgLmNvbmNhdChPYmplY3QuZ2V0T3duUHJvcGVydHlTeW1ib2xzKHRhcmdldCkpO1xuICB9O1xufSBlbHNlIHtcbiAgUmVmbGVjdE93bktleXMgPSBmdW5jdGlvbiBSZWZsZWN0T3duS2V5cyh0YXJnZXQpIHtcbiAgICByZXR1cm4gT2JqZWN0LmdldE93blByb3BlcnR5TmFtZXModGFyZ2V0KTtcbiAgfTtcbn1cblxuZnVuY3Rpb24gUHJvY2Vzc0VtaXRXYXJuaW5nKHdhcm5pbmcpIHtcbiAgaWYgKGNvbnNvbGUgJiYgY29uc29sZS53YXJuKSBjb25zb2xlLndhcm4od2FybmluZyk7XG59XG5cbnZhciBOdW1iZXJJc05hTiA9IE51bWJlci5pc05hTiB8fCBmdW5jdGlvbiBOdW1iZXJJc05hTih2YWx1ZSkge1xuICByZXR1cm4gdmFsdWUgIT09IHZhbHVlO1xufVxuXG5mdW5jdGlvbiBFdmVudEVtaXR0ZXIoKSB7XG4gIEV2ZW50RW1pdHRlci5pbml0LmNhbGwodGhpcyk7XG59XG5tb2R1bGUuZXhwb3J0cyA9IEV2ZW50RW1pdHRlcjtcblxuLy8gQmFja3dhcmRzLWNvbXBhdCB3aXRoIG5vZGUgMC4xMC54XG5FdmVudEVtaXR0ZXIuRXZlbnRFbWl0dGVyID0gRXZlbnRFbWl0dGVyO1xuXG5FdmVudEVtaXR0ZXIucHJvdG90eXBlLl9ldmVudHMgPSB1bmRlZmluZWQ7XG5FdmVudEVtaXR0ZXIucHJvdG90eXBlLl9ldmVudHNDb3VudCA9IDA7XG5FdmVudEVtaXR0ZXIucHJvdG90eXBlLl9tYXhMaXN0ZW5lcnMgPSB1bmRlZmluZWQ7XG5cbi8vIEJ5IGRlZmF1bHQgRXZlbnRFbWl0dGVycyB3aWxsIHByaW50IGEgd2FybmluZyBpZiBtb3JlIHRoYW4gMTAgbGlzdGVuZXJzIGFyZVxuLy8gYWRkZWQgdG8gaXQuIFRoaXMgaXMgYSB1c2VmdWwgZGVmYXVsdCB3aGljaCBoZWxwcyBmaW5kaW5nIG1lbW9yeSBsZWFrcy5cbnZhciBkZWZhdWx0TWF4TGlzdGVuZXJzID0gMTA7XG5cbmZ1bmN0aW9uIGNoZWNrTGlzdGVuZXIobGlzdGVuZXIpIHtcbiAgaWYgKHR5cGVvZiBsaXN0ZW5lciAhPT0gJ2Z1bmN0aW9uJykge1xuICAgIHRocm93IG5ldyBUeXBlRXJyb3IoJ1RoZSBcImxpc3RlbmVyXCIgYXJndW1lbnQgbXVzdCBiZSBvZiB0eXBlIEZ1bmN0aW9uLiBSZWNlaXZlZCB0eXBlICcgKyB0eXBlb2YgbGlzdGVuZXIpO1xuICB9XG59XG5cbk9iamVjdC5kZWZpbmVQcm9wZXJ0eShFdmVudEVtaXR0ZXIsICdkZWZhdWx0TWF4TGlzdGVuZXJzJywge1xuICBlbnVtZXJhYmxlOiB0cnVlLFxuICBnZXQ6IGZ1bmN0aW9uKCkge1xuICAgIHJldHVybiBkZWZhdWx0TWF4TGlzdGVuZXJzO1xuICB9LFxuICBzZXQ6IGZ1bmN0aW9uKGFyZykge1xuICAgIGlmICh0eXBlb2YgYXJnICE9PSAnbnVtYmVyJyB8fCBhcmcgPCAwIHx8IE51bWJlcklzTmFOKGFyZykpIHtcbiAgICAgIHRocm93IG5ldyBSYW5nZUVycm9yKCdUaGUgdmFsdWUgb2YgXCJkZWZhdWx0TWF4TGlzdGVuZXJzXCIgaXMgb3V0IG9mIHJhbmdlLiBJdCBtdXN0IGJlIGEgbm9uLW5lZ2F0aXZlIG51bWJlci4gUmVjZWl2ZWQgJyArIGFyZyArICcuJyk7XG4gICAgfVxuICAgIGRlZmF1bHRNYXhMaXN0ZW5lcnMgPSBhcmc7XG4gIH1cbn0pO1xuXG5FdmVudEVtaXR0ZXIuaW5pdCA9IGZ1bmN0aW9uKCkge1xuXG4gIGlmICh0aGlzLl9ldmVudHMgPT09IHVuZGVmaW5lZCB8fFxuICAgICAgdGhpcy5fZXZlbnRzID09PSBPYmplY3QuZ2V0UHJvdG90eXBlT2YodGhpcykuX2V2ZW50cykge1xuICAgIHRoaXMuX2V2ZW50cyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gICAgdGhpcy5fZXZlbnRzQ291bnQgPSAwO1xuICB9XG5cbiAgdGhpcy5fbWF4TGlzdGVuZXJzID0gdGhpcy5fbWF4TGlzdGVuZXJzIHx8IHVuZGVmaW5lZDtcbn07XG5cbi8vIE9idmlvdXNseSBub3QgYWxsIEVtaXR0ZXJzIHNob3VsZCBiZSBsaW1pdGVkIHRvIDEwLiBUaGlzIGZ1bmN0aW9uIGFsbG93c1xuLy8gdGhhdCB0byBiZSBpbmNyZWFzZWQuIFNldCB0byB6ZXJvIGZvciB1bmxpbWl0ZWQuXG5FdmVudEVtaXR0ZXIucHJvdG90eXBlLnNldE1heExpc3RlbmVycyA9IGZ1bmN0aW9uIHNldE1heExpc3RlbmVycyhuKSB7XG4gIGlmICh0eXBlb2YgbiAhPT0gJ251bWJlcicgfHwgbiA8IDAgfHwgTnVtYmVySXNOYU4obikpIHtcbiAgICB0aHJvdyBuZXcgUmFuZ2VFcnJvcignVGhlIHZhbHVlIG9mIFwiblwiIGlzIG91dCBvZiByYW5nZS4gSXQgbXVzdCBiZSBhIG5vbi1uZWdhdGl2ZSBudW1iZXIuIFJlY2VpdmVkICcgKyBuICsgJy4nKTtcbiAgfVxuICB0aGlzLl9tYXhMaXN0ZW5lcnMgPSBuO1xuICByZXR1cm4gdGhpcztcbn07XG5cbmZ1bmN0aW9uIF9nZXRNYXhMaXN0ZW5lcnModGhhdCkge1xuICBpZiAodGhhdC5fbWF4TGlzdGVuZXJzID09PSB1bmRlZmluZWQpXG4gICAgcmV0dXJuIEV2ZW50RW1pdHRlci5kZWZhdWx0TWF4TGlzdGVuZXJzO1xuICByZXR1cm4gdGhhdC5fbWF4TGlzdGVuZXJzO1xufVxuXG5FdmVudEVtaXR0ZXIucHJvdG90eXBlLmdldE1heExpc3RlbmVycyA9IGZ1bmN0aW9uIGdldE1heExpc3RlbmVycygpIHtcbiAgcmV0dXJuIF9nZXRNYXhMaXN0ZW5lcnModGhpcyk7XG59O1xuXG5FdmVudEVtaXR0ZXIucHJvdG90eXBlLmVtaXQgPSBmdW5jdGlvbiBlbWl0KHR5cGUpIHtcbiAgdmFyIGFyZ3MgPSBbXTtcbiAgZm9yICh2YXIgaSA9IDE7IGkgPCBhcmd1bWVudHMubGVuZ3RoOyBpKyspIGFyZ3MucHVzaChhcmd1bWVudHNbaV0pO1xuICB2YXIgZG9FcnJvciA9ICh0eXBlID09PSAnZXJyb3InKTtcblxuICB2YXIgZXZlbnRzID0gdGhpcy5fZXZlbnRzO1xuICBpZiAoZXZlbnRzICE9PSB1bmRlZmluZWQpXG4gICAgZG9FcnJvciA9IChkb0Vycm9yICYmIGV2ZW50cy5lcnJvciA9PT0gdW5kZWZpbmVkKTtcbiAgZWxzZSBpZiAoIWRvRXJyb3IpXG4gICAgcmV0dXJuIGZhbHNlO1xuXG4gIC8vIElmIHRoZXJlIGlzIG5vICdlcnJvcicgZXZlbnQgbGlzdGVuZXIgdGhlbiB0aHJvdy5cbiAgaWYgKGRvRXJyb3IpIHtcbiAgICB2YXIgZXI7XG4gICAgaWYgKGFyZ3MubGVuZ3RoID4gMClcbiAgICAgIGVyID0gYXJnc1swXTtcbiAgICBpZiAoZXIgaW5zdGFuY2VvZiBFcnJvcikge1xuICAgICAgLy8gTm90ZTogVGhlIGNvbW1lbnRzIG9uIHRoZSBgdGhyb3dgIGxpbmVzIGFyZSBpbnRlbnRpb25hbCwgdGhleSBzaG93XG4gICAgICAvLyB1cCBpbiBOb2RlJ3Mgb3V0cHV0IGlmIHRoaXMgcmVzdWx0cyBpbiBhbiB1bmhhbmRsZWQgZXhjZXB0aW9uLlxuICAgICAgdGhyb3cgZXI7IC8vIFVuaGFuZGxlZCAnZXJyb3InIGV2ZW50XG4gICAgfVxuICAgIC8vIEF0IGxlYXN0IGdpdmUgc29tZSBraW5kIG9mIGNvbnRleHQgdG8gdGhlIHVzZXJcbiAgICB2YXIgZXJyID0gbmV3IEVycm9yKCdVbmhhbmRsZWQgZXJyb3IuJyArIChlciA/ICcgKCcgKyBlci5tZXNzYWdlICsgJyknIDogJycpKTtcbiAgICBlcnIuY29udGV4dCA9IGVyO1xuICAgIHRocm93IGVycjsgLy8gVW5oYW5kbGVkICdlcnJvcicgZXZlbnRcbiAgfVxuXG4gIHZhciBoYW5kbGVyID0gZXZlbnRzW3R5cGVdO1xuXG4gIGlmIChoYW5kbGVyID09PSB1bmRlZmluZWQpXG4gICAgcmV0dXJuIGZhbHNlO1xuXG4gIGlmICh0eXBlb2YgaGFuZGxlciA9PT0gJ2Z1bmN0aW9uJykge1xuICAgIFJlZmxlY3RBcHBseShoYW5kbGVyLCB0aGlzLCBhcmdzKTtcbiAgfSBlbHNlIHtcbiAgICB2YXIgbGVuID0gaGFuZGxlci5sZW5ndGg7XG4gICAgdmFyIGxpc3RlbmVycyA9IGFycmF5Q2xvbmUoaGFuZGxlciwgbGVuKTtcbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IGxlbjsgKytpKVxuICAgICAgUmVmbGVjdEFwcGx5KGxpc3RlbmVyc1tpXSwgdGhpcywgYXJncyk7XG4gIH1cblxuICByZXR1cm4gdHJ1ZTtcbn07XG5cbmZ1bmN0aW9uIF9hZGRMaXN0ZW5lcih0YXJnZXQsIHR5cGUsIGxpc3RlbmVyLCBwcmVwZW5kKSB7XG4gIHZhciBtO1xuICB2YXIgZXZlbnRzO1xuICB2YXIgZXhpc3Rpbmc7XG5cbiAgY2hlY2tMaXN0ZW5lcihsaXN0ZW5lcik7XG5cbiAgZXZlbnRzID0gdGFyZ2V0Ll9ldmVudHM7XG4gIGlmIChldmVudHMgPT09IHVuZGVmaW5lZCkge1xuICAgIGV2ZW50cyA9IHRhcmdldC5fZXZlbnRzID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiAgICB0YXJnZXQuX2V2ZW50c0NvdW50ID0gMDtcbiAgfSBlbHNlIHtcbiAgICAvLyBUbyBhdm9pZCByZWN1cnNpb24gaW4gdGhlIGNhc2UgdGhhdCB0eXBlID09PSBcIm5ld0xpc3RlbmVyXCIhIEJlZm9yZVxuICAgIC8vIGFkZGluZyBpdCB0byB0aGUgbGlzdGVuZXJzLCBmaXJzdCBlbWl0IFwibmV3TGlzdGVuZXJcIi5cbiAgICBpZiAoZXZlbnRzLm5ld0xpc3RlbmVyICE9PSB1bmRlZmluZWQpIHtcbiAgICAgIHRhcmdldC5lbWl0KCduZXdMaXN0ZW5lcicsIHR5cGUsXG4gICAgICAgICAgICAgICAgICBsaXN0ZW5lci5saXN0ZW5lciA/IGxpc3RlbmVyLmxpc3RlbmVyIDogbGlzdGVuZXIpO1xuXG4gICAgICAvLyBSZS1hc3NpZ24gYGV2ZW50c2AgYmVjYXVzZSBhIG5ld0xpc3RlbmVyIGhhbmRsZXIgY291bGQgaGF2ZSBjYXVzZWQgdGhlXG4gICAgICAvLyB0aGlzLl9ldmVudHMgdG8gYmUgYXNzaWduZWQgdG8gYSBuZXcgb2JqZWN0XG4gICAgICBldmVudHMgPSB0YXJnZXQuX2V2ZW50cztcbiAgICB9XG4gICAgZXhpc3RpbmcgPSBldmVudHNbdHlwZV07XG4gIH1cblxuICBpZiAoZXhpc3RpbmcgPT09IHVuZGVmaW5lZCkge1xuICAgIC8vIE9wdGltaXplIHRoZSBjYXNlIG9mIG9uZSBsaXN0ZW5lci4gRG9uJ3QgbmVlZCB0aGUgZXh0cmEgYXJyYXkgb2JqZWN0LlxuICAgIGV4aXN0aW5nID0gZXZlbnRzW3R5cGVdID0gbGlzdGVuZXI7XG4gICAgKyt0YXJnZXQuX2V2ZW50c0NvdW50O1xuICB9IGVsc2Uge1xuICAgIGlmICh0eXBlb2YgZXhpc3RpbmcgPT09ICdmdW5jdGlvbicpIHtcbiAgICAgIC8vIEFkZGluZyB0aGUgc2Vjb25kIGVsZW1lbnQsIG5lZWQgdG8gY2hhbmdlIHRvIGFycmF5LlxuICAgICAgZXhpc3RpbmcgPSBldmVudHNbdHlwZV0gPVxuICAgICAgICBwcmVwZW5kID8gW2xpc3RlbmVyLCBleGlzdGluZ10gOiBbZXhpc3RpbmcsIGxpc3RlbmVyXTtcbiAgICAgIC8vIElmIHdlJ3ZlIGFscmVhZHkgZ290IGFuIGFycmF5LCBqdXN0IGFwcGVuZC5cbiAgICB9IGVsc2UgaWYgKHByZXBlbmQpIHtcbiAgICAgIGV4aXN0aW5nLnVuc2hpZnQobGlzdGVuZXIpO1xuICAgIH0gZWxzZSB7XG4gICAgICBleGlzdGluZy5wdXNoKGxpc3RlbmVyKTtcbiAgICB9XG5cbiAgICAvLyBDaGVjayBmb3IgbGlzdGVuZXIgbGVha1xuICAgIG0gPSBfZ2V0TWF4TGlzdGVuZXJzKHRhcmdldCk7XG4gICAgaWYgKG0gPiAwICYmIGV4aXN0aW5nLmxlbmd0aCA+IG0gJiYgIWV4aXN0aW5nLndhcm5lZCkge1xuICAgICAgZXhpc3Rpbmcud2FybmVkID0gdHJ1ZTtcbiAgICAgIC8vIE5vIGVycm9yIGNvZGUgZm9yIHRoaXMgc2luY2UgaXQgaXMgYSBXYXJuaW5nXG4gICAgICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgbm8tcmVzdHJpY3RlZC1zeW50YXhcbiAgICAgIHZhciB3ID0gbmV3IEVycm9yKCdQb3NzaWJsZSBFdmVudEVtaXR0ZXIgbWVtb3J5IGxlYWsgZGV0ZWN0ZWQuICcgK1xuICAgICAgICAgICAgICAgICAgICAgICAgICBleGlzdGluZy5sZW5ndGggKyAnICcgKyBTdHJpbmcodHlwZSkgKyAnIGxpc3RlbmVycyAnICtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgJ2FkZGVkLiBVc2UgZW1pdHRlci5zZXRNYXhMaXN0ZW5lcnMoKSB0byAnICtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgJ2luY3JlYXNlIGxpbWl0Jyk7XG4gICAgICB3Lm5hbWUgPSAnTWF4TGlzdGVuZXJzRXhjZWVkZWRXYXJuaW5nJztcbiAgICAgIHcuZW1pdHRlciA9IHRhcmdldDtcbiAgICAgIHcudHlwZSA9IHR5cGU7XG4gICAgICB3LmNvdW50ID0gZXhpc3RpbmcubGVuZ3RoO1xuICAgICAgUHJvY2Vzc0VtaXRXYXJuaW5nKHcpO1xuICAgIH1cbiAgfVxuXG4gIHJldHVybiB0YXJnZXQ7XG59XG5cbkV2ZW50RW1pdHRlci5wcm90b3R5cGUuYWRkTGlzdGVuZXIgPSBmdW5jdGlvbiBhZGRMaXN0ZW5lcih0eXBlLCBsaXN0ZW5lcikge1xuICByZXR1cm4gX2FkZExpc3RlbmVyKHRoaXMsIHR5cGUsIGxpc3RlbmVyLCBmYWxzZSk7XG59O1xuXG5FdmVudEVtaXR0ZXIucHJvdG90eXBlLm9uID0gRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5hZGRMaXN0ZW5lcjtcblxuRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5wcmVwZW5kTGlzdGVuZXIgPVxuICAgIGZ1bmN0aW9uIHByZXBlbmRMaXN0ZW5lcih0eXBlLCBsaXN0ZW5lcikge1xuICAgICAgcmV0dXJuIF9hZGRMaXN0ZW5lcih0aGlzLCB0eXBlLCBsaXN0ZW5lciwgdHJ1ZSk7XG4gICAgfTtcblxuZnVuY3Rpb24gb25jZVdyYXBwZXIoKSB7XG4gIGlmICghdGhpcy5maXJlZCkge1xuICAgIHRoaXMudGFyZ2V0LnJlbW92ZUxpc3RlbmVyKHRoaXMudHlwZSwgdGhpcy53cmFwRm4pO1xuICAgIHRoaXMuZmlyZWQgPSB0cnVlO1xuICAgIGlmIChhcmd1bWVudHMubGVuZ3RoID09PSAwKVxuICAgICAgcmV0dXJuIHRoaXMubGlzdGVuZXIuY2FsbCh0aGlzLnRhcmdldCk7XG4gICAgcmV0dXJuIHRoaXMubGlzdGVuZXIuYXBwbHkodGhpcy50YXJnZXQsIGFyZ3VtZW50cyk7XG4gIH1cbn1cblxuZnVuY3Rpb24gX29uY2VXcmFwKHRhcmdldCwgdHlwZSwgbGlzdGVuZXIpIHtcbiAgdmFyIHN0YXRlID0geyBmaXJlZDogZmFsc2UsIHdyYXBGbjogdW5kZWZpbmVkLCB0YXJnZXQ6IHRhcmdldCwgdHlwZTogdHlwZSwgbGlzdGVuZXI6IGxpc3RlbmVyIH07XG4gIHZhciB3cmFwcGVkID0gb25jZVdyYXBwZXIuYmluZChzdGF0ZSk7XG4gIHdyYXBwZWQubGlzdGVuZXIgPSBsaXN0ZW5lcjtcbiAgc3RhdGUud3JhcEZuID0gd3JhcHBlZDtcbiAgcmV0dXJuIHdyYXBwZWQ7XG59XG5cbkV2ZW50RW1pdHRlci5wcm90b3R5cGUub25jZSA9IGZ1bmN0aW9uIG9uY2UodHlwZSwgbGlzdGVuZXIpIHtcbiAgY2hlY2tMaXN0ZW5lcihsaXN0ZW5lcik7XG4gIHRoaXMub24odHlwZSwgX29uY2VXcmFwKHRoaXMsIHR5cGUsIGxpc3RlbmVyKSk7XG4gIHJldHVybiB0aGlzO1xufTtcblxuRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5wcmVwZW5kT25jZUxpc3RlbmVyID1cbiAgICBmdW5jdGlvbiBwcmVwZW5kT25jZUxpc3RlbmVyKHR5cGUsIGxpc3RlbmVyKSB7XG4gICAgICBjaGVja0xpc3RlbmVyKGxpc3RlbmVyKTtcbiAgICAgIHRoaXMucHJlcGVuZExpc3RlbmVyKHR5cGUsIF9vbmNlV3JhcCh0aGlzLCB0eXBlLCBsaXN0ZW5lcikpO1xuICAgICAgcmV0dXJuIHRoaXM7XG4gICAgfTtcblxuLy8gRW1pdHMgYSAncmVtb3ZlTGlzdGVuZXInIGV2ZW50IGlmIGFuZCBvbmx5IGlmIHRoZSBsaXN0ZW5lciB3YXMgcmVtb3ZlZC5cbkV2ZW50RW1pdHRlci5wcm90b3R5cGUucmVtb3ZlTGlzdGVuZXIgPVxuICAgIGZ1bmN0aW9uIHJlbW92ZUxpc3RlbmVyKHR5cGUsIGxpc3RlbmVyKSB7XG4gICAgICB2YXIgbGlzdCwgZXZlbnRzLCBwb3NpdGlvbiwgaSwgb3JpZ2luYWxMaXN0ZW5lcjtcblxuICAgICAgY2hlY2tMaXN0ZW5lcihsaXN0ZW5lcik7XG5cbiAgICAgIGV2ZW50cyA9IHRoaXMuX2V2ZW50cztcbiAgICAgIGlmIChldmVudHMgPT09IHVuZGVmaW5lZClcbiAgICAgICAgcmV0dXJuIHRoaXM7XG5cbiAgICAgIGxpc3QgPSBldmVudHNbdHlwZV07XG4gICAgICBpZiAobGlzdCA9PT0gdW5kZWZpbmVkKVxuICAgICAgICByZXR1cm4gdGhpcztcblxuICAgICAgaWYgKGxpc3QgPT09IGxpc3RlbmVyIHx8IGxpc3QubGlzdGVuZXIgPT09IGxpc3RlbmVyKSB7XG4gICAgICAgIGlmICgtLXRoaXMuX2V2ZW50c0NvdW50ID09PSAwKVxuICAgICAgICAgIHRoaXMuX2V2ZW50cyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgIGRlbGV0ZSBldmVudHNbdHlwZV07XG4gICAgICAgICAgaWYgKGV2ZW50cy5yZW1vdmVMaXN0ZW5lcilcbiAgICAgICAgICAgIHRoaXMuZW1pdCgncmVtb3ZlTGlzdGVuZXInLCB0eXBlLCBsaXN0Lmxpc3RlbmVyIHx8IGxpc3RlbmVyKTtcbiAgICAgICAgfVxuICAgICAgfSBlbHNlIGlmICh0eXBlb2YgbGlzdCAhPT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICBwb3NpdGlvbiA9IC0xO1xuXG4gICAgICAgIGZvciAoaSA9IGxpc3QubGVuZ3RoIC0gMTsgaSA+PSAwOyBpLS0pIHtcbiAgICAgICAgICBpZiAobGlzdFtpXSA9PT0gbGlzdGVuZXIgfHwgbGlzdFtpXS5saXN0ZW5lciA9PT0gbGlzdGVuZXIpIHtcbiAgICAgICAgICAgIG9yaWdpbmFsTGlzdGVuZXIgPSBsaXN0W2ldLmxpc3RlbmVyO1xuICAgICAgICAgICAgcG9zaXRpb24gPSBpO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHBvc2l0aW9uIDwgMClcbiAgICAgICAgICByZXR1cm4gdGhpcztcblxuICAgICAgICBpZiAocG9zaXRpb24gPT09IDApXG4gICAgICAgICAgbGlzdC5zaGlmdCgpO1xuICAgICAgICBlbHNlIHtcbiAgICAgICAgICBzcGxpY2VPbmUobGlzdCwgcG9zaXRpb24pO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKGxpc3QubGVuZ3RoID09PSAxKVxuICAgICAgICAgIGV2ZW50c1t0eXBlXSA9IGxpc3RbMF07XG5cbiAgICAgICAgaWYgKGV2ZW50cy5yZW1vdmVMaXN0ZW5lciAhPT0gdW5kZWZpbmVkKVxuICAgICAgICAgIHRoaXMuZW1pdCgncmVtb3ZlTGlzdGVuZXInLCB0eXBlLCBvcmlnaW5hbExpc3RlbmVyIHx8IGxpc3RlbmVyKTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIHRoaXM7XG4gICAgfTtcblxuRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5vZmYgPSBFdmVudEVtaXR0ZXIucHJvdG90eXBlLnJlbW92ZUxpc3RlbmVyO1xuXG5FdmVudEVtaXR0ZXIucHJvdG90eXBlLnJlbW92ZUFsbExpc3RlbmVycyA9XG4gICAgZnVuY3Rpb24gcmVtb3ZlQWxsTGlzdGVuZXJzKHR5cGUpIHtcbiAgICAgIHZhciBsaXN0ZW5lcnMsIGV2ZW50cywgaTtcblxuICAgICAgZXZlbnRzID0gdGhpcy5fZXZlbnRzO1xuICAgICAgaWYgKGV2ZW50cyA9PT0gdW5kZWZpbmVkKVxuICAgICAgICByZXR1cm4gdGhpcztcblxuICAgICAgLy8gbm90IGxpc3RlbmluZyBmb3IgcmVtb3ZlTGlzdGVuZXIsIG5vIG5lZWQgdG8gZW1pdFxuICAgICAgaWYgKGV2ZW50cy5yZW1vdmVMaXN0ZW5lciA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAgIGlmIChhcmd1bWVudHMubGVuZ3RoID09PSAwKSB7XG4gICAgICAgICAgdGhpcy5fZXZlbnRzID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiAgICAgICAgICB0aGlzLl9ldmVudHNDb3VudCA9IDA7XG4gICAgICAgIH0gZWxzZSBpZiAoZXZlbnRzW3R5cGVdICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICBpZiAoLS10aGlzLl9ldmVudHNDb3VudCA9PT0gMClcbiAgICAgICAgICAgIHRoaXMuX2V2ZW50cyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gICAgICAgICAgZWxzZVxuICAgICAgICAgICAgZGVsZXRlIGV2ZW50c1t0eXBlXTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdGhpcztcbiAgICAgIH1cblxuICAgICAgLy8gZW1pdCByZW1vdmVMaXN0ZW5lciBmb3IgYWxsIGxpc3RlbmVycyBvbiBhbGwgZXZlbnRzXG4gICAgICBpZiAoYXJndW1lbnRzLmxlbmd0aCA9PT0gMCkge1xuICAgICAgICB2YXIga2V5cyA9IE9iamVjdC5rZXlzKGV2ZW50cyk7XG4gICAgICAgIHZhciBrZXk7XG4gICAgICAgIGZvciAoaSA9IDA7IGkgPCBrZXlzLmxlbmd0aDsgKytpKSB7XG4gICAgICAgICAga2V5ID0ga2V5c1tpXTtcbiAgICAgICAgICBpZiAoa2V5ID09PSAncmVtb3ZlTGlzdGVuZXInKSBjb250aW51ZTtcbiAgICAgICAgICB0aGlzLnJlbW92ZUFsbExpc3RlbmVycyhrZXkpO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMucmVtb3ZlQWxsTGlzdGVuZXJzKCdyZW1vdmVMaXN0ZW5lcicpO1xuICAgICAgICB0aGlzLl9ldmVudHMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuICAgICAgICB0aGlzLl9ldmVudHNDb3VudCA9IDA7XG4gICAgICAgIHJldHVybiB0aGlzO1xuICAgICAgfVxuXG4gICAgICBsaXN0ZW5lcnMgPSBldmVudHNbdHlwZV07XG5cbiAgICAgIGlmICh0eXBlb2YgbGlzdGVuZXJzID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgIHRoaXMucmVtb3ZlTGlzdGVuZXIodHlwZSwgbGlzdGVuZXJzKTtcbiAgICAgIH0gZWxzZSBpZiAobGlzdGVuZXJzICE9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgLy8gTElGTyBvcmRlclxuICAgICAgICBmb3IgKGkgPSBsaXN0ZW5lcnMubGVuZ3RoIC0gMTsgaSA+PSAwOyBpLS0pIHtcbiAgICAgICAgICB0aGlzLnJlbW92ZUxpc3RlbmVyKHR5cGUsIGxpc3RlbmVyc1tpXSk7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgcmV0dXJuIHRoaXM7XG4gICAgfTtcblxuZnVuY3Rpb24gX2xpc3RlbmVycyh0YXJnZXQsIHR5cGUsIHVud3JhcCkge1xuICB2YXIgZXZlbnRzID0gdGFyZ2V0Ll9ldmVudHM7XG5cbiAgaWYgKGV2ZW50cyA9PT0gdW5kZWZpbmVkKVxuICAgIHJldHVybiBbXTtcblxuICB2YXIgZXZsaXN0ZW5lciA9IGV2ZW50c1t0eXBlXTtcbiAgaWYgKGV2bGlzdGVuZXIgPT09IHVuZGVmaW5lZClcbiAgICByZXR1cm4gW107XG5cbiAgaWYgKHR5cGVvZiBldmxpc3RlbmVyID09PSAnZnVuY3Rpb24nKVxuICAgIHJldHVybiB1bndyYXAgPyBbZXZsaXN0ZW5lci5saXN0ZW5lciB8fCBldmxpc3RlbmVyXSA6IFtldmxpc3RlbmVyXTtcblxuICByZXR1cm4gdW53cmFwID9cbiAgICB1bndyYXBMaXN0ZW5lcnMoZXZsaXN0ZW5lcikgOiBhcnJheUNsb25lKGV2bGlzdGVuZXIsIGV2bGlzdGVuZXIubGVuZ3RoKTtcbn1cblxuRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5saXN0ZW5lcnMgPSBmdW5jdGlvbiBsaXN0ZW5lcnModHlwZSkge1xuICByZXR1cm4gX2xpc3RlbmVycyh0aGlzLCB0eXBlLCB0cnVlKTtcbn07XG5cbkV2ZW50RW1pdHRlci5wcm90b3R5cGUucmF3TGlzdGVuZXJzID0gZnVuY3Rpb24gcmF3TGlzdGVuZXJzKHR5cGUpIHtcbiAgcmV0dXJuIF9saXN0ZW5lcnModGhpcywgdHlwZSwgZmFsc2UpO1xufTtcblxuRXZlbnRFbWl0dGVyLmxpc3RlbmVyQ291bnQgPSBmdW5jdGlvbihlbWl0dGVyLCB0eXBlKSB7XG4gIGlmICh0eXBlb2YgZW1pdHRlci5saXN0ZW5lckNvdW50ID09PSAnZnVuY3Rpb24nKSB7XG4gICAgcmV0dXJuIGVtaXR0ZXIubGlzdGVuZXJDb3VudCh0eXBlKTtcbiAgfSBlbHNlIHtcbiAgICByZXR1cm4gbGlzdGVuZXJDb3VudC5jYWxsKGVtaXR0ZXIsIHR5cGUpO1xuICB9XG59O1xuXG5FdmVudEVtaXR0ZXIucHJvdG90eXBlLmxpc3RlbmVyQ291bnQgPSBsaXN0ZW5lckNvdW50O1xuZnVuY3Rpb24gbGlzdGVuZXJDb3VudCh0eXBlKSB7XG4gIHZhciBldmVudHMgPSB0aGlzLl9ldmVudHM7XG5cbiAgaWYgKGV2ZW50cyAhPT0gdW5kZWZpbmVkKSB7XG4gICAgdmFyIGV2bGlzdGVuZXIgPSBldmVudHNbdHlwZV07XG5cbiAgICBpZiAodHlwZW9mIGV2bGlzdGVuZXIgPT09ICdmdW5jdGlvbicpIHtcbiAgICAgIHJldHVybiAxO1xuICAgIH0gZWxzZSBpZiAoZXZsaXN0ZW5lciAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICByZXR1cm4gZXZsaXN0ZW5lci5sZW5ndGg7XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIDA7XG59XG5cbkV2ZW50RW1pdHRlci5wcm90b3R5cGUuZXZlbnROYW1lcyA9IGZ1bmN0aW9uIGV2ZW50TmFtZXMoKSB7XG4gIHJldHVybiB0aGlzLl9ldmVudHNDb3VudCA+IDAgPyBSZWZsZWN0T3duS2V5cyh0aGlzLl9ldmVudHMpIDogW107XG59O1xuXG5mdW5jdGlvbiBhcnJheUNsb25lKGFyciwgbikge1xuICB2YXIgY29weSA9IG5ldyBBcnJheShuKTtcbiAgZm9yICh2YXIgaSA9IDA7IGkgPCBuOyArK2kpXG4gICAgY29weVtpXSA9IGFycltpXTtcbiAgcmV0dXJuIGNvcHk7XG59XG5cbmZ1bmN0aW9uIHNwbGljZU9uZShsaXN0LCBpbmRleCkge1xuICBmb3IgKDsgaW5kZXggKyAxIDwgbGlzdC5sZW5ndGg7IGluZGV4KyspXG4gICAgbGlzdFtpbmRleF0gPSBsaXN0W2luZGV4ICsgMV07XG4gIGxpc3QucG9wKCk7XG59XG5cbmZ1bmN0aW9uIHVud3JhcExpc3RlbmVycyhhcnIpIHtcbiAgdmFyIHJldCA9IG5ldyBBcnJheShhcnIubGVuZ3RoKTtcbiAgZm9yICh2YXIgaSA9IDA7IGkgPCByZXQubGVuZ3RoOyArK2kpIHtcbiAgICByZXRbaV0gPSBhcnJbaV0ubGlzdGVuZXIgfHwgYXJyW2ldO1xuICB9XG4gIHJldHVybiByZXQ7XG59XG4iLCIvLyBzaGltIGZvciB1c2luZyBwcm9jZXNzIGluIGJyb3dzZXJcbnZhciBwcm9jZXNzID0gbW9kdWxlLmV4cG9ydHMgPSB7fTtcblxuLy8gY2FjaGVkIGZyb20gd2hhdGV2ZXIgZ2xvYmFsIGlzIHByZXNlbnQgc28gdGhhdCB0ZXN0IHJ1bm5lcnMgdGhhdCBzdHViIGl0XG4vLyBkb24ndCBicmVhayB0aGluZ3MuICBCdXQgd2UgbmVlZCB0byB3cmFwIGl0IGluIGEgdHJ5IGNhdGNoIGluIGNhc2UgaXQgaXNcbi8vIHdyYXBwZWQgaW4gc3RyaWN0IG1vZGUgY29kZSB3aGljaCBkb2Vzbid0IGRlZmluZSBhbnkgZ2xvYmFscy4gIEl0J3MgaW5zaWRlIGFcbi8vIGZ1bmN0aW9uIGJlY2F1c2UgdHJ5L2NhdGNoZXMgZGVvcHRpbWl6ZSBpbiBjZXJ0YWluIGVuZ2luZXMuXG5cbnZhciBjYWNoZWRTZXRUaW1lb3V0O1xudmFyIGNhY2hlZENsZWFyVGltZW91dDtcblxuZnVuY3Rpb24gZGVmYXVsdFNldFRpbW91dCgpIHtcbiAgICB0aHJvdyBuZXcgRXJyb3IoJ3NldFRpbWVvdXQgaGFzIG5vdCBiZWVuIGRlZmluZWQnKTtcbn1cbmZ1bmN0aW9uIGRlZmF1bHRDbGVhclRpbWVvdXQgKCkge1xuICAgIHRocm93IG5ldyBFcnJvcignY2xlYXJUaW1lb3V0IGhhcyBub3QgYmVlbiBkZWZpbmVkJyk7XG59XG4oZnVuY3Rpb24gKCkge1xuICAgIHRyeSB7XG4gICAgICAgIGlmICh0eXBlb2Ygc2V0VGltZW91dCA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICAgICAgY2FjaGVkU2V0VGltZW91dCA9IHNldFRpbWVvdXQ7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBjYWNoZWRTZXRUaW1lb3V0ID0gZGVmYXVsdFNldFRpbW91dDtcbiAgICAgICAgfVxuICAgIH0gY2F0Y2ggKGUpIHtcbiAgICAgICAgY2FjaGVkU2V0VGltZW91dCA9IGRlZmF1bHRTZXRUaW1vdXQ7XG4gICAgfVxuICAgIHRyeSB7XG4gICAgICAgIGlmICh0eXBlb2YgY2xlYXJUaW1lb3V0ID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgICAgICBjYWNoZWRDbGVhclRpbWVvdXQgPSBjbGVhclRpbWVvdXQ7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBjYWNoZWRDbGVhclRpbWVvdXQgPSBkZWZhdWx0Q2xlYXJUaW1lb3V0O1xuICAgICAgICB9XG4gICAgfSBjYXRjaCAoZSkge1xuICAgICAgICBjYWNoZWRDbGVhclRpbWVvdXQgPSBkZWZhdWx0Q2xlYXJUaW1lb3V0O1xuICAgIH1cbn0gKCkpXG5mdW5jdGlvbiBydW5UaW1lb3V0KGZ1bikge1xuICAgIGlmIChjYWNoZWRTZXRUaW1lb3V0ID09PSBzZXRUaW1lb3V0KSB7XG4gICAgICAgIC8vbm9ybWFsIGVudmlyb21lbnRzIGluIHNhbmUgc2l0dWF0aW9uc1xuICAgICAgICByZXR1cm4gc2V0VGltZW91dChmdW4sIDApO1xuICAgIH1cbiAgICAvLyBpZiBzZXRUaW1lb3V0IHdhc24ndCBhdmFpbGFibGUgYnV0IHdhcyBsYXR0ZXIgZGVmaW5lZFxuICAgIGlmICgoY2FjaGVkU2V0VGltZW91dCA9PT0gZGVmYXVsdFNldFRpbW91dCB8fCAhY2FjaGVkU2V0VGltZW91dCkgJiYgc2V0VGltZW91dCkge1xuICAgICAgICBjYWNoZWRTZXRUaW1lb3V0ID0gc2V0VGltZW91dDtcbiAgICAgICAgcmV0dXJuIHNldFRpbWVvdXQoZnVuLCAwKTtcbiAgICB9XG4gICAgdHJ5IHtcbiAgICAgICAgLy8gd2hlbiB3aGVuIHNvbWVib2R5IGhhcyBzY3Jld2VkIHdpdGggc2V0VGltZW91dCBidXQgbm8gSS5FLiBtYWRkbmVzc1xuICAgICAgICByZXR1cm4gY2FjaGVkU2V0VGltZW91dChmdW4sIDApO1xuICAgIH0gY2F0Y2goZSl7XG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICAvLyBXaGVuIHdlIGFyZSBpbiBJLkUuIGJ1dCB0aGUgc2NyaXB0IGhhcyBiZWVuIGV2YWxlZCBzbyBJLkUuIGRvZXNuJ3QgdHJ1c3QgdGhlIGdsb2JhbCBvYmplY3Qgd2hlbiBjYWxsZWQgbm9ybWFsbHlcbiAgICAgICAgICAgIHJldHVybiBjYWNoZWRTZXRUaW1lb3V0LmNhbGwobnVsbCwgZnVuLCAwKTtcbiAgICAgICAgfSBjYXRjaChlKXtcbiAgICAgICAgICAgIC8vIHNhbWUgYXMgYWJvdmUgYnV0IHdoZW4gaXQncyBhIHZlcnNpb24gb2YgSS5FLiB0aGF0IG11c3QgaGF2ZSB0aGUgZ2xvYmFsIG9iamVjdCBmb3IgJ3RoaXMnLCBob3BmdWxseSBvdXIgY29udGV4dCBjb3JyZWN0IG90aGVyd2lzZSBpdCB3aWxsIHRocm93IGEgZ2xvYmFsIGVycm9yXG4gICAgICAgICAgICByZXR1cm4gY2FjaGVkU2V0VGltZW91dC5jYWxsKHRoaXMsIGZ1biwgMCk7XG4gICAgICAgIH1cbiAgICB9XG5cblxufVxuZnVuY3Rpb24gcnVuQ2xlYXJUaW1lb3V0KG1hcmtlcikge1xuICAgIGlmIChjYWNoZWRDbGVhclRpbWVvdXQgPT09IGNsZWFyVGltZW91dCkge1xuICAgICAgICAvL25vcm1hbCBlbnZpcm9tZW50cyBpbiBzYW5lIHNpdHVhdGlvbnNcbiAgICAgICAgcmV0dXJuIGNsZWFyVGltZW91dChtYXJrZXIpO1xuICAgIH1cbiAgICAvLyBpZiBjbGVhclRpbWVvdXQgd2Fzbid0IGF2YWlsYWJsZSBidXQgd2FzIGxhdHRlciBkZWZpbmVkXG4gICAgaWYgKChjYWNoZWRDbGVhclRpbWVvdXQgPT09IGRlZmF1bHRDbGVhclRpbWVvdXQgfHwgIWNhY2hlZENsZWFyVGltZW91dCkgJiYgY2xlYXJUaW1lb3V0KSB7XG4gICAgICAgIGNhY2hlZENsZWFyVGltZW91dCA9IGNsZWFyVGltZW91dDtcbiAgICAgICAgcmV0dXJuIGNsZWFyVGltZW91dChtYXJrZXIpO1xuICAgIH1cbiAgICB0cnkge1xuICAgICAgICAvLyB3aGVuIHdoZW4gc29tZWJvZHkgaGFzIHNjcmV3ZWQgd2l0aCBzZXRUaW1lb3V0IGJ1dCBubyBJLkUuIG1hZGRuZXNzXG4gICAgICAgIHJldHVybiBjYWNoZWRDbGVhclRpbWVvdXQobWFya2VyKTtcbiAgICB9IGNhdGNoIChlKXtcbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIC8vIFdoZW4gd2UgYXJlIGluIEkuRS4gYnV0IHRoZSBzY3JpcHQgaGFzIGJlZW4gZXZhbGVkIHNvIEkuRS4gZG9lc24ndCAgdHJ1c3QgdGhlIGdsb2JhbCBvYmplY3Qgd2hlbiBjYWxsZWQgbm9ybWFsbHlcbiAgICAgICAgICAgIHJldHVybiBjYWNoZWRDbGVhclRpbWVvdXQuY2FsbChudWxsLCBtYXJrZXIpO1xuICAgICAgICB9IGNhdGNoIChlKXtcbiAgICAgICAgICAgIC8vIHNhbWUgYXMgYWJvdmUgYnV0IHdoZW4gaXQncyBhIHZlcnNpb24gb2YgSS5FLiB0aGF0IG11c3QgaGF2ZSB0aGUgZ2xvYmFsIG9iamVjdCBmb3IgJ3RoaXMnLCBob3BmdWxseSBvdXIgY29udGV4dCBjb3JyZWN0IG90aGVyd2lzZSBpdCB3aWxsIHRocm93IGEgZ2xvYmFsIGVycm9yLlxuICAgICAgICAgICAgLy8gU29tZSB2ZXJzaW9ucyBvZiBJLkUuIGhhdmUgZGlmZmVyZW50IHJ1bGVzIGZvciBjbGVhclRpbWVvdXQgdnMgc2V0VGltZW91dFxuICAgICAgICAgICAgcmV0dXJuIGNhY2hlZENsZWFyVGltZW91dC5jYWxsKHRoaXMsIG1hcmtlcik7XG4gICAgICAgIH1cbiAgICB9XG5cblxuXG59XG52YXIgcXVldWUgPSBbXTtcbnZhciBkcmFpbmluZyA9IGZhbHNlO1xudmFyIGN1cnJlbnRRdWV1ZTtcbnZhciBxdWV1ZUluZGV4ID0gLTE7XG5cbmZ1bmN0aW9uIGNsZWFuVXBOZXh0VGljaygpIHtcbiAgICBpZiAoIWRyYWluaW5nIHx8ICFjdXJyZW50UXVldWUpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgIH1cbiAgICBkcmFpbmluZyA9IGZhbHNlO1xuICAgIGlmIChjdXJyZW50UXVldWUubGVuZ3RoKSB7XG4gICAgICAgIHF1ZXVlID0gY3VycmVudFF1ZXVlLmNvbmNhdChxdWV1ZSk7XG4gICAgfSBlbHNlIHtcbiAgICAgICAgcXVldWVJbmRleCA9IC0xO1xuICAgIH1cbiAgICBpZiAocXVldWUubGVuZ3RoKSB7XG4gICAgICAgIGRyYWluUXVldWUoKTtcbiAgICB9XG59XG5cbmZ1bmN0aW9uIGRyYWluUXVldWUoKSB7XG4gICAgaWYgKGRyYWluaW5nKSB7XG4gICAgICAgIHJldHVybjtcbiAgICB9XG4gICAgdmFyIHRpbWVvdXQgPSBydW5UaW1lb3V0KGNsZWFuVXBOZXh0VGljayk7XG4gICAgZHJhaW5pbmcgPSB0cnVlO1xuXG4gICAgdmFyIGxlbiA9IHF1ZXVlLmxlbmd0aDtcbiAgICB3aGlsZShsZW4pIHtcbiAgICAgICAgY3VycmVudFF1ZXVlID0gcXVldWU7XG4gICAgICAgIHF1ZXVlID0gW107XG4gICAgICAgIHdoaWxlICgrK3F1ZXVlSW5kZXggPCBsZW4pIHtcbiAgICAgICAgICAgIGlmIChjdXJyZW50UXVldWUpIHtcbiAgICAgICAgICAgICAgICBjdXJyZW50UXVldWVbcXVldWVJbmRleF0ucnVuKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgcXVldWVJbmRleCA9IC0xO1xuICAgICAgICBsZW4gPSBxdWV1ZS5sZW5ndGg7XG4gICAgfVxuICAgIGN1cnJlbnRRdWV1ZSA9IG51bGw7XG4gICAgZHJhaW5pbmcgPSBmYWxzZTtcbiAgICBydW5DbGVhclRpbWVvdXQodGltZW91dCk7XG59XG5cbnByb2Nlc3MubmV4dFRpY2sgPSBmdW5jdGlvbiAoZnVuKSB7XG4gICAgdmFyIGFyZ3MgPSBuZXcgQXJyYXkoYXJndW1lbnRzLmxlbmd0aCAtIDEpO1xuICAgIGlmIChhcmd1bWVudHMubGVuZ3RoID4gMSkge1xuICAgICAgICBmb3IgKHZhciBpID0gMTsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgYXJnc1tpIC0gMV0gPSBhcmd1bWVudHNbaV07XG4gICAgICAgIH1cbiAgICB9XG4gICAgcXVldWUucHVzaChuZXcgSXRlbShmdW4sIGFyZ3MpKTtcbiAgICBpZiAocXVldWUubGVuZ3RoID09PSAxICYmICFkcmFpbmluZykge1xuICAgICAgICBydW5UaW1lb3V0KGRyYWluUXVldWUpO1xuICAgIH1cbn07XG5cbi8vIHY4IGxpa2VzIHByZWRpY3RpYmxlIG9iamVjdHNcbmZ1bmN0aW9uIEl0ZW0oZnVuLCBhcnJheSkge1xuICAgIHRoaXMuZnVuID0gZnVuO1xuICAgIHRoaXMuYXJyYXkgPSBhcnJheTtcbn1cbkl0ZW0ucHJvdG90eXBlLnJ1biA9IGZ1bmN0aW9uICgpIHtcbiAgICB0aGlzLmZ1bi5hcHBseShudWxsLCB0aGlzLmFycmF5KTtcbn07XG5wcm9jZXNzLnRpdGxlID0gJ2Jyb3dzZXInO1xucHJvY2Vzcy5icm93c2VyID0gdHJ1ZTtcbnByb2Nlc3MuZW52ID0ge307XG5wcm9jZXNzLmFyZ3YgPSBbXTtcbnByb2Nlc3MudmVyc2lvbiA9ICcnOyAvLyBlbXB0eSBzdHJpbmcgdG8gYXZvaWQgcmVnZXhwIGlzc3Vlc1xucHJvY2Vzcy52ZXJzaW9ucyA9IHt9O1xuXG5mdW5jdGlvbiBub29wKCkge31cblxucHJvY2Vzcy5vbiA9IG5vb3A7XG5wcm9jZXNzLmFkZExpc3RlbmVyID0gbm9vcDtcbnByb2Nlc3Mub25jZSA9IG5vb3A7XG5wcm9jZXNzLm9mZiA9IG5vb3A7XG5wcm9jZXNzLnJlbW92ZUxpc3RlbmVyID0gbm9vcDtcbnByb2Nlc3MucmVtb3ZlQWxsTGlzdGVuZXJzID0gbm9vcDtcbnByb2Nlc3MuZW1pdCA9IG5vb3A7XG5wcm9jZXNzLnByZXBlbmRMaXN0ZW5lciA9IG5vb3A7XG5wcm9jZXNzLnByZXBlbmRPbmNlTGlzdGVuZXIgPSBub29wO1xuXG5wcm9jZXNzLmxpc3RlbmVycyA9IGZ1bmN0aW9uIChuYW1lKSB7IHJldHVybiBbXSB9XG5cbnByb2Nlc3MuYmluZGluZyA9IGZ1bmN0aW9uIChuYW1lKSB7XG4gICAgdGhyb3cgbmV3IEVycm9yKCdwcm9jZXNzLmJpbmRpbmcgaXMgbm90IHN1cHBvcnRlZCcpO1xufTtcblxucHJvY2Vzcy5jd2QgPSBmdW5jdGlvbiAoKSB7IHJldHVybiAnLycgfTtcbnByb2Nlc3MuY2hkaXIgPSBmdW5jdGlvbiAoZGlyKSB7XG4gICAgdGhyb3cgbmV3IEVycm9yKCdwcm9jZXNzLmNoZGlyIGlzIG5vdCBzdXBwb3J0ZWQnKTtcbn07XG5wcm9jZXNzLnVtYXNrID0gZnVuY3Rpb24oKSB7IHJldHVybiAwOyB9O1xuIiwiLyoqXG4gKiBDb3B5cmlnaHQgKGMpIDIwMTQtcHJlc2VudCwgRmFjZWJvb2ssIEluYy5cbiAqXG4gKiBUaGlzIHNvdXJjZSBjb2RlIGlzIGxpY2Vuc2VkIHVuZGVyIHRoZSBNSVQgbGljZW5zZSBmb3VuZCBpbiB0aGVcbiAqIExJQ0VOU0UgZmlsZSBpbiB0aGUgcm9vdCBkaXJlY3Rvcnkgb2YgdGhpcyBzb3VyY2UgdHJlZS5cbiAqL1xuXG52YXIgcnVudGltZSA9IChmdW5jdGlvbiAoZXhwb3J0cykge1xuICBcInVzZSBzdHJpY3RcIjtcblxuICB2YXIgT3AgPSBPYmplY3QucHJvdG90eXBlO1xuICB2YXIgaGFzT3duID0gT3AuaGFzT3duUHJvcGVydHk7XG4gIHZhciB1bmRlZmluZWQ7IC8vIE1vcmUgY29tcHJlc3NpYmxlIHRoYW4gdm9pZCAwLlxuICB2YXIgJFN5bWJvbCA9IHR5cGVvZiBTeW1ib2wgPT09IFwiZnVuY3Rpb25cIiA/IFN5bWJvbCA6IHt9O1xuICB2YXIgaXRlcmF0b3JTeW1ib2wgPSAkU3ltYm9sLml0ZXJhdG9yIHx8IFwiQEBpdGVyYXRvclwiO1xuICB2YXIgYXN5bmNJdGVyYXRvclN5bWJvbCA9ICRTeW1ib2wuYXN5bmNJdGVyYXRvciB8fCBcIkBAYXN5bmNJdGVyYXRvclwiO1xuICB2YXIgdG9TdHJpbmdUYWdTeW1ib2wgPSAkU3ltYm9sLnRvU3RyaW5nVGFnIHx8IFwiQEB0b1N0cmluZ1RhZ1wiO1xuXG4gIGZ1bmN0aW9uIHdyYXAoaW5uZXJGbiwgb3V0ZXJGbiwgc2VsZiwgdHJ5TG9jc0xpc3QpIHtcbiAgICAvLyBJZiBvdXRlckZuIHByb3ZpZGVkIGFuZCBvdXRlckZuLnByb3RvdHlwZSBpcyBhIEdlbmVyYXRvciwgdGhlbiBvdXRlckZuLnByb3RvdHlwZSBpbnN0YW5jZW9mIEdlbmVyYXRvci5cbiAgICB2YXIgcHJvdG9HZW5lcmF0b3IgPSBvdXRlckZuICYmIG91dGVyRm4ucHJvdG90eXBlIGluc3RhbmNlb2YgR2VuZXJhdG9yID8gb3V0ZXJGbiA6IEdlbmVyYXRvcjtcbiAgICB2YXIgZ2VuZXJhdG9yID0gT2JqZWN0LmNyZWF0ZShwcm90b0dlbmVyYXRvci5wcm90b3R5cGUpO1xuICAgIHZhciBjb250ZXh0ID0gbmV3IENvbnRleHQodHJ5TG9jc0xpc3QgfHwgW10pO1xuXG4gICAgLy8gVGhlIC5faW52b2tlIG1ldGhvZCB1bmlmaWVzIHRoZSBpbXBsZW1lbnRhdGlvbnMgb2YgdGhlIC5uZXh0LFxuICAgIC8vIC50aHJvdywgYW5kIC5yZXR1cm4gbWV0aG9kcy5cbiAgICBnZW5lcmF0b3IuX2ludm9rZSA9IG1ha2VJbnZva2VNZXRob2QoaW5uZXJGbiwgc2VsZiwgY29udGV4dCk7XG5cbiAgICByZXR1cm4gZ2VuZXJhdG9yO1xuICB9XG4gIGV4cG9ydHMud3JhcCA9IHdyYXA7XG5cbiAgLy8gVHJ5L2NhdGNoIGhlbHBlciB0byBtaW5pbWl6ZSBkZW9wdGltaXphdGlvbnMuIFJldHVybnMgYSBjb21wbGV0aW9uXG4gIC8vIHJlY29yZCBsaWtlIGNvbnRleHQudHJ5RW50cmllc1tpXS5jb21wbGV0aW9uLiBUaGlzIGludGVyZmFjZSBjb3VsZFxuICAvLyBoYXZlIGJlZW4gKGFuZCB3YXMgcHJldmlvdXNseSkgZGVzaWduZWQgdG8gdGFrZSBhIGNsb3N1cmUgdG8gYmVcbiAgLy8gaW52b2tlZCB3aXRob3V0IGFyZ3VtZW50cywgYnV0IGluIGFsbCB0aGUgY2FzZXMgd2UgY2FyZSBhYm91dCB3ZVxuICAvLyBhbHJlYWR5IGhhdmUgYW4gZXhpc3RpbmcgbWV0aG9kIHdlIHdhbnQgdG8gY2FsbCwgc28gdGhlcmUncyBubyBuZWVkXG4gIC8vIHRvIGNyZWF0ZSBhIG5ldyBmdW5jdGlvbiBvYmplY3QuIFdlIGNhbiBldmVuIGdldCBhd2F5IHdpdGggYXNzdW1pbmdcbiAgLy8gdGhlIG1ldGhvZCB0YWtlcyBleGFjdGx5IG9uZSBhcmd1bWVudCwgc2luY2UgdGhhdCBoYXBwZW5zIHRvIGJlIHRydWVcbiAgLy8gaW4gZXZlcnkgY2FzZSwgc28gd2UgZG9uJ3QgaGF2ZSB0byB0b3VjaCB0aGUgYXJndW1lbnRzIG9iamVjdC4gVGhlXG4gIC8vIG9ubHkgYWRkaXRpb25hbCBhbGxvY2F0aW9uIHJlcXVpcmVkIGlzIHRoZSBjb21wbGV0aW9uIHJlY29yZCwgd2hpY2hcbiAgLy8gaGFzIGEgc3RhYmxlIHNoYXBlIGFuZCBzbyBob3BlZnVsbHkgc2hvdWxkIGJlIGNoZWFwIHRvIGFsbG9jYXRlLlxuICBmdW5jdGlvbiB0cnlDYXRjaChmbiwgb2JqLCBhcmcpIHtcbiAgICB0cnkge1xuICAgICAgcmV0dXJuIHsgdHlwZTogXCJub3JtYWxcIiwgYXJnOiBmbi5jYWxsKG9iaiwgYXJnKSB9O1xuICAgIH0gY2F0Y2ggKGVycikge1xuICAgICAgcmV0dXJuIHsgdHlwZTogXCJ0aHJvd1wiLCBhcmc6IGVyciB9O1xuICAgIH1cbiAgfVxuXG4gIHZhciBHZW5TdGF0ZVN1c3BlbmRlZFN0YXJ0ID0gXCJzdXNwZW5kZWRTdGFydFwiO1xuICB2YXIgR2VuU3RhdGVTdXNwZW5kZWRZaWVsZCA9IFwic3VzcGVuZGVkWWllbGRcIjtcbiAgdmFyIEdlblN0YXRlRXhlY3V0aW5nID0gXCJleGVjdXRpbmdcIjtcbiAgdmFyIEdlblN0YXRlQ29tcGxldGVkID0gXCJjb21wbGV0ZWRcIjtcblxuICAvLyBSZXR1cm5pbmcgdGhpcyBvYmplY3QgZnJvbSB0aGUgaW5uZXJGbiBoYXMgdGhlIHNhbWUgZWZmZWN0IGFzXG4gIC8vIGJyZWFraW5nIG91dCBvZiB0aGUgZGlzcGF0Y2ggc3dpdGNoIHN0YXRlbWVudC5cbiAgdmFyIENvbnRpbnVlU2VudGluZWwgPSB7fTtcblxuICAvLyBEdW1teSBjb25zdHJ1Y3RvciBmdW5jdGlvbnMgdGhhdCB3ZSB1c2UgYXMgdGhlIC5jb25zdHJ1Y3RvciBhbmRcbiAgLy8gLmNvbnN0cnVjdG9yLnByb3RvdHlwZSBwcm9wZXJ0aWVzIGZvciBmdW5jdGlvbnMgdGhhdCByZXR1cm4gR2VuZXJhdG9yXG4gIC8vIG9iamVjdHMuIEZvciBmdWxsIHNwZWMgY29tcGxpYW5jZSwgeW91IG1heSB3aXNoIHRvIGNvbmZpZ3VyZSB5b3VyXG4gIC8vIG1pbmlmaWVyIG5vdCB0byBtYW5nbGUgdGhlIG5hbWVzIG9mIHRoZXNlIHR3byBmdW5jdGlvbnMuXG4gIGZ1bmN0aW9uIEdlbmVyYXRvcigpIHt9XG4gIGZ1bmN0aW9uIEdlbmVyYXRvckZ1bmN0aW9uKCkge31cbiAgZnVuY3Rpb24gR2VuZXJhdG9yRnVuY3Rpb25Qcm90b3R5cGUoKSB7fVxuXG4gIC8vIFRoaXMgaXMgYSBwb2x5ZmlsbCBmb3IgJUl0ZXJhdG9yUHJvdG90eXBlJSBmb3IgZW52aXJvbm1lbnRzIHRoYXRcbiAgLy8gZG9uJ3QgbmF0aXZlbHkgc3VwcG9ydCBpdC5cbiAgdmFyIEl0ZXJhdG9yUHJvdG90eXBlID0ge307XG4gIEl0ZXJhdG9yUHJvdG90eXBlW2l0ZXJhdG9yU3ltYm9sXSA9IGZ1bmN0aW9uICgpIHtcbiAgICByZXR1cm4gdGhpcztcbiAgfTtcblxuICB2YXIgZ2V0UHJvdG8gPSBPYmplY3QuZ2V0UHJvdG90eXBlT2Y7XG4gIHZhciBOYXRpdmVJdGVyYXRvclByb3RvdHlwZSA9IGdldFByb3RvICYmIGdldFByb3RvKGdldFByb3RvKHZhbHVlcyhbXSkpKTtcbiAgaWYgKE5hdGl2ZUl0ZXJhdG9yUHJvdG90eXBlICYmXG4gICAgICBOYXRpdmVJdGVyYXRvclByb3RvdHlwZSAhPT0gT3AgJiZcbiAgICAgIGhhc093bi5jYWxsKE5hdGl2ZUl0ZXJhdG9yUHJvdG90eXBlLCBpdGVyYXRvclN5bWJvbCkpIHtcbiAgICAvLyBUaGlzIGVudmlyb25tZW50IGhhcyBhIG5hdGl2ZSAlSXRlcmF0b3JQcm90b3R5cGUlOyB1c2UgaXQgaW5zdGVhZFxuICAgIC8vIG9mIHRoZSBwb2x5ZmlsbC5cbiAgICBJdGVyYXRvclByb3RvdHlwZSA9IE5hdGl2ZUl0ZXJhdG9yUHJvdG90eXBlO1xuICB9XG5cbiAgdmFyIEdwID0gR2VuZXJhdG9yRnVuY3Rpb25Qcm90b3R5cGUucHJvdG90eXBlID1cbiAgICBHZW5lcmF0b3IucHJvdG90eXBlID0gT2JqZWN0LmNyZWF0ZShJdGVyYXRvclByb3RvdHlwZSk7XG4gIEdlbmVyYXRvckZ1bmN0aW9uLnByb3RvdHlwZSA9IEdwLmNvbnN0cnVjdG9yID0gR2VuZXJhdG9yRnVuY3Rpb25Qcm90b3R5cGU7XG4gIEdlbmVyYXRvckZ1bmN0aW9uUHJvdG90eXBlLmNvbnN0cnVjdG9yID0gR2VuZXJhdG9yRnVuY3Rpb247XG4gIEdlbmVyYXRvckZ1bmN0aW9uUHJvdG90eXBlW3RvU3RyaW5nVGFnU3ltYm9sXSA9XG4gICAgR2VuZXJhdG9yRnVuY3Rpb24uZGlzcGxheU5hbWUgPSBcIkdlbmVyYXRvckZ1bmN0aW9uXCI7XG5cbiAgLy8gSGVscGVyIGZvciBkZWZpbmluZyB0aGUgLm5leHQsIC50aHJvdywgYW5kIC5yZXR1cm4gbWV0aG9kcyBvZiB0aGVcbiAgLy8gSXRlcmF0b3IgaW50ZXJmYWNlIGluIHRlcm1zIG9mIGEgc2luZ2xlIC5faW52b2tlIG1ldGhvZC5cbiAgZnVuY3Rpb24gZGVmaW5lSXRlcmF0b3JNZXRob2RzKHByb3RvdHlwZSkge1xuICAgIFtcIm5leHRcIiwgXCJ0aHJvd1wiLCBcInJldHVyblwiXS5mb3JFYWNoKGZ1bmN0aW9uKG1ldGhvZCkge1xuICAgICAgcHJvdG90eXBlW21ldGhvZF0gPSBmdW5jdGlvbihhcmcpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX2ludm9rZShtZXRob2QsIGFyZyk7XG4gICAgICB9O1xuICAgIH0pO1xuICB9XG5cbiAgZXhwb3J0cy5pc0dlbmVyYXRvckZ1bmN0aW9uID0gZnVuY3Rpb24oZ2VuRnVuKSB7XG4gICAgdmFyIGN0b3IgPSB0eXBlb2YgZ2VuRnVuID09PSBcImZ1bmN0aW9uXCIgJiYgZ2VuRnVuLmNvbnN0cnVjdG9yO1xuICAgIHJldHVybiBjdG9yXG4gICAgICA/IGN0b3IgPT09IEdlbmVyYXRvckZ1bmN0aW9uIHx8XG4gICAgICAgIC8vIEZvciB0aGUgbmF0aXZlIEdlbmVyYXRvckZ1bmN0aW9uIGNvbnN0cnVjdG9yLCB0aGUgYmVzdCB3ZSBjYW5cbiAgICAgICAgLy8gZG8gaXMgdG8gY2hlY2sgaXRzIC5uYW1lIHByb3BlcnR5LlxuICAgICAgICAoY3Rvci5kaXNwbGF5TmFtZSB8fCBjdG9yLm5hbWUpID09PSBcIkdlbmVyYXRvckZ1bmN0aW9uXCJcbiAgICAgIDogZmFsc2U7XG4gIH07XG5cbiAgZXhwb3J0cy5tYXJrID0gZnVuY3Rpb24oZ2VuRnVuKSB7XG4gICAgaWYgKE9iamVjdC5zZXRQcm90b3R5cGVPZikge1xuICAgICAgT2JqZWN0LnNldFByb3RvdHlwZU9mKGdlbkZ1biwgR2VuZXJhdG9yRnVuY3Rpb25Qcm90b3R5cGUpO1xuICAgIH0gZWxzZSB7XG4gICAgICBnZW5GdW4uX19wcm90b19fID0gR2VuZXJhdG9yRnVuY3Rpb25Qcm90b3R5cGU7XG4gICAgICBpZiAoISh0b1N0cmluZ1RhZ1N5bWJvbCBpbiBnZW5GdW4pKSB7XG4gICAgICAgIGdlbkZ1blt0b1N0cmluZ1RhZ1N5bWJvbF0gPSBcIkdlbmVyYXRvckZ1bmN0aW9uXCI7XG4gICAgICB9XG4gICAgfVxuICAgIGdlbkZ1bi5wcm90b3R5cGUgPSBPYmplY3QuY3JlYXRlKEdwKTtcbiAgICByZXR1cm4gZ2VuRnVuO1xuICB9O1xuXG4gIC8vIFdpdGhpbiB0aGUgYm9keSBvZiBhbnkgYXN5bmMgZnVuY3Rpb24sIGBhd2FpdCB4YCBpcyB0cmFuc2Zvcm1lZCB0b1xuICAvLyBgeWllbGQgcmVnZW5lcmF0b3JSdW50aW1lLmF3cmFwKHgpYCwgc28gdGhhdCB0aGUgcnVudGltZSBjYW4gdGVzdFxuICAvLyBgaGFzT3duLmNhbGwodmFsdWUsIFwiX19hd2FpdFwiKWAgdG8gZGV0ZXJtaW5lIGlmIHRoZSB5aWVsZGVkIHZhbHVlIGlzXG4gIC8vIG1lYW50IHRvIGJlIGF3YWl0ZWQuXG4gIGV4cG9ydHMuYXdyYXAgPSBmdW5jdGlvbihhcmcpIHtcbiAgICByZXR1cm4geyBfX2F3YWl0OiBhcmcgfTtcbiAgfTtcblxuICBmdW5jdGlvbiBBc3luY0l0ZXJhdG9yKGdlbmVyYXRvciwgUHJvbWlzZUltcGwpIHtcbiAgICBmdW5jdGlvbiBpbnZva2UobWV0aG9kLCBhcmcsIHJlc29sdmUsIHJlamVjdCkge1xuICAgICAgdmFyIHJlY29yZCA9IHRyeUNhdGNoKGdlbmVyYXRvclttZXRob2RdLCBnZW5lcmF0b3IsIGFyZyk7XG4gICAgICBpZiAocmVjb3JkLnR5cGUgPT09IFwidGhyb3dcIikge1xuICAgICAgICByZWplY3QocmVjb3JkLmFyZyk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB2YXIgcmVzdWx0ID0gcmVjb3JkLmFyZztcbiAgICAgICAgdmFyIHZhbHVlID0gcmVzdWx0LnZhbHVlO1xuICAgICAgICBpZiAodmFsdWUgJiZcbiAgICAgICAgICAgIHR5cGVvZiB2YWx1ZSA9PT0gXCJvYmplY3RcIiAmJlxuICAgICAgICAgICAgaGFzT3duLmNhbGwodmFsdWUsIFwiX19hd2FpdFwiKSkge1xuICAgICAgICAgIHJldHVybiBQcm9taXNlSW1wbC5yZXNvbHZlKHZhbHVlLl9fYXdhaXQpLnRoZW4oZnVuY3Rpb24odmFsdWUpIHtcbiAgICAgICAgICAgIGludm9rZShcIm5leHRcIiwgdmFsdWUsIHJlc29sdmUsIHJlamVjdCk7XG4gICAgICAgICAgfSwgZnVuY3Rpb24oZXJyKSB7XG4gICAgICAgICAgICBpbnZva2UoXCJ0aHJvd1wiLCBlcnIsIHJlc29sdmUsIHJlamVjdCk7XG4gICAgICAgICAgfSk7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gUHJvbWlzZUltcGwucmVzb2x2ZSh2YWx1ZSkudGhlbihmdW5jdGlvbih1bndyYXBwZWQpIHtcbiAgICAgICAgICAvLyBXaGVuIGEgeWllbGRlZCBQcm9taXNlIGlzIHJlc29sdmVkLCBpdHMgZmluYWwgdmFsdWUgYmVjb21lc1xuICAgICAgICAgIC8vIHRoZSAudmFsdWUgb2YgdGhlIFByb21pc2U8e3ZhbHVlLGRvbmV9PiByZXN1bHQgZm9yIHRoZVxuICAgICAgICAgIC8vIGN1cnJlbnQgaXRlcmF0aW9uLlxuICAgICAgICAgIHJlc3VsdC52YWx1ZSA9IHVud3JhcHBlZDtcbiAgICAgICAgICByZXNvbHZlKHJlc3VsdCk7XG4gICAgICAgIH0sIGZ1bmN0aW9uKGVycm9yKSB7XG4gICAgICAgICAgLy8gSWYgYSByZWplY3RlZCBQcm9taXNlIHdhcyB5aWVsZGVkLCB0aHJvdyB0aGUgcmVqZWN0aW9uIGJhY2tcbiAgICAgICAgICAvLyBpbnRvIHRoZSBhc3luYyBnZW5lcmF0b3IgZnVuY3Rpb24gc28gaXQgY2FuIGJlIGhhbmRsZWQgdGhlcmUuXG4gICAgICAgICAgcmV0dXJuIGludm9rZShcInRocm93XCIsIGVycm9yLCByZXNvbHZlLCByZWplY3QpO1xuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICB2YXIgcHJldmlvdXNQcm9taXNlO1xuXG4gICAgZnVuY3Rpb24gZW5xdWV1ZShtZXRob2QsIGFyZykge1xuICAgICAgZnVuY3Rpb24gY2FsbEludm9rZVdpdGhNZXRob2RBbmRBcmcoKSB7XG4gICAgICAgIHJldHVybiBuZXcgUHJvbWlzZUltcGwoZnVuY3Rpb24ocmVzb2x2ZSwgcmVqZWN0KSB7XG4gICAgICAgICAgaW52b2tlKG1ldGhvZCwgYXJnLCByZXNvbHZlLCByZWplY3QpO1xuICAgICAgICB9KTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIHByZXZpb3VzUHJvbWlzZSA9XG4gICAgICAgIC8vIElmIGVucXVldWUgaGFzIGJlZW4gY2FsbGVkIGJlZm9yZSwgdGhlbiB3ZSB3YW50IHRvIHdhaXQgdW50aWxcbiAgICAgICAgLy8gYWxsIHByZXZpb3VzIFByb21pc2VzIGhhdmUgYmVlbiByZXNvbHZlZCBiZWZvcmUgY2FsbGluZyBpbnZva2UsXG4gICAgICAgIC8vIHNvIHRoYXQgcmVzdWx0cyBhcmUgYWx3YXlzIGRlbGl2ZXJlZCBpbiB0aGUgY29ycmVjdCBvcmRlci4gSWZcbiAgICAgICAgLy8gZW5xdWV1ZSBoYXMgbm90IGJlZW4gY2FsbGVkIGJlZm9yZSwgdGhlbiBpdCBpcyBpbXBvcnRhbnQgdG9cbiAgICAgICAgLy8gY2FsbCBpbnZva2UgaW1tZWRpYXRlbHksIHdpdGhvdXQgd2FpdGluZyBvbiBhIGNhbGxiYWNrIHRvIGZpcmUsXG4gICAgICAgIC8vIHNvIHRoYXQgdGhlIGFzeW5jIGdlbmVyYXRvciBmdW5jdGlvbiBoYXMgdGhlIG9wcG9ydHVuaXR5IHRvIGRvXG4gICAgICAgIC8vIGFueSBuZWNlc3Nhcnkgc2V0dXAgaW4gYSBwcmVkaWN0YWJsZSB3YXkuIFRoaXMgcHJlZGljdGFiaWxpdHlcbiAgICAgICAgLy8gaXMgd2h5IHRoZSBQcm9taXNlIGNvbnN0cnVjdG9yIHN5bmNocm9ub3VzbHkgaW52b2tlcyBpdHNcbiAgICAgICAgLy8gZXhlY3V0b3IgY2FsbGJhY2ssIGFuZCB3aHkgYXN5bmMgZnVuY3Rpb25zIHN5bmNocm9ub3VzbHlcbiAgICAgICAgLy8gZXhlY3V0ZSBjb2RlIGJlZm9yZSB0aGUgZmlyc3QgYXdhaXQuIFNpbmNlIHdlIGltcGxlbWVudCBzaW1wbGVcbiAgICAgICAgLy8gYXN5bmMgZnVuY3Rpb25zIGluIHRlcm1zIG9mIGFzeW5jIGdlbmVyYXRvcnMsIGl0IGlzIGVzcGVjaWFsbHlcbiAgICAgICAgLy8gaW1wb3J0YW50IHRvIGdldCB0aGlzIHJpZ2h0LCBldmVuIHRob3VnaCBpdCByZXF1aXJlcyBjYXJlLlxuICAgICAgICBwcmV2aW91c1Byb21pc2UgPyBwcmV2aW91c1Byb21pc2UudGhlbihcbiAgICAgICAgICBjYWxsSW52b2tlV2l0aE1ldGhvZEFuZEFyZyxcbiAgICAgICAgICAvLyBBdm9pZCBwcm9wYWdhdGluZyBmYWlsdXJlcyB0byBQcm9taXNlcyByZXR1cm5lZCBieSBsYXRlclxuICAgICAgICAgIC8vIGludm9jYXRpb25zIG9mIHRoZSBpdGVyYXRvci5cbiAgICAgICAgICBjYWxsSW52b2tlV2l0aE1ldGhvZEFuZEFyZ1xuICAgICAgICApIDogY2FsbEludm9rZVdpdGhNZXRob2RBbmRBcmcoKTtcbiAgICB9XG5cbiAgICAvLyBEZWZpbmUgdGhlIHVuaWZpZWQgaGVscGVyIG1ldGhvZCB0aGF0IGlzIHVzZWQgdG8gaW1wbGVtZW50IC5uZXh0LFxuICAgIC8vIC50aHJvdywgYW5kIC5yZXR1cm4gKHNlZSBkZWZpbmVJdGVyYXRvck1ldGhvZHMpLlxuICAgIHRoaXMuX2ludm9rZSA9IGVucXVldWU7XG4gIH1cblxuICBkZWZpbmVJdGVyYXRvck1ldGhvZHMoQXN5bmNJdGVyYXRvci5wcm90b3R5cGUpO1xuICBBc3luY0l0ZXJhdG9yLnByb3RvdHlwZVthc3luY0l0ZXJhdG9yU3ltYm9sXSA9IGZ1bmN0aW9uICgpIHtcbiAgICByZXR1cm4gdGhpcztcbiAgfTtcbiAgZXhwb3J0cy5Bc3luY0l0ZXJhdG9yID0gQXN5bmNJdGVyYXRvcjtcblxuICAvLyBOb3RlIHRoYXQgc2ltcGxlIGFzeW5jIGZ1bmN0aW9ucyBhcmUgaW1wbGVtZW50ZWQgb24gdG9wIG9mXG4gIC8vIEFzeW5jSXRlcmF0b3Igb2JqZWN0czsgdGhleSBqdXN0IHJldHVybiBhIFByb21pc2UgZm9yIHRoZSB2YWx1ZSBvZlxuICAvLyB0aGUgZmluYWwgcmVzdWx0IHByb2R1Y2VkIGJ5IHRoZSBpdGVyYXRvci5cbiAgZXhwb3J0cy5hc3luYyA9IGZ1bmN0aW9uKGlubmVyRm4sIG91dGVyRm4sIHNlbGYsIHRyeUxvY3NMaXN0LCBQcm9taXNlSW1wbCkge1xuICAgIGlmIChQcm9taXNlSW1wbCA9PT0gdm9pZCAwKSBQcm9taXNlSW1wbCA9IFByb21pc2U7XG5cbiAgICB2YXIgaXRlciA9IG5ldyBBc3luY0l0ZXJhdG9yKFxuICAgICAgd3JhcChpbm5lckZuLCBvdXRlckZuLCBzZWxmLCB0cnlMb2NzTGlzdCksXG4gICAgICBQcm9taXNlSW1wbFxuICAgICk7XG5cbiAgICByZXR1cm4gZXhwb3J0cy5pc0dlbmVyYXRvckZ1bmN0aW9uKG91dGVyRm4pXG4gICAgICA/IGl0ZXIgLy8gSWYgb3V0ZXJGbiBpcyBhIGdlbmVyYXRvciwgcmV0dXJuIHRoZSBmdWxsIGl0ZXJhdG9yLlxuICAgICAgOiBpdGVyLm5leHQoKS50aGVuKGZ1bmN0aW9uKHJlc3VsdCkge1xuICAgICAgICAgIHJldHVybiByZXN1bHQuZG9uZSA/IHJlc3VsdC52YWx1ZSA6IGl0ZXIubmV4dCgpO1xuICAgICAgICB9KTtcbiAgfTtcblxuICBmdW5jdGlvbiBtYWtlSW52b2tlTWV0aG9kKGlubmVyRm4sIHNlbGYsIGNvbnRleHQpIHtcbiAgICB2YXIgc3RhdGUgPSBHZW5TdGF0ZVN1c3BlbmRlZFN0YXJ0O1xuXG4gICAgcmV0dXJuIGZ1bmN0aW9uIGludm9rZShtZXRob2QsIGFyZykge1xuICAgICAgaWYgKHN0YXRlID09PSBHZW5TdGF0ZUV4ZWN1dGluZykge1xuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJHZW5lcmF0b3IgaXMgYWxyZWFkeSBydW5uaW5nXCIpO1xuICAgICAgfVxuXG4gICAgICBpZiAoc3RhdGUgPT09IEdlblN0YXRlQ29tcGxldGVkKSB7XG4gICAgICAgIGlmIChtZXRob2QgPT09IFwidGhyb3dcIikge1xuICAgICAgICAgIHRocm93IGFyZztcbiAgICAgICAgfVxuXG4gICAgICAgIC8vIEJlIGZvcmdpdmluZywgcGVyIDI1LjMuMy4zLjMgb2YgdGhlIHNwZWM6XG4gICAgICAgIC8vIGh0dHBzOi8vcGVvcGxlLm1vemlsbGEub3JnL35qb3JlbmRvcmZmL2VzNi1kcmFmdC5odG1sI3NlYy1nZW5lcmF0b3JyZXN1bWVcbiAgICAgICAgcmV0dXJuIGRvbmVSZXN1bHQoKTtcbiAgICAgIH1cblxuICAgICAgY29udGV4dC5tZXRob2QgPSBtZXRob2Q7XG4gICAgICBjb250ZXh0LmFyZyA9IGFyZztcblxuICAgICAgd2hpbGUgKHRydWUpIHtcbiAgICAgICAgdmFyIGRlbGVnYXRlID0gY29udGV4dC5kZWxlZ2F0ZTtcbiAgICAgICAgaWYgKGRlbGVnYXRlKSB7XG4gICAgICAgICAgdmFyIGRlbGVnYXRlUmVzdWx0ID0gbWF5YmVJbnZva2VEZWxlZ2F0ZShkZWxlZ2F0ZSwgY29udGV4dCk7XG4gICAgICAgICAgaWYgKGRlbGVnYXRlUmVzdWx0KSB7XG4gICAgICAgICAgICBpZiAoZGVsZWdhdGVSZXN1bHQgPT09IENvbnRpbnVlU2VudGluZWwpIGNvbnRpbnVlO1xuICAgICAgICAgICAgcmV0dXJuIGRlbGVnYXRlUmVzdWx0O1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChjb250ZXh0Lm1ldGhvZCA9PT0gXCJuZXh0XCIpIHtcbiAgICAgICAgICAvLyBTZXR0aW5nIGNvbnRleHQuX3NlbnQgZm9yIGxlZ2FjeSBzdXBwb3J0IG9mIEJhYmVsJ3NcbiAgICAgICAgICAvLyBmdW5jdGlvbi5zZW50IGltcGxlbWVudGF0aW9uLlxuICAgICAgICAgIGNvbnRleHQuc2VudCA9IGNvbnRleHQuX3NlbnQgPSBjb250ZXh0LmFyZztcblxuICAgICAgICB9IGVsc2UgaWYgKGNvbnRleHQubWV0aG9kID09PSBcInRocm93XCIpIHtcbiAgICAgICAgICBpZiAoc3RhdGUgPT09IEdlblN0YXRlU3VzcGVuZGVkU3RhcnQpIHtcbiAgICAgICAgICAgIHN0YXRlID0gR2VuU3RhdGVDb21wbGV0ZWQ7XG4gICAgICAgICAgICB0aHJvdyBjb250ZXh0LmFyZztcbiAgICAgICAgICB9XG5cbiAgICAgICAgICBjb250ZXh0LmRpc3BhdGNoRXhjZXB0aW9uKGNvbnRleHQuYXJnKTtcblxuICAgICAgICB9IGVsc2UgaWYgKGNvbnRleHQubWV0aG9kID09PSBcInJldHVyblwiKSB7XG4gICAgICAgICAgY29udGV4dC5hYnJ1cHQoXCJyZXR1cm5cIiwgY29udGV4dC5hcmcpO1xuICAgICAgICB9XG5cbiAgICAgICAgc3RhdGUgPSBHZW5TdGF0ZUV4ZWN1dGluZztcblxuICAgICAgICB2YXIgcmVjb3JkID0gdHJ5Q2F0Y2goaW5uZXJGbiwgc2VsZiwgY29udGV4dCk7XG4gICAgICAgIGlmIChyZWNvcmQudHlwZSA9PT0gXCJub3JtYWxcIikge1xuICAgICAgICAgIC8vIElmIGFuIGV4Y2VwdGlvbiBpcyB0aHJvd24gZnJvbSBpbm5lckZuLCB3ZSBsZWF2ZSBzdGF0ZSA9PT1cbiAgICAgICAgICAvLyBHZW5TdGF0ZUV4ZWN1dGluZyBhbmQgbG9vcCBiYWNrIGZvciBhbm90aGVyIGludm9jYXRpb24uXG4gICAgICAgICAgc3RhdGUgPSBjb250ZXh0LmRvbmVcbiAgICAgICAgICAgID8gR2VuU3RhdGVDb21wbGV0ZWRcbiAgICAgICAgICAgIDogR2VuU3RhdGVTdXNwZW5kZWRZaWVsZDtcblxuICAgICAgICAgIGlmIChyZWNvcmQuYXJnID09PSBDb250aW51ZVNlbnRpbmVsKSB7XG4gICAgICAgICAgICBjb250aW51ZTtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgdmFsdWU6IHJlY29yZC5hcmcsXG4gICAgICAgICAgICBkb25lOiBjb250ZXh0LmRvbmVcbiAgICAgICAgICB9O1xuXG4gICAgICAgIH0gZWxzZSBpZiAocmVjb3JkLnR5cGUgPT09IFwidGhyb3dcIikge1xuICAgICAgICAgIHN0YXRlID0gR2VuU3RhdGVDb21wbGV0ZWQ7XG4gICAgICAgICAgLy8gRGlzcGF0Y2ggdGhlIGV4Y2VwdGlvbiBieSBsb29waW5nIGJhY2sgYXJvdW5kIHRvIHRoZVxuICAgICAgICAgIC8vIGNvbnRleHQuZGlzcGF0Y2hFeGNlcHRpb24oY29udGV4dC5hcmcpIGNhbGwgYWJvdmUuXG4gICAgICAgICAgY29udGV4dC5tZXRob2QgPSBcInRocm93XCI7XG4gICAgICAgICAgY29udGV4dC5hcmcgPSByZWNvcmQuYXJnO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfTtcbiAgfVxuXG4gIC8vIENhbGwgZGVsZWdhdGUuaXRlcmF0b3JbY29udGV4dC5tZXRob2RdKGNvbnRleHQuYXJnKSBhbmQgaGFuZGxlIHRoZVxuICAvLyByZXN1bHQsIGVpdGhlciBieSByZXR1cm5pbmcgYSB7IHZhbHVlLCBkb25lIH0gcmVzdWx0IGZyb20gdGhlXG4gIC8vIGRlbGVnYXRlIGl0ZXJhdG9yLCBvciBieSBtb2RpZnlpbmcgY29udGV4dC5tZXRob2QgYW5kIGNvbnRleHQuYXJnLFxuICAvLyBzZXR0aW5nIGNvbnRleHQuZGVsZWdhdGUgdG8gbnVsbCwgYW5kIHJldHVybmluZyB0aGUgQ29udGludWVTZW50aW5lbC5cbiAgZnVuY3Rpb24gbWF5YmVJbnZva2VEZWxlZ2F0ZShkZWxlZ2F0ZSwgY29udGV4dCkge1xuICAgIHZhciBtZXRob2QgPSBkZWxlZ2F0ZS5pdGVyYXRvcltjb250ZXh0Lm1ldGhvZF07XG4gICAgaWYgKG1ldGhvZCA9PT0gdW5kZWZpbmVkKSB7XG4gICAgICAvLyBBIC50aHJvdyBvciAucmV0dXJuIHdoZW4gdGhlIGRlbGVnYXRlIGl0ZXJhdG9yIGhhcyBubyAudGhyb3dcbiAgICAgIC8vIG1ldGhvZCBhbHdheXMgdGVybWluYXRlcyB0aGUgeWllbGQqIGxvb3AuXG4gICAgICBjb250ZXh0LmRlbGVnYXRlID0gbnVsbDtcblxuICAgICAgaWYgKGNvbnRleHQubWV0aG9kID09PSBcInRocm93XCIpIHtcbiAgICAgICAgLy8gTm90ZTogW1wicmV0dXJuXCJdIG11c3QgYmUgdXNlZCBmb3IgRVMzIHBhcnNpbmcgY29tcGF0aWJpbGl0eS5cbiAgICAgICAgaWYgKGRlbGVnYXRlLml0ZXJhdG9yW1wicmV0dXJuXCJdKSB7XG4gICAgICAgICAgLy8gSWYgdGhlIGRlbGVnYXRlIGl0ZXJhdG9yIGhhcyBhIHJldHVybiBtZXRob2QsIGdpdmUgaXQgYVxuICAgICAgICAgIC8vIGNoYW5jZSB0byBjbGVhbiB1cC5cbiAgICAgICAgICBjb250ZXh0Lm1ldGhvZCA9IFwicmV0dXJuXCI7XG4gICAgICAgICAgY29udGV4dC5hcmcgPSB1bmRlZmluZWQ7XG4gICAgICAgICAgbWF5YmVJbnZva2VEZWxlZ2F0ZShkZWxlZ2F0ZSwgY29udGV4dCk7XG5cbiAgICAgICAgICBpZiAoY29udGV4dC5tZXRob2QgPT09IFwidGhyb3dcIikge1xuICAgICAgICAgICAgLy8gSWYgbWF5YmVJbnZva2VEZWxlZ2F0ZShjb250ZXh0KSBjaGFuZ2VkIGNvbnRleHQubWV0aG9kIGZyb21cbiAgICAgICAgICAgIC8vIFwicmV0dXJuXCIgdG8gXCJ0aHJvd1wiLCBsZXQgdGhhdCBvdmVycmlkZSB0aGUgVHlwZUVycm9yIGJlbG93LlxuICAgICAgICAgICAgcmV0dXJuIENvbnRpbnVlU2VudGluZWw7XG4gICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgY29udGV4dC5tZXRob2QgPSBcInRocm93XCI7XG4gICAgICAgIGNvbnRleHQuYXJnID0gbmV3IFR5cGVFcnJvcihcbiAgICAgICAgICBcIlRoZSBpdGVyYXRvciBkb2VzIG5vdCBwcm92aWRlIGEgJ3Rocm93JyBtZXRob2RcIik7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBDb250aW51ZVNlbnRpbmVsO1xuICAgIH1cblxuICAgIHZhciByZWNvcmQgPSB0cnlDYXRjaChtZXRob2QsIGRlbGVnYXRlLml0ZXJhdG9yLCBjb250ZXh0LmFyZyk7XG5cbiAgICBpZiAocmVjb3JkLnR5cGUgPT09IFwidGhyb3dcIikge1xuICAgICAgY29udGV4dC5tZXRob2QgPSBcInRocm93XCI7XG4gICAgICBjb250ZXh0LmFyZyA9IHJlY29yZC5hcmc7XG4gICAgICBjb250ZXh0LmRlbGVnYXRlID0gbnVsbDtcbiAgICAgIHJldHVybiBDb250aW51ZVNlbnRpbmVsO1xuICAgIH1cblxuICAgIHZhciBpbmZvID0gcmVjb3JkLmFyZztcblxuICAgIGlmICghIGluZm8pIHtcbiAgICAgIGNvbnRleHQubWV0aG9kID0gXCJ0aHJvd1wiO1xuICAgICAgY29udGV4dC5hcmcgPSBuZXcgVHlwZUVycm9yKFwiaXRlcmF0b3IgcmVzdWx0IGlzIG5vdCBhbiBvYmplY3RcIik7XG4gICAgICBjb250ZXh0LmRlbGVnYXRlID0gbnVsbDtcbiAgICAgIHJldHVybiBDb250aW51ZVNlbnRpbmVsO1xuICAgIH1cblxuICAgIGlmIChpbmZvLmRvbmUpIHtcbiAgICAgIC8vIEFzc2lnbiB0aGUgcmVzdWx0IG9mIHRoZSBmaW5pc2hlZCBkZWxlZ2F0ZSB0byB0aGUgdGVtcG9yYXJ5XG4gICAgICAvLyB2YXJpYWJsZSBzcGVjaWZpZWQgYnkgZGVsZWdhdGUucmVzdWx0TmFtZSAoc2VlIGRlbGVnYXRlWWllbGQpLlxuICAgICAgY29udGV4dFtkZWxlZ2F0ZS5yZXN1bHROYW1lXSA9IGluZm8udmFsdWU7XG5cbiAgICAgIC8vIFJlc3VtZSBleGVjdXRpb24gYXQgdGhlIGRlc2lyZWQgbG9jYXRpb24gKHNlZSBkZWxlZ2F0ZVlpZWxkKS5cbiAgICAgIGNvbnRleHQubmV4dCA9IGRlbGVnYXRlLm5leHRMb2M7XG5cbiAgICAgIC8vIElmIGNvbnRleHQubWV0aG9kIHdhcyBcInRocm93XCIgYnV0IHRoZSBkZWxlZ2F0ZSBoYW5kbGVkIHRoZVxuICAgICAgLy8gZXhjZXB0aW9uLCBsZXQgdGhlIG91dGVyIGdlbmVyYXRvciBwcm9jZWVkIG5vcm1hbGx5LiBJZlxuICAgICAgLy8gY29udGV4dC5tZXRob2Qgd2FzIFwibmV4dFwiLCBmb3JnZXQgY29udGV4dC5hcmcgc2luY2UgaXQgaGFzIGJlZW5cbiAgICAgIC8vIFwiY29uc3VtZWRcIiBieSB0aGUgZGVsZWdhdGUgaXRlcmF0b3IuIElmIGNvbnRleHQubWV0aG9kIHdhc1xuICAgICAgLy8gXCJyZXR1cm5cIiwgYWxsb3cgdGhlIG9yaWdpbmFsIC5yZXR1cm4gY2FsbCB0byBjb250aW51ZSBpbiB0aGVcbiAgICAgIC8vIG91dGVyIGdlbmVyYXRvci5cbiAgICAgIGlmIChjb250ZXh0Lm1ldGhvZCAhPT0gXCJyZXR1cm5cIikge1xuICAgICAgICBjb250ZXh0Lm1ldGhvZCA9IFwibmV4dFwiO1xuICAgICAgICBjb250ZXh0LmFyZyA9IHVuZGVmaW5lZDtcbiAgICAgIH1cblxuICAgIH0gZWxzZSB7XG4gICAgICAvLyBSZS15aWVsZCB0aGUgcmVzdWx0IHJldHVybmVkIGJ5IHRoZSBkZWxlZ2F0ZSBtZXRob2QuXG4gICAgICByZXR1cm4gaW5mbztcbiAgICB9XG5cbiAgICAvLyBUaGUgZGVsZWdhdGUgaXRlcmF0b3IgaXMgZmluaXNoZWQsIHNvIGZvcmdldCBpdCBhbmQgY29udGludWUgd2l0aFxuICAgIC8vIHRoZSBvdXRlciBnZW5lcmF0b3IuXG4gICAgY29udGV4dC5kZWxlZ2F0ZSA9IG51bGw7XG4gICAgcmV0dXJuIENvbnRpbnVlU2VudGluZWw7XG4gIH1cblxuICAvLyBEZWZpbmUgR2VuZXJhdG9yLnByb3RvdHlwZS57bmV4dCx0aHJvdyxyZXR1cm59IGluIHRlcm1zIG9mIHRoZVxuICAvLyB1bmlmaWVkIC5faW52b2tlIGhlbHBlciBtZXRob2QuXG4gIGRlZmluZUl0ZXJhdG9yTWV0aG9kcyhHcCk7XG5cbiAgR3BbdG9TdHJpbmdUYWdTeW1ib2xdID0gXCJHZW5lcmF0b3JcIjtcblxuICAvLyBBIEdlbmVyYXRvciBzaG91bGQgYWx3YXlzIHJldHVybiBpdHNlbGYgYXMgdGhlIGl0ZXJhdG9yIG9iamVjdCB3aGVuIHRoZVxuICAvLyBAQGl0ZXJhdG9yIGZ1bmN0aW9uIGlzIGNhbGxlZCBvbiBpdC4gU29tZSBicm93c2VycycgaW1wbGVtZW50YXRpb25zIG9mIHRoZVxuICAvLyBpdGVyYXRvciBwcm90b3R5cGUgY2hhaW4gaW5jb3JyZWN0bHkgaW1wbGVtZW50IHRoaXMsIGNhdXNpbmcgdGhlIEdlbmVyYXRvclxuICAvLyBvYmplY3QgdG8gbm90IGJlIHJldHVybmVkIGZyb20gdGhpcyBjYWxsLiBUaGlzIGVuc3VyZXMgdGhhdCBkb2Vzbid0IGhhcHBlbi5cbiAgLy8gU2VlIGh0dHBzOi8vZ2l0aHViLmNvbS9mYWNlYm9vay9yZWdlbmVyYXRvci9pc3N1ZXMvMjc0IGZvciBtb3JlIGRldGFpbHMuXG4gIEdwW2l0ZXJhdG9yU3ltYm9sXSA9IGZ1bmN0aW9uKCkge1xuICAgIHJldHVybiB0aGlzO1xuICB9O1xuXG4gIEdwLnRvU3RyaW5nID0gZnVuY3Rpb24oKSB7XG4gICAgcmV0dXJuIFwiW29iamVjdCBHZW5lcmF0b3JdXCI7XG4gIH07XG5cbiAgZnVuY3Rpb24gcHVzaFRyeUVudHJ5KGxvY3MpIHtcbiAgICB2YXIgZW50cnkgPSB7IHRyeUxvYzogbG9jc1swXSB9O1xuXG4gICAgaWYgKDEgaW4gbG9jcykge1xuICAgICAgZW50cnkuY2F0Y2hMb2MgPSBsb2NzWzFdO1xuICAgIH1cblxuICAgIGlmICgyIGluIGxvY3MpIHtcbiAgICAgIGVudHJ5LmZpbmFsbHlMb2MgPSBsb2NzWzJdO1xuICAgICAgZW50cnkuYWZ0ZXJMb2MgPSBsb2NzWzNdO1xuICAgIH1cblxuICAgIHRoaXMudHJ5RW50cmllcy5wdXNoKGVudHJ5KTtcbiAgfVxuXG4gIGZ1bmN0aW9uIHJlc2V0VHJ5RW50cnkoZW50cnkpIHtcbiAgICB2YXIgcmVjb3JkID0gZW50cnkuY29tcGxldGlvbiB8fCB7fTtcbiAgICByZWNvcmQudHlwZSA9IFwibm9ybWFsXCI7XG4gICAgZGVsZXRlIHJlY29yZC5hcmc7XG4gICAgZW50cnkuY29tcGxldGlvbiA9IHJlY29yZDtcbiAgfVxuXG4gIGZ1bmN0aW9uIENvbnRleHQodHJ5TG9jc0xpc3QpIHtcbiAgICAvLyBUaGUgcm9vdCBlbnRyeSBvYmplY3QgKGVmZmVjdGl2ZWx5IGEgdHJ5IHN0YXRlbWVudCB3aXRob3V0IGEgY2F0Y2hcbiAgICAvLyBvciBhIGZpbmFsbHkgYmxvY2spIGdpdmVzIHVzIGEgcGxhY2UgdG8gc3RvcmUgdmFsdWVzIHRocm93biBmcm9tXG4gICAgLy8gbG9jYXRpb25zIHdoZXJlIHRoZXJlIGlzIG5vIGVuY2xvc2luZyB0cnkgc3RhdGVtZW50LlxuICAgIHRoaXMudHJ5RW50cmllcyA9IFt7IHRyeUxvYzogXCJyb290XCIgfV07XG4gICAgdHJ5TG9jc0xpc3QuZm9yRWFjaChwdXNoVHJ5RW50cnksIHRoaXMpO1xuICAgIHRoaXMucmVzZXQodHJ1ZSk7XG4gIH1cblxuICBleHBvcnRzLmtleXMgPSBmdW5jdGlvbihvYmplY3QpIHtcbiAgICB2YXIga2V5cyA9IFtdO1xuICAgIGZvciAodmFyIGtleSBpbiBvYmplY3QpIHtcbiAgICAgIGtleXMucHVzaChrZXkpO1xuICAgIH1cbiAgICBrZXlzLnJldmVyc2UoKTtcblxuICAgIC8vIFJhdGhlciB0aGFuIHJldHVybmluZyBhbiBvYmplY3Qgd2l0aCBhIG5leHQgbWV0aG9kLCB3ZSBrZWVwXG4gICAgLy8gdGhpbmdzIHNpbXBsZSBhbmQgcmV0dXJuIHRoZSBuZXh0IGZ1bmN0aW9uIGl0c2VsZi5cbiAgICByZXR1cm4gZnVuY3Rpb24gbmV4dCgpIHtcbiAgICAgIHdoaWxlIChrZXlzLmxlbmd0aCkge1xuICAgICAgICB2YXIga2V5ID0ga2V5cy5wb3AoKTtcbiAgICAgICAgaWYgKGtleSBpbiBvYmplY3QpIHtcbiAgICAgICAgICBuZXh0LnZhbHVlID0ga2V5O1xuICAgICAgICAgIG5leHQuZG9uZSA9IGZhbHNlO1xuICAgICAgICAgIHJldHVybiBuZXh0O1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIC8vIFRvIGF2b2lkIGNyZWF0aW5nIGFuIGFkZGl0aW9uYWwgb2JqZWN0LCB3ZSBqdXN0IGhhbmcgdGhlIC52YWx1ZVxuICAgICAgLy8gYW5kIC5kb25lIHByb3BlcnRpZXMgb2ZmIHRoZSBuZXh0IGZ1bmN0aW9uIG9iamVjdCBpdHNlbGYuIFRoaXNcbiAgICAgIC8vIGFsc28gZW5zdXJlcyB0aGF0IHRoZSBtaW5pZmllciB3aWxsIG5vdCBhbm9ueW1pemUgdGhlIGZ1bmN0aW9uLlxuICAgICAgbmV4dC5kb25lID0gdHJ1ZTtcbiAgICAgIHJldHVybiBuZXh0O1xuICAgIH07XG4gIH07XG5cbiAgZnVuY3Rpb24gdmFsdWVzKGl0ZXJhYmxlKSB7XG4gICAgaWYgKGl0ZXJhYmxlKSB7XG4gICAgICB2YXIgaXRlcmF0b3JNZXRob2QgPSBpdGVyYWJsZVtpdGVyYXRvclN5bWJvbF07XG4gICAgICBpZiAoaXRlcmF0b3JNZXRob2QpIHtcbiAgICAgICAgcmV0dXJuIGl0ZXJhdG9yTWV0aG9kLmNhbGwoaXRlcmFibGUpO1xuICAgICAgfVxuXG4gICAgICBpZiAodHlwZW9mIGl0ZXJhYmxlLm5leHQgPT09IFwiZnVuY3Rpb25cIikge1xuICAgICAgICByZXR1cm4gaXRlcmFibGU7XG4gICAgICB9XG5cbiAgICAgIGlmICghaXNOYU4oaXRlcmFibGUubGVuZ3RoKSkge1xuICAgICAgICB2YXIgaSA9IC0xLCBuZXh0ID0gZnVuY3Rpb24gbmV4dCgpIHtcbiAgICAgICAgICB3aGlsZSAoKytpIDwgaXRlcmFibGUubGVuZ3RoKSB7XG4gICAgICAgICAgICBpZiAoaGFzT3duLmNhbGwoaXRlcmFibGUsIGkpKSB7XG4gICAgICAgICAgICAgIG5leHQudmFsdWUgPSBpdGVyYWJsZVtpXTtcbiAgICAgICAgICAgICAgbmV4dC5kb25lID0gZmFsc2U7XG4gICAgICAgICAgICAgIHJldHVybiBuZXh0O1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cblxuICAgICAgICAgIG5leHQudmFsdWUgPSB1bmRlZmluZWQ7XG4gICAgICAgICAgbmV4dC5kb25lID0gdHJ1ZTtcblxuICAgICAgICAgIHJldHVybiBuZXh0O1xuICAgICAgICB9O1xuXG4gICAgICAgIHJldHVybiBuZXh0Lm5leHQgPSBuZXh0O1xuICAgICAgfVxuICAgIH1cblxuICAgIC8vIFJldHVybiBhbiBpdGVyYXRvciB3aXRoIG5vIHZhbHVlcy5cbiAgICByZXR1cm4geyBuZXh0OiBkb25lUmVzdWx0IH07XG4gIH1cbiAgZXhwb3J0cy52YWx1ZXMgPSB2YWx1ZXM7XG5cbiAgZnVuY3Rpb24gZG9uZVJlc3VsdCgpIHtcbiAgICByZXR1cm4geyB2YWx1ZTogdW5kZWZpbmVkLCBkb25lOiB0cnVlIH07XG4gIH1cblxuICBDb250ZXh0LnByb3RvdHlwZSA9IHtcbiAgICBjb25zdHJ1Y3RvcjogQ29udGV4dCxcblxuICAgIHJlc2V0OiBmdW5jdGlvbihza2lwVGVtcFJlc2V0KSB7XG4gICAgICB0aGlzLnByZXYgPSAwO1xuICAgICAgdGhpcy5uZXh0ID0gMDtcbiAgICAgIC8vIFJlc2V0dGluZyBjb250ZXh0Ll9zZW50IGZvciBsZWdhY3kgc3VwcG9ydCBvZiBCYWJlbCdzXG4gICAgICAvLyBmdW5jdGlvbi5zZW50IGltcGxlbWVudGF0aW9uLlxuICAgICAgdGhpcy5zZW50ID0gdGhpcy5fc2VudCA9IHVuZGVmaW5lZDtcbiAgICAgIHRoaXMuZG9uZSA9IGZhbHNlO1xuICAgICAgdGhpcy5kZWxlZ2F0ZSA9IG51bGw7XG5cbiAgICAgIHRoaXMubWV0aG9kID0gXCJuZXh0XCI7XG4gICAgICB0aGlzLmFyZyA9IHVuZGVmaW5lZDtcblxuICAgICAgdGhpcy50cnlFbnRyaWVzLmZvckVhY2gocmVzZXRUcnlFbnRyeSk7XG5cbiAgICAgIGlmICghc2tpcFRlbXBSZXNldCkge1xuICAgICAgICBmb3IgKHZhciBuYW1lIGluIHRoaXMpIHtcbiAgICAgICAgICAvLyBOb3Qgc3VyZSBhYm91dCB0aGUgb3B0aW1hbCBvcmRlciBvZiB0aGVzZSBjb25kaXRpb25zOlxuICAgICAgICAgIGlmIChuYW1lLmNoYXJBdCgwKSA9PT0gXCJ0XCIgJiZcbiAgICAgICAgICAgICAgaGFzT3duLmNhbGwodGhpcywgbmFtZSkgJiZcbiAgICAgICAgICAgICAgIWlzTmFOKCtuYW1lLnNsaWNlKDEpKSkge1xuICAgICAgICAgICAgdGhpc1tuYW1lXSA9IHVuZGVmaW5lZDtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9LFxuXG4gICAgc3RvcDogZnVuY3Rpb24oKSB7XG4gICAgICB0aGlzLmRvbmUgPSB0cnVlO1xuXG4gICAgICB2YXIgcm9vdEVudHJ5ID0gdGhpcy50cnlFbnRyaWVzWzBdO1xuICAgICAgdmFyIHJvb3RSZWNvcmQgPSByb290RW50cnkuY29tcGxldGlvbjtcbiAgICAgIGlmIChyb290UmVjb3JkLnR5cGUgPT09IFwidGhyb3dcIikge1xuICAgICAgICB0aHJvdyByb290UmVjb3JkLmFyZztcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIHRoaXMucnZhbDtcbiAgICB9LFxuXG4gICAgZGlzcGF0Y2hFeGNlcHRpb246IGZ1bmN0aW9uKGV4Y2VwdGlvbikge1xuICAgICAgaWYgKHRoaXMuZG9uZSkge1xuICAgICAgICB0aHJvdyBleGNlcHRpb247XG4gICAgICB9XG5cbiAgICAgIHZhciBjb250ZXh0ID0gdGhpcztcbiAgICAgIGZ1bmN0aW9uIGhhbmRsZShsb2MsIGNhdWdodCkge1xuICAgICAgICByZWNvcmQudHlwZSA9IFwidGhyb3dcIjtcbiAgICAgICAgcmVjb3JkLmFyZyA9IGV4Y2VwdGlvbjtcbiAgICAgICAgY29udGV4dC5uZXh0ID0gbG9jO1xuXG4gICAgICAgIGlmIChjYXVnaHQpIHtcbiAgICAgICAgICAvLyBJZiB0aGUgZGlzcGF0Y2hlZCBleGNlcHRpb24gd2FzIGNhdWdodCBieSBhIGNhdGNoIGJsb2NrLFxuICAgICAgICAgIC8vIHRoZW4gbGV0IHRoYXQgY2F0Y2ggYmxvY2sgaGFuZGxlIHRoZSBleGNlcHRpb24gbm9ybWFsbHkuXG4gICAgICAgICAgY29udGV4dC5tZXRob2QgPSBcIm5leHRcIjtcbiAgICAgICAgICBjb250ZXh0LmFyZyA9IHVuZGVmaW5lZDtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiAhISBjYXVnaHQ7XG4gICAgICB9XG5cbiAgICAgIGZvciAodmFyIGkgPSB0aGlzLnRyeUVudHJpZXMubGVuZ3RoIC0gMTsgaSA+PSAwOyAtLWkpIHtcbiAgICAgICAgdmFyIGVudHJ5ID0gdGhpcy50cnlFbnRyaWVzW2ldO1xuICAgICAgICB2YXIgcmVjb3JkID0gZW50cnkuY29tcGxldGlvbjtcblxuICAgICAgICBpZiAoZW50cnkudHJ5TG9jID09PSBcInJvb3RcIikge1xuICAgICAgICAgIC8vIEV4Y2VwdGlvbiB0aHJvd24gb3V0c2lkZSBvZiBhbnkgdHJ5IGJsb2NrIHRoYXQgY291bGQgaGFuZGxlXG4gICAgICAgICAgLy8gaXQsIHNvIHNldCB0aGUgY29tcGxldGlvbiB2YWx1ZSBvZiB0aGUgZW50aXJlIGZ1bmN0aW9uIHRvXG4gICAgICAgICAgLy8gdGhyb3cgdGhlIGV4Y2VwdGlvbi5cbiAgICAgICAgICByZXR1cm4gaGFuZGxlKFwiZW5kXCIpO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKGVudHJ5LnRyeUxvYyA8PSB0aGlzLnByZXYpIHtcbiAgICAgICAgICB2YXIgaGFzQ2F0Y2ggPSBoYXNPd24uY2FsbChlbnRyeSwgXCJjYXRjaExvY1wiKTtcbiAgICAgICAgICB2YXIgaGFzRmluYWxseSA9IGhhc093bi5jYWxsKGVudHJ5LCBcImZpbmFsbHlMb2NcIik7XG5cbiAgICAgICAgICBpZiAoaGFzQ2F0Y2ggJiYgaGFzRmluYWxseSkge1xuICAgICAgICAgICAgaWYgKHRoaXMucHJldiA8IGVudHJ5LmNhdGNoTG9jKSB7XG4gICAgICAgICAgICAgIHJldHVybiBoYW5kbGUoZW50cnkuY2F0Y2hMb2MsIHRydWUpO1xuICAgICAgICAgICAgfSBlbHNlIGlmICh0aGlzLnByZXYgPCBlbnRyeS5maW5hbGx5TG9jKSB7XG4gICAgICAgICAgICAgIHJldHVybiBoYW5kbGUoZW50cnkuZmluYWxseUxvYyk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICB9IGVsc2UgaWYgKGhhc0NhdGNoKSB7XG4gICAgICAgICAgICBpZiAodGhpcy5wcmV2IDwgZW50cnkuY2F0Y2hMb2MpIHtcbiAgICAgICAgICAgICAgcmV0dXJuIGhhbmRsZShlbnRyeS5jYXRjaExvYywgdHJ1ZSk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICB9IGVsc2UgaWYgKGhhc0ZpbmFsbHkpIHtcbiAgICAgICAgICAgIGlmICh0aGlzLnByZXYgPCBlbnRyeS5maW5hbGx5TG9jKSB7XG4gICAgICAgICAgICAgIHJldHVybiBoYW5kbGUoZW50cnkuZmluYWxseUxvYyk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwidHJ5IHN0YXRlbWVudCB3aXRob3V0IGNhdGNoIG9yIGZpbmFsbHlcIik7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfSxcblxuICAgIGFicnVwdDogZnVuY3Rpb24odHlwZSwgYXJnKSB7XG4gICAgICBmb3IgKHZhciBpID0gdGhpcy50cnlFbnRyaWVzLmxlbmd0aCAtIDE7IGkgPj0gMDsgLS1pKSB7XG4gICAgICAgIHZhciBlbnRyeSA9IHRoaXMudHJ5RW50cmllc1tpXTtcbiAgICAgICAgaWYgKGVudHJ5LnRyeUxvYyA8PSB0aGlzLnByZXYgJiZcbiAgICAgICAgICAgIGhhc093bi5jYWxsKGVudHJ5LCBcImZpbmFsbHlMb2NcIikgJiZcbiAgICAgICAgICAgIHRoaXMucHJldiA8IGVudHJ5LmZpbmFsbHlMb2MpIHtcbiAgICAgICAgICB2YXIgZmluYWxseUVudHJ5ID0gZW50cnk7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgaWYgKGZpbmFsbHlFbnRyeSAmJlxuICAgICAgICAgICh0eXBlID09PSBcImJyZWFrXCIgfHxcbiAgICAgICAgICAgdHlwZSA9PT0gXCJjb250aW51ZVwiKSAmJlxuICAgICAgICAgIGZpbmFsbHlFbnRyeS50cnlMb2MgPD0gYXJnICYmXG4gICAgICAgICAgYXJnIDw9IGZpbmFsbHlFbnRyeS5maW5hbGx5TG9jKSB7XG4gICAgICAgIC8vIElnbm9yZSB0aGUgZmluYWxseSBlbnRyeSBpZiBjb250cm9sIGlzIG5vdCBqdW1waW5nIHRvIGFcbiAgICAgICAgLy8gbG9jYXRpb24gb3V0c2lkZSB0aGUgdHJ5L2NhdGNoIGJsb2NrLlxuICAgICAgICBmaW5hbGx5RW50cnkgPSBudWxsO1xuICAgICAgfVxuXG4gICAgICB2YXIgcmVjb3JkID0gZmluYWxseUVudHJ5ID8gZmluYWxseUVudHJ5LmNvbXBsZXRpb24gOiB7fTtcbiAgICAgIHJlY29yZC50eXBlID0gdHlwZTtcbiAgICAgIHJlY29yZC5hcmcgPSBhcmc7XG5cbiAgICAgIGlmIChmaW5hbGx5RW50cnkpIHtcbiAgICAgICAgdGhpcy5tZXRob2QgPSBcIm5leHRcIjtcbiAgICAgICAgdGhpcy5uZXh0ID0gZmluYWxseUVudHJ5LmZpbmFsbHlMb2M7XG4gICAgICAgIHJldHVybiBDb250aW51ZVNlbnRpbmVsO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gdGhpcy5jb21wbGV0ZShyZWNvcmQpO1xuICAgIH0sXG5cbiAgICBjb21wbGV0ZTogZnVuY3Rpb24ocmVjb3JkLCBhZnRlckxvYykge1xuICAgICAgaWYgKHJlY29yZC50eXBlID09PSBcInRocm93XCIpIHtcbiAgICAgICAgdGhyb3cgcmVjb3JkLmFyZztcbiAgICAgIH1cblxuICAgICAgaWYgKHJlY29yZC50eXBlID09PSBcImJyZWFrXCIgfHxcbiAgICAgICAgICByZWNvcmQudHlwZSA9PT0gXCJjb250aW51ZVwiKSB7XG4gICAgICAgIHRoaXMubmV4dCA9IHJlY29yZC5hcmc7XG4gICAgICB9IGVsc2UgaWYgKHJlY29yZC50eXBlID09PSBcInJldHVyblwiKSB7XG4gICAgICAgIHRoaXMucnZhbCA9IHRoaXMuYXJnID0gcmVjb3JkLmFyZztcbiAgICAgICAgdGhpcy5tZXRob2QgPSBcInJldHVyblwiO1xuICAgICAgICB0aGlzLm5leHQgPSBcImVuZFwiO1xuICAgICAgfSBlbHNlIGlmIChyZWNvcmQudHlwZSA9PT0gXCJub3JtYWxcIiAmJiBhZnRlckxvYykge1xuICAgICAgICB0aGlzLm5leHQgPSBhZnRlckxvYztcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIENvbnRpbnVlU2VudGluZWw7XG4gICAgfSxcblxuICAgIGZpbmlzaDogZnVuY3Rpb24oZmluYWxseUxvYykge1xuICAgICAgZm9yICh2YXIgaSA9IHRoaXMudHJ5RW50cmllcy5sZW5ndGggLSAxOyBpID49IDA7IC0taSkge1xuICAgICAgICB2YXIgZW50cnkgPSB0aGlzLnRyeUVudHJpZXNbaV07XG4gICAgICAgIGlmIChlbnRyeS5maW5hbGx5TG9jID09PSBmaW5hbGx5TG9jKSB7XG4gICAgICAgICAgdGhpcy5jb21wbGV0ZShlbnRyeS5jb21wbGV0aW9uLCBlbnRyeS5hZnRlckxvYyk7XG4gICAgICAgICAgcmVzZXRUcnlFbnRyeShlbnRyeSk7XG4gICAgICAgICAgcmV0dXJuIENvbnRpbnVlU2VudGluZWw7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9LFxuXG4gICAgXCJjYXRjaFwiOiBmdW5jdGlvbih0cnlMb2MpIHtcbiAgICAgIGZvciAodmFyIGkgPSB0aGlzLnRyeUVudHJpZXMubGVuZ3RoIC0gMTsgaSA+PSAwOyAtLWkpIHtcbiAgICAgICAgdmFyIGVudHJ5ID0gdGhpcy50cnlFbnRyaWVzW2ldO1xuICAgICAgICBpZiAoZW50cnkudHJ5TG9jID09PSB0cnlMb2MpIHtcbiAgICAgICAgICB2YXIgcmVjb3JkID0gZW50cnkuY29tcGxldGlvbjtcbiAgICAgICAgICBpZiAocmVjb3JkLnR5cGUgPT09IFwidGhyb3dcIikge1xuICAgICAgICAgICAgdmFyIHRocm93biA9IHJlY29yZC5hcmc7XG4gICAgICAgICAgICByZXNldFRyeUVudHJ5KGVudHJ5KTtcbiAgICAgICAgICB9XG4gICAgICAgICAgcmV0dXJuIHRocm93bjtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICAvLyBUaGUgY29udGV4dC5jYXRjaCBtZXRob2QgbXVzdCBvbmx5IGJlIGNhbGxlZCB3aXRoIGEgbG9jYXRpb25cbiAgICAgIC8vIGFyZ3VtZW50IHRoYXQgY29ycmVzcG9uZHMgdG8gYSBrbm93biBjYXRjaCBibG9jay5cbiAgICAgIHRocm93IG5ldyBFcnJvcihcImlsbGVnYWwgY2F0Y2ggYXR0ZW1wdFwiKTtcbiAgICB9LFxuXG4gICAgZGVsZWdhdGVZaWVsZDogZnVuY3Rpb24oaXRlcmFibGUsIHJlc3VsdE5hbWUsIG5leHRMb2MpIHtcbiAgICAgIHRoaXMuZGVsZWdhdGUgPSB7XG4gICAgICAgIGl0ZXJhdG9yOiB2YWx1ZXMoaXRlcmFibGUpLFxuICAgICAgICByZXN1bHROYW1lOiByZXN1bHROYW1lLFxuICAgICAgICBuZXh0TG9jOiBuZXh0TG9jXG4gICAgICB9O1xuXG4gICAgICBpZiAodGhpcy5tZXRob2QgPT09IFwibmV4dFwiKSB7XG4gICAgICAgIC8vIERlbGliZXJhdGVseSBmb3JnZXQgdGhlIGxhc3Qgc2VudCB2YWx1ZSBzbyB0aGF0IHdlIGRvbid0XG4gICAgICAgIC8vIGFjY2lkZW50YWxseSBwYXNzIGl0IG9uIHRvIHRoZSBkZWxlZ2F0ZS5cbiAgICAgICAgdGhpcy5hcmcgPSB1bmRlZmluZWQ7XG4gICAgICB9XG5cbiAgICAgIHJldHVybiBDb250aW51ZVNlbnRpbmVsO1xuICAgIH1cbiAgfTtcblxuICAvLyBSZWdhcmRsZXNzIG9mIHdoZXRoZXIgdGhpcyBzY3JpcHQgaXMgZXhlY3V0aW5nIGFzIGEgQ29tbW9uSlMgbW9kdWxlXG4gIC8vIG9yIG5vdCwgcmV0dXJuIHRoZSBydW50aW1lIG9iamVjdCBzbyB0aGF0IHdlIGNhbiBkZWNsYXJlIHRoZSB2YXJpYWJsZVxuICAvLyByZWdlbmVyYXRvclJ1bnRpbWUgaW4gdGhlIG91dGVyIHNjb3BlLCB3aGljaCBhbGxvd3MgdGhpcyBtb2R1bGUgdG8gYmVcbiAgLy8gaW5qZWN0ZWQgZWFzaWx5IGJ5IGBiaW4vcmVnZW5lcmF0b3IgLS1pbmNsdWRlLXJ1bnRpbWUgc2NyaXB0LmpzYC5cbiAgcmV0dXJuIGV4cG9ydHM7XG5cbn0oXG4gIC8vIElmIHRoaXMgc2NyaXB0IGlzIGV4ZWN1dGluZyBhcyBhIENvbW1vbkpTIG1vZHVsZSwgdXNlIG1vZHVsZS5leHBvcnRzXG4gIC8vIGFzIHRoZSByZWdlbmVyYXRvclJ1bnRpbWUgbmFtZXNwYWNlLiBPdGhlcndpc2UgY3JlYXRlIGEgbmV3IGVtcHR5XG4gIC8vIG9iamVjdC4gRWl0aGVyIHdheSwgdGhlIHJlc3VsdGluZyBvYmplY3Qgd2lsbCBiZSB1c2VkIHRvIGluaXRpYWxpemVcbiAgLy8gdGhlIHJlZ2VuZXJhdG9yUnVudGltZSB2YXJpYWJsZSBhdCB0aGUgdG9wIG9mIHRoaXMgZmlsZS5cbiAgdHlwZW9mIG1vZHVsZSA9PT0gXCJvYmplY3RcIiA/IG1vZHVsZS5leHBvcnRzIDoge31cbikpO1xuXG50cnkge1xuICByZWdlbmVyYXRvclJ1bnRpbWUgPSBydW50aW1lO1xufSBjYXRjaCAoYWNjaWRlbnRhbFN0cmljdE1vZGUpIHtcbiAgLy8gVGhpcyBtb2R1bGUgc2hvdWxkIG5vdCBiZSBydW5uaW5nIGluIHN0cmljdCBtb2RlLCBzbyB0aGUgYWJvdmVcbiAgLy8gYXNzaWdubWVudCBzaG91bGQgYWx3YXlzIHdvcmsgdW5sZXNzIHNvbWV0aGluZyBpcyBtaXNjb25maWd1cmVkLiBKdXN0XG4gIC8vIGluIGNhc2UgcnVudGltZS5qcyBhY2NpZGVudGFsbHkgcnVucyBpbiBzdHJpY3QgbW9kZSwgd2UgY2FuIGVzY2FwZVxuICAvLyBzdHJpY3QgbW9kZSB1c2luZyBhIGdsb2JhbCBGdW5jdGlvbiBjYWxsLiBUaGlzIGNvdWxkIGNvbmNlaXZhYmx5IGZhaWxcbiAgLy8gaWYgYSBDb250ZW50IFNlY3VyaXR5IFBvbGljeSBmb3JiaWRzIHVzaW5nIEZ1bmN0aW9uLCBidXQgaW4gdGhhdCBjYXNlXG4gIC8vIHRoZSBwcm9wZXIgc29sdXRpb24gaXMgdG8gZml4IHRoZSBhY2NpZGVudGFsIHN0cmljdCBtb2RlIHByb2JsZW0uIElmXG4gIC8vIHlvdSd2ZSBtaXNjb25maWd1cmVkIHlvdXIgYnVuZGxlciB0byBmb3JjZSBzdHJpY3QgbW9kZSBhbmQgYXBwbGllZCBhXG4gIC8vIENTUCB0byBmb3JiaWQgRnVuY3Rpb24sIGFuZCB5b3UncmUgbm90IHdpbGxpbmcgdG8gZml4IGVpdGhlciBvZiB0aG9zZVxuICAvLyBwcm9ibGVtcywgcGxlYXNlIGRldGFpbCB5b3VyIHVuaXF1ZSBwcmVkaWNhbWVudCBpbiBhIEdpdEh1YiBpc3N1ZS5cbiAgRnVuY3Rpb24oXCJyXCIsIFwicmVnZW5lcmF0b3JSdW50aW1lID0gclwiKShydW50aW1lKTtcbn1cbiIsIi8qISAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxyXG5Db3B5cmlnaHQgKGMpIE1pY3Jvc29mdCBDb3Jwb3JhdGlvbi4gQWxsIHJpZ2h0cyByZXNlcnZlZC5cclxuTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTsgeW91IG1heSBub3QgdXNlXHJcbnRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlXHJcbkxpY2Vuc2UgYXQgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcblxyXG5USElTIENPREUgSVMgUFJPVklERUQgT04gQU4gKkFTIElTKiBCQVNJUywgV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZXHJcbktJTkQsIEVJVEhFUiBFWFBSRVNTIE9SIElNUExJRUQsIElOQ0xVRElORyBXSVRIT1VUIExJTUlUQVRJT04gQU5ZIElNUExJRURcclxuV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIFRJVExFLCBGSVRORVNTIEZPUiBBIFBBUlRJQ1VMQVIgUFVSUE9TRSxcclxuTUVSQ0hBTlRBQkxJVFkgT1IgTk9OLUlORlJJTkdFTUVOVC5cclxuXHJcblNlZSB0aGUgQXBhY2hlIFZlcnNpb24gMi4wIExpY2Vuc2UgZm9yIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9uc1xyXG5hbmQgbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqICovXHJcbi8qIGdsb2JhbCBSZWZsZWN0LCBQcm9taXNlICovXHJcblxyXG52YXIgZXh0ZW5kU3RhdGljcyA9IGZ1bmN0aW9uKGQsIGIpIHtcclxuICAgIGV4dGVuZFN0YXRpY3MgPSBPYmplY3Quc2V0UHJvdG90eXBlT2YgfHxcclxuICAgICAgICAoeyBfX3Byb3RvX186IFtdIH0gaW5zdGFuY2VvZiBBcnJheSAmJiBmdW5jdGlvbiAoZCwgYikgeyBkLl9fcHJvdG9fXyA9IGI7IH0pIHx8XHJcbiAgICAgICAgZnVuY3Rpb24gKGQsIGIpIHsgZm9yICh2YXIgcCBpbiBiKSBpZiAoYi5oYXNPd25Qcm9wZXJ0eShwKSkgZFtwXSA9IGJbcF07IH07XHJcbiAgICByZXR1cm4gZXh0ZW5kU3RhdGljcyhkLCBiKTtcclxufTtcclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX2V4dGVuZHMoZCwgYikge1xyXG4gICAgZXh0ZW5kU3RhdGljcyhkLCBiKTtcclxuICAgIGZ1bmN0aW9uIF9fKCkgeyB0aGlzLmNvbnN0cnVjdG9yID0gZDsgfVxyXG4gICAgZC5wcm90b3R5cGUgPSBiID09PSBudWxsID8gT2JqZWN0LmNyZWF0ZShiKSA6IChfXy5wcm90b3R5cGUgPSBiLnByb3RvdHlwZSwgbmV3IF9fKCkpO1xyXG59XHJcblxyXG5leHBvcnQgdmFyIF9fYXNzaWduID0gZnVuY3Rpb24oKSB7XHJcbiAgICBfX2Fzc2lnbiA9IE9iamVjdC5hc3NpZ24gfHwgZnVuY3Rpb24gX19hc3NpZ24odCkge1xyXG4gICAgICAgIGZvciAodmFyIHMsIGkgPSAxLCBuID0gYXJndW1lbnRzLmxlbmd0aDsgaSA8IG47IGkrKykge1xyXG4gICAgICAgICAgICBzID0gYXJndW1lbnRzW2ldO1xyXG4gICAgICAgICAgICBmb3IgKHZhciBwIGluIHMpIGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwocywgcCkpIHRbcF0gPSBzW3BdO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdDtcclxuICAgIH1cclxuICAgIHJldHVybiBfX2Fzc2lnbi5hcHBseSh0aGlzLCBhcmd1bWVudHMpO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19yZXN0KHMsIGUpIHtcclxuICAgIHZhciB0ID0ge307XHJcbiAgICBmb3IgKHZhciBwIGluIHMpIGlmIChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwocywgcCkgJiYgZS5pbmRleE9mKHApIDwgMClcclxuICAgICAgICB0W3BdID0gc1twXTtcclxuICAgIGlmIChzICE9IG51bGwgJiYgdHlwZW9mIE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMgPT09IFwiZnVuY3Rpb25cIilcclxuICAgICAgICBmb3IgKHZhciBpID0gMCwgcCA9IE9iamVjdC5nZXRPd25Qcm9wZXJ0eVN5bWJvbHMocyk7IGkgPCBwLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgIGlmIChlLmluZGV4T2YocFtpXSkgPCAwICYmIE9iamVjdC5wcm90b3R5cGUucHJvcGVydHlJc0VudW1lcmFibGUuY2FsbChzLCBwW2ldKSlcclxuICAgICAgICAgICAgICAgIHRbcFtpXV0gPSBzW3BbaV1dO1xyXG4gICAgICAgIH1cclxuICAgIHJldHVybiB0O1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19kZWNvcmF0ZShkZWNvcmF0b3JzLCB0YXJnZXQsIGtleSwgZGVzYykge1xyXG4gICAgdmFyIGMgPSBhcmd1bWVudHMubGVuZ3RoLCByID0gYyA8IDMgPyB0YXJnZXQgOiBkZXNjID09PSBudWxsID8gZGVzYyA9IE9iamVjdC5nZXRPd25Qcm9wZXJ0eURlc2NyaXB0b3IodGFyZ2V0LCBrZXkpIDogZGVzYywgZDtcclxuICAgIGlmICh0eXBlb2YgUmVmbGVjdCA9PT0gXCJvYmplY3RcIiAmJiB0eXBlb2YgUmVmbGVjdC5kZWNvcmF0ZSA9PT0gXCJmdW5jdGlvblwiKSByID0gUmVmbGVjdC5kZWNvcmF0ZShkZWNvcmF0b3JzLCB0YXJnZXQsIGtleSwgZGVzYyk7XHJcbiAgICBlbHNlIGZvciAodmFyIGkgPSBkZWNvcmF0b3JzLmxlbmd0aCAtIDE7IGkgPj0gMDsgaS0tKSBpZiAoZCA9IGRlY29yYXRvcnNbaV0pIHIgPSAoYyA8IDMgPyBkKHIpIDogYyA+IDMgPyBkKHRhcmdldCwga2V5LCByKSA6IGQodGFyZ2V0LCBrZXkpKSB8fCByO1xyXG4gICAgcmV0dXJuIGMgPiAzICYmIHIgJiYgT2JqZWN0LmRlZmluZVByb3BlcnR5KHRhcmdldCwga2V5LCByKSwgcjtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9fcGFyYW0ocGFyYW1JbmRleCwgZGVjb3JhdG9yKSB7XHJcbiAgICByZXR1cm4gZnVuY3Rpb24gKHRhcmdldCwga2V5KSB7IGRlY29yYXRvcih0YXJnZXQsIGtleSwgcGFyYW1JbmRleCk7IH1cclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9fbWV0YWRhdGEobWV0YWRhdGFLZXksIG1ldGFkYXRhVmFsdWUpIHtcclxuICAgIGlmICh0eXBlb2YgUmVmbGVjdCA9PT0gXCJvYmplY3RcIiAmJiB0eXBlb2YgUmVmbGVjdC5tZXRhZGF0YSA9PT0gXCJmdW5jdGlvblwiKSByZXR1cm4gUmVmbGVjdC5tZXRhZGF0YShtZXRhZGF0YUtleSwgbWV0YWRhdGFWYWx1ZSk7XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX2F3YWl0ZXIodGhpc0FyZywgX2FyZ3VtZW50cywgUCwgZ2VuZXJhdG9yKSB7XHJcbiAgICBmdW5jdGlvbiBhZG9wdCh2YWx1ZSkgeyByZXR1cm4gdmFsdWUgaW5zdGFuY2VvZiBQID8gdmFsdWUgOiBuZXcgUChmdW5jdGlvbiAocmVzb2x2ZSkgeyByZXNvbHZlKHZhbHVlKTsgfSk7IH1cclxuICAgIHJldHVybiBuZXcgKFAgfHwgKFAgPSBQcm9taXNlKSkoZnVuY3Rpb24gKHJlc29sdmUsIHJlamVjdCkge1xyXG4gICAgICAgIGZ1bmN0aW9uIGZ1bGZpbGxlZCh2YWx1ZSkgeyB0cnkgeyBzdGVwKGdlbmVyYXRvci5uZXh0KHZhbHVlKSk7IH0gY2F0Y2ggKGUpIHsgcmVqZWN0KGUpOyB9IH1cclxuICAgICAgICBmdW5jdGlvbiByZWplY3RlZCh2YWx1ZSkgeyB0cnkgeyBzdGVwKGdlbmVyYXRvcltcInRocm93XCJdKHZhbHVlKSk7IH0gY2F0Y2ggKGUpIHsgcmVqZWN0KGUpOyB9IH1cclxuICAgICAgICBmdW5jdGlvbiBzdGVwKHJlc3VsdCkgeyByZXN1bHQuZG9uZSA/IHJlc29sdmUocmVzdWx0LnZhbHVlKSA6IGFkb3B0KHJlc3VsdC52YWx1ZSkudGhlbihmdWxmaWxsZWQsIHJlamVjdGVkKTsgfVxyXG4gICAgICAgIHN0ZXAoKGdlbmVyYXRvciA9IGdlbmVyYXRvci5hcHBseSh0aGlzQXJnLCBfYXJndW1lbnRzIHx8IFtdKSkubmV4dCgpKTtcclxuICAgIH0pO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19nZW5lcmF0b3IodGhpc0FyZywgYm9keSkge1xyXG4gICAgdmFyIF8gPSB7IGxhYmVsOiAwLCBzZW50OiBmdW5jdGlvbigpIHsgaWYgKHRbMF0gJiAxKSB0aHJvdyB0WzFdOyByZXR1cm4gdFsxXTsgfSwgdHJ5czogW10sIG9wczogW10gfSwgZiwgeSwgdCwgZztcclxuICAgIHJldHVybiBnID0geyBuZXh0OiB2ZXJiKDApLCBcInRocm93XCI6IHZlcmIoMSksIFwicmV0dXJuXCI6IHZlcmIoMikgfSwgdHlwZW9mIFN5bWJvbCA9PT0gXCJmdW5jdGlvblwiICYmIChnW1N5bWJvbC5pdGVyYXRvcl0gPSBmdW5jdGlvbigpIHsgcmV0dXJuIHRoaXM7IH0pLCBnO1xyXG4gICAgZnVuY3Rpb24gdmVyYihuKSB7IHJldHVybiBmdW5jdGlvbiAodikgeyByZXR1cm4gc3RlcChbbiwgdl0pOyB9OyB9XHJcbiAgICBmdW5jdGlvbiBzdGVwKG9wKSB7XHJcbiAgICAgICAgaWYgKGYpIHRocm93IG5ldyBUeXBlRXJyb3IoXCJHZW5lcmF0b3IgaXMgYWxyZWFkeSBleGVjdXRpbmcuXCIpO1xyXG4gICAgICAgIHdoaWxlIChfKSB0cnkge1xyXG4gICAgICAgICAgICBpZiAoZiA9IDEsIHkgJiYgKHQgPSBvcFswXSAmIDIgPyB5W1wicmV0dXJuXCJdIDogb3BbMF0gPyB5W1widGhyb3dcIl0gfHwgKCh0ID0geVtcInJldHVyblwiXSkgJiYgdC5jYWxsKHkpLCAwKSA6IHkubmV4dCkgJiYgISh0ID0gdC5jYWxsKHksIG9wWzFdKSkuZG9uZSkgcmV0dXJuIHQ7XHJcbiAgICAgICAgICAgIGlmICh5ID0gMCwgdCkgb3AgPSBbb3BbMF0gJiAyLCB0LnZhbHVlXTtcclxuICAgICAgICAgICAgc3dpdGNoIChvcFswXSkge1xyXG4gICAgICAgICAgICAgICAgY2FzZSAwOiBjYXNlIDE6IHQgPSBvcDsgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICBjYXNlIDQ6IF8ubGFiZWwrKzsgcmV0dXJuIHsgdmFsdWU6IG9wWzFdLCBkb25lOiBmYWxzZSB9O1xyXG4gICAgICAgICAgICAgICAgY2FzZSA1OiBfLmxhYmVsKys7IHkgPSBvcFsxXTsgb3AgPSBbMF07IGNvbnRpbnVlO1xyXG4gICAgICAgICAgICAgICAgY2FzZSA3OiBvcCA9IF8ub3BzLnBvcCgpOyBfLnRyeXMucG9wKCk7IGNvbnRpbnVlO1xyXG4gICAgICAgICAgICAgICAgZGVmYXVsdDpcclxuICAgICAgICAgICAgICAgICAgICBpZiAoISh0ID0gXy50cnlzLCB0ID0gdC5sZW5ndGggPiAwICYmIHRbdC5sZW5ndGggLSAxXSkgJiYgKG9wWzBdID09PSA2IHx8IG9wWzBdID09PSAyKSkgeyBfID0gMDsgY29udGludWU7IH1cclxuICAgICAgICAgICAgICAgICAgICBpZiAob3BbMF0gPT09IDMgJiYgKCF0IHx8IChvcFsxXSA+IHRbMF0gJiYgb3BbMV0gPCB0WzNdKSkpIHsgXy5sYWJlbCA9IG9wWzFdOyBicmVhazsgfVxyXG4gICAgICAgICAgICAgICAgICAgIGlmIChvcFswXSA9PT0gNiAmJiBfLmxhYmVsIDwgdFsxXSkgeyBfLmxhYmVsID0gdFsxXTsgdCA9IG9wOyBicmVhazsgfVxyXG4gICAgICAgICAgICAgICAgICAgIGlmICh0ICYmIF8ubGFiZWwgPCB0WzJdKSB7IF8ubGFiZWwgPSB0WzJdOyBfLm9wcy5wdXNoKG9wKTsgYnJlYWs7IH1cclxuICAgICAgICAgICAgICAgICAgICBpZiAodFsyXSkgXy5vcHMucG9wKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgXy50cnlzLnBvcCgpOyBjb250aW51ZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBvcCA9IGJvZHkuY2FsbCh0aGlzQXJnLCBfKTtcclxuICAgICAgICB9IGNhdGNoIChlKSB7IG9wID0gWzYsIGVdOyB5ID0gMDsgfSBmaW5hbGx5IHsgZiA9IHQgPSAwOyB9XHJcbiAgICAgICAgaWYgKG9wWzBdICYgNSkgdGhyb3cgb3BbMV07IHJldHVybiB7IHZhbHVlOiBvcFswXSA/IG9wWzFdIDogdm9pZCAwLCBkb25lOiB0cnVlIH07XHJcbiAgICB9XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX2V4cG9ydFN0YXIobSwgZXhwb3J0cykge1xyXG4gICAgZm9yICh2YXIgcCBpbiBtKSBpZiAoIWV4cG9ydHMuaGFzT3duUHJvcGVydHkocCkpIGV4cG9ydHNbcF0gPSBtW3BdO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX192YWx1ZXMobykge1xyXG4gICAgdmFyIHMgPSB0eXBlb2YgU3ltYm9sID09PSBcImZ1bmN0aW9uXCIgJiYgU3ltYm9sLml0ZXJhdG9yLCBtID0gcyAmJiBvW3NdLCBpID0gMDtcclxuICAgIGlmIChtKSByZXR1cm4gbS5jYWxsKG8pO1xyXG4gICAgaWYgKG8gJiYgdHlwZW9mIG8ubGVuZ3RoID09PSBcIm51bWJlclwiKSByZXR1cm4ge1xyXG4gICAgICAgIG5leHQ6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgaWYgKG8gJiYgaSA+PSBvLmxlbmd0aCkgbyA9IHZvaWQgMDtcclxuICAgICAgICAgICAgcmV0dXJuIHsgdmFsdWU6IG8gJiYgb1tpKytdLCBkb25lOiAhbyB9O1xyXG4gICAgICAgIH1cclxuICAgIH07XHJcbiAgICB0aHJvdyBuZXcgVHlwZUVycm9yKHMgPyBcIk9iamVjdCBpcyBub3QgaXRlcmFibGUuXCIgOiBcIlN5bWJvbC5pdGVyYXRvciBpcyBub3QgZGVmaW5lZC5cIik7XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX3JlYWQobywgbikge1xyXG4gICAgdmFyIG0gPSB0eXBlb2YgU3ltYm9sID09PSBcImZ1bmN0aW9uXCIgJiYgb1tTeW1ib2wuaXRlcmF0b3JdO1xyXG4gICAgaWYgKCFtKSByZXR1cm4gbztcclxuICAgIHZhciBpID0gbS5jYWxsKG8pLCByLCBhciA9IFtdLCBlO1xyXG4gICAgdHJ5IHtcclxuICAgICAgICB3aGlsZSAoKG4gPT09IHZvaWQgMCB8fCBuLS0gPiAwKSAmJiAhKHIgPSBpLm5leHQoKSkuZG9uZSkgYXIucHVzaChyLnZhbHVlKTtcclxuICAgIH1cclxuICAgIGNhdGNoIChlcnJvcikgeyBlID0geyBlcnJvcjogZXJyb3IgfTsgfVxyXG4gICAgZmluYWxseSB7XHJcbiAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgaWYgKHIgJiYgIXIuZG9uZSAmJiAobSA9IGlbXCJyZXR1cm5cIl0pKSBtLmNhbGwoaSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGZpbmFsbHkgeyBpZiAoZSkgdGhyb3cgZS5lcnJvcjsgfVxyXG4gICAgfVxyXG4gICAgcmV0dXJuIGFyO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19zcHJlYWQoKSB7XHJcbiAgICBmb3IgKHZhciBhciA9IFtdLCBpID0gMDsgaSA8IGFyZ3VtZW50cy5sZW5ndGg7IGkrKylcclxuICAgICAgICBhciA9IGFyLmNvbmNhdChfX3JlYWQoYXJndW1lbnRzW2ldKSk7XHJcbiAgICByZXR1cm4gYXI7XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX3NwcmVhZEFycmF5cygpIHtcclxuICAgIGZvciAodmFyIHMgPSAwLCBpID0gMCwgaWwgPSBhcmd1bWVudHMubGVuZ3RoOyBpIDwgaWw7IGkrKykgcyArPSBhcmd1bWVudHNbaV0ubGVuZ3RoO1xyXG4gICAgZm9yICh2YXIgciA9IEFycmF5KHMpLCBrID0gMCwgaSA9IDA7IGkgPCBpbDsgaSsrKVxyXG4gICAgICAgIGZvciAodmFyIGEgPSBhcmd1bWVudHNbaV0sIGogPSAwLCBqbCA9IGEubGVuZ3RoOyBqIDwgamw7IGorKywgaysrKVxyXG4gICAgICAgICAgICByW2tdID0gYVtqXTtcclxuICAgIHJldHVybiByO1xyXG59O1xyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9fYXdhaXQodikge1xyXG4gICAgcmV0dXJuIHRoaXMgaW5zdGFuY2VvZiBfX2F3YWl0ID8gKHRoaXMudiA9IHYsIHRoaXMpIDogbmV3IF9fYXdhaXQodik7XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX2FzeW5jR2VuZXJhdG9yKHRoaXNBcmcsIF9hcmd1bWVudHMsIGdlbmVyYXRvcikge1xyXG4gICAgaWYgKCFTeW1ib2wuYXN5bmNJdGVyYXRvcikgdGhyb3cgbmV3IFR5cGVFcnJvcihcIlN5bWJvbC5hc3luY0l0ZXJhdG9yIGlzIG5vdCBkZWZpbmVkLlwiKTtcclxuICAgIHZhciBnID0gZ2VuZXJhdG9yLmFwcGx5KHRoaXNBcmcsIF9hcmd1bWVudHMgfHwgW10pLCBpLCBxID0gW107XHJcbiAgICByZXR1cm4gaSA9IHt9LCB2ZXJiKFwibmV4dFwiKSwgdmVyYihcInRocm93XCIpLCB2ZXJiKFwicmV0dXJuXCIpLCBpW1N5bWJvbC5hc3luY0l0ZXJhdG9yXSA9IGZ1bmN0aW9uICgpIHsgcmV0dXJuIHRoaXM7IH0sIGk7XHJcbiAgICBmdW5jdGlvbiB2ZXJiKG4pIHsgaWYgKGdbbl0pIGlbbl0gPSBmdW5jdGlvbiAodikgeyByZXR1cm4gbmV3IFByb21pc2UoZnVuY3Rpb24gKGEsIGIpIHsgcS5wdXNoKFtuLCB2LCBhLCBiXSkgPiAxIHx8IHJlc3VtZShuLCB2KTsgfSk7IH07IH1cclxuICAgIGZ1bmN0aW9uIHJlc3VtZShuLCB2KSB7IHRyeSB7IHN0ZXAoZ1tuXSh2KSk7IH0gY2F0Y2ggKGUpIHsgc2V0dGxlKHFbMF1bM10sIGUpOyB9IH1cclxuICAgIGZ1bmN0aW9uIHN0ZXAocikgeyByLnZhbHVlIGluc3RhbmNlb2YgX19hd2FpdCA/IFByb21pc2UucmVzb2x2ZShyLnZhbHVlLnYpLnRoZW4oZnVsZmlsbCwgcmVqZWN0KSA6IHNldHRsZShxWzBdWzJdLCByKTsgfVxyXG4gICAgZnVuY3Rpb24gZnVsZmlsbCh2YWx1ZSkgeyByZXN1bWUoXCJuZXh0XCIsIHZhbHVlKTsgfVxyXG4gICAgZnVuY3Rpb24gcmVqZWN0KHZhbHVlKSB7IHJlc3VtZShcInRocm93XCIsIHZhbHVlKTsgfVxyXG4gICAgZnVuY3Rpb24gc2V0dGxlKGYsIHYpIHsgaWYgKGYodiksIHEuc2hpZnQoKSwgcS5sZW5ndGgpIHJlc3VtZShxWzBdWzBdLCBxWzBdWzFdKTsgfVxyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19hc3luY0RlbGVnYXRvcihvKSB7XHJcbiAgICB2YXIgaSwgcDtcclxuICAgIHJldHVybiBpID0ge30sIHZlcmIoXCJuZXh0XCIpLCB2ZXJiKFwidGhyb3dcIiwgZnVuY3Rpb24gKGUpIHsgdGhyb3cgZTsgfSksIHZlcmIoXCJyZXR1cm5cIiksIGlbU3ltYm9sLml0ZXJhdG9yXSA9IGZ1bmN0aW9uICgpIHsgcmV0dXJuIHRoaXM7IH0sIGk7XHJcbiAgICBmdW5jdGlvbiB2ZXJiKG4sIGYpIHsgaVtuXSA9IG9bbl0gPyBmdW5jdGlvbiAodikgeyByZXR1cm4gKHAgPSAhcCkgPyB7IHZhbHVlOiBfX2F3YWl0KG9bbl0odikpLCBkb25lOiBuID09PSBcInJldHVyblwiIH0gOiBmID8gZih2KSA6IHY7IH0gOiBmOyB9XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBfX2FzeW5jVmFsdWVzKG8pIHtcclxuICAgIGlmICghU3ltYm9sLmFzeW5jSXRlcmF0b3IpIHRocm93IG5ldyBUeXBlRXJyb3IoXCJTeW1ib2wuYXN5bmNJdGVyYXRvciBpcyBub3QgZGVmaW5lZC5cIik7XHJcbiAgICB2YXIgbSA9IG9bU3ltYm9sLmFzeW5jSXRlcmF0b3JdLCBpO1xyXG4gICAgcmV0dXJuIG0gPyBtLmNhbGwobykgOiAobyA9IHR5cGVvZiBfX3ZhbHVlcyA9PT0gXCJmdW5jdGlvblwiID8gX192YWx1ZXMobykgOiBvW1N5bWJvbC5pdGVyYXRvcl0oKSwgaSA9IHt9LCB2ZXJiKFwibmV4dFwiKSwgdmVyYihcInRocm93XCIpLCB2ZXJiKFwicmV0dXJuXCIpLCBpW1N5bWJvbC5hc3luY0l0ZXJhdG9yXSA9IGZ1bmN0aW9uICgpIHsgcmV0dXJuIHRoaXM7IH0sIGkpO1xyXG4gICAgZnVuY3Rpb24gdmVyYihuKSB7IGlbbl0gPSBvW25dICYmIGZ1bmN0aW9uICh2KSB7IHJldHVybiBuZXcgUHJvbWlzZShmdW5jdGlvbiAocmVzb2x2ZSwgcmVqZWN0KSB7IHYgPSBvW25dKHYpLCBzZXR0bGUocmVzb2x2ZSwgcmVqZWN0LCB2LmRvbmUsIHYudmFsdWUpOyB9KTsgfTsgfVxyXG4gICAgZnVuY3Rpb24gc2V0dGxlKHJlc29sdmUsIHJlamVjdCwgZCwgdikgeyBQcm9taXNlLnJlc29sdmUodikudGhlbihmdW5jdGlvbih2KSB7IHJlc29sdmUoeyB2YWx1ZTogdiwgZG9uZTogZCB9KTsgfSwgcmVqZWN0KTsgfVxyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19tYWtlVGVtcGxhdGVPYmplY3QoY29va2VkLCByYXcpIHtcclxuICAgIGlmIChPYmplY3QuZGVmaW5lUHJvcGVydHkpIHsgT2JqZWN0LmRlZmluZVByb3BlcnR5KGNvb2tlZCwgXCJyYXdcIiwgeyB2YWx1ZTogcmF3IH0pOyB9IGVsc2UgeyBjb29rZWQucmF3ID0gcmF3OyB9XHJcbiAgICByZXR1cm4gY29va2VkO1xyXG59O1xyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9faW1wb3J0U3Rhcihtb2QpIHtcclxuICAgIGlmIChtb2QgJiYgbW9kLl9fZXNNb2R1bGUpIHJldHVybiBtb2Q7XHJcbiAgICB2YXIgcmVzdWx0ID0ge307XHJcbiAgICBpZiAobW9kICE9IG51bGwpIGZvciAodmFyIGsgaW4gbW9kKSBpZiAoT2JqZWN0Lmhhc093blByb3BlcnR5LmNhbGwobW9kLCBrKSkgcmVzdWx0W2tdID0gbW9kW2tdO1xyXG4gICAgcmVzdWx0LmRlZmF1bHQgPSBtb2Q7XHJcbiAgICByZXR1cm4gcmVzdWx0O1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19pbXBvcnREZWZhdWx0KG1vZCkge1xyXG4gICAgcmV0dXJuIChtb2QgJiYgbW9kLl9fZXNNb2R1bGUpID8gbW9kIDogeyBkZWZhdWx0OiBtb2QgfTtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIF9fY2xhc3NQcml2YXRlRmllbGRHZXQocmVjZWl2ZXIsIHByaXZhdGVNYXApIHtcclxuICAgIGlmICghcHJpdmF0ZU1hcC5oYXMocmVjZWl2ZXIpKSB7XHJcbiAgICAgICAgdGhyb3cgbmV3IFR5cGVFcnJvcihcImF0dGVtcHRlZCB0byBnZXQgcHJpdmF0ZSBmaWVsZCBvbiBub24taW5zdGFuY2VcIik7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gcHJpdmF0ZU1hcC5nZXQocmVjZWl2ZXIpO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gX19jbGFzc1ByaXZhdGVGaWVsZFNldChyZWNlaXZlciwgcHJpdmF0ZU1hcCwgdmFsdWUpIHtcclxuICAgIGlmICghcHJpdmF0ZU1hcC5oYXMocmVjZWl2ZXIpKSB7XHJcbiAgICAgICAgdGhyb3cgbmV3IFR5cGVFcnJvcihcImF0dGVtcHRlZCB0byBzZXQgcHJpdmF0ZSBmaWVsZCBvbiBub24taW5zdGFuY2VcIik7XHJcbiAgICB9XHJcbiAgICBwcml2YXRlTWFwLnNldChyZWNlaXZlciwgdmFsdWUpO1xyXG4gICAgcmV0dXJuIHZhbHVlO1xyXG59XHJcbiIsInZhciBnO1xuXG4vLyBUaGlzIHdvcmtzIGluIG5vbi1zdHJpY3QgbW9kZVxuZyA9IChmdW5jdGlvbigpIHtcblx0cmV0dXJuIHRoaXM7XG59KSgpO1xuXG50cnkge1xuXHQvLyBUaGlzIHdvcmtzIGlmIGV2YWwgaXMgYWxsb3dlZCAoc2VlIENTUClcblx0ZyA9IGcgfHwgbmV3IEZ1bmN0aW9uKFwicmV0dXJuIHRoaXNcIikoKTtcbn0gY2F0Y2ggKGUpIHtcblx0Ly8gVGhpcyB3b3JrcyBpZiB0aGUgd2luZG93IHJlZmVyZW5jZSBpcyBhdmFpbGFibGVcblx0aWYgKHR5cGVvZiB3aW5kb3cgPT09IFwib2JqZWN0XCIpIGcgPSB3aW5kb3c7XG59XG5cbi8vIGcgY2FuIHN0aWxsIGJlIHVuZGVmaW5lZCwgYnV0IG5vdGhpbmcgdG8gZG8gYWJvdXQgaXQuLi5cbi8vIFdlIHJldHVybiB1bmRlZmluZWQsIGluc3RlYWQgb2Ygbm90aGluZyBoZXJlLCBzbyBpdCdzXG4vLyBlYXNpZXIgdG8gaGFuZGxlIHRoaXMgY2FzZS4gaWYoIWdsb2JhbCkgeyAuLi59XG5cbm1vZHVsZS5leHBvcnRzID0gZztcbiIsIm1vZHVsZS5leHBvcnRzID0gZnVuY3Rpb24ob3JpZ2luYWxNb2R1bGUpIHtcblx0aWYgKCFvcmlnaW5hbE1vZHVsZS53ZWJwYWNrUG9seWZpbGwpIHtcblx0XHR2YXIgbW9kdWxlID0gT2JqZWN0LmNyZWF0ZShvcmlnaW5hbE1vZHVsZSk7XG5cdFx0Ly8gbW9kdWxlLnBhcmVudCA9IHVuZGVmaW5lZCBieSBkZWZhdWx0XG5cdFx0aWYgKCFtb2R1bGUuY2hpbGRyZW4pIG1vZHVsZS5jaGlsZHJlbiA9IFtdO1xuXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShtb2R1bGUsIFwibG9hZGVkXCIsIHtcblx0XHRcdGVudW1lcmFibGU6IHRydWUsXG5cdFx0XHRnZXQ6IGZ1bmN0aW9uKCkge1xuXHRcdFx0XHRyZXR1cm4gbW9kdWxlLmw7XG5cdFx0XHR9XG5cdFx0fSk7XG5cdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG1vZHVsZSwgXCJpZFwiLCB7XG5cdFx0XHRlbnVtZXJhYmxlOiB0cnVlLFxuXHRcdFx0Z2V0OiBmdW5jdGlvbigpIHtcblx0XHRcdFx0cmV0dXJuIG1vZHVsZS5pO1xuXHRcdFx0fVxuXHRcdH0pO1xuXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShtb2R1bGUsIFwiZXhwb3J0c1wiLCB7XG5cdFx0XHRlbnVtZXJhYmxlOiB0cnVlXG5cdFx0fSk7XG5cdFx0bW9kdWxlLndlYnBhY2tQb2x5ZmlsbCA9IDE7XG5cdH1cblx0cmV0dXJuIG1vZHVsZTtcbn07XG4iLCJpbXBvcnQgeyBwYXJzZVByaWNlLCBwYXJzZUN1cnJlbmN5IH0gZnJvbSAnLi4vdXRpbHMnO1xuXG5jb25zdCBjYXJkTnVtYmVyID0gKHN0cikgPT4gc3RyPy5yZXBsYWNlKC9cXHMvZywgJycpO1xuY29uc3QgaW1hZ2UgPSAoc3RyKSA9PiBzdHI/LnJlcGxhY2UoJ2Nocm9tZS1leHRlbnNpb246Ly8nLCAnaHR0cHM6Ly8nKTtcbmNvbnN0IHF1YW50aXR5ID0gKHN0cikgPT4gcGFyc2VJbnQoc3RyLCAxMCk7XG5jb25zdCBudW1lcmljU2t1ID0gKHN0cikgPT4gW2Ake3N0cn1gLm1hdGNoKC9bMC05XXs0LH0vKV0uam9pbignJyk7XG5cbmV4cG9ydCBkZWZhdWx0IHtcbiAgaW1hZ2UsXG4gIHF1YW50aXR5LFxuICBudW1lcmljU2t1LFxuICBjYXJkTnVtYmVyLFxuICBwcmljZTogcGFyc2VQcmljZSxcbiAgdG90YWw6IHBhcnNlUHJpY2UsXG4gIGN1cnJlbmN5OiBwYXJzZUN1cnJlbmN5LFxuICB0b3RhbEN1cnJlbmN5OiBwYXJzZUN1cnJlbmN5LFxufTtcbiIsImltcG9ydCBFdmVudEVtaXR0ZXIgZnJvbSAnZXZlbnRzJztcbmltcG9ydCBodG1sSGVscGVycyBmcm9tICcuL2h0bWwtaGVscGVycyc7XG5pbXBvcnQganNvbkhlbHBlcnMgZnJvbSAnLi9qc29uLWhlbHBlcnMnO1xuaW1wb3J0IHtcbiAgcGFyc2VOb2RlLCBwYXJzZUh0bWwsIGdldCwgc3Vic3RpdHV0ZSxcbn0gZnJvbSAnLi4vdXRpbHMnO1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBQYXJzZXIgZXh0ZW5kcyBFdmVudEVtaXR0ZXIge1xuICBjb25zdHJ1Y3Rvcihjb25maWcgPSB7fSkge1xuICAgIHN1cGVyKCk7XG4gICAgdGhpcy5jb25maWcgPSBjb25maWc7XG4gICAgdGhpcy5kZWZpbmVPYnNlcnZlZFByb3BlcnR5KCdwYXJzZWREYXRhJyk7XG4gIH1cblxuICBhc3luYyBmZXRjaERhdGEodXJsLCBvcHRpb25zKSB7XG4gICAgaWYgKHRoaXMuYWJvcnRDb250cm9sbGVyKSB7XG4gICAgICB0aGlzLmFib3J0Q29udHJvbGxlci5hYm9ydCgpO1xuICAgIH1cbiAgICB0aGlzLmFib3J0Q29udHJvbGxlciA9IG5ldyBBYm9ydENvbnRyb2xsZXIoKTtcbiAgICBjb25zdCB7IHNpZ25hbCB9ID0gdGhpcy5hYm9ydENvbnRyb2xsZXI7XG4gICAgY29uc3QgcmVzcG9uc2UgPSBhd2FpdCBmZXRjaCh1cmwsIHsgc2lnbmFsLCAuLi5vcHRpb25zIH0pO1xuICAgIGlmICghcmVzcG9uc2Uub2spIHtcbiAgICAgIHRocm93IG5ldyBFcnJvcignUGFyc2VyIGZldGNoIGZhaWxlZC4nKTtcbiAgICB9XG4gICAgY29uc3QgW3BhcnNlVHlwZV0gPSBPYmplY3QuZW50cmllcyh7XG4gICAgICBqc29uOiAnYXBwbGljYXRpb24vanNvbicsXG4gICAgICB0ZXh0OiAndGV4dCcsXG4gICAgfSkuZmluZCgoWywgdmFsdWVdKSA9PiByZXNwb25zZS5oZWFkZXJzLmdldCgnY29udGVudC10eXBlJykuaW5jbHVkZXModmFsdWUpKTtcbiAgICBjb25zdCBkYXRhID0gYXdhaXQgcmVzcG9uc2VbcGFyc2VUeXBlXSgpO1xuICAgIGlmIChwYXJzZVR5cGUgPT09ICd0ZXh0Jykge1xuICAgICAgcmV0dXJuIHBhcnNlSHRtbChkYXRhKTtcbiAgICB9XG4gICAgcmV0dXJuIGRhdGE7XG4gIH1cblxuICBwYXJzZUl0ZW0oeyBuYW1lLCBwYXJhbXMsIHBhcmVudCA9IGRvY3VtZW50IH0pIHtcbiAgICBjb25zdCBpc0h0bWwgPSAocGFyZW50LmRvY3VtZW50RWxlbWVudCB8fCBwYXJlbnQpIGluc3RhbmNlb2YgSFRNTEVsZW1lbnQ7XG4gICAgY29uc3Qgc3RyID0gaXNIdG1sID8gcGFyc2VOb2RlKHBhcmFtcywgcGFyZW50KSA6IGdldChwYXJlbnQsIHBhcmFtcy5wYXRoKTtcbiAgICBjb25zdCB7IGpzb25IZWxwZXIsIGh0bWxIZWxwZXIgfSA9IHBhcmFtcztcbiAgICBjb25zdCBoZWxwZXJOYW1lID0gKGlzSHRtbCA/IGh0bWxIZWxwZXIgOiBqc29uSGVscGVyKSB8fCBuYW1lO1xuICAgIGNvbnN0IGhlbHBlciA9IChpc0h0bWwgPyBodG1sSGVscGVycyA6IGpzb25IZWxwZXJzKVtoZWxwZXJOYW1lXTtcbiAgICByZXR1cm4gW25hbWUsIGhlbHBlciA/IGhlbHBlcihzdHIpIDogc3RyXTtcbiAgfVxuXG4gIHBhcnNlQ29udGFpbmVyKHsgcGFyZW50ID0gZG9jdW1lbnQsIHJ1bGVzID0ge30gfSkge1xuICAgIGNvbnN0IGVudHJpZXMgPSBPYmplY3QuZW50cmllcyhydWxlcylcbiAgICAgIC5tYXAoKFtuYW1lLCBwYXJhbXNdKSA9PiB0aGlzLnBhcnNlSXRlbSh7IG5hbWUsIHBhcmFtcywgcGFyZW50IH0pKTtcbiAgICByZXR1cm4gT2JqZWN0LmZyb21FbnRyaWVzKFxuICAgICAgZW50cmllc1xuICAgICAgICAubWFwKChbbmFtZSwgdmFsdWVdKSA9PiB7XG4gICAgICAgICAgY29uc3Qgc3RyID0gcnVsZXM/LltuYW1lXT8uY29udGVudDtcbiAgICAgICAgICByZXR1cm4gW25hbWUsIHN0ciA/IHN1YnN0aXR1dGUoeyBzdHIsIGVudHJpZXMgfSkgOiB2YWx1ZV07XG4gICAgICAgIH0pLFxuICAgICk7XG4gIH1cblxuICBwYXJzZUl0ZW1zKGRvYyA9IGRvY3VtZW50KSB7XG4gICAgY29uc3QgeyBpdGVtcyA9IHt9IH0gPSB0aGlzLmNvbmZpZztcbiAgICBjb25zdCBpc0h0bWwgPSAoZG9jLmRvY3VtZW50RWxlbWVudCB8fCBkb2MpIGluc3RhbmNlb2YgSFRNTEVsZW1lbnQ7XG4gICAgcmV0dXJuIE9iamVjdC5mcm9tRW50cmllcyhcbiAgICAgIE9iamVjdFxuICAgICAgICAuZW50cmllcyhpdGVtcylcbiAgICAgICAgLm1hcCgoW25hbWUsIGNvbmZpZyA9IHt9XSkgPT4ge1xuICAgICAgICAgIGNvbnN0IHsgY29udGFpbmVyLCBydWxlcyB9ID0gY29uZmlnO1xuICAgICAgICAgIGlmIChjb250YWluZXIpIHtcbiAgICAgICAgICAgIGNvbnN0IHsgc2VsZWN0b3IsIHBhdGggfSA9IGNvbnRhaW5lcjtcbiAgICAgICAgICAgIGNvbnN0IHBhcmVudHMgPSBpc0h0bWwgPyBbLi4uZG9jLnF1ZXJ5U2VsZWN0b3JBbGwoc2VsZWN0b3IpXSA6IChnZXQoZG9jLCBwYXRoKSB8fCBbXSk7XG4gICAgICAgICAgICByZXR1cm4gW25hbWUsIHBhcmVudHMubWFwKChwYXJlbnQpID0+IHRoaXMucGFyc2VDb250YWluZXIoeyBwYXJlbnQsIHJ1bGVzIH0pKV07XG4gICAgICAgICAgfVxuICAgICAgICAgIHJldHVybiB0aGlzLnBhcnNlSXRlbSh7IG5hbWUsIHBhcmFtczogY29uZmlnLCBwYXJlbnQ6IGRvYyB9KTtcbiAgICAgICAgfSksXG4gICAgKTtcbiAgfVxuXG4gIGFzeW5jIHByb2Nlc3MoKSB7XG4gICAgY29uc3QgZGF0YSA9IGF3YWl0IHRoaXMuZmV0Y2hEYXRhKHRoaXMuY29uZmlnLnVybCk7XG4gICAgY29uc29sZS50aW1lKCdwcm9jZXNzJyk7XG4gICAgY29uc3QgcGFyc2VkRGF0YSA9IHRoaXMucGFyc2VJdGVtcyhkYXRhKTtcbiAgICBjb25zb2xlLnRpbWVFbmQoJ3Byb2Nlc3MnKTtcbiAgICBpZiAocGFyc2VkRGF0YSkge1xuICAgICAgdGhpcy5wYXJzZWREYXRhID0gcGFyc2VkRGF0YTtcbiAgICB9XG4gICAgcmV0dXJuIHRoaXMucGFyc2VkRGF0YTtcbiAgfVxuXG4gIGRlZmluZU9ic2VydmVkUHJvcGVydHkobmFtZSkge1xuICAgIE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0aGlzLCBuYW1lLCB7XG4gICAgICBnZXQ6ICgpID0+IHRoaXNbYF8ke25hbWV9YF0sXG4gICAgICBzZXQ6ICh2YWx1ZSkgPT4ge1xuICAgICAgICBpZiAodGhpc1tgXyR7bmFtZX1gXSAhPT0gdmFsdWUpIHtcbiAgICAgICAgICB0aGlzW2BfJHtuYW1lfWBdID0gdmFsdWU7XG4gICAgICAgICAgdGhpcy5lbWl0KGB1cGRhdGU6JHtuYW1lfWAsIHZhbHVlKTtcbiAgICAgICAgfVxuICAgICAgfSxcbiAgICB9KTtcbiAgfVxufVxuIiwiaW1wb3J0IHsgcGFyc2VQcmljZSwgcGFyc2VDdXJyZW5jeSB9IGZyb20gJy4uL3V0aWxzJztcblxuY29uc3QgY2FyZE51bWJlciA9IChzdHIpID0+IHN0ci5yZXBsYWNlKC9cXHMvZywgJycpO1xuY29uc3QgaW1hZ2UgPSAoc3RyKSA9PiBzdHI/LnJlcGxhY2UoL2Nocm9tZS1leHRlbnNpb246XFwvXFwvfF5cXC9cXC8vLCAnaHR0cHM6Ly8nKTtcbmNvbnN0IHF1YW50aXR5ID0gKHN0cikgPT4gcGFyc2VJbnQoc3RyLCAxMCk7XG5jb25zdCBudW1lcmljU2t1ID0gKHN0cikgPT4gW2Ake3N0cn1gLm1hdGNoKC9bMC05XXs0LH0vKV0uam9pbignJyk7XG5jb25zdCBjdXJyeXNQcmljZSA9IChudW0pID0+IG51bSAvIDEwMDtcblxuZXhwb3J0IGRlZmF1bHQge1xuICBpbWFnZSxcbiAgcXVhbnRpdHksXG4gIG51bWVyaWNTa3UsXG4gIGNhcmROdW1iZXIsXG4gIGN1cnJ5c1ByaWNlLFxuICBwcmljZTogcGFyc2VQcmljZSxcbiAgdG90YWw6IHBhcnNlUHJpY2UsXG4gIGN1cnJlbmN5OiBwYXJzZUN1cnJlbmN5LFxuICB0b3RhbEN1cnJlbmN5OiBwYXJzZUN1cnJlbmN5LFxufTtcbiIsImltcG9ydCAqIGFzIFNlbnRyeSBmcm9tICdAc2VudHJ5L2Jyb3dzZXInO1xuXG5mdW5jdGlvbiBpbml0U2VudHJ5KGJlZm9yZUluaXRGdW5jdGlvbiA9ICgpID0+IHt9KSB7XG4gIGJlZm9yZUluaXRGdW5jdGlvbihTZW50cnkpO1xuXG4gIFNlbnRyeS5pbml0KHtcbiAgICBkc246ICdodHRwczovLzQwZjAyMzczODMwZDQwOWY5YTQ0MjM3YjI0NzI4MjFiQG80MTE5MzUuaW5nZXN0LnNlbnRyeS5pby81Mjg3ODUyJyxcbiAgfSk7XG59XG5cbmV4cG9ydCBkZWZhdWx0IGluaXRTZW50cnk7XG4iLCJleHBvcnQgY29uc3QgdmFsaWRhdGVPYmplY3QgPSAoZmllbGRzID0gW10sIG9iamVjdCkgPT4gZmllbGRzXG4gIC5ldmVyeSgobmFtZSkgPT4gdHlwZW9mIG9iamVjdFtuYW1lXSAhPT0gJ3VuZGVmaW5lZCcpO1xuXG5leHBvcnQgY29uc3QgbWF0Y2ggPSAoc3RyLCBtYXRjaGVyKSA9PiB7XG4gIHRyeSB7XG4gICAgcmV0dXJuIHN0ci5zZWFyY2gobWF0Y2hlcikgPiAtMTtcbiAgfSBjYXRjaCAoZSkge1xuICAgIHJldHVybiBzdHIuaW5kZXhPZihtYXRjaGVyKSA+IDA7XG4gIH1cbn07XG5cbmV4cG9ydCBjb25zdCBwYXJzZVByaWNlID0gKHByaWNlKSA9PiB7XG4gIGxldCBzdHIgPSBgJHtwcmljZX1gXG4gICAgLnJlcGxhY2UoL1sk4oKswqPvv6FdL2csICcnKVxuICAgIC5yZXBsYWNlKC9bXlxcZC4sXFxuXS9nLCAnJylcbiAgICAudHJpbSgpO1xuICBjb25zdCBjb21tYUluZGV4ID0gc3RyLmluZGV4T2YoJywnKTtcbiAgY29uc3QgZG90SW5kZXggPSBzdHIuaW5kZXhPZignLicpO1xuICBpZiAoY29tbWFJbmRleCA+IGRvdEluZGV4KSB7XG4gICAgc3RyID0gc3RyLnJlcGxhY2UoJy4nLCAnJykucmVwbGFjZSgnLCcsICcuJyk7XG4gIH0gZWxzZSBpZiAoY29tbWFJbmRleCA8IGRvdEluZGV4IHx8IGRvdEluZGV4IDwgMCkge1xuICAgIHN0ciA9IHN0ci5yZXBsYWNlKCcsJywgJycpO1xuICB9XG4gIHJldHVybiBwYXJzZUZsb2F0KHN0cik7XG59O1xuXG5leHBvcnQgY29uc3QgZ2V0Q3VycmVuY3kgPSAobWF0Y2hlcikgPT4ge1xuICBjb25zdCByZXN1bHQgPSBbXG4gICAgWydlbi1VUycsICdVU0QnLCAnJCddLFxuICAgIFsnZGUtREUnLCAnRVVSJywgJ+KCrCddLFxuICAgIFsnZW4tVVMnLCAnR0JQJywgJ8Kj77+hJ10sXG4gIF0uZmluZCgoWywgY29kZSwgbWF0Y2hTeW1ib2xdKSA9PiBjb2RlID09PSBtYXRjaGVyIHx8IG1hdGNoU3ltYm9sLmluY2x1ZGVzKG1hdGNoZXIpKTtcbiAgaWYgKHJlc3VsdCkge1xuICAgIGNvbnN0IFtsYW5ndWFnZSwgY29kZV0gPSByZXN1bHQ7XG4gICAgcmV0dXJuIHsgbGFuZ3VhZ2UsIGNvZGUgfTtcbiAgfVxuICByZXR1cm4gbnVsbDtcbn07XG5cbmV4cG9ydCBjb25zdCBnZW5lcmF0ZVByaWNlID0gKGRhdGEpID0+IHtcbiAgY29uc3QgeyB2YWx1ZSwgY3VycmVuY3kgfSA9IGRhdGE7XG4gIGNvbnN0IHsgbGFuZ3VhZ2UsIGNvZGUgfSA9IGdldEN1cnJlbmN5KGN1cnJlbmN5KTtcbiAgY29uc3QgcGFyYW1zID0geyBzdHlsZTogJ2N1cnJlbmN5JywgY3VycmVuY3k6IGNvZGUgfTtcbiAgcmV0dXJuIG5ldyBJbnRsLk51bWJlckZvcm1hdChsYW5ndWFnZSwgcGFyYW1zKS5mb3JtYXQodmFsdWUpO1xufTtcblxuZXhwb3J0IGNvbnN0IHBhcnNlQ3VycmVuY3kgPSAoc3RyKSA9PiB7XG4gIGNvbnN0IHBhcnNlZFN0ciA9IFtgJHtzdHJ9YC5tYXRjaCgvWyTigqzCo++/oV18R0JQfFVTRHxFVVIvKV0uam9pbigpO1xuICByZXR1cm4gZ2V0Q3VycmVuY3kocGFyc2VkU3RyKTtcbn07XG5cbmV4cG9ydCBjb25zdCBjb21wbGV4UHJpY2UgPSAoc3RyKSA9PiAoe1xuICBwcmljZTogYCR7c3RyfWAuc3BsaXQoJy0nKS5tYXAoKHByaWNlKSA9PiBwYXJzZVByaWNlKHByaWNlKSksXG4gIGN1cnJlbmN5OiBwYXJzZUN1cnJlbmN5KHN0ciksXG59KTtcblxuZXhwb3J0IGNvbnN0IHBhcnNlTm9kZSA9ICh7IHNlbGVjdG9yLCBhdHRyaWJ1dGUsIGVsIH0sIHBhcmVudE5vZGUgPSBkb2N1bWVudCkgPT4ge1xuICBjb25zdCBub2RlID0gZWwgfHwgcGFyZW50Tm9kZS5xdWVyeVNlbGVjdG9yKHNlbGVjdG9yKTtcbiAgaWYgKG5vZGUpIHtcbiAgICBjb25zdCB0ZXh0Tm9kZSA9IFsuLi5ub2RlLmNoaWxkTm9kZXNdLmZpbmQoKGNoaWxkKSA9PiBjaGlsZC5ub2RlTmFtZS5pbmNsdWRlcygnd2hvbGVUZXh0JykpO1xuICAgIHJldHVybiBgJHtcbiAgICAgIG5vZGUuZ2V0QXR0cmlidXRlKGF0dHJpYnV0ZSlcbiAgICAgIHx8ICh0ZXh0Tm9kZSAmJiB0ZXh0Tm9kZS53aG9sZVRleHQpXG4gICAgICB8fCBub2RlLmlubmVyVGV4dFxuICAgICAgfHwgbm9kZS52YWx1ZVxuICAgICAgfHwgbm9kZS5jb250ZW50XG4gICAgICB8fCBub2RlLnNyY1xuICAgIH1gO1xuICB9XG4gIHJldHVybiBudWxsO1xufTtcblxuZXhwb3J0IGNvbnN0IHBhcnNlSHRtbCA9IChodG1sVGV4dCkgPT4gbmV3IERPTVBhcnNlcigpLnBhcnNlRnJvbVN0cmluZyhodG1sVGV4dCwgJ3RleHQvaHRtbCcpO1xuXG5leHBvcnQgY29uc3QgY2FsY3VsYXRlRGlzY291bnQgPSAoeyBjdXJyZW5jeSwgcHJpY2UsIHJlYmF0ZSB9KSA9PiB7XG4gIGNvbnN0IGRpc2NvdW50ZWRQcmljZSA9IHBhcnNlSW50KHByaWNlIC0gKHByaWNlICogcmViYXRlKSAvIDEwMCwgMTApO1xuICBjb25zdCBwYXJhbXMgPSB7IG1pbmltdW1GcmFjdGlvbkRpZ2l0czogMiwgbWF4aW11bUZyYWN0aW9uRGlnaXRzOiAyIH07XG4gIHJldHVybiBgJHtjdXJyZW5jeX0ke2Rpc2NvdW50ZWRQcmljZS50b0xvY2FsZVN0cmluZyhwYXJhbXMpfS4wMGA7XG59O1xuXG5leHBvcnQgY29uc3QgYXN5bmNUaW1lb3V0ID0gYXN5bmMgKHQpID0+IG5ldyBQcm9taXNlKChyKSA9PiBzZXRUaW1lb3V0KCgpID0+IHIodHJ1ZSksIHQpKTtcblxuZXhwb3J0IGNvbnN0IGNyZWF0ZUhpZGRlbkZyYW1lID0gKHVybCkgPT4ge1xuICBjb25zdCBmcmFtZSA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2lmcmFtZScpO1xuICBmcmFtZS5zdHlsZSA9ICd3aWR0aDogMXB4OyBoZWlnaHQ6IDFweDsgb3BhY2l0eTogMDsgcG9zaXRpb246IGZpeGVkOyBib3R0b206IDA7IHJpZ2h0OiAwOyc7XG4gIGZyYW1lLnNyYyA9IHVybDtcbiAgcmV0dXJuIGZyYW1lO1xufTtcblxuZXhwb3J0IGNvbnN0IGNvcHlUb0NsaXBib2FyZCA9IChzdHIpID0+IHtcbiAgY29uc3QgaSA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2lucHV0Jyk7XG4gIGkuc3R5bGUgPSAncG9zaXRpb246IGFic29sdXRlOyBib3R0b206IC05OTk5OTk5OTsgcmlnaHQ6IC05OTk5OTk5OTk7JztcbiAgZG9jdW1lbnQuYm9keS5hcHBlbmRDaGlsZChpKTtcbiAgaS52YWx1ZSA9IHN0cjtcbiAgaS5zZWxlY3QoKTtcbiAgZG9jdW1lbnQuZXhlY0NvbW1hbmQoJ0NvcHknKTtcbiAgZG9jdW1lbnQuYm9keS5yZW1vdmVDaGlsZChpKTtcbn07XG5cbmV4cG9ydCBjb25zdCBhc3luY1dyYXBwZXIgPSBhc3luYyAobWV0aG9kLCBhcmdzKSA9PiBtZXRob2QoYXJncyk7XG5cbmV4cG9ydCBjb25zdCBjcmVhdGVTaGFkb3dSb290ID0gKGlkKSA9PiB7XG4gIGNvbnN0IHdyYXBwZXIgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKTtcbiAgd3JhcHBlci5pZCA9IGlkO1xuICBkb2N1bWVudC5ib2R5LmFwcGVuZCh3cmFwcGVyKTtcbiAgY29uc3Qgc2hhZG93cm9vdCA9IHdyYXBwZXIuYXR0YWNoU2hhZG93KHsgbW9kZTogJ29wZW4nIH0pO1xuICBzaGFkb3dyb290LmlubmVySFRNTCA9IGBcbiAgICA8c3R5bGU+QGltcG9ydCB1cmwoXCIke2Nocm9tZS5leHRlbnNpb24uZ2V0VVJMKCdjb250ZW50L3N0eWxlcy5jc3MnKX1cIik7PC9zdHlsZT5cbiAgICA8ZGl2IGlkPVwiJHtpZH1cIiAvPmA7XG4gIHJldHVybiBzaGFkb3dyb290O1xufTtcblxuZXhwb3J0IGNvbnN0IGdldCA9IChvYmplY3QsIHBhdGgpID0+IHtcbiAgY29uc3QgYXJyYXlQYXRoID0gdHlwZW9mIHBhdGggPT09ICdzdHJpbmcnID8gcGF0aC5zcGxpdCgnLicpIDogcGF0aDtcbiAgaWYgKEFycmF5LmlzQXJyYXkob2JqZWN0KSAmJiBOdW1iZXIuaXNOYU4ocGFyc2VJbnQoYXJyYXlQYXRoWzBdLCAxMCkpKSB7XG4gICAgcmV0dXJuIG9iamVjdFxuICAgICAgLm1hcCgob2JqZWN0RGF0YSkgPT4gZ2V0KG9iamVjdERhdGEsIFsuLi5hcnJheVBhdGhdKSlcbiAgICAgIC5mbGF0KEluZmluaXR5KTtcbiAgfVxuICBjb25zdCBkYXRhID0gb2JqZWN0W2FycmF5UGF0aFswXV07XG4gIGFycmF5UGF0aC5zaGlmdCgpO1xuICBpZiAoZGF0YSAmJiBhcnJheVBhdGgubGVuZ3RoKSB7XG4gICAgcmV0dXJuIGdldChkYXRhLCBhcnJheVBhdGgpO1xuICB9XG4gIHJldHVybiBkYXRhO1xufTtcblxuZXhwb3J0IGNvbnN0IHN1YnN0aXR1dGUgPSAoeyBzdHIsIG9iaiwgZW50cmllcyA9IE9iamVjdC5lbnRyaWVzKG9iaikgfSkgPT4ge1xuICBsZXQgc3RyUmVzdWx0ID0gc3RyO1xuICBpZiAoZW50cmllcykge1xuICAgIHN0clJlc3VsdCA9IGVudHJpZXNcbiAgICAgIC5yZWR1Y2UoKHJlc3VsdCwgW25hbWUsIHZhbHVlXSkgPT4gcmVzdWx0LnJlcGxhY2UoYCUke25hbWV9YCwgdmFsdWUpLCBzdHJSZXN1bHQpO1xuICB9XG4gIHJldHVybiBzdHJSZXN1bHQ7XG59O1xuIiwiZXhwb3J0IGNvbnN0IHZhbGlkYXRlT2JqZWN0ID0gKGZpZWxkcyA9IFtdLCBvYmplY3QpID0+IGZpZWxkc1xuICAuZXZlcnkoKG5hbWUpID0+IHR5cGVvZiBvYmplY3RbbmFtZV0gIT09ICd1bmRlZmluZWQnKTtcblxuZXhwb3J0IGNvbnN0IG1hdGNoID0gKHN0ciwgbWF0Y2hlcikgPT4ge1xuICB0cnkge1xuICAgIHJldHVybiBzdHIuc2VhcmNoKG1hdGNoZXIpID4gLTE7XG4gIH0gY2F0Y2ggKGUpIHtcbiAgICByZXR1cm4gc3RyLmluZGV4T2YobWF0Y2hlcikgPiAwO1xuICB9XG59O1xuXG5leHBvcnQgY29uc3QgcGFyc2VQcmljZSA9IChwcmljZSkgPT4ge1xuICBsZXQgc3RyID0gYCR7cHJpY2V9YFxuICAgIC5yZXBsYWNlKC9bJOKCrMKj77+hXS9nLCAnJylcbiAgICAucmVwbGFjZSgvW15cXGQuLFxcbl0vZywgJycpXG4gICAgLnRyaW0oKTtcbiAgY29uc3QgY29tbWFJbmRleCA9IHN0ci5pbmRleE9mKCcsJyk7XG4gIGNvbnN0IGRvdEluZGV4ID0gc3RyLmluZGV4T2YoJy4nKTtcbiAgaWYgKGNvbW1hSW5kZXggPiBkb3RJbmRleCkge1xuICAgIHN0ciA9IHN0ci5yZXBsYWNlKCcuJywgJycpLnJlcGxhY2UoJywnLCAnLicpO1xuICB9IGVsc2UgaWYgKGNvbW1hSW5kZXggPCBkb3RJbmRleCB8fCBkb3RJbmRleCA8IDApIHtcbiAgICBzdHIgPSBzdHIucmVwbGFjZSgnLCcsICcnKTtcbiAgfVxuICByZXR1cm4gcGFyc2VGbG9hdChzdHIpO1xufTtcblxuZXhwb3J0IGNvbnN0IGdldEN1cnJlbmN5ID0gKG1hdGNoZXIpID0+IHtcbiAgY29uc3QgcmVzdWx0ID0gW1xuICAgIFsnZW4tVVMnLCAnVVNEJywgJyQnXSxcbiAgICBbJ2RlLURFJywgJ0VVUicsICfigqwnXSxcbiAgICBbJ2VuLVVTJywgJ0dCUCcsICfCo++/oSddLFxuICBdLmZpbmQoKFssIGNvZGUsIG1hdGNoU3ltYm9sXSkgPT4gY29kZSA9PT0gbWF0Y2hlciB8fCBtYXRjaFN5bWJvbC5pbmNsdWRlcyhtYXRjaGVyKSk7XG4gIGlmIChyZXN1bHQpIHtcbiAgICBjb25zdCBbbGFuZ3VhZ2UsIGNvZGVdID0gcmVzdWx0O1xuICAgIHJldHVybiB7IGxhbmd1YWdlLCBjb2RlIH07XG4gIH1cbiAgcmV0dXJuIG51bGw7XG59O1xuXG5leHBvcnQgY29uc3QgcGFyc2VDdXJyZW5jeSA9IChzdHIpID0+IHtcbiAgY29uc3QgcGFyc2VkU3RyID0gW2Ake3N0cn1gLm1hdGNoKC9bJOKCrMKj77+hXS8pXS5qb2luKCk7XG4gIHJldHVybiBnZXRDdXJyZW5jeShwYXJzZWRTdHIpO1xufTtcblxuZXhwb3J0IGNvbnN0IGNvbXBsZXhQcmljZSA9IChzdHIpID0+ICh7XG4gIHByaWNlOiBgJHtzdHJ9YC5zcGxpdCgnLScpLm1hcCgocHJpY2UpID0+IHBhcnNlUHJpY2UocHJpY2UpKSxcbiAgY3VycmVuY3k6IHBhcnNlQ3VycmVuY3koc3RyKSxcbn0pO1xuXG5leHBvcnQgY29uc3QgcGFyc2VOb2RlID0gKHsgc2VsZWN0b3IsIGF0dHJpYnV0ZSwgZWwgfSwgcGFyZW50Tm9kZSA9IGRvY3VtZW50KSA9PiB7XG4gIGNvbnN0IG5vZGUgPSBlbCB8fCBwYXJlbnROb2RlLnF1ZXJ5U2VsZWN0b3Ioc2VsZWN0b3IpO1xuICBpZiAobm9kZSkge1xuICAgIGNvbnN0IHRleHROb2RlID0gWy4uLm5vZGUuY2hpbGROb2Rlc10uZmluZCgoY2hpbGQpID0+IGNoaWxkLm5vZGVOYW1lLmluY2x1ZGVzKCd3aG9sZVRleHQnKSk7XG4gICAgcmV0dXJuIGAke1xuICAgICAgbm9kZS5nZXRBdHRyaWJ1dGUoYXR0cmlidXRlKVxuICAgICAgfHwgKHRleHROb2RlICYmIHRleHROb2RlLndob2xlVGV4dClcbiAgICAgIHx8IG5vZGUuaW5uZXJUZXh0XG4gICAgICB8fCBub2RlLnZhbHVlXG4gICAgICB8fCBub2RlLmNvbnRlbnRcbiAgICAgIHx8IG5vZGUuc3JjXG4gICAgfWA7XG4gIH1cbiAgcmV0dXJuIG51bGw7XG59O1xuXG5leHBvcnQgY29uc3QgcGFyc2VIdG1sID0gKGh0bWxUZXh0KSA9PiBuZXcgRE9NUGFyc2VyKCkucGFyc2VGcm9tU3RyaW5nKGh0bWxUZXh0LCAndGV4dC9odG1sJyk7XG5cbmV4cG9ydCBjb25zdCBjYWxjdWxhdGVEaXNjb3VudCA9ICh7IGN1cnJlbmN5LCBwcmljZSwgcmViYXRlIH0pID0+IHtcbiAgY29uc3QgZGlzY291bnRlZFByaWNlID0gcGFyc2VJbnQocHJpY2UgLSAocHJpY2UgKiByZWJhdGUpIC8gMTAwLCAxMCk7XG4gIGNvbnN0IHBhcmFtcyA9IHsgbWluaW11bUZyYWN0aW9uRGlnaXRzOiAyLCBtYXhpbXVtRnJhY3Rpb25EaWdpdHM6IDIgfTtcbiAgcmV0dXJuIGAke2N1cnJlbmN5fSR7ZGlzY291bnRlZFByaWNlLnRvTG9jYWxlU3RyaW5nKHBhcmFtcyl9LjAwYDtcbn07XG5cbmV4cG9ydCBjb25zdCBhc3luY1RpbWVvdXQgPSBhc3luYyAodCkgPT4gbmV3IFByb21pc2UoKHIpID0+IHNldFRpbWVvdXQoKCkgPT4gcih0cnVlKSwgdCkpO1xuXG5leHBvcnQgY29uc3QgY3JlYXRlSGlkZGVuRnJhbWUgPSAodXJsKSA9PiB7XG4gIGNvbnN0IGZyYW1lID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnaWZyYW1lJyk7XG4gIGZyYW1lLnN0eWxlID0gJ3dpZHRoOiAxcHg7IGhlaWdodDogMXB4OyBvcGFjaXR5OiAwOyBwb3NpdGlvbjogZml4ZWQ7IGJvdHRvbTogMDsgcmlnaHQ6IDA7JztcbiAgZnJhbWUuc3JjID0gdXJsO1xuICByZXR1cm4gZnJhbWU7XG59O1xuXG5leHBvcnQgY29uc3QgY29weVRvQ2xpcGJvYXJkID0gKHN0cikgPT4ge1xuICBjb25zdCBpID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnaW5wdXQnKTtcbiAgaS5zdHlsZSA9ICdwb3NpdGlvbjogYWJzb2x1dGU7IGJvdHRvbTogLTk5OTk5OTk5OyByaWdodDogLTk5OTk5OTk5OTsnO1xuICBkb2N1bWVudC5ib2R5LmFwcGVuZENoaWxkKGkpO1xuICBpLnZhbHVlID0gc3RyO1xuICBpLnNlbGVjdCgpO1xuICBkb2N1bWVudC5leGVjQ29tbWFuZCgnQ29weScpO1xuICBkb2N1bWVudC5ib2R5LnJlbW92ZUNoaWxkKGkpO1xufTtcblxuZXhwb3J0IGNvbnN0IGFzeW5jV3JhcHBlciA9IGFzeW5jIChtZXRob2QsIGFyZ3MpID0+IG1ldGhvZChhcmdzKTtcblxuZXhwb3J0IGNvbnN0IGNyZWF0ZVNoYWRvd1Jvb3QgPSAoaWQpID0+IHtcbiAgY29uc3Qgd3JhcHBlciA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xuICB3cmFwcGVyLmlkID0gaWQ7XG4gIGRvY3VtZW50LmJvZHkuYXBwZW5kKHdyYXBwZXIpO1xuICBjb25zdCBzaGFkb3dyb290ID0gd3JhcHBlci5hdHRhY2hTaGFkb3coeyBtb2RlOiAnb3BlbicgfSk7XG4gIHNoYWRvd3Jvb3QuaW5uZXJIVE1MID0gYFxuICAgIDxzdHlsZT5AaW1wb3J0IHVybChcIiR7Y2hyb21lLmV4dGVuc2lvbi5nZXRVUkwoJ2NvbnRlbnQvc3R5bGVzLmNzcycpfVwiKTs8L3N0eWxlPlxuICAgIDxkaXYgaWQ9XCIke2lkfVwiIC8+YDtcbiAgcmV0dXJuIHNoYWRvd3Jvb3Q7XG59O1xuXG5leHBvcnQgY29uc3QgZ2V0ID0gKG9iamVjdCwgcGF0aCkgPT4ge1xuICBjb25zdCBhcnJheVBhdGggPSB0eXBlb2YgcGF0aCA9PT0gJ3N0cmluZycgPyBwYXRoLnNwbGl0KCcuJykgOiBwYXRoO1xuICBpZiAoQXJyYXkuaXNBcnJheShvYmplY3QpICYmIE51bWJlci5pc05hTihwYXJzZUludChhcnJheVBhdGhbMF0sIDEwKSkpIHtcbiAgICByZXR1cm4gb2JqZWN0XG4gICAgICAubWFwKChvYmplY3REYXRhKSA9PiBnZXQob2JqZWN0RGF0YSwgWy4uLmFycmF5UGF0aF0pKVxuICAgICAgLmZsYXQoSW5maW5pdHkpO1xuICB9XG4gIGNvbnN0IGRhdGEgPSBvYmplY3RbYXJyYXlQYXRoWzBdXTtcbiAgYXJyYXlQYXRoLnNoaWZ0KCk7XG4gIGlmIChkYXRhICYmIGFycmF5UGF0aC5sZW5ndGgpIHtcbiAgICByZXR1cm4gZ2V0KGRhdGEsIGFycmF5UGF0aCk7XG4gIH1cbiAgcmV0dXJuIGRhdGE7XG59O1xuXG5leHBvcnQgY29uc3Qgc3Vic3RpdHV0ZSA9IChzdHIsIG9iaikgPT4ge1xuICBsZXQgcmVzdWx0ID0gc3RyO1xuICBpZiAob2JqKSB7XG4gICAgT2JqZWN0LmVudHJpZXMob2JqKS5mb3JFYWNoKChbbmFtZSwgdmFsdWVdKSA9PiB7XG4gICAgICByZXN1bHQgPSBzdHIucmVwbGFjZShgJSR7bmFtZX1gLCB2YWx1ZSk7XG4gICAgfSk7XG4gIH1cbiAgcmV0dXJuIHJlc3VsdDtcbn07XG4iXSwic291cmNlUm9vdCI6IiJ9