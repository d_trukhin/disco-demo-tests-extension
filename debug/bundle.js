/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./source/debug/app.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./source/debug/app.js":
/*!*****************************!*\
  !*** ./source/debug/app.js ***!
  \*****************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _app_less__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./app.less */ "./source/debug/app.less");
/* harmony import */ var _app_less__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_app_less__WEBPACK_IMPORTED_MODULE_0__);


var _browser$extension$ge = browser.extension.getBackgroundPage(),
    app = _browser$extension$ge.app; // User


var userJSON = JSON.stringify(app.user.data, null, 2);
document.getElementById('user').textContent = userJSON; // Cookies awin1.com

browser.cookies.getAll({
  domain: 'awin1.com'
}).then(function (cookies) {
  var cookiesJSON = JSON.stringify(cookies, null, 2);
  document.getElementById('awin-cookies').textContent = cookiesJSON;
}); // Merchants

var merchantsData = app.merchants.data;
var merchantsStates = app.merchants.states;
var merchantsValidations = app.validationStore.validatedMerchants;
var merchants = merchantsData.map(function (merchantData) {
  var id = merchantData.id;
  var state = merchantsStates[id];
  var validation = merchantsValidations[id] || {};
  return {
    merchant: merchantData,
    state: state,
    validation: validation
  };
});
var merchantsJSON = JSON.stringify(merchants, null, 2);
document.getElementById('merchants').textContent = merchantsJSON;

/***/ }),

/***/ "./source/debug/app.less":
/*!*******************************!*\
  !*** ./source/debug/app.less ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc291cmNlL2RlYnVnL2FwcC5qcyIsIndlYnBhY2s6Ly8vLi9zb3VyY2UvZGVidWcvYXBwLmxlc3M/NTY5ZCJdLCJuYW1lcyI6WyJicm93c2VyIiwiZXh0ZW5zaW9uIiwiZ2V0QmFja2dyb3VuZFBhZ2UiLCJhcHAiLCJ1c2VySlNPTiIsIkpTT04iLCJzdHJpbmdpZnkiLCJ1c2VyIiwiZGF0YSIsImRvY3VtZW50IiwiZ2V0RWxlbWVudEJ5SWQiLCJ0ZXh0Q29udGVudCIsImNvb2tpZXMiLCJnZXRBbGwiLCJkb21haW4iLCJ0aGVuIiwiY29va2llc0pTT04iLCJtZXJjaGFudHNEYXRhIiwibWVyY2hhbnRzIiwibWVyY2hhbnRzU3RhdGVzIiwic3RhdGVzIiwibWVyY2hhbnRzVmFsaWRhdGlvbnMiLCJ2YWxpZGF0aW9uU3RvcmUiLCJ2YWxpZGF0ZWRNZXJjaGFudHMiLCJtYXAiLCJtZXJjaGFudERhdGEiLCJpZCIsInN0YXRlIiwidmFsaWRhdGlvbiIsIm1lcmNoYW50IiwibWVyY2hhbnRzSlNPTiJdLCJtYXBwaW5ncyI6IjtRQUFBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBOzs7UUFHQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMENBQTBDLGdDQUFnQztRQUMxRTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLHdEQUF3RCxrQkFBa0I7UUFDMUU7UUFDQSxpREFBaUQsY0FBYztRQUMvRDs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0EseUNBQXlDLGlDQUFpQztRQUMxRSxnSEFBZ0gsbUJBQW1CLEVBQUU7UUFDckk7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwyQkFBMkIsMEJBQTBCLEVBQUU7UUFDdkQsaUNBQWlDLGVBQWU7UUFDaEQ7UUFDQTtRQUNBOztRQUVBO1FBQ0Esc0RBQXNELCtEQUErRDs7UUFFckg7UUFDQTs7O1FBR0E7UUFDQTs7Ozs7Ozs7Ozs7OztBQ2xGQTtBQUFBO0FBQUE7QUFBQTs7NEJBRWdCQSxPQUFPLENBQUNDLFNBQVIsQ0FBa0JDLGlCQUFsQixFO0lBQVJDLEcseUJBQUFBLEcsRUFFUjs7O0FBQ0EsSUFBTUMsUUFBUSxHQUFHQyxJQUFJLENBQUNDLFNBQUwsQ0FBZUgsR0FBRyxDQUFDSSxJQUFKLENBQVNDLElBQXhCLEVBQThCLElBQTlCLEVBQW9DLENBQXBDLENBQWpCO0FBQ0FDLFFBQVEsQ0FBQ0MsY0FBVCxDQUF3QixNQUF4QixFQUFnQ0MsV0FBaEMsR0FBOENQLFFBQTlDLEMsQ0FFQTs7QUFDQUosT0FBTyxDQUFDWSxPQUFSLENBQWdCQyxNQUFoQixDQUF1QjtBQUFFQyxRQUFNLEVBQUU7QUFBVixDQUF2QixFQUFnREMsSUFBaEQsQ0FBcUQsVUFBQ0gsT0FBRCxFQUFhO0FBQ2hFLE1BQU1JLFdBQVcsR0FBR1gsSUFBSSxDQUFDQyxTQUFMLENBQWVNLE9BQWYsRUFBd0IsSUFBeEIsRUFBOEIsQ0FBOUIsQ0FBcEI7QUFDQUgsVUFBUSxDQUFDQyxjQUFULENBQXdCLGNBQXhCLEVBQXdDQyxXQUF4QyxHQUFzREssV0FBdEQ7QUFDRCxDQUhELEUsQ0FLQTs7QUFDQSxJQUFNQyxhQUFhLEdBQUdkLEdBQUcsQ0FBQ2UsU0FBSixDQUFjVixJQUFwQztBQUNBLElBQU1XLGVBQWUsR0FBR2hCLEdBQUcsQ0FBQ2UsU0FBSixDQUFjRSxNQUF0QztBQUNBLElBQU1DLG9CQUFvQixHQUFHbEIsR0FBRyxDQUFDbUIsZUFBSixDQUFvQkMsa0JBQWpEO0FBRUEsSUFBTUwsU0FBUyxHQUFHRCxhQUFhLENBQUNPLEdBQWQsQ0FBa0IsVUFBQ0MsWUFBRCxFQUFrQjtBQUFBLE1BQzVDQyxFQUQ0QyxHQUNyQ0QsWUFEcUMsQ0FDNUNDLEVBRDRDO0FBR3BELE1BQU1DLEtBQUssR0FBR1IsZUFBZSxDQUFDTyxFQUFELENBQTdCO0FBQ0EsTUFBTUUsVUFBVSxHQUFHUCxvQkFBb0IsQ0FBQ0ssRUFBRCxDQUFwQixJQUE0QixFQUEvQztBQUVBLFNBQU87QUFDTEcsWUFBUSxFQUFFSixZQURMO0FBRUxFLFNBQUssRUFBTEEsS0FGSztBQUdMQyxjQUFVLEVBQVZBO0FBSEssR0FBUDtBQUtELENBWGlCLENBQWxCO0FBYUEsSUFBTUUsYUFBYSxHQUFHekIsSUFBSSxDQUFDQyxTQUFMLENBQWVZLFNBQWYsRUFBMEIsSUFBMUIsRUFBZ0MsQ0FBaEMsQ0FBdEI7QUFDQVQsUUFBUSxDQUFDQyxjQUFULENBQXdCLFdBQXhCLEVBQXFDQyxXQUFyQyxHQUFtRG1CLGFBQW5ELEM7Ozs7Ozs7Ozs7O0FDakNBLHVDIiwiZmlsZSI6ImRlYnVnL2J1bmRsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb24gZm9yIGhhcm1vbnkgZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kID0gZnVuY3Rpb24oZXhwb3J0cywgbmFtZSwgZ2V0dGVyKSB7XG4gXHRcdGlmKCFfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZXhwb3J0cywgbmFtZSkpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgbmFtZSwgeyBlbnVtZXJhYmxlOiB0cnVlLCBnZXQ6IGdldHRlciB9KTtcbiBcdFx0fVxuIFx0fTtcblxuIFx0Ly8gZGVmaW5lIF9fZXNNb2R1bGUgb24gZXhwb3J0c1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yID0gZnVuY3Rpb24oZXhwb3J0cykge1xuIFx0XHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcbiBcdFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcbiBcdFx0fVxuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xuIFx0fTtcblxuIFx0Ly8gY3JlYXRlIGEgZmFrZSBuYW1lc3BhY2Ugb2JqZWN0XG4gXHQvLyBtb2RlICYgMTogdmFsdWUgaXMgYSBtb2R1bGUgaWQsIHJlcXVpcmUgaXRcbiBcdC8vIG1vZGUgJiAyOiBtZXJnZSBhbGwgcHJvcGVydGllcyBvZiB2YWx1ZSBpbnRvIHRoZSBuc1xuIFx0Ly8gbW9kZSAmIDQ6IHJldHVybiB2YWx1ZSB3aGVuIGFscmVhZHkgbnMgb2JqZWN0XG4gXHQvLyBtb2RlICYgOHwxOiBiZWhhdmUgbGlrZSByZXF1aXJlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnQgPSBmdW5jdGlvbih2YWx1ZSwgbW9kZSkge1xuIFx0XHRpZihtb2RlICYgMSkgdmFsdWUgPSBfX3dlYnBhY2tfcmVxdWlyZV9fKHZhbHVlKTtcbiBcdFx0aWYobW9kZSAmIDgpIHJldHVybiB2YWx1ZTtcbiBcdFx0aWYoKG1vZGUgJiA0KSAmJiB0eXBlb2YgdmFsdWUgPT09ICdvYmplY3QnICYmIHZhbHVlICYmIHZhbHVlLl9fZXNNb2R1bGUpIHJldHVybiB2YWx1ZTtcbiBcdFx0dmFyIG5zID0gT2JqZWN0LmNyZWF0ZShudWxsKTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5yKG5zKTtcbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KG5zLCAnZGVmYXVsdCcsIHsgZW51bWVyYWJsZTogdHJ1ZSwgdmFsdWU6IHZhbHVlIH0pO1xuIFx0XHRpZihtb2RlICYgMiAmJiB0eXBlb2YgdmFsdWUgIT0gJ3N0cmluZycpIGZvcih2YXIga2V5IGluIHZhbHVlKSBfX3dlYnBhY2tfcmVxdWlyZV9fLmQobnMsIGtleSwgZnVuY3Rpb24oa2V5KSB7IHJldHVybiB2YWx1ZVtrZXldOyB9LmJpbmQobnVsbCwga2V5KSk7XG4gXHRcdHJldHVybiBucztcbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cblxuIFx0Ly8gTG9hZCBlbnRyeSBtb2R1bGUgYW5kIHJldHVybiBleHBvcnRzXG4gXHRyZXR1cm4gX193ZWJwYWNrX3JlcXVpcmVfXyhfX3dlYnBhY2tfcmVxdWlyZV9fLnMgPSBcIi4vc291cmNlL2RlYnVnL2FwcC5qc1wiKTtcbiIsImltcG9ydCAnLi9hcHAubGVzcyc7XG5cbmNvbnN0IHsgYXBwIH0gPSBicm93c2VyLmV4dGVuc2lvbi5nZXRCYWNrZ3JvdW5kUGFnZSgpO1xuXG4vLyBVc2VyXG5jb25zdCB1c2VySlNPTiA9IEpTT04uc3RyaW5naWZ5KGFwcC51c2VyLmRhdGEsIG51bGwsIDIpO1xuZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ3VzZXInKS50ZXh0Q29udGVudCA9IHVzZXJKU09OO1xuXG4vLyBDb29raWVzIGF3aW4xLmNvbVxuYnJvd3Nlci5jb29raWVzLmdldEFsbCh7IGRvbWFpbjogJ2F3aW4xLmNvbScgfSkudGhlbigoY29va2llcykgPT4ge1xuICBjb25zdCBjb29raWVzSlNPTiA9IEpTT04uc3RyaW5naWZ5KGNvb2tpZXMsIG51bGwsIDIpO1xuICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnYXdpbi1jb29raWVzJykudGV4dENvbnRlbnQgPSBjb29raWVzSlNPTjtcbn0pO1xuXG4vLyBNZXJjaGFudHNcbmNvbnN0IG1lcmNoYW50c0RhdGEgPSBhcHAubWVyY2hhbnRzLmRhdGE7XG5jb25zdCBtZXJjaGFudHNTdGF0ZXMgPSBhcHAubWVyY2hhbnRzLnN0YXRlcztcbmNvbnN0IG1lcmNoYW50c1ZhbGlkYXRpb25zID0gYXBwLnZhbGlkYXRpb25TdG9yZS52YWxpZGF0ZWRNZXJjaGFudHM7XG5cbmNvbnN0IG1lcmNoYW50cyA9IG1lcmNoYW50c0RhdGEubWFwKChtZXJjaGFudERhdGEpID0+IHtcbiAgY29uc3QgeyBpZCB9ID0gbWVyY2hhbnREYXRhO1xuXG4gIGNvbnN0IHN0YXRlID0gbWVyY2hhbnRzU3RhdGVzW2lkXTtcbiAgY29uc3QgdmFsaWRhdGlvbiA9IG1lcmNoYW50c1ZhbGlkYXRpb25zW2lkXSB8fCB7fTtcblxuICByZXR1cm4ge1xuICAgIG1lcmNoYW50OiBtZXJjaGFudERhdGEsXG4gICAgc3RhdGUsXG4gICAgdmFsaWRhdGlvbixcbiAgfTtcbn0pO1xuXG5jb25zdCBtZXJjaGFudHNKU09OID0gSlNPTi5zdHJpbmdpZnkobWVyY2hhbnRzLCBudWxsLCAyKTtcbmRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdtZXJjaGFudHMnKS50ZXh0Q29udGVudCA9IG1lcmNoYW50c0pTT047XG4iLCIvLyBleHRyYWN0ZWQgYnkgbWluaS1jc3MtZXh0cmFjdC1wbHVnaW4iXSwic291cmNlUm9vdCI6IiJ9